<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class OtherGI_Controller extends Library_Controller {

    public function get_table_header(){
        $array_insert_header = [];
        $array_insert_header[Other_Kategori::$S_ASSET] = ["No", "Kode Material", "Deskripsi", "Qty", "Net Price", "Harga Total"];
        $array_insert_header[Other_Kategori::$S_SERVICE] = ["No", "Kode Service", "Deskripsi", "Kode Asset", "Deskripsi", "Qty", "Net Price"];
        $array_insert_header[Other_Kategori::$S_MISC] = ["No", "Kode Material", "Deskripsi", "Qty", "Net Price", "Harga Total"];
        $array_insert_header[Other_Kategori::$S_SPAREPART] = ["No", "Kode Material", "Deskripsi", "Stock", "Qty Keluar", "Kode GL", "Kode CC", "Alasan"];


        $id_kategori = $this->i_p("k");
        $kategori = $this->other_model->get(Other_Kategori::$TABLE_NAME, Other_Kategori::$ID, $id_kategori)->row_array()[Other_Kategori::$NAMA];

        $kal = "<tr>";
        for ($i = 0; $i < count($array_insert_header[$kategori]); $i++) {
            $kal .= "<td class='text-center fw-bold'>". $array_insert_header[$kategori][$i] ."</td>";
        }
        $kal .= "<td class='text-center fw-bold'>Aksi</td>";

        $kal .= "</tr>";

        echo $kal;
    }

    public function insert_plus(){ 
        $ctr = $this->i_p("c");
        $tipe = $this->i_p("t");

        $id_kategori= $this->i_p("k");
        $kategori = $this->other_model->get(Other_Kategori::$TABLE_NAME, Other_Kategori::$ID, $id_kategori)->row_array()[Other_Kategori::$NAMA];

        if ($this->equals($tipe, OTHER_GI_PLUS_TYPE_INSERT)) {
            $kal = '
                <tr class="tr_detail_product_' . $ctr . '">
                    <td class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>';

            if (!$this->equals($kategori, Other_Kategori::$S_SERVICE)) {
                $kal .= '<td style="width:10%" class="text-center font-sm f-aleo"> 
                            <input style="width:100%"onkeyup="cek_material('. $id_kategori .',' . $ctr . ')" class="f-aleo" type="text" id="txt_id_material_' . $ctr . '"/>
                        </td>
                    
                        <td style="width:20%" class="text-center font-sm f-aleo"> 
                            <input style="width:100%" value="Material belum ditemukan" disabled class="red-text text-left f-aleo" type="text" id="txt_desc_material_' . $ctr . '"/>
                        </td>';
            }
            if ($this->equals($kategori, Other_Kategori::$S_SERVICE)) {
                $kal .= '<td style="width:15%" class="text-center font-sm f-aleo"> 
                            <input style="width:50%" onkeyup="cek_service(' . $id_kategori . ',' . $ctr . ')" class="f-aleo" type="text" id="txt_id_service_' . $ctr . '"/>
                        </td>
                    
                        <td style="width:20%" class="text-center font-sm f-aleo"> 
                            <input style="width:100%" value="Service belum ditemukan" disabled class="red-text text-left" f-aleo" type="text" id="txt_desc_service_' . $ctr . '"/>
                        </td>
                        
                        <td style="width:15%" class="text-center font-sm f-aleo"> 
                            <input style="width:50%" onkeyup="cek_material(1,' . $ctr . ')" class="f-aleo" type="text" id="txt_id_material_' . $ctr . '"/>
                        </td>
                    
                        <td style="width:20%" class="text-center font-sm f-aleo"> 
                            <input style="width:100%" value="Material belum ditemukan" disabled class="red-text text-left" f-aleo" type="text" id="txt_desc_material_' . $ctr . '"/>
                        </td>';
            }

            //Qty Stock & Qty
            $kal .= '<td style="width:5%" class="text-center font-sm"> 
                        <input style="width:100%" type="number" class="text-right" disabled id="txt_qty_stock_' . $ctr . '"/>
                    </td>
                    <td style="width:5%" class="text-center font-sm"> 
                        <input style="width:100%" onkeyup="cek_stock('. $ctr .')" type="number" class="text-right" id="txt_qty_' . $ctr . '"/>
                    </td>';

            //Kode GI & Kode CC
            $kal .= '<td style="width:15%" class="text-center font-sm"> 
                        <input type="text" id="txt_kode_gl_' . $ctr . '" style="width:100%" onkeyup="get_gl_by_id(' . $ctr . ')" class="black-text">
                        <input type="text" id="txt_desc_gl_' . $ctr . '" style="width:100%" value="Input Kode GL" disabled class="red-text pt">
                    </td>
                    <td style="width:15%" class="text-center font-sm"> 
                        <input type="text" id="txt_kode_cc_' . $ctr . '" style="width:100%" onkeyup="get_cc_by_id(' . $ctr . ')" class="black-text">
                        <input type="text" id="txt_desc_cc_' . $ctr . '" style="width:100%" value="Input Kode CC " disabled class="red-text pt">
                    </td>';

            //Keterangan
            $kal .= '<td style="width:20%" class="text-center font-sm"> 
                            <textarea rows=3 style="width:100%;resize:none" placeholder="Alasan pemakaian" class="text-left f-aleo" type="text" id="txt_alasan_' . $ctr . '"></textarea>
                        </td>';

            $kal .= '<td style="width:2%" class="font-sm text-center"> 
                    <button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
                    X
                    </button>
                </td>
            </tr>';
        }
        echo $kal;
    }
    public function insert_po(){
        $this->insert(Other_Purchase_Order::$TABLE_NAME);
    }
    public function insert_gi(){
        $this->insert(Other_GI_Header::$TABLE_NAME);
    }

    //1 RM PM, 2 LAIN LAIN
    public static $STATIC_HEADER_REKAP = [
        "Tanggal", "Nomor PO", "Vendor", "No", "Material / Asset", "Deskripsi", "Service", "Deskripsi", "Qty Awal (KG/PCS)", "Qty Sisa (KG/PCS)","Net Price (Rupiah)"
    ];
    public static $STATIC_HEADER_REKAP_CLASS = [
        "cb_0", "cb_0", "cb_0", "cb_0", "cb_0", "cb_0", "cb_2", "cb_2", "cb_0", "cb_1", "cb_2"
    ];

    public function get_cb_kolom_tampil(){
        $data = [];

        $kal = "<div class='row'>";
        for ($i = 0; $i < count($this::$STATIC_HEADER_REKAP); $i++) {
            $kal .= "<div class='col-sm-3'>
                        <label>
                        <input class='rekap_class " . $this::$STATIC_HEADER_REKAP_CLASS[$i] . "' type='checkbox' value='tanggal' id='cb_" . $i . "'></input>
                        " . $this::$STATIC_HEADER_REKAP[$i] . "</label>
                    </div>";
        }
        $kal .= "</div>";
        $data["kal"] = $kal;
        $data["ctr"] = count($this::$STATIC_HEADER_REKAP);
        echo json_encode($data);
    }
    public function get_view_report(){
        $kal = "";
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $arr_header_class = $this->i_p("hc");
        $data = $this->other_model->po_get_report($dt, $st);

        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>';
        $kal .= '<tr class="table-primary">';
        for ($i = 0; $i < count($this::$STATIC_HEADER_REKAP); $i++) {
            if ($arr_header_class[$i] == 1)
                $kal .= '<th scope="col" class="fw-bold align-middle text-center">' . $this::$STATIC_HEADER_REKAP[$i] . '</th>';
        }
        $kal .= '</tr></thead>';
        $kal .= '<tbody>';
        foreach ($data->result_array() as $row_po) {
            $ctr = 0;
            $kal .= "<tr>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Other_Purchase_Order::$TANGGAL] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Other_Purchase_Order::$ID] . "</td>";
            //$kal .= "<td class='text-center'>" . ($row_po[Purchase_Order_Awal::$NOMOR_URUT]/10) . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>" . $row_po[Other_Purchase_Order_Awal::$NAMA_VENDOR] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Other_Purchase_Order_Awal::$NOMOR_URUT] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Other_Purchase_Order_Awal::$ID_OTHER_MATERIAL] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>" . $row_po[Other_Purchase_Order_Awal::$NAMA_MATERIAL] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Other_Purchase_Order_Awal::$ID_OTHER_SERVICE] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>" . $row_po[Other_Purchase_Order_Awal::$NAMA_SERVICE] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-right'>" . $this->decimal($row_po[Other_Purchase_Order_Awal::$QTY], 3) . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-right'>-</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-right'>". $this->rupiah($row_po[Other_Purchase_Order_Awal::$NET_PRICE]) ."</td>";
            $kal .= "</tr>";
        }
        $kal .= '   </tbody>
                        </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_by_no(){
        $data = [];
        $table_detail = "";
        $data['detail_product'] = '
						<thead>
							<tr>
								<td class="text-center font-sm f-aleo">Nomor</td>
								<td class="text-center font-sm f-aleo">Material</td>
								<td class="text-center font-sm f-aleo">Qty PO</td>
								<td class="text-center font-sm f-aleo">Qty Diterima</td>
							</tr>
						</thead><tbody>';

        $nomor_po = $this->i_p("n");
        $qry = $this->other_model->po_get_by_no($nomor_po);
        $data['num_rows'] = $qry[Other_Purchase_Order::$TABLE_NAME]->num_rows();
        $ctr = 0;

        if ($qry[Other_Purchase_Order_Awal::$TABLE_NAME]->num_rows() > 0) {
            $data[Other_Purchase_Order_Awal::$NOMOR_PO] = "PO Nomor " . $qry[Other_Purchase_Order_Awal::$TABLE_NAME]->row_array()[Other_Purchase_Order_Awal::$NOMOR_PO] . "<br>
														 Tanggal : " . date('d M Y', strtotime($qry[Other_Purchase_Order::$TABLE_NAME]->row_array()[Other_Purchase_Order::$TANGGAL])) . "<br>
                                                         Vendor: " . $qry[Other_Purchase_Order_Awal::$TABLE_NAME]->row_array()[Other_Purchase_Order_Awal::$NAMA_VENDOR] ;
            foreach ($qry[Other_Purchase_Order_Awal::$TABLE_NAME]->result_array() as $row_po_awal) {
                $qty = $this->other_model->po_get_last_qty(
                    $row_po_awal[Other_Purchase_Order::$ID],
                    $row_po_awal[Other_Purchase_Order_Awal::$NOMOR_URUT],
                    $row_po_awal[Other_Purchase_Order_Awal::$QTY]
                );
                if ($qty > 0) {
                    $table_detail .= "<tr>";
                    $table_detail .= '<td class="text-center font-sm f-aleo">' . $row_po_awal[Other_Purchase_Order_Awal::$NOMOR_URUT] . '<input type="hidden" id="txt_no_urut_' . $ctr . '" value=' . $row_po_awal[Other_Purchase_Order_Awal::$NOMOR_URUT]  . ' /></td>';
                    $table_detail .= '<td class="text-left font-sm f-aleo">' . $row_po_awal[Other_Purchase_Order_Awal::$NAMA_MATERIAL] . '<input type="hidden" id="txt_id_material_' . $ctr . '" value="' . $row_po_awal[Other_Purchase_Order_Awal::$ID_OTHER_MATERIAL] . '" /></td>';
                    $table_detail .= '<td class="text-right font-sm f-aleo">' . $qty . '<input type="hidden" id="txt_qty_' . $ctr . '" value=' . $qty . ' /></td>';
                    $table_detail .= '<td class="text-center font-sm f-aleo"><input type="number" class="text-right" placeholder=0 onkeyup="check_qty(' . $ctr . ')" id="txt_qty_terima_' . $ctr . '"></input></td>';
                    $table_detail .= "</tr>";
                    $ctr += 1;
                }
            }
        }
        $data['ctr'] = $ctr;
        $data['detail_product'] .= $table_detail;
        $data['detail_product'] .= "</tbody>";

        echo json_encode($data);
    }

    public function gr_insert(){
        $this->insert(Other_GR_Header::$TABLE_NAME);
    }

    /*public function material_get_by_id(){
        $id_material = $this->i_p("im");
        $kategori = $this->i_p("ik");
        $data = $this->other_model->material_get_by_id($id_material, $kategori);
        $temp = [];
        if($data->num_rows()>0){
            foreach ($data->result_array() as $row) {
                $temp[Other_Material::$ID] = $row[Other_Material::$ID];
                $temp[Other_Material::$DESKRIPSI] = $row[Other_Material::$DESKRIPSI];

            }
        }
        echo json_encode($temp);
    }*/
}
