<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Test_Controller extends Library_Controller {

    public function index(){
        $this->load->view('tes');
    }
}
