<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class WS_Controller extends Library_Controller {

    public function upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_work_sheet";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->ws_model->update_batch($data);
    }
    public function create_new(){
        $semi_id = $this->i_p("si");
        $semi_qty = $this->i_p("sq");
        $fg_id = $this->i_p("fi");
        $fg_qty = $this->i_p("fq");
        $ctr = $this->i_p("c");

        echo MESSAGE[$this->ws_model->create_new($ctr, $semi_id, $semi_qty, $fg_id, $fg_qty)];
    }
    
    public function checker_insert(){
        $this->insert(Worksheet_Checker_Header::$TABLE_NAME);
    }
    public function checker_adjustment(){
        $this->insert(Worksheet_Checker_Header::$S_ADJUSTMENT_CHECKER);
    }
    public function admin_proses_insert(){
        $this->insert($this::$NAV_WORK_SHEET_ADMIN_PROSES);
    }
    public function checker_fg_insert_gr(){
        $this->insert($this::$NAV_WORK_SHEET_CHECKER_FG_INSERT_GR);
    }
    public function cancel(){
        $nomor_gi = $this->i_p("n");
        echo MESSAGE[$this->ws_model->cancel($nomor_gi)];
    }
    public function change_status(){
        $nomor_ws=$this->i_p("n");
        $is_deleted=$this->i_p("i");

        echo MESSAGE[$this->ws_model->change_status($nomor_ws,$is_deleted)];
    }

    public function plus(){
        $ctr = $this->i_p("c");

        //BOM Datalist
        $data_list = "<datalist id='list_bom_$ctr'>";
        $data_bom = $this->bom_model->get(BOM::$TABLE_NAME,null,null,null,null,false);
        if ($data_bom->num_rows() > 0) {
            foreach ($data_bom->result_array() as $row_bom) {
                if(substr($row_bom[BOM::$ID], 0, 4) === '8000')
                    $data_list .= "<option value=" . $row_bom[BOM::$ID] . ">" . $row_bom[BOM::$DESCRIPTION] . "</option>";
            }
        }
        $data_list .= "</datalist>";
        $kal = $data_list;
        
        $kal .= '
            <tr class="tr_detail_product_' . $ctr . '">
                <td class="text-center font-sm">' . ($ctr + 1) . '</td>';

            $kal .= '<td class="text-center font-sm">'.$data_list.' 
                        <input onkeyup="get_detail_fg(' . $ctr . ')" class="form-control" type="text" list="list_bom_'.$ctr.'" id="txt_kode_fg_' . $ctr . '"/>
                    </td>
                
                    <td class="text-center font-sm"> 
                        <input style="width:100%" value="Produk belum ditemukan" disabled class="form-control red-text text-left" type="text" id="txt_desc_fg_' . $ctr . '"/>
                    </td>
                        
                    <td class="text-center font-sm"> 
                        <input type="hidden" id="txt_pv_fg_'.$ctr.'">
                        <input style="width:100%" value="Alt BOM belum ditemukan" disabled class="form-control red-text text-right" type="text" id="txt_bom_fg_' . $ctr . '"/>
                    </td>
                    
                    <td class="text-center font-sm"> 
                        <input style="width:100%" disabled class="form-control text-right" type="number" id="txt_kg_fg_' . $ctr . '"/>
                    </td>
                    
                    <td class="text-center font-sm"> 
                        <input style="width:100%" onkeyup="is_number_key(event,' . $ctr . ')" class="form-control text-right" type="number" id="txt_tin_' . $ctr . '"/>
                    </td>

                    <td class="text-center font-sm"> 
                        <input style="width:100%" value=0 disabled class="form-control text-right" type="number" id="txt_kg_' . $ctr . '"/>
                    </td>';

        $kal .= '<td style="width:2%" class="font-sm text-center"> 
                <button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
                X
                </button>
            </td>
        </tr>';
        
        echo $kal;
    }

    public function get_last_number(){
        $number = $this->ws_model->get_last_number();
        echo $number;
    }
    public function get_view_by_no(){
        $data = [];
        $table_detail = "";
        $data['detail_product'] = '
						<thead>
							<tr>
								<td class="text-center font-sm ">
                                Status <input type="checkbox" id="cb_toggle" onchange="toggle_all()"></td>
								<td class="text-center font-sm ">No</td>
								<td class="text-center font-sm ">Material</td>
								<td class="text-center font-sm ">Jenis</td>
								<td class="text-center font-sm ">Qty</td>
								<td class="text-center font-sm ">Vendor</td>
								<td class="text-center font-sm ">Stock (KG/PCS)</td>
								<td class="text-center font-sm ">Qty Diproses</td>
							</tr>
						</thead><tbody>';
        $ws = $this->i_p("n");
        $qry = $this->ws_model->get_by_no($ws);
        $data['num_rows'] = $qry[Worksheet_Awal::$TABLE_NAME]->num_rows();
        $ctr = 0;
        if ($qry[Worksheet_Awal::$TABLE_NAME]->num_rows() > 0) {
            $data[Worksheet_Awal::$NOMOR_WS] = "WS Nomor " . $qry[Worksheet_Awal::$TABLE_NAME]->row_array()[Worksheet_Awal::$NOMOR_WS] . "<br>
														 Tanggal : " . date('d M Y', strtotime($qry[Worksheet::$TABLE_NAME]->row_array()[Worksheet::$TANGGAL]));
            $data[Worksheet::$TANGGAL]=$qry[Worksheet::$TABLE_NAME]->row_array()[Worksheet::$TANGGAL];
            foreach ($qry[Worksheet_Awal::$TABLE_NAME]->result_array() as $row_ws_awal) {
                $is_konsinyasi_and_not = false;
                $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM]);
                if ($qry_material->num_rows() > 1) {
                    foreach ($qry_material->result_array() as $row_material) {
                        if (!$row_material[Material_RM_PM::$IS_KONSINYASI]) {
                            $material = $row_material;
                            $is_konsinyasi_and_not = true;
                        }
                    }
                } else {
                    $material = $qry_material->row_array();
                }
                $checker_rm_pm = $this->rmpmchecker_model->get(Checker_RM_PM::$TABLE_NAME, Checker_RM_PM::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array();
                $is_same = false;
                if ($this->equals($checker_rm_pm[Checker_RM_PM::$JENIS], $material[Material_RM_PM::$JENIS])) {
                    $is_same = true;
                }
                $disabled = "disabled";
                $checked = "";
                if ($is_same) {
                    $disabled = "";
                    $checked = "checked";
                }
                $qty = 0;
                $is_cetak_detail = true;
                $is_made_once = false;
                $only_first = false;
                foreach ($qry[Worksheet_Checker_Header::$TABLE_NAME]->result_array() as $row_header) {
                    if(!$only_first){
                        //Hanya ambil yang pertama supaya tanggal awal
                        $data[Worksheet::$TANGGAL] = $row_header[Worksheet_Checker_Header::$TANGGAL];
                        $only_first = true;
                    }
                    if ($this->equals($row_ws_awal[Worksheet_Awal::$NOMOR_WS], $row_header[Worksheet_Checker_Header::$NOMOR_WS])) {
                        foreach ($qry[Worksheet_Checker_Detail::$TABLE_NAME]->result_array() as $row_detail) {
                            if ($this->equals($row_ws_awal[Worksheet_Awal::$NOMOR_URUT], $row_detail[Worksheet_Checker_Detail::$NOMOR_URUT])) {
                                $qty = $this->ws_model->get_last_qty($row_ws_awal[Worksheet_Awal::$NOMOR_WS], $row_ws_awal[Worksheet_Awal::$NOMOR_URUT], $row_ws_awal[Worksheet_Awal::$QTY]);
                                $is_made_once = true;
                            }
                        }
                    }
                }
                if (!$is_made_once)
                    $qty = $row_ws_awal[Worksheet_Awal::$QTY];

                if ($qty <= 0) {
                    $is_cetak_detail = false;
                }
                if ($is_cetak_detail) {
                    $table_detail .= "<tr>";
                    $table_detail .= '<td class="text-center align-center font-sm ">
                                                    <input class="checkbox_ws" type="checkbox" ' . $checked . ' onchange="toggle_checkbox_qty(' . $ctr . ')" id="cb_qty_' . $ctr . '">
                                                </td>';
                    $table_detail .= '<td class="text-center font-sm ">' . $row_ws_awal[Worksheet_Awal::$NOMOR_URUT] . '<input type="hidden" id="txt_no_urut_' . $ctr . '" value=' . $row_ws_awal[Purchase_Order_Awal::$NOMOR_URUT]  . '/></td>';
                    $table_detail .= '<td class="text-center font-sm ">' . $material[Material_RM_PM::$KODE_PANGGIL] . '<input type="hidden" id="txt_id_material_' . $ctr . '" value="' . $material[Material_RM_PM::$ID] . '" /></td>';
                    $table_detail .= '<td class="text-center font-sm ">' . $material[Material_RM_PM::$JENIS] . '</td>';
                    $table_detail .= '<td class="text-right font-sm "><span id="txt_view_qty_ws_' . $ctr . '">' . $this->decimal($qty) . ' ' . $material[Material_RM_PM::$UNIT] . '</span><input type="hidden" id="txt_qty_ws_' . $ctr . '" value=' . $qty . '></input></td>';
                    $qry_stock = $this->rmpm_model->get(Material_RM_PM_Stock::$TABLE_NAME, Material_RM_PM::$ID, $material[Material_RM_PM::$ID], null, null, false);

                    $td_vendor = "";
                    $stock = 0;
                    $idx_vendor = 0;
                    $kal_vendor = "";
                    if ($qry_stock->num_rows() > 1) { //jika material rm pm (konsinyasi dan tidak konsinyasi) ada 2 stock atau lebih berarti harus ada pilihan vendor
                        $stock = $qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
                        if (!$is_konsinyasi_and_not) { //jika material tsb bukan punya 2 tipe (konsinyasi dan bukan)
                            $td_vendor .= '<select id="cb_vendor_' . $ctr . '" ' . $disabled . ' class="font-sm " onchange=change_stock(' . $ctr . ')>';
                            foreach ($qry_stock->result_array() as $row_stock) {
                                $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_stock[Material_RM_PM_Stock::$ID_VENDOR])->row_array()[Vendor::$NAMA];
                                $td_vendor .= '<option value=' . $row_stock[Material_RM_PM_Stock::$QTY] . '>' . $vendor . '</option>';
                                $kal_vendor .= '<input type="hidden" id="txt_id_vendor_' . $ctr . '_' . ($idx_vendor++) . '" value="' . $row_stock[Material_RM_PM_Stock::$ID_VENDOR] . '"/>';
                            }
                            $td_vendor .= "</select>";
                            $td_vendor .= $kal_vendor;
                        }
                    } else if ($qry_stock->num_rows() > 0) {
                        $stock = $qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
                    } else {
                        $stock = 0;
                    }

                    //Vendor
                    $table_detail .= '<td class="text-right font-sm ">' . $td_vendor . '</td>';
                    $table_detail .= '<td class="font-sm "><input class="text-right" type="number" disabled value=' . $stock . ' id="txt_stock_' . $ctr . '"></input></td>';
                    $table_detail .= '<td class="font-sm "><input class="text-right" type="number" ' . $disabled . ' value=' . $qty . ' onkeyup="check_qty(' . $ctr . ')" id="txt_qty_proses_' . $ctr . '"></input></td>';
                    $table_detail .= "</tr>";
                    $ctr += 1;
                }
            }
        }
        $data['ctr'] = $ctr;
        $data['detail_product'] .= $table_detail;
        $data['detail_product'] .= "</tbody>";
        echo json_encode($data);
    }
    public function get_view_checker_insert(){
        $kal = "	
					<h3 class='text-center fw-bold'>INPUT WS</h3>
					<div class='row'>
						<div class='col-sm-1'></div>
						<div class='col-sm-10'>
							<h5 class='text-center col-sm-12 red-text fw-bold'>* Catatan: F1297 (Masukkan Qty Kemasan Sak), sisanya otomatis memotong Konsinyasi. </h5>
							<div class='row'>
								<div class='col-sm-2 text-right'></div>
								<div class='col-sm-2 text-right'>No. Work Sheet</div>
								<div class='col-sm-1 text-left '>:</div>
								<div class='col-sm-3 text-left'><input style='width:100%' onkeyup='get_ws_by_no()' type='text' id='txt_nomor_ws' /></div>
								<div class='col-sm-2 text-right'></div>
							</div>
							<div class='row'>
								<div class='col-sm-2 text-right'></div>
								<div class='col-sm-2 text-right'>Tanggal Transaksi</div>
								<div class='col-sm-1 text-left '>:</div>
								<div class='col-sm-3'><input class='black-text' style='width:100%' disabled type='date' id='txt_tanggal_buat' /></div>
								<div class='col-sm-2 text-right'></div>
							</div>
							<br>
							<h4 style='width:100%' id='txt_title_ws' class='text-center green-text'></h4>
						</div>
						<div class='col-sm-1'></div>
				   	</div>
					<div class='row'>
						<div class='col-sm-1'></div>
						<div class='col-sm-10 text-center'>
							<button type='button' id='btn_generate' disabled class='center text-center btn btn-outline-success' onclick='gen_ws()' data-mdb-ripple-color='dark'>
							GENERATE
							</button>
						</div>
						<div class='col-sm-1'></div>
					</div>
					<div class='row'>
						<div class='col-sm-12'>
							<table class='tabel_detail_product' style='visibility:hidden'>

							</table>
						</div>
					</div>";
        echo $kal;
    }
    public function get_view_checker_adjustment(){
        $tipe = $this->i_p("t");
        $header = "ADJUSTMENT WS";
        if($this->equals($tipe,RMPM_GET_TYPE_GR_SEMI))
            $header = "GR SEMI";
        $kal = "	
					<h3 class='text-center fw-bold '>$header</h3>
					<div class='row'>
						<div class='col-sm-1'></div>
						<div class='col-sm-10'>
							<div class='row'>
								<div class='col-sm-2  text-right'></div>
								<div class='col-sm-2  text-right'>No. Work Sheet</div>
								<div class='col-sm-1  text-left '>:</div>
								<div class='col-sm-3 text-left'><input class='' style='width:100%' onkeyup='get_ws_by_no()' type='text' id='txt_nomor_ws' /></div>
								<div class='col-sm-4  text-left' id='div_status'></div>
							</div>
							<div class='row'>
								<div class='col-sm-2  text-right'></div>
								<div class='col-sm-2  text-right'>Tanggal Transaksi</div>
								<div class='col-sm-1  text-left '>:</div>
								<div class='col-sm-3'><input class='black-text' style='width:100%' disabled type='date' id='txt_tanggal_buat' /></div>
								<div class='col-sm-2  text-right'></div>
							</div>
							<br>
							<h4 style='width:100%' id='txt_title_ws' class='text-center  green-text'></h4>
						</div>
						<div class='col-sm-1'></div>
				   	</div>
					<div class='row'>
						<div class='col-sm-1'></div>
						<div class='col-sm-10 text-center'>
							<button type='button' id='btn_plus_material' disabled class='center text-center btn btn-outline-primary' onclick='plus_product()' data-mdb-ripple-color='dark'>
							Tambah
							</button>
							<button type='button' id='btn_generate' disabled class='center text-center btn btn-outline-success' onclick='gen_adjustment_ws()' data-mdb-ripple-color='dark'>
							GENERATE
							</button>
						</div>
						<div class='col-sm-1'></div>
					</div>
					<div class='row'>
						<div class='col-sm-12'>
							<table class='tabel_detail_product' style='visibility:hidden'>

							</table>
						</div>
					</div>";
        echo $kal;
    }
    public function get_view_cancel(){
        $kal = "";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");
        $data = $this->ws_model->get_cancel_by_tanggal($dari_tanggal,$sampai_tanggal);

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Nomor Good Issue</th>
									<th scope="col" class="text-center">Aksi</th>
									';
        $kal .= '			</tr>
							</thead><tbody>';
        if (isset($data) && $data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row[Worksheet_Checker_Header::$ID] . "</td>";
                $kal .= "<td class='text-center'>" .
                    '<button onclick="fill_detail_modal(' . "'" . $row[Worksheet_Checker_Header::$ID] . "'" . ')" class="btn btn-outline-primary" data-mdb-ripple-color="dark">Ubah</button>' .
                    "</td>";
                $kal .= "</tr>";
            }
        }
        $kal .= '</tbody>
							</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_detail_modal(){
        $table_detail = "";
        $data['detail_product_checker'] = '
					
					<table id="tbl_detail_product_checker" class="table table-bordered table-sm">
						<thead>
							<tr class="table-primary">
								<th scope="col" class="text-center font-sm ">Nomor Urut</th>
								<th scope="col" class="text-center font-sm ">Material</th>
								<th scope="col" class="text-center font-sm ">Jenis</th>
								<th scope="col" class="text-center font-sm ">Vendor</th>
								<th scope="col" class="text-center font-sm ">Qty</th>
							</tr>
						</thead><tbody>';
        $gi = $this->i_p("n");
        $qry = $this->ws_model->checker_get_by_gi($gi);
        if ($qry[Worksheet_Checker_Header::$TABLE_NAME]->num_rows() > 0) {
            $data[Worksheet_Checker_Header::$NOMOR_WS] = $qry[Worksheet_Checker_Header::$TABLE_NAME]->row_array()[Worksheet_Checker_Header::$NOMOR_WS];
            $data[Worksheet_Checker_Header::$ID] = $qry[Worksheet_Checker_Header::$TABLE_NAME]->row_array()[Worksheet_Checker_Header::$ID];
            foreach ($qry[Worksheet_Checker_Detail::$TABLE_NAME]->result_array() as $row_detail) {

                $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_detail[Worksheet_Checker_Detail::$ID_MATERIAL]);
                $material = $qry_material->row_array();

                $table_detail .= "<tr>";
                if (isset($row_detail[Worksheet_Checker_Detail::$NOMOR_URUT]))
                    $table_detail .= '<td class="text-center font-sm ">' . $row_detail[Worksheet_Checker_Detail::$NOMOR_URUT] . '</td>';
                else
                    $table_detail .= '<td class="text-center font-sm "> (Adjustment) </td>';

                $table_detail .= '<td class="text-center font-sm ">' . $material[Material_RM_PM::$KODE_PANGGIL] . '</td>';
                $table_detail .= '<td class="text-center font-sm ">' . $material[Material_RM_PM::$JENIS] . '</td>';

                if (isset($row_detail[Worksheet_Checker_Detail::$ID_VENDOR])) {
                    $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_detail[Worksheet_Checker_Detail::$ID_VENDOR])->row_array();
                    $table_detail .= '<td class="text-center font-sm ">' . $vendor[Vendor::$NAMA] . '</td>';
                } else {
                    $table_detail .= '<td class="text-center font-sm "> - </td>';
                }
                $table_detail .= '<td class="text-right font-sm "><span>' . $this->decimal($row_detail[Worksheet_Checker_Detail::$QTY]) . ' ' . $material[Material_RM_PM::$UNIT] . '</span></td>';
                $table_detail .= "</tr>";
            }
        }
        $data['detail_product_checker'] .= $table_detail;
        $data['detail_product_checker'] .= "</tbody>";

        //WS Awal
        $table_detail = "";
        $data['detail_product_awal'] = '
					<table id="tbl_detail_product_awal" class="table table-bordered table-sm">
						<thead>
							<tr class="table-primary">
								<th scope="col" class="text-center font-sm ">Nomor</th>
								<th scope="col" class="text-center font-sm ">Material</th>
								<th scope="col" class="text-center font-sm ">Qty</th>
							</tr>
						</thead><tbody>';
        $qry = $this->ws_model->get_by_no($data[Worksheet_Checker_Header::$NOMOR_WS]);
        if ($qry[Worksheet_Awal::$TABLE_NAME]->num_rows() > 0) {
            foreach ($qry[Worksheet_Awal::$TABLE_NAME]->result_array() as $row_ws_awal) {

                $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM]);
                $material = $qry_material->row_array();

                $table_detail .= "<tr>";
                $table_detail .= '<td class="text-center font-sm ">' . $row_ws_awal[Worksheet_Awal::$NOMOR_URUT] . '</td>';
                $table_detail .= '<td class="text-center font-sm ">' . $material[Material_RM_PM::$KODE_PANGGIL] . '</td>';
                $table_detail .= '<td class="text-right font-sm "><span>' . $this->decimal($row_ws_awal[Worksheet_Awal::$QTY]) . ' ' . $material[Material_RM_PM::$UNIT] . '</span></td>';
                $table_detail .= "</tr>";
            }
        }
        $data['detail_product_awal'] .= $table_detail;
        $data['detail_product_awal'] .= "</tbody>";

        echo json_encode($data);
    }
    public function get_view_detail(){
        $ws = $this->i_p("n");
        $data['content'] = '
						<table id="tbl_detail_ws" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Nomor Urut</th>
									<th scope="col" class="text-center">Kode Panggil</th>
									<th scope="col" class="text-center">Jenis</th>
									<th scope="col" class="text-center">Vendor</th>
									<th scope="col" class="text-center">Qty Awal</th>
									<th scope="col" class="text-center">Qty Checker</th>
									';
        $data['content'] .= '</tr>
							</thead><tbody>';
        $qry = $this->ws_model->get_by_no($ws);
        $ctr = 0;
        $data_tampil = array();
        $total_awal = 0;
        $total_checker = 0;
        if ($qry[Worksheet_Awal::$TABLE_NAME]->num_rows() > 0) {
            //WS Awal & WS Checker Insert
            foreach ($qry[Worksheet_Awal::$TABLE_NAME]->result_array() as $row_ws_awal) {
                $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM]);
                $material = $qry_material->row_array();

                $data_tampil[$ctr][Worksheet_Awal::$NOMOR_URUT] = $row_ws_awal[Worksheet_Awal::$NOMOR_URUT];
                $data_tampil[$ctr][Material_RM_PM::$KODE_PANGGIL] = $material[Material_RM_PM::$KODE_PANGGIL];
                $data_tampil[$ctr][Material_RM_PM::$JENIS] = $material[Material_RM_PM::$JENIS];
                $data_tampil[$ctr][Material_RM_PM::$UNIT] = $material[Material_RM_PM::$UNIT];

                $qry_detail_checker = $this->ws_model->checker_detail_get_qty_by_nows_nourut($ws, $row_ws_awal[Worksheet_Awal::$NOMOR_URUT]);
                if ($qry_detail_checker->num_rows() > 0) {
                    foreach ($qry_detail_checker->result_array() as $row_detail) {
                        $data_tampil[$ctr][Worksheet_Awal::$NOMOR_URUT] = $row_ws_awal[Worksheet_Awal::$NOMOR_URUT];
                        $data_tampil[$ctr][Material_RM_PM::$KODE_PANGGIL] = $material[Material_RM_PM::$KODE_PANGGIL];
                        $data_tampil[$ctr][Material_RM_PM::$JENIS] = $material[Material_RM_PM::$JENIS];
                        $data_tampil[$ctr][Material_RM_PM::$UNIT] = $material[Material_RM_PM::$UNIT];

                        if (isset($row_detail[Worksheet_Checker_Detail::$ID_VENDOR])) {
                            $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_detail[Worksheet_Checker_Detail::$ID_VENDOR])->row_array();
                            $data_tampil[$ctr][Vendor::$NAMA] = $vendor[Vendor::$NAMA];
                        } else
                            $data_tampil[$ctr][Vendor::$NAMA] = "-";

                        $data_tampil[$ctr][Worksheet_Awal::$QTY] = $row_ws_awal[Worksheet_Awal::$QTY];
                        $data_tampil[$ctr][Worksheet_Checker_Detail::$S_TOT_QTY] = $row_detail[Worksheet_Checker_Detail::$S_TOT_QTY];
                        $total_checker += $data_tampil[$ctr][Worksheet_Checker_Detail::$S_TOT_QTY];
                        $ctr++;
                    }
                } else {
                    $data_tampil[$ctr][Vendor::$NAMA] = "-";
                    $data_tampil[$ctr][Worksheet_Awal::$QTY] = $row_ws_awal[Worksheet_Awal::$QTY];
                    $data_tampil[$ctr][Worksheet_Checker_Detail::$S_TOT_QTY] = 0;
                    $ctr++;
                }
                $total_awal += $row_ws_awal[Worksheet_Awal::$QTY];
            }
            //WS Checker Adjustment
            $data_adjustment = $this->ws_model->checker_detail_get_qty_by_nows_nourut($ws);
            if ($data_adjustment->num_rows() > 0) {
                foreach ($data_adjustment->result_array() as $row_adj) {
                    $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_adj[Worksheet_Checker_Detail::$ID_MATERIAL]);
                    $material = $qry_material->row_array();

                    $data_tampil[$ctr][Worksheet_Awal::$NOMOR_URUT] = "ADJ";
                    $data_tampil[$ctr][Material_RM_PM::$KODE_PANGGIL] = $material[Material_RM_PM::$KODE_PANGGIL];
                    $data_tampil[$ctr][Material_RM_PM::$JENIS] = $material[Material_RM_PM::$JENIS];
                    $data_tampil[$ctr][Material_RM_PM::$UNIT] = $material[Material_RM_PM::$UNIT];

                    if (isset($row_adj[Worksheet_Checker_Detail::$ID_VENDOR])) {
                        $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_adj[Worksheet_Checker_Detail::$ID_VENDOR])->row_array();
                        $data_tampil[$ctr][Vendor::$NAMA] = $vendor[Vendor::$NAMA];
                    } else
                        $data_tampil[$ctr][Vendor::$NAMA] = "-";

                    $data_tampil[$ctr][Worksheet_Awal::$QTY] = 0;
                    $data_tampil[$ctr][Worksheet_Checker_Detail::$S_TOT_QTY] = $row_adj[Worksheet_Checker_Detail::$S_TOT_QTY];
                    $total_checker += $data_tampil[$ctr][Worksheet_Checker_Detail::$S_TOT_QTY];
                    $ctr++;
                }
            }
            for ($i = 0; $i < $ctr; $i++) {
                $data['content'] .= "<tr>";
                    $data['content'] .= '<td class="text-center font-sm ">' . $data_tampil[$i][Worksheet_Awal::$NOMOR_URUT] . '</td>';
                    $data['content'] .= '<td class="text-center font-sm ">' . $data_tampil[$i][Material_RM_PM::$KODE_PANGGIL] . '</td>';
                    $data['content'] .= '<td class="text-center font-sm ">' . $data_tampil[$i][Material_RM_PM::$JENIS] . '</td>';
                    $data['content'] .= '<td class="text-center font-sm ">' . $data_tampil[$i][Vendor::$NAMA] . '</td>';
                    $data['content'] .= '<td class="text-right font-sm ">' . $this->decimal($data_tampil[$i][Worksheet_Awal::$QTY], 3) . ' ' . $data_tampil[$i][Material_RM_PM::$UNIT] . '</td>';
                    $data['content'] .= '<td class="text-right font-sm ">' . $this->decimal(floatval($data_tampil[$i][Worksheet_Checker_Detail::$S_TOT_QTY]), 3) . ' ' . $data_tampil[$i][Material_RM_PM::$UNIT] . '</td>';
                $data['content'] .= "</tr>";
            }

            //Total
            $data['content'] .= "<tr>";
                $data['content'] .= '<td colspan=4 class="text-right font-sm ">Total</td>';
                $data['content'] .= '<td class="text-right font-sm "> ' . $this->decimal($total_awal, 3) . ' </td>';
                $data['content'] .= '<td class="text-right font-sm "> ' . $this->decimal($total_checker, 3) . '</td>';
            $data['content'] .= "</tr>";
        }
        $data['content'] .= "</tbody></table>";

        $data['status'] = "SUKSES";

        echo json_encode($data);
    }
    public function get_view_report_selisih(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");

        $kal = "";
        $kal .= '<div class="row"style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">WS Semi</th>
									<th scope="col" class="text-center">Qty (KG)</th>
									<th scope="col" class="text-center">Selisih (KG)</th>
									<th scope="col" class="text-center">Qty (KG)</th>
									<th scope="col" class="text-center">WS FG</th>
								</tr>
							</thead><tbody>';
        $data = $this->ws_model->report_selisih_get_by_tanggal($dt, $st);

        $data_tampil = [];
        $ctr = 0;
        foreach ($data->result_array() as $row) {

            if (!$this->ws_model->is_fg($row[Worksheet_Checker_Header::$NOMOR_WS])) {
                $data_tampil[$ctr] = [];
                $data_tampil[$ctr][Worksheet_Connect::$NO_SEMI] = $row[Worksheet_Checker_Header::$NOMOR_WS];
                $data_all_ws = $this->ws_model->get_by_no($row[Worksheet_Checker_Header::$NOMOR_WS]);
                $total = 0;
                foreach ($data_all_ws[Worksheet_Checker_Detail::$TABLE_NAME]->result_array() as $row_all_ws) {
                    $total += $row_all_ws[Worksheet_Checker_Detail::$QTY];
                }
                $data_tampil[$ctr][Worksheet_Checker_Detail::$S_TOT_QTY] = $total;

                //WS FG
                $qry_connect = $this->ws_model->connect_transit_get_by(Worksheet_Connect::$NO_SEMI, $row[Worksheet_Checker_Header::$NOMOR_WS]);
                $arr_ws_fg = array();
                if ($qry_connect->num_rows() > 0) {
                    $ctr_fg = 0;
                    foreach ($qry_connect->result_array() as $row_connect) {
                        $arr_ws_fg[$ctr_fg][Worksheet_Connect::$NO_FG] = $row_connect[Worksheet_Connect::$NO_FG];
                        $arr_ws_fg[$ctr_fg][Gudang_RM_PM_Transit::$BERAT] = $row_connect[Gudang_RM_PM_Transit::$BERAT];
                        $ctr_fg++;
                    }
                }
                $data_tampil[$ctr]["arr_fg"] = $arr_ws_fg;

                $ctr++;
            }
        }

        for ($i = 0; $i < $ctr; $i++) {
            $arr_fg = $data_tampil[$i]["arr_fg"];
            $td_ws_fg = "";
            $total_ws_fg = 0;
            for ($j = 0; $j < count($arr_fg); $j++) {
                $total_ws_fg += $arr_fg[$j][Gudang_RM_PM_Transit::$BERAT];
                $td_ws_fg .= $arr_fg[$j][Worksheet_Connect::$NO_FG] . " (" . $arr_fg[$j][Gudang_RM_PM_Transit::$BERAT] . ")";
                if ($j < count($arr_fg) - 1)
                    $td_ws_fg .= ", ";
            }

            if($data_tampil[$i][Worksheet_Checker_Detail::$S_TOT_QTY]>=1){
                $selisih = floatval(strval($data_tampil[$i][Worksheet_Checker_Detail::$S_TOT_QTY])) - floatval(strval($total_ws_fg));
                if ($selisih == 0)
                    $kal .= "<tr class='green-text'>";
                else
                    $kal .= "<tr class='red-text'>";

                $kal .= "<td>" . $data_tampil[$i][Worksheet_Connect::$NO_SEMI] . "</td>";
                $kal .= "<td class='text-right'>" . $this->decimal(strval($data_tampil[$i][Worksheet_Checker_Detail::$S_TOT_QTY]),2) . "</td>";


                $kal .= "<td class='text-right'>" . $this->decimal($selisih, 2) . "</td>";
                $kal .= "<td class='text-right'>" . $this->decimal($total_ws_fg,2) . "</td>";

                $kal .= "<td>" . $td_ws_fg . "</td>";
                $kal .= "</tr>";
            }
        }
        /*untuk 3 baris ws semi 1 baris ws fg
			for ($i = 0; $i < $ctr; $i++) {
				$arr_fg = $data_tampil[$i]["arr_fg"];
				$is_cetak=true;
				for ($j = 0; $j < count($arr_fg); $j++) {
					$kal .= "<tr>";
						$kal .= "<td>" . $data_tampil[$i][Worksheet_Connect::$NO_SEMI] . "</td>";
					if($is_cetak){
						
						$kal .= "<td>" . $data_tampil[$i][Worksheet_Checker_Detail::$S_TOT_QTY] . "</td>";
					}else{
						$kal .= "<td></td>";
					}
						$kal .= "<td>" . $arr_fg[$j][Worksheet_Connect::$NO_FG] . "</td>";
						$kal .= "<td class='text-right'>" . $arr_fg[$j][Gudang_RM_PM_Transit::$BERAT] . "</td>";
					$kal .= "</tr>";
					$is_cetak=false;
				}
			}
			*/

        /*
			//untuk 3 baris jadi 1 baris tapi ngebug library datatable
			for($i=0;$i<$ctr;$i++){
				$kal .= "<tr>";
					$arr_fg = $data_tampil[$i]["arr_fg"];
					
					$row_span=count($arr_fg)+1;
					$html_row_span= "rowspan='".$row_span."'";
					$kal .= "<td style='vertical-align : middle;' $html_row_span>" .$data_tampil[$i][Worksheet_Connect::$NO_SEMI] . "</td>";
					$kal .= "<td style='vertical-align : middle;' $html_row_span class='text-right'>" . $data_tampil[$i][Worksheet_Checker_Detail::$S_TOT_QTY] . "</td>";
					if($row_span>1){
						$kal .= "</tr>";

					}					
					for($j=0;$j<count($arr_fg);$j++){
						if($row_span>1)
							$kal.="<tr>";
								$kal .= "<td>" . $arr_fg[$j][Worksheet_Connect::$NO_FG] . "</td>";
								$kal .= "<td class='text-right'>" . $arr_fg[$j][Gudang_RM_PM_Transit::$BERAT] . "</td>";
						if ($row_span > 1)	
							$kal .= "</tr>";
						
					}
				if ($row_span == 1) 
					$kal .= "</tr>";
			}*/

        $kal .= '	</tbody>
						 </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_report_connect(){
        $kal = "<br>";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");

        $data = $this->ws_model->connect_get_by_tanggal($dari_tanggal,$sampai_tanggal);

        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
            $kal .= '<h5 class="red-text text-center">*Teks Merah : Masih Berada di Gudang Transit</h5>';
            $kal .= '<table id="mastertable" class=" table table-bordered table-sm">
                        <thead>
                            <tr class="table-primary">
                                <th scope="col" class="text-center">Tgl Connect</th>
                                <th scope="col" class="text-center">WS Semi</th>
                                <th scope="col" class="text-center">WS FG</th>
                                <th scope="col" class="text-center">Material</th>
                                <th scope="col" class="text-center">Premix</th>
                                <th scope="col" class="text-center">Makeup</th>
                                <th scope="col" class="text-center">Filling</th>
                                <th scope="col" class="text-center">Berat (KG)</th>
                                ';
            $kal .= '       </tr>
                        </thead><tbody>';
            if($data->num_rows()>0){
                foreach ($data->result_array() as $row_connect) {
                    $text_color="red-text";
                    if($row_connect[Gudang_RM_PM_Transit::$IS_DONE])
                        $text_color="green-text";
                    $kal .= "<tr class='$text_color'>";
                        $kal .= "<td class='text-center'>" . $row_connect[Gudang_RM_PM_Transit::$TANGGAL_GENERATE] . "</td>";
                        $kal .= "<td class='text-center'>" . $row_connect[Worksheet_Connect::$NO_SEMI] . "</td>";
                        $kal .= "<td class='text-center'>" . $row_connect[Worksheet_Connect::$NO_FG] . "</td>";
                        $kal .= "<td class='text-center'>" . $row_connect[Material::$DESCRIPTION] . "</td>";
                        $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_connect[Worksheet_Connect::$NIK_PREMIX])->row_array();
                        $kal .= "<td class='text-info text-center' title='". $karyawan[Karyawan::$NAMA] ."'>" . $row_connect[Worksheet_Connect::$NIK_PREMIX] . "</td>";
                        $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_connect[Worksheet_Connect::$NIK_MAKEUP])->row_array();
                        $kal .= "<td class='text-info text-center' title='". $karyawan[Karyawan::$NAMA] ."'>" . $row_connect[Worksheet_Connect::$NIK_MAKEUP] . "</td>";
                        $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_connect[Worksheet_Connect::$NIK_FILLING])->row_array();
                        $kal .= "<td class='text-info text-center' title='". $karyawan[Karyawan::$NAMA] ."'>" . $row_connect[Worksheet_Connect::$NIK_FILLING] . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($row_connect[Gudang_RM_PM_Transit::$BERAT]) . "</td>";
                        
                    $kal .= "</tr>";
                }
            }
            $kal .= '</tbody>
                                </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_admin_proses(){
        $ws = $this->i_p("n");
        $tipe = $this->i_p("t");
        $data['content'] = '';
        $data["status"] = STATUS_SUCCESS;
        $qry = $this->ws_model->get_by_no($ws);
        $total_awal = 0;
        $total_checker = 0;
        $is_update=false;
        $tabel= "<div class='row'>
                    <div class='col-sm-3'></div>
                    <div class='col-sm-6'>
                        <table class='table table-bordered table-sm'>
                        <thead>
                            <tr class='table-primary text-center fw-bold'>
                                <th class='fw-bold'>Vendor</th>
                                <th class='fw-bold'>Qty (KG)</th>
                            </tr>
                        </thead><tbody>
                    ";
        if ($qry[Worksheet_Awal::$TABLE_NAME]->num_rows() > 0) {
            //WS Awal & WS Checker Insert
            foreach ($qry[Worksheet_Awal::$TABLE_NAME]->result_array() as $row_ws_awal) {
                $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM]);
                $material = $qry_material->row_array();
                $qry_detail_checker = $this->ws_model->checker_detail_get_qty_by_nows_nourut($ws, $row_ws_awal[Worksheet_Awal::$NOMOR_URUT]);
                if ($qry_detail_checker->num_rows() > 0) {
                    foreach ($qry_detail_checker->result_array() as $row_detail) {
                        if($row_detail[Worksheet_Checker_Detail::$ID_VENDOR]!=null){
                            $vendor=$this->vendor_model->get(Vendor::$TABLE_NAME,Vendor::$ID,$row_detail[Worksheet_Checker_Detail::$ID_VENDOR])->row_array()[Vendor::$NAMA];
                            $tabel .= "<tr>
                                        <td>". $vendor ."</td>
                                        <td class='text-right'>". $row_detail[Worksheet_Checker_Detail::$S_TOT_QTY] ."</td>
                                        </tr>";
                        }
                        $total_checker += $row_detail[Worksheet_Checker_Detail::$S_TOT_QTY];
                    }
                }
                $total_awal += $row_ws_awal[Worksheet_Awal::$QTY];
            }

            //WS Checker Adjustment
            $data_adjustment = $this->ws_model->checker_detail_get_qty_by_nows_nourut($ws);
            if ($data_adjustment->num_rows() > 0) {
                foreach ($data_adjustment->result_array() as $row_adj) {
                    if ($row_adj[Worksheet_Checker_Detail::$ID_VENDOR] != null) {
                        $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_adj[Worksheet_Checker_Detail::$ID_VENDOR])->row_array()[Vendor::$NAMA];
                        $tabel .= "<tr>
                                        <td>" . $vendor . "</td>
                                        <td class='text-right'>" . $row_adj[Worksheet_Checker_Detail::$S_TOT_QTY] . "</td>
                                    </tr>";
                    }
                    $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_adj[Worksheet_Checker_Detail::$ID_MATERIAL]);
                    $material = $qry_material->row_array();
                    $total_checker += $row_adj[Worksheet_Checker_Detail::$S_TOT_QTY];
                }
            }
            $tabel.= "</tbody>
                    </table></div>
                    <div class='col-sm-3'></div>
                </div>";

            //Total
            if ($this->equals($tipe, Worksheet::$S_SEMI) && !$this->ws_model->is_semi_single($ws)) {
                //Pengurangan Total Checker dengan WS Connect
                $query = $this->ws_model->get(Worksheet_Connect::$TABLE_NAME, Worksheet_Connect::$NO_SEMI, $ws);
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {
                        $query_transit = $this->rmpm_model->get(Gudang_RM_PM_Transit::$TABLE_NAME, Gudang_RM_PM_Transit::$ID_CONNECT_WS, $row[Worksheet_Connect::$ID], null, null, false);
                        if ($query_transit->num_rows() > 0) {
                            foreach ($query_transit->result_array() as $row_transit) {
                                $total_checker -= $row_transit[Gudang_RM_PM_Transit::$BERAT];
                            }
                        }
                    }
                    $is_update=true;
                }

                
                $data['total_checker'] = $total_checker;
                $data['content'] .= '<h3 class=" text-center fw-bold"> Vendor Latex</h3>';
                $data['content'] .= $tabel;

                $data['content'] .= '<h3 class=" text-center green-text fw-bold"> Total: ' . $this->decimal($total_checker, 3) . ' KG</h3>';
                $data['content'] .= "<div class='row'>
                                            <div class='col-sm-12'>
                                                <div class='row'>
                                                    <div class='col-sm-2  text-right'></div>
                                                    <div class='col-sm-2  text-right'>No. WS FG</div>
                                                    <div class='col-sm-1  text-left '>:</div>
                                                    <div class='col-sm-3 text-left'><input class='' style='width:100%' type='text' id='txt_nomor_ws_fg' /></div>
                                                    <div class='col-sm-2  text-right'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='row'>
                                            <div class='col-sm-1'></div>
                                            <div class='col-sm-10 text-center'>
                                                <button type='button' id='btn_generate_fg' class='center text-center btn btn-outline-primary' onclick='gen_detail_ws(" . '"FG"' . ")' data-mdb-ripple-color='dark'>
                                                GENERATE
                                                </button>
                                            </div>
                                            <div class='col-sm-1'></div>
                                        </div><br>
                                        ";
                if ($total_checker == 0)
                    //$data["status"] = MESSAGE[Worksheet_Connect::$MESSAGE_FAILED_INSERT_ALREADY_CONNECTED];
                if (!$this->equals($material[Material_RM_PM::$UNIT], Material_RM_PM::$S_UNIT_KG)) {
                    $data["status"] = MESSAGE[Worksheet::$MESSAGE_FAILED_SEARCH_NOT_SEMI];
                }
                
            } else if ($this->equals($tipe, Worksheet::$S_FG) || $this->ws_model->is_semi_single($ws)) {
                if($this->ws_model->is_semi_single($ws)){
                    $tipe = Worksheet::$S_SEMI;
                    $semi = $ws;
                    $data['content'] .= "<h2 class='text-center fw-bold'>WS Single </h2>";
                    $data['content'] .= "<h6 class='text-center fw-bold red-text'>Hanya digunakan untuk Input CM & Waktu</h6><br>";

                }else{
                    $semi = $this->i_p("ns");
                }
                $data['content'] .= "<h4 class='text-center fw-bold'>Colour Matcher</h4>";

                $data['content'] .= '<h5 class=" text-center red-text fw-bold"> 
							Jika WS hanya ada 1 Checker Colour Matcher, boleh hanya isikan premix<br>
							Jika WS ada 2 Checker Colour Matcher, ketiga checker harus diisikan<br>
										</h5>';


                //Waktu Proses Produksi
                //Cari dulu WS Semi sudah ada atau tidak, jika ada semua tanggal isi dari WS sebelumnya
                $ws_connect = $this->ws_model->get(Worksheet_Connect::$TABLE_NAME, Worksheet_Connect::$NO_FG, $ws, null, null, false);
                //NIK Colour Matcher
                $qry = $this->karyawan_model->get_colour_matcher();
                $temp = "";
                $option = "";
                if ($ws_connect->num_rows() > 0) {
                    $is_update=true;
                    $ws_connect = $ws_connect->row_array();
                    foreach ($qry->result_array() as $row_nik) {
                        $selected = "";
                        if($ws_connect[Worksheet_Connect::$NIK_PREMIX]==$row_nik[Karyawan::$ID]){
                            $selected = "selected";

                        }
                        $option .= "<option $selected value=" . $row_nik[Karyawan::$ID] . ">" . $row_nik[Karyawan::$ID] . " - " . $row_nik[Karyawan::$NAMA] . "</option>";
                    }
                    $select = "<div class='row'>";
                    $select .= "<div class='col-sm-3'></div>";
                    $select .= "<div class='text-right  col-sm-2'>Premix:</div>";
                    $temp = "<select id='txt_premix' class='form-control  font-sm'>$option</select>";
                    $select .= "<div class=' col-sm-4'>$temp</div>";
                    $select .= "</div>";

                    foreach ($qry->result_array() as $row_nik) {
                        $selected = "";
                        if ($ws_connect[Worksheet_Connect::$NIK_MAKEUP] == $row_nik[Karyawan::$ID]) {
                            $selected = "selected";
                        }
                        $option .= "<option $selected value=" . $row_nik[Karyawan::$ID] . ">" . $row_nik[Karyawan::$ID] . " - " . $row_nik[Karyawan::$NAMA] . "</option>";
                    }
                    $select .= "<div class='row'>";
                    $select .= "<div class='col-sm-3'></div>";
                    $select .= "<div class='text-right  col-sm-2'>Makeup:</div>";
                    $temp = "<select id='txt_makeup' class='form-control  font-sm'>$option</select>";
                    $select .= "<div class=' col-sm-4'>$temp</div>";
                    $select .= "</div>";

                    foreach ($qry->result_array() as $row_nik) {
                        $selected = "";
                        if ($ws_connect[Worksheet_Connect::$NIK_FILLING] == $row_nik[Karyawan::$ID]) {
                            $selected = "selected";
                        }
                        $option .= "<option $selected value=" . $row_nik[Karyawan::$ID] . ">" . $row_nik[Karyawan::$ID] . " - " . $row_nik[Karyawan::$NAMA] . "</option>";
                    }
                    $select .= "<div class='row'>";
                    $select .= "<div class='col-sm-3'></div>";
                    $select .= "<div class='text-right  col-sm-2'>Filling:</div>";
                    $temp = "<select id='txt_filling' class='form-control  font-sm'>$option</select>";
                    $select .= "<div class=' col-sm-4'>$temp</div>";
                    $select .= "</div><br>";

                    $data['content'] .= $select;
                }else{
                    $option = "<option value=-1 selected></option>";

                    foreach ($qry->result_array() as $row_nik) {
                        $option .= "<option value=" . $row_nik[Karyawan::$ID] . ">" . $row_nik[Karyawan::$ID] . " - " . $row_nik[Karyawan::$NAMA] . "</option>";
                    }
                    $select = "<div class='row'>";
                    $select .= "<div class='col-sm-3'></div>";
                    $select .= "<div class='text-right  col-sm-2'>Premix:</div>";
                    $temp = "<select id='txt_premix' class='form-control  font-sm'>$option</select>";
                    $select .= "<div class=' col-sm-4'>$temp</div>";
                    $select .= "</div>";

                    $select .= "<div class='row'>";
                    $select .= "<div class='col-sm-3'></div>";
                    $select .= "<div class='text-right  col-sm-2'>Makeup:</div>";
                    $temp = "<select id='txt_makeup' class='form-control  font-sm'>$option</select>";
                    $select .= "<div class=' col-sm-4'>$temp</div>";
                    $select .= "</div>";

                    $select .= "<div class='row'>";
                    $select .= "<div class='col-sm-3'></div>";
                    $select .= "<div class='text-right  col-sm-2'>Filling:</div>";
                    $temp = "<select id='txt_filling' class='form-control  font-sm'>$option</select>";
                    $select .= "<div class=' col-sm-4'>$temp</div>";
                    $select .= "</div><br>";

                    $data['content'] .= $select;
                }
               
                $arr_waktu = [];
                $arr_waktu[0] = []; //Persiapan
                $arr_waktu[1] = []; //Premix
                $arr_waktu[2] = []; //Makeup
                $arr_waktu[3] = []; //CM
                $arr_waktu[4] = []; //QC
                $ws_connect = $this->ws_model->get(Worksheet_Connect::$TABLE_NAME, Worksheet_Connect::$NO_SEMI, $semi, null, null, false);

                if($ws_connect->num_rows()>0){
                    $ws_connect = $ws_connect->row_array();
                    
                    $arr_waktu[0][Worksheet_Connect::$PERSIAPAN_FROM] = $ws_connect[Worksheet_Connect::$PERSIAPAN_FROM];
                    $arr_waktu[0][Worksheet_Connect::$PERSIAPAN_TO] = $ws_connect[Worksheet_Connect::$PERSIAPAN_TO];

                    $arr_waktu[1][Worksheet_Connect::$PREMIX_FROM] = $ws_connect[Worksheet_Connect::$PREMIX_FROM];
                    $arr_waktu[1][Worksheet_Connect::$PREMIX_TO] = $ws_connect[Worksheet_Connect::$PREMIX_TO];

                    $arr_waktu[2][Worksheet_Connect::$MAKEUP_FROM] = $ws_connect[Worksheet_Connect::$MAKEUP_FROM];
                    $arr_waktu[2][Worksheet_Connect::$MAKEUP_TO] = $ws_connect[Worksheet_Connect::$MAKEUP_TO];

                    $arr_waktu[3][Worksheet_Connect::$CM_FROM] = $ws_connect[Worksheet_Connect::$CM_FROM];
                    $arr_waktu[3][Worksheet_Connect::$CM_TO] = $ws_connect[Worksheet_Connect::$CM_TO];

                    $arr_waktu[4][Worksheet_Connect::$QC_FROM] = $ws_connect[Worksheet_Connect::$QC_FROM];
                    $arr_waktu[4][Worksheet_Connect::$QC_TO] = $ws_connect[Worksheet_Connect::$QC_TO];
                }else{
                    $arr_waktu[0][Worksheet_Connect::$PERSIAPAN_FROM] = "";
                    $arr_waktu[0][Worksheet_Connect::$PERSIAPAN_TO] = "";

                    $arr_waktu[1][Worksheet_Connect::$PREMIX_FROM] = "";
                    $arr_waktu[1][Worksheet_Connect::$PREMIX_TO] = "";

                    $arr_waktu[2][Worksheet_Connect::$MAKEUP_FROM] = "";
                    $arr_waktu[2][Worksheet_Connect::$MAKEUP_TO] = "";

                    $arr_waktu[3][Worksheet_Connect::$CM_FROM] = "";
                    $arr_waktu[3][Worksheet_Connect::$CM_TO] = "";

                    $arr_waktu[4][Worksheet_Connect::$QC_FROM] = "";
                    $arr_waktu[4][Worksheet_Connect::$QC_TO] = "";
                }
                
                $data['content'] .= "<h4 class='text-center fw-bold'>Waktu Proses Produksi</h4>";
                
                $temp_waktu = "";

                //Persiapan
                $temp_waktu .= "<h6 class='text-center fw-bold'>Persiapan</h6>";
                $temp_waktu .= "<div class='row'>";
                    $temp_waktu .= "<div class='col-sm-2'></div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Dari:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_pf" value="' . $arr_waktu[0][Worksheet_Connect::$PERSIAPAN_FROM] . '">' . "</div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Sampai:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>". '<input type="datetime-local" id="txt_pt" value="' . $arr_waktu[0][Worksheet_Connect::$PERSIAPAN_TO] . '">' ."</div>";
                $temp_waktu .= "</div>";

                //Premix
                $temp_waktu .= "<h6 class='text-center fw-bold'>Premix</h6>";
                $temp_waktu .= "<div class='row'>";
                    $temp_waktu .= "<div class='col-sm-2'></div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Dari:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>". '<input type="datetime-local" id="txt_prf" value="' . $arr_waktu[1][Worksheet_Connect::$PREMIX_FROM] . '">' ."</div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Sampai:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>". '<input type="datetime-local" id="txt_prt" value="' . $arr_waktu[1][Worksheet_Connect::$PREMIX_TO] . '">' ."</div>";
                $temp_waktu .= "</div>";

                //Makeup
                $temp_waktu .= "<h6 class='text-center fw-bold'>Makeup</h6>";
                $temp_waktu .= "<div class='row'>";
                    $temp_waktu .= "<div class='col-sm-2'></div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Dari:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_mf" value="' . $arr_waktu[2][Worksheet_Connect::$MAKEUP_FROM] . '">' . "</div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Sampai:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_mt" value="' . $arr_waktu[2][Worksheet_Connect::$MAKEUP_TO] . '">' . "</div>";
                $temp_waktu .= "</div>";

                //CM
                $temp_waktu .= "<h6 class='text-center fw-bold'>Colour Matcher</h6>";
                $temp_waktu .= "<div class='row'>";
                    $temp_waktu .= "<div class='col-sm-2'></div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Dari:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_cf" value="' . $arr_waktu[3][Worksheet_Connect::$CM_FROM] . '">' . "</div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Sampai:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_ct" value="' . $arr_waktu[3][Worksheet_Connect::$CM_TO] . '">' . "</div>";
                $temp_waktu .= "</div>";

                //QC
                $temp_waktu .= "<h6 class='text-center fw-bold'>Quality Control</h6>";
                $temp_waktu .= "<div class='row'>";
                    $temp_waktu .= "<div class='col-sm-2'></div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Dari:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_qf" value="' . $arr_waktu[4][Worksheet_Connect::$QC_FROM] . '">' . "</div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Sampai:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_qt" value="' . $arr_waktu[4][Worksheet_Connect::$QC_TO] . '">' . "</div>";
                $temp_waktu .= "</div>";

                //Filling
                $temp_waktu .= "<h6 class='text-center fw-bold'>Filling</h6>";
                $temp_waktu .= "<div class='row'>";
                    $temp_waktu .= "<div class='col-sm-2'></div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Dari:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_ff">' . "</div>";
                    $temp_waktu .= "<div class='text-right  col-sm-1'>Sampai:</div>";
                    $temp_waktu .= "<div class=' col-sm-3'>" . '<input type="datetime-local" id="txt_ft">' . "</div>";
                $temp_waktu .= "</div>";

                $data['content'] .= $temp_waktu . "<br>";
                if ($this->equals($tipe, Worksheet::$S_FG)) {
                    $data['product_code'] = $this->ws_model->get_product_code_by_nows($ws);
                    $data['content'] .= '<h3 class=" text-center green-text fw-bold"> Product Code: ' . $data['product_code'] . '</h3>';
                    $data['content'] .= '<h5 class=" text-center red-text fw-bold"> Pastikan Product Code sama dengan yang tertera pada WS</h5>';
                    
                    /*
					<button type="button" class="btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
						Tambah Kemasan
					</button>
					
					$data['content'] .= "
										<div class='row'>
											<div class='col-sm-12'>
												<table class='tabel_detail_product' style='visibility:hidden'>
													<tr>
														<td class='text-center font-sm '>Nomor</td>
														<td class='text-center font-sm '>Kemasan</td>
														<td class='text-center font-sm '>Tin</td>
														<td class='text-center font-sm '>Berat</td>
														<td class='text-center font-sm '>Aksi</td>
													</tr>		
												</table>
											</div>
										</div>
										";
					*/
                    if(!$is_update){
                        $data['content'] .= "
                                                <div class='row'>
                                                    <div class='col-sm-1'></div>
                                                    <div class='col-sm-10 text-center'>
                                                        <table class='tabel_detail_product'>
                                                            <tr>
                                                                <td class='text-center font-sm '>Kemasan</td>
                                                                <td class='text-center font-sm '>Tin</td>
                                                                <td class='text-center font-sm '>SG</td>
                                                                <td class='text-center font-sm '>@ KG</td>
                                                                <td class='text-center font-sm '>Berat (KG)</td>
                                                            </tr>		
                                                ";


                        $arr_ws_awal = $this->ws_model->get(Worksheet_Awal::$TABLE_NAME, Worksheet_Awal::$NOMOR_WS, $ws, null, null, false);
                        if ($arr_ws_awal->num_rows() > 0) {
                            //Ambil Kemasan dari PM
                            $material_rm_pm_ws = null;
                            foreach ($arr_ws_awal->result_array() as $row_ws_awal) {
                                if ($row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_FG] == NULL) {
                                    $material_rm_pm_ws = $this->ws_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM])->row_array();
                                    if ($material_rm_pm_ws[Material_RM_PM::$KEMASAN] != null) {
                                        break;
                                    }
                                }
                            }
                            //Ambil Total Tin dari Insert WS dan Adjustment WS
                            $total_tin = $this->ws_model->get_tin_kemasan($ws, $material_rm_pm_ws[Material_RM_PM::$ID]);

                            $data['content'] .= "<tr>";
                                $data['content'] .= '<td style="width:20%" class="text-center font-sm ">
                                                                        <input disabled style="width:20%" class="text-center" id="txt_kemasan" value="' . $material_rm_pm_ws[Material_RM_PM::$KEMASAN] . '"></input>
                                                                    </td>';
                                $data['content'] .= '<td class="text-center font-sm ">
                                                                        <input class="text-right" id="txt_tin" type="number" disabled value="' . $total_tin . '"></input>
                                                                    </td>';
                                $disabled = "disabled";
                                $sg = 1;
                                if (in_array($material_rm_pm_ws[Material_RM_PM::$KODE_MATERIAL], Material_RM_PM::$S_ARR_INPUT_SG)) {
                                    $disabled = "";
                                    $sg = 1.40;
                                }


                                $data['content'] .= '<td class="text-center font-sm ">
                                                                        <input class="text-right" id="txt_sg" type="number" ' . $disabled . ' onchange="update_berat(this.value)" onkeyup="update_berat(this.value)" step=0.01 min=1.40 max=1.53 value=1.40></input>
                                                                    </td>';
                                if(strpos($data['product_code'],'LINEN')!==false){
                                    $data['content'] .= '<td class="text-center font-sm ">
                                                                        <input class="text-right" id="txt_kg" type="number" disabled value=22></input>
                                                                    </td>';
                                }else{
                                    $data['content'] .= '<td class="text-center font-sm ">
                                                                        <input class="text-right" id="txt_kg" type="number" disabled value=' . $material_rm_pm_ws[Material_RM_PM::$KG] . '></input>
                                                                    </td>';
                                }
                                
                                
                                if(strpos($data['product_code'],'LINEN')!==false){
                                    $data['content'] .= '<td class="text-center font-sm ">
                                                                    <input class="text-right" id="txt_berat" type="number" disabled value=' . ($sg * 22 * $total_tin) . '></input>
                                                                </td>';
                                }else{
                                    $data['content'] .= '<td class="text-center font-sm ">
                                                                    <input class="text-right" id="txt_berat" type="number" disabled value=' . ($sg * $material_rm_pm_ws[Material_RM_PM::$KG] * $total_tin) . '></input>
                                                                </td>';
                                }
                            $data['content'] .= "</tr>";
                        }
                        $data['content'] .= "
                                                        </table>
                                                    </div>
                                                    <div class='col-sm-1'></div>
                                                </div>	
                                                ";
                        if ($total_checker == 0)
                            $data["status"] = MESSAGE[Worksheet::$MESSAGE_FAILED_SEARCH_QTY_CHECKER_STILL_ZERO];
                        if (!$this->equals($material[Material_RM_PM::$UNIT], Material_RM_PM::$S_UNIT_PCS)) {
                            $data["status"] = MESSAGE[Worksheet::$MESSAGE_FAILED_SEARCH_NOT_FG];
                        }
                    }else{
                    $data['content'] .= '<h5 class=" text-center red-text fw-bold"> Update CM & Waktu Produksi (Tidak bisa mengupdate SG karena sudah diinput sebelumnya)</h5>';

                    }
                }
                $data['content'] .= '
										<div class="text-center" style="margin-bottom:2%">
											
											<button type="button" class="btn btn-outline-primary" onclick="gen_ws_admin_proses()" data-mdb-ripple-color="dark">
												Generate
											</button>
										</div>
										';
            }
        } else {
            $data["status"] = MESSAGE[Worksheet::$MESSAGE_FAILED_SEARCH_NOT_FOUND];
        }
        echo json_encode($data);
    }
    public function get_view_admin_proses_history(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $nomor_ws = $this->i_p("n");
        $kal = "";
        $data = $this->ws_model->admin_proses_get_history($tanggal_dari, $tanggal_sampai, $nomor_ws);

        $arr_header = [
            "Tanggal", "WS Semi", "WS FG", "Persiapan", "Premix", "Makeup", "Colour Matching", "QC", "Filling"
        ];

        $kal .= '<div class="row" style="margin-left:1%;margin-right:0%">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center align-middle'>" . $row[Worksheet_Connect_History::$TANGGAL] . "</td>";
                $kal .= "<td class='text-center align-middle'>" . $row[Worksheet_Connect_History::$NO_SEMI] . "</td>";
                $kal .= "<td class='text-center align-middle'>" . $row[Worksheet_Connect_History::$NO_FG] . "</td>";

                //Persiapan
                $datetime1 = strtotime($row[Worksheet_Connect_History::$PERSIAPAN_FROM]);
                $datetime2 = strtotime($row[Worksheet_Connect_History::$PERSIAPAN_TO]);

                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                $minutes = $secs / 60;
                $kal .= "<td class='text-center'>[" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$PERSIAPAN_FROM])) . "] <br> [" . date("Y-m-d H:i", strtotime($row[Worksheet_Connect_History::$PERSIAPAN_TO])) . "]<br>$minutes menit</td>";
                $html = "NIK: - ";
                if ($row[Worksheet_Connect_History::$NIK_PREMIX] != null) {
                    $premix = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[Worksheet_Connect_History::$NIK_PREMIX])->row_array();
                    $html = "NIK: <span class='blue-text' title='" . $premix[Karyawan::$NAMA] . "'>" . $row[Worksheet_Connect_History::$NIK_PREMIX] . "</span><br>";
                }
                //Premix
                $datetime1 = strtotime($row[Worksheet_Connect_History::$PREMIX_FROM]);
                $datetime2 = strtotime($row[Worksheet_Connect_History::$PREMIX_TO]);

                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                $minutes = $secs / 60;

                $kal .= "<td class='text-center'>" . $html . "[" . date("Y-m-d H:i", strtotime($row[Worksheet_Connect_History::$PREMIX_FROM])) . "] <br> [" . date("Y-m-d H:i", strtotime($row[Worksheet_Connect_History::$PREMIX_TO])) . "]<br>$minutes Menit</td>";
                $html = "NIK: - ";
                if ($row[Worksheet_Connect_History::$NIK_MAKEUP] != null) {
                    $makeup = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[Worksheet_Connect_History::$NIK_MAKEUP])->row_array();
                    $html = "NIK: <span class='blue-text' title='" . $makeup[Karyawan::$NAMA] . "'>" . $row[Worksheet_Connect_History::$NIK_MAKEUP] . "</span><br>";
                }
                
                //Makeup
                $datetime1 = strtotime($row[Worksheet_Connect_History::$MAKEUP_FROM]);
                $datetime2 = strtotime($row[Worksheet_Connect_History::$MAKEUP_TO]);

                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                $minutes = $secs / 60;
                $kal .= "<td class='text-center'>". $html ."[" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$MAKEUP_FROM])) . "] <br> [" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$MAKEUP_TO])) . "]<br>$minutes Menit</td>";

                //CM
                $datetime1 = strtotime($row[Worksheet_Connect_History::$CM_FROM]);
                $datetime2 = strtotime($row[Worksheet_Connect_History::$CM_TO]);

                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                $minutes = $secs / 60;
                $kal .= "<td class='text-center'>". $html ."[" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$CM_FROM])) . "] <br> [" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$CM_TO])) . "]<br>$minutes Minutes</td>";

                //QC
                $datetime1 = strtotime($row[Worksheet_Connect_History::$QC_FROM]);
                $datetime2 = strtotime($row[Worksheet_Connect_History::$QC_TO]);

                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                $minutes = $secs / 60;
                $kal .= "<td class='text-center'>". $html ."[" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$QC_FROM])) . "] <br> [" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$QC_TO])) . "]<br> $minutes Menit</td>";
                
                $html = "NIK: - ";
                if ($row[Worksheet_Connect_History::$NIK_FILLING] != null) {
                    $filling = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[Worksheet_Connect_History::$NIK_FILLING])->row_array();
                    $html = "NIK: <span class='blue-text' title='" . $filling[Karyawan::$NAMA] . "'>" . $row[Worksheet_Connect_History::$NIK_FILLING] . "</span><br>";
                }

                //Filling
                $datetime1 = strtotime($row[Worksheet_Connect_History::$FILLING_FROM]);
                $datetime2 = strtotime($row[Worksheet_Connect_History::$FILLING_TO]);

                $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                $minutes = $secs / 60;
                $kal .= "<td class='text-center'>". $html ."[" . date("Y-m-d H:i",strtotime($row[Worksheet_Connect_History::$FILLING_FROM])) . "] <br> [" . date("Y-m-d H:i", strtotime($row[Worksheet_Connect_History::$FILLING_TO])) . "]<br>$minutes Menit</td>";
                
                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_fg_gr(){
        $no_ws = $this->i_p("n");
        $data = [];
        $data["status"] = STATUS_SUCCESS;
        $kal = '
				<thead>
					<tr>
						<td class="text-center font-sm ">Kode Barang</td>
						<td class="text-center font-sm ">Warna</td>
						<td class="text-center font-sm ">Kemasan</td>
						<td class="text-center font-sm ">Tin</td>
						<td class="text-center font-sm ">Dus</td>
						<td class="text-center font-sm ">Barang</td>
					</tr>
				</thead><tbody>';
        $query = $this->ws_model->get(Worksheet_Connect::$TABLE_NAME, Worksheet_Connect::$NO_FG, $no_ws);
        if ($query->num_rows() > 0) {
            $id_connect_ws = $query->row_array()[Worksheet_Connect::$ID];
            $this->db->where(Gudang_RM_PM_Transit::$ID_CONNECT_WS, $id_connect_ws);
            $this->db->where(Gudang_RM_PM_Transit::$IS_DONE,0);
            $query_transit = $this->db->get(Gudang_RM_PM_Transit::$TABLE_NAME);
            if ($query_transit->num_rows() > 0) {
                foreach ($query_transit->result_array() as $row) {
                    $material = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $row[Gudang_RM_PM_Transit::$ID_MATERIAL_FG])->row_array();
                    $dus = 0;
                    $tin = $row[Gudang_RM_PM_Transit::$QTY];
                    if ($material[Material::$BOX] > 1) {
                        $dus = intdiv($tin, $material[Material::$BOX]);
                        $tin %= $material[Material::$BOX];
                    }

                    $kal .= '
						<tr>
							<td style="width:1%" class="text-center font-sm ">' . $material[Material::$KODE_BARANG] . '</td>
							<td style="width:10%" class="text-center font-sm "> 
								' . $material[Material::$WARNA] . '
							</td>

							<td style="width:5%" class="text-center font-sm "> 
								' . $material[Material::$KEMASAN] . '
							</td>
						
							<td style="width:5%" class="text-right font-sm "> 
								' . $tin . '
							</td>
							
							<td style="width:5%" class="text-right font-sm "> 
								' . $dus . '
							</td>

							<td class="font-sm  text-center"> 
								' . $material[Material::$DESCRIPTION] . '
							</td>
						</tr>';
                }
                $kal .= "</tbody>";
            } else {
                $data["status"] = MESSAGE[Worksheet_Connect::$MESSAGE_FAILED_SEARCH_WS_FG_ALREADY_INPUT];
            }
        } else {
            $data["status"] = MESSAGE[Worksheet_Connect::$MESSAGE_FAILED_SEARCH_WS_FG_NOT_FOUND];
        }
        $data["detail_product"] = $kal;
        echo json_encode($data);
    }
    public function get_view_sisa(){
        $kal = "";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");
        $nomor_ws = $this->i_p("n");

        $data = $this->ws_model->get_by($dari_tanggal,$sampai_tanggal,$nomor_ws);

        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
            $kal .= '<table id="mastertable" class=" table table-bordered table-sm">
                                <thead>
                                    <tr class="table-primary">
                                        <th scope="col" class="text-center">Nomor WS</th>
                                        <th scope="col" class="text-center">NIK</th>
                                        ';
            $kal .= '			</tr>
                                </thead><tbody>';
            if($data->num_rows()>0){
                foreach ($data->result_array() as $row_ws) {
                    $kal .= "<tr>";
                        $kal .= "<td class='text-center'>" . $row_ws[Worksheet::$ID] . "</td>";
                        $data_header=$this->ws_model->get(Worksheet_Checker_Header::$TABLE_NAME,Worksheet_Checker_Header::$NOMOR_WS,$row_ws[Worksheet::$ID]);
                        
                        if($data_header->num_rows()>0){
                            $kal.= "<td class='text-center'>";
                            $arr_nik=[];
                            foreach($data_header->result_array() as $row_header){
                                if(!in_array($row_header[Worksheet_Checker_Header::$NIK], $arr_nik))
                                    array_push($arr_nik,$row_header[Worksheet_Checker_Header::$NIK]);
                            }
                            for($i=0;$i<count($arr_nik);$i++){
                                $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $arr_nik[$i])->row_array();
                                $kal .= "<span class='text-info' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $arr_nik[$i] . "</span> "; 
                            }
                            $kal.="</td>";
                        }else{
                            $kal .= "<td class='text-center'> - </td>";
                        }
                    $kal .= "</tr>";
                }
            }
            $kal .= '</tbody>
                                </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_tutup(){
        $kal = "";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");
        $data = $this->ws_model->get_utuh($dari_tanggal,$sampai_tanggal);

        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-3"></div>';
            $kal .= '<div class="col-sm-6">';
                $kal .= '<table id="mastertable" class=" table table-bordered table-sm">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col" class="text-center">Tanggal</th>
                                    <th scope="col" class="text-center">Nomor WS</th>
                                    <th scope="col" class="text-center">Status</th>
                                    <th scope="col" class="text-center">Aksi</th>
                                </tr>
                            </thead><tbody>';
                if ($data->num_rows() > 0) {
                    foreach ($data->result_array() as $row_ws) {
                        $kal .= "<tr>";
                            $kal .= "<td class='text-center'>" . $row_ws[Worksheet::$TANGGAL] . "</td>";
                            $kal .= "<td class='text-center'>" . $row_ws[Worksheet::$ID] . "</td>";
                            if($row_ws[Worksheet::$IS_DELETED]==0){
                                $kal .= "<td class='text-center green-text'>AKTIF</td>";
                                $kal .= "<td class='text-center'><button type='button' class='center btn btn-outline-danger' data-mdb-ripple-color='dark' onclick='update_status(".$row_ws[Worksheet::$ID].",".($row_ws[Worksheet::$IS_DELETED]*-1+1).")'>TUTUP</button></td>";
                            }else{
                                $kal .= "<td class='text-center red-text'>NON-AKTIF</td>";
                                $kal .= "<td class='text-center'><button type='button' class='center btn btn-outline-success' data-mdb-ripple-color='dark' onclick='update_status(".$row_ws[Worksheet::$ID].",".($row_ws[Worksheet::$IS_DELETED]*-1+1).")'>KEMBALI</button></td>";
                            }
                        $kal .= "</tr>";
                    }
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_informasi(){
        $kal = "";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");
        $kategori = $this->i_p("ik");
        $nomor_ws_from = $this->i_p("nf");
        $nomor_ws_to = $this->i_p("nt");
        $hirarki = $this->i_p("h");
        $show_total = $this->i_p("s");
        if ($this->equals($show_total, "y"))
            $show_total = true;
        else
            $show_total = false;

        if ($kategori == 1) { //Header
            $arr_header = [
                "WS", "Material", "FG Code Lama","FG Code Baru", "Tanggal Create",  "Qty Awal", "Qty Produksi", "Qty Terima", "Unit", "Tanggal Finish", "Status" 
            ];
            $data = $this->ws_model->info_header_get_informasi($nomor_ws_from,$nomor_ws_to, $hirarki, $dari_tanggal, $sampai_tanggal);
            $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
            if ($data->num_rows() > 0) {
                $arr_ws_fg = [];
                foreach ($data->result_array() as $row) {
                    $unit = "";
                    $is_fg = false;
                    $tanggal_finish = "-";
                    $data_adjustment = $this->ws_model->checker_detail_get_qty_by_nows_nourut($row[Worksheet::$ID]);
                    $temp_desc = [];
                    if (in_array($row[Worksheet::$ID], $arr_ws_fg)) {
                        $is_fg = true;
                    } else {
                        $pv = $this->pv_model->get(BOM_Production_Version::$TABLE_NAME, BOM_Production_Version::$ID, $row[Worksheet::$ID_PV], null, null, false)->row_array();
                        $material = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $pv[BOM_Production_Version::$PRODUCT_CODE]);

                        if ($material->num_rows() > 0) {
                            $temp_desc[Material::$ID] = $material->row_array()[Material::$ID];
                            $temp_desc[Material::$ID_NEW] = $material->row_array()[Material::$ID_NEW];
                            $temp_desc[Material::$DESCRIPTION] = $material->row_array()[Material::$PRODUCT_CODE];
                        }else {
                            $temp_desc[Material::$ID] = "-";
                            $temp_desc[Material::$ID_NEW] = "-";
                            $temp_desc[Material::$DESCRIPTION] = $this->bom_model->get_by_id_bom_pv($row[Worksheet::$ID_PV])->row_array()[BOM::$ID];

                        }
                    }
                    if (!$is_fg) {
                        $kal .= "<tr>";
                        $kal .= "<td class='text-center'>" . $row[Worksheet::$ID] . "</td>";
                        $kal .= "<td class='text-center'> " . $temp_desc[Material::$DESCRIPTION] . "</td>";
                        $kal .= "<td class='text-center'> " . $temp_desc[Material::$ID] . "</td>";
                        $kal .= "<td class='text-center'> " . $temp_desc[Material::$ID_NEW] . "</td>";
                        $kal .= "<td class='text-center'>" . $row[Worksheet::$TANGGAL] . "</td>";

                        

                        if ($this->ws_model->is_fg($row[Worksheet::$ID])) { //FG
                            $pv = $this->pv_model->get(BOM_Production_Version::$TABLE_NAME, BOM_Production_Version::$ID, $row[Worksheet::$ID_PV], null, null, false)->row_array();
                            $material = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $pv[BOM_Production_Version::$PRODUCT_CODE])->row_array();
                            if ($material[Material::$NET] != 0)
                                $total_awal = $row[Worksheet_Awal::$QTY] / ($material[Material::$NET]);
                            else
                                $total_awal = $row[Worksheet_Awal::$QTY];

                            //Awal
                            $kal .= "<td class='text-right'> " . floor($total_awal) . "</td>";

                            //Adjustment
                            $ws_awal = $this->ws_model->get(Worksheet_Awal::$TABLE_NAME, Worksheet_Awal::$NOMOR_WS, $row[Worksheet::$ID], null, null, false);
                            foreach ($ws_awal->result_array() as $row_ws_awal) {
                                if ($row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_FG] == NULL) {
                                    $material_rm_pm_ws = $this->ws_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM])->row_array();
                                    if ($material_rm_pm_ws[Material_RM_PM::$KEMASAN] != null) {
                                        break;
                                    }
                                }
                            }

                            //Ambil Total Tin dari Insert WS dan Adjustment WS
                            $total_tin = $this->ws_model->get_tin_kemasan_adjustment($row[Worksheet::$ID], $material_rm_pm_ws[Material_RM_PM::$ID]);
                            $kal .= "<td class='text-right'> " . floor($total_awal + $total_tin) . "</td>";

                            //Terima
                            //Cek apakah sudah disambung atau belum
                            $ws_connect = $this->ws_model->connect_transit_get_by(Worksheet_Connect::$NO_FG, $row[Worksheet::$ID]);
                            if ($ws_connect->num_rows() > 0) {
                                $tanggal_finish = $ws_connect->row_array()[Gudang_RM_PM_Transit::$TANGGAL_GENERATE];
                                $kal .= "<td class='text-right'> " . floor($total_awal + $total_tin) . "</td>";
                            } else {
                                $kal .= "<td class='text-right'> 0 </td>";
                            }
                            $unit = "TIN";

                            if (!in_array($row[Worksheet::$ID], $arr_ws_fg))
                                array_push($arr_ws_fg, $row[Worksheet::$ID]);
                        } else if (!in_array($row[Worksheet::$ID], $arr_ws_fg)) { //SEMI
                            $total_awal = $row[Worksheet_Awal::$QTY];

                            //Awal
                            $kal .= "<td class='text-right'> " . round($total_awal, 2) . "</td>";

                            //Adjustment
                            $qty_adj = 0;
                            if ($data_adjustment->num_rows() > 0)
                                $qty_adj = $data_adjustment->row_array()["qty_total"];

                            $kal .= "<td class='text-right'> " . round($total_awal + $qty_adj, 2) . "</td>";

                            //Terima
                            //Cek apakah sudah disambung atau belum
                            $ws_connect = $this->ws_model->connect_transit_get_by(Worksheet_Connect::$NO_SEMI, $row[Worksheet::$ID]);
                            if ($ws_connect->num_rows() > 0) {
                                $kal .= "<td class='text-right'> " . ($total_awal + $qty_adj) . "</td>";
                                $tanggal_finish = $ws_connect->row_array()[Gudang_RM_PM_Transit::$TANGGAL_GENERATE];
                            } else {
                                $kal .= "<td class='text-right'>  0 </td>";
                            }
                            $unit = Material_RM_PM::$S_UNIT_KG;
                        }

                        $kal .= "<td class='text-center'> " . $unit . "</td>";
                        $kal .= "<td class='text-center'>" . $tanggal_finish . "</td>";

                        if ($row[Worksheet::$IS_DELETED] == 1)
                            $kal .= "<td class='text-center text-danger'> DELETED </td>";
                        else if ($row[Worksheet::$IS_DONE] == 1)
                            $kal .= "<td class='text-center text-success'> DONE </td>";
                        else
                            $kal .= "<td class='text-center text-success'> DONE </td>";

                        $kal .= "</tr>";
                    }
                }
            }
            $kal .= '   </tbody>
                </table>';
            $kal .= "</div>";
            $kal .= "</div>";
            echo $kal;

        }else{ //Detail
            $arr_header = [
                "Tanggal", "WS", "Material", "Nomor GI", "No Urut", "Value (Rp)", "Qty", "Unit", "Vendor",  "NIK CM"
            ];
            $data = $this->ws_model->info_detail_get_informasi($nomor_ws_from,$nomor_ws_to, $dari_tanggal, $sampai_tanggal);
            $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
            if ($data->num_rows() > 0) {
                $arr_cm = [];
                $total_qty = 0;
                $total_value = 0;
                $arr_gi = [];
                $first_time=true;
                $old_ws="";
                foreach ($data->result_array() as $row) {
                    if (!in_array($row[Worksheet::$ID], $arr_gi) && $row[Worksheet_Checker_Detail::$NOMOR_URUT] != null) {
                        array_push($arr_gi, $row[Worksheet::$ID]);
                        if($show_total){
                            if(!$first_time){
                                //reset semua kasih total
                                $kal .= "<tr>";
                                    $kal .= "<td> </td>";
                                    $kal .= "<td> </td>";
                                    $kal .= "<td> </td>";
                                    $kal .= "<td> </td>";
                                    $kal .= "<td> </td>";
                                    $kal .= "<td class='text-right'>" . $this->rupiah($total_value) . "</td>";
                                    $kal .= "<td class='text-right'>" . $this->decimal($total_qty) . "</td>";
                                    $kal .= "<td> </td>";
                                    $kal .= "<td> </td>";
                                    $kal .= "<td> </td>";
                                $kal .= "</tr>";
                                $total_qty = 0;
                                $total_value = 0;
                            }else{
                                $first_time=false;
                            }
                        }else{
                            
                                $kal .= "<tr>";
                                $kal .= "<td class='text-center'></td>";

                                $worksheet=$this->ws_model->get(Worksheet::$TABLE_NAME, Worksheet::$ID,$old_ws)->row_array();

                                $kal .= "<td class='text-center'>". $old_ws ."</td>";
                                $kal .= "<td class='text-center'>" . $this->bom_model->get_by_id_bom_pv($worksheet[Worksheet::$ID_PV])->row_array()[BOM::$ID] . "</td>";
                                $kal .= "<td class='text-center'> </td>";
                                $kal .= "<td class='text-center'> </td>";
                                $kal .= "<td class='text-right'>" . $this->rupiah($total_value) . "</td>";
                                $kal .= "<td class='text-right'>" . $this->decimal($total_qty) . "</td>";
                                $kal .= "<td> </td>";
                                $kal .= "<td> </td>";
                                $kal .= "<td> </td>";
                                $kal .= "</tr>";
                                $total_qty = 0;
                                $total_value = 0;
                            
                        
                        }
                    }else{ //Adjustment
                        if($row[Worksheet_Checker_Detail::$NOMOR_URUT]==null && $show_total){
                            $kal .= "<tr>";
                            $kal .= "<td> </td>";
                            $kal .= "<td> </td>";
                            $kal .= "<td> </td>";
                            $kal .= "<td> </td>";
                            $kal .= "<td> </td>";
                            $kal .= "<td class='text-right'>" . $this->rupiah($total_value) . "</td>";
                            $kal .= "<td class='text-right'>" . $this->decimal($total_qty) . "</td>";
                            $kal .= "<td> </td>";
                            $kal .= "<td> </td>";
                            $kal .= "<td> </td>";
                            $kal .= "</tr>";
                            $total_qty = 0;
                            $total_value = 0;
                        }
                        
                    }
                    if (!in_array($row[Worksheet::$ID], $arr_cm) && $this->ws_model->is_fg($row[Worksheet::$ID])) {
                        $data_cm = $this->ws_model->get(Worksheet_Connect::$TABLE_NAME, Worksheet_Connect::$NO_FG, $row[Worksheet::$ID]);
                       
                    }else{
                        $data_cm = $this->ws_model->get(Worksheet_Connect::$TABLE_NAME, Worksheet_Connect::$NO_SEMI, $row[Worksheet::$ID]);
                    }
                    if (!in_array($row[Worksheet::$ID], $arr_cm)){
                        $cm = "";
                        array_push($arr_cm, $row[Worksheet::$ID]);


                        if($data_cm->num_rows()>0){
                            $cm .= $data_cm->row_array()[Worksheet_Connect::$NIK_PREMIX];

                            if (strpos($data_cm->row_array()[Worksheet_Connect::$NIK_MAKEUP], $cm) == -1) {
                                $cm .= ", " . $data_cm->row_array()[Worksheet_Connect::$NIK_MAKEUP];
                            }
                            if (strpos($data_cm->row_array()[Worksheet_Connect::$NIK_FILLING], $cm) == -1) {
                                $cm .= ", " . $data_cm->row_array()[Worksheet_Connect::$NIK_FILLING];
                            }
                            $arr_cm[$row[Worksheet::$ID]] = $cm;
                        }else{
                            $arr_cm[$row[Worksheet::$ID]] = "-";

                        }
                    }
                    $kal .= "<tr>";
                    $kal .= "<td class='text-center'>" . $row[Worksheet_Checker_Header::$TANGGAL] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Worksheet::$ID] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Worksheet_Checker_Header::$ID] . "</td>";
                    if(isset($row[Worksheet_Checker_Detail::$NOMOR_URUT]))
                        $kal .= "<td class='text-center'>" . $row[Worksheet_Checker_Detail::$NOMOR_URUT] . "</td>";
                    else
                        $kal .= "<td class='text-center'>ADJUSTMENT</td>";

                    $kal .= "<td class='text-right'>" . $this->rupiah($row[Worksheet_Checker_Detail::$VALUE]) . "</td>";
                    $total_value += $row[Worksheet_Checker_Detail::$VALUE];
                    $kal .= "<td class='text-right'>" . $this->decimal($row[Worksheet_Checker_Detail::$QTY]) . "</td>";
                    $total_qty += $row[Worksheet_Checker_Detail::$QTY];

                    $kal .= "<td class='text-center'>" . $row[Material_RM_PM::$UNIT] . "</td>";
                    if ($row[Worksheet_Checker_Detail::$ID_VENDOR] != null){
                        $vendor=$this->vendor_model->get(Vendor::$TABLE_NAME,Vendor::$ID, $row[Worksheet_Checker_Detail::$ID_VENDOR])->row_array();
                        $kal .= "<td class='text-center'>[" .$row[Worksheet_Checker_Detail::$ID_VENDOR] . "] " . $vendor[Vendor::$NAMA]. "</td>";
                    }
                    else
                        $kal .= "<td class='text-center'> - </td>";

                    if(isset($arr_cm[$row[Worksheet::$ID]])){
                        $kal .= "<td class='text-center'>" . $arr_cm[$row[Worksheet::$ID]] . "</td>";

                    }else{
                        $kal .= "<td class='text-center'> - </td>";

                    }
                    $kal .= "</tr>";
                    $old_ws = $row[Worksheet::$ID];
                }
            }
            $kal .= '   </tbody>
                </table>';
            $kal .= "</div>";
            $kal .= "</div>";
            echo $kal;

        }
        
       
    }
    public function get_view_print_by_no(){
        $data = [];
        $table_detail = "";
        $data['detail_product'] = '
						<thead>
							<tr class="text-center">
                                <td style="border: 1px solid black;" class="fw-bold">NOMOR</td>
                                <td style="border: 1px solid black;" class="fw-bold">KODE</td>
                                <td style="border: 1px solid black;" class="fw-bold">QUANTITY</td>
                                <td style="border: 1px solid black;" class="fw-bold">SATUAN</td>
							</tr>
						</thead><tbody>';

        $ws = $this->i_p("n");
        $qry = $this->ws_model->get_print_by_no($ws);

        $data_ws = $qry[Worksheet::$TABLE_NAME];
        $data['num_rows'] = $data_ws->num_rows();
        if($data_ws->num_rows()>0){
            //WS
            $data_ws = $data_ws->row_array();
            $data[Worksheet::$ID] = $data_ws[Worksheet::$ID];
            $data[Worksheet::$CTR_PRINT] = "R" . ($data_ws[Worksheet::$CTR_PRINT] + 1);
            $data[Worksheet::$BATCH_NUMBER] = $data_ws[Worksheet::$BATCH_NUMBER];

            //BOM
            $data_bom = $this->bom_model->get_by_id_bom_pv($data_ws[Worksheet::$ID_PV])->row_array();

            $data[BOM::$ID] = $data_bom[BOM::$ID];
            $data[BOM::$DESCRIPTION] = $data_bom[BOM::$DESCRIPTION];
            $data[BOM::$JOB_INSTRUCTION] = $data_bom[BOM::$JOB_INSTRUCTION];

            //QC ITEM
            $items="";
            $qc_items = explode("\n",$data_bom[BOM::$QC]);
            for ($i = 0; $i < count($qc_items); $i++) {
                $qc_item = explode("|", $qc_items[$i]);
                $items.= '<tr style="line-height:30px">
                            <td style="font-size:110%;border:1px solid black;" class="text-center">'.($i + 1).'</td>
                                    ';
                $align=["text-left","text-center","text-left",""];
                $padding=["padding-left:1%","", "padding-left:1%",""];
                for ($j = 0; $j < count($qc_item); $j++) {
                    if($qc_item[$j] == "0"){
                        $items .= '<td style="font-size:110%;border:1px solid black;' . $padding[$j] . '" class="' . $align[$j] . '"></td>';
                    }else{
                        $items .= '<td style="font-size:110%;border:1px solid black;' . $padding[$j] . '" class="' . $align[$j] . '">' . $qc_item[$j] . '</td>';

                    }
                }
                $items .= '<td style="font-size:110%;border:1px solid black;" class="text-center"></td>
                        </tr>';
            }
            $data[BOM::$QC] = $items;


            //BOM Alt
            $data_bom_alt = $this->bom_model->get(BOM_Alt::$TABLE_NAME, BOM_Alt::$ID, $data_bom[BOM_Production_Version::$ID_ALT], null, null, false)->row_array();
            $data[BOM_Alt::$ALT_NO] = $data_bom_alt[BOM_Alt::$ALT_NO];

            if($this->ws_model->is_fg($data[Worksheet::$ID])){
                $material = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $data_bom[BOM::$ID]);
                if($material->num_rows()>0){
                    $material = $material->row_array();
                }else{
				    $material = $this-> fg_model->sage_get_by_sap_id($data_bom[BOM::$ID])->row_array();
                }
                $data["tipe"] = Worksheet::$S_FG;
                $data_link = $this->ws_model->get(Worksheet_Link::$TABLE_NAME, Worksheet_Link::$NO_FG, $data[Worksheet::$ID])->row_array();
                $data[Worksheet::$S_SEMI] = $data_link[Worksheet_Link::$NO_SEMI];

                //Ambil Desc WS Seminya
                $data_ws_semi=$this->ws_model->get(Worksheet::$TABLE_NAME,Worksheet::$ID, $data_link[Worksheet_Link::$NO_SEMI]);
                $data_pv_semi = $this->ws_model->get(BOM_Production_Version::$TABLE_NAME, BOM_Production_Version::$ID, $data_ws_semi->row_array()[Worksheet::$ID_PV], null, null, false);
                $data_bom_semi = $this->ws_model->get(BOM::$TABLE_NAME, BOM::$ID, $data_pv_semi->row_array()[BOM_Production_Version::$PRODUCT_CODE], null, null, false);
                $data["prodname"] = $data_bom_semi->row_array()[BOM::$DESCRIPTION]." - ". $material[Material::$PACKING];

            }else
                $data["tipe"] = Worksheet::$S_SEMI;

            
            $ctr = 0;
            $total = 0;
            $is_kaleng = false;
            if ($qry[Worksheet_Awal::$TABLE_NAME]->num_rows() > 0) {
                foreach ($qry[Worksheet_Awal::$TABLE_NAME]->result_array() as $row_ws_awal) {
                    if ($row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_FG] != null) {
                        $table_detail .= "<tr style='font-family:'Times New Roman', Times, serif'>";
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;" class="text-center">' . ($ctr + 1) . '</td>';
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;" class="text-center">' . $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_FG] . '</td>';
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;padding-right:1%;" class="text-right">' . $this->decimal($row_ws_awal[Worksheet_Awal::$QTY], 3) . '</td>';
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;" class="text-center"> KG </td>';
                        $table_detail .= "</tr>";
                        $data[Worksheet_Awal::$QTY] = $this->decimal($row_ws_awal[Worksheet_Awal::$QTY], 3);

                    }else{
                        $material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM])->row_array();
                        $table_detail .= "<tr style='font-family:'Times New Roman', Times, serif'>";
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;" class="text-center">' . ($ctr + 1) . '</td>';
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;" class="text-center">' . $row_ws_awal[Worksheet_Awal::$KODE_MATERIAL_RM_PM] . '</td>';
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;padding-right:1%;" class="text-right">' . $this->decimal($row_ws_awal[Worksheet_Awal::$QTY], 3) . '</td>';
                            $table_detail .= '<td style="font-size:110%;border: 1px solid black;" class="text-center">' . $material[Material_RM_PM::$UNIT] . '</td>';
                        $table_detail .= "</tr>";
                        if(!$is_kaleng){
                            $data['batch_size'] = $this->decimal($row_ws_awal[Worksheet_Awal::$QTY], 3);
                            $is_kaleng = true;
                        }
                    }
                    
                    $ctr += 1;
                    $total += ($row_ws_awal[Worksheet_Awal::$QTY]);
                }
                //Total
                if (!$this->ws_model->is_fg($data[Worksheet::$ID])) {
                $table_detail .= '<tr>
                                <td class="text-right" style="font-size:110%;padding-right:1%" colspan="2" >Total</td>
                                <td class="text-right" style="font-size:110%;padding-right:1%">'. $this->decimal($total,3) .'</td>
                                <td class="text-right"></td>
                            </tr>';
                $data[Worksheet_Awal::$QTY] = $this->decimal($total, 3);

                }
                
            }

            $data['ctr'] = $ctr;
            $data['detail_product'] .= $table_detail;
            $data['detail_product'] .= "</tbody>";
        }


        echo json_encode($data);
    }

    public function print_plus_count(){
        $nomor_ws=$this->i_p("n");
        //Print nambah 1
        $this->ws_model->plus_print_count($nomor_ws);
        echo $nomor_ws;
    }
}
