<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class FG_Controller extends Library_Controller {

    public static $UPLOAD_MASTER="u_master";
    public static $UPLOAD_STOCK_TAHUNAN = "u_stock_tahunan";
    public static $UPLOAD_STOCK ="u_stock";

    public function upload($type){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->equals($type,$this::$UPLOAD_MASTER))
            $txt_file = "txt_upl_material";
        else if($this->equals($type,$this::$UPLOAD_STOCK_TAHUNAN))
            $txt_file = "txt_upl_stock_opname_tahunan";
        else if ($this->equals($type, $this::$UPLOAD_STOCK))
            $txt_file = "txt_upl_stock";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($type,$filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($type,$filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;
        if($this->equals($type,$this::$UPLOAD_STOCK_TAHUNAN))
            $row_header = 2;
        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }

        if ($this->equals($type, $this::$UPLOAD_MASTER))
            return $this->fg_model->update_batch($data);
        else if ($this->equals($type, $this::$UPLOAD_STOCK_TAHUNAN))
            return $this->fg_model->stock_tahunan_update_batch($data);
        else if ($this->equals($type, $this::$UPLOAD_STOCK))
            return $this->fg_model->stock_update_batch($data);
        
    }

    public function plus(){
        $ctr = $this->i_p("c");
        $tipe = $this->i_p("t");

        $mode_1 = "dus";
        $mode_2 = "tin";
        $disabled = "";
        $event = 'onkeyup="check_product(' . $ctr . ')"';
        $width_1 = "width:10%";
        $width_2 = "width:7%";
        if ($this->equals($tipe, FG_PLUS_TYPE_OPNAME)) {
            $mode_1 = "stock_program";
            $mode_2 = "stock_fisik";
            $disabled = "disabled";
            $event = 'onkeyup="check_varian_color(' . $ctr . ')" onkeyup="check_varian_color(' . $ctr . ')"';
            $width_1 = "width:12%";
            $width_2 = "width:12%";
        }
        $kal = '
			<tr class="tr_detail_product_' . $ctr . '">
				<td style="width:1%" class="text-center font-sm">' . ($ctr + 1) . '</td>
				<td style="width:5%" class="text-left font-sm"> 
					<input style="width:100%" onkeyup="check_product(' . $ctr . ')" type="text" id="txt_kode_barang_' . $ctr . '"/>
				</td>
			
				<td style="width:10%" class="text-left font-sm"> 
					<input style="width:100%" onkeyup="check_product(' . $ctr . ')" type="text" id="txt_warna_' . $ctr . '"/>
				</td>
			
				<td style="width:5%" class="text-left font-sm"> 
					<input style="width:100%" onkeyup="check_product(' . $ctr . ')" type="text" id="txt_kemasan_' . $ctr . '"/>
				</td>
			
				<td style="' . $width_2 . '" class="text-left font-sm"> 
					<input placeholder=0 min=0 style="width:100%" ' . $event . ' type="number" id="txt_' . $mode_2 . '_' . $ctr . '"/>
				</td>

				<td style="' . $width_1 . '" class="text-left font-sm"> 
					<input ' . $disabled . ' placeholder=0 min=0 style="width:100%" onkeyup="check_product(' . $ctr . ')" type="number" id="txt_' . $mode_1 . '_' . $ctr . '"/>
				</td>
				';
        if ($this->equals($tipe, FG_PLUS_TYPE_OPNAME)) {
            $kal .= '<td style="width:12%" class="text-left font-sm"> 
							<input value=0 min=0 disabled style="width:100%" type="number" id="txt_varian_' . $ctr . '"/>
						</td>';
        }
        if ($this->equals($tipe, FG_PLUS_TYPE_OUT)) {
            $kal .= '<input style="width:100%" type=hidden value=0 id="txt_box_' . $ctr . '"/>
						<td style="width:7%" class="text-left font-sm"> 
							<input min=0 style="width:100%" disabled placeholder="0" onkeyup="check_product(' . $ctr . ')" type="number" id="txt_stock_tin_' . $ctr . '"/>
						</td>
						<td style="width:5%" class="text-left font-sm"> 
							<input min=0 style="width:100%" disabled placeholder="0" onkeyup="check_product(' . $ctr . ')" type="number" id="txt_stock_dus_' . $ctr . '"/>
						</td>
						';
        }
        $kal .= '
				<td class="font-sm"> 
					<input style="border:0;width:100%" value="barang belum ditemukan!" disabled class="text-center red-text" type="text" id="txt_barang_desc_' . $ctr . '">
				</td>
				<td style="width:5%" class="font-sm text-center"> 
					<button style="width:100%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
					X
					</button>
				</td>
			</tr>';
        echo $kal;
    }
    public function get(){
        $kode_barang = $this->i_p("kb");
        $kemasan = $this->i_p("k");
        $warna = $this->i_p("w");
        $num_rows = $this->fg_model->get_by_hirarki($kode_barang, $kemasan, $warna)->num_rows();
        $temp = [];
        $temp["num_rows"] = $num_rows;
        if ($num_rows > 0) {
            $material = $this->fg_model->get_by_hirarki($kode_barang, $kemasan, $warna)->row_array();
            $temp[Material::$DESCRIPTION] = $material[Material::$DESCRIPTION];
            $stock = $this->fg_model->stock_get_by($material[Material::$ID]);
            $stock_dus = 0;
            $stock_tin = 0;
            if ($material[Material::$BOX] > 1) {
                $stock_dus = intdiv($stock, $material[Material::$BOX]);
                $stock_tin = $stock % $material[Material::$BOX];
            } else {
                $stock_tin = $stock;
            }
            $temp["stock_dus"] = $stock_dus;
            $temp[Material::$BOX] = $material[Material::$BOX];
            $temp["stock_tin"] = $stock_tin;
            $stock_fisik = $this->model->get_fisik_stock_by($material[Material::$ID]);
            $temp['stock_fisik'] = $stock_fisik;
            $temp['stock_program'] = $stock;
        }
        echo json_encode($temp);
    }
    public function get_filter(){
        $tipe = $this->i_p("t");
        $kemasan = $this->i_p("k");
        $kode_barang = $this->i_p("kb");
        $data = $this->fg_model->get_distinct($tipe, $kode_barang, $kemasan);
        $temp = [];
        $ctr = 0;
        foreach ($data->result_array() as $row) {
            $temp[$ctr]["value"] = $row["value"];
            $ctr++;
        }
        echo json_encode($temp);
    }
    public function insert_fisik_stock(){
        $this->insert(Material_Fisik::$TABLE_NAME);
    }

    public function view_sisa_transit(){
        $kal = "";
        $data = $this->fg_model->get_all_sisa_transit();
        $arr_header = ["Tanggal", "Nomor WS", "Kode Produk", "Deskripsi", "Qty (Dus)", "Qty (Tin)"];

        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>'. $this->gen_table_header($arr_header) .'</thead><tbody>';
                foreach ($data->result_array() as $row_fg) {
                        $kal .= "<tr>";
                            $kal .= "<td class='text-center'>" . $row_fg[Gudang_RM_PM_Transit::$TANGGAL_GENERATE] . "</td>";
                            $kal .= "<td class='text-center'>" . $row_fg[Worksheet_Connect::$NO_FG] . "</td>";
                            $kal .= "<td class='text-left'>" . $row_fg[Material::$PRODUCT_CODE] . "</td>";
                            $kal .= "<td class='text-left'>" . $row_fg[Material::$DESCRIPTION] . "</td>";

                            $dus = 0;
                            $tin = 0;
                            $total_tin = $row_fg[Gudang_RM_PM_Transit::$QTY];
                            if ($row_fg[Material::$BOX] > 1) {
                                $dus = intdiv($total_tin, $row_fg[Material::$BOX]);
                                $tin = $total_tin % $row_fg[Material::$BOX];
                            } else {
                                $tin = $total_tin;
                            }
                            $kal .= "<td class='text-right'>" . $dus . "</td>";
                            $kal .= "<td class='text-right'>" . $tin . "</td>";
                        $kal .= "</tr>";
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function view_history_stock(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $kode_barang = $this->i_p("kb");
        $kemasan = $this->i_p("k");
        $warna = $this->i_p("w");
        $gudang = $this->i_p("g");
        $nomor_sj = $this->i_p("n");
        $arr_header = ["Nomor", "Tanggal", "Material", "Stock Awal", "Debit", 
                        "Kredit", "Stock Akhir", "Gudang", "Pesan"];

        $kal = "";
        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12">';
                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>'. $this->gen_table_header($arr_header) .'</thead><tbody>';
                $data = $this->fg_model->get_history_stock($nomor_sj, $dt, $st, $kode_barang, $kemasan, $warna, $gudang);

                foreach ($data->result_array() as $row) {
                    if ($row[Stock_Gudang::$DEBIT] == 0) $kal .= "<tr class='red-text'>";
                    else $kal .= "<tr class='green-text'>";

                        $kal .= "<td class='text-center align-middle'>" . $row[Stock_Gudang::$ID] . "</td>";
                        $kal .= "<td class='text-center align-middle'>" . date("Y-m-d H:i", strtotime($row[Stock_Gudang::$TANGGAL])) . "</td>";
                        $material = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $row[Stock_Gudang::$ID_MATERIAL])->row_array()[Material::$PRODUCT_CODE];
                        $kal .= "<td class='text-center align-middle'>" . $material . "</td>";
                        $kal .= "<td class='text-right align-middle'>" . $row[Stock_Gudang::$STOCK_AWAL] . "</td>";
                        $kal .= "<td class='text-right align-middle'>" . $row[Stock_Gudang::$DEBIT] . "</td>";
                        $kal .= "<td class='text-right align-middle'>" . $row[Stock_Gudang::$KREDIT] . "</td>";
                        $kal .= "<td class='text-right align-middle'>" . $row[Stock_Gudang::$STOCK_AKHIR] . "</td>";
                        $gudang = $this->model->get(Gudang::$TABLE_NAME, Gudang::$ID, $row[Gudang::$ID]);
                        $temp = "RAK SUDAH DIHAPUS";
                        if($gudang->num_rows()>0){
                            $gudang = $gudang->row_array();
                            $temp = $gudang[Gudang::$NAMA_GUDANG] . " - RAK " . $gudang[Gudang::$RAK] . $gudang[Gudang::$KOLOM] . ' ' . $gudang[Gudang::$TINGKAT];
                        }
                        $kal .= "<td class='text-center align-middle'>" . $temp . "</td>";
                        $kal .= "<td>" . $row[Stock_Gudang::$PESAN] . "</td>";
                    $kal .= "</tr>";
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function view_stock(){
        $kal = "";
        $data = $this->fg_model->stock_get_all();
        $arr_header=["FG Code (Lama)","FG Code (Baru)","Kode Produk","Deksripsi","Stock (Tin)"];

        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>'. $this->gen_table_header($arr_header) .'</thead><tbody>';
                for ($i = 0; $i < count($data); $i++) {
                    $kal .= "<tr>";
                        $kal .= "<td class='text-center'>" . $data[$i][Material::$ID] . "</td>";
                        $kal .= "<td class='text-center'>" . $data[$i][Material::$ID_NEW] . "</td>";
                        $kal .= "<td class='text-left'>" . $data[$i][Material::$PRODUCT_CODE] . "</td>";
                        $kal .= "<td class='text-left'>" . $data[$i][Material::$DESCRIPTION] . "</td>";
                        $kal .= "<td class='text-right'>" . $data[$i][Fifo_Stock_Gudang::$QTY] . "</td>";
                    $kal .= "</tr>";
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function view_stock_by_rak(){
        $kal = "";
        $data = $this->fg_model->stock_get_by_rak();
        $arr_header=["FG Code (Lama)", "FG Code (Baru)", "Kode Produk", "Deskripsi", "Gudang", "Rak", "Kolom", "Tingkat", "Stock (Tin)"];

        $kal .= '<div class="row" style="margin-right:1%">';
            //$kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-12">';
                $kal .= '<table id="mastertable" style="margin-left:1%" class="table table-bordered table-sm">
                            <thead>.'. $this->gen_table_header($arr_header) .'</thead><tbody>';

                if($data->num_rows()>0){
                    foreach($data->result_array() as $row_stock){
                        $kal .= "<tr>";
                            $kal .= "<td class='align-middle text-center'>" . $row_stock[Material::$ID] . "</td>";
                            $kal .= "<td class='align-middle text-center'>" . $row_stock[Material::$ID_NEW] . "</td>";
                            $kal .= "<td class='align-middle text-left'>" . $row_stock[Material::$PRODUCT_CODE] . "</td>";
                            $kal .= "<td class='align-middle text-left'>" . $row_stock[Material::$DESCRIPTION] . "</td>";
                            $kal .= "<td class='align-middle text-center'>" . $row_stock[Gudang::$NAMA_GUDANG]."</td>";
                            $kal .= "<td class='align-middle text-center'>" . $row_stock[Gudang::$RAK] ."</td>";
                            $kal .= "<td class='align-middle text-center'>" . $row_stock[Gudang::$KOLOM] ."</td>";
                            $kal .= "<td class='align-middle text-center'>" . $row_stock[Gudang::$TINGKAT] ."</td>";
                            $kal .= "<td class='align-middle text-right'>" . $row_stock[Fifo_Stock_Gudang::$QTY] . "</td>";
                        $kal .= "</tr>";
                    }
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            //$kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function view_stock_opname(){
        $jenis = $this->i_p('j');
        $result = [];
        $result["button_view"] = "";
        $kal_button = "";
        $kal_content = "";
        if ($this->equals($jenis, STOCK_OPNAME_TYPE_HARIAN)) {
            $kal_button .= '<button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
							    Tambah
                            </button>
                            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="insert_fisik_stock()" data-mdb-ripple-color="dark">
                                Generate
                            </button>';
            $kal_content = '<table class="tabel_detail_product" style="visibility:hidden">
                                <tr>
                                    <td class="text-center font-sm">Nomor</td>
                                    <td class="text-center font-sm">Kode Barang</td>
                                    <td class="text-center font-sm">Warna</td>
                                    <td class="text-center font-sm">Kemasan</td>
                                    <td class="text-center font-sm">Fisik</td>
                                    <td class="text-center font-sm">Program</td>
                                    <td class="text-center font-sm">Varian</td>
                                    <td class="text-center font-sm">Barang</td>
                                </tr>
                            </table>';
        } else {
            $kal_button .= '<button type="button" id="btn_generate" class="center btn btn-outline-success" data-toggle="modal" data-target="#mod_upl_stock_opname_tahunan" data-mdb-ripple-color="dark">
							Upload Stock Opname Tahunan
						</button>';

            $kal_content .= '<div class="row text-center f-aleo-bold">';
                $kal_content .= '<div class="col-sm-5 text-right">';
                    $kal_content .= '<input type="checkbox" onchange="toggle_checkbox_gudang()" id="cb_gudang">';
                $kal_content .= '</div>';
                $kal_content .= '<div class="col-sm-7 text-left">';
                    $kal_content .= 'Tampilkan Lokasi Gudang';
                $kal_content .= '</div>';
            $kal_content .= '</div>';
            $kal_content .= '<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">FG Code (Lama)</th>
										<th scope="col" class="text-center">FG Code (Baru)</th>
										<th scope="col" class="text-center">Kode Produk</th>
										<th scope="col" class="text-center">Deskripsi</th>
										<th scope="col" class="text-center data_gudang">Gudang</th>
										<th scope="col" class="text-center">Program</th>
										<th scope="col" class="text-center">Fisik</th>
										<th scope="col" class="text-center">Varian</th>
									</tr>
								</thead><tbody>';
            $data = $this->fg_model->stock_get_all("Y");

            for ($i = 0; $i < count($data); $i++) {
                $kal_content .= "<tr>";
                $kal_content .= "<td class='text-center'>" . $data[$i][Material::$ID] . "</td>";
                $kal_content .= "<td class='text-center'>" . $data[$i][Material::$ID_NEW] . "</td>";
                $kal_content .= "<td class='text-center'>" . $data[$i][Material::$PRODUCT_CODE] . "</td>";
                $kal_content .= "<td class='text-left'>" . $data[$i][Material::$DESCRIPTION] . "</td>";
                $kal_content .= "<td class='text-left data_gudang'>" . $data[$i][Gudang::$TABLE_NAME] . "</td>";
                $kal_content .= "<td class='text-right'>" . number_format($data[$i][Fifo_Stock_Gudang::$QTY], 0, '.', ',') . "</td>";
                $kal_content .= "<td class='text-right'>" . number_format($data[$i][Material_Fisik::$QTY_FISIK], 0, '.', ',') . "</td>";
                $kal_content .= "<td class='text-right'>" . number_format(($data[$i][Material_Fisik::$QTY_FISIK] - $data[$i][Fifo_Stock_Gudang::$QTY]), 0, '.', ',') . "</td>";
                $kal_content .= "</tr>";
            }
            $kal_content .= '</tbody>
								</table>';

            $kal_content .= "</div>";
        }
        $result["button_view"] = $kal_button;
        $result["content_view"] = $kal_content;
        echo json_encode($result);
    }
    public function view_history_stock_opname(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");

        $arr_header=["Tanggal","NIK","FG Code (Lama)","FG Code (Baru)","Kode Produk","Fisik","Program","Varian"];

        $kal = "";
        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>'. $this->gen_table_header($arr_header) .'</thead><tbody>';
                $data = $this->fg_model->get_history_stock_opname($dt, $st);
                $arr_id_material=[];
                foreach ($data->result_array() as $row) {
                    $color="";
                    if(!in_array($row[Material::$ID],$arr_id_material))
                        array_push($arr_id_material,$row[Material::$ID]);
                    else
                        $color="red-text";
                    
                    $kal .= "<tr class='$color'>";
                        if($row[Material_Fisik::$TANGGAL]==null){
                            $kal .= "<td class='text-center'> </td>";
                        }else
                            $kal .= "<td class='text-center'>" . date("Y-m-d | H:i", strtotime($row[Material_Fisik::$TANGGAL])) . "</td>";
                        $kal .= "<td class='blue-text text-center' title='" . $row[Karyawan::$NAMA] . "'>" . $row[Karyawan::$ID] . "</td>";
                        $kal .= "<td class='text-center'>" . $row[Material::$ID] . "</td>";
                        $kal .= "<td class='text-center'>" . $row[Material::$ID_NEW] . "</td>";
                        $kal .= "<td class='text-left'>" . $row[Material::$PRODUCT_CODE] . "</td>";
                        $kal .= "<td class='text-right'>" . $row[Material_Fisik::$QTY_FISIK] . "</td>";
                        $kal .= "<td class='text-right'>" . $row[Material_Fisik::$QTY_PROGRAM] . "</td>";
                        $kal .= "<td class='text-right'>" . ($row[Material_Fisik::$QTY_FISIK] - $row[Material_Fisik::$QTY_PROGRAM]) . "</td>";
                    $kal .= "</tr>";
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function view_template_stock_opname(){
        $arr_header = ["FG Code (Lama)","FG Code (Baru)" , "Rak", "Kolom", "Tingkat", "Kapasitas", "Kode Produk", "Box", "PCS", "Qty","Qty Dus","Qty Tin"];

        $kal = "";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';
        $hide=$this->i_p("h");
        $data = $this->fg_model->get_template_stock_opname($hide);
        foreach ($data->result_array() as $row) {
            $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row[Material::$ID] . "</td>";
                $kal .= "<td class='text-center'>" . $row[Material::$ID_NEW] . "</td>";
                $kal .= "<td class='text-center'>" . $row[Gudang::$RAK] . "</td>";
                $kal .= "<td class='text-center'>" . $row[Gudang::$KOLOM] . "</td>";
                $kal .= "<td class='text-center'>" . $row[Gudang::$TINGKAT] . "</td>";
                $kal .= "<td class='text-right'>" . $row[Gudang::$KAPASITAS] . "</td>";
                $kal .= "<td class='text-left'>" . $row[Material::$PRODUCT_CODE] . "</td>";
                $kal .= "<td class='text-right'>" . $row[Material::$BOX] . "</td>";
                $kal .= "<td class='text-right'>" . $row[FG_Stock_Opname::$S_PCS] . "</td>";
                $kal .= "<td class='text-right'>" . $row[Fifo_Stock_Gudang::$QTY] . "</td>";
                $kal .= "<td class='text-right'>" . $row[FG_Stock_Opname::$S_QTY_DUS] . "</td>";
                $kal .= "<td class='text-right'>" . $row[FG_Stock_Opname::$S_QTY_TIN] . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function view_rekap_sj(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $nomor_sj = $this->i_p("n");
        $kode_barang = $this->i_p("kb");
        $kemasan = $this->i_p("k");
        $warna = $this->i_p("w");
        $jenis = $this->i_p("j");
        $query = "";

        $arr_header = ["Tanggal Input", "Tanggal Kirim", "Kode GR/GI", "NIK", "Nomor SJ", 
                        "FG Code (Lama)", "FG Code (Baru)", "Kode Produk","Tin"];

        $kal = "";
        $kal .= '<div class="row" style="margin-left:1%;margin-right:1%">';
            //$kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-12">';
                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>' . $this->gen_table_header($arr_header) . '</thead>
                            <tbody>';
        $query = $this->goodreceipt_model->get_rekap_sj($dt, $st, $nomor_sj, $kode_barang, $kemasan, $warna, $jenis);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                        $kal .= "<tr>";
                            $kal .= "<td class='text-center green-text'>" . date('Y-m-d | H:i', strtotime($row[H_Good_Receipt::$TANGGAL])) . "</td>";
                            $kal .= "<td class='text-center green-text'> - </td>";
                            $kal .= "<td class='text-center green-text'>" . $row[H_Good_Receipt::$ID] . "</td>";

                            $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[H_Good_Receipt::$NIK])->row_array();
                            $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row[H_Good_Receipt::$NIK] . "</td>";
                            $kal .= "<td class='text-center green-text'>" . $row[H_Good_Receipt::$NOMOR_SJ] . "</td>";
                            $kal .= "<td class='text-center green-text'>" . $row[D_Good_Receipt::$ID_MATERIAL] . "</td>";
                            $kal .= "<td class='text-center green-text'>" . $row[Material::$ID_NEW] . "</td>";

                            $material = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $row[D_Good_Receipt::$ID_MATERIAL])->row_array();
                            $kal .= "<td class='text-center green-text'>" . $material[Material::$PRODUCT_CODE] . "</td>";

                            $dus = $row[D_Good_Receipt::$DUS];
                            $tin = $row[D_Good_Receipt::$TIN];
                            $tampil_tin = ($dus * $row[Material::$BOX]) + $tin;
                            $kal .= "<td class='text-right green-text'>" . $tampil_tin . "</td>";
                        $kal .= "</tr>";
            }
        }

        $query = $this->goodissue_model->get_rekap_sj($dt, $st, $nomor_sj, $kode_barang, $kemasan, $warna, $jenis);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                        $kal .= "<tr>";
                            $kal .= "<td class='text-center red-text'>" . date('Y-m-d | H:i', strtotime($row[H_Good_Issue::$TANGGAL])) . "</td>";
                            if (isset($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]))
                                $kal .= "<td class='text-center red-text'>" . date('Y-m-d', strtotime($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN])) . "</td>";
                            else
                                $kal .= "<td class='text-center red-text'> - </td>";

                            $kal .= "<td class='text-center red-text'>" . $row[H_Good_Issue::$ID] . "</td>";

                            $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[H_Good_Issue::$NIK])->row_array();
                            $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row[H_Good_Issue::$NIK] . "</td>";
                            $kal .= "<td class='text-center red-text'>" . $row[H_Good_Issue::$NOMOR_SJ] . "</td>";
                            $kal .= "<td class='text-center red-text'>" . $row[D_Good_Issue::$ID_MATERIAL] . "</td>";
                            $kal .= "<td class='text-center red-text'>" . $row[Material::$ID_NEW] . "</td>";


                            $material = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $row[D_Good_Issue::$ID_MATERIAL])->row_array();
                            $kal .= "<td class='text-center red-text'>" . $material[Material::$PRODUCT_CODE] . "</td>";

                            $dus = $row[D_Good_Issue::$DUS];
                            $tin = $row[D_Good_Issue::$TIN];
                            $tampil_tin = ($dus * $row[Material::$BOX]) + $tin;
                            $tampil_tin *= -1;
                            $kal .= "<td class='text-right red-text'>" . $tampil_tin . "</td>";

                        $kal .= "</tr>";
            }
        }
                $kal .= '	</tbody>
                        </table>';
            $kal .= "</div>";
            //$kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }

    
}
