<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class OtherAC_Controller extends Library_Controller {

    public function get_all(){
        $data = $this->other_model->get(Other_Asset_Class::$TABLE_NAME);
        $temp = [];
        $ctr = 0;
        foreach ($data->result_array() as $row) {
            $temp[$ctr][Other_Asset_Class::$ID] = $row[Other_Asset_Class::$ID];
            $temp[$ctr][Other_Asset_Class::$DESKRIPSI] = $row[Other_Asset_Class::$DESKRIPSI];
            $ctr++;
        }
        echo json_encode($temp);
    }

    public function get_gl_by_id(){
        $id_ac = $this->i_p("i");
        $id_gl = $this->other_model->get(Other_Asset_Class::$TABLE_NAME, Other_Asset_Class::$ID, $id_ac)->row_array()[Other_Asset_Class::$ID_GL];
        echo $id_gl;
    }
}
