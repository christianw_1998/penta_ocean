<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class PR_Controller extends Library_Controller {
    
    public function get_tabel_awal(){
        $faktor_pengali = $this->i_p("f");
        $tipe = $this->i_p("t");

        $arr_header = ["<input type='checkbox' onclick='toggle_check(this)'></input>", "ID Material", "Deskripsi", "Vendor", "Buffer Stock", "Current Stock", "Qty PR", "Note"];

        $kal = "";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead><tbody>';
        $data = $this->pr_model->get_tabel_awal($tipe);

        $ctr=0;
        foreach ($data->result_array() as $row) {
            $curr_stock=$this->rmpm_model->stock_get_by($row[Material_RM_PM::$ID],$row[Worksheet_Checker_Detail::$ID_VENDOR]);
            $qty_pr= round($row[Purchase_Request::$S_BUFFER_STOCK] * $faktor_pengali, 2)-$curr_stock;
            $check="";
            if($qty_pr>0){
                $check= "<input type='checkbox' checked id='chk_". $ctr ."' value=$ctr>";
            }else{
                $check = "<input type='checkbox' id='chk_" . $ctr . "' value=$ctr>";

            }
            $kal .= "<tr>";
            $hidden = "<input type='hidden' id='txt_idm_" . $ctr . "' value='" . $row[Material_RM_PM::$ID] . "'>
                        <input type='hidden' id='txt_qpr_" . $ctr . "' value='" . round($qty_pr, 2) . "'>
                        <input type='hidden' id='txt_idv_" . $ctr . "' value='" . $row[Worksheet_Checker_Detail::$ID_VENDOR] . "'>";
            $kal .= "<td class='align-middle text-center'>". $hidden . $check."</td>";
            $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";
            $kal .= "<td class='align-middle text-left'>" . $row[Material_RM_PM::$DESCRIPTION] . "</td>";
            $kal .= "<td class='align-middle text-center'>" . $row[Worksheet_Checker_Detail::$ID_VENDOR] . "</td>";
            $kal .= "<td class='align-middle text-right'>" . round($row[Purchase_Request::$S_BUFFER_STOCK]*$faktor_pengali,2) . "</td>";
            $kal .= "<td class='align-middle text-right'>" . round($curr_stock,2). "</td>";
            $kal .= "<td class='align-middle text-right'>" . round($qty_pr,2) . "</td>";
            if($qty_pr>0){
                $kal .= "<td class='align-middle text-success text-center'>Perlu dibuat PR</td>";

            }else{
                $kal .= "<td class='align-middle text-danger text-center'>Tidak Perlu dibuat PR</td>";

            }
            
            $kal .= "</tr>";
            $ctr++;
        }
        //untuk yang tidak pernah ditransaksikan
        $data_pr_zero_trans=$this->db->query("select * from material_rm_pm_stock,material_rm_pm where material_rm_pm_stock.id_material_rm_pm=material_rm_pm.id_material_rm_pm and material_rm_pm.tipe='". $tipe . "' and id_material_rm_pm_stock not in (SELECT distinct material_rm_pm_stock.id_material_rm_pm_stock FROM material_rm_pm_stock,worksheet_checker_detail,material_rm_pm
                where material_rm_pm_stock.id_material_rm_pm=worksheet_checker_detail.id_material_rm_pm AND material_rm_pm_stock.id_material_rm_pm=material_rm_pm.id_material_rm_pm and
                material_rm_pm.tipe='". $tipe ."')");
        if($data_pr_zero_trans->num_rows()>0){
            foreach ($data_pr_zero_trans->result_array() as $row) {
                $curr_stock = $this->rmpm_model->stock_get_by($row[Material_RM_PM::$ID], $row[Worksheet_Checker_Detail::$ID_VENDOR]);
                $material_rm_pm=$this->rmpm_model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$ID,$row[Material_RM_PM_Stock::$ID_MATERIAL])->row_array();
                $check = "<input type='checkbox' id='chk_" . $ctr . "' value=$ctr>";
                $kal .= "<tr>";
                $hidden = "<input type='hidden' id='txt_idm_" . $ctr . "' value='" . $row[Material_RM_PM_Stock::$ID_MATERIAL] . "'>
                        <input type='hidden' id='txt_qpr_" . $ctr . "' value='0'>
                        <input type='hidden' id='txt_idv_" . $ctr . "' value='" . $row[Material_RM_PM_Stock::$ID_VENDOR] . "'>";
                $kal .= "<td class='align-middle text-center'>" . $hidden . $check . "</td>";
                $kal .= "<td class='align-middle text-center'>" . $material_rm_pm[Material_RM_PM::$KODE_MATERIAL] . "</td>";
                $kal .= "<td class='align-middle text-left'>" . $material_rm_pm[Material_RM_PM::$DESCRIPTION] . "</td>";
                $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM_Stock::$ID_VENDOR] . "</td>";
                $kal .= "<td class='align-middle text-right'>0</td>";
                $kal .= "<td class='align-middle text-right'>" . round($curr_stock, 2) . "</td>";
                $kal .= "<td class='align-middle text-right'>". round(0-$curr_stock,2)."</td>";
                $kal .= "<td class='align-middle text-danger text-center'>Tidak Perlu dibuat PR</td>";


                $kal .= "</tr>";
                $ctr++;
            }
        }
        $kal .= '	</tbody>
						</table>';
        $kal .= "</div>";
        $kal .= "</div>";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12 text-center">
                    <button type="button" class="center text-center btn btn-outline-success" onclick="gen_pr()" data-mdb-ripple-color="dark">
                    GENERATE PR
                    </button>
                </div>';
        $kal .= "</div>";

        echo $kal;
    }
    public function get_view_history(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $kal = "";
        $data = $this->pr_model->get_history($tanggal_dari, $tanggal_sampai);

        $arr_header = [
            "Kode PR", "ID Material", "Deskripsi", "ID Vendor","Tanggal", "Qty", "Sudah di PO"
        ];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row[Purchase_Request_Header::$ID] . "</td>";
                $kal .= "<td class='text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";
                $kal .= "<td class='text-left'>" . $row[Material_RM_PM::$DESCRIPTION] . "</td>";

                if ($row[Purchase_Request_Detail::$ID_VENDOR] != null) {
                    $vendor=$this->vendor_model->get(Vendor::$TABLE_NAME,Vendor::$ID,$row[Purchase_Request_Detail::$ID_VENDOR])->row_array()[Vendor::$NAMA];
                    $kal .= "<td class='text-center blue-text' title='$vendor'>" . $row[Purchase_Request_Detail::$ID_VENDOR] . "</td>";
                }else
                    $kal .= "<td class='text-center'>-</td>";

                $kal .= "<td class='text-center'>" . $row[Purchase_Request_Header::$TANGGAL] . "</td>";
                $kal .= "<td class='text-right'>" . $this->decimal($row[Purchase_Request_Detail::$QTY_PR]) . "</td>";

                if($row[Purchase_Request_Header::$IS_DONE]==1){
                    $kal .= "<td class='text-center green-text'> V </td>";
                }else{
                    $kal .= "<td class='text-center red-text'> X </td>";

                }
                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_tutup(){
        $kal = "";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");
        $data = $this->pr_model->get_utuh($dari_tanggal,$sampai_tanggal);

        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-3"></div>';
            $kal .= '<div class="col-sm-6">';
                $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col" class="text-center">Tanggal</th>
                                    <th scope="col" class="text-center">Nomor PO</th>
                                    <th scope="col" class="text-center">Status</th>
                                    <th scope="col" class="text-center">Aksi</th>
                                </tr>
                            </thead><tbody>';
                if ($data->num_rows() > 0) {
                    foreach ($data->result_array() as $row_pr) {
                        $kal .= "<tr>";
                            $kal .= "<td class='text-center'>" . $row_pr[Purchase_Request_Header::$TANGGAL] . "</td>";
                            $kal .= "<td class='text-center'>" . $row_pr[Purchase_Request_Header::$ID] . "</td>";
                            if($row_pr[Purchase_Request_Header::$IS_DELETED]==0){
                                $kal .= "<td class='text-center green-text'>AKTIF</td>";
                                $kal .= "<td class='text-center'><button type='button' class='center btn btn-outline-danger' data-mdb-ripple-color='dark' onclick='update_status(" . $row_pr[Purchase_Request_Header::$ID] . "," . ($row_pr[Purchase_Request_Header::$IS_DELETED] * -1 + 1) . ")'>TUTUP</button></td>";
                            }else{
                                $kal .= "<td class='text-center red-text'>NON-AKTIF</td>";
                                $kal .= "<td class='text-center'><button type='button' class='center btn btn-outline-success' data-mdb-ripple-color='dark' onclick='update_status(" . $row_pr[Purchase_Request_Header::$ID] . "," . ($row_pr[Purchase_Request_Header::$IS_DELETED] * -1 + 1) . ")'>KEMBALI</button></td>";
                            }
                        $kal .= "</tr>";
                    }
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }

    public function insert_pr(){
        $arr_id_material = $this->i_p("am");
        $arr_id_vendor = $this->i_p("av");
        $arr_qty_pr = $this->i_p("aq");
        $ctr = $this->i_p("c");

        echo MESSAGE[$this->pr_model->insert($ctr,$arr_id_material,$arr_id_vendor,$arr_qty_pr)];
    }
    public function change_status()
    {
        $nomor_pr = $this->i_p("n");
        $is_deleted = $this->i_p("i");

        echo MESSAGE[$this->pr_model->change_status($nomor_pr, $is_deleted)];
    }
    
}
