<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class OtherMaterial_Controller extends Library_Controller {

    public function get_last_code(){
        $kp = strtoupper($this->i_p("kp"));
        $max = intval($this->other_model->material_get_last_code($kp));
        $kode = $kp . str_pad($max+1, 4, "0", STR_PAD_LEFT);
        echo $kode;
    }

    public function material_insert(){
        $this->insert(Other_Material::$TABLE_NAME);
    }

    //1 ASSET, 2 MISC, 3 SERVICE, 4 SPAREPART
    public static $STATIC_HEADER_REKAP = [
        "Tanggal", "Kategori", "Kode Material", "Deskripsi", "Asset Class", "Cost Center", "Vendor", "Kode GL"
    ];
    public static $STATIC_HEADER_REKAP_CLASS = [
        "cb_0", "cb_0", "cb_0", "cb_0", "cb_1", "cb_0", "cb_0", "cb_0"
    ];

    public function get_cb_kolom_tampil(){
        $data=[];
        
        $kal = "<div class='row'>";
        for ($i = 0; $i < count($this::$STATIC_HEADER_REKAP); $i++) {
            $kal .= "<div class='col-sm-3'>
                        <label>
                        <input checked class='rekap_class ". $this::$STATIC_HEADER_REKAP_CLASS[$i] ."' type='checkbox' value='tanggal' id='cb_". $i ."'></input>
                        ". $this::$STATIC_HEADER_REKAP[$i] ."</label>
                    </div>";
        }
        $kal .= "</div>";
        $data["kal"]=$kal;
        $data["ctr"]= count($this::$STATIC_HEADER_REKAP);
        echo json_encode($data);
    }

    //1 ASSET, 2 MISC, 3 SERVICE, 4 SPAREPART
    public static $STATIC_HEADER_HISTORY = [
        "Tanggal", "Nomor PO", "Nomor SJ", "Nomor GI/GR", "Material", "Vendor", "Stock Awal", "V. Awal (Rp)", "Debit", "Kredit", "Stock Akhir", "V. Akhir (Rp)", "Pesan"
    ];
    public static $STATIC_HEADER_HISTORY_CLASS = [
        "cb_0", "cb_0",
        "cb_0", "cb_0", "cb_0", "cb_0", "cb_4", "cb_4", "cb_4", "cb_4",
        "cb_4", "cb_4", "cb_0"
    ];

    public function history_get_cb_kolom_tampil(){
        $data = [];

        $kal = "<div class='row'>";
        for ($i = 0; $i < count($this::$STATIC_HEADER_HISTORY); $i++) {
            $kal .= "<div class='col-sm-3'>
                        <label>
                        <input checked class='rekap_class " . $this::$STATIC_HEADER_HISTORY_CLASS[$i] . "' type='checkbox' id='cb_" . $i . "'></input>
                        " . $this::$STATIC_HEADER_HISTORY[$i] . "</label>
                    </div>";
        }
        $kal .= "</div>";
        $data["kal"] = $kal;
        $data["ctr"] = count($this::$STATIC_HEADER_HISTORY);
        echo json_encode($data);
    }
    public function get_view_history(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $id_kategori = $this->i_p("ik");
        $arr_header_class = $this->i_p("hc");
        $data = $this->other_model->material_get_history($dt, $st, $id_kategori);

        $kal = "";

        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>';
        $kal .= '<tr class="table-primary">
                    <th scope="col" class="fw-bold align-middle text-center">Nomor</th>';
        for ($i = 0; $i < count($this::$STATIC_HEADER_HISTORY); $i++) {
            if ($arr_header_class[$i] == 1)
                $kal .= '<th scope="col" class="fw-bold align-middle text-center">' . $this::$STATIC_HEADER_HISTORY[$i] . '</th>';
        }
        $kal .= '</tr></thead>';
        $kal .= '<tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $ctr = 0;
                $text_color = "green-text";

                if ($row[Stock_Gudang_Other::$KREDIT] != 0) {
                    $text_color="red-text";
                }

                $kal .= "<tr class='$text_color'>";
                    $kal .= "<td class='align-middle text-center'>" . $row[Stock_Gudang_Other::$ID] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-center'>" . date("Y-m-d",strtotime($row[Stock_Gudang_Other::$TANGGAL])) . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-center'>" . $row[Stock_Gudang_Other::$NOMOR_PO] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-left'>" . $row[Stock_Gudang_Other::$NOMOR_SJ] . "</td>";
                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-left'>" . $row[Stock_Gudang_Other::$NOMOR_GI_GR] . "</td>";
                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-left'>[" . $row[Other_Material::$ID] . "] ". $row[Other_Material::$DESKRIPSI] ."</td>";

                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-center'>[" . $row[Stock_Gudang_Other::$ID_VENDOR] . "] " . $row[Stock_Gudang_Other::$NAMA_VENDOR] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-right'>" . $row[Stock_Gudang_Other::$STOCK_AWAL] . "</td>";
                
                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-right'>" . $this->rupiah($row[Stock_Gudang_Other::$VALUE_AWAL]) . "</td>";
                
                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-right'>" . $row[Stock_Gudang_Other::$DEBIT] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-right'>" . $row[Stock_Gudang_Other::$KREDIT] . "</td>";
                
                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-right'>" . $row[Stock_Gudang_Other::$STOCK_AKHIR] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-right'>" . $this->rupiah($row[Stock_Gudang_Other::$VALUE_AKHIR]) . "</td>";
                
                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-left'>" . $row[Stock_Gudang_Other::$PESAN] . "</td>";

                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_rekap(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $id_kategori = $this->i_p("ik");
        $arr_header_class = $this->i_p("hc");
        $data = $this->other_model->material_get_rekap($dt, $st, $id_kategori);

        $kal ="";

        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>';
        $kal .= '<tr class="table-primary">';
        for ($i = 0; $i < count($this::$STATIC_HEADER_REKAP); $i++) {
            if($arr_header_class[$i]==1)
                $kal .= '<th scope="col" class="fw-bold align-middle text-center">' . $this::$STATIC_HEADER_REKAP[$i] . '</th>';
        }
        $kal .= '</tr></thead>';
        $kal .='<tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $ctr = 0;

                $kal .= "<tr>";
                if ($arr_header_class[$ctr++] == 1)
                    $kal .= "<td class='align-middle text-center'>" . $row[Other_Material::$TANGGAL] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row[Other_Kategori::$NAMA] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row[Other_Material::$ID] . "</td>";
                
                if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>" . $row[Other_Material::$DESKRIPSI] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row[Other_Asset_Class::$ID] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>[" . $row[Cost_Center::$ID] . "] " . $row[Cost_Center::$NAMA] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>[" . $row[Vendor::$ID] . "] " . $row[Other_Material::$NAMA_VENDOR] . "</td>";

                if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>[" . $row[General_Ledger::$ID] . "] " . $row[General_Ledger::$DESKRIPSI] . "</td>";

                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_stock(){
        $kal = "";
        $data = $this->other_model->material_stock_get_all();
        $arr_header=["Kode Material","Deskripsi","Stock"];

        $kal .= '<div class="row" style="margin-right:1%;margin-left:1%">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
							<thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';
        foreach ($data->result_array() as $row_material) {
            $kal .= "<tr>";
            $kal .= "<td class='text-center'>" . $row_material[Other_Material::$ID] . "</td>";
            $kal .= "<td class='text-left'>" . $row_material[Other_Material::$DESKRIPSI] . "</td>";
            
            $kal .= "<td class='text-right'>" . $row_material[Other_Material_Stock::$QTY] . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '	</tbody>
						</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }

    public function get_by_id(){
        $id_material = $this->i_p("im");
        $kategori = $this->i_p("ik");
        $data = $this->other_model->material_get_by_id($id_material, $kategori);
        $temp = [];
        if($data->num_rows()>0){
            foreach ($data->result_array() as $row) {
                $temp[Other_Material::$ID] = $row[Other_Material::$ID];
                $temp[Other_Material::$DESKRIPSI] = $row[Other_Material::$DESKRIPSI];

                $stock = $this->other_model->get(Other_Material_Stock::$TABLE_NAME, Other_Material_Stock::$ID_OTHER_MATERIAL, $temp[Other_Material::$ID], null, null, false)->row_array();
                $temp[Other_Material_Stock::$QTY] = $stock[Other_Material_Stock::$QTY];
            }
        }
        echo json_encode($temp);
    }
}
