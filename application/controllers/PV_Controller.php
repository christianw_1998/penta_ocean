<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class PV_Controller extends Library_Controller {

    public function plus(){
        $ctr = $this->i_p("c");

        
           $kal = '
                <tr class="tr_detail_product_' . $ctr . '">
                    <td class="text-center font-sm">' . ($ctr + 1) . '</td>';

               $kal .= '<td class="text-center font-sm"> 
                            <input onkeyup="cek_product(' . $ctr . ')" class="form-control " type="text" id="txt_bom_' . $ctr . '"/>
                        </td>
                    
                        <td class="text-center font-sm"> 
                            <input style="width:100%" value="Produk belum ditemukan" disabled class="form-control red-text text-left" f-aleo" type="text" id="txt_desc_bom_' . $ctr . '"/>
                        </td>';
            
                $kal .= '
                        <td style="width:15%" class="text-center font-sm"> 
                            <select class="form-control"style="width:100%" id="dd_pv_'. $ctr .'"></select>
                        </td>';

                $kal .= '
                    <td style="width:20%" class="text-center font-sm"> 
						Dari: <br><input class="text-center black-text form-control" type="date" id="txt_valid_from_' . $ctr . '"/>
						Sampai: <br><input class="text-center black-text form-control" type="date" id="txt_valid_to_' . $ctr . '"/>
						Update Terakhir: <br><input disabled class="text-center red-text form-control" type="date" id="txt_last_date_' . $ctr . '"/>
                    </td>
                ';

            $kal .= '<td style="width:2%" class="font-sm text-center"> 
                    <button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
                    X
                    </button>
                </td>
            </tr>';
        
        echo $kal;
    }
    public function insert_pv(){
        $this->insert(BOM_Production_Version::$TABLE_NAME);
    }

    public function get_view_rekap(){
        $vf_tanggal_dari = $this->i_p("df");
        $vf_tanggal_sampai = $this->i_p("sf");
        $vt_tanggal_dari = $this->i_p("dt");
        $vt_tanggal_sampai = $this->i_p("st");
        $kode_produk = $this->i_p("kp");
        $kal = "";
        $data = $this->bom_model->pv_get_rekap($vf_tanggal_dari, $vf_tanggal_sampai, $vt_tanggal_dari, $vt_tanggal_sampai, $kode_produk);

        $arr_header = [
            "Valid Dari", "Valid Sampai", "Kode Produk", "Deskripsi", "Alt BOM yang Dipakai", 
        ];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row[BOM_Production_Version::$VALID_FROM] . "</td>";
                $kal .= "<td class='text-center'>" . $row[BOM_Production_Version::$VALID_TO] . "</td>";
                $kal .= "<td class='text-center'>" . $row[BOM_Production_Version::$PRODUCT_CODE] . "</td>";
                $kal .= "<td class='text-left'>" . $row[BOM::$DESCRIPTION] . "</td>";
                $kal .= "<td class='text-right'>" . $row[BOM_Alt::$ALT_NO] . "</td>";

                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }

    public function semi_get_active_by_product_code(){
        $pc = $this->i_p("pc");

        $data_bom = $this->pv_model->get_active_by_product_code($pc);
        $data=[];
        $data["num_rows"]= $data_bom->num_rows();
        if($data_bom->num_rows()>0){
            $bom = $data_bom->row_array();
            $data[BOM_Production_Version::$ID] = $bom[BOM_Production_Version::$ID];
            $data[BOM_Alt::$ALT_NO] = $bom[BOM_Alt::$ALT_NO];
        }

        //Cek apakah ada di Master RM PM, jika ada maka tidak perlu buat WS FG
        $material=$this->rmpm_model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$KODE_MATERIAL,$pc);
        if($material->num_rows()>0){
            $data["is_rm"] = true;
        }else{
            $data["is_rm"] = false;

        }
        echo json_encode($data);
    }

    public function fg_get_by_product_code(){
        $pc = $this->i_p("pc");

        $data_bom = $this->pv_model->fg_get_by_product_code($pc);
        $data=[];
        $data["num_rows"]= $data_bom->num_rows();
        if($data_bom->num_rows()>0){
            $fg = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $pc);
            $bom = $data_bom->row_array();

            if($fg->num_rows()>0){
                $data[BOM_Alt_Recipe::$QTY] = $fg->row_array()[Material::$NET];
            }else{
				$fg = $this->fg_model->sage_get_by_sap_id($pc);
                $data[BOM_Alt_Recipe::$QTY] = $fg->row_array()[Material::$NET];
            }
            $data[BOM_Production_Version::$ID] = $bom[BOM_Production_Version::$ID];
            $data[BOM_Alt::$ALT_NO] = $bom[BOM_Alt::$ALT_NO];
            $data[BOM::$DESCRIPTION] = $bom[BOM::$DESCRIPTION];
        }
        echo json_encode($data);
    }
}
