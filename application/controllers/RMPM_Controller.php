<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class RMPM_Controller extends Library_Controller {

	public static $TABLE_STOCK_OPNAME="STOCK_OPNAME";
	
	public function upload(){
		$config[CONFIG_UPLOAD_PATH] = './asset/upload/';
		$config[CONFIG_ALLOWED_TYPES] = 'xlsx';
		$config[CONFIG_MAX_SIZE] = '5000';
		$this->load->library('upload');
		$this->upload->initialize($config);

		$txt_file = "txt_upl_material";
		if ($this->upload->do_upload($txt_file)) {
			$upload_data = $this->upload->data();
			$filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

			$message = $this->update_db(Material_RM_PM::$TABLE_NAME, $filename);
			if (isset(MESSAGE[$message]))
				echo MESSAGE[$message];
			else
				echo $message;

			unlink($filename);
		} else {
			$data = $this->upload->display_errors();
			echo $this->get_message($data);
		}
	}
	public function upload_stock(){
		$config[CONFIG_UPLOAD_PATH] = './asset/upload/';
		$config[CONFIG_ALLOWED_TYPES] = 'xlsx';
		$config[CONFIG_MAX_SIZE] = '5000';
		$this->load->library('upload');
		$this->upload->initialize($config);

		$txt_file = "txt_upl_stock_material_rm_pm";
		if ($this->upload->do_upload($txt_file)) {
			$upload_data = $this->upload->data();
			$filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

			$message = $this->update_db(Material_RM_PM_Stock::$TABLE_NAME,$filename);
			if (isset(MESSAGE[$message]))
				echo MESSAGE[$message];
			else
				echo $message;

			unlink($filename);
		} else {
			$data = $this->upload->display_errors();
			echo $this->get_message($data);
		}
	}
	public function upload_stock_opname(){
		$config[CONFIG_UPLOAD_PATH] = './asset/upload/';
		$config[CONFIG_ALLOWED_TYPES] = 'xlsx';
		$config[CONFIG_MAX_SIZE] = '5000';
		$this->load->library('upload');
		$this->upload->initialize($config);

		$txt_file = "txt_upl_stock_opname_tahunan";
		if ($this->upload->do_upload($txt_file)) {
			$upload_data = $this->upload->data();
			$filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

			$message = $this->update_db($this::$TABLE_STOCK_OPNAME,$filename);
			if (isset(MESSAGE[$message]))
				echo MESSAGE[$message];
			else
				echo $message;

			unlink($filename);
		} else {
			$data = $this->upload->display_errors();
			echo $this->get_message($data);
		}
	}
	public function update_db($table,$filename){
		$objPHPExcel = PHPExcel_IOFactory::load($filename);
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

		$row_header = 1;
		if($this->equals($table,$this::$TABLE_STOCK_OPNAME))
			$row_header=2;

		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
			$col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

			$cell = $objPHPExcel->getActiveSheet()->getCell($cell);

			if (PHPExcel_Shared_Date::isDateTime($cell)) {
				$cellValue = $cell->getValue();
				$dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
				$data_value =  date('Y-m-d', $dateValue);
			}
			//The header will/should be in row 1 only. of course, this can be modified to suit your need.
			if ($row == $row_header) {
				$header[$row][$col] = $data_value;
			} else {
				$arr_data[$row][$col] = $data_value;
			}
		}

		$data = NULL;
		//send the data in an array format
		if (isset($header) && isset($arr_data)) {
			$data['header'] = $header;
			$data['values'] = $arr_data;
		}
		if($this->equals($table,Material_RM_PM_Stock::$TABLE_NAME))
			return $this->rmpm_model->stock_update_batch($data);
		else if($this->equals($table, Material_RM_PM::$TABLE_NAME))
			return $this->rmpm_model->update_batch($data);
		else if($this->equals($table, $this::$TABLE_STOCK_OPNAME))
			return $this->rmpm_model->stock_opname_update_batch($data);
		return -1;
	}

	public function plus(){
		$ctr = $this->i_p("c");
		$tipe = $this->i_p("t");
		
		if(!$this->equals($tipe,RMPM_PLUS_TYPE_INPUT_PRICE)){
			$checker_rm_pm = $this->rmpmchecker_model->get(Checker_RM_PM::$TABLE_NAME, Checker_RM_PM::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array();
			$disabled = "";
			$width_1 = "width:5%";
			$width_2 = "width:8%";
			if ($this->equals($tipe, RMPM_PLUS_TYPE_OPNAME)) {
				$disabled = "disabled";
				$width_1 = "width:5%";
				$width_2 = "width:5%";
			}
			$kal = '
				<tr class="tr_detail_product_' . $ctr . '">
					<td style="width:1%" class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>
					<td style="width:2%" class="text-left font-sm f-aleo"> 
						<input style="width:100%" onkeyup="check_product(' . $ctr . ')" class="f-aleo" type="text" id="txt_kode_panggil_' . $ctr . '"/>
						<input style="width:100%" class="f-aleo" type="hidden" id="txt_id_material_' . $ctr . '"/>
					</td>
				
					<td style="width:10%" class="text-center font-sm f-aleo"> 
						<input style="border:0; width:100%" value="barang belum ditemukan!" disabled class="red-text text-center f-aleo" type="text" id="txt_barang_desc_' . $ctr . '"/>
					</td>';
			if(!$this->equals($tipe, RMPM_PLUS_TYPE_OPNAME))
					$kal.='<td style="' . $width_2 . '" id="td_vendor_' . $ctr . '" class="text-left font-sm f-aleo"></td>';
			
			$kal.= '<td style="' . $width_1 . '" class="text-right font-sm f-aleo"> 
						<input ' . $disabled . ' value=0 min=0 style="width:100%" disabled class="role-accounting role-purchasing text-right f-aleo" type="number" id="txt_stock_' . $ctr . '"/>
					</td>';
				
			if($this->equals($tipe, RMPM_PLUS_TYPE_OPNAME)){
				$kal .= '<td style="' . $width_1 . '" class="text-right font-sm f-aleo"> 
						<input placeholder=0 min=0 style="width:100%" class="text-right f-aleo" type="number" id="txt_fisik_0_' . $ctr . '"/>
					</td>';
				if(!$this->equals($checker_rm_pm[Checker_RM_PM::$JENIS],Checker_RM_PM::$S_TYPE_KEMASAN))
					$kal .= '<td style="' . $width_1 . '" class="text-right font-sm f-aleo"> 
						<input placeholder=0 min=0 style="width:100%" class="text-right f-aleo" type="number" id="txt_fisik_1_' . $ctr . '"/>
					</td>';
			}

			if(!$this->equals($tipe, RMPM_PLUS_TYPE_OPNAME))
					$kal .= '<td style="' . $width_1 . '" class="text-left font-sm f-aleo"> 
						<input onkeyup="check_adjustment_qty(' . $ctr . ')" placeholder=0 min=0 style="width:100%" class="text-right f-aleo" type="number" id="txt_qty_proses_' . $ctr . '"/>
					</td>';
			/*else
					$kal .= '<td style="' . $width_1 . '" class="text-left font-sm f-aleo"> 
						<input disabled  placeholder=0 min=0 style="width:100%" class="text-right f-aleo" type="number" id="txt_varian_' . $ctr . '"/>
					</td>';*/

			$kal .= '<td style="width:2%" class="font-sm text-center f-aleo"> 
					<button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
					X
					</button>
				</td>
			</tr>';
		}else{

			//Vendor Datalist
			$data_list = "<datalist id='list_vendor'>";
			$data_vendor = $this->vendor_model->get(Vendor::$TABLE_NAME);
			if ($data_vendor->num_rows() > 0) {
				foreach ($data_vendor->result_array() as $row_vendor) {
					$data_list .= "<option value=" . $row_vendor[Vendor::$ID] . ">" . $row_vendor[Vendor::$NAMA]. "</option>";
				}
			}
			$data_list .= "</datalist>";

			//RMPM Datalist
			$data_list .= "<datalist id='list_rmpm'>";
			$data_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME);
			if ($data_material->num_rows() > 0) {
				foreach ($data_material->result_array() as $row_material) {
					$data_list .= "<option value=" . $row_material[Material_RM_PM::$KODE_MATERIAL] . ">" . $row_material[Material_RM_PM::$DESCRIPTION] . "</option>";
				}
			}
			$data_list .= "</datalist>";

			$kal = '
				<tr class="tr_detail_product_' . $ctr . '">
					<td style="width:1%" class="text-center font-sm f-aleo">'. $data_list.' ' . ($ctr + 1) . '</td>
					<td style="width:10%" class="text-left font-sm f-aleo"> 
						<input style="width:100%" list="list_vendor" onkeyup="check_vendor(' . $ctr . ')" class="f-aleo" type="text" id="txt_kode_vendor_' . $ctr . '"/>
						<input class="f-aleo" type="hidden" id="txt_id_vendor_' . $ctr . '"/>
					</td>
				
					<td style="width:30%" class="text-center font-sm f-aleo"> 
						<input style="border:0; width:100%" value="Vendor belum ditemukan!" disabled class="red-text text-center f-aleo" type="text" id="txt_desc_vendor_' . $ctr . '"/>
					</td>
					<td style="width:10%" class="text-left font-sm f-aleo"> 
						<input style="width:100%" list="list_rmpm" onkeyup="check_product(' . $ctr . ')" class="f-aleo" type="text" id="txt_kode_panggil_' . $ctr . '"/>
						<input class="f-aleo" type="hidden" id="txt_id_material_' . $ctr . '"/>
					</td>
					<td style="width:30%" class="text-center font-sm f-aleo"> 
						<input style="border:0; width:100%" value="Material belum ditemukan!" disabled class="red-text text-center f-aleo" type="text" id="txt_desc_mat_' . $ctr . '"/>
					</td>
					<td style="width:35%" id="td_konsinyasi_' . $ctr . '" class="text-center font-sm f-aleo"> 
					</td>
					<td style="width:10%" class="text-center font-sm f-aleo"> 
						Dari: <input class="text-center black-text f-aleo" type="date" id="txt_valid_from_' . $ctr . '"/>
						Sampai: <input class="text-center black-text f-aleo" type="date" id="txt_valid_to_' . $ctr . '"/>

					</td>
					<td style="width:10%"class="text-center font-sm f-aleo"> 
						<select class="form-control f-aleo" id="dd_currency_' . $ctr . '">
							<option value="usd">USD</option>
							<option value="idr">IDR</option>
            			</select>
						<hr>
						<input class="text-center f-aleo" type="number" min=0 id="txt_price_' . $ctr . '"/>
					</td>
					';
			$kal .= '<td style="width:2%" class="font-sm text-center f-aleo"> 
					<button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
					X
					</button>
				</td>
			</tr>';
		}	
		echo $kal;
	}
	public function get(){
		$kode_panggil = $this->i_p("kp");
		$tipe = ($this->i_p("t") !== null) ? $this->i_p("t") : null;
		$temp = [];

		//if(!$this->equals($tipe, RMPM_GET_TYPE_PRICE)){
			$idx_konsinyasi = 0;
			$num_rows = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_PANGGIL, $kode_panggil)->num_rows();
			$temp["num_rows"] = $num_rows;

			if($num_rows == 0){ //Tidak Ketemu pakai kode panggil, sekarang coba cari pakai kode material
				$num_rows = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $kode_panggil)->num_rows();
				$temp["num_rows"] = $num_rows;
			}

			if ($num_rows > 0) {
				$is_konsinyasi_and_not = false;
				$qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_PANGGIL, $kode_panggil);
				if($qry_material->num_rows() == 0) { //Tidak Ketemu pakai kode panggil, sekarang coba cari pakai kode material
					$qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $kode_panggil);
				}
				if ($qry_material->num_rows() > 1) {
					foreach ($qry_material->result_array() as $row_material) {
						$is_konsinyasi_and_not = true;
						if ($tipe == null && $row_material[Material_RM_PM::$IS_KONSINYASI]) { //tipe == null artinya ambil konsinyasi
							$material = $row_material;
						} else {
							$stock_konsinyasi = $this->rmpm_model->get(Material_RM_PM_Stock::$TABLE_NAME, Material_RM_PM::$ID, $row_material[Material_RM_PM::$ID], null, null, false)->row_array();
							if ($row_material[Material_RM_PM::$IS_KONSINYASI]){
								$temp["konsinyasi_nama"][$idx_konsinyasi] = "KONSINYASI";
								$material = $qry_material->row_array();
							}else
								$temp["konsinyasi_nama"][$idx_konsinyasi] = "BUKAN KONSINYASI";

							$temp["konsinyasi_qty"][$idx_konsinyasi] = $stock_konsinyasi[Material_RM_PM_Stock::$QTY];
							$temp["konsinyasi_id"][$idx_konsinyasi] = $row_material[Material_RM_PM::$ID];
							$idx_konsinyasi++;
						}
					}
				} else {
					$material = $qry_material->row_array();
				}
				$temp[Material_RM_PM::$DESCRIPTION] = $material[Material_RM_PM::$DESCRIPTION];
				$temp[Material_RM_PM::$ID] = $material[Material_RM_PM::$ID];
				$temp[Material_RM_PM::$KODE_MATERIAL] = $material[Material_RM_PM::$KODE_MATERIAL];
				$qry_stock = $this->rmpm_model->get(Material_RM_PM_Stock::$TABLE_NAME, Material_RM_PM::$ID, $material[Material_RM_PM::$ID], null, null, false);

				$stock = 0;
				$idx_vendor = 0;

				if ($qry_stock->num_rows() > 1) { //jika material rm pm (konsinyasi dan tidak konsinyasi) ada 2 stock atau lebih berarti harus ada pilihan vendor
					$stock = $qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
					
					if (!$is_konsinyasi_and_not) { //jika material tsb bukan punya 2 tipe (konsinyasi dan bukan)
						foreach ($qry_stock->result_array() as $row_stock) {
							$vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_stock[Material_RM_PM_Stock::$ID_VENDOR])->row_array()[Vendor::$NAMA];
							$temp["vendor_nama"][$idx_vendor] = $vendor;
							$temp["vendor_qty"][$idx_vendor] = $row_stock[Material_RM_PM_Stock::$QTY];
							$temp["vendor_id"][$idx_vendor] = $row_stock[Material_RM_PM_Stock::$ID_VENDOR];
							$idx_vendor++;
						}
					}
				} else {
					$stock = $qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
				}

				$temp["ctr_vendor"] = $idx_vendor;
				$temp["ctr_konsinyasi"] = $idx_konsinyasi;
				$temp["stock"] = $stock;
			}
		/*}else if($this->equals($tipe, RMPM_GET_TYPE_PRICE)){
			$qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $kode_panggil);
			$temp["num_rows"] = $qry_material->num_rows();
			
			if ($qry_material->num_rows() > 0) {
				$material=$qry_material->row_array();
				$temp[Material_RM_PM::$ID] = $material[Material_RM_PM::$ID];
				$temp[Material_RM_PM::$DESCRIPTION] = $material[Material_RM_PM::$DESCRIPTION];
			}
			
		}*/
		/* old
		if ($this->equals($tipe, RMPM_GET_TYPE_ADJUSTMENT)) {
			$temp["is_semi_product"]=false;
			$temp["stock_proses"] = 0;
			$nomor_ws = $this->i_p("n");
			//cek apakah WS Single atau bukan
			$data_ws_link = $this->ws_model->get(Worksheet_Link::$TABLE_NAME, Worksheet_Link::$NO_SEMI, $nomor_ws);
			if($data_ws_link->num_rows()==0){
				$data_ws = $this->ws_model->get(Worksheet::$TABLE_NAME,Worksheet::$ID,$nomor_ws);
				if($data_ws->num_rows()>0){
					$data_bom_pv=$this->bom_model->get_by_id_bom_pv($data_ws->row_array()[Worksheet::$ID_PV]);
					//cek lagi apakah produk yang dipanggil adalah aslinya, kalau iya baru jalankan ini
					if ($data_bom_pv->num_rows() > 0) {
						$temp["is_semi_product"] = true;

						$total = $this->ws_model->checker_detail_get_qty_by_nows_nourut($nomor_ws)->row_array()["qty_total"];
						$temp["stock_proses"] = $total;
					}
				}
			}
		}
		*/
		if ($this->equals($tipe, RMPM_GET_TYPE_GR_SEMI)|| $this->equals($tipe, RMPM_GET_TYPE_ADJUSTMENT)) {
			$temp["is_semi_product"] = false;
			$temp["stock_proses"] = 0;
			$nomor_ws = $this->i_p("n");
			//cek apakah WS Single atau bukan
			if($this->ws_model->is_semi_single($nomor_ws)){

				//cek jika tipe GR Semi, produk yang dipanggil harus sama dengan bom
				$data_ws=$this->ws_model->get(Worksheet::$TABLE_NAME,Worksheet::$ID,$nomor_ws)->row_array();
				$data_bom_pv=$this->ws_model->get(BOM_Production_Version::$TABLE_NAME,BOM_Production_Version::$ID,$data_ws[Worksheet::$ID_PV],null,null,false)->row_array();
				$product_code=$data_bom_pv[BOM_Production_Version::$PRODUCT_CODE];
				if($this->equals($tipe, RMPM_GET_TYPE_GR_SEMI) && isset($temp[Material_RM_PM::$KODE_MATERIAL]) && $this->equals($product_code,$temp[Material_RM_PM::$KODE_MATERIAL])){
					$temp["is_semi_product"] = true;
					//cek lagi apakah produk yang dipanggil adalah aslinya, kalau iya baru jalankan ini
					$total = $this->db->query("select sum(worksheet_checker_detail.qty) as total from worksheet_checker_detail,worksheet_checker_header where 
											worksheet_checker_detail.id_worksheet_checker_header=worksheet_checker_header.id_worksheet_checker_header and nomor_worksheet='" . $nomor_ws . "'")->row_array()["total"];
					$temp["stock_proses"] = $total;
				}else if($this->equals($tipe, RMPM_GET_TYPE_ADJUSTMENT) && isset($temp[Material_RM_PM::$KODE_MATERIAL]) && $this->equals($product_code,$temp[Material_RM_PM::$KODE_MATERIAL])){
					$temp["num_rows"] = 0;

				}
				
			}
		}
		echo json_encode($temp);
	}
	public function get_all(){
		$data = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME);
		$temp = [];
		$ctr = 0;
		foreach ($data->result_array() as $row) {
			$temp[$ctr]["id"] = $row[Material_RM_PM::$ID];
			$temp[$ctr]["description"] = $row[Material_RM_PM::$KODE_PANGGIL] . " (" . $row[Material_RM_PM::$DESCRIPTION] . ")";
			$ctr++;
		}
		echo json_encode($temp);
	}

	public function get_view_stock_opname(){
		$jenis = $this->i_p('j');
		$result = [];
		$result["button_view"] = "";
		$kal_button = "";
		$kal_content = "";
		if ($this->equals($jenis, STOCK_OPNAME_TYPE_HARIAN)) {
			$kal_button .= '<button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
							Tambah
						</button>
						<button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="insert_fisik_stock()" data-mdb-ripple-color="dark">
							Generate
						</button>';
			$kal_content = '<table class="tabel_detail_product" style="visibility:hidden">
									<tr>
										<td class="text-center font-sm f-aleo">Nomor</td>
										<td class="text-center font-sm f-aleo">Kode Panggil</td>
										<td class="text-center font-sm f-aleo">Deskripsi</td>
										<td class="text-center font-sm f-aleo">Program</td>';
			
			$checker_rm_pm=$this->rmpmchecker_model->get(Checker_RM_PM::$TABLE_NAME,Checker_RM_PM::$NIK,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array();
			if($this->equals($checker_rm_pm[Checker_RM_PM::$JENIS],Checker_RM_PM::$S_TYPE_KEMASAN))
				$kal_content .= '<td class="text-center font-sm f-aleo">Fisik (PCS)</td>';
			else{
				if($this->equals($checker_rm_pm[Checker_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_CAIRAN)){
					$kal_content .= '<td class="text-center font-sm f-aleo">Fisik (Drum)</td>';
					$kal_content .= '<td class="text-center font-sm f-aleo">Fisik (KG/Ltr)</td>';

				}else{
					$kal_content .= '<td class="text-center font-sm f-aleo">Fisik (Sak)</td>';
					$kal_content .= '<td class="text-center font-sm f-aleo">Fisik (KG)</td>';

				}


			}

			$kal_content .= '			<td class="text-center font-sm f-aleo">Aksi</td>
									</tr>
								</table>';
		} else {
			$kal_button .= '<button type="button" id="btn_generate" class="center btn btn-outline-success" data-toggle="modal" data-target="#mod_upl_stock_opname_tahunan" data-mdb-ripple-color="dark">
							Upload Stock Opname Tahunan
						</button>';

			$kal_content .= '<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">Kode Material</th>
										<th scope="col" class="text-center">Deskripsi</th>
										<th scope="col" class="text-center">Program</th>
										<th scope="col" class="text-center">Fisik</th>
										<th scope="col" class="text-center">Varian</th>
										<th scope="col" class="text-center">Value (Rp)</th>
									</tr>
								</thead><tbody>';
			$data = $this->rmpm_model->stock_get_all(true);

			for ($i = 0; $i < count($data); $i++) {
				$kal_content .= "<tr>";
				$kal_content .= "<td class='text-center'>" . $data[$i][Material_RM_PM::$KODE_MATERIAL] . "</td>";
				$kal_content .= "<td class='text-left'>" . $data[$i][Material_RM_PM::$DESCRIPTION] . "</td>";
				$kal_content .= "<td class='text-right'>" . $this->decimal($data[$i][Material_RM_PM_Fisik::$QTY_PROGRAM], 3) . "</td>";
				$kal_content .= "<td class='text-right'>" . $this->decimal($data[$i][Material_RM_PM_Fisik::$QTY_FISIK], 3) . "</td>";
				$kal_content .= "<td class='text-right'>" . $this->decimal(($data[$i][Material_RM_PM_Fisik::$QTY_FISIK] - $data[$i][Material_RM_PM_Fisik::$QTY_PROGRAM]), 3) . "</td>";
				$kal_content .= "<td class='text-right'>" . $this->rupiah(floatval($this->decimal(($data[$i][Material_RM_PM_Fisik::$QTY_FISIK] - $data[$i][Material_RM_PM_Fisik::$QTY_PROGRAM]), 3))*$data[$i][Material_RM_PM_Stock::$S_NET_PRICE]) . "</td>";
				$kal_content .= "</tr>";
			}
			$kal_content .= '</tbody>
								</table>';

			$kal_content .= "</div>";
		}
		$result["button_view"] = $kal_button;
		$result["content_view"] = $kal_content;
		echo json_encode($result);
	}
	public function get_view_history_stock_opname(){
		$dt = $this->i_p("dt");
		$st = $this->i_p("st");

		$kal = "";
		$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-10">';
				$kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Tanggal</th>
									<th scope="col" class="text-center">NIK</th>
									<th scope="col" class="text-center">Kode Material</th>
									<th scope="col" class="text-center">Deskripsi</th>
									<th scope="col" class="text-center">Fisik</th>
									<th scope="col" class="text-center">Program</th>
									<th scope="col" class="text-center">Varian</th>		
								</tr>
							</thead><tbody>';
				$data = $this->rmpmfisik_model->get_by_tanggal($dt, $st);

				$role=$this->karyawan_model->get(Karyawan::$TABLE_NAME,Karyawan::$ID,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array()[Karyawan::$ID_ROLE];
				$jenis="";
				if($this->equals($role,6)){
					$jenis=$this->rmpmchecker_model->get(Checker_RM_PM::$TABLE_NAME,Checker_RM_PM::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array()[Checker_RM_PM::$JENIS];
				}
				$arr_id_material=[];
				foreach ($data->result_array() as $row) {
					if($this->equals($jenis,"") ||(!$this->equals($jenis,"") && $this->equals($jenis,$row[Material_RM_PM::$JENIS]))){
						$color="";
						if($row[Material_RM_PM_Fisik::$QTY_FISIK]!=null && $row[Material_RM_PM_Fisik::$QTY_PROGRAM]!=null){
							$color="green-text";
							$batas=0;
							if(!$this->equals($row[Material_RM_PM::$JENIS],"KEMASAN")){

								if((0.1 * $row[Material_Fisik::$QTY_FISIK]) < (0.1 * $row[Material_RM_PM::$BOX]))
									$batas = (0.1 * $row[Material_Fisik::$QTY_FISIK]);
								else if((0.1*$row[Material_Fisik::$QTY_FISIK]) > (0.1 * $row[Material_RM_PM::$BOX]))
									$batas = (0.1 * $row[Material_RM_PM::$BOX]);

							}if(abs($row[Material_RM_PM_Fisik::$QTY_FISIK]-$row[Material_RM_PM_Fisik::$QTY_PROGRAM])>$batas)
								$color="red-text";
						}
						if(!in_array($row[Material_RM_PM::$ID],$arr_id_material)){
							//array_push($arr_id_material,$row[Material_RM_PM::$ID]);
							$kal .= "<tr class='$color'>";
								//$kal.= "<td class='text-center'>".$row->id_material_fisik."</td>";
								if($row[Material_RM_PM_Fisik::$TANGGAL]==null)
									$kal .= "<td class='text-center'> </td>";
								else
									$kal .= "<td class='text-center'>" . date("Y-m-d H:i:s", strtotime($row[Material_RM_PM_Fisik::$TANGGAL])) . "</td>";
								$kal .= "<td class='blue-text text-center' title='" . $row[Karyawan::$NAMA] . "'>" . $row[Material_RM_PM_Fisik::$NIK] . "</td>";
								$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";
								$kal .= "<td class='text-left'>" . $row[Material_RM_PM::$DESCRIPTION] . "</td>";
								$kal .= "<td class='text-right'>" . $this->decimal($row[Material_RM_PM_Fisik::$QTY_FISIK],3) . "</td>";
								$kal .= "<td class='text-right'>" . $this->decimal($row[Material_RM_PM_Fisik::$QTY_PROGRAM],3) . "</td>";
								$kal .= "<td class='text-right'>" . $this->decimal(($row[Material_RM_PM_Fisik::$QTY_FISIK] - $row[Material_RM_PM_Fisik::$QTY_PROGRAM]),3) . "</td>";
							$kal .= "</tr>";
						}
					}
				}
				$kal .= '	</tbody>
						</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-1"></div>';
		$kal .= "</div>";
		echo $kal;
	}
	public function get_view_history_stock(){
		$dt = $this->i_p("dt");
		$st = $this->i_p("st");
		$n = $this->i_p("n");
		$m = $this->i_p("m");

		$kal = "";
		$kal .= '<div class="row">';
			$kal .= '<div class="col-sm-12">';
				$kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
							<thead>
								<tr class="table-primary align-content-end">
									<th scope="col" class="align-middle text-center">Nomor</th>
									<th scope="col" class="align-middle text-center">Tanggal</th>
									<th scope="col" class="align-middle text-center">Material</th>
									<th scope="col" class="align-middle role-ppic role-accounting role-purchasing text-center">Vendor</th>
									<th scope="col" class="align-middle text-center">Nomor SJ</th>
									<th scope="col" class="align-middle text-center">Stock Awal</th>
									<th scope="col" class="align-middle text-center role-accounting role-purchasing">V. Awal (Rp)</th>
									<th scope="col" class="align-middle text-center">Debit</th>
									<th scope="col" class="align-middle text-center">Kredit</th>
									<th scope="col" class="align-middle text-center">Stock Akhir</th>
									<th scope="col" class="align-middle text-center role-accounting role-purchasing">V. Akhir (Rp)</th>
									<th scope="col" class="align-middle text-center">Pesan</th>		
								</tr>
							</thead><tbody>';
				$data = $this->rmpm_model->stock_get_history_by($dt, $st, $n, $m);

				foreach ($data->result_array() as $row) {
					if ($row[Stock_Gudang_RM_PM::$DEBIT] == 0)
						$kal .= "<tr class='red-text'>";
					else
						$kal .= "<tr class='green-text'>";

						$kal .= "<td class='align-middle text-center'>" . $row[Stock_Gudang_RM_PM::$ID] . "</td>";
						$kal .= "<td style='width:10%' class='align-middle text-center'>" . date("Y-m-d", strtotime($row[Stock_Gudang_RM_PM::$TANGGAL])) . "</td>";
						$kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$KODE_PANGGIL] . "</td>";
						if ($row[Stock_Gudang_RM_PM::$ID_VENDOR] != null) {
							$vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row[Stock_Gudang_RM_PM::$ID_VENDOR])->row_array()[Vendor::$NAMA];
							$kal .= "<td class='align-middle text-center role-accounting role-ppic role-purchasing'>" . $vendor . "</td>";
						} else {
							$kal .= "<td class='align-middle text-center role-accounting role-ppic role-purchasing'> - </td>";
						}
						$kal .= "<td class='align-middle text-center'>" . $row[Stock_Gudang_RM_PM::$NOMOR_SJ] . "</td>";
						$kal .= "<td class='align-middle text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$STOCK_AWAL], 3) . "</td>";
						$kal .= "<td class='align-middle text-right role-accounting role-purchasing'>" . $this->rupiah($row[Stock_Gudang_RM_PM::$VALUE_AWAL]) . "</td>";
						$kal .= "<td class='align-middle text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$DEBIT], 3) . "</td>";
						$kal .= "<td class='align-middle text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$KREDIT], 3) . "</td>";
						$kal .= "<td class='align-middle text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$STOCK_AKHIR], 3) . "</td>";
						$kal .= "<td class='align-middle text-right role-accounting role-purchasing'>" . $this->rupiah($row[Stock_Gudang_RM_PM::$VALUE_AKHIR]) . "</td>";

						$kal .= "<td>" . $row[Stock_Gudang_RM_PM::$PESAN] . "</td>";
					$kal .= "</tr>";
				}
				$kal .= '	</tbody>
						</table>';
			$kal .= "</div>";
		$kal .= "</div>";
		echo $kal;
	}
	public function get_view_stock(){
		$kal = "";
		$data = $this->rmpm_model->stock_get_all();

		$kal .= '<div class="row" style="margin-right:1%;margin-left:1%">';
			//$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-12">';
				$kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="align-middle text-center">Kode Material</th>
									<th scope="col" class="align-middle text-center">Deskripsi</th>
									<th scope="col" class="align-middle text-center">Vendor</th>
									<th scope="col" class="align-middle text-center">Stock</th>
									<th scope="col" class="align-middle role-purchasing role-accounting text-center">Value</th>
									<th scope="col" class="align-middle role-purchasing role-accounting text-center">Value per Unit</th>
								</tr>
							</thead><tbody>';
				foreach ($data->result_array() as $row_material) {
					$kal .= "<tr>";
						$kal .= "<td class='text-center'>" . $row_material[Material_RM_PM::$KODE_MATERIAL] . "</td>";
						$kal .= "<td class='text-left'>" . $row_material[Material_RM_PM::$DESCRIPTION] . "</td>";
						if ($row_material[Material_RM_PM_Stock::$ID_VENDOR] != null) {
							$vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_material[Material_RM_PM_Stock::$ID_VENDOR])->row_array();
							$kal .= "<td class='text-left'>" . $vendor[Vendor::$NAMA] . "</td>";
						} else {
							$kal .= "<td class='text-center'> - </td>";
						}
						$kal .= "<td class='text-right'>" . $this->decimal($row_material[Material_RM_PM_Stock::$QTY], 3) . "</td>";
						$kal .= "<td class='text-right role-purchasing  role-accounting'>" . $this->rupiah($row_material[Material_RM_PM_Stock::$VALUE]) . "</td>";
						if ($row_material[Material_RM_PM_Stock::$QTY] > 0)
							$kal .= "<td class='text-right role-purchasing  role-accounting'>" . $this->rupiah($row_material[Material_RM_PM_Stock::$VALUE] / $row_material[Material_RM_PM_Stock::$QTY], 3) . "</td>";
						else
							$kal .= "<td class='text-right role-purchasing  role-accounting'>0</td>";

					$kal .= "</tr>";
				}
				$kal .= '	</tbody>
						</table>';
			$kal .= "</div>";
			//$kal .= '<div class="col-sm-1"></div>';
		$kal .= "</div>";
		echo $kal;
	}
	public function get_view_rekap_po_ws(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $nomor_po_ws = $this->i_p("n");
        $mat = $this->i_p("m");
        $header = "";
        $query = "";

        $header = '<th scope="col" class="text-center">Tanggal PO / WS</th>
					<th scope="col" class="text-center">Kode GR/GI</th>
					<th scope="col" class="text-center">NIK</th>
					<th scope="col" class="text-center">Nomor PO / WS</th>
					<th scope="col" class="text-center">Vendor</th>
					<th scope="col" class="text-center">Kode Material</th>
					<th scope="col" class="text-center">Kode Panggil</th>
					<th scope="col" class="text-center">Unit</th>
					<th scope="col" class="text-center">Qty</th>';
        $query = $this->po_model->get_rekap($tanggal_dari, $tanggal_sampai, $nomor_po_ws, $mat);

        $kal = "";
        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
						<thead>
							<tr class="table-primary">
								' . $header . '
							</tr>
						</thead><tbody>';
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center align-middle green-text'>" . date('Y-m-d', strtotime($row[Purchase_Order_Checker_Header::$TANGGAL])) . "</td>";
                $kal .= "<td class='text-center align-middle green-text'>" . $row[Purchase_Order_Checker_Header::$ID] . "</td>";

                $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[Purchase_Order_Checker_Header::$NIK])->row_array();
                $kal .= "<td class='text-center align-middle blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row[Purchase_Order_Checker_Header::$NIK] . "</td>";
                $kal .= "<td class='text-center align-middle green-text'>" . $row[Purchase_Order_Checker_Header::$NOMOR_PO] . "</td>";
                $kal .= "<td class='text-left align-middle green-text'>" . $row[Vendor::$NAMA] . "</td>";

				$material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row[Purchase_Order_Checker_Detail::$ID_MATERIAL])->row_array();
				$kal .= "<td class='text-center align-middle green-text'>" . $material[Material_RM_PM::$KODE_MATERIAL] . "</td>";
				$kal .= "<td class='text-center align-middle green-text'>" . $row[Material_RM_PM::$KODE_PANGGIL] . "</td>";
				$kal .= "<td class='text-center align-middle green-text'>" . $material[Material_RM_PM::$UNIT] . "</td>";
                $kal .= "<td class='text-right align-middle green-text'>" . $this->decimal($row[Purchase_Order_Checker_Detail::$S_TOT_QTY],3) . "</td>";

                $kal .= "</tr>";
            }
        }

        $query = $this->ws_model->get_rekap($tanggal_dari, $tanggal_sampai, $nomor_po_ws, $mat);
		$text_color="";
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
				$row[Worksheet_Checker_Detail::$S_TOT_QTY]*=-1;
				if($row[Worksheet_Checker_Detail::$S_TOT_QTY]>0) //Barang Masuk
					$text_color = "green-text";
				else
					$text_color = "red-text";

                $kal .= "<tr>";
                $kal .= "<td class='text-center align-middle $text_color'>" . date('Y-m-d', strtotime($row[Worksheet_Checker_Header::$TANGGAL])) . "</td>";
                $kal .= "<td class='text-center align-middle $text_color'>" . $row[Worksheet_Checker_Header::$ID] . "</td>";

                $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[Worksheet_Checker_Header::$NIK])->row_array();
                $kal .= "<td class='text-center align-middle blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row[Worksheet_Checker_Header::$NIK] . "</td>";
                $kal .= "<td class='text-center align-middle $text_color'>" . $row[Worksheet_Checker_Header::$NOMOR_WS] . "</td>";
				
				$vendor="-";
				if($row[Worksheet_Checker_Detail::$ID_VENDOR]!=null)
					$vendor=$this->vendor_model->get(Vendor::$TABLE_NAME,Vendor::$ID,$row[Worksheet_Checker_Detail::$ID_VENDOR])->row_array()[Vendor::$NAMA];
				
				$kal .= "<td class='text-left align-middle $text_color'>" . $vendor . "</td>";

                $material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row[Worksheet_Checker_Detail::$ID_MATERIAL])->row_array();
				$kal .= "<td class='text-center align-middle $text_color'>" . $material[Material_RM_PM::$KODE_MATERIAL] . "</td>";
				$kal .= "<td class='text-center align-middle $text_color'>" . $row[Material_RM_PM::$KODE_PANGGIL] . "</td>";
				$kal .= "<td class='text-center align-middle $text_color'>" . $material[Material_RM_PM::$UNIT] . "</td>";
				$kal .= "<td class='text-right align-middle $text_color'>" . $this->decimal($row[Worksheet_Checker_Detail::$S_TOT_QTY],3) . "</td>";


            }
        }
        $kal .= '	</tbody>
					</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }

	//Untuk Cek GI / GR
	public function document_get_view_by_id(){
		$i = $this->i_p("i");
		//Cek dulu apakah GR
		$qry = $this->rmpm_model->get(Purchase_Order_Checker_Header::$TABLE_NAME,Purchase_Order_Checker_Header::$ID,$i);
		if($qry->num_rows()>0){ //Adalah GR
			$po=$qry->row_array();
			$karyawan=$this->karyawan_model->get(Karyawan::$TABLE_NAME,Karyawan::$ID,$po[Purchase_Order_Checker_Header::$NIK])->row_array();
			$data["message"] = MESSAGE[Document::$MESSAGE_SUCCESS_SEARCH];
			
			$po_awal=$this->po_model->get(Purchase_Order_Awal::$TABLE_NAME, Purchase_Order_Awal::$NOMOR_PO,$po[Purchase_Order_Checker_Header::$NOMOR_PO],null,null,false)->row_array();
			if(isset($po_awal[Purchase_Order_Awal::$ID_VENDOR]))
				$vendor=$this->vendor_model->get(Vendor::$TABLE_NAME,Vendor::$ID,$po_awal[Purchase_Order_Awal::$ID_VENDOR])->row_array();
			else
				$vendor="-";
			if ($this->str_contains($po[Purchase_Order_Checker_Header::$ID], "ADJ")) {
				$pesan = "-";
				$qry_pesan = $this->rmpm_model->stock_get_by_pesan($po[Purchase_Order_Checker_Header::$ID]);
				if ($qry_pesan->num_rows() > 0)
					$pesan = rtrim(explode("Pesan:", $qry_pesan->row_array()[Stock_Gudang_RM_PM::$PESAN])[1], ")");
				$data["title"] = "MUTASI PO<br>
							Oleh: " . $karyawan[Karyawan::$NAMA] . "<br>
							Tanggal: " . date("d F Y", strtotime($po[Purchase_Order_Checker_Header::$TANGGAL]))."<br>".
							"Pesan: ". $pesan;
			} else {
				$data["title"]="PO ". $po[Purchase_Order_Checker_Header::$NOMOR_PO]." (Nomor SJ: ". $po[Purchase_Order_Checker_Header::$NOMOR_SJ].")<br>
							Vendor: ". $vendor[Vendor::$NAMA] ."<br>
							Oleh: ". $karyawan[Karyawan::$NAMA]."<br>Tanggal: ". date("d F Y",strtotime($po[Purchase_Order_Checker_Header::$TANGGAL]));
			}
		}else{
			$qry = $this->rmpm_model->get(Worksheet_Checker_Header::$TABLE_NAME, Worksheet_Checker_Header::$ID, $i);
			if ($qry->num_rows() > 0) { //Adalah GI
				$ws = $qry->row_array();
				$karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $ws[Worksheet_Checker_Header::$NIK])->row_array();
				$data["message"] = MESSAGE[Document::$MESSAGE_SUCCESS_SEARCH];

				if($this->str_contains($ws[Worksheet_Checker_Header::$ID],"ADJ")){
					$pesan="-";
					$qry_pesan=$this->rmpm_model->stock_get_by_pesan($ws[Worksheet_Checker_Header::$ID]);
					if($qry_pesan->num_rows()>0)
						$pesan=rtrim(explode("Pesan:",$qry_pesan->row_array()[Stock_Gudang_RM_PM::$PESAN])[1],")");
					$data["title"] = "MUTASI WS<br>
							Oleh: " . $karyawan[Karyawan::$NAMA] . "<br>Tanggal: " . date("d F Y", strtotime($ws[Worksheet_Checker_Header::$TANGGAL]))."<br>".
							"Pesan: ". $pesan;
				}else{
					$data["title"] = "WS " . $ws[Worksheet_Checker_Header::$NOMOR_WS] . "<br>
							Oleh: " . $karyawan[Karyawan::$NAMA] . "<br>Tanggal: " . date("d F Y", strtotime($ws[Worksheet_Checker_Header::$TANGGAL]));
				}
			} else {
				$data["message"] = MESSAGE[Document::$MESSAGE_FAILED_SEARCH_NOT_FOUND];
			}
		}

		$kal = "";
		$kal .= '<div class="row">';
		$kal .= '<div class="col-sm-3"></div>';
		$kal .= '<div class="col-sm-6">';
		if (isset($po)) {
			$kal .= '<table id="mastertable" class="table table-bordered table-sm">
						<thead>
							<tr class="table-primary">
								<th scope="col" class="text-center">No Urut</th>
								<th scope="col" class="text-center">Material</th>
								<th scope="col" class="text-center">Deskripsi</th>
								<th scope="col" class="text-center">Qty</th>
							</tr>
						</thead><tbody>';
		}
		if (isset($ws)) {
			$kal .= '<table id="mastertable" class="table table-bordered table-sm">
						<thead>
							<tr class="table-primary">
								<th scope="col" class="text-center">Material</th>
								<th scope="col" class="text-center">Qty</th>
							</tr>
						</thead><tbody>';
		}
		if($qry->num_rows()>0){
			if(isset($po)){
				$qry_detail=$this->po_model->get(Purchase_Order_Checker_Detail::$TABLE_NAME,Purchase_Order_Checker_Detail::$ID_HEADER,$po[Purchase_Order_Checker_Header::$ID],null,null,false);
			}
			if(isset($ws)){
				$qry_detail=$this->ws_model->get(Worksheet_Checker_Detail::$TABLE_NAME, Worksheet_Checker_Detail::$ID_HEADER,$ws[Worksheet_Checker_Header::$ID],null,null,false);
			}
			if($qry_detail->num_rows()>0){
				foreach ($qry_detail->result_array() as $row_detail) {
					$kal.="<tr class='f-aleo'>";
						if (isset($po)) {
							$material=$this->rmpm_model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$ID,$row_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL])->row_array();
							$awal=$this->po_model->awal_get_by_id_material($po[Purchase_Order_Checker_Header::$NOMOR_PO],$material[Material_RM_PM::$ID]);
							$kal .= "<td class='text-center'>" . $row_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT] . "</td>";
							$kal .= "<td class='text-center'>" . $material[Material_RM_PM::$KODE_MATERIAL] . "</td>";
							$kal .= "<td class='text-left'>" . $awal->row_array()[Purchase_Order_Awal::$SHORT_TEXT] . "</td>";
							$kal .= "<td class='text-right'>" . $this->decimal($row_detail[Purchase_Order_Checker_Detail::$QTY],3) . "</td>";
						}
						if(isset($ws)){
							$material=$this->rmpm_model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$ID,$row_detail[Worksheet_Checker_Detail::$ID_MATERIAL])->row_array();
							$kal .= "<td class='text-center'>" . $material[Material_RM_PM::$KODE_MATERIAL] . "</td>";
							$kal .= "<td class='text-right'>" . $this->decimal($row_detail[Worksheet_Checker_Detail::$QTY],3) . "</td>";
						}
					$kal.="</tr>";
				}
			}
		}
		$kal .= '	</tbody>
						 </table>';
		$kal .= "</div>";
		$kal .= '<div class="col-sm-3"></div>';
		$kal .= "</div>";

		$data["content"]=$kal;
		echo json_encode($data);
	}
	public function insert_gr_mutasi(){
		$this->insert($this::$NAV_RM_PM_GOOD_RECEIPT_MUTASI);
	}
	public function insert_gi_mutasi(){
		$this->insert($this::$NAV_RM_PM_GOOD_ISSUE_MUTASI);
	}
	public function insert_fisik_stock(){
		$this->insert(Material_RM_PM_Fisik::$TABLE_NAME);
	}
}
