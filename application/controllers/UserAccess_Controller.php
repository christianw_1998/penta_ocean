<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class UserAccess_Controller extends Library_Controller {

	public function get_all_no_restriction(){
		$query = $this->useraccess_model->get_all_no_restriction();
		if ($query['status'] == 1) {
			$kal = "";
			$kal .= '<div class="row" style="margin-right:1%">';
				$kal .= '<div class="col-sm-1"></div>';
				$kal .= '<div class="col-sm-10">';
					$kal .= '<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">NIK Bawahan</th>
										<th scope="col" class="text-center">NIK Atasan</th>
										<th scope="col" class="text-center">Tipe Akses</th>
										<th scope="col" class="text-center">Status</th>
										<th scope="col" class="text-center">Aksi</th>
									</tr>
								</thead><tbody>';
					foreach ($query['data']->result() as $row) {
						$class = "";
						$text = "";
						if ($row->is_deleted == 1) {
							$class = 'red-text';
							$text = 'DIHAPUS';
						} else {
							$class = 'green-text';
							$text = 'AKTIF';
						}
						$kal .= "<tr>";
							$nama = $this->karyawan_model->get_by(Karyawan::$ID, $row->nik_bawahan)->row()->k_nama;
							$kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row->nik_bawahan . "</td>";
							$nama = $this->karyawan_model->get_by(Karyawan::$ID, $row->nik_atasan)->row()->k_nama;
							$kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row->nik_atasan . "</td>";
							$kal .= "<td class='text-center'>" . $row->description . "</td>";
							$kal .= "<td class='text-center $class'>" . $text . "</td>";
							$kal .= '<td class="text-center"> 
										<button type="button" class="btn btn-outline-primary" onclick=set_user_access(' . $row->nik_bawahan . ',' . $row->id_access . ',' . $row->nik_atasan . ',"' . "while_update" . '") data-mdb-ripple-color="dark">
											Ubah
										</button>
									 </td>';
						$kal .= "</tr>";
					}
					$kal .= '	</tbody>
							</table>';
				$kal .= "</div>";
				$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
			echo $kal;
		} else {
			echo MESSAGE[$query['data']];
		}
	}
	public function get(){
		$qry=$this->useraccess_model->get_all();
		
		if($qry['status']==1){
			$kal="";
			$kal.="<div class='row'>";
				$kal.="<div class='col-sm-2'></div>";
				$kal.="<div class='col-sm-8'>";
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">NIK</th>
									<th scope="col" class="text-center">Nama</th>
									<th scope="col" class="text-center">Akses</th>
									<th scope="col" class="text-center">Deskripsi</th>
									<th scope="col" class="text-center">Status</th>
									<th scope="col" class="text-center">Aksi</th>';
					$kal.='		</tr>
							</thead><tbody>';
						foreach($qry['data']->result_array() as $row){
							$kal.="<tr>";
								$kal.="<td>".$row[Karyawan::$ID]."</td>"; 
								$kal.="<td>".$row[Karyawan::$NAMA]."</td>"; 
								$kal.="<td>".$row[List_Access::$ACCESS]."</td>";
								$kal.="<td>".$row[List_Access::$DESCRIPTION]."</td>";
								if($row[User_Access::$IS_TRUE]==1){
									$kal.="<td class='green-text'>DIBERIKAN</td>";
									$kal.="<td><button type='button' class='center btn btn-outline-danger' data-mdb-ripple-color='dark' onclick='update_role_access(".$row[Karyawan::$ID].",".$row[List_Access::$ID].",".($row[User_Access::$IS_TRUE]*-1+1).")'>TOLAK</button></td>";
								}else{
									$kal.="<td class='red-text'>TIDAK DIBERIKAN</td>";
									$kal.="<td><button type='button' class='center btn btn-outline-success' data-mdb-ripple-color='dark' onclick='update_role_access(".$row[Karyawan::$ID].",".$row[List_Access::$ID].",".($row[User_Access::$IS_TRUE]*-1+1).")'>IJINKAN</button></td>";
								}
							$kal.="</tr>";
							
						}
					$kal.="	</tbody>
						</table>";
				$kal.="</div>";
				$kal.="<div class='col-sm-2'></div>";
			$kal.="</div>";

			echo $kal;
		}else{
			echo MESSAGE[$qry['data']];
		}
	}
	public function change_status(){
		$this->insert($this::$NAV_USER_ACCESS_NO_RESTRICTION);
	}
	public function insert_form(){
		$this->insert_f($this::$NAV_USER_ACCESS_NO_RESTRICTION);
	}

	public function insert_ua(){
		$this->insert($this::$NAV_USER_ACCESS);
	}
}
