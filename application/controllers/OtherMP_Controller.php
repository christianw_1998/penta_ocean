<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class OtherMP_Controller extends Library_Controller {

    public function get_all(){
        $data = $this->other_model->get(Other_Metode_Penyusutan::$TABLE_NAME);
        $temp = [];
        $ctr = 0;
        foreach ($data->result_array() as $row) {
            $temp[$ctr][Other_Metode_Penyusutan::$ID] = $row[Other_Metode_Penyusutan::$ID];
            $temp[$ctr][Other_Metode_Penyusutan::$MASA_MANFAAT] = $row[Other_Metode_Penyusutan::$MASA_MANFAAT];
            $temp[$ctr][Other_Metode_Penyusutan::$METODE] = $row[Other_Metode_Penyusutan::$METODE];
            $temp[$ctr][Other_Metode_Penyusutan::$TARIF] = $row[Other_Metode_Penyusutan::$TARIF];
            $ctr++;
        }
        echo json_encode($temp);
    }
}
