<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class FOH_Controller extends Library_Controller {
    
    public static $STATIC_ARR_SETTING=[51401001, 51401002, 51401004, 51401005, 51401006, 51401007,
                                        51402001, 51501001, 51501002, 51501003, 51503001, 51503002,
                                        51505000, 51506001, 51510001, 51510002
    ];
    public function view_insert(){
        $kal = "";
        $ctr = 0;

        //GL Datalist
        $data_list = "<datalist id='list_gl'>";
        $data_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, null, null, null, null, false);
        if ($data_gl->num_rows() > 0) {
            foreach ($data_gl->result_array() as $row_gl) {
                $data_list .= "<option value=" . $row_gl[General_Ledger::$ID] . ">" . $row_gl[General_Ledger::$DESKRIPSI] . "</option>";
            }
        }
        $data_list .= "</datalist>";

        $qry_gl = $this->gl_model->get_by_arr($this::$STATIC_ARR_SETTING);
        if ($qry_gl->num_rows() > 0) {
            foreach($qry_gl->result_array() as $row_gl){
                $kal .= '
                    <tr class="tr_detail_gl_' . $ctr . '">
                        <td class="text-center font-sm">' . $data_list . ($ctr + 1) . '</td>
                        <td class="text-center font-sm" style="width:35%"> 
                            <input style="width:50%" list="list_gl" onkeyup="check_gl(' . $ctr . ')" id="txt_id_gl_' . $ctr . '" value="'. $row_gl[General_Ledger::$ID] . '"/>
                        </td>
                    
                        <td class="text-center font-sm" style="width:50%"> 
                            <input style="width:100%" class="gl green-text" disabled id="txt_desc_gl_' . $ctr . '" value="'. $row_gl[General_Ledger::$DESKRIPSI] . '"/>
                        </td>
                        <td class="text-center font-sm" style="width:20%">                     
                            <input class="text-right" style="width:80%" type="number" onkeyup="calculate_total()"placeholder=0 id="txt_harga_' . $ctr . '"/>
                        </td>
                        <td style="width:2%" class="font-sm text-center"> 
                            <button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_gl(' . $ctr . ')" data-mdb-ripple-color="dark">
                            X
                            </button>
                        </td>
                    </tr>';
                $ctr++;
            }
        }
        $data=[];
        $data["html"] = $kal;
        $data["ctr"] = $ctr;
        $data["btn"] = '';
        echo json_encode($data);
    }
    public function view_update_get_by_id(){
        $kal = "";
        $ctr = 0;
        $id = $this->i_p("i");
        $mode = $this->i_p("m");
        $disabled = "";
        $data = [];

        if(!$this->equals($mode,"update")){ //validasi
            $disabled = "disabled";
        }
        //Cek apakah tervalidasi atau belum
        $qry_foh = $this->foh_model->get(FOH::$TABLE_NAME, FOH::$ID, $id, null, null, false);
        if ($qry_foh->num_rows() > 0) {
            $validasi = $qry_foh->row_array()[FOH::$IS_VALIDATED];
            if ($validasi == 1) {
                $data["ctr"] = $ctr;
                echo json_encode($data);
                return;
            }
        }

        $qry_foh_gl = $this->foh_model->get(FOH_GL::$TABLE_NAME, FOH_GL::$ID_FOH, $id, null, null, false);
        if ($qry_foh_gl->num_rows() > 0) {
            foreach ($qry_foh_gl->result_array() as $row_foh_gl) {
                $deskripsi = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $row_foh_gl[FOH_GL::$ID_GL], null, null, false)->row_array()[General_Ledger::$DESKRIPSI];
                $kal .= '
                    <tr class="tr_detail_gl_' . $ctr . '">
                        <td class="text-center font-sm">' . ($ctr + 1) . '</td>
                        <td class="text-center font-sm" style="width:35%"> 
                            <input style="width:50%" list="list_gl" disabled id="txt_id_gl_' . $ctr . '" value="' . $row_foh_gl[FOH_GL::$ID_GL] . '"/>
                        </td>
                    
                        <td class="text-center font-sm" style="width:50%"> 
                            <input style="width:100%" class="gl green-text" disabled id="txt_desc_gl_' . $ctr . '" value="' . $deskripsi . '"/>
                        </td>
                        <td class="text-center font-sm" style="width:20%">                     
                            <input class="text-right" style="width:80%" '.$disabled.' type="number" value="'.  $row_foh_gl[FOH_GL::$HARGA] .'" id="txt_harga_' . $ctr . '"/>
                        </td>
                        <td style="width:2%" class="font-sm text-center"> 
                            <button style="width:90%" disabled type="button" class="text-center center btn btn-outline-danger" onclick="minus_gl(' . $ctr . ')" data-mdb-ripple-color="dark">
                            X
                            </button>
                        </td>
                    </tr>';
                $ctr++;
            }
        }
        $data["html"] = $kal;
        $data["ctr"] = $ctr;
        $data["btn"] = '';
        echo json_encode($data);
    }
    public function view_history(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $kal = "";
        $data_foh = $this->foh_model->get_history($tanggal_dari, $tanggal_sampai);

        $arr_header = [
            "ID FOH", "ID GL", "Deskripsi", "Harga","Tanggal Input", "Tanggal Update", "Tanggal Validasi"
        ];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data_foh->num_rows() > 0) {
            foreach ($data_foh->result_array() as $row_foh) {
                $kal .= "<tr>";
                    $kal .= "<td class='text-center'>" . $row_foh[FOH::$ID] . "</td>";
                    $kal .= "<td class='text-center'>" . $row_foh[FOH_GL::$ID_GL] . "</td>";
                    $kal .= "<td class='text-left'>" . $row_foh[General_Ledger::$DESKRIPSI] . "</td>";
                    $kal .= "<td class='text-right'>" . $this->rupiah($row_foh[FOH_GL::$HARGA]) . "</td>";
                if ($row_foh[FOH::$TANGGAL_INPUT] != null)
                    $kal .= "<td class='text-center'>" . date("Y-m-d", strtotime($row_foh[FOH::$TANGGAL_INPUT])) . "</td>";
                else
                    $kal .= "<td class='text-center'> - </td>";
                    
                if ($row_foh[FOH_GL::$TANGGAL_UPDATE] != null)
                    $kal .= "<td class='text-center'>" . date("Y-m-d", strtotime($row_foh[FOH_GL::$TANGGAL_UPDATE])) . "</td>";
                else
                    $kal .= "<td class='text-center'> - </td>";

                if($row_foh[FOH::$TANGGAL_VALIDASI]!=null)
                    $kal .= "<td class='text-center'>" . date("Y-m-d", strtotime($row_foh[FOH::$TANGGAL_VALIDASI])) . "</td>";
                else
                    $kal .= "<td class='text-center'> - </td>";
                
                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }

    public function foh_insert(){
        $arr_id_gl = $this->i_p("ag");
        $arr_harga = $this->i_p("ah");
        $ctr = $this->i_p("c");
        echo MESSAGE[$this->foh_model->insert(
            $ctr,
            $arr_id_gl,
            $arr_harga
        )];
    }
    public function foh_update(){
        $id_foh = $this->i_p("if");
        $arr_id_gl = $this->i_p("ag");
        $arr_harga = $this->i_p("ah");
        $ctr = $this->i_p("c");
        echo MESSAGE[$this->foh_model->update(
            $id_foh,
            $ctr,
            $arr_id_gl,
            $arr_harga
        )];
    }
    public function foh_validasi(){
        $id_foh = $this->i_p("if");
        echo MESSAGE[$this->foh_model->validasi(
            $id_foh
        )];
    }

    public function plus(){
        $ctr = $this->i_p("c");

        //GL Datalist
        $data_list = "<datalist id='list_gl'>";
        $data_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, null, null, null, null, false);
        if ($data_gl->num_rows() > 0) {
            foreach ($data_gl->result_array() as $row_gl) {
                $data_list .= "<option value=" . $row_gl[General_Ledger::$ID] . ">" . $row_gl[General_Ledger::$DESKRIPSI] . "</option>";
            }
        }
        $data_list .= "</datalist>";

        $kal = '
                <tr class="tr_detail_gl_' . $ctr . '">
                    <td class="text-center font-sm">' . $data_list . ($ctr + 1) . '</td>
                    <td class="text-center font-sm" style="width:25%"> 
                        <input style="width:50%" list="list_gl" onkeyup="check_gl(' . $ctr . ')" id="txt_id_gl_' . $ctr . '"/>
                    </td>
                
                    <td class="text-left font-sm" style="width:60%"> 
                        <input style="width:100%" class="gl red-text" value="GL tidak ditemukan" id="txt_desc_gl_' . $ctr . '" disabled>
                    </td>
                    <td class="text-center font-sm"style="width:20%">                     
                        <input class="text-right" style="width:80%" type="number" placeholder=0 id="txt_harga_' . $ctr . '"/>
                    </td>
                    <td style="width:2%" class="font-sm text-center"> 
                        <button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_gl(' . $ctr . ')" data-mdb-ripple-color="dark">
                        X
                        </button>
                    </td>
                </tr>';
        
        echo $kal;
    }

    
}

