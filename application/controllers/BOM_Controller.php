<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class BOM_Controller extends Library_Controller {
    
    public function upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_bom";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->bom_model->update_batch($data);
    }
    public function jd_qc_upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_jd_qc";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->jd_qc_update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function jd_qc_update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->bom_model->jd_qc_update_batch($data);
    }

    public function view_master(){
        $data_bom=$this->bom_model->get_master();
        $kal = "";
        if($data_bom->num_rows()>0){
            $arr_header = ["Kode Produk", "Deskripsi", "Alt BOM", "Detail"];
            $ctr = 0;
            $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';
            foreach ($data_bom->result_array() as $row_bom) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'><input type='hidden' id='txt_bom_id_" . $ctr . "' value=" . $row_bom[BOM::$ID] . ">" . $row_bom[BOM::$ID] . "</td>";
                $kal .= "<td class='text-center'>" . $row_bom[BOM::$DESCRIPTION] . "</td>";
                $kal .= "<td class='text-center'><input type='hidden' id='txt_alt_id_" . $ctr . "' value=" . $row_bom[BOM_Alt::$ID] . ">" . $row_bom[BOM_Alt::$ALT_NO] . "</td>";
                $kal .= "<td class='text-center'>
                            <button type='button' class='btn btn-outline-info' onclick='gen_detail(" . $ctr . ")' data-mdb-ripple-color='dark'>
                                Detail
                            </button></td>";
                $kal .= "</tr>";
                $ctr++;
            }
            $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= "</div>";
            echo $kal;
        }
    }
    public function view_modal_by_bom_alt(){
        $modal = "";
        $id_alt = $this->i_p("ia");
        $id_bom = $this->i_p("ib");

        $row_bom = $this->bom_model->get(BOM::$TABLE_NAME, BOM::$ID, $id_bom, null, null, false)->row_array();
        $data_recipe = $this->bom_model->get(BOM_Alt_Recipe::$TABLE_NAME, BOM_Alt_Recipe::$ID_ALT, $id_alt, null, null, false);

        $row_alt = $this->bom_model->get(BOM_Alt::$TABLE_NAME, BOM_Alt::$ID, $id_alt, null, null, false)->row_array();

        $arr_recipe = [];
        $ctr_recipe = 0;
        $total=0;
        if ($data_recipe->num_rows() > 0) {
            foreach ($data_recipe->result_array() as $row_recipe) {
                $mat = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_recipe[BOM_Alt_Recipe::$ID_MATERIAL])->row_array();
                $arr_recipe[$ctr_recipe][Material_RM_PM::$DESCRIPTION] = $mat[Material_RM_PM::$DESCRIPTION];
                $arr_recipe[$ctr_recipe][Material_RM_PM::$KODE_MATERIAL] = $mat[Material_RM_PM::$KODE_MATERIAL];
                $arr_recipe[$ctr_recipe][BOM_Alt_Recipe::$QTY] = $row_recipe[BOM_Alt_Recipe::$QTY];
                $arr_recipe[$ctr_recipe][BOM_Alt_Recipe::$NOMOR_URUT] = $row_recipe[BOM_Alt_Recipe::$NOMOR_URUT];
                if($this->equals($row_recipe[BOM_Alt_Recipe::$UNIT],Material_RM_PM::$S_UNIT_KG))
                    $total += $arr_recipe[$ctr_recipe][BOM_Alt_Recipe::$QTY];
                $ctr_recipe++;
            }
        }

        $modal .= '
                <div class="f-aleo modal fade" id="mod_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">BOM ' . strtoupper($row_bom[BOM::$ID]) . '</h5>
                            </div>
                            <div class="modal-body" style="margin:1%">
                                <div class="row">
                                    <h4 class="text-center col-sm-12 fw-bold">' . $row_bom[BOM::$DESCRIPTION] . '</h4>
                                    <h5 class="text-center col-sm-12 fw-bold">Alt BOM: ' . $row_alt[BOM_Alt::$ALT_NO] . ' </h5>
                                    <h5 class="text-center col-sm-12 fw-bold">Total: ' . $this->decimal($total) . ' KG </h5>
                                </div>
                                <hr style="padding-left:3%">
                                <div class="row">
                                    <table class="table table-bordered table-sm">
                                        <thead><tr class="text-center table-primary">
                                            <th class="fw-bold">No Urut</th>
                                            <th class="fw-bold">Kode Material</th>
                                            <th class="fw-bold">Deskripsi</th>
                                            <th class="fw-bold">Qty (KG / PCS)</th>
                                        </tr></thead>
                                        <tbody>';

        for ($i = 0; $i < $ctr_recipe; $i++) {
            $modal .= '<tr>';
                $modal .= "<td class='text-right'>" . $arr_recipe[$i][BOM_Alt_Recipe::$NOMOR_URUT] . "</td>";
                $modal .= "<td class='text-center'>" . $arr_recipe[$i][Material_RM_PM::$KODE_MATERIAL] . "</td>";
                $modal .= "<td class='text-left'>" . $arr_recipe[$i][Material_RM_PM::$DESCRIPTION] . "</td>";
                $modal .= "<td class='text-right'>" . $this->decimal($arr_recipe[$i][BOM_Alt_Recipe::$QTY],3) . "</td>";
            $modal .= '</tr>';
        }


        $modal .= '                         </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        ';
        echo $modal;
    }

    public function get_desc_by_product_code(){
        $product_code = $this->i_p("kp");
        $data=[];
        $data_bom = $this->bom_model->get(BOM::$TABLE_NAME, BOM::$ID, $product_code, null, null, false);
        $data["num_rows"] = 0;
        if($data_bom->num_rows()>0){
            $data["num_rows"] = $data_bom->num_rows();
            $data[BOM::$DESCRIPTION]=$data_bom->row_array()[BOM::$DESCRIPTION];
        }
        //Ambil Update Terakhir
        $data[BOM_Production_Version::$CHECK_DATE] = $this->pv_model->get_last_date($product_code);
        
        echo json_encode($data);
    }
    
    public function alt_get_opt(){
        $product_code = $this->i_p("kp");

        $data=[];
        $data["num_rows"] = 0;

        $data_bom_alt = $this->bom_model->get(BOM_Alt::$TABLE_NAME, BOM_Alt::$PRODUCT_CODE, $product_code, null, null, false);
        if($data_bom_alt->num_rows()>0){
            $ctr=0;
            foreach($data_bom_alt->result_array() as $row_alt){
                $data[$ctr][BOM_Alt::$ALT_NO] = $row_alt[BOM_Alt::$ALT_NO];
                $data[$ctr][BOM_Alt::$ID] = $row_alt[BOM_Alt::$ID];
                $ctr++;
            }
            $data["ctr"] = $ctr;
        }

        echo json_encode($data);
    }

    public function pv_set_all_now(){
        $this->bom_model->pv_set_all_now();
    }

    
}
