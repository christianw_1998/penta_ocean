<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class GL_Controller extends Library_Controller {
    
    public function upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_gl";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->gl_model->update_batch($data);
    }

    public function get(){
        $id = $this->i_p("i");
        $data = $this->gl_model->get(General_Ledger::$TABLE_NAME,General_Ledger::$ID,$id,null,null,false);
        $temp=[];
        $temp["num_rows"] = $data->num_rows();
        if ($data->num_rows() > 0) {
            $gl = $data->row_array();
            $temp[General_Ledger::$DESKRIPSI] = $gl[General_Ledger::$DESKRIPSI];
            $temp[General_Ledger::$ID] = $gl[General_Ledger::$ID];
                    
        }        
        echo json_encode($temp);
    }

    public function view_table(){
        $kal = "";
        $arr_header = ["Aksi","ID General Ledger", "Deskripsi"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL GENERAL LEDGER</h1>";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                        <thead>' . $this->gen_table_header($arr_header) . '</thead>
                        <tbody>';
        $data = $this->other_model->get(General_Ledger::$TABLE_NAME,null,null,null,null,false);
        foreach ($data->result_array() as $row) {
            $kal .= "<tr>";
                $kal .= "<td style='width:10%'class='align-middle text-center'><input type='checkbox' value=" . $row[General_Ledger::$ID] . " id='chk_" . $row[General_Ledger::$ID] . "'/></td>";
                $kal .= "<td class='align-middle text-center'>" . $row[General_Ledger::$ID] . "</td>";
                $kal .= "<td class='align-middle text-left'>" . $row[General_Ledger::$DESKRIPSI] . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '</tbody>
                    </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';

        $kal .= "</div>";
        echo $kal;
    }
}
