<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class OtherKategori_Controller extends Library_Controller {

    public function get_all(){
        $data = $this->other_model->get(Other_Kategori::$TABLE_NAME);
        $temp = [];
        $ctr = 0;
        foreach ($data->result_array() as $row) {
            $temp[$ctr][Other_Kategori::$ID] = $row[Other_Kategori::$ID];
            $temp[$ctr][Other_Kategori::$NAMA] = $row[Other_Kategori::$NAMA];
            $ctr++;
        }
        echo json_encode($temp);
    }
}
