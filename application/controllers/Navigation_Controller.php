<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Navigation_Controller extends Library_Controller {

	public function index(){
		$this->load->view("home");
	}
	public function master($params){
		$this->load->view("master_" . $params);
	}
	public function finished($params,$params2=null){
		$data = [];
		$data['access_revision'] = $this->model->get_access_by_user_id($_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID], ACCESS_REVISION_SURAT_JALAN);
		if(isset($params2)){
			$data[$params2]=$params2;
		}
		$this->load->view("finished_" . $params, $data);
	}
	public function rm_pm($params,$params2=null){
		$data=[];
		if (isset($params2)) {
			$data[$params2] = $params2;
		}
		$this->load->view("rm_pm_" . $params,$data);
	}
	public function setting($params){
		$this->load->view("setting_" . $params);
	}
	public function user_access(){
		$this->load->view("user_access");
	}
	public function po($params){
		$this->load->view("purchase_order_" . $params);
	}
	public function pr($params){
		$this->load->view("purchase_request_" . $params);
	}
	public function ws($params){
		$this->load->view("work_sheet_" . $params);
	}
	public function kinerja($params){
		$this->load->view("kinerja_" . $params);

	}
	public function other($params,$params2){
		$this->load->view("other_" . $params."_". $params2);
	}
	public function bom($params){
		$this->load->view("bom_" . $params);
	}
	public function pv($params){
		$this->load->view("production_version_" . $params);
	}
	public function foh($params){
		$this->load->view("foh_" . $params);
	}
	public function payment($params){
		$this->load->view("payment_" . $params);
	}
	public function payment_voucher($params,$params2=null){
		if(isset($params2))
			$this->load->view("payment_voucher_" . $params . "_" . $params2);
		else
			$this->load->view("payment_voucher_" . $params);

	}
	public function forecast($params){
		$this->load->view("forecast_" . $params);
	}
}
