<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Master_Controller extends Library_Controller {

    public function material_fg(){
        $kal="";
        $arr_header = ["FG Code (Lama)", "FG Code (Baru)", "Kode Produk", "Deskripsi", "Pack", "Box", "Net", "Gross", "Kubik", "Kode Barang", "Kms", "Warna"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL MATERIAL FG SAGE</h1>";
        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12">';
            $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                        <thead>'. $this->gen_table_header($arr_header) .'</thead>
                        <tbody>';
            $data = $this->fg_model->get(Material::$TABLE_NAME);
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                    $kal .= "<td class='align-middle text-center'>" . $row[Material::$ID] . "</td>";
                    $kal .= "<td class='align-middle text-center'>" . $row[Material::$ID_NEW] . "</td>";
                    $kal .= "<td class='align-middle text-left'>" . $row[Material::$PRODUCT_CODE] . "</td>";
                    $kal .= "<td class='align-middle text-left'>" . $row[Material::$DESCRIPTION] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material::$PACKING] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material::$BOX] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material::$NET] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material::$GROSS] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material::$KUBIK] . "</td>";
                    $kal .= "<td class='align-middle text-center'>" . $row[Material::$KODE_BARANG] . "</td>";
                    $kal .= "<td class='align-middle text-center'>" . $row[Material::$KEMASAN] . "</td>";
                    $kal .= "<td class='align-middle text-center'>" . $row[Material::$WARNA] . "</td>";
                    /*$kal.='<td class="text-center"> <button type="button" class="btn btn-outline-danger" onclick="delete_material('.$row->id_material.')" data-mdb-ripple-color="dark">
                                            Hapus
                                        </button></td>';*/
                $kal .= "</tr>";
            }
            $kal .= '</tbody>
                    </table>';
            $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function material_rm_pm(){
        $kal="";
        $arr_header = ["ID", "Jenis", "Tipe", "Konsinyasi", "Deskripsi", "Unit", "Box", "Gudang", "Kode Panggil"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL MATERIAL RM PM</h1>";
            $kal .= '<div class="row">';
                $kal .= '<div class="col-sm-12">';
                $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                            <thead>'. $this->gen_table_header($arr_header) .'</thead>
                            <tbody>';
                $data = $this->model->get(Material_RM_PM::$TABLE_NAME);
                foreach ($data->result_array() as $row) {
                    $kal .= "<tr>";
                        $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";

                        if (isset($row[Material_RM_PM::$JENIS]))
                            $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$JENIS] . "</td>";
                        else
                            $kal .= "<td class='align-middle text-center'> - </td>";

                        $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$TIPE] . "</td>";

                        if (($row[Material_RM_PM::$IS_KONSINYASI]))
                            $kal .= "<td class='align-middle text-center green-text'> V </td>";
                        else
                            $kal .= "<td class='align-middle text-center red-text'> X </td>";

                        $kal .= "<td class='align-middle text-left'>" . $row[Material_RM_PM::$DESCRIPTION] . "</td>";
                        $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$UNIT] . "</td>";
                        $kal .= "<td class='align-middle text-right'>" . $row[Material_RM_PM::$BOX] . "</td>";

                        $temp = $this->model->get(Gudang_RM_PM::$TABLE_NAME, Gudang_RM_PM::$ID_MATERIAL_RM_PM, $row[Material_RM_PM::$ID]);

                        if ($temp->num_rows() > 0)
                            $kal .= "<td class='align-middle text-center'>" . $temp->row_array()[Gudang_RM_PM::$NAMA_GUDANG] . "</td>";
                        else
                            $kal .= "<td class='align-middle text-center'> - </td>";

                        $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$KODE_PANGGIL] . "</td>";

                    $kal .= "</tr>";
                }
                $kal .= '</tbody>
                        </table>';
                $kal .= "</div>";
            $kal .= "</div>";
        echo $kal;
    }
    public function material_fg_sap(){
        $kal="";
        $arr_header = ["FG Code (SAP)", "FG Code (SAGE Old)", "FG Code (SAGE New)","Kode Produk","Deskripsi","Pack","Box","Net"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL MATERIAL FG SAP</h1>";
        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12">';
            $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                        <thead>'. $this->gen_table_header($arr_header) .'</thead>
                        <tbody>';
            $data = $this->fg_model->get(Material_FG_SAP::$TABLE_NAME);
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                    $kal .= "<td class='align-middle text-center'>" . $row[Material_FG_SAP::$ID] . "</td>";
                    if(isset($row[Material_FG_SAP::$ID_SAGE])){
                        $fg_sage=$this->fg_model->get(Material::$TABLE_NAME,Material::$ID,$row[Material_FG_SAP::$ID_SAGE])->row_array();
                        $kal .= "<td class='align-middle text-center'>" . $fg_sage[Material::$ID] . "</td>";
                        $kal .= "<td class='align-middle text-center'>" . $fg_sage[Material::$ID_NEW] . "</td>";

                    }else{
                        $kal .= "<td class='align-middle text-center'> - </td>";
                        $kal .= "<td class='align-middle text-center'> - </td>";
                    }
                    $kal .= "<td class='align-middle text-left'>" . $row[Material_FG_SAP::$PRODUCT_MEMO]." - " . $row[Material_FG_SAP::$PACKING] . "</td>";
                    $kal .= "<td class='align-middle text-left'>" . $row[Material_FG_SAP::$DESCRIPTION] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material_FG_SAP::$PACKING] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material_FG_SAP::$BOX] . "</td>";
                    $kal .= "<td class='align-middle text-right'>" . $row[Material_FG_SAP::$NET] . "</td>";

                $kal .= "</tr>";
            }
            $kal .= '</tbody>
                    </table>';
            $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function karyawan(){
        $role = $this->karyawan_model->get_by(Karyawan::$USERNAME, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$USERNAME])->row_array()[Role::$ID];
        $kal="";
        
        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL KARYAWAN</h1>";
        $kal .= "<h6 class='f-aleo-bold text-center red-text'>Merah : Outsource</h6>";
        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                        <thead>
                            <tr class="table-primary">
                                <th scope="col" class="text-center">NIK</th>
                                <th scope="col" class="text-center">Nama</th>
                                <th scope="col" class="text-center">NPWP</th>
                                <th scope="col" class="text-center">Role</th>
                                <th scope="col" class="text-center">Kategori</th>
                                ';
        if ($this->equals($role,ROLE_KEPALA)) {
            $kal .= '<th class="role-kepala text-center" scope="col">Gaji Pokok (Rp)</th>';
            $kal .= '<th scope="col" class="role-kepala text-center">Tunjangan (Rp)</th>';
            $kal .= '<th scope="col" class="role-kepala text-center">Saldo Cuti</th>';
        }
        $kal .= '			
                            </tr>
                        </thead><tbody>';

        $data = $this->karyawan_model->get_all();
        foreach ($data->result_array() as $row) {
            if ($row[Karyawan::$IS_OUTSOURCE] == 1)
                $kal .= "<tr class='red-text'>";
            else
                $kal .= "<tr>";
            $kal .= "<td>" . $row[Karyawan::$ID] . "</td>";
            $kal .= "<td>" . ucfirst($row[Karyawan::$S_K_NAMA]) . "</td>";
            $kal .= "<td>" . $row[Karyawan::$NPWP] . "</td>";
            $kal .= "<td>" . ucfirst($row[Role::$S_R_NAMA]) . "</td>";
            $kal .= "<td>" . ucfirst($row[Kategori_Karyawan::$TIPE]) . "</td>";

            if ($role == ROLE_KEPALA) {
                $kal .= "<td class='role-kepala text-right'>" . $this->rupiah($row[Karyawan::$GAJI_POKOK]) . "</td>";
                $kal .= "<td class='role-kepala text-right'>" . $this->rupiah($row[Karyawan::$TUNJANGAN_JABATAN]) . "</td>";
                $kal .= "<td>" . $row[Karyawan::$SALDO_CUTI] . "</td>";
            }

            $kal .= "</tr>";
        }
        $kal .= '</tbody>
                        </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function kalender(){
        $kal="";
        $arr_header=["Tanggal","Keterangan","Aksi"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL KALENDER</h1>";
        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead>
                    <tbody>';
        $data = $this->kalender_model->get(Kalender::$TABLE_NAME);
        foreach ($data->result_array() as $row) {
            $tanggal = strtotime($row[Kalender::$TANGGAL]);
            $tampil_tanggal = date("l, j F Y", $tanggal);
            $kal .= "<tr>";
            $kal .= "<td class='text-center'>" . $tampil_tanggal . "</td>";
            $kal .= "<td class='text-center'>" . ucfirst($row[Kalender::$KETERANGAN]) . "</td>";
            $kal .= '<td class="text-center"> <button type="button" class="btn btn-outline-danger" onclick="delete_kalender(' . $row[Kalender::$ID] . ')" data-mdb-ripple-color="dark">
							Hapus
						  </button></td>';
            $kal .= "</tr>";
        }
        $kal .= '</tbody>
							</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function absensi(){
        $kal="";
        $arr_header=["NIK","Nama","Tanggal","Masuk","Keluar","Keterangan"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL ABSENSI</h1>";
            $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                        <thead>'. $this->gen_table_header($arr_header) .'</thead>
                        <tbody>';
            $data = $this->absensi_model->get(Absensi::$TABLE_NAME);
            foreach ($data->result() as $row) {
                $tanggal = strtotime($row[Absensi::$TANGGAL]);
                $tampil_tanggal = date("d/m/Y", $tanggal);
                $tampil_jam_masuk = date("H:i", strtotime($row->jam_masuk));
                $tampil_jam_keluar = date("H:i", strtotime($row->jam_keluar));

                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row->nik . "</td>";
                $kal .= "<td class='text-center'>" . $this->capitalize($row->nama) . "</td>";
                $kal .= "<td class='text-center'>" . $tampil_tanggal . "</td>";
                $kal .= "<td class='text-center'>" . $tampil_jam_masuk . "</td>";
                $kal .= "<td class='text-center'>" . $tampil_jam_keluar . "</td>";
                $kal .= "<td class='text-center'>" . ucfirst($row->keterangan) . "</td>";
                $kal .= "</tr>";
            }
            $kal .= '</tbody>
							</table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= "</div>";
        echo $kal;
    }
    public function cutoff(){
        $kal="";
        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL CUTOFF</h1>";
            $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-3"></div>';
            $kal .= '<div class="col-sm-6">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Dari Tanggal</th>
									<th scope="col" class="text-center">Sampai Tanggal</th>
									<th scope="col" class="text-center">Keterangan</th>
									<th scope="col" class="text-center">Aksi</th>';
            $kal .= '		</tr>
							</thead><tbody>';
            $data = $this->cutoff_model->get(Cutoff::$TABLE_NAME);
            foreach ($data->result_array() as $row) {
                $tanggal_dari = strtotime($row[Cutoff::$TANGGAL_DARI]);
                $tampil_tanggal_dari = date("l, j F Y", $tanggal_dari);
                $tanggal_sampai = strtotime($row[Cutoff::$TANGGAL_SAMPAI]);
                $tampil_tanggal_sampai = date("l, j F Y", $tanggal_sampai);
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $tampil_tanggal_dari . "</td>";
                $kal .= "<td class='text-center'>" . $tampil_tanggal_sampai . "</td>";
                $kal .= "<td class='text-center'>" . ucfirst($row[Cutoff::$KETERANGAN]) . "</td>";
                $kal .= '<td class="text-center"> <button type="button" class="btn btn-outline-danger" onclick="delete_cutoff(' . $row[Cutoff::$ID] . ')" data-mdb-ripple-color="dark">
							Hapus
						  </button>';
                $kal .= "</tr>";
            }
            $kal .= '</tbody>
							</table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-3"></div>';
            $kal .= "</div>";
        echo $kal;
    }
    public function payterm(){
        $kal = "";
        $arr_header = ["ID Payterm", "Deskripsi"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL PAYTERM</h1>";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                        <thead>' . $this->gen_table_header($arr_header) . '</thead>
                        <tbody>';
        $data = $this->payterm_model->get(Payterm::$TABLE_NAME);
        foreach ($data->result_array() as $row) {
            $kal .= "<tr>";
                $kal .= "<td class='align-middle text-center'>" . $row[Payterm::$ID] . "</td>";
                $kal .= "<td class='align-middle text-left'>" . $row[Payterm::$DESKRIPSI] . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '</tbody>
                    </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';

        $kal .= "</div>";
        echo $kal;
    }
    public function vendor(){
        $kal = "";
        $arr_header = ["ID Vendor", "Nama", "Alamat","Kode Pos", "Currency", "Sales Person", "NPWP"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL VENDOR</h1>";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                        <thead>' . $this->gen_table_header($arr_header) . '</thead>
                        <tbody>';
        $data = $this->vendor_model->get(Vendor::$TABLE_NAME);
        foreach ($data->result_array() as $row) {
            $kal .= "<tr>";
                $kal .= "<td class='align-middle text-center'>" . $row[Vendor::$ID] . "</td>";
                $kal .= "<td class='align-middle text-left'>" . $row[Vendor::$NAMA] . "</td>";
                $kal .= "<td class='align-middle text-left'>" . strtoupper($row[Vendor::$ALAMAT]) . "</td>";
                //$kal .= "<td class='align-middle text-left'>" . $row[Vendor::$KOTA] . "</td>";
                //$kal .= "<td class='align-middle text-left'>" . $row[Vendor::$NEGARA] . "</td>";
                $kal .= "<td class='align-middle text-center'>" . $row[Vendor::$KODE_POS] . "</td>";
                $kal .= "<td class='align-middle text-center'>" . $row[Vendor::$CURRENCY] . "</td>";
                $kal .= "<td class='align-middle text-left'>" . ucwords(strtolower($row[Vendor::$SALES_PERSON])) . "</td>";
                $kal .= "<td class='align-middle text-left'>" . $row[Vendor::$NPWP] . "</td>";
                //$kal .= "<td class='align-middle text-left'>" . $row[Vendor::$EMAIL] . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '</tbody>
                    </table>';
        $kal .= "</div>";

        $kal .= "</div>";
        echo $kal;
    }
    public function general_ledger(){
        $kal = "";
        $arr_header = ["ID General Ledger", "Deskripsi"];

        $kal .= "<h1 class='f-aleo-bold text-center'>TABEL GENERAL LEDGER</h1>";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                        <thead>' . $this->gen_table_header($arr_header) . '</thead>
                        <tbody>';
        $data = $this->other_model->get(General_Ledger::$TABLE_NAME,null,null,null,null,false);
        foreach ($data->result_array() as $row) {
            $kal .= "<tr>";
                $kal .= "<td class='align-middle text-center'>" . $row[General_Ledger::$ID] . "</td>";
                $kal .= "<td class='align-middle text-left'>" . $row[General_Ledger::$DESKRIPSI] . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '</tbody>
                    </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';

        $kal .= "</div>";
        echo $kal;
    }

    public function view_kebersihan(){
        $kal="";
        $nik = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
        $kebersihan_area=$this->kebersihan_model->get(Kebersihan_Checker_Area::$TABLE_NAME,Kebersihan_Checker_Area::$NIK,$nik,null,null,false);
        if($kebersihan_area->num_rows()>0){
            $kal .= "<div>";
                $kal .= "<div class='row' style='margin-right:1%'>";
                    $kal .= "<div class='col-sm-3'></div>";
                    $kal .= "<div class='col-sm-6 shadow-lg p-3 mb-5 bg-white rounded border border-success' style='padding:1%; margin-right:1%; margin-left:1%'>";
                        $kal .= "<h1 class='text-center f-aleo-bold'>Kebersihan</h1><hr>";
            $ctr=0;
                $kal .= "<div class='row text-center' style='margin-right:1%'>";

            foreach($kebersihan_area->result_array() as $row_kebersihan){
                        $gudang = $this->kebersihan_model->get(Kebersihan_Gudang::$TABLE_NAME,Kebersihan_Gudang::$ID,$row_kebersihan[Kebersihan_Checker_Area::$GUDANG],null,null,false)->row_array();
                        $kal .= "<div class='col-sm-4 f-aleo-bold text-center'>". $gudang[Kebersihan_Gudang::$GUDANG] ." - ". $gudang[Kebersihan_Gudang::$RAK] ."";
                        $kal .= "<select class='form-control' id='dd_kebersihan_". $ctr ."'>
                                    <option value=0>KOTOR</option>
                                    <option value=1>BERSIH</option>
                                </select>";
                        $kal .= "<input type='hidden' id='txt_gudang_". $ctr ."' value=". $gudang[Kebersihan_Gudang::$ID] ."></div>";
                $ctr++;
            }
                $kal.="</div>";
                    $kal .= '<button type="button" style="width:100%;margin-left:0%" id="btn_tambah_barang" class="center btn btn-outline-success" onclick="input_kebersihan()" data-mdb-ripple-color="dark">
                                    Submit
                                </button>';
                    $kal .= "</div>";
                    $kal .= "<div class='col-sm-3'></div>";
                    
                    $kal .= "</div>";
                $kal .= "</div>";
            $kal .= "</div>";


        }

        return $kal;
    }

    public function today_info(){
        $kal = "<br>";

        //Kebersihan
        $kal .= $this->view_kebersihan();

        //FG
        $kal .= "<div class='role-accounting role-checker-fg'>";
            $kal .= "<div class='row' style='margin-right:1%'>";
                $kal .= "<div class='col-sm-1'></div>";
                $kal .= "<div class='col-sm-10 shadow-lg p-3 mb-5 bg-white rounded border' style='padding:1%; margin-right:1%; margin-left:1%'>";
                    $kal .= "<h1 class='text-center f-aleo-bold'>Finished Good</h1><hr>";
                    $kal .= "<div class='row'>";
                        $kal .= "<div class='col-sm-6'>";

                            $kal .= "<h3 class='f-aleo-bold'>Barang Masuk</h3>";
                            $fg_in_info=$this->goodreceipt_model->get_today_info()->num_rows();
                            $fg_transit_info = $this->fg_model->get_all_sisa_transit()->num_rows();

                            $kal .= "<ul>"; 
                                $kal .= "<li>Terdapat <a class='font-italic' href='".site_url("home/finished/rekap_sj/").$this::$TODAY_INFO."'>$fg_in_info Surat Jalan</a> yang dibuat hari ini</li>";
                                $kal .= "<li>Terdapat <a class='font-italic' href='" . site_url("home/finished/gudang_transit_report") . "'>$fg_transit_info Work Sheet</a> yang masih berada di Gudang Transit</li>";
                            $kal .= "</ul>";
                        $kal .= "</div>";

                        $kal .= "<div class='col-sm-6'>";

                            $kal .= "<h3 class='f-aleo-bold'>Barang Keluar</h3>";
                            $fg_out_info = $this->goodissue_model->get_today_info()->num_rows();
                            $kal .= "<ul>"; 
                                $kal .= "<li>Terdapat <a class='font-italic' href='".site_url("home/finished/rekap_sj/").$this::$TODAY_INFO."'>$fg_out_info Surat Jalan</a> yang dibuat hari ini</li>";
                            $kal .= "</ul>";
                    $kal .= "</div>";
                $kal .= "</div>";
                $kal .= "<div class='col-sm-1'></div>";

            $kal .= "</div>";
        $kal .= "</div>";

        //RM PM
        $kal .= "<div class='role-accounting role-ppic role-checker-rm-pm role-admin-rm-pm role-purchasing'>";
            $kal .= "<div class='row'style='margin-right:1%'>";
                $kal .= "<div class='col-sm-1'></div>";
                $kal .= "<div class='rounded shadow-lg p-3 mb-5 bg-white col-sm-10 border' style='padding:1%; margin-right:1%; margin-left:1%;'>";
                    $kal .= "<h1 class='text-center f-aleo-bold'>Raw Material Package Material</h1><hr>";
                     $kal .= "<div class='row'>";
                        $kal .= "<div class='col-sm-6'>";
                            $kal .= "<h3 class='f-aleo-bold'>Barang Masuk</h3>";
                            $rmpm_in_info = $this->po_model->get_today_info()->num_rows();
                            $kal .= "<ul>"; 
                                $kal .= "<li>Terdapat <a class='font-italic' href='" . site_url("home/rmpm/rekap_po_ws/").$this::$TODAY_INFO . "'>$rmpm_in_info Purchase Order</a> yang diproses hari ini</li>";
                            $kal .= "</ul>"; 
                        $kal .= "</div>";

                        $kal .= "<div class='col-sm-6'>";
                            $kal .= "<h3 class='f-aleo-bold'>Barang Keluar</h3>";
                            $rmpm_out_info = $this->ws_model->get_today_info();
                            $kal .= "<ul>"; 
                                $kal .= "<li>Terdapat <a class='font-italic' href='" . site_url("home/rmpm/rekap_po_ws/") . $this::$TODAY_INFO . "'>".$rmpm_out_info[Worksheet::$S_FG]." Work Sheet FG</a> yang diproses hari ini</li>";
                                $kal .= "<li>Terdapat <a class='font-italic' href='" . site_url("home/rmpm/rekap_po_ws/") . $this::$TODAY_INFO . "'>".$rmpm_out_info[Worksheet::$S_SEMI]." Work Sheet Semi</a> yang diproses hari ini</li>";
                            $kal .= "</ul>"; 
                        $kal .= "</div>";
                    $kal .= "</div>";
                $kal .= "</div>";
                $kal .= "<div class='col-sm-1'></div>";
            $kal .= "</div>";
        $kal .= "</div>";

        echo $kal;

    }
}
