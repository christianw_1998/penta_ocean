<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Payment_Controller extends Library_Controller {
    
    public function check_cancel(){
        $id = $this->i_p("i");
        $reason = $this->i_p("r");
    
        if (strpos($id, Payment_Voucher_Detail_H::$S_INSERT_ID) === 0) { //Adalah Payment Voucher Detail
            $this->voucher_detail_cancel($id, $reason);
            return;
        } else if (strpos($id, Payment_Voucher_H::$S_INSERT_ID) === 0) { //Adalah Payment Voucher
            $this->voucher_cancel($id, $reason);
            return;
        } else{
            $data = [];
            $data["message"] = MESSAGE[Payment_Voucher_Detail_H::$MESSAGE_FAILED_SEARCH_NOT_FOUND];
            echo json_encode($data);
        }
    }
    public function voucher_cancel($id,$reason){
        echo MESSAGE[$this->payment_model->voucher_cancel($id,$reason)];
    }

    public function check_view_rekap(){
        $id = $this->i_p("i");
        $id_cancel = null;
        //Cek dulu apakah rekap ini cancelan
        // if (strpos($id, Payment_Voucher_Cancel::$S_INSERT_ID) === 0) { //Adalah Payment Voucher Detail
            
        //     //Cari apakah ini cancel PVD atau PV
        //     $payment_voucher=$this->payment_model->get(Payment_Voucher_Detail_H::$TABLE_NAME,Payment_Voucher_Detail_H::$ID_CANCEL,$id,null,null,false);
        //     if($payment_voucher->num_rows()>0){
        //         foreach($payment_voucher->result_array() as $row_payment){
        //             echo $row_payment[Payment_Voucher_Detail_H::$ID] != $id;
        //             if($row_payment[Payment_Voucher_Detail_H::$ID]!=$id){
        //                 $id_cancel = $row_payment[Payment_Voucher_Detail_H::$ID_CANCEL];
        //                 $id = $row_payment[Payment_Voucher_Detail_H::$ID];
        //                 break;
        //             }
        //         }
        //     }
        //     $payment_voucher = $this->payment_model->get(Payment_Voucher_H::$TABLE_NAME, Payment_Voucher_H::$ID_CANCEL, $id, null, null, false);
        //     if ($payment_voucher->num_rows() > 0) {
        //         foreach ($payment_voucher->result_array() as $row_payment) {
        //             if ($row_payment[Payment_Voucher_H::$ID] != $id) {
        //                 $id_cancel = $row_payment[Payment_Voucher_H::$ID_CANCEL];
        //                 $id = $row_payment[Payment_Voucher_H::$ID];
        //             }
        //         }
        //     }
        // }

        if (strpos($id, Payment_Voucher_Detail_H::$S_INSERT_ID) === 0) { //Adalah Payment Voucher Detail
            if($id_cancel!=null)
                $this->voucher_detail_view_rekap($id_cancel);
            else
                $this->voucher_detail_view_rekap($id);

            return;
        } else if (strpos($id, Payment_Voucher_H::$S_INSERT_ID) === 0) { //Adalah Payment Voucher
            
            if ($id_cancel != null)
                $this->view_rekap($id_cancel);
            else
                $this->view_rekap($id);
            return;
        } else{
            $data = [];
            $data["message"] = MESSAGE[Payment_Voucher_Detail_H::$MESSAGE_FAILED_SEARCH_NOT_FOUND];
            echo json_encode($data);
        }
    }

    public function gl_view_rekap(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $gl_dari = $this->i_p("dg");
        $gl_sampai = $this->i_p("sg");
        $gl_list = $this->i_p("lg");

        $data_table = [];
        $data_table_clearing = [];
        $data_gl = [];
        $data_ctr = [];
        $data_ctr_clearing = [];
        $arr_header = [
            "Assignment", "DocumentNo", "Doc. Date", "Amount", "Clearing", "Text", "Reference",
            "Order", "PO","Cost Center"
        ];
        $kal = "";
        $data_payment = $this->payment_model->get_history($tanggal_dari, $tanggal_sampai, $gl_dari, $gl_sampai,$gl_list);
        if ($data_payment->num_rows() > 0) {
            foreach ($data_payment->result_array() as $row_payment) {
                $id_gl = $row_payment[Payment_Voucher_Detail_D::$ID_GL];
                if(!in_array($id_gl,$data_gl)){
                    array_push($data_gl, $id_gl);
                    $data_table[$id_gl] = [];
                    $data_table_clearing[$id_gl] = [];
                    $data_ctr[$id_gl] = 0;
                    $data_ctr_clearing[$id_gl]=0;
                }
                // $tipe="";
                // if ($row_payment[Payment_Voucher_Detail_D::$AMOUNT] < 0) { //Cancelan
                //     if ($row_payment[Payment_Voucher_Detail_D::$TIPE] != -1)
                //         $tipe = "K";
                //     else
                //         $tipe = "D";
                // } else {
                //     if ($row_payment[Payment_Voucher_Detail_D::$TIPE] == -1)
                //         $tipe = "K";
                //     else
                //         $tipe = "D";
                // }
                //$data_table[$row_payment[Payment_Voucher_Detail_D::$ID_GL]][$data_ctr[$row_payment[Payment_Voucher_Detail_D::$ID_GL]]][$arr_header[2]] = $tipe;
               
                //Clearing
                $clearing = " - ";
                $pvt = $this->payment_model->get(Payment_Voucher_Transaction::$TABLE_NAME, Payment_Voucher_Transaction::$ID_PVD_HEADER, $row_payment[Payment_Voucher_Detail_H::$ID], null, null, false);
                if ($pvt->num_rows() > 0) {
                    $pv = $this->payment_model->get(Payment_Voucher_D::$TABLE_NAME, Payment_Voucher_D::$ID, $pvt->row_array()[Payment_Voucher_Transaction::$ID_PV_DETAIL], null, null, false);
                    if ($pv->num_rows() > 0) {
                        $clearing = $pv->row_array()[Payment_Voucher_D::$ID_HEADER];
                    }
                } else {
                    //Apakah Cancelan
                    if ($row_payment[Payment_Voucher_Detail_H::$ID_CANCEL] != null) {
                        $clearing = $row_payment[Payment_Voucher_Detail_H::$ID_CANCEL];
                    }
                }

                $entry = [
                    $arr_header[0] => $row_payment[Payment_Voucher_Detail_D::$ASSIGNMENT],
                    $arr_header[1] => $row_payment[Payment_Voucher_Detail_D::$ID_HEADER],
                    $arr_header[2] => $row_payment[Payment_Voucher_Detail_H::$TANGGAL],
                    $arr_header[3] => ($row_payment[Payment_Voucher_Detail_D::$AMOUNT] * $row_payment[Payment_Voucher_Detail_D::$TIPE]),
                    $arr_header[4] => $clearing,
                    $arr_header[5] => $row_payment[Payment_Voucher_Detail_H::$TEXT],
                    $arr_header[6] => $row_payment[Payment_Voucher_Detail_H::$REFERENCE],
                    $arr_header[7] => $row_payment[Payment_Voucher_Detail_D::$ID_MATERIAL],
                    $arr_header[8] => " - ",
                    $arr_header[9] => $row_payment[Payment_Voucher_Detail_D::$ID_CC]
                ];
                if ($clearing != " - ") {
                    // Masuk ke array clearing
                    $data_table_clearing[$id_gl][$data_ctr_clearing[$id_gl]] = $entry;
                    $data_ctr_clearing[$id_gl]++;
                } else {
                    // Masuk ke array non-clearing
                    $data_table[$id_gl][$data_ctr[$id_gl]] = $entry;
                    $data_ctr[$id_gl]++;
                }
            }
        }else{
            $kal .= "<div class='row'>";
            $kal .= "<div class='col-sm-12 text-center'>";
            $kal .= "<h3 class='red-text fw-bold'>Data Tidak Ditemukan</h3>";
            $kal .= "</div>";
            $kal .= "</div>";
            echo $kal;
            return;
        }
        

        $kal .= '<div class="row">';
        //$kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-12">';

        $data_combined = [];

        foreach ($data_table as $gl_id => $rows) {
            // Data non-clearing
            $data_combined[$gl_id]['non_clearing'] = $rows;
        }

        // Sekarang gabungkan data clearing
        foreach ($data_table_clearing as $gl_id => $rows) {
            // Data clearing
            $data_combined[$gl_id]['clearing'] = $rows;
        }
        ksort($data_combined);

        $index = 0; // Counter untuk auto increment index
        foreach ($data_combined as $gl_id => $data_rows) {
            
            $row_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $gl_id)->row_array();
            $kal .= '<br><h3>GL ' . $gl_id . " [" . $row_gl[General_Ledger::$DESKRIPSI] . "]" . '</h3>';
            $kal .= '<table class="table table-bordered table-sm"  style="table-layout: fixed; width: 100%;"><thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';
            // Menambahkan input type hidden untuk setiap index auto increment
            $kal .= "";
            $total_all = 0;
            foreach (['non_clearing', 'clearing'] as $type) {
                if (isset($data_rows[$type]) && !empty($data_rows[$type])) {
                    $total = 0;

                    foreach ($data_rows[$type] as $row) {
                        $kal .= "<tr style='line-height: 10px;'>";
                        foreach ($arr_header as $header) {
                            $align = ($header == "Amount") ? "text-right" : "text-center";
                            $value = ($header == "Amount") ? $this->rupiah($row[$header]) : $row[$header];

                            if ($header == "Amount") {
                                $total += $row[$header]; // Menambahkan nilai Amount ke total
                            }

                            if ($header == "Text") {
                                $kal .= "<input type='hidden' id='txt_gl_{$index}' value='{$gl_id}\t[{$row_gl[General_Ledger::$DESKRIPSI]}]'>
                            <td style='overflow: hidden; white-space: nowrap; text-overflow: ellipsis;' class='text-left blue-text' title='{$value}'>{$value}</td>";
                            } else if ($header == "Cost Center") {
                                $row_cc = $this->cc_model->get(Cost_Center::$TABLE_NAME, Cost_Center::$ID, $value)->row_array();
                                $kal .= "<td style='overflow: hidden; white-space: nowrap; text-overflow: ellipsis;' class='text-center blue-text' title='" . $row_cc[Cost_Center::$NAMA] . "'>{$value}</td>";
                            } else {
                                $kal .= "<td class='{$align}'>{$value}</td>";
                            }
                        }
                        $kal .= "</tr>";
                    }

                    // Menambahkan baris total
                    $kal .= "<tr style='line-height: 10px;'>" . str_repeat("<td></td>", 10) . "</tr>";
                    $kal .= "<tr style='line-height: 10px;'>
                                <td></td>
                                <td></td>
                                <td class='fw-bold text-right'>Total</td>
                                <td class='text-right'>" . $this->rupiah($total) . "</td>" .
                                    str_repeat("<td></td>", 6) . "
                            </tr>";
                    $kal .= "<tr style='line-height: 10px;'>" . str_repeat("<td></td>", 10) . "</tr>";
                    $total_all += $total;
                }
            }
            //Total Clearing dengan Non Clearing
            //Jika salah 1 0 maka tidak perlu tampil 
            //if($total_clearing != 0 && $total_non_clearing != 0){
                $kal .= "<tr style='line-height: 10px;'>";
                    $kal .= "<td class='text-right fw-bold'>Account</td>";
                    $kal .= "<td class='text-center'>$gl_id</td>";
                    $kal .= "<td class='fw-bold text-right'>Total</td>";
                    $kal .= "<td class='text-right'>" . $this->rupiah($total_all) . "</td>";
                    for ($i = 0; $i < 6; $i++) { $kal .= "<td></td>"; }
                $kal .= "</tr>";
            //}
            $kal .= '</tbody></table>';

            $index++; // Increment index untuk setiap iterasi
        }
        
        $kal .= "</div>";
        //$kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function view_rekap($id){
        $id_header = $id;
        $data = [];
        $data["message"] = MESSAGE[Payment_Voucher_Detail_H::$MESSAGE_SUCCESS_INSERT];
        $kal = "";

        $arr_header = ["Account", "Nama", "Amount", "Tipe", "CC", "Text"];

        // Header Rekap
        $qry_header = $this->payment_model->get(Payment_Voucher_H::$TABLE_NAME, Payment_Voucher_H::$ID, $id_header, null, null, false);
        if ($qry_header->num_rows() == 0) {
            $data["message"] = MESSAGE[Payment_Voucher_Detail_H::$MESSAGE_FAILED_SEARCH_NOT_FOUND];
            echo json_encode($data);
            return;
        }

        $row_header = $qry_header->row_array();
        $disabled = $this->equals($row_header[Payment_Voucher_H::$ID_CANCEL], null) ? "" : "disabled";

        // Tombol Aksi
        $kal .= '<div class="row text-center">
                <div class="col-sm-12">
                    <button type="button" ' . $disabled . ' id="btn_edit" class="btn btn-outline-warning" onclick="edit()">Ubah</button>
                    <button type="button" ' . $disabled . ' id="btn_update" disabled class="btn btn-outline-success" onclick="update()">Simpan</button>
                    <button type="button" ' . $disabled . ' id="btn_cancel" class="btn btn-outline-danger" onclick="cancel()">Cancel</button>
                </div>
            </div>';

        // Informasi Header
        $tanggal = strtotime($row_header[Payment_Voucher_H::$TANGGAL]);
        $kal .= '<div class="row">
                <div class="col-sm-6">
                    <table>
                        <tr><td class="fw-bold">Nomor Header</td><td>:</td><td><span id="txt_header">' . $row_header[Payment_Voucher_H::$ID] . '</span></td></tr>
                        <tr><td class="fw-bold">Tanggal</td><td>:</td><td>' . date("d-m-Y", $tanggal) . '</td></tr>
                        <tr><td class="fw-bold">Assignment</td><td>:</td><td>' . $row_header[Payment_Voucher_H::$ASSIGNMENT] . '</td></tr>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table>
                        <tr><td class="fw-bold">Currency</td><td>:</td><td>IDR</td></tr>
                        <tr><td class="fw-bold">Tahun</td><td>:</td><td>' . date("Y", $tanggal) . '</td></tr>
                        <tr><td class="fw-bold">Bulan</td><td>:</td><td>' . date("F", $tanggal) . '</td></tr>
                    </table>
                </div>
            </div>';

        // Input Text
        $kal .= '<div class="row">
                <div class="col-sm-2 text-right">Text :</div>
                <div class="col-sm-8"><input disabled value="' . $row_header[Payment_Voucher_Detail_H::$TEXT] . '" type="text" style="width:100%" id="txt_text"></div>
            </div><br><br>';

        // Table Header
        $kal .= '<div class="row"><div class="col-sm-12">
                <table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';

        // GL Bank
        $gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $row_header[Payment_Voucher_H::$ID_GL])->row_array();
        $kal .= '<tr>
                <td class="text-center">' . $row_header[Payment_Voucher_H::$ID_GL] . '</td>
                <td class="text-left">' . $gl[General_Ledger::$DESKRIPSI] . '</td>
                <td class="text-right">' . $this->rupiah($row_header[Payment_Voucher_H::$TOTAL] * -1) . '</td>
                <td class="text-center">K</td>
                <td class="text-center"> - </td>
                <td class="text-center"> - </td>
            </tr>';

        // Detail Payment Voucher
        $data_detail = $this->payment_model->get(Payment_Voucher_D::$TABLE_NAME, Payment_Voucher_D::$ID_HEADER, $id_header, null, null, false);
        $ctr = 1;
        if ($data_detail->num_rows() > 0) {
            foreach ($data_detail->result_array() as $row_detail) {
                $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_detail[Payment_Voucher_D::$ID_VENDOR])->row_array();
                $kal .= '<tr>
                        <td class="text-center">' . $row_detail[Payment_Voucher_D::$ID_VENDOR] . '</td>
                        <td class="text-left">' . $vendor[Vendor::$NAMA] . '</td>
                        <td class="text-right">' . $this->rupiah($row_detail[Payment_Voucher_D::$AMOUNT]) . '</td>
                        <td class="text-center">D</td>
                        <td class="text-center"> - </td>
                        <td class="text-left">' . $row_header[Payment_Voucher_H::$TEXT] . '</td>
                        <input type="hidden" id="txt_detail_' . $ctr . '" value="' . $row_detail[Payment_Voucher_Detail_D::$ID] . '">
                    </tr>';
                $ctr++;
            }
        }

        // Detail Pembulatan
        if (!$this->equals($row_header[Payment_Voucher_H::$PEMBULATAN_ID_GL], null)) {
            $gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $row_header[Payment_Voucher_H::$PEMBULATAN_ID_GL])->row_array();
            $kal .= '<tr>
                    <td class="text-center">' . $row_header[Payment_Voucher_H::$PEMBULATAN_ID_GL] . '</td>
                    <td class="text-left">' . $gl[General_Ledger::$DESKRIPSI] . '</td>
                    <td class="text-right">' . $this->rupiah(abs($row_header[Payment_Voucher_H::$PEMBULATAN_AMOUNT])) . '</td>
                    <td class="text-center">D</td>
                    <td class="text-center">' . $row_header[Payment_Voucher_H::$PEMBULATAN_ID_CC] . '</td>
                    <td class="text-left">' . $row_header[Payment_Voucher_H::$PEMBULATAN_DESCRIPTION] . '</td>
                </tr>';
        }

        $kal .= '</tbody></table><input type="hidden" id="txt_ctr" value="' . $ctr . '"></div></div>';

        $data["kal"] = $kal;
        echo json_encode($data);
    }

    public function plus_vendor(){
        $ctr = $this->i_p("c");
        $kal = "";
        $modal = "";
        $kal .= "<tr class='tr_detail_vendor_$ctr'>
                    <td style='width:2%' class='text-center'>
                        ". ($ctr + 1) ." </td>
                    <td style='width:10%'>
                        <input style='width:100%' type='text' placeholder='ID Vendor' onchange='check_vendor($ctr)' list='list_vendor' id='txt_id_vendor_$ctr'/>
                    </td>
                    <td style='width:50%' class='text-center'>
                        <span id='txt_desc_vendor_$ctr' class='red-text span'>Vendor tidak ditemukan</span>
                    </td>
                    <td style='width:10%' class='text-center'>
                        <button style='width:100%' type='button' id='btn_preview_vendor' onclick='preview_vendor($ctr)' data-mdb-ripple-color='dark'>
                            Cari
                        </button>
                    </td>
                    <td style='width:30%;padding-right:5%' class='text-right'>
                        <span class='text-right' id='txt_tampil_total_$ctr'>0</span>
                        <input type='hidden' id='txt_total_$ctr' />
                    </td>
                    <td style='width:30%' class='text-right'>
                        <button type='button' class='red-text' onclick='minus_vendor($ctr)' data-mdb-ripple-color='dark'>
                            X
                        </button>
                    </td>
                </div>
                ";

        $modal= '<div class="f-aleo modal fade" id="mod_vendor_'.$ctr. '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl " role="document">
                        <!--Content-->
                        <div class="modal-content">
                        <!--Header-->
                        <div class="modal-header">
                            <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">TRANSAKSI VENDOR</h5>
                        </div>
                        <!--Body-->
                        <div class="modal-body" id="div_payment_vendor_'.$ctr.'" style="margin:1%">
                            
                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <button type="button" onclick="update_vendor_total('.$ctr.')" class="btn btn-outline-success">OK</button>
                        </div>
                        </div>
                        </form>
                        <!--/.Content-->
                    </div>
                    </div>';
        $data["kal"] = $kal;
        $data["modal"] = $modal;
        echo json_encode($data);
    }
    public function get_vendor_payment_detail(){
        $ctr=$this->i_p("c");
        $id_vendor=$this->i_p("iv");
        $kal = "";
        $arr_header=["ID PV Detail", "Tanggal", "Net Amount", "Payment Amount", "Partial"];
        $data_payment=$this->payment_model->voucher_detail_get_by_id_vendor($id_vendor);
        if($data_payment->num_rows()>0){
            $kal .= '<table class="table table-bordered table-sm" id="tabel_payment_vendor_' . $ctr . '">
                        <thead>' . $this->gen_table_header($arr_header) . '</thead>
                        <tbody>';
            $total = 0;
            $idx = 0;
            foreach($data_payment->result_array() as $row_payment){
                if($row_payment[Payment_Voucher_Detail_H::$TOTAL] != 0){
                    $temp_total= $row_payment[Payment_Voucher_Detail_H::$TOTAL];
                    $kal .= "<tr>
                                <td class='text-center'>". $row_payment[Payment_Voucher_Detail_H::$ID] . "</td>
                                <td class='text-center'>" . date("d F Y", strtotime($row_payment[Payment_Voucher_Detail_H::$TANGGAL])) . "</td>
                                <td style='user-select: none' ondblclick='toggle_payment_detail($idx,$id_vendor)' class='red-text text-right' id='txt_action_" . $id_vendor . "_" . $idx . "'>" . $this->rupiah($temp_total) . "</td>
                                <td class='text-center'>
                                    <input disabled type='number' onkeyup='update_modal_vendor_total($idx,$id_vendor)' id='txt_amount_" . $id_vendor . "_" . $idx . "' value=" . ($temp_total) . ">
                                    <input disabled type='hidden' id='txt_hidden_amount_" . $id_vendor . "_" . $idx . "' value=" . $temp_total . ">
                                </td>
                                <td class='text-center'><input onchange='update_modal_vendor_total($idx,$id_vendor)' value='". $row_payment[Payment_Voucher_Detail_H::$ID] ."' type='checkbox' id='ck_". $row_payment[Payment_Voucher_Detail_H::$ID_VENDOR] ."_". $idx ."'></td>
                            </tr>";
                    $idx++;
                    $total += ($row_payment[Payment_Voucher_Detail_H::$TOTAL]);
                }
            }
            if($total!=0){
                $kal .= "<tr>
                    <td colspan='2' class='fw-bold text-right'>Total</td>
                    <td class='text-right' id='txt_modal_total_". $id_vendor ."'>" . $this->rupiah($total) . "</td>
                </tr>";
            }
            $kal .= '
                </tbody>
            </table>';
        }

        echo $kal;
    }
    
    public function view_modal_preview(){
        $kal="";
        
        $tanggal = $this->i_p("t");
        $reference = $this->i_p("r");
        $assignment = $this->i_p("as");
        $id_gl = $this->i_p("ig");

        $ctr = $this->i_p("c");
        $text = $this->i_p("te");
        $pembulatan_id_gl = $this->i_p("pg");
        $pembulatan_tipe = $this->i_p("pt");
        $pembulatan_amount = $this->i_p("pa");
        $pembulatan_id_cc = $this->i_p("pc");
        $pembulatan_desc = $this->i_p("pd");

        $arr_id_vendor = $this->i_p("av");
        $arr_total = $this->i_p("at");

        $arr_header = [
            "Account", "Nama", "Amount", "Tipe", "CC", "Text"
        ];

        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12 text-center">';

        //Header Rekap
        $kal .= '<div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        <table>
                            <tr>
                                <td class="text-right fw-bold">Reference</td>
                                <td>:</td>
                                <td class="text-left">' . $reference . '</td>
                            </tr>
                            <tr>
                                <td class="text-right fw-bold">Tanggal</td>
                                <td>:</td>
                                <td class="text-left">'. $tanggal . '</td>
                            </tr>
                            <tr>
                                <td class="text-right fw-bold">Assignment</td>
                                <td>:</td>
                                <td class="text-left">' . $assignment . '</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2"></div>
                </div><br>';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                        <thead>' . $this->gen_table_header($arr_header) . '</thead>
                        <tbody>';

            $total = 0;
            for ($i = 0;$i < $ctr; $i++) {
                $total += $arr_total[$i];

            }
            //GL Bank
            $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $id_gl . "</td>";
                $gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $id_gl)->row_array();
                $kal .= "<td class='text-left'>" . $gl[General_Ledger::$DESKRIPSI] . "</td>";
                $kal .= "<td class='text-right'> " . $this->rupiah(($total + ($pembulatan_amount * $pembulatan_tipe))*-1) . "</td>";
                $kal .= "<td class='text-center'>K</td>";
                $kal .= "<td class='text-center'> - </td>";
                $kal .= "<td class='text-center'> - </td>";
            $kal .= "</tr>";
            
            for ($i = 0;$i < $ctr; $i++) {
                $kal .= "<tr>";
                    $kal .= "<td class='text-center'>" . $arr_id_vendor[$i] . "</td>";
                    $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $arr_id_vendor[$i])->row_array();
                    $kal .= "<td class='text-left'>" . $vendor[Vendor::$NAMA] . "</td>";
                    $kal .= "<td class='text-right'>" . $this->rupiah($arr_total[$i]) . "</td>";
                    $kal .= "<td class='text-center'>D</td>";
                    $kal .= "<td class='text-center'> - </td>";
                    $kal .= "<td class='text-left'>" . $text . "</td>";
                $kal .= "</tr>";
            }

            if($pembulatan_id_gl!=""){
                //Pembulatan
                $kal .= "<tr>";
                    $kal .= "<td class='text-center'>" . $pembulatan_id_gl . "</td>";
                    $gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $pembulatan_id_gl)->row_array();
                    $kal .= "<td class='text-left'>" . $gl[General_Ledger::$DESKRIPSI] . "</td>";
                    $kal .= "<td class='text-right'> " . $this->rupiah(($pembulatan_amount * $pembulatan_tipe)) . "</td>";
                    if($pembulatan_tipe == 1)
                        $kal .= "<td class='text-center'> D </td>";
                    else 
                        $kal .= "<td class='text-center'> K </td>";
                    $kal .= "<td class='text-center'>". $pembulatan_id_cc ."</td>";
                    $kal .= "<td class='text-left'>". $pembulatan_desc ."</td>";
                $kal .= "</tr>";
            }
            $kal .= '   </tbody>
                    </table>';
            $kal .= "</div>";
            $kal .= "</div>";
        echo $kal;
    }
    public function payment_insert(){
        $pembulatan_id_gl = $this->i_p("pg");
        $pembulatan_tipe = $this->i_p("pt");
        $pembulatan_amount = $this->i_p("pa");
        $pembulatan_id_cc = $this->i_p("pc");
        $pembulatan_desc = $this->i_p("pd");
        $tanggal = $this->i_p("t");
        $reference = $this->i_p("r");
        $text = $this->i_p("te");
        $assignment = $this->i_p("as");
        $id_gl = $this->i_p("ig");
        $ctr = $this->i_p("cv");
        

        $arr_id_vendor = $this->i_p("av");
        $arr_total = $this->i_p("at");
        $arr_detail_h = $this->i_p("ad");
        $arr_idx_detail_h = $this->i_p("ai");
        $arr_amount_detail_h = $this->i_p("aa");

        
        echo MESSAGE[$this->payment_model->voucher_insert(
            $ctr,$tanggal,$reference,$text,$assignment,$id_gl,
            $pembulatan_id_gl,$pembulatan_tipe,$pembulatan_amount,$pembulatan_id_cc,$pembulatan_desc,
            $arr_id_vendor,$arr_total,$arr_detail_h,$arr_idx_detail_h,$arr_amount_detail_h
        )];
    }

    public function voucher_detail_plus(){
        $ctr = $this->i_p("c");
        $data_list = "";
        //GL Datalist
        $data_list .= "<datalist id='list_gl'>";
        $data_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, null, null, null, null, false);
        if ($data_gl->num_rows() > 0) {
            foreach ($data_gl->result_array() as $row_gl) {
                $data_list .= "<option value=" . $row_gl[General_Ledger::$ID] . ">" . $row_gl[General_Ledger::$DESKRIPSI] . "</option>";
            }
        }
        $data_list .= "</datalist>";

        //CC Datalist
        $data_list .= "<datalist id='list_cc'>";
        $data_cc = $this->cc_model->get(Cost_Center::$TABLE_NAME);
        if ($data_cc->num_rows() > 0) {
            foreach ($data_cc->result_array() as $row_cc) {
                $data_list .= "<option value=" . $row_cc[Cost_Center::$ID] . ">" . $row_cc[Cost_Center::$NAMA] . "</option>";
            }
        }
        $data_list .= "</datalist>";

        //Other Material Ordered Datalist
        $data_list .= "<datalist id='list_order'>";
        $data_order = $this->payment_model->get_newest(Payment_Voucher_Detail_D::$TABLE_NAME,Payment_Voucher_Detail_D::$ID_MATERIAL,Payment_Voucher_Detail_D::$ID,10);
        if ($data_order->num_rows() > 0) {
            foreach ($data_order->result_array() as $row_order) {
                if($row_order[Payment_Voucher_Detail_D::$ID_MATERIAL]!=null){
                    $row_material= $this->other_model->get(Other_Material::$TABLE_NAME,Other_Material::$ID, $row_order[Payment_Voucher_Detail_D::$ID_MATERIAL],null,null,false)->row_array();
                    $data_list .= "<option value=" . $row_order[Payment_Voucher_Detail_D::$ID_MATERIAL] . ">" . $row_material[Other_Material::$DESKRIPSI] . "</option>";
                }
            }
        }
        $data_list .= "</datalist>";

        //Assignment Ordered Datalist
        $data_list .= "<datalist id='list_assignment'>";
        $data_assignment = $this->payment_model->get_newest(Payment_Voucher_Detail_D::$TABLE_NAME, Payment_Voucher_Detail_D::$ASSIGNMENT, Payment_Voucher_Detail_D::$ID, 10);
        if ($data_assignment->num_rows() > 0) {
            foreach ($data_assignment->result_array() as $row_assignment) {
                $data_list .= "<option value=" . $row_assignment[Payment_Voucher_Detail_D::$ASSIGNMENT] . ">" . $row_assignment[Payment_Voucher_Detail_D::$ASSIGNMENT] . "</option>";
                
            }
        }
        $data_list .= "</datalist>";

        //Description Ordered Datalist
        $data_list .= "<datalist id='list_desc'>";
        $data_desc = $this->payment_model->get_newest(Payment_Voucher_Detail_D::$TABLE_NAME, Payment_Voucher_Detail_D::$DESCRIPTION, Payment_Voucher_Detail_D::$ID, 10);
        if ($data_desc->num_rows() > 0) {
            foreach ($data_desc->result_array() as $row_desc) {
                $data_list .= "<option value=" . $row_desc[Payment_Voucher_Detail_D::$DESCRIPTION] . ">" . $row_assignment[Payment_Voucher_Detail_D::$DESCRIPTION] . "</option>";
            }
        }
        $data_list .= "</datalist>";

        $kal = '
                <tr class="tr_detail_payment_' . $ctr . '">
                    <td class="text-center font-sm">' . $data_list . ($ctr + 1) . '</td>
                    <td class="text-center font-sm" style="width:10%"> 
                        <input style="width:100%" list="list_gl" onkeyup="check_gl(' . $ctr . ')" id="txt_id_gl_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm" style="width:20%"> 
                        <input style="width:100%" class="gl green-text" disabled id="txt_desc_gl_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm" style="width:10%">                     
                        <input class="text-left" onkeyup="calculate_total()" style="width:100%" type="number" id="txt_nominal_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm" style="width:10%"> 
                        <input style="width:100%" list="list_cc" id="txt_id_cc_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm"style="width:6%">                     
                        <select style="width:100%;font-size:120%" id="dd_tipe_' . $ctr . '" onchange="calculate_total()">
                            <option value=1>D</option>
                            <option value=-1>K</option>
                        </select>
                    </td>
                    <td class="text-center font-sm" style="width:25%">                     
                        <input style="width:100%" list="list_desc" type="text" id="txt_desc_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm"style="width:10%">                     
                        <input style="width:100%" list="list_assignment" type="text" id="txt_assignment_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm"style="width:13%">                     
                        <input style="width:100%" list="list_order" type="text" id="txt_order_' . $ctr . '"/>
                    </td>
                    <td style="width:2%" class="font-sm text-center"> 
                        <button style="width:90%;height:50%" type="button" class="red-text" onclick="minus_payment(' . $ctr . ')" data-mdb-ripple-color="dark">
                            X
                        </button>
                    </td>
                </tr>';
        
        echo $kal;
    }
    public function voucher_detail_plus_tax(){
        $ctr = $this->i_p("c");
        $id_tax = $this->i_p("it");
        $id_gl= $this->payment_model->get(Taxes::$TABLE_NAME,Taxes::$ID,$id_tax)->row_array()[Taxes::$ID_GL];

        $row_gl=$this->gl_model->get(General_Ledger::$TABLE_NAME,General_Ledger::$ID,$id_gl)->row_array();

        $kal = '
                <tr class="tr_detail_payment_' . $ctr . '">
                    <td class="text-center font-sm">' . ($ctr + 1) . '</td>
                    <td class="text-center font-sm" style="width:10%"> 
                        <input disabled style="width:100%" list="list_gl" value="'. $row_gl[General_Ledger::$ID] .'" id="txt_id_gl_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm" style="width:20%"> 
                        <input disabled style="width:100%" class="gl green-text" value="'. $row_gl[General_Ledger::$DESKRIPSI] .'" id="txt_desc_gl_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm" style="width:10%">                     
                        <input disabled class="text-left"  style="width:100%" type="number" id="txt_nominal_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm" style="width:10%"> 
                        <input disabled style="width:100%" value=10102500 id="txt_id_cc_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm"style="width:6%">                     
                        <select disabled style="width:100%;font-size:120%" id="dd_tipe_' . $ctr . '" onchange="calculate_total()">
                            <option value=1>D</option>
                            <option value=-1>K</option>
                        </select>
                    </td>
                    <td class="text-center font-sm" style="width:25%">                     
                        <input disabled style="width:100%" type="text" id="txt_desc_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm"style="width:10%">                     
                        <input disabled style="width:100%" type="text" id="txt_assignment_' . $ctr . '"/>
                    </td>
                    <td class="text-center font-sm"style="width:13%">                     
                        <input disabled style="width:100%" type="text" id="txt_order_' . $ctr . '"/>
                    </td>
                    <td style="width:2%" class="font-sm text-center"> 
                        <button disabled style="width:90%;height:50%" type="button" class="red-text" onclick="minus_payment(' . $ctr . ')" data-mdb-ripple-color="dark">
                            X
                        </button>
                    </td>
                </tr>';
        
        echo $kal;
    }
    public function voucher_detail_view_rekap($id){
        $id_header = $id;
        $data = [];
        $data["message"] = MESSAGE[Payment_Voucher_Detail_H::$MESSAGE_SUCCESS_INSERT];
        $kal = "";

        $arr_header = [
            "Account", "Nama", "Deskripsi", "Amount", "Tipe", "CC", "Assignment", "Order"
        ];

        
        //Header Rekap
        $qry_header = $this->payment_model->get(Payment_Voucher_Detail_H::$TABLE_NAME, Payment_Voucher_Detail_H::$ID, $id_header, null, null, false);
        if ($qry_header->num_rows() > 0) {
            $row_header = $qry_header->row_array();
            $kal .= '<div class="row">';
            $disabled = "";
            if(!$this->equals($row_header[Payment_Voucher_Detail_H::$ID_CANCEL],null))
                $disabled= "disabled";
            $kal .= '<div class="col-sm-12 text-center">
                        <button type="button" '. $disabled .' id="btn_edit" class=" center btn btn-outline-warning" onclick="edit()" data-mdb-ripple-color="dark">
                        Ubah
                        </button>
                        <button type="button" '. $disabled .' id="btn_update" disabled class=" center btn btn-outline-success" onclick="update()" data-mdb-ripple-color="dark">
                        Simpan
                        </button>
                        <button type="button" '. $disabled .' id="btn_cancel" class=" center btn btn-outline-danger" onclick="cancel()" data-mdb-ripple-color="dark">
                        Cancel
                        </button>
                    </div>';
            $kal .= '</div>';
            $kal .= '<div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-4">
                            <table>
                                <tr>
                                    <td class="text-right fw-bold">Nomor Header</td>
                                    <td>:</td>
                                    <td class="text-left"><span id="txt_header">' . $row_header[Payment_Voucher_Detail_H::$ID] . '</span></td>
                                </tr>
                                <tr>
                                    <td class="text-right fw-bold">Tanggal</td>
                                    <td>:</td>
                                    <td class="text-left">' . $row_header[Payment_Voucher_Detail_H::$TANGGAL] . '</td>
                                </tr>
                                <tr>
                                    <td class="text-right fw-bold">Reference</td>
                                    <td>:</td>
                                    <td class="text-left">' . $row_header[Payment_Voucher_Detail_H::$REFERENCE] . '</td>
                                </tr>

                            </table>
                        </div>
                        <div class="col-sm-4">
                            <table>
                                <tr>
                                    <td class="text-right fw-bold">Currency</td>
                                    <td>:</td>
                                    <td class="text-left"> IDR </td>
                                </tr>
                                <tr>
                                    <td class="text-right fw-bold">Tahun</td>
                                    <td>:</td>
                                    <td class="text-left">' . date("Y", strtotime($row_header[Payment_Voucher_Detail_H::$TANGGAL])) . '</td>
                                </tr>
                                <tr>
                                    <td class="text-right fw-bold">Bulan</td>
                                    <td>:</td>
                                    <td class="text-left">' . date("F", strtotime($row_header[Payment_Voucher_Detail_H::$TANGGAL])) . '</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>';
            $kal .= '<div class="row">
                        <div class="col-sm-2 text-right">
                            Text :
                        </div>
                        <div class="col-sm-8">
                            <input disabled value="'. $row_header[Payment_Voucher_Detail_H::$TEXT] . '"type="text" style="width:100%" id="txt_text">
                        </div>
                        <div class="col-sm-2"></div>
                    </div>
                    <br><br>';
            $kal .= '<div class="row" style="margin-left:2%;margin-right:1%">';
            $kal .= '<div class="col-sm-12">';

                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>' . $this->gen_table_header($arr_header) . '</thead>
                            <tbody>';
                //Detail Rekap
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row_header[Payment_Voucher_Detail_H::$ID_VENDOR] . "</td>";
                $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_header[Payment_Voucher_Detail_H::$ID_VENDOR])->row_array();
                $kal .= "<td class='text-left'>" . $vendor[Vendor::$NAMA] . "</td>";
                $kal .= "<td class='text-center'> - </td>";
                $kal .= "<td class='text-right'> - " . $this->rupiah(abs($row_header[Payment_Voucher_Detail_H::$TOTAL])) . "</td>";
                $kal .= "<td class='text-center'> K </td>";
                $kal .= "<td class='text-center'> - </td>";
                $kal .= "<td class='text-center'> - </td>";
                $kal .= "<td class='text-center'> - </td>";

                $kal .= "</tr>";

                $data_detail = $this->payment_model->get(Payment_Voucher_Detail_D::$TABLE_NAME, Payment_Voucher_Detail_D::$ID_HEADER, $id_header, null, null, false);
                if ($data_detail->num_rows() > 0) {
                    $ctr = 1; 
                    foreach ($data_detail->result_array() as $row_detail) {
                        $kal .= "<tr>";
                        $kal .= "<td class='text-center align-middle'>" . $row_detail[Payment_Voucher_Detail_D::$ID_GL] . "</td>";
                        $gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $row_detail[Payment_Voucher_Detail_D::$ID_GL], null, null, false)->row_array();
                        $kal .= "<td class='text-left align-middle'>" . $gl[General_Ledger::$DESKRIPSI] . "</td>";
                        $kal .= '<td class="text-left"><input title="'. $row_detail[Payment_Voucher_Detail_D::$DESCRIPTION] .'"style="width:100%" id="txt_desc_'. $ctr .'" disabled value="' . $row_detail[Payment_Voucher_Detail_D::$DESCRIPTION] . '"/></td>';
                        $tipe = "D";
                        if ($row_detail[Payment_Voucher_Detail_D::$TIPE] == -1){
                            $tipe = "K";

                        }
                        $kal .= "<td class='text-right align-middle'>" . $this->rupiah($row_detail[Payment_Voucher_Detail_D::$AMOUNT]* $row_detail[Payment_Voucher_Detail_D::$TIPE]) . "</td>";

                        $kal .= "<td class='text-center align-middle'>" . $tipe . "</td>";
                        $kal .= "<td class='text-center align-middle'>" . $row_detail[Payment_Voucher_Detail_D::$ID_CC] . "</td>";
                        $kal .= "<td style='width:0%' class='text-center align-middle'>" . '<input disabled value="' . $row_detail[Payment_Voucher_Detail_D::$ASSIGNMENT] . '"type="text" style="width:100%" id="txt_assignment_'.$ctr.'">' . "</td>";

                        $material = $this->other_model->get(Other_Material::$TABLE_NAME, Other_Material::$ID, $row_detail[Payment_Voucher_Detail_D::$ID_MATERIAL], null, null, false)->row_array();

                        $kal .= "<td class='text-center align-middle blue-text' title='" . $material[Other_Material::$DESKRIPSI] . "'>" . $row_detail[Payment_Voucher_Detail_D::$ID_MATERIAL] . "</td>";
                        $kal .= "<input type=hidden id='txt_detail_".$ctr."' value='". $row_detail[Payment_Voucher_Detail_D::$ID] ."'>";
                        $kal .= "</tr>";
                        $ctr++;
                    }
                }
                $kal .= '   </tbody>
                        </table>';
                $kal .= "<input type=hidden id='txt_ctr' value=" . $ctr . ">";
                
                $kal .= "</div>"; //col-sm-10
            $kal .= "</div>"; //row
            $data["kal"] = $kal;
        } else {
            $data["message"] = MESSAGE[Payment_Voucher_Detail_H::$MESSAGE_FAILED_SEARCH_NOT_FOUND];
        }
        echo json_encode($data);
    }
    public function voucher_detail_view_modal_preview(){
        $kal="";
        $id_vendor = $this->i_p("iv");
        $tanggal = $this->i_p("t");
        $reference = $this->i_p("r");
        $total = $this->i_p("to");
        $ctr = $this->i_p("c");

        $arr_id_gl = $this->i_p("ag");
        $arr_nominal = $this->i_p("an");
        $arr_id_cc = $this->i_p("ac");
        $arr_tipe = $this->i_p("at");
        $arr_description = $this->i_p("ad");
        $arr_assignment = $this->i_p("aa");
        $arr_order = $this->i_p("ao");

        $arr_header = [
            "Vendor", "Nama", "Deskripsi", "Amount", "Tipe", "CC", "Assignment", "Order"
        ];

        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12 text-center">';

        //Header Rekap
        $kal .= '<div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        <table>
                            <tr>
                                <td class="text-right fw-bold">Tanggal</td>
                                <td>:</td>
                                <td class="text-left">'. $tanggal .'</td>
                            </tr>
                            <tr>
                                <td class="text-right fw-bold">Reference</td>
                                <td>:</td>
                                <td class="text-left">' . $reference . '</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-4">
                        <table>
                            <tr>
                                <td class="text-right fw-bold">Currency</td>
                                <td>:</td>
                                <td class="text-left"> IDR </td>
                            </tr>
                            <tr>
                                <td class="text-right fw-bold">Tahun</td>
                                <td>:</td>
                                <td class="text-left">' . date("Y", strtotime($tanggal)) . '</td>
                            </tr>
                            <tr>
                                <td class="text-right fw-bold">Bulan</td>
                                <td>:</td>
                                <td class="text-left">' . date("F", strtotime($tanggal)) . '</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-2"></div>
                </div>';
            $kal .= '<br><table id="mastertable" class="table table-bordered table-sm">
                        <thead>' . $this->gen_table_header($arr_header) . '</thead>
                        <tbody>';

            //Detail Rekap
            $kal .= "<tr>";
            $kal .= "<td class='text-center'>" . $id_vendor . "</td>";
            $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $id_vendor)->row_array();
            $kal .= "<td class='text-left'>" . $vendor[Vendor::$NAMA] . "</td>";
            $kal .= "<td class='text-center'> - </td>";
            $kal .= "<td class='text-right'> - " . $this->rupiah(abs($total)) . "</td>";
            $kal .= "<td class='text-center'> K </td>";
            $kal .= "<td class='text-center'> - </td>";
            $kal .= "<td class='text-center'> - </td>";
            $kal .= "<td class='text-center'> - </td>";

            $kal .= "</tr>";

            
            for ($i = 0;$i < $ctr; $i++) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $arr_id_gl[$i] . "</td>";
                $gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, General_Ledger::$ID, $arr_id_gl[$i])->row_array();
                $kal .= "<td class='text-left'>" . $gl[General_Ledger::$DESKRIPSI] . "</td>";
                $kal .= "<td class='text-left'>" . $arr_description[$i] . "</td>";
                $kal .= "<td class='text-right'>" . $this->rupiah($arr_nominal[$i] * $arr_tipe[$i]) . "</td>";
                $tipe = "D";
                if ($arr_tipe[$i] == -1)
                    $tipe = "K";

                $kal .= "<td class='text-center'>" . $tipe . "</td>";
                $row_cc=$this->cc_model->get(Cost_Center::$TABLE_NAME,Cost_Center::$ID, $arr_id_cc[$i])->row_array();
                $kal .= "<td class='text-center blue-text' title='". $row_cc[Cost_Center::$NAMA] ."'>" . $arr_id_cc[$i] . "</td>";
                $kal .= "<td class='text-center'>" . $arr_assignment[$i] . "</td>";
                $material = $this->other_model->get(Other_Material::$TABLE_NAME, Other_Material::$ID, $arr_order[$i], null, null, false)->row_array();

                $kal .= "<td class='text-center blue-text' title='" . $material[Other_Material::$DESKRIPSI] . "'>" . $arr_order[$i] . "</td>";

                $kal .= "</tr>";
            }

            $kal .= '   </tbody>
                    </table>';
            $kal .= "</div>";
            $kal .= "</div>";
        echo $kal;
    }
    public function voucher_detail_insert(){
        
        $id_vendor = $this->i_p("iv");
        $tanggal = $this->i_p("t");
        $reference = $this->i_p("r");
        $text = $this->i_p("te");
        $tax = $this->i_p("ta");
        $total = $this->i_p("to");
        $ctr = $this->i_p("c");

        $arr_id_gl = $this->i_p("ag");
        $arr_nominal = $this->i_p("an");
        $arr_id_cc = $this->i_p("ac");
        $arr_tipe = $this->i_p("at");
        $arr_description = $this->i_p("ad");
        $arr_assignment = $this->i_p("aa");
        $arr_order = $this->i_p("ao");

        echo MESSAGE[$this->payment_model->voucher_detail_insert(
            $ctr,$id_vendor,$tanggal,$reference,$text,$tax,$total,
            $arr_id_gl,$arr_nominal,$arr_id_cc,$arr_tipe,$arr_description,$arr_assignment,$arr_order
        )];
    }
    public function voucher_detail_update(){
        
        $header = $this->i_p("h");
        $text = $this->i_p("te");

        $arr_assignment = $this->i_p("aa");
        $arr_detail = $this->i_p("ad");
        $arr_desc = $this->i_p("an");
        $ctr = $this->i_p("c");

        echo MESSAGE[$this->payment_model->voucher_detail_update(
            $ctr,$header,$text,$arr_detail,$arr_assignment,$arr_desc
        )];
    }
    public function voucher_detail_cancel($id,$reason){
        echo MESSAGE[$this->payment_model->voucher_detail_cancel($id,$reason)];
    }
    
    
    
    public function jurnal_lain_insert(){
        $data_rekap = json_decode($this->i_p('data_rekap'), true);
        $tanggal = $this->i_p('ta');
        $reference = $this->i_p('r');
        $text = $this->i_p('te');

        echo MESSAGE[$this->payment_model->jurnal_lain_insert($tanggal, $reference, $text, $data_rekap)];
    }
}

