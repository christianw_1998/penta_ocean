<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Settings_Controller extends Library_Controller {

	public function change_password(){
		$current_password = $this->i_p("cp");
		$new_password = $this->i_p("np");
		$confirm_new_password = $this->i_p("cnp");
		echo MESSAGE[$this->settings_model->change_password($current_password, $new_password, $confirm_new_password)];
	}

	public function change_nomor_sj(){
		$old_sj = $this->i_p("os");
		$new_sj = $this->i_p("ns");
		$sto = $this->i_p("s");
		echo MESSAGE[$this->settings_model->change_no_sj($old_sj, $new_sj, $sto)];
	}
	
	public function user_access($params){
		$this->user_access($params);
	}

}
