<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class CC_Controller extends Library_Controller {
    
    public function upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx|xls';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_cc";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 2;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->cc_model->update_batch($data);
    }

    public function get_all(){
        $data = $this->cc_model->get_active();
        $temp = [];
        $ctr = 0;
        foreach ($data->result_array() as $row) {
            $temp[$ctr][Cost_Center::$ID] = $row[Cost_Center::$ID];
            $temp[$ctr][Cost_Center::$NAMA] = $row[Cost_Center::$NAMA];
            $ctr++;
        }
        echo json_encode($temp);
    }

    public function get_by_id(){
        $id = $this->i_p("i");
        
        $data = $this->cc_model->get(Cost_Center::$TABLE_NAME,Cost_Center::$ID,$id);
        $temp=[];
        if ($data->num_rows() > 0) {
            $cc= $data->row_array();
            $temp[Cost_Center::$NAMA] = $cc[Cost_Center::$NAMA];
            $temp[Cost_Center::$ID] = $cc[Cost_Center::$ID];
                    
        }        
        echo json_encode($temp);
    }
}
