<?php
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Welcome extends CI_Controller {
	
	//Static Variables
	public static $UANG_HADIR;
	public static $UANG_LUAR_KOTA;

	public static $MESSAGE_UPLOAD_SUCCESS="FILE BERHASIL DI UPLOAD!!";
	public static $MESSAGE_UPLOAD_ERROR_FILE_NOT_SELECTED="YOU DID NOT SELECT A FILE TO UPLOAD";
	public static $MESSAGE_UPLOAD_ERROR_NOT_ALLOWED_FILETYPE="THE FILETYPE YOU ARE ATTEMPTING TO UPLOAD IS NOT ALLOWED";

	//All Navigation
	public static $NAV_ABSENSI_GAJI="GAJI_ABSENSI";
	public static $NAV_ABSENSI_TABEL="ABSENSI_TABEL";
	public static $NAV_ABSENSI_PERFORMANCE="ABSENSI_PERFORMANCE";
	public static $NAV_SLIP_GAJI="SLIP_GAJI";
	public static $NAV_REKAP_GAJI="REKAP_GAJI";
	public static $NAV_REKAP_PERFORMANCE="REKAP_PERFORMANCE";
	public static $NAV_GOOD_RECEIPT="GOOD_RECEIPT";
	public static $NAV_GOOD_RECEIPT_ADJUSTMENT="GOOD_RECEIPT_ADJUSTMENT";
	public static $NAV_MATERIAL_FISIK="MATERIAL_FISIK";
	public static $NAV_GOOD_ISSUE="GOOD_ISSUE";
	public static $NAV_GOOD_ISSUE_ADJUSTMENT="GOOD_ISSUE_ADJUSTMENT";
	public static $NAV_USER_ACCESS="USER_ACCESS";
	public static $NAV_USER_ACCESS_NO_RESTRICTION="USER_ACCESS_NO_RESTRICTION";
	public static $NAV_KINERJA_GOOD_ISSUE="KINERJA_GOOD_ISSUE";
	public static $NAV_WORK_SHEET_ADMIN_PROSES= "WS_ADMIN_PROSES_INSERT";
	public static $NAV_WORK_SHEET_CHECKER_FG_INSERT_GR= "WS_CHECKER_FG_INSERT_GR";
	public static $NAV_PURCHASE_ORDER_REVISI_QTY= "PO_REVISI_QTY";
	public static $NAV_RM_PM_GOOD_RECEIPT_MUTASI= "GOOD_RECEIPT_RM_PM_MUTASI";
	public static $NAV_RM_PM_GOOD_ISSUE_MUTASI= "GOOD_ISSUE_RM_PM_MUTASI";

	
	/*public function settings($type,$type2 = NULL){
		if($this->equals($type,SETTING_CHANGE_PASSWORD)){
			$current_password = $this->i_p("cp");
			$new_password = $this->i_p("np");
			$confirm_new_password = $this->i_p("cnp");
			echo MESSAGE[$this->model->change_password($current_password, $new_password, $confirm_new_password)];
		}
		if($this->equals($type,SETTING_USER_ACCESS)){
			$this->user_access($type2);
		}
		if($this->equals($type,SETTING_CHANGE_NO_SJ)) {
			$old_sj = $this->i_p("os");
			$new_sj = $this->i_p("ns");
			$sto = $this->i_p("s");
			echo MESSAGE[$this->model->change_no_sj($old_sj, $new_sj, $sto)];
		}
	}*/

	/*public function update_all_good(){
		$this->model->update_good();
	}*/

	//kinerja
	public function kinerja($params){
		if($this->equals($params, KINERJA_GEN_LAPORAN)){
			$tanggal_dari = $this->i_p("dt");
			$tanggal_sampai = $this->i_p("st");
			$kal="";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-2"></div>';
			$kal .= '<div class="col-sm-8">';
			$kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Nama</th>
									<th scope="col" class="text-center">Container (KG)</th>
									<th scope="col" class="text-center">LCL (KG)</th>
									<th scope="col" class="text-center">Pail (KG)</th>
									<th scope="col" class="text-center">Non Pail (KG)</th>
									<th scope="col" class="role-kepala text-center">Insentif (Rp)</th>
									';
			$kal .= '			</tr>
							</thead><tbody>';
				
			//Outsource
			$data = $this->get_kinerja_by($tanggal_dari, $tanggal_sampai);
				$kal .= "<tr>";
					$kal .= "<td class='text-center'>" . $data[KINERJA_KEY_TIPE] . "</td>";
					$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_CONTAINER]) . "</td>";
					$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_LCL]) . "</td>";
					$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_PAIL]) . "</td>";
					$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL]) . "</td>";
					$insentif=0;
					$temp=$this->model->get(Standar_Insentif::$TABLE_NAME,Standar_Insentif::$NAMA,S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_DUS)->row_array()[Standar_Insentif::$NOMINAL]*$data[KINERJA_KEY_NON_PAIL];	
					$insentif+=$temp;
					$temp= $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_PAIL)->row_array()[Standar_Insentif::$NOMINAL]*$data[KINERJA_KEY_PAIL];
					$insentif+=$temp;
					$temp=$this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_CONTAINER)->row_array()[Standar_Insentif::$NOMINAL]*$data[KINERJA_KEY_CONTAINER];
					$insentif+=$temp;
					$temp=$this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_LCL)->row_array()[Standar_Insentif::$NOMINAL]*$data[KINERJA_KEY_LCL];
					$insentif+=$temp;
					$kal .= "<td class='role-kepala text-center'>" . $this->rupiah($insentif) . "</td>";
				$kal .= "</tr>";

			//Sopir
			$arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori",KARYAWAN_SUB_KATEGORI_SOPIR);
			if($arr_karyawan->num_rows() > 0){
				foreach($arr_karyawan->result_array() as $row){
					$data = $this->get_kinerja_by($tanggal_dari, $tanggal_sampai,$row[Karyawan::$ID]);
					$kal .= "<tr>";
						$kal .= "<td class='text-center'>(SOPIR) " . $data[KINERJA_KEY_TIPE] . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_CONTAINER]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_LCL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_PAIL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL]) . "</td>";
						$insentif = 0;
						if($data[KINERJA_KEY_LCL]>30000)
							$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_SOPIR_HIGHER_THAN_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
						else
							$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_SOPIR_LESS_THAN_EQUAL_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
						$insentif += $temp;
						$kal .= "<td class='role-kepala text-center'>" . $this->rupiah($insentif) . "</td>";
					$kal .= "</tr>";
				}
			}
			//Sopir FK
			$arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori",KARYAWAN_SUB_KATEGORI_GENERAL_SOPIR_FK);
			if($arr_karyawan->num_rows() > 0){
				foreach($arr_karyawan->result_array() as $row){
					$data = $this->get_kinerja_by($tanggal_dari, $tanggal_sampai,$row[Karyawan::$ID]);
					$kal .= "<tr>";
						$kal .= "<td class='text-center'>(SOPIR FK) " . $data[KINERJA_KEY_TIPE] . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_CONTAINER]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_LCL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_PAIL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL]) . "</td>";
						$insentif = 0;
						$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_SOPIR_FK_DUS)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_NON_PAIL];
						$insentif += $temp;
						$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_SOPIR_FK_CONTAINER)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_CONTAINER];
						$insentif += $temp;
						$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_SOPIR_FK_LCL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
						$insentif += $temp;
						$kal .= "<td class='role-kepala text-center'>" . $this->rupiah($insentif) . "</td>";
					$kal .= "</tr>";
				}
			}
			//Kernet
			$arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori",KARYAWAN_SUB_KATEGORI_KERNET);
			if($arr_karyawan->num_rows() > 0){
				foreach($arr_karyawan->result_array() as $row){
					$data = $this->get_kinerja_by($tanggal_dari, $tanggal_sampai,$row[Karyawan::$ID]);
					$kal .= "<tr>";
						$kal .= "<td class='text-center'>(KERNET) " . $data[KINERJA_KEY_TIPE] . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_CONTAINER]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_LCL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_PAIL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL]) . "</td>";
						$insentif = 0;
						if ($data[KINERJA_KEY_LCL] > 30000)
							$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_KERNET_HIGHER_THAN_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
						else
							$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_KERNET_LESS_THAN_EQUAL_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
						$insentif += $temp;
						$kal .= "<td class='role-kepala text-center'>" . $this->rupiah($insentif) . "</td>";
					$kal .= "</tr>";
				}
			}
			//Checker
			$arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori",KARYAWAN_SUB_KATEGORI_GENERAL_CHECKER);
			if($arr_karyawan->num_rows() > 0){
				foreach($arr_karyawan->result_array() as $row){
					$data = $this->get_kinerja_by($tanggal_dari, $tanggal_sampai,$row[Karyawan::$ID]);
					$kal .= "<tr>";
						$kal .= "<td class='text-center'>(CHECKER) " . $data[KINERJA_KEY_TIPE] . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_CONTAINER]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_LCL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_PAIL]) . "</td>";
						$kal .= "<td class='text-center'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL]) . "</td>";
						$insentif = 0;
						$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_DUS)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_NON_PAIL];
						$insentif += $temp;
						$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_PAIL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_PAIL];
						$insentif += $temp;
						$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_CONTAINER)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_CONTAINER];
						$insentif += $temp;
						$temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_LCL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
						$insentif += $temp;
						$kal .= "<td class='role-kepala text-center'>" . $this->rupiah($insentif) . "</td>";
					$kal .= "</tr>";
				}
			}

			$kal .= '	</tbody>
					</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-2"></div>';
			$kal .= "</div>";
			echo $kal;
		}
		else if($this->equals($params, KINERJA_GET_ALL_GOOD_ISSUE)){
			$kal="";
			$data=$this->model->kinerja_get_all_distinct_nomor_sj_good_issue();
			
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-4"></div>';
				$kal.='<div class="col-sm-4">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Nomor Surat Jalan</th>
									<th scope="col" class="text-center">Aksi</th>
									';
					$kal.='		</tr>
							</thead><tbody>';
					if($data->num_rows() > 0){
						foreach($data->result_array() as $row){
							//if(is_numeric($row[H_Good_Issue::$NOMOR_SJ])){
								$text_color="red-text";
					
								if($row[H_Good_Issue::$ID_EKSPEDISI]!=null)
									$text_color="green-text";
								
								$kal.="<tr class='".$text_color."'>";
									$kal.="<td class='text-center'>".$row[H_Good_Issue::$NOMOR_SJ]."</td>";
									$kal.="<td class='text-center'>".'<button onclick="fill_detail_modal('."'".$row[H_Good_Issue::$NOMOR_SJ]."'".')" class="btn btn-outline-primary" data-mdb-ripple-color="dark">Ubah</button>
									'."</td>";
								$kal.="</tr>";
							//}
						}
					}
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
			echo $kal;
		}
		else if($this->equals($params, KINERJA_GET_VIEW_DETAIL_SJ)){
			$data=[];
			$sj=$this->i_p("n");
			$temp=$this->model->get_good_issue_by_sj($sj);
			if($temp->num_rows()>0){
				$h_good_issue=$temp->row_array();
				$data["tipe_ekspedisi"]=$h_good_issue[H_Good_Issue::$EKSPEDISI];
				if($h_good_issue[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]!=null)
					$data["tanggal"]=date("Y-m-d",strtotime($h_good_issue[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]));
				else
					$data["tanggal"]="";

				//array ekspedisi
				$kal="";
				$arr_ekspedisi = $this->model->get(Ekspedisi::$TABLE_NAME, null, null, Ekspedisi::$NAMA_EKSPEDISI);
				$kal .= '<option value=-1>--PILIH EKSPEDISI--</option>';
				if($arr_ekspedisi->num_rows()>0){
					foreach($arr_ekspedisi->result_array() as $row2){
						$selected="";
						$tipe="";
						if($row2[Ekspedisi::$ID]==$h_good_issue[H_Good_Issue::$ID_EKSPEDISI])
							$selected="selected";
						
						$tampilan= $row2[Ekspedisi::$INISIAL];
						if($row2[Ekspedisi::$TUJUAN]!=null)
							$tampilan .= " - " . $row2[Ekspedisi::$TUJUAN];
						if($row2[Ekspedisi::$IS_TYPE_LCL]){
							$tipe = KINERJA_KEY_LCL;
							$kal.="<option $selected class='ekspedisi_".$tipe."' value='".$row2[Ekspedisi::$ID]."'>".$tampilan."</option>";
						}
						if($row2[Ekspedisi::$IS_TYPE_CONTAINER]){
							$tipe = KINERJA_KEY_CONTAINER;
							$kal.="<option $selected class='ekspedisi_".$tipe."' value='".$row2[Ekspedisi::$ID]."'>".$tampilan."</option>";
						}
						if ($row2[Ekspedisi::$IS_TYPE_SIRCLO]) {
							$tipe = KINERJA_KEY_SIRCLO;
							$kal .= "<option $selected class='ekspedisi_" . $tipe . "' value='" . $row2[Ekspedisi::$ID] . "'>" . $tampilan . "</option>";
						}
					}
				}
				$data['opt_ekspedisi']=$kal;

				//array supir
				$kal="";
				$kal.='<option value=-1>TIDAK ADA</option>';
				$arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori",KARYAWAN_SUB_KATEGORI_SOPIR);
				if($arr_karyawan->num_rows()>0){
					foreach($arr_karyawan->result_array() as $row2){
						$selected="";
						if($row2[Karyawan::$ID]==$h_good_issue[H_Good_Issue::$NIK_SOPIR])
							$selected="selected";
						$karyawan=$this->karyawan_model->get_by(Karyawan::$ID,$row2[Karyawan::$ID])->row();
						$kal.="<option $selected value='".$row2[Karyawan::$ID]."'>".$karyawan->k_nama."</option>";
					}
				}
				$data['opt_sopir']=$kal;

				//array kernet
				$kal="";
				$kal.='<option value=-1>TIDAK ADA</option>';
				$arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori",KARYAWAN_SUB_KATEGORI_KERNET);
				if($arr_karyawan->num_rows()>0){
					foreach($arr_karyawan->result_array() as $row2){
						$selected="";
						if($row2[Karyawan::$ID]==$h_good_issue[H_Good_Issue::$NIK_KERNET])
							$selected="selected";
						$karyawan=$this->karyawan_model->get_by(Karyawan::$ID,$row2[Karyawan::$ID])->row();
						$kal.="<option $selected value='".$row2[Karyawan::$ID]."'>".$karyawan->k_nama."</option>";
					}
				}
				$data['opt_kernet']=$kal;
			}
			echo json_encode($data);
		}
		else if($this->equals($params, KINERJA_INSERT_GOOD_ISSUE)){
			$this->insert($this::$NAV_KINERJA_GOOD_ISSUE);
		}
		else if ($this->equals($params, KINERJA_GEN_DETAIL_SJ)) {
			$tanggal_dari = $this->i_p("dt");
			$tanggal_sampai = $this->i_p("st");
			$tipe=$this->i_p("t");
			$header="";
			$query="";
			if($this->equals($tipe,"OUT")){
				$header= '<th scope="col" class="text-center">Tanggal</th>
							<th scope="col" class="text-center">Surat Jalan</th>
							<th scope="col" class="text-center">Ekspedisi</th>
							<th scope="col" class="text-center">Tipe</th>
							<th scope="col" class="text-center">NIK Checker</th>
							<th scope="col" class="text-center">NIK Kernet</th>
							<th scope="col" class="text-center">NIK Sopir</th>
							<th scope="col" class="text-center">Qty</th>';
				$query = $this->model->get_good_issue_by_tanggal($tanggal_dari,$tanggal_sampai);
				
			}else{
				$header = '<th scope="col" class="text-center">Tanggal</th>
							<th scope="col" class="text-center">Surat Jalan</th>
							<th scope="col" class="text-center">NIK Checker</th>
							<th scope="col" class="text-center">Qty Pail</th>
							<th scope="col" class="text-center">Qty Non Pail</th>';
				$query = $this->model->get_good_receipt_by_tanggal($tanggal_dari,$tanggal_sampai);
				
			}

			$kal = "";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-10">';
			$kal .= '<table id="mastertable" class="table table-bordered table-sm">
						<thead>
							<tr class="table-primary">
								'.$header.'
							</tr>
						</thead><tbody>';
			if($query->num_rows()>0){
				foreach($query->result_array() as $row){
					if($this->equals($tipe,"OUT")){
						$type="";
						$kal.="<tr>";
							$kal .= "<td class='text-center'>" .date('Y-m-d',strtotime($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN])). "</td>";
							$kal .= "<td class='text-center'>" .$row[H_Good_Issue::$NOMOR_SJ]. "</td>";
							
							if($row[H_Good_Issue::$ID_EKSPEDISI]!=null){
								$ekspedisi = $this->model->get(Ekspedisi::$TABLE_NAME, "id_ekspedisi", $row[H_Good_Issue::$ID_EKSPEDISI])->row_array();
								$tampil = $ekspedisi[Ekspedisi::$INISIAL];
								if($ekspedisi[Ekspedisi::$TUJUAN]!=NULL)
									$tampil.=" - ".$ekspedisi[Ekspedisi::$TUJUAN];
								$kal.= "<td class='text-left'>".$tampil."</td>";
								
								if($this->equals($row[H_Good_Issue::$EKSPEDISI],KINERJA_KEY_LCL))
									$type=KINERJA_KEY_LCL;
								else if($this->equals($row[H_Good_Issue::$EKSPEDISI],KINERJA_KEY_CONTAINER))
									$type=KINERJA_KEY_CONTAINER;

								$kal .= "<td class='text-center'>$type</td>";
							} 
							else {
								$kal .= "<td class='text-center'> - </td>";
								$kal .= "<td class='text-center'> - </td>";
							}
							
							$nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK])->row()->k_nama;
							$kal .= "<td class='blue-text text-center' title='".$nama."'>".$row[H_Good_Issue::$NIK]."</td>";
							
							if($row[H_Good_Issue::$NIK_KERNET]!=null){
								$nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK_KERNET])->row()->k_nama;
								$kal .= "<td class='blue-text text-center' title='".$nama."'>".$row[H_Good_Issue::$NIK_KERNET]."</td>";
							}
							else{
								$kal .= "<td class='text-center'> - </td>";
							}

							if ($row[H_Good_Issue::$NIK_SOPIR] != null) {
								$nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK_SOPIR])->row()->k_nama;
								$kal .= "<td class='blue-text text-center' title='".$nama."'>".$row[H_Good_Issue::$NIK_SOPIR]."</td>";
							} 
							else {
								$kal .= "<td class='text-center'> - </td>";
							}

							$data = $this->get_kinerja_by($tanggal_dari, $tanggal_sampai,NULL,$row[H_Good_Issue::$ID]);
							if(!$this->equals($type,""))
								$kal .= "<td class='text-right'>".$this->decimal($data[$type])."</td>";
							else
								$kal .= "<td class='text-center'> - </td>";
						$kal.="</tr>";
					}else{
						$kal .= "<tr>";
							$kal .= "<td class='text-center'>" .date('Y-m-d',strtotime($row[H_Good_Receipt::$TANGGAL])). "</td>";
							$kal .= "<td class='text-center'>" . $row[H_Good_Receipt::$NOMOR_SJ] . "</td>";
							$nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Receipt::$NIK])->row()->k_nama;
							$kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Receipt::$NIK] . "</td>";
							$data=$this->get_kinerja_by($tanggal_dari,$tanggal_sampai,null,$row[H_Good_Receipt::$ID]);
							$kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_PAIL]) . "</td>";
							$kal .= "<td class='text-right'>-</td>";
							$kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL]) . "</td>";
						$kal .= "</tr>";
						
					} 
				}
			}
			$kal .= '	</tbody>
					</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
			echo $kal;
		}
		else if($this->equals($params, KINERJA_EKSPEDISI_REPORT)){
			$this->ekspedisi(EKSPEDISI_GEN_LAPORAN);
		} 
	}

	//ekspedisi
	public function ekspedisi($params){
		if($this->equals($params,EKSPEDISI_GET_ALL)){
			$data=$this->model->get(Ekspedisi::$TABLE_NAME);
			$temp=[];
			$ctr=0;
			foreach($data->result_array() as $row){
				$temp[$ctr][Ekspedisi::$ID]=$row[Ekspedisi::$ID];
				$temp[$ctr][Ekspedisi::$NAMA_EKSPEDISI]=$row[Ekspedisi::$NAMA_EKSPEDISI];
				$ctr++;
			}
			echo json_encode($temp);
		} 
		else if ($this->equals($params, EKSPEDISI_GEN_LAPORAN)) {
			
		}
	}

	//user access - Done
	/*public function user_access($params){
		if($this->equals($params,USER_ACCESS_GET_ALL)){
			$query=$this->model->get_all_user_access_no_restriction();
			if($query['status']==1){
				$kal="";
				$kal.='<div class="row" style="margin-right:1%">';
					$kal.='<div class="col-sm-1"></div>';
					$kal.='<div class="col-sm-10">';
						$kal.='<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">NIK Bawahan</th>
										<th scope="col" class="text-center">NIK Atasan</th>
										<th scope="col" class="text-center">Tipe Akses</th>
										<th scope="col" class="text-center">Status</th>
										<th scope="col" class="text-center">Aksi</th>';
						$kal.='		</tr>
								</thead><tbody>';
						foreach($query['data']->result() as $row){
							$class="";
							$text="";
							if($row->is_deleted==1){
								$class='red-text';
								$text='DIHAPUS';
							}else{
								$class='green-text';
								$text='AKTIF';
							}
							$kal.="<tr>";
								$nama=$this->karyawan_model->get_by(Karyawan::$ID,$row->nik_bawahan)->row()->k_nama;
								$kal.="<td class='blue-text text-center' title='".$nama."'>".$row->nik_bawahan."</td>";
								$nama=$this->karyawan_model->get_by(Karyawan::$ID,$row->nik_atasan)->row()->k_nama;
								$kal.="<td class='blue-text text-center' title='".$nama."'>".$row->nik_atasan."</td>";
								$kal.="<td class='text-center'>".$row->description."</td>";
								$kal.="<td class='text-center $class'>".$text."</td>";
								$kal.='<td class="text-center"> 
											<button type="button" class="btn btn-outline-primary" onclick=set_user_access('.$row->nik_bawahan.','.$row->id_access.','.$row->nik_atasan.',"'."while_update".'") data-mdb-ripple-color="dark">
												Ubah
											</button>
										</td>';
							$kal.="</tr>";
						}
						$kal.='</tbody>
								</table>';
					$kal.="</div>";
					$kal.='<div class="col-sm-1"></div>';
				$kal.="</div>";
				echo $kal;
			}else{
				echo MESSAGE[$query['data']];
			}
		}else if($this->equals($params,USER_ACCESS_CHANGE_STATUS)){
			$this->insert($this::$NAV_USER_ACCESS_NO_RESTRICTION);
		}else if($this->equals($params,USER_ACCESS_INSERT_FORM)){
			$this->insert_f($this::$NAV_USER_ACCESS_NO_RESTRICTION);
		}
	}*/

	//upload function
	public function upload_data($table){
		$config[CONFIG_UPLOAD_PATH] = './asset/upload/';
		$config[CONFIG_ALLOWED_TYPES] = 'xlsx';
		$config[CONFIG_MAX_SIZE] = '5000';
		$this->load->library('upload'); 
		$this->upload->initialize($config);

		$txt_file="";
		if($this->equals($table,Karyawan::$TABLE_NAME))
			$txt_file= "txt_upl_karyawan";
		else if ($this->equals($table, Material::$TABLE_NAME) || $this->equals($table, Material_RM_PM::$TABLE_NAME)||
					$this->equals($table,Material_FG_SAP::$TABLE_NAME))
			$txt_file= "txt_upl_material";
		else if ($this->equals($table, Absensi::$TABLE_NAME))
			$txt_file= "txt_upl_abs_karyawan";
		else if ($this->equals($table, Gudang::$TABLE_NAME) || $this->equals($table, Gudang_RM_PM::$TABLE_NAME))
			$txt_file= "txt_upl_gudang";
		else if ($this->equals($table, Ekspedisi::$TABLE_NAME))
			$txt_file = "txt_upl_ekspedisi";
		// else if ($this->equals($table, Vendor::$TABLE_NAME))
		// 	$txt_file = "txt_upl_vendor";
		// // else if ($this->equals($table, MATERIAL_UPLOAD_STOCK))
		// 	$txt_file = "txt_upl_stock_material";
		// //else if($this->equals($table,Material_RM_PM_Stock::$TABLE_NAME))
		// 	//$txt_file = "txt_upl_stock_material_rm_pm";
		// else if ($this->equals($table, MATERIAL_UPLOAD_STOCK_OPNAME))
		// 	$txt_file = "txt_upl_stock_opname_tahunan";
		//else if ($this->equals($table, Purchase_Order::$TABLE_NAME))
		//	$txt_file = "txt_upl_purchase_order";
		else if ($this->equals($table, Worksheet::$TABLE_NAME))
			$txt_file = "txt_upl_work_sheet";
		/*else 
			$txt_file ="txt_upl_good";*/

		if($this->upload->do_upload($txt_file)){
			$upload_data = $this->upload->data();
			$filename = $upload_data[CONFIG_FILE_PATH].'/'.$upload_data[CONFIG_FILE_NAME];
			$message=$this->update_db($table, $filename);
			if(isset(MESSAGE[$message]))
				echo MESSAGE[$message];
			else
				echo $message;

			unlink($filename);
		}else {
			$data=$this->upload->display_errors();
			echo $this->get_message($data);
		}
	}
	public function update_db($table,$filename){
		$objPHPExcel=PHPExcel_IOFactory::load($filename);
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		$row_1=array(Material::$TABLE_NAME,Material_RM_PM::$TABLE_NAME,
						Gudang::$TABLE_NAME,Gudang_RM_PM::$TABLE_NAME,Ekspedisi::$TABLE_NAME,
						Vendor::$TABLE_NAME,strtolower(MATERIAL_UPLOAD_STOCK),
						Material_RM_PM_Stock::$TABLE_NAME,Purchase_Order::$TABLE_NAME,Worksheet::$TABLE_NAME,
						Material_FG_SAP::$TABLE_NAME);
		$row_2=array(Karyawan::$TABLE_NAME);
		$row_5=array(Absensi::$TABLE_NAME);

		$row_header=0;
		if(in_array(strtolower($table),$row_1))
			$row_header=1;
		else if(in_array(strtolower($table),$row_2))
			$row_header=2;
		else if(in_array(strtolower($table),$row_5))
			$row_header=5;
		/*else
			$row_header = 1;*/
		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
			$col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
			
			$cell= $objPHPExcel->getActiveSheet()->getCell($cell);

			if (PHPExcel_Shared_Date::isDateTime($cell)) {
				$cellValue = $cell->getValue();
				$dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
				$data_value =  date('Y-m-d', $dateValue);
			}  
			//The header will/should be in row 1 only. of course, this can be modified to suit your need.
			if ($row == $row_header) {
				$header[$row][$col] = $data_value;
			} else {
				$arr_data[$row][$col] = $data_value;
			}
		}
		
		$data=NULL;
		//send the data in an array format
		if(isset($header) && isset($arr_data)){
			$data['header'] = $header;
			$data['values'] = $arr_data;
		}

		if($this->equals($table,Karyawan::$TABLE_NAME))
			return $this->model->karyawan_update_batch($data);
		// else if ($this->equals($table, Material::$TABLE_NAME))
		// 	return $this->model->material_update_batch($data);
		else if ($this->equals($table, Material_RM_PM::$TABLE_NAME))
			return $this->model->material_rm_pm_update_batch($data);
		else if ($this->equals($table, Absensi::$TABLE_NAME))
			return $this->model->karyawan_absensi_update_batch($data);
		else if ($this->equals($table, Gudang::$TABLE_NAME))		
			return $this->model->gudang_update_batch($data);
		else if ($this->equals($table, Ekspedisi::$TABLE_NAME))		
			return $this->model->ekspedisi_update_batch($data);
		else if ($this->equals($table, Gudang_RM_PM::$TABLE_NAME))		
			return $this->model->gudang_rm_pm_update_batch($data);
		else if ($this->equals($table, Vendor::$TABLE_NAME))
			return $this->model->vendor_update_batch($data);
		// else if ($this->equals($table, MATERIAL_UPLOAD_STOCK))
		// 	return $this->model->material_stock_update_batch($data);
		// else if ($this->equals($table, MATERIAL_UPLOAD_STOCK_OPNAME))
		// 	return $this->model->material_stock_opname_tahunan_update_batch($data);
		else if ($this->equals($table, Material_RM_PM_Stock::$TABLE_NAME))
			return $this->model->material_rm_pm_stock_update_batch($data);
		// else if ($this->equals($table, Purchase_Order::$TABLE_NAME))
		// 	return $this->model->po_update_batch($data);
		// else if ($this->equals($table, Worksheet::$TABLE_NAME))
		// 	return $this->model->ws_update_batch($data);
		else if ($this->equals($table, Material_FG_SAP::$TABLE_NAME))
			return $this->model->material_fg_sap_update_batch($data);
		/*else //remark kalau sudah
			return $this->model->good_update_batch($data);*/
	}
	
	//query function
	public function master(){
		$tipe=strtoupper($this->i_p("t"));
		$kal="";
		if($this->equals($tipe,Material::$TABLE_NAME)){
			$kal .= "<h1 class='f-aleo-bold text-center'>TABEL MATERIAL FG</h1>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">ID</th>
									<th scope="col" class="text-center">Product Code</th>
									<th scope="col" class="text-center">Description</th>
									<th scope="col" class="text-center">Packing</th>
									<th scope="col" class="text-center">Box</th>
									<th scope="col" class="text-center">Net</th>
									<th scope="col" class="text-center">Gross</th>
									<th scope="col" class="text-center">Kubik</th>
									<th scope="col" class="text-center">Kode Barang</th>
									<th scope="col" class="text-center">Kemasan</th>
									<th scope="col" class="text-center">Warna</th>';
					$kal.='		</tr>
							</thead><tbody>';
					$data=$this->model->get(Material::$TABLE_NAME);
					foreach($data->result() as $row){
						$kal.="<tr>";
							$kal.="<td class='text-center'>".$row->id_material."</td>";
							$kal.="<td class='text-center'>".$row->product_code."</td>";
							$kal.="<td class='text-center'>".$row->description."</td>";
							$kal.="<td class='text-center'>".$row->packing."</td>";
							$kal.="<td class='text-center'>".$row->box."</td>";
							$kal.="<td class='text-center'>".$row->net."</td>";
							$kal.="<td class='text-center'>".$row->gross."</td>";
							$kal.="<td class='text-center'>".$row->kubik."</td>";
							$kal.="<td class='text-center'>".$row->kode_barang."</td>";
							$kal.="<td class='text-center'>".$row->kemasan."</td>";
							$kal.="<td class='text-center'>".$row->warna."</td>";
							/*$kal.='<td class="text-center"> <button type="button" class="btn btn-outline-danger" onclick="delete_material('.$row->id_material.')" data-mdb-ripple-color="dark">
									Hapus
								</button></td>';*/
						$kal.="</tr>";
					}
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-1"></div>';
			$kal.="</div>";
		}
		else if($this->equals($tipe, Material_RM_PM::$TABLE_NAME)) {
			$kal .= "<h1 class='f-aleo-bold text-center'>TABEL MATERIAL RM PM</h1>";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-10">';
			$kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">ID</th>
									<th scope="col" class="text-center">Jenis</th>
									<th scope="col" class="text-center">Tipe</th>
									<th scope="col" class="text-center">Konsinyasi</th>
									<th scope="col" class="text-center">Description</th>
									<th scope="col" class="text-center">Unit</th>
									<th scope="col" class="text-center">Box</th>
									<th scope="col" class="text-center">Gudang</th>
									<th scope="col" class="text-center">Kode Panggil</th>';
			$kal .= '			</tr>
							</thead><tbody>';
			$data = $this->model->get(Material_RM_PM::$TABLE_NAME);
			foreach ($data->result_array() as $row) {
				$kal .= "<tr>";
					$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";

					if (isset($row[Material_RM_PM::$JENIS]))
						$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$JENIS] . "</td>";
					else
						$kal .= "<td class='text-center'> - </td>";

					$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$TIPE] . "</td>";

					if (($row[Material_RM_PM::$IS_KONSINYASI]))
						$kal .= "<td class='text-center green-text'> V </td>";
					else
						$kal .= "<td class='text-center red-text'> X </td>";

					$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$DESCRIPTION] . "</td>";
					$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$UNIT] . "</td>";
					$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$BOX] . "</td>";

					$temp=$this->model->get(Gudang_RM_PM::$TABLE_NAME,Gudang_RM_PM::$ID_MATERIAL_RM_PM,$row[Material_RM_PM::$ID]);
					
					if($temp->num_rows()>0)
						$kal .= "<td class='text-center'>" . $temp->row_array()[Gudang_RM_PM::$NAMA_GUDANG] . "</td>";
					else
						$kal .= "<td class='text-center'> - </td>";

					$kal .= "<td class='text-center'>" . $row[Material_RM_PM::$KODE_PANGGIL] . "</td>";

				$kal .= "</tr>";
			}
			$kal .= '</tbody>
							</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
		}
		else if($this->equals($tipe,Karyawan::$TABLE_NAME)){
			$kal .= "<h1 class='f-aleo-bold text-center'>TABEL " . $tipe . "</h1>";
			$role=$this->karyawan_model->get_by("username",$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$USERNAME])->row_array()[Role::$ID];
			$kal.="<h6 class='f-aleo-bold text-center red-text'>Merah : Outsource</h6>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">NIK</th>
									<th scope="col" class="text-center">Nama</th>
									<th scope="col" class="text-center">NPWP</th>
									<th scope="col" class="text-center">Role</th>
									<th scope="col" class="text-center">Kategori</th>
									';
					if($role==ROLE_KEPALA){
						$kal.='<th class="role-kepala text-center" scope="col">Gaji Pokok (Rp)</th>';
						$kal.='<th scope="col" class="role-kepala text-center">Tunjangan (Rp)</th>';
						$kal.='<th scope="col" class="role-kepala text-center">Saldo Cuti</th>';
						}
					$kal.='			
								</tr>
							</thead><tbody>';
					$data=$this->model->get_all_karyawan();
					
					foreach($data->result() as $row){
						if($row->is_outsource==1)
							$kal.="<tr class='red-text'>";
						else
							$kal.="<tr>";
								$kal.="<td>".$row->nik."</td>";
								$kal.="<td>".ucfirst($row->k_nama)."</td>";
								$kal.="<td>".$row->npwp."</td>";
								$kal.="<td>".ucfirst($row->r_nama)."</td>";
								$kal.="<td>".ucfirst($row->tipe)."</td>";
								
								if($role==ROLE_KEPALA){
									$kal.="<td class='role-kepala text-right'>".$this->rupiah($row->gaji_pokok)."</td>";
									$kal.="<td class='role-kepala text-right'>".$this->rupiah($row->tunjangan_jabatan)."</td>";
									$kal.="<td>".$row->saldo_cuti."</td>";
								}
								
							$kal.="</tr>";
					}
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-1"></div>';
			$kal.="</div>";
			
		}
		else if($this->equals($tipe,Kalender::$TABLE_NAME)){
			$kal .= "<h1 class='f-aleo-bold text-center'>TABEL " . $tipe . "</h1>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Tanggal</th>
									<th scope="col" class="text-center">Keterangan</th>
									<th scope="col" class="text-center">Aksi</th>';
					$kal.='		</tr>
							</thead><tbody>';
					$data=$this->model->get(Kalender::$TABLE_NAME);
					foreach($data->result() as $row){
						$tanggal=strtotime($row->tanggal);
						$tampil_tanggal=date("l, j F Y",$tanggal);
						$kal.="<tr>";
							$kal.="<td class='text-center'>".$tampil_tanggal."</td>";
							$kal.="<td class='text-center'>".ucfirst($row->keterangan)."</td>";
							$kal.='<td class="text-center"> <button type="button" class="btn btn-outline-danger" onclick="delete_kalender('.$row->id_kalender.')" data-mdb-ripple-color="dark">
							Hapus
						  </button></td>';
						$kal.="</tr>";
					}
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		else if($this->equals($tipe,Absensi::$TABLE_NAME)){
			$kal .= "<h1 class='f-aleo-bold text-center'>TABEL " . $tipe . "</h1>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">NIK</th>
									<th scope="col" class="text-center">Nama</th>
									<th scope="col" class="text-center">Tanggal</th>
									<th scope="col" class="text-center">Masuk</th>
									<th scope="col" class="text-center">Keluar</th>
									<th scope="col" class="text-center">Keterangan</th>';
					$kal.='		</tr>
							</thead><tbody>';
					$data=$this->model->get_all_absensi();
					foreach($data->result() as $row){
						$tanggal=strtotime($row->tanggal);
						$tampil_tanggal=date("d/m/Y",$tanggal);
						$tampil_jam_masuk=date("H:i",strtotime($row->jam_masuk));
						$tampil_jam_keluar=date("H:i",strtotime($row->jam_keluar));

						$kal.="<tr>";
							$kal.="<td class='text-center'>".$row->nik."</td>";
							$kal.="<td class='text-center'>".$this->capitalize($row->nama)."</td>";
							$kal.="<td class='text-center'>".$tampil_tanggal."</td>";
							$kal.="<td class='text-center'>".$tampil_jam_masuk."</td>";
							$kal.="<td class='text-center'>".$tampil_jam_keluar."</td>";
							$kal.="<td class='text-center'>".ucfirst($row->keterangan)."</td>";
						$kal.="</tr>";
					}
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-1"></div>';
			$kal.="</div>";
		}
		else if($this->equals($tipe,Cutoff::$TABLE_NAME)){
			$kal .= "<h1 class='f-aleo-bold text-center'>TABEL " . $tipe . "</h1>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Dari Tanggal</th>
									<th scope="col" class="text-center">Sampai Tanggal</th>
									<th scope="col" class="text-center">Keterangan</th>
									<th scope="col" class="text-center">Aksi</th>';
					$kal.='		</tr>
							</thead><tbody>';
					$data=$this->model->get(Cutoff::$TABLE_NAME);
					foreach($data->result() as $row){
						$tanggal_dari=strtotime($row->tanggal_dari);
						$tampil_tanggal_dari=date("l, j F Y",$tanggal_dari);
						$tanggal_sampai=strtotime($row->tanggal_sampai);
						$tampil_tanggal_sampai=date("l, j F Y",$tanggal_sampai);
						$kal.="<tr>";
							$kal.="<td class='text-center'>".$tampil_tanggal_dari."</td>";
							$kal.="<td class='text-center'>".$tampil_tanggal_sampai."</td>";
							$kal.="<td class='text-center'>".ucfirst($row->keterangan)."</td>";
							$kal.='<td class="text-center"> <button type="button" class="btn btn-outline-danger" onclick="delete_cutoff('.$row->id_cut_off.')" data-mdb-ripple-color="dark">
							Hapus
						  </button>';
						$kal.="</tr>";
					}
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		else if($this->equals($tipe,Standar_Jam_Kerja::$TABLE_NAME)){
			$kal .= "<h1 class='f-aleo-bold text-center'>TABEL " . $tipe . "</h1>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Periode</th>
									<th scope="col" class="text-center">Jam Kerja (Menit)</th>
									<th scope="col" class="text-center">Aksi</th>';
					$kal.='		</tr>
							</thead><tbody>';
					$data=$this->model->get_all_standar_jam_kerja();
					foreach($data->result() as $row){
						$kal.="<tr>";
							$kal.="<td class='text-center'>".$row->periode."</td>";
							$kal.="<td class='text-center'>".$row->menit."</td>";
							$kal.='<td class="text-center"> <button type="button" class="btn btn-outline-danger" onclick="delete_s_jam_kerja('.$row->id_standar_jam_kerja.')" data-mdb-ripple-color="dark">
							Hapus
						  </button>';
						$kal.="</tr>";
					}
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		echo $kal;
	}
	public function insert($table){
		$table=strtoupper($table);
		if($this->equals($table,Karyawan::$TABLE_NAME)){
			$ni=$this->i_p("ni");
			$na=$this->i_p("na");
			$np=$this->i_p("np");
			$g=$this->i_p("g");
			$t=$this->i_p("t");
			$c=$this->i_p("c");
			$r=$this->i_p("r");
			$k=$this->i_p("k");
			$io=$this->i_p("io");
			$sk=$this->i_p("sk");
			echo MESSAGE[$this->karyawan_model->insert($ni,$na,$np,$g,$t,$c,$r,$k,$io,$sk)];
		}
		else if($this->equals($table,Kalender::$TABLE_NAME)){
			$t=$this->i_p("t");
			$k=$this->i_p("k");
			echo MESSAGE[$this->model->insert_kalender($t,$k)];
		}
		else if($this->equals($table,Cutoff::$TABLE_NAME)){
			$tf=$this->i_p("tf");
			$tt=$this->i_p("tt");
			echo MESSAGE[$this->model->insert_cutoff($tf,$tt)];
		}
		else if($this->equals($table,Standar_Insentif::$TABLE_NAME)){
			$st=$this->i_p("stk");
			$si=$this->i_p("silk");
			echo MESSAGE[$this->model->insert_s_insentif($st,$si)];
		}
		else if($this->equals($table,$this::$NAV_GOOD_RECEIPT)){
			$sto=$this->i_p("s");
			$ctr=$this->i_p("c");
			$r=$this->i_p("r");
			$arr_kode_barang=$this->i_p("kb");
			$arr_kemasan_barang=$this->i_p("keb");
			$arr_dus_barang=$this->i_p("db");
			$arr_tin_barang=$this->i_p("tb");
			$arr_warna_barang=$this->i_p("wb");
			$no_sj=$this->i_p("n");
			//echo $ctr;
			echo MESSAGE[$this->model->insert_good_receipt($ctr,$r,$sto,$no_sj,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang)];
		}
		else if($this->equals($table,$this::$NAV_GOOD_RECEIPT_ADJUSTMENT)){
			$ctr=$this->i_p("c");
			$arr_kode_barang=$this->i_p("kb");
			$arr_kemasan_barang=$this->i_p("keb");
			$arr_dus_barang=$this->i_p("db");
			$arr_tin_barang=$this->i_p("tb");
			$arr_warna_barang=$this->i_p("wb");
			$keterangan = $this->i_p("k");
			echo MESSAGE[$this->model->insert_good_receipt_adjustment($ctr,$keterangan,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang)];
		}
		else if($this->equals($table,$this::$NAV_GOOD_ISSUE)){
			$sto=$this->i_p("s");
			$ctr=$this->i_p("c");
			$r=$this->i_p("r");
			$e=$this->i_p("e");
			$arr_kode_barang=$this->i_p("kb");
			$arr_kemasan_barang=$this->i_p("keb");
			$arr_dus_barang=$this->i_p("db");
			$arr_tin_barang=$this->i_p("tb");
			$arr_warna_barang=$this->i_p("wb");
			$no_sj=$this->i_p("n");
			echo MESSAGE[$this->model->insert_good_issue($ctr,$r,$sto,$e,$no_sj,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang)];
		}
		else if($this->equals($table,$this::$NAV_GOOD_ISSUE_ADJUSTMENT)){
			$ctr=$this->i_p("c");
			$arr_kode_barang=$this->i_p("kb");
			$arr_kemasan_barang=$this->i_p("keb");
			$arr_dus_barang=$this->i_p("db");
			$arr_tin_barang=$this->i_p("tb");
			$arr_warna_barang=$this->i_p("wb");
			$keterangan=$this->i_p("k");
			echo MESSAGE[$this->model->insert_good_issue_adjustment($ctr,$keterangan,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang)];
		
		}	
		else if($this->equals($table,Material_Fisik::$TABLE_NAME)){
			$ctr=$this->i_p("c");
			$arr_kode_barang=$this->i_p("kb");
			$arr_kemasan_barang=$this->i_p("keb");
			$arr_stock_fisik_barang=$this->i_p("sfb");
			$arr_warna_barang=$this->i_p("wb");
			echo MESSAGE[$this->model->insert_material_fisik($ctr,$arr_kode_barang,$arr_kemasan_barang,$arr_stock_fisik_barang,$arr_warna_barang)];
		}
		else if($this->equals($table,$this::$NAV_USER_ACCESS)){
			$nik=$this->i_p("ni");
			$id_access=$this->i_p("ia");
			$is_true=$this->i_p("it");
			
			echo MESSAGE[$this->model->insert_user_access($nik,$id_access,$is_true)];
		}
		else if($this->equals($table,$this::$NAV_USER_ACCESS_NO_RESTRICTION)){
			$nik_bawahan=$this->i_p("nb");
			$nik_atasan=$this->i_p("na");
			$id_access=$this->i_p("i");
			$tipe=$this->i_p("t");
			
			echo MESSAGE[$this->model->insert_user_access_no_restriction($nik_bawahan,$nik_atasan,$id_access,$tipe)];
		}
		else if($this->equals($table,$this::$NAV_KINERJA_GOOD_ISSUE)){
			$sopir=$this->i_p("ns");
			$kernet=$this->i_p("nk");
			$sj=$this->i_p("n");
			$tipe_eks=$this->i_p("te");
			$eks=$this->i_p("ie");
			$tanggal=$this->i_p("t");
			echo MESSAGE[$this->kinerja_model->fg_insert_good_issue($sj,$tanggal,$eks,$tipe_eks,$sopir,$kernet)];
		}
		else if($this->equals($table,Purchase_Order_Checker_Header::$TABLE_NAME)){
			$arr_id_material=$this->i_p("ai");
			$arr_no_urut=$this->i_p("an");
			$arr_qty=$this->i_p("aq");
			$nomor_po = $this->i_p("np");
			$nomor_sj =$this->i_p("ns");
			$tanggal = $this->i_p("t");
			$ctr = $this->i_p("c");
			echo MESSAGE[$this->model->insert_po_checker($ctr,$nomor_po,$nomor_sj,$tanggal,$arr_id_material,$arr_no_urut,$arr_qty)];
		}
		else if($this->equals($table,$this::$NAV_PURCHASE_ORDER_REVISI_QTY)){
			$arr_id_material=$this->i_p("ai");
			$arr_no_urut=$this->i_p("an");
			$arr_qty=$this->i_p("aq");
			$nomor_po = $this->i_p("np");
			$ctr = $this->i_p("c");
			echo MESSAGE[$this->model->po_revisi_qty($ctr,$nomor_po,$arr_id_material,$arr_no_urut,$arr_qty)];
		}
		else if($this->equals($table,Worksheet_Checker_Header::$TABLE_NAME)){
			$arr_id_material=$this->i_p("ai");
			$arr_no_urut=$this->i_p("an");
			$arr_qty=$this->i_p("aq");
			$arr_vendor=$this->i_p("av");
			$nomor_ws = $this->i_p("nw");
			$tanggal = $this->i_p("t");
			$ctr = $this->i_p("c");
			echo MESSAGE[$this->model->insert_ws_checker($ctr,$nomor_ws,$tanggal,$arr_id_material,$arr_vendor,$arr_no_urut,$arr_qty)];
		} 
		else if($this->equals($table,Worksheet_Checker_Header::$S_ADJUSTMENT_CHECKER)) {
			$arr_id_material = $this->i_p("ai");
			$arr_qty = $this->i_p("aq");
			$arr_vendor = $this->i_p("av");
			$nomor_ws = $this->i_p("nw");
			$tanggal = $this->i_p("t");
			$ctr = $this->i_p("c");
			echo MESSAGE[$this->model->adjustment_ws_checker($ctr, $nomor_ws, $tanggal, $arr_id_material, $arr_vendor, $arr_qty)];
		}
		else if($this->equals($table,$this::$NAV_WORK_SHEET_ADMIN_PROSES)){
			/*$arr_kemasan=$this->i_p("ak");
			$arr_tin=$this->i_p("at");
			$arr_berat = $this->i_p("ab");
			$ws_semi = $this->i_p("wss");
			$ws_fg = $this->i_p("wsf");
			$ctr=$this->i_p("c");*/
			$kemasan = $this->i_p("k");
			$tin = $this->i_p("t");
			$berat = $this->i_p("b");
			$ws_semi = $this->i_p("wss");
			$ws_fg = $this->i_p("wsf");
			$premix = $this->i_p("p");
			$makeup = $this->i_p("m");
			$filling = $this->i_p("f");
			echo MESSAGE[$this->model->ws_admin_proses_insert($ws_semi, $ws_fg, $kemasan,$tin,$berat,$premix,$makeup,$filling)];
		}
		else if($this->equals($table, $this::$NAV_WORK_SHEET_CHECKER_FG_INSERT_GR)){
			$no_ws = $this->i_p("n");
			echo MESSAGE[$this->model->ws_checker_fg_insert_gr($no_ws)];
		}
		else if($this->equals($table,$this::$NAV_RM_PM_GOOD_RECEIPT_MUTASI)){
			$ctr = $this->i_p("c");
			$arr_id_material = $this->i_p("ai");
			$arr_qty = $this->i_p("aq");
			$keterangan = $this->i_p("k");
			$arr_vendor = $this->i_p("av");
			echo MESSAGE[$this->model->insert_good_receipt_rm_pm_mutasi($ctr, $keterangan, $arr_id_material, $arr_qty, $arr_vendor)];
		}
		else if($this->equals($table,$this::$NAV_RM_PM_GOOD_ISSUE_MUTASI)){
			$ctr = $this->i_p("c");
			$arr_id_material = $this->i_p("ai");
			$arr_qty = $this->i_p("aq");
			$keterangan = $this->i_p("k");
			$arr_vendor = $this->i_p("av");
			echo MESSAGE[$this->model->insert_good_issue_rm_pm_mutasi($ctr,$keterangan,$arr_id_material,$arr_qty,$arr_vendor)];
		}
	}
	public function delete($table){
		$id=$this->i_p('id');
		$table=strtoupper($table);
		if($this->equals($table,Kalender::$TABLE_NAME)){
			echo MESSAGE[$this->model->delete_kalender($id)];
		}
		else if($this->equals($table,Cutoff::$TABLE_NAME)){
			echo MESSAGE[$this->model->delete_cutoff($id)];
		}
		else if($this->equals($table,Material::$TABLE_NAME)){
			echo MESSAGE[$this->model->delete_material($id)];
		}
	}

	//form function
	public function insert_f($table){ //insert form
		$kal="";
		$table=strtoupper($table);
		if($this->equals($table,Standar_Insentif::$TABLE_NAME)){
			$this->static_update_s_insentif();
			$kal.="<h1 class='f-aleo-bold text-center'>DATA STANDAR INSENTIF</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table class="table table-borderless">';
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Insentif Luar Kota (Rp)</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" value='.$this::$UANG_LUAR_KOTA.' class="f-aleo" type="number" id="txt_i_luar_kota"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Tunjangan Kehadiran (Rp)</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" value='.$this::$UANG_HADIR.' class="f-aleo" type="number" id="txt_t_kehadiran"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
						$kal.='<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary btn_modify_s_insentif" onclick="update_s_insentif()" data-mdb-ripple-color="dark">
							Update
						  </button></td>';
						$kal.="</tr>";
					$kal.="</table>";
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,Karyawan::$TABLE_NAME)){
			$kal.="<h1 class='f-aleo-bold text-center'>DATA KARYAWAN</h1>";
			$kal.="<h6 class='f-aleo-bold text-center red-text'>* : Wajib Diisi</h6>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table class="table table-borderless">';
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>NIK*</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-left font-sm'>";
								$kal.='<input onkeyup="check_nik()" style="width:50%" class="f-aleo" type="text" id="txt_nik"/>';
								$kal.='<input disabled type="text" id="txt_status_nik" style="border:0;width:50%" class="f-aleo font-sm"></input>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Nama</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" class="f-aleo" type="text" id="txt_nama"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>NPWP</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" class="f-aleo" type="text" id="txt_npwp"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr class='role-kepala'>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Gaji Pokok (Rp)</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" value=0 class="f-aleo" type="number" id="txt_gp"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr class='role-kepala'>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Tunjangan (Rp)</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" value=0 class="f-aleo" type="number" id="txt_tunjangan"/>';
							$kal.="</td>";
						$kal.="</tr>";
						/*$kal.="<tr class='role-kepala'>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Saldo Cuti (Hari)</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" value=0 class="f-aleo" type="number" id="txt_s_cuti"/>';
							$kal.="</td>";
						$kal.="</tr>";*/
						$kal.="<tr class='role-kepala'>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Role</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<select class="form-control f-aleo" id="dd_role">';
								$data=$this->model->get("role");
								foreach($data->result() as $row){
									$kal.='<option value='.$row->id_role.'>'.$row->nama.'</option>';
								}
									$kal.='<option value=-1>TIDAK ADA</option>';
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Kategori</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<select onchange="check_sub_kategori()" class="form-control f-aleo" id="dd_kategori">';
								$data=$this->model->get("kategori_karyawan");
								foreach($data->result() as $row){
									$kal.='<option value='.$row->id_kategori.'>'.$row->tipe.'</option>';
								}
									$kal.='<option value=-1>TIDAK ADA</option>';
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr class='tr_sub_kategori'>";
							$kal.="<td class='td_sub_kategori align-middle text-right font-sm f-aleo'>Sub Kategori</td>";
							$kal.="<td class='td_sub_kategori align-middle text-right font-sm'>:</td>";
							$kal.="<td class='td_sub_kategori td_sub_kategori_dd align-middle text-right font-sm'>";
								$kal.='<select class="form-control f-aleo" id="dd_sub_kategori">';
									$kal.='<option value=-1>TIDAK ADA</option>';
									$data=$this->model->get(Sub_Kategori::$TABLE_NAME);
									foreach($data->result() as $row){
										$kal.='<option value='.$row->id_sub_kategori.' class="opt_sub_kategori opt_sub_kat_'.$row->id_kategori.'">'.strtoupper($row->nama).'</option>';
									}
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
						$kal.='<td></td>
							   <td></td>
							   <td>
								<div class="form-check f-aleo font-sm">
									<input class="form-check-input" type="checkbox" value="" id="cb_outsource">
									<label class="form-check-label" for="cb_outsource">
									OUTSOURCE
									</label>
								</div>
							   </td>';
						$kal.="</tr>";
						$kal.="<tr>";
						$kal.='<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary btn_modify_karyawan" onclick="insert_karyawan()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
						$kal.="</tr>";
					$kal.="</table>";
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,Kalender::$TABLE_NAME)){
			$kal.="<h1 class='f-aleo-bold text-center'>INSERT KALENDER</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table class="table table-borderless">';
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Tanggal</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" class="f-aleo" type="date" id="txt_tanggal"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Keterangan</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<textarea type="area" class="f-aleo" style="width:100%" id="txt_keterangan"></textarea>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
						$kal.='<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_calendar()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
						$kal.="</tr>";
					$kal.="</table>";
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,Cutoff::$TABLE_NAME)){
			$kal.="<h1 class='f-aleo-bold text-center'>INSERT CUT OFF</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table class="table table-borderless">';
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Dari Tanggal</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" class="f-aleo" type="date" id="txt_tanggal_from"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Sampai Tanggal</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" class="f-aleo" type="date" id="txt_tanggal_to"/>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
						$kal.='<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_cutoff()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
						$kal.="</tr>";
					$kal.="</table>";
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,Standar_Jam_Kerja::$TABLE_NAME)){
			$kal.="<h1 class='f-aleo-bold text-center'>INSERT STANDAR JAM KERJA</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-3"></div>';
				$kal.='<div class="col-sm-6">';
					$kal.='<table class="table table-borderless">';
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Bulan</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<select class="form-control" id="dd_bulan">';
								for($i=1;$i<count(ARR_BULAN);$i++){
									$kal.='<option value='.ARR_BULAN[$i].'>'.ARR_BULAN[$i].'</option>';
								}
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Tahun</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<select class="form-control" id="dd_tahun">';
								for($i=0;$i<count(ARR_TAHUN);$i++){
									$kal.='<option value='.ARR_TAHUN[$i].'>'.ARR_TAHUN[$i].'</option>';
								}
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Jam Kerja (Menit)</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.="<input style='margin-top:7%' class='form-control' id='txt_jam_kerja' type='number'/>'";
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
						$kal.='<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_s_jam_kerja()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
						$kal.="</tr>";
					$kal.="</table>";
				$kal.="</div>";
				$kal.='<div class="col-sm-3"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,$this::$NAV_USER_ACCESS_NO_RESTRICTION)){
			$kal.="<br>";
			$kal.="<h3 class='f-aleo-bold text-center'>TAMBAH HAK AKSES</h3>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table class="table table-borderless">';
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Karyawan Bawahan</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<select class="form-control" id="dd_nik_bawahan">';
								$query=$this->model->get_all_karyawan();
								foreach($query->result() as $row){
									$kal.='<option value='.$row->nik.'>'.$row->nik." - ".$row->k_nama.'</option>';
								}
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";

						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Karyawan Atasan</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<select class="form-control" id="dd_nik_atasan">';
								$query=$this->model->get_all_karyawan();
								foreach($query->result() as $row){
									$kal.='<option value='.$row->nik.'>'.$row->nik." - ".$row->k_nama.'</option>';
								}
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Hak Akses</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<select class="form-control" id="dd_list_access" onchange="change_desc_text()">';
								$query=$this->model->get(List_Access::$TABLE_NAME);
								foreach($query->result() as $row){
									$kal.='<option id="'.$row->description.'" value='.$row->id_access.'>'.$row->access.'</option>';
								}
								$kal.='</select>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Deskripsi</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<textarea rows=5 style="width:100%;resize: none" class="f-aleo" disabled type="text" id="txt_deskripsi"></textarea>';
							$kal.="</td>";
						$kal.="</tr>";
						$kal.="<tr>";
							$kal.='<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_user_access()" data-mdb-ripple-color="dark">
								Tambah
							</button></td>';
						$kal.="</tr>";
					$kal.="</table>";
				$kal.="</div>";
				$kal.='<div class="col-sm-1"></div>';
			$kal.="</div>";
		}
		echo $kal;
	}
	public function laporan_f($table){
		$table=strtoupper($table);
		$kal="";
		
		if($this->equals($table,Absensi::$TABLE_NAME)){
			$kal.="<h1 class='f-aleo-bold text-center'>PERFORMA KEHADIRAN BULANAN</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Bulan</label>
								<select class="form-control" id="dd_bulan">';
			for($i=0;$i<count(ARR_BULAN);$i++){
								$kal.='<option value='.$i.'>'.ARR_BULAN[$i].'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$temp=strtolower($table);
					$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_laporan('."'$temp'".')">
								GENERATE LAPORAN
							</button>
							</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		if($this->equals($table,$this::$NAV_ABSENSI_GAJI)){
			
			$kal.="<h1 class='f-aleo-bold text-center'>LAPORAN GAJI ABSENSI</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Bulan</label>
								<select class="form-control" id="dd_bulan">';
			for($i=0;$i<count(ARR_BULAN_NO_SEMUA);$i++){
								$kal.='<option value='.$i.'>'.ARR_BULAN_NO_SEMUA[$i].'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$temp=strtolower($table);
					$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_laporan('."'$temp'".')">
								GENERATE LAPORAN
							</button>
							</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		if($this->equals($table,$this::$NAV_ABSENSI_TABEL)){
			$kal.="<h1 class='f-aleo-bold text-center'>TABEL ABSENSI</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Bulan</label>
								<select class="form-control" id="dd_bulan">';
			for($i=0;$i<count(ARR_BULAN_NO_SEMUA);$i++){
								$kal.='<option value='.$i.'>'.ARR_BULAN_NO_SEMUA[$i].'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$temp=strtolower($table);
					$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_laporan('."'$temp'".')">
								GENERATE LAPORAN
							</button>
							</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		if($this->equals($table,$this::$NAV_REKAP_PERFORMANCE)){
			$kal.="<h1 class='f-aleo-bold text-center'>REKAP PERFORMANCE</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Bulan</label>
								<select class="form-control" id="dd_bulan">';
			for($i=0;$i<count(ARR_BULAN_NO_SEMUA);$i++){
								$kal.='<option value='.$i.'>'.ARR_BULAN_NO_SEMUA[$i].'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$temp=strtolower($table);
					$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_laporan('."'$temp'".')">
								GENERATE LAPORAN
							</button>
							</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		if($this->equals($table,$this::$NAV_REKAP_GAJI)){
			$kal.="<h1 class='f-aleo-bold text-center'>REKAP GAJI</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Bulan</label>
								<select class="form-control" id="dd_bulan">';
			for($i=0;$i<count(ARR_BULAN_NO_SEMUA);$i++){
								$kal.='<option value='.$i.'>'.ARR_BULAN_NO_SEMUA[$i].'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$temp=strtolower($table);
					$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_laporan('."'$temp'".')">
								GENERATE LAPORAN
							</button>
							</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		echo $kal;
	}
	public function rincian_f($table){
		$table=strtoupper($table);
		$kal="";
		
		if($this->equals($table,Absensi::$TABLE_NAME)){
			$kal.="<h1 class='f-aleo-bold text-center'>RINCIAN $table</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tipe</label>
								<select class="form-control" id="dd_tipe">';
			for($i=0;$i<count(ARR_TIPE_RINCIAN_ABSENSI);$i++){
								$kal.='<option value='.$i.'>'.ARR_TIPE_RINCIAN_ABSENSI[$i].'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_rincian('."'absensi'".')">
								GENERATE RINCIAN
							</button>
						</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,$this::$NAV_ABSENSI_PERFORMANCE)){
			$kal.="<h1 class='f-aleo-bold text-center'>PERFORMA KEHADIRAN PER KARYAWAN</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Nama Karyawan</label>
								<select class="form-control" id="dd_tipe">';
			$arr_karyawan=$this->model->get_all_karyawan();
			foreach($arr_karyawan->result() as $row){
								$kal.='<option value='.$row->nik.'>'.$row->nik.' - '.$this->capitalize($row->k_nama).'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_rincian('."'absensi_performance'".')">
								GENERATE RINCIAN
							</button>
						</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,$this::$NAV_SLIP_GAJI)){
			$kal.="<h1 class='f-aleo-bold text-center'>GENERATE SLIP GAJI</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-3">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Bulan</label>
								<select class="form-control" id="dd_bulan">';
			for($i=0;$i<count(ARR_BULAN_NO_SEMUA);$i++){
								$kal.='<option value='.$i.'>'.ARR_BULAN_NO_SEMUA[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-2">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Nama Karyawan</label>
								<select class="form-control" id="dd_tipe">';
			$arr_karyawan=$this->model->get_all_karyawan();
			foreach($arr_karyawan->result() as $row){
								$kal.='<option value='.$row->nik.'>'.$row->nik.' - '.$this->capitalize($row->k_nama).'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-2"></div>';
				$kal.='<div class="text-right col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_slip_gaji()">
								GENERATE
							</button>
						</div>';
				$kal.='<div class="text-left col-sm-4">
						<button type="button" class="btn btn-outline-success" onclick="gen_slip_gaji_all_karyawan()">
							GENERATE ALL
						</button>
					</div>';
				$kal.='<div class="col-sm-2"></div>';
			$kal.="</div>";
		}
		else if($this->equals($table,Karyawan_Insentif::$TABLE_NAME)){
			$kal.="<h1 class='f-aleo-bold text-center'>INSENTIF KARYAWAN</h1>";
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-3">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Bulan</label>
								<select class="form-control" id="dd_bulan">';
			for($i=0;$i<count(ARR_BULAN_NO_SEMUA);$i++){
								$kal.='<option value='.$i.'>'.ARR_BULAN_NO_SEMUA[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-2">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Tahun</label>
								<select class="form-control" id="dd_tahun">';
			for($i=0;$i<count(ARR_TAHUN);$i++){
								$kal.='<option value='.$i.'>'.ARR_TAHUN[$i].'</option>';
			}
								
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
				$kal.='<div class="col-sm-5">';
					$kal.='<div class="f-aleo form-group">
								<label for="exampleFormControlSelect1">Nama Karyawan</label>
								<select class="form-control" id="dd_tipe">';
			$arr_karyawan=$this->model->get_all_karyawan();
			foreach($arr_karyawan->result() as $row){
								$kal.='<option value='.$row->nik.'>'.$row->nik.' - '.$this->capitalize($row->k_nama).'</option>';
			}
						$kal.='</select>';
					$kal.="</div>";
				$kal.="</div>";
			$kal.="</div>";
			$kal.="<div class='row' style='margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn btn-outline-success" onclick="gen_karyawan_insentif()">
								GENERATE
							</button>
						</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
		}
		echo $kal;
	}

	//generate function
	public function gen_laporan(){
		$tipe=strtoupper($this->i_p("t"));
		$bulan=intval($this->i_p("b"));
		$tahun=intval($this->i_p("ta"));
		$kal="";
		
		$kal.="<div class='div_laporan animated fadeInDown'>";
		$kal.="<br>";
		
		if($this->equals($tipe,Absensi::$TABLE_NAME)){
			$kal.="<h2 class='f-aleo-bold text-center'>Laporan Absensi ".ARR_TAHUN[$tahun]." - ".ARR_BULAN[$bulan]."</h2>";
				$standar=$this->calculate_standar_jam_kerja(ARR_BULAN[$bulan],ARR_TAHUN[$tahun]);
				$kal.="<h5 class='f-aleo text-center'>Standar Total Jam Kerja: ".$standar." Menit</h5>";
				$kal.='<div class="row" style="margin-right:1%">';
					$kal.='<div class="col-sm-1"></div>';
					$kal.='<div class="col-sm-10">';
						$kal.='<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">NIK</th>
										<th scope="col" class="text-center">Nama</th>
										<th scope="col" class="text-center">Total Jam Kerja (Menit)</th>
										<th scope="col" class="text-center">Cuti (Hari)</th>
										<th scope="col" class="text-center">Alpha (Hari)</th>
										<th scope="col" class="text-center">Sakit (Hari)</th>
										<th scope="col" class="text-center">Lembur (Jam)</th>
										<th scope="col" class="text-center">Luar Kota (Hari)</th>
										<th scope="col" class="text-center">Performa (%)</th>';
						$kal.='		</tr>
								</thead><tbody>';
						
						//Perhitungan Jam Kerja
						$arr_abs_karyawan=array();
						$data=$this->model->get_all_karyawan();
						
						foreach($data->result() as $row){
							
							$nik=$row->nik;
							$arr_abs_karyawan[$nik]=0;
							$where="karyawan.nik=$nik";
							if($bulan!=0)$where.=" and month(tanggal)='$bulan'";
							$where.=" and year(tanggal)='".ARR_TAHUN[$tahun]."'";
							$data_absensi=$this->model->get_absensi_where("$where");
							foreach($data_absensi->result() as $row2){
								$minutes=$this->calculate_work_minutes($nik,$row2->jam_masuk,$row2->jam_keluar,$row2->tanggal,$row2->keterangan);
								$arr_abs_karyawan[$nik]+=$minutes;
							}
						}
						//End Perhitungan Jam Kerja

						foreach($data->result() as $row){
							$nik=$row->nik;
							$where="karyawan.nik=$nik";
							if($bulan!=0)$where.=" and month(tanggal)='$bulan'";
							$where.=" and year(tanggal)='".ARR_TAHUN[$tahun]."'";
							$kal.="<tr>";
								$kal.="<td>".$nik."</td>";
								$kal.="<td>".ucfirst($row->k_nama)."</td>";
								$kal.="<td class='text-center'>".$arr_abs_karyawan[$row->nik]."</td>";

								$check_holiday=" tanggal not in (select tanggal from kalender where is_deleted=0)";
								$cuti=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_CUTI."%' and $check_holiday")->num_rows();
								$kal.="<td class='text-center'>$cuti</td>";

								$alpha=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_ALPHA."%'")->num_rows();
								$kal.="<td class='text-center'>$alpha</td>";

								$sakit=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_SAKIT."%'")->num_rows();
								$kal.="<td class='text-center'>$sakit</td>";

								$lembur=$this->calculate_lembur_minutes($nik,$where);
								$kal.="<td class='text-center'>".round($lembur/60,2)."</td>";

								$luarkota=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_LUAR_KOTA."%'")->num_rows();
								$kal.="<td class='text-center'>$luarkota</td>";

								$performa=$this->calculate_performance_kehadiran($arr_abs_karyawan[$row->nik],$standar);
								$kal.="<td class='text-center'>".round($performa,2)."</td>";
							$kal.="</tr>";
						}
						$kal.='</tbody>
								</table>';
					$kal.="</div>";
					$kal.='<div class="col-sm-1"></div>';
				$kal.="</div>";
			$kal.="</div>";
		}
		if($this->equals($tipe,$this::$NAV_REKAP_GAJI)){
			$this->static_update_s_insentif();	
			$kal.="<h2 class='f-aleo-bold text-center'>Rekap Gaji ".ARR_TAHUN[$tahun]." - ".ARR_BULAN_NO_SEMUA[$bulan]."</h2>";
			$kal.="<h6 class='f-aleo-bold text-center red-text'>Keterangan: T(Tunjangan), I(Insentif)</h6>";	
				$kal.='<div class="row" style="margin-right:1%">';
					$kal.='<div class="col-sm-1"></div>';
					$kal.='<div class="col-sm-10">';
						$kal.='<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">NIK</th>
										<th scope="col" class="text-center">Nama</th>
										<th scope="col" class="text-center">Gaji Pokok</th>
										<th scope="col" class="text-center">T Jabatan</th>
										<th scope="col" class="text-center">T Obat</th>
										<th scope="col" class="text-center">I Absensi</th>
										<th scope="col" class="text-center">I Kinerja</th>
										<th scope="col" class="text-center">BPJS</th>
										<th scope="col" class="text-center">Total</th>
										';
						$kal.='		</tr>
								</thead><tbody>';
						
						//Perhitungan Jam Kerja
						$data=$this->model->get_all_karyawan();

						//Get Cut Off Period
						$keterangan=ARR_BULAN_NO_SEMUA[$bulan]." ".ARR_TAHUN[$tahun];
						$cutoff=$this->model->get_cutoff_by_keterangan($keterangan)->row();
						if(isset($cutoff)){
							$kal.="<h4 class='f-aleo-bold text-center green-text'>Periode Cut Off: ".$cutoff->tanggal_dari." sampai ".$cutoff->tanggal_sampai."</h4>";

							$total_all_karyawan=0;
							foreach($data->result() as $row){
								$nik=$row->nik;
								$where="karyawan.nik=$nik";
								$kal.="<tr>";
									$kal.="<td>".$row->nik."</td>";
									$kal.="<td class='text-center'>".ucfirst($row->k_nama)."</td>";
									$kal.="<td class='text-right'>".$this->rupiah($row->gaji_pokok)."</td>";
									$kal.="<td class='text-right'>".$this->rupiah($row->tunjangan_jabatan)."</td>";
									$tunjangan_obat=0;
									$query=$this->model->get_karyawan_insentif_ket_not_null($row->nik,$bulan,$tahun,S_INSENTIF_T_OBAT);
									if($query->num_rows()>0)
										$tunjangan_obat=$query->row()->nominal;
									$kal.="<td class='text-right'>".$this->rupiah($tunjangan_obat)."</td>";
									
									//Insentif Kehadiran
									$insentif_kehadiran=$this->calculate_insentif_kehadiran_rupiah($nik,$bulan,$tahun);
									$kal.="<td class='text-right'>".$this->rupiah($insentif_kehadiran)."</td>";

									//Insentif Kinerja
									$kal.="<td class='text-right'>".$this->rupiah(0)."</td>";

									//Sub Total
									$sub_total=$row->gaji_pokok+$row->tunjangan_jabatan+$tunjangan_obat+$insentif_kehadiran;

									//Potong BPJS & Kesehatan
									$potong=0;
									
									if($row->gaji_pokok>STANDAR_POTONG_BPJS){
										$potong=$sub_total*4/100;
									}else
										$potong=STANDAR_POTONG_BPJS*4/100;
									
									$kal.="<td class='text-right'>-".$this->rupiah($potong)."</td>";
									
									//Total
									$total=$sub_total-$potong;
									$kal.="<td class='text-right'>".$this->rupiah($total)."</td>";
									$total_all_karyawan+=$total;
								$kal.="</tr>";
							}
							$kal.="<h5 class='f-aleo-bold text-center'>Total Semua Karyawan: Rp. ".$this->rupiah($total_all_karyawan)."</h5>";
						}else{	
							$kal.="<h3 class='f-aleo-bold text-center red-text'>Tidak ada periode Cut Off pada bulan tersebut, silahkan masukkan terlebih dahulu!</h3>";
						}
						
						$kal.='</tbody>
								</table>';
					$kal.="</div>";
					$kal.='<div class="col-sm-1"></div>';
				$kal.="</div>";
			$kal.="</div>";
			
		}
		if($this->equals($tipe,$this::$NAV_REKAP_PERFORMANCE)){
			$kal.="<h2 class='f-aleo-bold text-center'>Rekap Performance ".ARR_TAHUN[$tahun]." - ".ARR_BULAN_NO_SEMUA[$bulan]."</h2>";
				$kal.='<div class="row" style="margin-right:1%">';
					$kal.='<div class="col-sm-1"></div>';
					$kal.='<div class="col-sm-10">';
						$kal.='<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">NIK</th>
										<th scope="col" class="text-center">Nama</th>
										<th scope="col" class="text-center">Kehadiran (%)</th>
										<th scope="col" class="text-center">Produksi (%)</th>
										<th scope="col" class="text-center">Stock (%)</th>
										<th scope="col" class="text-center">Kebersihan (%)</th>
										';
						$kal.='		</tr>
								</thead><tbody>';
						
						
						$data=$this->model->get_all_karyawan();

						foreach($data->result() as $row){
							$nik=$row->nik;
							$where="karyawan.nik=$nik";
							$where.=" and month(tanggal)=".($bulan+1);
							$where.=" and year(tanggal)=".ARR_TAHUN[$tahun];

							$kal.="<tr>";
								$kal.="<td>".$row->nik."</td>";
								$kal.="<td class='text-center'>".ucfirst($row->k_nama)."</td>";
								
								//Performa Kehadiran
								$standar=$this->calculate_standar_jam_kerja(ARR_BULAN_NO_SEMUA[$bulan],ARR_TAHUN[$tahun]);
								$data_absensi=$this->model->get_absensi_where("$where");
								$jam_kerja=0;
								foreach($data_absensi->result() as $row2){
									$minutes=$this->calculate_work_minutes($nik,$row2->jam_masuk,$row2->jam_keluar,$row2->tanggal,$row2->keterangan);
									$jam_kerja+=$minutes;
								}
								$performa_kehadiran=$this->calculate_performance_kehadiran($jam_kerja,$standar);
								$kal.="<td class='text-center'>$performa_kehadiran</td>";

								//Performa Produksi
								$kal.="<td class='text-center'>0</td>";

								//Performa Stock
								$kal.="<td class='text-center'>0</td>";

								//Performa Kebersihan
								$kal.="<td class='text-center'>0</td>";
							$kal.="</tr>";
						}
						
						$kal.='</tbody>
								</table>';
					$kal.="</div>";
					$kal.='<div class="col-sm-1"></div>';
				$kal.="</div>";
			$kal.="</div>";
			
		}
		if($this->equals($tipe,$this::$NAV_ABSENSI_GAJI)){
			$this->static_update_s_insentif();	
			$kal.="<h2 class='f-aleo-bold text-center'>Laporan Gaji ".ARR_TAHUN[$tahun]." - ".ARR_BULAN_NO_SEMUA[$bulan]."</h2>";
				$kal.='<div class="row" style="margin-right:1%">';
					$kal.='<div class="col-sm-1"></div>';
					$kal.='<div class="col-sm-10">';
						$kal.='<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">NIK</th>
										<th scope="col" class="text-center">Nama</th>
										<th scope="col" class="text-center">Luar Kota (Rp)</th>
										<th scope="col" class="text-center">Lembur (Rp)</th>
										<th scope="col" class="text-center">Kehadiran (Rp)</th>
										<th scope="col" class="text-center">Alpha (Rp)</th>
										<th scope="col" class="text-center">Total (Rp)</th>
										';
						$kal.='		</tr>
								</thead><tbody>';
						
						//Perhitungan Jam Kerja
						$arr_lbr_karyawan=array();
						$data=$this->model->get_all_karyawan();
						
						//Perhitungan Uang Kehadiran
						$arr_hdr_karyawan=array();

						//Get Cut Off Period
						$keterangan=ARR_BULAN_NO_SEMUA[$bulan]." ".ARR_TAHUN[$tahun];
						$cutoff=$this->model->get_cutoff_by_keterangan($keterangan)->row();
						if(isset($cutoff)){
							$kal.="<h4 class='f-aleo-bold text-center green-text'>Periode Cut Off: ".$cutoff->tanggal_dari." sampai ".$cutoff->tanggal_sampai."</h4>";
							//Perhitungan Gaji Lembur
							foreach($data->result() as $row){
								$lembur=0;
								$nik=$row->nik;
								$arr_lbr_karyawan[$nik]=$this->calculate_lembur_rupiah($nik,$bulan,$tahun);
							}
							//End Perhitungan Gaji Lembur
							
							
							foreach($data->result() as $row){
								$nik=$row->nik;
								$arr_hdr_karyawan[$nik]=0;
								$where="karyawan.nik=$nik";

								$ctr_bulan=$bulan+1;
								
								$where.=" and tanggal>='".$cutoff->tanggal_dari."'";
								$where.=" and tanggal<='".$cutoff->tanggal_sampai."'";
								
								$alpha=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_ALPHA."%'")->num_rows();
								$sakit=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_SAKIT."%'")->num_rows();
								$cuti=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_CUTI."%'")->num_rows();
								
								if($alpha!=0||$sakit!=0||!$this->model->available_karyawan_insentif($nik,S_INSENTIF_T_KEHADIRAN))
									$arr_hdr_karyawan[$nik]=0;
								else
									$arr_hdr_karyawan[$nik]=$this::$UANG_HADIR;
							}


							foreach($data->result() as $row){
								$nik=$row->nik;
								$where="karyawan.nik=$nik";
								$ctr_bulan=$bulan+1;
								$where.=" and tanggal>='".$cutoff->tanggal_dari."'";
								$where.=" and tanggal<='".$cutoff->tanggal_sampai."'";
								$kal.="<tr>";
									$luar_kota=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_LUAR_KOTA."%'")->num_rows();
									$alpha=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_ALPHA."%'")->num_rows();
									$sakit=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_SAKIT."%'")->num_rows();
									$cuti=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_CUTI."%'")->num_rows();
									//$lain=$this->model->get_absensi_where("$where and upper(keterangan) like '%LAIN-LAIN%'")->num_rows();
									

									$kal.="<td>".$row->nik."</td>";
									$kal.="<td>".ucfirst($row->k_nama)."</td>";
									$gaji_lk=$luar_kota*$this::$UANG_LUAR_KOTA;
									$kal.="<td class='text-right'>".$this->rupiah($gaji_lk)." </td>";
									$kal.="<td class='text-right'>".$this->rupiah($arr_lbr_karyawan[$nik])."</td>";
									$kal.="<td class='text-right'>".$this->rupiah($arr_hdr_karyawan[$nik])."</td>";
									$gaji_alpha=$this->calculate_alpha_rupiah($row->nik,$alpha);
									$total=$arr_lbr_karyawan[$nik]+$arr_hdr_karyawan[$nik]+$gaji_lk-$gaji_alpha;
									$kal.="<td class='text-right'>-".$this->rupiah($gaji_alpha)."</td>";
									$kal.="<td class='text-right'>".$this->rupiah($total)."</td>";
								$kal.="</tr>";
							}
						}else{	
							$kal.="<h3 class='f-aleo-bold text-center red-text'>Tidak ada periode Cut Off pada bulan tersebut, silahkan masukkan terlebih dahulu!</h3>";
						}
						$kal.='</tbody>
								</table>';
					$kal.="</div>";
					$kal.='<div class="col-sm-1"></div>';
				$kal.="</div>";
			$kal.="</div>";
		}
		if($this->equals($tipe,$this::$NAV_ABSENSI_TABEL)){
			$kal.="<h2 class='f-aleo-bold text-center'>Tabel Absensi ".ARR_TAHUN[$tahun]." - ".ARR_BULAN_NO_SEMUA[$bulan]."</h2>";
				$standar=$this->calculate_standar_jam_kerja(ARR_BULAN_NO_SEMUA[$bulan],ARR_TAHUN[$tahun]);
				$kal.="<h5 class='f-aleo text-center'>Standar Total Jam Kerja: ".$standar." Menit</h5>";
				$kal.="<h6 class='f-aleo text-center'><span class='red-text'>Merah</span> : Hari Libur</h6>";
				$kal.='<div class="row" style="margin-right:1%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">NIK</th>
									<th scope="col" class="text-center">Nama</th>
									<th scope="col" class="text-center">Tanggal</th>
									<th scope="col" class="text-center">Masuk</th>
									<th scope="col" class="text-center">Keluar</th>
									<th scope="col" class="text-center">Jam Kerja (Menit)</th>
									<th scope="col" class="text-center">Cuti</th>
									<th scope="col" class="text-center">Alpha</th>
									<th scope="col" class="text-center">Sakit</th>
									<th scope="col" class="text-center">Luar Kota</th>
									<th scope="col" class="text-center">Lembur</th>';
					$kal.='		</tr>
							</thead><tbody>';
					$data=$this->model->get_all_karyawan();
					foreach($data->result() as $row){
						$where=" karyawan.nik=".$row->nik;
						$where.=" and month(tanggal)=".($bulan+1);
						$where.=" and year(tanggal)=".ARR_TAHUN[$tahun];
						$data_absensi=$this->model->get_absensi_where("$where");
						foreach($data_absensi->result() as $row2){
							$tanggal=strtotime($row2->tanggal);
							$tampil_tanggal=date("d/m/Y",$tanggal);
							$tampil_jam_masuk=date("H:i",strtotime($row2->jam_masuk));
							$tampil_jam_keluar=date("H:i",strtotime($row2->jam_keluar));

							$minutes = $this->calculate_work_minutes($row->nik,$row2->jam_masuk,$row2->jam_keluar,$row2->tanggal,$row2->keterangan);
							$kal.="<tr>";
							$red_text="";
							if($this->is_holiday($row2->tanggal)){
								$red_text.="red-text";
							}
								$kal.="<td class='text-center $red_text'>".$row2->nik."</td>";
								$kal.="<td class='text-center $red_text'>".$this->capitalize($row2->nama)."</td>";
								$kal.="<td class='text-center $red_text'>".$tampil_tanggal."</td>";
								if($row2->jam_masuk===NULL)
									$kal.="<td class='text-center $red_text'> - </td>";
								else
									$kal.="<td class='text-center $red_text'>".$tampil_jam_masuk."</td>";
								

								if($row2->jam_keluar===NULL)
									$kal.="<td class='text-center $red_text'> - </td>";
								else
									$kal.="<td class='text-center $red_text'>".$tampil_jam_keluar."</td>";
								
								if($minutes!=0)
									$kal.="<td class='text-center $red_text'>".$minutes."</td>";
								else
									$kal.="<td class='text-center $red_text'> - </td>";

								$tanggal=" tanggal='".$row2->tanggal."'";
								$cuti=$this->model->get_absensi_where("$where and $tanggal and upper(keterangan) like '%".LEGEND_CUTI."%'")->num_rows();
								if($cuti>0)
									$kal.="<td class='text-right $red_text'><i class='green-text material-icons'>check</i></td>";
								else
									$kal.="<td class='text-right $red_text'><i class='material-icons'>clear</i></td>";

								$alpha=$this->model->get_absensi_where("$where and $tanggal and upper(keterangan) like '%".LEGEND_ALPHA."%'")->num_rows();
								if($alpha>0)
									$kal.="<td class='text-right $red_text'><i class='material-icons  green-text'>check</i></td>";
								else
									$kal.="<td class='text-right $red_text'><i class='material-icons'>clear</i></td>";

								$sakit=$this->model->get_absensi_where("$where and $tanggal and upper(keterangan) like '%".LEGEND_SAKIT."%'")->num_rows();
								if($sakit>0)
									$kal.="<td class='text-right $red_text'><i class='material-icons green-text '>check</i></td>";
								else
									$kal.="<td class='text-right $red_text'><i class='material-icons'>clear</i></td>";

								$luarkota=$this->model->get_absensi_where("$where and $tanggal and upper(keterangan) like '%".LEGEND_LUAR_KOTA."%'")->num_rows();
								if($luarkota>0)
									$kal.="<td class='text-right $red_text'><i class='material-icons green-text '>check</i></td>";
								else
									$kal.="<td class='text-right $red_text'><i class='material-icons'>clear</i></td>";

								$lembur=$this->model->get_absensi_where("$where and $tanggal and upper(keterangan) like '%".LEGEND_LEMBUR."%'")->num_rows();
								if($lembur>0)
									$kal.="<td class='text-right $red_text'><i class='material-icons green-text '>check</i></td>";
								else
									$kal.="<td class='text-right $red_text'><i class='material-icons'>clear</i></td>";
							$kal.="</tr>";
						}
					}
					$kal.='</tbody>
								</table>';
					$kal.="</div>";
					$kal.='<div class="col-sm-1"></div>';
				$kal.="</div>";
		}
		echo $kal;
	}
	public function gen_rincian(){
		$tipe=strtoupper($this->i_p("t"));
		$tipe_dd=intval($this->i_p("td")); //kalau performance jadi NIK
		$tahun=intval($this->i_p("ta"));
		$kal="";
		$kal.="<div id='div_laporan' class='div_laporan animated fadeInDown'>";
		$kal.="<br>";
		if($this->equals($tipe,Absensi::$TABLE_NAME)){
			$tt_absensi=strtoupper(ARR_TIPE_RINCIAN_ABSENSI[$tipe_dd]);

			$concat="(Hari)";
			if($this->equals($tt_absensi,LEGEND_JAM_KERJA))
				$concat="(Menit)";

			$kal.="<h2 class='f-aleo-bold text-center'>Rincian Absensi ".ARR_TIPE_RINCIAN_ABSENSI[$tipe_dd]." - ".ARR_TAHUN[$tahun]." $concat</h2>";
			$kal.='<div class="row" style="margin-right:2%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Nama</th>';

					for($i=1;$i<count(ARR_BULAN);$i++){
						$kal.='		<th scope="col" class="text-center">'.substr(ARR_BULAN[$i],0,3).'</th>';
					}
					
					$kal.='			<th scope="col" class="text-center">Total</th>';
					if($tt_absensi=="CUTI"){
						$kal.='		<th scope="col" class="text-center">Sisa</th>';
					}

					$kal.='		</tr>
							</thead><tbody>';
							
					//Perhitungan Jam Kerja
					$arr_abs_karyawan=array();
					$data=$this->model->get_all_karyawan();
					
					if($this->equals($tt_absensi,LEGEND_JAM_KERJA)){
						foreach($data->result() as $row){
							$nik=$row->nik;
							$arr_abs_karyawan[$nik]=array();
							$where="karyawan.nik=$nik";
							$where.=" and year(tanggal)='".ARR_TAHUN[$tahun]."'";
							for($i=1;$i<count(ARR_BULAN);$i++){
								$arr_abs_karyawan[$nik][$i]=0;
								$w_bulan="month(tanggal)=$i";
								$data_absensi=$this->model->get_absensi_where("$where and $w_bulan");
								foreach($data_absensi->result() as $row2){
									$minutes=$minutes=$this->calculate_work_minutes($nik,$row2->jam_masuk,$row2->jam_keluar,$row2->tanggal,$row2->keterangan);
									$arr_abs_karyawan[$nik][$i]+=$minutes;
								}
							}
						}
						
					}
					//End Perhitungan Jam Kerja

					foreach($data->result() as $row){
						$total=0;
						$nik=$row->nik;
						$where="karyawan.nik=$nik";
						$where.=" and year(tanggal)='".ARR_TAHUN[$tahun]."'";
						if(!$this->equals($tt_absensi,LEGEND_JAM_KERJA)){
							$where.="and upper(keterangan) like '%".$tt_absensi."%'";
						}
						$kal.="<tr>";
							$kal.="<td>".ucfirst($row->k_nama)."</td>";
							//$kal.="<td class='text-right'>".$arr_abs_karyawan[$row->nik]." Menit</td>";
							if(!$this->equals($tt_absensi,LEGEND_JAM_KERJA)){
								$qry_tipe=0;
								for($i=1;$i<count(ARR_BULAN);$i++){
									$qry_tipe=$this->model->get_absensi_where("$where and month(tanggal)=$i")->num_rows();
									$kal.="<td class='text-center'>$qry_tipe</td>";
									$total+=$qry_tipe;
								}
							}else{
								for($i=1;$i<count(ARR_BULAN);$i++){
									$kal.="<td class='text-center'>".$arr_abs_karyawan[$nik][$i]."</td>";
									$total+=$arr_abs_karyawan[$nik][$i];
								}
							}
							$kal.="<td class='text-center'>$total</td>";
							if($this->equals($tt_absensi,LEGEND_CUTI)){
								$saldo_cuti=$this->db->query("select * from karyawan where nik=$nik")->row()->saldo_cuti;
								$kal.="<td class='text-center'>$saldo_cuti</td>";
							}
						$kal.="</tr>";
					}
					
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-1"></div>';
			$kal.="</div>";
		}
		else if($this->equals($tipe,$this::$NAV_ABSENSI_PERFORMANCE)){
			$karyawan=$this->karyawan_model->get_by(Karyawan::$ID,$tipe_dd)->row();
			
			$kal.='<div class="row" style="margin-right:2%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table>';
						$kal.='<tr>';
							$kal.='<td class="font-md text-right f-aleo-bold">Nama</td>';
							$kal.='<td class="font-md text-right f-aleo-bold">:</td>';
							$kal.='<td class="font-md  f-aleo">'.$this->capitalize($karyawan->k_nama).'</td>';
						$kal.='</tr>';
						$kal.='<tr>';
							$kal.='<td class="font-md text-right f-aleo-bold">NIK</td>';
							$kal.='<td class="font-md text-right f-aleo-bold">:</td>';
							$kal.='<td class="font-md  f-aleo">'.$karyawan->nik.'</td>';
						$kal.='</tr>';
						$kal.='<tr>';
							$kal.='<td class="font-md text-right f-aleo-bold">Bagian</td>';
							$kal.='<td class="font-md  f-aleo-bold">:</td>';
							$kal.='<td class="font-md  f-aleo">'.$this->capitalize($karyawan->tipe).'</td>';
						$kal.='</tr>';
					$kal.='</table>';
				$kal.='</div>';
				$kal.='<div class="col-sm-1"></div>';
			$kal.='</div>';
			$kal.='<div class="row">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center fw-bold">Bulan</th>
									<th scope="col" class="text-center fw-bold">Standar Jam Kerja (Menit)</th>
									<th scope="col" class="text-center fw-bold">Jam Kerja (Menit)</th>
									<th scope="col" class="text-center fw-bold">Sakit (Hari)</th>
									<th scope="col" class="text-center fw-bold">Alpha (Hari)</th>
									<th scope="col" class="text-center fw-bold">Cuti (Hari)</th>
									<th scope="col" class="text-center fw-bold">Lembur (Jam)</th>
									<th scope="col" class="text-center fw-bold">Tukar Hari (Hari)</th>
									<th scope="col" class="text-center fw-bold">Performance (%)</th>';
					$kal.='		</tr>
							</thead><tbody>';
					
					for($i=0;$i<count(ARR_BULAN_NO_SEMUA);$i++){
						$standar=$this->calculate_standar_jam_kerja(ARR_BULAN_NO_SEMUA[$i],ARR_TAHUN[$tahun]);
						$kal.="<tr>";
							$kal.="<td class='text-right fw-bold'>".ucfirst(ARR_BULAN_NO_SEMUA[$i])."</td>";
							$kal.="<td>".$standar."</td>";
							$where="karyawan.nik=".$karyawan->nik;
							$where.=" and month(tanggal)=".($i+1);
							$where.=" and year(tanggal)=".ARR_TAHUN[$tahun];
							
							//Jam Kerja
							$data_absensi=$this->model->get_absensi_where("$where");
							$jam_kerja=0;
							foreach($data_absensi->result() as $row2){
								$minutes=$this->calculate_work_minutes($karyawan->nik,$row2->jam_masuk,$row2->jam_keluar,$row2->tanggal,$row2->keterangan);
								$jam_kerja+=$minutes;
							}
							$kal.="<td class='text-center'>$jam_kerja</td>";

							//Sakit
							$sakit=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_SAKIT."%'")->num_rows();
							$kal.="<td class='text-center'>$sakit</td>";

							//Alpha
							$alpha=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_ALPHA."%'")->num_rows();
							$kal.="<td class='text-center'>$alpha</td>";

							//Cuti
							$cuti=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_CUTI."%'")->num_rows();
							$kal.="<td class='text-center'>$cuti</td>";

							//Lembur
							$lembur=$this->calculate_lembur_minutes($karyawan->nik,$where);
							$kal.="<td class='text-center'>".round($lembur/60,2)."</td>";

							//Tukar Hari
							$tukar=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_TUKAR_HARI."%'")->num_rows();
							$kal.="<td class='text-center'>$tukar</td>";

							//Performance
							$performa=$this->calculate_performance_kehadiran($jam_kerja,$standar);
							$kal.="<td class='text-center'>$performa</td>";

						$kal.="</tr>";
					}
					
					$kal.='</tbody>
							</table>';
				$kal.="</div>";
				$kal.='<div class="col-sm-1"></div>';
			$kal.="</div>";
		}
		else if($this->equals($tipe,$this::$NAV_SLIP_GAJI)){
			$bulan=intval($this->i_p("bu"));
			
			$karyawan=$this->karyawan_model->get_by(Karyawan::$ID,$tipe_dd)->row();
			/* Print Ke Word
			$kal.="<div class='row' style='margin-top:1%;margin-right:1%'>";
				$kal.='<div class="col-sm-4"></div>';
				$kal.='<div class="text-center col-sm-4">
							<button type="button" class="btn_print btn btn-outline-success" onclick=Export2Word("div_laporan","Slip_Gaji_'.$karyawan->nik.'")>
								PRINT KE WORD (DOCX)
							</button>
						</div>';
				$kal.='<div class="col-sm-4"></div>';
			$kal.="</div>";
			*/
			$kal.='<div class="div_print">';
				$kal.=$this->gen_slip_gaji_template($karyawan->nik,$bulan,$tahun);
			$kal.='</div>';
		}
		else if($this->equals($tipe,Karyawan_Insentif::$TABLE_NAME)){
			$bulan=intval($this->i_p("bu"));
			
			$karyawan=$this->karyawan_model->get_by(Karyawan::$ID,$tipe_dd)->row();
			$kal.='<div class="row" style="margin-right:2%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table>';
						$kal.='<tr>';
							$kal.='<td class="font-md f-aleo-bold">NIK</td>';
							$kal.='<td class="font-md text-right f-aleo-bold">&nbsp&nbsp:&nbsp</td>';
							$kal.='<td class="font-md  f-aleo">'.$karyawan->nik.'</td>';
						$kal.='</tr>';
						$kal.='<tr>';
							$kal.='<td class="font-md f-aleo-bold">Nama</td>';
							$kal.='<td class="font-md text-right f-aleo-bold">&nbsp&nbsp:&nbsp</td>';
							$kal.='<td class="font-md  f-aleo">'.$this->capitalize($karyawan->k_nama).'</td>';
						$kal.='</tr>';
						$kal.='<tr>';
							$kal.='<td class="font-md f-aleo-bold">Bulan</td>';
							$kal.='<td class="font-md text-right f-aleo-bold">&nbsp&nbsp:&nbsp</td>';
							$kal.='<td class="font-md  f-aleo">'.ARR_BULAN_NO_SEMUA[$bulan].' '.ARR_TAHUN[$tahun].'</td>';
						$kal.='</tr>';
					$kal.='</table>';
				$kal.='</div>';
				$kal.='<div class="col-sm-1"></div>';
			$kal.='</div>';
			$kal.="<br>";
			$kal.='<div class="row" style="margin-right:2%">';
				$kal.='<div class="col-sm-1"></div>';
				$kal.='<div class="col-sm-10">';
					$kal.='<table>';
			$ctr=0;
			$kal.='<input style="width:100%" class="f-aleo" value="'.$bulan.'" type="hidden" id="txt_bulan"/>';
			$kal.='<input style="width:100%" class="f-aleo" value="'.$tahun.'" type="hidden" id="txt_tahun"/>';
			$kal.='<input style="width:100%" class="f-aleo" value="'.$tipe_dd.'" type="hidden" id="txt_nik"/>';
			$data=$this->model->get_karyawan_insentif_ket_not_null($karyawan->nik,$bulan,$tahun);
					foreach($data->result() as $row){
						$lower=strtolower($row->nama);
						$periode=$row->keterangan;
						$disabled="";
						if($periode!==NULL){
							$temp=explode(" ",$periode); //$temp[0]="JANUARI"; $temp[1]="2021";
							$tahun_check=$this->get_index_from_array(ARR_TAHUN,$temp[1]);
							$bulan_check=$this->get_index_from_array(ARR_BULAN_NO_SEMUA,$temp[0]);
							
							if(date("m")!=$bulan_check||date("Y")!=$tahun_check||$row->nominal!=0){
								$disabled="disabled";
							}
							if($row->nominal==0){
								$disabled="";
							}
						}
						$kal.='<tr>';
							$kal.='<td class="font-sm f-aleo-bold">'.$this->capitalize(str_replace('_',' ',$row->nama)).'</td>';
							$kal.='<td class="font-sm text-right f-aleo-bold">&nbsp&nbsp:&nbsp</td>';
							$kal.='<td class="font-sm text-right f-aleo"><input '.$disabled.' style="width:100%" class="f-aleo txt_'.$lower.'" value='.$row->nominal.' type="number" id="txt_ctr_'.$ctr.'"/>
									<input style="width:100%" class="f-aleo" value="'.$row->nama.'" type="hidden" id="txt_nama_'.$ctr.'"/>
							</td>';
						$kal.='</tr>';
						$ctr++;
					}
			$kal.='</table>';
				$kal.='</div>';
				$kal.='<div class="col-sm-1"></div>';
			$kal.='</div>';
			if($ctr>0){
				$kal.="<div class='row' style='margin-right:1%'>";
					$kal.='<div class="col-sm-4"></div>';
					$kal.='<div class="text-center col-sm-4">
								<button type="button" class="btn btn-outline-success" onclick="update_karyawan_insentif('.$ctr.')">
									UPDATE
								</button>
							</div>';
					$kal.='<div class="col-sm-4"></div>';
				$kal.="</div>";
			}else{
				$kal.="<div class='row' style='margin-right:1%'>";
					$kal.='<div class="col-sm-4"></div>';
					$kal.='<div class="f-aleo-bold text-center red-text font-md col-sm-4">
								Tidak ada Insentif yang bisa diubah!!
							</div>';
					$kal.='<div class="col-sm-4"></div>';
				$kal.="</div>";
			}
		}
		$kal.="</div>";
		echo $kal;
	}
	public function gen_slip_gaji_all_karyawan(){
		$tahun=intval($this->i_p("ta"));
		$bulan=intval($this->i_p("bu"));
		$kal="";
		$query=$this->model->get_all_karyawan();
		$kal.="<div id='div_laporan' class='div_laporan animated fadeInDown'>";
			$kal.='<div class="div_print">';
			foreach($query->result() as $karyawan){
				if($karyawan->is_outsource==0)
					$kal.=$this->gen_slip_gaji_template($karyawan->nik,$bulan,$tahun);
			}
			$kal.='</div>';
		$kal.="</div>";
		echo $kal;
	}
	public function gen_slip_gaji_template($nik,$bulan,$tahun){
		$kal="";
		$temp_kiri=array();
		$temp_kanan=array();
		$karyawan=$this->karyawan_model->get_by(Karyawan::$ID,$nik)->row();
		$kal.='<div class="row" style="margin-right:2%">';
			$kal.='<div class="col-sm-1"></div>';
			$kal.='<div class="col-sm-6">';
				$kal.='<table style="width:50%">';
					$kal.='<tr>';
						$kal.='<td class="font-md f-aleo-bold">NIK</td>';
						$kal.='<td class="font-md text-right f-aleo-bold">&nbsp&nbsp:&nbsp</td>';
						$kal.='<td class="font-md f-aleo">'.$karyawan->nik.'</td>';
					$kal.='</tr>';
					$kal.='<tr>';
						$kal.='<td class="font-md f-aleo-bold">Nama</td>';
						$kal.='<td class="font-md text-right f-aleo-bold">&nbsp&nbsp:&nbsp</td>';
						$kal.='<td class="font-md  f-aleo">'.$this->capitalize($karyawan->k_nama).'</td>';
					$kal.='</tr>';
					$kal.='<tr>';
						$kal.='<td class="font-md f-aleo-bold">Bulan</td>';
						$kal.='<td class="font-md text-right f-aleo-bold">&nbsp&nbsp:&nbsp</td>';
						$kal.='<td class="font-md  f-aleo">'.ARR_BULAN_NO_SEMUA[$bulan].' '.ARR_TAHUN[$tahun].'</td>';
					$kal.='</tr>';
				$kal.='</table>';
			$kal.='</div>';
			$kal.='<div class="col-sm-1"></div>';
		$kal.='</div>';
		$kal.="<hr style='border:2px solid black;width:50%'>";
		$ctr_kiri=0;
		$ctr_kanan=0;

		$total=0;
		//Gaji Pokok
		$gaji_pokok=0;
		if($karyawan->gaji_pokok>0){
			$temp_kiri[$ctr_kiri]="";
			$temp_kiri[$ctr_kiri].='<tr>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Gaji Pokok</td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm"></td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah($karyawan->gaji_pokok).'</td>';
			$temp_kiri[$ctr_kiri].='</tr>';
			$ctr_kiri++;
			$gaji_pokok=$karyawan->gaji_pokok;
		}

		//Tunjangan Jabatan
		$tunjangan_jabatan=0;
		if($karyawan->tunjangan_jabatan>0){
			$temp_kiri[$ctr_kiri]="";
			$temp_kiri[$ctr_kiri].='<tr>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Tunjangan Jabatan</td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm"></td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah($karyawan->tunjangan_jabatan).'</td>';
			$temp_kiri[$ctr_kiri].='</tr>';
			$ctr_kiri++;
			$tunjangan_jabatan=$karyawan->tunjangan_jabatan;
		}

		//Tunjangan Obat
		$is_t_obat=$this->model->available_karyawan_insentif($karyawan->nik,S_INSENTIF_T_OBAT);
		$tunjangan_obat=0;
		if($is_t_obat){
			$query=$this->model->get_karyawan_insentif_ket_not_null($karyawan->nik,$bulan,$tahun,S_INSENTIF_T_OBAT);
			if($query->num_rows()>0)
				$tunjangan_obat=$query->row()->nominal;
			$temp_kiri[$ctr_kiri]="";
			$temp_kiri[$ctr_kiri].='<tr>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Tunjangan Obat</td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo font-sm"></td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah($tunjangan_obat).'</td>';
			$temp_kiri[$ctr_kiri].='</tr>';
			$ctr_kiri++;
		}

		//Insentif Kehadiran
		$insentif_kehadiran=$this->calculate_insentif_kehadiran_rupiah($karyawan->nik,$bulan,$tahun);

		if($insentif_kehadiran!=0){
			$temp_kiri[$ctr_kiri]="";
			$temp_kiri[$ctr_kiri].='<tr>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Insentif Kehadiran</td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo font-sm"></td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah($insentif_kehadiran).'</td>';
			$temp_kiri[$ctr_kiri].='</tr>';
			$ctr_kiri++;
		}
		

		//Insentif Kinerja
		$insentif_kinerja=0;
		if($insentif_kinerja>0){
			$temp_kiri[$ctr_kiri]="";
			$temp_kiri[$ctr_kiri].='<tr>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Insentif Kinerja</td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo font-sm"></td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah(0).'</td>';
			$temp_kiri[$ctr_kiri].='</tr>';
			$ctr_kiri++;
		}

		//Penyesuaian
		$penyesuaian=0;
		$query=$this->model->get_karyawan_insentif_ket_not_null($karyawan->nik,$bulan,$tahun,S_INSENTIF_PENYESUAIAN);
		if($query->num_rows()>0)
			$penyesuaian=$query->row()->nominal;
		if($penyesuaian>0){
			$temp_kiri[$ctr_kiri]="";
			$temp_kiri[$ctr_kiri].='<tr>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Insentif Lain-Lain</td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo font-sm"></td>';
				$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah($penyesuaian).'</td>';
			$temp_kiri[$ctr_kiri].='</tr>';
			$ctr_kiri++;
		}

		$total=$gaji_pokok+$tunjangan_jabatan+$tunjangan_obat+$insentif_kehadiran+$insentif_kinerja+$penyesuaian;

		//Subtotal
		$temp_kiri[$ctr_kiri]="";
		$temp_kiri[$ctr_kiri].='<tr style="border-top:2px solid black">';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Sub Total</td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo font-sm"></td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah($total).'</td>';
		$temp_kiri[$ctr_kiri].='</tr>';
		$ctr_kiri++;

		//Potong BPJS & Kesehatan
		$potong=0;
		
		if($gaji_pokok>STANDAR_POTONG_BPJS){
			$potong=$total*4/100;
		}else
			$potong=STANDAR_POTONG_BPJS*4/100;

		$temp_kiri[$ctr_kiri]="";
		$temp_kiri[$ctr_kiri].='<tr>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Potong BPJS & Kesehatan</td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo font-sm">4%</td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">-Rp. '.$this->rupiah($potong).'</td>';
		$temp_kiri[$ctr_kiri].='</tr>';
		$ctr_kiri++;

		//Total
		$temp_kiri[$ctr_kiri]="";
		$temp_kiri[$ctr_kiri].='<tr>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Total</td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo font-sm"></td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Rp. '.$this->rupiah($total-$potong).'</td>';
		$temp_kiri[$ctr_kiri].='</tr>';
		$ctr_kiri++;

		//Sistem Pembayaran
		$temp_kiri[$ctr_kiri]="";
		$temp_kiri[$ctr_kiri].='<tr>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm">Sistem Pembayaran</td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo-bold font-sm"></td>';
			$temp_kiri[$ctr_kiri].='<td class="f-aleo text-right font-sm">Transfer/Tunai</td>';
		$temp_kiri[$ctr_kiri].='</tr>';
		$ctr_kiri++;
		
		
		$standar=0;
		if($bulan!=0){
			$standar=$this->calculate_standar_jam_kerja(ARR_BULAN_NO_SEMUA[$bulan-1],ARR_TAHUN[$tahun]);
		
			//Performa Kehadiran
			$temp_kanan[$ctr_kanan]="";
			$temp_kanan[$ctr_kanan].='<tr>';
				$temp_kanan[$ctr_kanan].='<td class="text-right f-aleo-bold font-sm">Performance</td>';
				$temp_kanan[$ctr_kanan].='<td class="f-aleo font-sm">Kehadiran</td>';		
					$where="karyawan.nik=".$karyawan->nik;
					$where.=" and month(tanggal)=".($bulan);
					$where.=" and year(tanggal)=".ARR_TAHUN[$tahun];
					
					//Jam Kerja
					$data_absensi=$this->model->get_absensi_where("$where");
					$jam_kerja=0;
					foreach($data_absensi->result() as $row2){
						$minutes=$this->calculate_work_minutes($karyawan->nik,$row2->jam_masuk,$row2->jam_keluar,$row2->tanggal,$row2->keterangan);
						$jam_kerja+=$minutes;
					}

					//Performance
					$performa=$this->calculate_performance_kehadiran($jam_kerja,$standar);
				$temp_kanan[$ctr_kanan].='<td class="f-aleo text-right font-sm">'.$performa.'%</td>';
			$temp_kanan[$ctr_kanan].='</tr>';
			$ctr_kanan++;

			//Performa Produksi
			if($karyawan->kat_id==1){ 
				$temp_kanan[$ctr_kanan]="";
				$temp_kanan[$ctr_kanan].='<tr>';
					$temp_kanan[$ctr_kanan].='<td class="text-right f-aleo-bold font-sm"></td>';
					$temp_kanan[$ctr_kanan].='<td class="f-aleo font-sm">Produksi</td>';
					$standar=$this->calculate_standar_jam_kerja(ARR_BULAN_NO_SEMUA[$bulan-1],ARR_TAHUN[$tahun]);						
						$where="karyawan.nik=".$karyawan->nik;
						$where.=" and month(tanggal)=".($bulan+1);
						$where.=" and year(tanggal)=".ARR_TAHUN[$tahun];
						
						//Jam Kerja
						$data_absensi=$this->model->get_absensi_where("$where");
						$jam_kerja=0;
						foreach($data_absensi->result() as $row2){
							$minutes=$this->calculate_work_minutes($karyawan->nik,$row2->jam_masuk,$row2->jam_keluar,$row2->tanggal,$row2->keterangan);
							$jam_kerja+=$minutes;
						}

						
						//Performance
						$performa=$this->calculate_performance_kehadiran($jam_kerja,$standar);
					$temp_kanan[$ctr_kanan].='<td class="f-aleo text-right font-sm">0%</td>';
				$temp_kanan[$ctr_kanan].='</tr>';
				$ctr_kanan++;
			}

			//Performa Stock
			$temp_kanan[$ctr_kanan]="";
			$temp_kanan[$ctr_kanan].='<tr>';
				$temp_kanan[$ctr_kanan].='<td class="text-right f-aleo-bold font-sm"></td>';
				$temp_kanan[$ctr_kanan].='<td class="f-aleo font-sm">Stock</td>';
				$temp_kanan[$ctr_kanan].='<td class="f-aleo text-right font-sm">0%</td>';
			$temp_kanan[$ctr_kanan].='</tr>';
			$ctr_kanan++;

			//Performa Kebersihan
			$temp_kanan[$ctr_kanan]="";
			$temp_kanan[$ctr_kanan].='<tr>';
				$temp_kanan[$ctr_kanan].='<td class="text-right f-aleo-bold font-sm"></td>';
				$temp_kanan[$ctr_kanan].='<td class="f-aleo font-sm">Kebersihan</td>';
				$temp_kanan[$ctr_kanan].='<td class="f-aleo text-right font-sm">0%</td>';
			$temp_kanan[$ctr_kanan].='</tr>';
			$ctr_kanan++;
		}
		$kal.='<div class="row">';
			$kal.='<div class="col-sm-1"></div>';
			$kal.='<div class="col-sm-6">';
				$kal.='<table style="width:70%">';
					for($i=0;$i<count($temp_kiri);$i++){
						$kal.=$temp_kiri[$i];
					}
				$kal.='</table>';
			$kal.='</div>';
			$kal.='<div class="col-sm-4" style="float:left">';
				$kal.='<table style="width:70%">';
					for($i=0;$i<count($temp_kanan);$i++){
						$kal.=$temp_kanan[$i];
					}
				$kal.='</table>';
			$kal.='</div>';
			$kal.='<div class="col-sm-1"></div>';
		$kal.='</div>';
		$kal.="<hr style='border:1px solid black;width:90%'>";
		return $kal;
	}

	//material function
	/*public function material_rm_pm($params){
		if($this->equals($params, MATERIAL_RM_PM_UPLOAD_STOCK)){
			$this->upload_data(Material_RM_PM::$TABLE_NAME);
		} else if ($this->equals($params, MATERIAL_RM_PM_GET_HISTORY_STOCK)) {
			$dt = $this->i_p("dt");
			$st = $this->i_p("st");
			$n = $this->i_p("n");
			$m = $this->i_p("m");

			$kal = "";
			$kal .= '<div class="row">';
				$kal .= '<div class="col-sm-1"></div>';
				$kal .= '<div class="col-sm-10">';
				$kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Nomor</th>
									<th scope="col" class="text-center">Tanggal</th>
									<th scope="col" class="text-center">Material</th>
									<th scope="col" class="role-ppic role-accounting role-purchasing text-center">Vendor</th>
									<th scope="col" class="text-center">Nomor SJ</th>
									<th scope="col" class="text-center">Stock Awal</th>
									<th scope="col" class="text-center">Debit</th>
									<th scope="col" class="text-center">Kredit</th>
									<th scope="col" class="text-center">Stock Akhir</th>
									<th scope="col" class="text-center">Pesan</th>		
								</tr>
							</thead><tbody>';
			$data = $this->model->get_history_stock_rm_pm($dt, $st, $n, $m);

			foreach ($data->result_array() as $row) {
				if ($row[Stock_Gudang_RM_PM::$DEBIT] == 0)
					$kal .= "<tr class='red-text'>";
				else
					$kal .= "<tr class='green-text'>";

						$kal .= "<td>" . $row[Stock_Gudang_RM_PM::$ID] . "</td>";
						$kal .= "<td>" . date("d-m-Y", strtotime($row[Stock_Gudang_RM_PM::$TANGGAL])) . "</td>";
						//$material = $this->model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row[Stock_Gudang_RM_PM::$ID_MATERIAL])->row_array()[Material_RM_PM::$KODE_PANGGIL];
						$kal .= "<td>" . $row[Material_RM_PM::$KODE_PANGGIL] . "</td>";
					if($row[Stock_Gudang_RM_PM::$ID_VENDOR]!=null){
						$vendor=$this->model->get(Vendor::$TABLE_NAME,Vendor::$ID, $row[Stock_Gudang_RM_PM::$ID_VENDOR])->row_array()[Vendor::$NAMA];
						$kal .= "<td class='role-accounting role-ppic role-purchasing'>" . $vendor . "</td>";
					}else{
						$kal .= "<td class='role-accounting role-ppic role-purchasing'> - </td>";
					}
						$kal .= "<td>" . $row[Stock_Gudang_RM_PM::$NOMOR_SJ] . "</td>";
						$kal .= "<td class='text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$STOCK_AWAL],3) . "</td>";
						$kal .= "<td class='text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$DEBIT],3) . "</td>";
						$kal .= "<td class='text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$KREDIT],3) . "</td>";
						$kal .= "<td class='text-right'>" . $this->decimal($row[Stock_Gudang_RM_PM::$STOCK_AKHIR],3) . "</td>";
						$kal .= "<td>" . $row[Stock_Gudang_RM_PM::$PESAN] . "</td>";
					$kal .= "</tr>";
			}
				$kal .= '	</tbody>
						 </table>';
				$kal .= "</div>";
				$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
			echo $kal;
		} else if ($this->equals($params, MATERIAL_RM_PM_GET_ALL_STOCK)) {
			$kal = "";
			$data = $this->model->get_all_material_rm_pm_stock();

			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-10">';
			$kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Material Code</th>
									<th scope="col" class="text-center">Description</th>
									<th scope="col" class="text-center">Vendor</th>
									<th scope="col" class="text-center">Stock</th>
									<th scope="col" class="role-purchasing role-accounting text-center">Value</th>
									<th scope="col" class="role-purchasing role-accounting text-center">Value per Unit</th>
									';
			$kal .= '			</tr>
							</thead><tbody>';
			foreach($data->result_array() as $row_material){
				$kal .= "<tr>";
				$kal .= "<td class='text-center'>" . $row_material[Material_RM_PM::$KODE_MATERIAL] . "</td>";
				$kal .= "<td class='text-left'>" . $row_material[Material_RM_PM::$DESCRIPTION] . "</td>";
				if($row_material[Material_RM_PM_Stock::$ID_VENDOR]!=null){
					$vendor=$this->model->get(Vendor::$TABLE_NAME,Vendor::$ID,$row_material[Material_RM_PM_Stock::$ID_VENDOR])->row_array();
					$kal .= "<td class='text-left'>" . $vendor[Vendor::$NAMA] . "</td>";
				}else{
					$kal .= "<td class='text-left'> - </td>";

				}
				$kal .= "<td class='text-right'>" . $this->decimal($row_material[Material_RM_PM_Stock::$QTY],3) . "</td>";
				$kal .= "<td class='text-right role-purchasing  role-accounting'>" . $this->rupiah($row_material[Material_RM_PM_Stock::$VALUE]) . "</td>";
				$kal .= "<td class='text-right role-purchasing  role-accounting'>" . $this->rupiah($row_material[Material_RM_PM_Stock::$VALUE]/ $row_material[Material_RM_PM_Stock::$QTY],3) . "</td>";
				$kal .= "</tr>";
			}
			$kal .= '</tbody>
							</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
			echo $kal;
		} else if ($this->equals($params, MATERIAL_RM_PM_PLUS)) {
			$ctr = $this->i_p("c");
			$tipe = $this->i_p("t");

			$disabled = "";
			$width_1 = "width:5%";
			$width_2 = "width:8%";
			if ($this->equals($tipe, "OPNAME")) {
				$mode_1 = "stock_program";
				$mode_2 = "stock_fisik";
				$disabled = "disabled";
				$event = 'onchange="check_varian_color(' . $ctr . ')" onkeyup="check_varian_color(' . $ctr . ')"';
				$width_1 = "width:12%";
				$width_2 = "width:12%";
			}
			$kal = '
			<tr class="tr_detail_product_' . $ctr . '">
				<td style="width:1%" class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>
				<td style="width:2%" class="text-left font-sm f-aleo"> 
					<input style="width:100%" onkeyup="check_product(' . $ctr . ')" class="f-aleo" type="text" id="txt_kode_panggil_' . $ctr . '"/>
					<input style="width:100%" class="f-aleo" type="hidden" id="txt_id_material_' . $ctr . '"/>
				</td>
			
				<td style="width:10%" class="text-center font-sm f-aleo"> 
					<input style="border:0; width:100%" value="barang belum ditemukan!" disabled class="red-text text-center f-aleo" type="text" id="txt_barang_desc_' . $ctr . '"/>
				</td>
			
				<td style="' . $width_2 . '" id="td_vendor_' . $ctr . '" class="text-left font-sm f-aleo"></td>

				<td style="' . $width_1 . '" class="text-right font-sm f-aleo"> 
					<input ' . $disabled . ' value=0 min=0 style="width:100%" disabled class="text-right f-aleo" type="number" id="txt_stock_' . $ctr . '"/>
				</td>
			
				<td style="' . $width_1 . '" class="text-left font-sm f-aleo"> 
					<input ' . $disabled . ' onkeyup="check_adjustment_qty(' . $ctr . ')" placeholder=0 min=0 style="width:100%" class="text-right f-aleo" type="number" id="txt_qty_proses_' . $ctr . '"/>
				</td>
				';
			$kal .= '
				<td style="width:2%" class="font-sm text-center f-aleo"> 
					<button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
					X
					</button>
				</td>
			</tr>';
			echo $kal;
		} else if ($this->equals($params, MATERIAL_RM_PM_GET)) {
			$kode_panggil = $this->i_p("kp");
			$tipe= ($this->i_p("t")!==null) ? $this->i_p("t") : null;
			$num_rows = $this->model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$KODE_PANGGIL,$kode_panggil)->num_rows();
			$temp = [];
			$temp["num_rows"] = $num_rows;

			if ($num_rows > 0) {
				$is_konsinyasi_and_not = false;
				$qry_material = $this->model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$KODE_PANGGIL,$kode_panggil);
				if ($qry_material->num_rows() > 1) {
					foreach ($qry_material->result_array() as $row_material) {
						if ($tipe==null && $row_material[Material_RM_PM::$IS_KONSINYASI]) { //tipe == null artinya ambil konsinyasi
							$material = $row_material;
							$is_konsinyasi_and_not = true;
						}else{
							if(!$row_material[Material_RM_PM::$IS_KONSINYASI])
								$material = $row_material;
						}
					}
				} else {
					$material = $qry_material->row_array();
				}
				$temp[Material_RM_PM::$DESCRIPTION] = $material[Material_RM_PM::$DESCRIPTION];
				$temp[Material_RM_PM::$ID] = $material[Material_RM_PM::$ID];
				$qry_stock = $this->model->get(Material_RM_PM_Stock::$TABLE_NAME, Material_RM_PM::$ID, $material[Material_RM_PM::$ID], null, null, 1);

				$stock = 0;
				$idx_vendor = 0;
				if ($qry_stock->num_rows() > 1) { //jika material rm pm (konsinyasi dan tidak konsinyasi) ada 2 stock atau lebih berarti harus ada pilihan vendor
					$stock = $qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
					if (!$is_konsinyasi_and_not) { //jika material tsb bukan punya 2 tipe (konsinyasi dan bukan)
						foreach ($qry_stock->result_array() as $row_stock) {
							$vendor = $this->model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_stock[Material_RM_PM_Stock::$ID_VENDOR])->row_array()[Vendor::$NAMA];
							$temp["vendor_nama"][$idx_vendor] = $vendor;
							$temp["vendor_qty"][$idx_vendor] = $row_stock[Material_RM_PM_Stock::$QTY];
							$temp["vendor_id"][$idx_vendor] = $row_stock[Material_RM_PM_Stock::$ID_VENDOR];
							$idx_vendor++;
						}
					}
				} else {
					$stock = $qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
				}

				$temp["ctr_vendor"] = $idx_vendor;
				$temp[Material_RM_PM::$DESCRIPTION] = $material[Material::$DESCRIPTION];
				$temp["stock"] = $stock;
			}
			echo json_encode($temp);
		} else if ($this->equals($params, MATERIAL_RM_PM_GET_ALL)){
			$data=$this->model->get(Material_RM_PM::$TABLE_NAME);
			$temp=[];
			$ctr=0;
			foreach($data->result_array() as $row){
				$temp[$ctr]["id"] = $row[Material_RM_PM::$ID];
				$temp[$ctr]["description"]=$row[Material_RM_PM::$KODE_PANGGIL]." (".$row[Material_RM_PM::$DESCRIPTION].")";
				$ctr++;
			}
			echo json_encode($temp);
		} else if ($this->equals($params, MATERIAL_RM_PM_INSERT_GOOD_RECEIPT_MUTASI)) {
			$this->insert($this::$NAV_RM_PM_GOOD_RECEIPT_MUTASI);
		} else if ($this->equals($params, MATERIAL_RM_PM_INSERT_GOOD_ISSUE_MUTASI)) {
			$this->insert($this::$NAV_RM_PM_GOOD_ISSUE_MUTASI);
		}
		
	}*/
	public function material($params){
		if ($this->equals($params, MATERIAL_GET)) {
			$kode_barang = $this->i_p("kb");
			$kemasan = $this->i_p("k");
			$warna = $this->i_p("w");
			$num_rows = $this->model->get_material($kode_barang, $kemasan, $warna)->num_rows();
			$temp = [];
			$temp["num_rows"] = $num_rows;
			if ($num_rows > 0) {
				$material = $this->model->get_material($kode_barang, $kemasan, $warna)->row_array();
				$temp["description"] = $material[Material::$DESCRIPTION];
				$stock = $this->model->get_stock_by($material[Material::$ID]);
				$stock_dus = 0;
				$stock_tin = 0;
				if ($material[Material::$BOX] > 1) {
					$stock_dus = intdiv($stock, $material[Material::$BOX]);
					$stock_tin = $stock % $material[Material::$BOX];
				} else {
					$stock_tin = $stock;
				}
				$temp["stock_dus"] = $stock_dus;
				$temp["box"] = $material[Material::$BOX];
				$temp["stock_tin"] = $stock_tin;
				$stock_fisik = $this->model->get_fisik_stock_by($material[Material::$ID]);
				$temp['stock_fisik'] = $stock_fisik;
				$temp['stock_program'] = $stock;
			}
			echo json_encode($temp);
		} else if ($this->equals($params, MATERIAL_PLUS)) {
			$ctr = $this->i_p("c");
			$tipe = $this->i_p("t");

			$mode_1 = "dus";
			$mode_2 = "tin";
			$disabled = "";
			$event = 'onchange="check_product(' . $ctr . ')"';
			$width_1 = "width:10%";
			$width_2 = "width:7%";
			if ($this->equals($tipe, "OPNAME")) {
				$mode_1 = "stock_program";
				$mode_2 = "stock_fisik";
				$disabled = "disabled";
				$event = 'onchange="check_varian_color(' . $ctr . ')" onkeyup="check_varian_color(' . $ctr . ')"';
				$width_1 = "width:12%";
				$width_2 = "width:12%";
			}
			$kal = '
			<tr class="tr_detail_product_' . $ctr . '">
				<td style="width:1%" class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>
				<td style="width:10%" class="text-left font-sm f-aleo"> 
					<input style="width:100%" onchange="check_product(' . $ctr . ')" class="f-aleo" type="text" id="txt_kode_barang_' . $ctr . '"/>
				</td>
			
				<td style="width:10%" class="text-left font-sm f-aleo"> 
					<input style="width:100%" onchange="check_product(' . $ctr . ')" class="f-aleo" type="text" id="txt_warna_' . $ctr . '"/>
				</td>
			
				<td style="width:5%" class="text-left font-sm f-aleo"> 
					<input style="width:100%" onchange="check_product(' . $ctr . ')" class="f-aleo" type="text" id="txt_kemasan_' . $ctr . '"/>
				</td>
			
				<td style="' . $width_2 . '" class="text-left font-sm f-aleo"> 
					<input value=0 min=0 style="width:100%" ' . $event . ' class="f-aleo" type="number" id="txt_' . $mode_2 . '_' . $ctr . '"/>
				</td>

				<td style="' . $width_1 . '" class="text-left font-sm f-aleo"> 
					<input ' . $disabled . ' value=0 min=0 style="width:100%" onchange="check_product(' . $ctr . ')" class="f-aleo" type="number" id="txt_' . $mode_1 . '_' . $ctr . '"/>
				</td>
			
				
				';
			if ($this->equals($tipe, "OPNAME")) {
				$kal .= '<td style="width:5%" class="text-left font-sm f-aleo"> 
							<input value=0 min=0 disabled style="width:100%" class="f-aleo" type="number" id="txt_varian_' . $ctr . '"/>
						</td>';
			}
			if ($this->equals($tipe, "OUT")) {
				$kal .= '<input style="width:100%" type=hidden value=0 id="txt_box_' . $ctr . '"/>
						<td style="width:7%" class="text-left font-sm f-aleo"> 
							<input min=0 style="width:100%" disabled value="0" onchange="check_product(' . $ctr . ')" class="f-aleo" type="number" id="txt_stock_tin_' . $ctr . '"/>
						</td>
						<td style="width:5%" class="text-left font-sm f-aleo"> 
							<input min=0 style="width:100%" disabled value="0" onchange="check_product(' . $ctr . ')" class="f-aleo" type="number" id="txt_stock_dus_' . $ctr . '"/>
						</td>

						';
			}
			$kal .= '
				<td class="font-sm f-aleo"> 
					<input style="border:0;width:100%" value="barang belum ditemukan!" disabled class="f-aleo text-center red-text" type="text" id="txt_barang_desc_' . $ctr . '">
				</td>
				<td style="width:5%" class="font-sm text-center f-aleo"> 
					<button style="width:100%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
					X
					</button>
				</td>
			</tr>';
			echo $kal;
		} else if ($this->equals($params, MATERIAL_PLUS_ADMIN_RM_PM)) {
			$ctr = $this->i_p("c");
			$product_code = $this->i_p("p");

			$disabled = "";
			$width_1 = "width:5%";
			$width_2 = "width:8%";
			
			$material=$this->model->material_get_by_product_code_like($product_code);

			if($material->num_rows() > 0){
				$option="";
				foreach($material->result_array() as $row){
					$option.= "<option value='".$row[Material::$ID]. "'>" . $row[Material::$KEMASAN] . "</option>";
				}
				$kal = '
					<tr class="tr_detail_product_' . $ctr . '">
						<td style="width:1%" class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>
						<td style="width:2%" class="text-left font-sm f-aleo"> 
							<select class="form-control" id="dd_kemasan_'.$ctr.'">
							'.$option.'
							</select>
						</td>
					
						<td style="' . $width_1 . '" class="text-right font-sm f-aleo"> 
							<input value=0 min=0 style="width:100%" class="text-right f-aleo" type="number" id="txt_tin_' . $ctr . '"/>
						</td>
					
						<td style="' . $width_1 . '" class="text-left font-sm f-aleo"> 
							<input value=0 min=0 style="width:100%" class="text-right f-aleo" type="number" id="txt_berat_' . $ctr . '"/>
						</td>
					';
				$kal .= '
						<td style="width:2%" class="font-sm text-center f-aleo"> 
							<button style="width:90%" type="button" class="text-center center btn btn-outline-danger" onclick="minus_product(' . $ctr . ')" data-mdb-ripple-color="dark">
							X
							</button>
						</td>
					</tr>';
				echo $kal;
			}
		} else if ($this->equals($params, MATERIAL_INSERT_GOOD_RECEIPT)) {
			$this->insert($this::$NAV_GOOD_RECEIPT);
		} else if ($this->equals($params, MATERIAL_INSERT_GOOD_RECEIPT_ADJUSTMENT)) {
			$this->insert($this::$NAV_GOOD_RECEIPT_ADJUSTMENT);
		} else if ($this->equals($params, MATERIAL_INSERT_FISIK_STOCK)) {
			$this->insert(Material_Fisik::$TABLE_NAME);
		} else if ($this->equals($params, MATERIAL_GET_GOOD_RECEIPT_BY_SJ)) {
			$sj = $this->i_p("i");
			$query = $this->model->get_good_receipt_by_sj($sj);
			$data = [];
			$data["num_rows"] = $query->num_rows();
			if ($query->num_rows() > 0) {
				$rows = $query->row_array();
				$data[H_Good_Receipt::$STO] = $rows[H_Good_Receipt::$STO];
				$tanggal = date('d F Y H:i', strtotime($rows[H_Good_Receipt::$TANGGAL]));
				$ctr = 0;
				$arr_tampil = [];
				$ctr_tampil = 0;
				$kal = '
				<thead>
					<tr>
						<td class="text-center font-sm f-aleo">Nomor</td>
						<td class="text-center font-sm f-aleo">Kode Barang</td>
						<td class="text-center font-sm f-aleo">Warna</td>
						<td class="text-center font-sm f-aleo">Kemasan</td>
						<td class="text-center font-sm f-aleo">Tin</td>
						<td class="text-center font-sm f-aleo">Dus</td>
						<td class="text-center font-sm f-aleo">Gudang</td>
						<td class="text-center font-sm f-aleo">Barang</td>
					</tr>
				</thead><tbody>';

				foreach ($query->result_array() as $row) {
					$idx = -1;
					for ($i = 0; $i < $ctr_tampil; $i++) {
						if ($this->equals($row[H_Good_Receipt::$ID], $arr_tampil[$i][H_Good_Receipt::$ID]))
							$idx = $i;
					}
					if ($idx == -1) {
						$arr_tampil[$ctr_tampil][H_Good_Receipt::$ID] = $row[H_Good_Receipt::$ID];
						$karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Receipt::$NIK])->row_array();
						$arr_tampil[$ctr_tampil][Karyawan::$NAMA] = $karyawan["k_nama"];

						if ($row[H_Good_Receipt::$KODE_REVISI] != null)
							$arr_tampil[$ctr_tampil][Karyawan::$NAMA] = $karyawan["k_nama"] . "(R)";
						$ctr_tampil++;
					}
					$desc = $this->model->get_material($row[Material::$KODE_BARANG], $row[Material::$KEMASAN], $row[Material::$WARNA])->row_array()[Material::$DESCRIPTION];
					$kal .= '
						<tr class="tr_detail_product_' . $ctr . '">
							<td style="width:1%" class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>
							<td style="width:10%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$KODE_BARANG] . '
							</td>
						
							<td style="width:10%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$WARNA] . '
							</td>

							<td style="width:5%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$KEMASAN] . '
							</td>
						
							<td style="width:5%" class="text-right font-sm f-aleo"> 
								' . $row[D_Good_Receipt::$TIN] . '
							</td>
							
							<td style="width:5%" class="text-right font-sm f-aleo"> 
								' . $row[D_Good_Receipt::$DUS] . '
							</td>
							<td class="text-left font-sm f-aleo">';
					$temp = $this->model->get(Gudang::$TABLE_NAME,Gudang::$ID,$row[D_Good_Receipt::$ID_GUDANG]);
					foreach ($temp->result_array() as $row2) {
						if ($this->equals($row[D_Good_Receipt::$ID_GUDANG], $row2[Gudang::$ID]))
							$kal .= $row2[Gudang::$NAMA_GUDANG] . ' - RAK ' . $row2[Gudang::$RAK] . $row2[Gudang::$KOLOM] . ' ' . $row2[Gudang::$TINGKAT];
					}
					$kal .= '
							</td>

							<td class="font-sm f-aleo"> 
								' . $desc . '
							</td>
						</tr>';
					$ctr++;
				}
				$kal .= "</tbody>";

				$kode_gr = "";
				$nama = "";
				for ($i = 0; $i < $ctr_tampil; $i++) {
					$kode_gr .= "| " . $arr_tampil[$i][H_Good_Receipt::$ID] . " |";
					$nama  .= "| " . $arr_tampil[$i][Karyawan::$NAMA] . " |";
				}
				$data["html"] = 'Kode GR : ' . $kode_gr . '<br> Dibuat oleh : ' . $nama . '<br>Tanggal: ' . $tanggal;
				$data["ctr"] = $ctr;
				$data["kode"] = $sj; //Surat Jalan
				$data["detail_product"] = $kal;
			}
			echo json_encode($data);
		} else if ($this->equals($params, MATERIAL_GET_GOOD_ISSUE_BY_SJ)) {
			$sj = $this->i_p("i");
			$query = $this->model->get_good_issue_by_sj($sj);
			$data = [];
			$data["num_rows"] = $query->num_rows();
			if ($query->num_rows() > 0) {
				$rows = $query->row_array();
				$data[H_Good_Issue::$STO] = $rows[H_Good_Issue::$STO];
				$tanggal = date('d F Y H:i', strtotime($rows[H_Good_Issue::$TANGGAL]));
				$data[H_Good_Issue::$EKSPEDISI] = $rows[H_Good_Issue::$EKSPEDISI];
				$ctr = 0;
				$ctr_tampil = 0;
				$arr_tampil = [];
				$kal = '
				<thead>
					<tr>
						<td class="text-center font-sm f-aleo">Nomor</td>
						<td class="text-center font-sm f-aleo">Kode Barang</td>
						<td class="text-center font-sm f-aleo">Warna</td>
						<td class="text-center font-sm f-aleo">Kemasan</td>
						<td class="text-center font-sm f-aleo">Tin</td>
						<td class="text-center font-sm f-aleo">Dus</td>
						<td class="text-center font-sm f-aleo">Gudang</td>
						<td class="text-center font-sm f-aleo">Barang</td>
					</tr>
				</thead><tbody>';
				foreach ($query->result_array() as $row) {
					$idx = -1;
					for ($i = 0; $i < $ctr_tampil; $i++) {
						if ($this->equals($row[H_Good_Issue::$ID], $arr_tampil[$i][H_Good_Issue::$ID]))
							$idx = $i;
					}
					if ($idx == -1) {
						$arr_tampil[$ctr_tampil][H_Good_Issue::$ID] = $row[H_Good_Issue::$ID];
						$karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK])->row_array();
						if ($row[H_Good_Issue::$KODE_REVISI] == null)
							$arr_tampil[$ctr_tampil][Karyawan::$NAMA] = $karyawan["k_nama"];
						else
							$arr_tampil[$ctr_tampil][Karyawan::$NAMA] = $karyawan["k_nama"] . "(R)";
						$ctr_tampil++;
					}
					$material = $this->model->get_material($row[Material::$KODE_BARANG], $row[Material::$KEMASAN], $row[Material::$WARNA])->row_array();
					$kal .= '
						<tr class="tr_detail_product_' . $ctr . '">
							<td style="width:1%" class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>
							<td style="width:10%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$KODE_BARANG] . '
							</td>
						
							<td style="width:10%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$WARNA] . '
							</td>

							<td style="width:5%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$KEMASAN] . '
							</td>
						
							<td style="width:5%" class="text-right font-sm f-aleo"> 
								' . $row[D_Good_Issue::$TIN] . '
							</td>
							
							<td style="width:5%" class="text-right font-sm f-aleo"> 
								' . $row[D_Good_Issue::$DUS] . '
							</td>

							<td class="text-center font-sm f-aleo">';
					$temp = $this->model->get(Gudang::$TABLE_NAME,Gudang::$ID,$row[D_Good_Issue::$ID_GUDANG]);
					foreach ($temp->result_array() as $row2) {
						if ($this->equals($row[D_Good_Issue::$ID_GUDANG], $row2[Gudang::$ID]))
							$kal .= $row2[Gudang::$NAMA_GUDANG] . ' - RAK ' . $row2[Gudang::$RAK] . $row2[Gudang::$KOLOM] . ' ' . $row2[Gudang::$TINGKAT];
					}
					$kal .= '
							</td>

							<td class="font-sm f-aleo"> 
								' . $material[Material::$DESCRIPTION] . '
							</td>
						</tr>';
					$ctr++;
				}
				$kal .= "</tbody>";

				$kode_gi = "";
				$nama = "";
				for ($i = 0; $i < $ctr_tampil; $i++) {
					$kode_gi .= "| " . $arr_tampil[$i][H_Good_Issue::$ID] . " |";
					$nama  .= "| " . $arr_tampil[$i][Karyawan::$NAMA] . " |";
				}
				$data["html"] = 'Kode GI : ' . $kode_gi . '<br> Dibuat oleh : ' . $nama . '<br>Tanggal: ' . $tanggal;
				$data["ctr"] = $ctr;
				$data["kode"] = $sj; //Surat Jalan
				$data["detail_product"] = $kal;
			}
			echo json_encode($data);
		} else if ($this->equals($params, MATERIAL_INSERT_GOOD_ISSUE)) {
			$this->insert($this::$NAV_GOOD_ISSUE);
		} else if ($this->equals($params, MATERIAL_INSERT_GOOD_ISSUE_ADJUSTMENT)) {
			$this->insert($this::$NAV_GOOD_ISSUE_ADJUSTMENT);
		} else if ($this->equals($params, MATERIAL_GET_HISTORY_STOCK)) {
			$dt = $this->i_p("dt");
			$st = $this->i_p("st");
			$kb = $this->i_p("kb");
			$k = $this->i_p("k");
			$w = $this->i_p("w");
			$g = $this->i_p("g");
			$n = $this->i_p("n");

			$kal = "";
			$kal .= '<div class="row">';
				$kal .= '<div class="col-sm-1"></div>';
				$kal .= '<div class="col-sm-10">';
				$kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">Nomor</th>
									<th scope="col" class="text-center">Tanggal</th>
									<th scope="col" class="text-center">Material</th>
									<th scope="col" class="text-center">Stock Awal</th>
									<th scope="col" class="text-center">Debit</th>
									<th scope="col" class="text-center">Kredit</th>
									<th scope="col" class="text-center">Stock Akhir</th>
									<th scope="col" class="text-center">Gudang</th>
									<th scope="col" class="text-center">Pesan</th>		
								</tr>
							</thead><tbody>';
			$data = $this->model->get_history_stock($n, $dt, $st, $kb, $k, $w, $g);

			foreach ($data->result_array() as $row) {
				if ($row[Stock_Gudang::$DEBIT] == 0)
					$kal .= "<tr class='red-text'>";
				else
					$kal .= "<tr class='green-text'>";

					$kal .= "<td>" . $row[Stock_Gudang::$ID] . "</td>";
					$kal .= "<td>" . date("Y-m-d H:i", strtotime($row[Stock_Gudang::$TANGGAL])) . "</td>";
					$material = $this->model->get(Material::$TABLE_NAME, Material::$ID, $row[Stock_Gudang::$ID_MATERIAL])->row_array()[Material::$PRODUCT_CODE];
					$kal .= "<td>" . $material . "</td>";
					$kal .= "<td>" . $row[Stock_Gudang::$STOCK_AWAL] . "</td>";
					$kal .= "<td>" . $row[Stock_Gudang::$DEBIT] . "</td>";
					$kal .= "<td>" . $row[Stock_Gudang::$KREDIT] . "</td>";
					$kal .= "<td>" . $row[Stock_Gudang::$STOCK_AKHIR] . "</td>";
					$gudang = $this->model->get(Gudang::$TABLE_NAME, Gudang::$ID, $row[Gudang::$ID])->row_array();
					$temp = $gudang[Gudang::$NAMA_GUDANG] . " - RAK " . $gudang[Gudang::$RAK] . $gudang[Gudang::$KOLOM] . ' ' . $gudang[Gudang::$TINGKAT];
					$kal .= "<td>" . $temp . "</td>";
					$kal .= "<td>" . $row[Stock_Gudang::$PESAN] . "</td>";
				$kal .= "</tr>";
			}
			$kal .= '</tbody>
							</table>';
				$kal .= "</div>";
				$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
			echo $kal;
		} else if ($this->equals($params, MATERIAL_GET_ALL)) {
			$t = $this->i_p("t");
			$k = $this->i_p("k");
			$kb = $this->i_p("kb");
			$data = $this->model->get_material_distinct($t, $kb, $k);
			$temp = [];
			$ctr = 0;
			foreach ($data->result() as $row) {
				$temp[$ctr]["value"] = $row->value;
				$ctr++;
			}
			echo json_encode($temp);
		} else if ($this->equals($params, MATERIAL_GET_ALL_STOCK)) {
			$kal = "";
			$data = $this->model->get_all_material_stock();

			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-2"></div>';
			$kal .= '<div class="col-sm-8">';
			$kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">FG Code</th>
									<th scope="col" class="text-center">Product Code</th>
									<th scope="col" class="text-center">Description</th>
									<th scope="col" class="text-center">Program Stock</th>
									';
			$kal .= '		</tr>
							</thead><tbody>';
			for ($i = 0; $i < count($data); $i++) {
				$kal .= "<tr>";
				$kal .= "<td class='text-center'>" . $data[$i][Material::$ID] . "</td>";
				$kal .= "<td class='text-center'>" . $data[$i][Material::$PRODUCT_CODE] . "</td>";
				$kal .= "<td class='text-center'>" . $data[$i][Material::$DESCRIPTION] . "</td>";
				$kal .= "<td class='text-center'>" . $data[$i][Fifo_Stock_Gudang::$QTY] . "</td>";
				$kal .= "</tr>";
			}
			$kal .= '</tbody>
							</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-2"></div>';
			$kal .= "</div>";
			echo $kal;
		} else if ($this->equals($params, MATERIAL_UPLOAD_STOCK)) {
			$this->upload_data(MATERIAL_UPLOAD_STOCK);
		} else if ($this->equals($params, MATERIAL_GET_VIEW_STOCK_OPNAME)) {
			$jenis = $this->i_p('j');
			$result = [];
			$result["button_view"] = "";
			$kal_button = "";
			$kal_content = "";
			if ($this->equals($jenis, "HARIAN")) {
				$kal_button .= '<button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
							Tambah Barang
						</button>
						<button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="insert_fisik_stock()" data-mdb-ripple-color="dark">
							Generate
						</button>';
				$kal_content = '<table class="tabel_detail_product" style="visibility:hidden">
									<tr>
										<td class="text-center font-sm f-aleo">Nomor</td>
										<td class="text-center font-sm f-aleo">Kode Barang</td>
										<td class="text-center font-sm f-aleo">Warna</td>
										<td class="text-center font-sm f-aleo">Kemasan</td>
										<td class="text-center font-sm f-aleo">Fisik</td>
										<td class="text-center font-sm f-aleo">Program</td>
										<td class="text-center font-sm f-aleo">Varian</td>
										<td class="text-center font-sm f-aleo">Barang</td>
									</tr>
								</table>';
			} else {
				$kal_button .= '<button type="button" id="btn_generate" class="center btn btn-outline-success" data-toggle="modal" data-target="#mod_upl_stock_opname_tahunan" data-mdb-ripple-color="dark">
							Upload Stock Opname Tahunan
						</button>';

				$kal_content .= '<div class="row text-center f-aleo-bold">';
				$kal_content .= '<div class="col-sm-5 text-right">';
				$kal_content .= '<input type="checkbox" onchange="toggle_checkbox_gudang()" id="cb_gudang">';
				$kal_content .= '</div>';
				$kal_content .= '<div class="col-sm-7 text-left">';
				$kal_content .= 'Tampilkan Lokasi Gudang';
				$kal_content .= '</div>';
				$kal_content .= '</div>';
				$kal_content .= '<table id="mastertable" class="table table-bordered table-sm">
								<thead>
									<tr class="table-primary">
										<th scope="col" class="text-center">FG Code</th>
										<th scope="col" class="text-center">Product Code</th>
										<th scope="col" class="text-center">Description</th>
										<th scope="col" class="text-center data_gudang">Gudang</th>
										<th scope="col" class="text-center">Program</th>
										<th scope="col" class="text-center">Fisik</th>
										<th scope="col" class="text-center">Varian</th>
									</tr>
								</thead><tbody>';
				$data = $this->model->get_all_material_stock("Y");

				for ($i = 0; $i < count($data); $i++) {
					$kal_content .= "<tr>";
					$kal_content .= "<td class='text-center'>" . $data[$i][Material::$ID] . "</td>";
					$kal_content .= "<td class='text-center'>" . $data[$i][Material::$PRODUCT_CODE] . "</td>";
					$kal_content .= "<td class='text-center'>" . $data[$i][Material::$DESCRIPTION] . "</td>";
					$kal_content .= "<td class='text-center data_gudang'>" . $data[$i]['GUDANG'] . "</td>";
					$kal_content .= "<td class='text-right'>" . number_format($data[$i][Fifo_Stock_Gudang::$QTY], 0, '.', ',') . "</td>";
					$kal_content .= "<td class='text-right'>" . number_format($data[$i][Material_Fisik::$QTY_FISIK], 0, '.', ',') . "</td>";
					$kal_content .= "<td class='text-right'>" . number_format(($data[$i][Material_Fisik::$QTY_FISIK] - $data[$i][Fifo_Stock_Gudang::$QTY]), 0, '.', ',') . "</td>";
					$kal_content .= "</tr>";
				}
				$kal_content .= '</tbody>
								</table>';

				$kal_content .= "</div>";
			}
			$result["button_view"] = $kal_button;
			$result["content_view"] = $kal_content;
			echo json_encode($result);
		} else if ($this->equals($params, MATERIAL_UPLOAD_STOCK_OPNAME)) {
			$this->upload_data(MATERIAL_UPLOAD_STOCK_OPNAME);
		} else if ($this->equals($params, MATERIAL_GET_HISTORY_STOCK_OPNAME)) {
			$dt = $this->i_p("dt");
			$st = $this->i_p("st");

			$kal = "";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-10">';
			$kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									
									<th scope="col" class="text-center">Tanggal</th>
									<th scope="col" class="text-center">NIK</th>
									<th scope="col" class="text-center">FG Code</th>
									<th scope="col" class="text-center">Kode Produk</th>
									<th scope="col" class="text-center">Fisik</th>
									<th scope="col" class="text-center">Program</th>
									<th scope="col" class="text-center">Varian</th>		
								</tr>
							</thead><tbody>';
			$data = $this->model->get_history_stock_opname($dt, $st);

			foreach ($data->result() as $row) {
				$kal .= "<tr>";
				//$kal.= "<td class='text-center'>".$row->id_material_fisik."</td>";
				$kal .= "<td class='text-center'>" . date("Y-m-d H:i:s", strtotime($row->tanggal)) . "</td>";
				$kal .= "<td class='blue-text text-center' title='" . $row->nama . "'>" . $row->nik . "</td>";
				$kal .= "<td class='text-center'>" . $row->id_material . "</td>";
				$kal .= "<td class='text-center'>" . $row->product_code . "</td>";
				$kal .= "<td class='text-right'>" . $this->decimal($row->qty_fisik) . "</td>";
				$kal .= "<td class='text-right'>" . $this->decimal($row->qty_program) . "</td>";

				$kal .= "<td class='text-right'>" . $this->decimal(($row->qty_fisik - $row->qty_program)) . "</td>";
				$kal .= "</tr>";
			}
			$kal .= '</tbody>
							</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
			echo $kal;
		} else if ($this->equals($params, MATERIAL_GEN_REKAP_SJ)) {
			$tanggal_dari = $this->i_p("dt");
			$tanggal_sampai = $this->i_p("st");
			$nomor_sj = $this->i_p("n");
			$kode_barang=$this->i_p("kb");
			$kemasan=$this->i_p("k");
			$warna=$this->i_p("w");
			$header = "";
			$query = "";
			
			$header = '<th scope="col" class="text-center">Tanggal Input</th>
						<th scope="col" class="text-center">Tanggal Kirim</th>
						<th scope="col" class="text-center">Kode</th>
						<th scope="col" class="text-center">NIK</th>
						<th scope="col" class="text-center">Nomor SJ</th>
						<th scope="col" class="text-center">FG Code</th>
						<th scope="col" class="text-center">Product Code</th>
						<th scope="col" class="text-center">Tin</th>';
			$query = $this->model->good_receipt_get_all_rekap_sj($tanggal_dari, $tanggal_sampai, $nomor_sj,$kode_barang,$kemasan,$warna);

			$kal = "";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-10">';
			$kal .= '<table id="mastertable" class="table table-bordered table-sm">
						<thead>
							<tr class="table-primary">
								' . $header . '
							</tr>
						</thead><tbody>';
			if ($query->num_rows() > 0) {
				foreach ($query->result_array() as $row) {
					$kal .= "<tr class='green-text'>";
						$kal .= "<td class='text-center'>" . date('Y-m-d', strtotime($row[H_Good_Receipt::$TANGGAL])) . "</td>";
						$kal .= "<td class='text-center'> - </td>";
						$kal .= "<td class='text-center'>" . $row[H_Good_Receipt::$ID] . "</td>";
						
						$karyawan=$this->karyawan_model->get(Karyawan::$TABLE_NAME,Karyawan::$ID,$row[H_Good_Receipt::$NIK])->row_array();
						$kal .= "<td class='text-center' title='".$karyawan[Karyawan::$NAMA]."'>" . $row[H_Good_Receipt::$NIK] . "</td>";
						$kal .= "<td class='text-center'>" . $row[H_Good_Receipt::$NOMOR_SJ] . "</td>";
						$kal .= "<td class='text-center'>" . $row[D_Good_Receipt::$ID_MATERIAL] . "</td>";
						
						$material=$this->model->get(Material::$TABLE_NAME,Material::$ID,$row[D_Good_Receipt::$ID_MATERIAL])->row_array();
						$kal .= "<td class='text-center'>" . $material[Material::$PRODUCT_CODE] . "</td>";
						
						$dus = $row[D_Good_Receipt::$DUS];
						$tin = $row[D_Good_Receipt::$TIN];
						/*if($row[Material::$BOX]>1){
							$dus+=(intdiv($row[D_Good_Receipt::$TIN],$row[Material::$BOX]));
							$tin%=$row[Material::$BOX];
						}*/
						
						$tampil_tin=($dus*$row[Material::$BOX])+$tin;
						//$kal .= "<td class='text-center'>" . $dus . "</td>";
						$kal .= "<td class='text-center'>" . $tampil_tin . "</td>";

					$kal .= "</tr>";
					
				}
			}
			$query = $this->model->good_issue_get_all_rekap_sj($tanggal_dari, $tanggal_sampai,$nomor_sj,$kode_barang,$kemasan,$warna);
			if ($query->num_rows() > 0) {
				foreach ($query->result_array() as $row) {
					$kal .= "<tr class='red-text'>";
					$kal .= "<td class='text-center'>" . date('Y-m-d', strtotime($row[H_Good_Issue::$TANGGAL])) . "</td>";	
					if(isset($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]))
						$kal .= "<td class='text-center'>" . date('Y-m-d', strtotime($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN])) . "</td>";
					else
						$kal .= "<td class='text-center'> - </td>";

					$kal .= "<td class='text-center'>" . $row[H_Good_Issue::$ID] . "</td>";
					
					$karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row[H_Good_Issue::$NIK])->row_array();
					$kal .= "<td class='text-center' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row[H_Good_Issue::$NIK] . "</td>";
					$kal .= "<td class='text-center'>" . $row[H_Good_Issue::$NOMOR_SJ] . "</td>";
					$kal .= "<td class='text-center'>" . $row[D_Good_Issue::$ID_MATERIAL] . "</td>";

					$material = $this->model->get(Material::$TABLE_NAME, Material::$ID, $row[D_Good_Issue::$ID_MATERIAL])->row_array();
					$kal .= "<td class='text-center'>" . $material[Material::$PRODUCT_CODE] . "</td>";
					
					$dus = $row[D_Good_Issue::$DUS];
					$tin = $row[D_Good_Issue::$TIN];
					/*if ($row[Material::$BOX] > 1) {
						$dus += (intdiv($row[D_Good_Issue::$TIN], $row[Material::$BOX]));
						$tin %= $row[Material::$BOX];
					}*/
					
					$tampil_tin = ($dus * $row[Material::$BOX]) + $tin;
					$tampil_tin*=-1;
					//$kal .= "<td class='text-center'>" . $dus . "</td>";
					$kal .= "<td class='text-center'>" . $tampil_tin . "</td>";
					
					$kal .= "</tr>";
				}
			}
			$kal .= '	</tbody>
					</table>';
			$kal .= "</div>";
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
			echo $kal;
		}
	}
	public function material_fg_sap($params){
		if($this->equals($params,MATERIAL_FG_SAP_UPLOAD)){
			$this->upload_data(Material_FG_SAP::$TABLE_NAME);
		}
	}

	//gudang
	public function gudang($params){
		if($this->equals($params,GUDANG_GET_ALL)){
			$data=$this->model->get_gudang_distinct("nama_gudang");
			$temp=[];
			$ctr=0;
			foreach($data->result() as $row){
				$temp[$ctr]["description"]=$row->nama_gudang;
				$ctr++;
			}
			echo json_encode($temp);
		}
	}
	
	//get or set directly from or to db function	
	/*public function get_role(){
		$role=$this->karyawan_model->get_by(Karyawan::$ID,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array()[Role::$ID];
		
		echo $role;
	}
	public function get_karyawan_by_nik(){
		$nik=$this->i_p("n");
		$num_rows=$this->karyawan_model->get_by(Karyawan::$ID,$nik)->num_rows();
		$temp=[];
		$temp['num_rows']=$num_rows;
		if($num_rows>0){
			$data=$this->karyawan_model->get_by(Karyawan::$ID,$nik)->row();
			$temp['nik']=$data->nik;
			$temp['nama']=$data->k_nama;
			$temp['r_id']=$data->r_id;
			$temp['gaji_pokok']=$data->gaji_pokok;
			$temp['npwp']=$data->npwp;
			$temp['tunjangan_jabatan']=$data->tunjangan_jabatan;
			$temp['kat_id']=$data->kat_id;
			$temp['s_cuti']=$data->saldo_cuti;
			$temp['is_outsource']=$data->is_outsource;
			$temp['subkat_id']=$data->subkat_id;
		}
		echo json_encode($temp);
	}*/


	public function get_standar_jam_kerja($ni,$tanggal){
		$data=[];
		$ctr=0;
		$qry=$this->karyawan_model->get_by(Karyawan::$ID,$ni)->row();
		$where="karyawan.nik=$ni";
		$where.=" and tanggal='$tanggal'";
		$absensi=$this->model->get_absensi_where("$where")->row();
		$temp=$this->model->get_str_jam_kerja($qry->id_kategori,$qry->id_sub_kategori);
		foreach($temp->result() as $row){
			$data[$ctr]['jam_masuk']=$row->jam_masuk;
			$data[$ctr]['jam_keluar']=$row->jam_keluar;
			$ctr++;
		}
		
		
		$j_masuk=new DateTime($absensi->jam_masuk);
		$idx=-1;
		$min=99999;
		for($i=0;$i<$ctr;$i++){
			$temp_j_masuk=new DateTime($data[$i]['jam_masuk']);
			$interval = $j_masuk->diff($temp_j_masuk);
			$minutes=(intval($interval->format("%H")*60)+intval($interval->format("%i")));
			if($minutes<=$min){
				$min=$minutes;
				$idx=$i;
			}
		}
		$result=[];
		$result['jam_masuk']=$data[$idx]["jam_masuk"];
		$result['jam_keluar']=$data[$idx]["jam_keluar"];
		return $result;
		
	}
	public function static_update_s_insentif(){
		$this::$UANG_HADIR=$this->model->get(Standar_Insentif::$TABLE_NAME,Standar_Insentif::$NAMA,S_INSENTIF_T_KEHADIRAN)->row_array()[Standar_Insentif::$NOMINAL];
		$this::$UANG_LUAR_KOTA=$this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_LUAR_KOTA)->row_array()[Standar_Insentif::$NOMINAL];
	}
	public function update_karyawan_insentif(){
		$tipe_dd=intval($this->i_p("td")); //NIK
		$tahun=intval($this->i_p("ta"));
		$bulan=intval($this->i_p("bu"));
		$tunjangan=$this->i_p("tu");
		$nama=$this->i_p("na");
		$ctr=$this->i_p("c");
		$tahun=ARR_TAHUN[$tahun];
		$bulan=strtoupper(ARR_BULAN_NO_SEMUA[$bulan]);
		$keterangan=$bulan." ".$tahun;
		$data=[];
		for($i=0;$i<$ctr;$i++){
			$data[$i]=str_replace("_"," ",$nama[$i])." : ".MESSAGE[$this->model->update_karyawan_insentif($tipe_dd,$nama[$i],$keterangan,$tunjangan[$i])];
		}
		echo json_encode($data);

	}
	/*public function get_all_user_access(){
		$qry=$this->model->get_all_user_access();
		
		if($qry['status']==1){
			$kal="";
			$kal.="<div class='row'>";
				$kal.="<div class='col-sm-2'></div>";
				$kal.="<div class='col-sm-8'>";
					$kal.='<table id="mastertable" class="table table-bordered table-sm">
							<thead>
								<tr class="table-primary">
									<th scope="col" class="text-center">NIK</th>
									<th scope="col" class="text-center">Nama</th>
									<th scope="col" class="text-center">Akses</th>
									<th scope="col" class="text-center">Deskripsi</th>
									<th scope="col" class="text-center">Status</th>
									<th scope="col" class="text-center">Aksi</th>';
					$kal.='		</tr>
							</thead>
							<tbody>';
						foreach($qry['data']->result() as $row){
							$kal.="<tr>";
								$kal.="<td>".$row->nik."</td>"; 
								$kal.="<td>".$row->nama."</td>"; 
								$kal.="<td>".$row->access."</td>";
								$kal.="<td>".$row->description."</td>";
								if($row->is_true==1){
									$kal.="<td class='green-text'>DIBERIKAN</td>";
									$kal.="<td><button type='button' class='center btn btn-outline-danger' data-mdb-ripple-color='dark' onclick='update_role_access(".$row->nik.",".$row->id_access.",".($row->is_true*-1+1).")'>TOLAK</button></td>";
								}else{
									$kal.="<td class='red-text'>TIDAK DIBERIKAN</td>";
									$kal.="<td><button type='button' class='center btn btn-outline-success' data-mdb-ripple-color='dark' onclick='update_role_access(".$row->nik.",".$row->id_access.",".($row->is_true*-1+1).")'>IJINKAN</button></td>";
								}
							$kal.="</tr>";
							
						}
						$kal.="</tbody>
						</table>";
				$kal.="</div>";
				$kal.="<div class='col-sm-2'></div>";
			$kal.="</div>";

			echo $kal;
		}else{
			echo MESSAGE[$qry['data']];
		}
	}*/
	public function get_kinerja_by($tanggal_dari, $tanggal_sampai, $nik = NULL, $code = NULL){
		$data = $this->model->get_kinerja_by($tanggal_dari, $tanggal_sampai, $nik, $code);
		return $data;
	}

	//helper function - Done
	public function i_p($value){
		return $this->input->post($value);
	}
	public function rupiah($angka,$with_decimal = 0){
		return number_format($angka,$with_decimal,'.',',');
	}
	public function decimal($angka,$params=null){
		if($params!=null)
			return number_format($angka, $params, '.', ',');
			
		return number_format($angka,2,'.',',');
	}
	public function capitalize($words){
		return ucwords(strtolower($words));
	}
	public function equals($string1,$string2){
		if(strcmp(strtoupper($string1),strtoupper($string2))!==0)
			return false;
		return true;
	}
	public function str_contains($haystack,$needle){
		if(strpos(strtoupper($haystack),strtoupper($needle))!==FALSE)
			return true;
		return false;
	}
	public function get_index_from_array($array,$key){
		for($i=0;$i<count($array);$i++){
			if($this->equals($array[$i],$key))
				return $i;
		}
		return -1;
	}
	public function contains_number($string){
		for($i=0;$i<count($string);$i++){
			if(is_numeric($string[$i]))
				return true;
		}
		return false;
	}


	//check function
	public function get_message($data){
		if($this->str_contains($data,$this::$MESSAGE_UPLOAD_ERROR_FILE_NOT_SELECTED)){
			return "File belum dipilih, silahkan cek lagi!";
		}else if($this->str_contains($data,$this::$MESSAGE_UPLOAD_ERROR_NOT_ALLOWED_FILETYPE)){
			return "File yang diupload bukan dalam format xlsx!";
		}
		return $data;
	}
	public function is_weekends($tanggal){
		$cek_weekends=strtoupper(date("l",strtotime($tanggal)));
		if($this->equals($cek_weekends,"SATURDAY")||$this->equals($cek_weekends,"SUNDAY"))
			return true;
		return false;
	}
	public function is_holiday($tanggal){
		//Get Hari Libur
		$arr_holiday=array();
		$data_holiday=$this->model->get_holiday_date();
		foreach($data_holiday->result() as $row){
			array_push($arr_holiday,$row->tanggal);
		}
		if(in_array($tanggal,$arr_holiday) || $this->is_weekends($tanggal))
			return true;
		return false;
	}

	//calculate function
	public function calculate_standar_jam_kerja($bulan,$tahun){
		$waktu=0;
		if($this->equals($bulan,"SEMUA")){
			for($i=1;$i<=count(ARR_BULAN_NO_SEMUA);$i++){
				$time=strtotime($i."-".$tahun);
				$date=new DateTime($time);
				$lastday=$date->format('t');
				for($j=1;$j<=$lastday;$j++){
					$time=$tahun."-".$i."-".$j;
					if(!$this->is_holiday(date('Y-m-d',strtotime($time)))){
						$waktu+=510;
					}
				}
			}
		}else{
			$bln=$this->get_index_from_array(ARR_BULAN,$bulan);
			$time=$tahun."-".$bln."-01";
			$date=new DateTime($time);
			$lastday=$date->format('t');
			for($i=1;$i<=$lastday;$i++){
				$time=$tahun."-".$bln."-".$i;
				if(!$this->is_holiday(date('Y-m-d',strtotime($time)))){
					$waktu+=510;
				}
			}

		}
		return $waktu;

	}
	public function calculate_lembur_minutes($nik,$where){
		$lembur=0;
		$data=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_LEMBUR."%'");
		foreach($data->result() as $row){
			$jam_masuk=new DateTime($row->jam_masuk);
			$jam_keluar=new DateTime($row->jam_keluar);
			$interval = $jam_masuk->diff($jam_keluar);
							
			$minutes=(intval($interval->format("%H")*60)+intval($interval->format("%i")));

			if(!$this->is_holiday($row->tanggal))
				$minutes-=480;
				
			$minutes-=30;
			/*if($nik==NIK_PAK_MAD){
				if($minutes>=120)
					$minutes=120;
			}*/
			if($minutes<=0)
				$minutes=0;
			$lembur+=$minutes;
		}
		return $lembur;	
	}
	public function calculate_performance_kehadiran($minutes,$standar){
		$performance=($minutes*100)/$standar;
		return round($performance,2);
	}
	public function calculate_work_minutes($ni,$j_masuk,$j_keluar,$tanggal,$keterangan){
		$minutes=0;
		if($this->str_contains($keterangan,LEGEND_LUAR_KOTA)||$this->str_contains($keterangan,LEGEND_CUTI)){
			$minutes+=510;
		}
		else if($j_masuk===NULL||$j_keluar===NULL){
			$minutes=0;
		}
		else if($j_masuk!==NULL){
			$data=$this->get_standar_jam_kerja($ni,$tanggal);
			$jam_masuk=new DateTime($j_masuk);
			$str_jam_masuk=new DateTime($data["jam_masuk"]);
			$jam_keluar=new DateTime($j_keluar);
			$str_jam_keluar=new DateTime($data["jam_keluar"]);
			//echo $jam_masuk->format("H:i")."-".$jam_keluar->format("H:i")."<br>";
			
			if(!$this->is_holiday($tanggal)||$this->equals($keterangan,LEGEND_TUKAR_HARI)){
				if($jam_keluar>$str_jam_keluar)
					$jam_keluar=$str_jam_keluar;
				if($jam_masuk<$str_jam_masuk)
					$jam_masuk=$str_jam_masuk;
			}
			//walaupun libur atau tidak libur, jika lembur jam kerja 0
			
			if($this->str_contains($keterangan,LEGEND_LEMBUR)){
				$jam_keluar=$str_jam_keluar;
				$jam_masuk=$str_jam_keluar;
			}

			$interval = $jam_masuk->diff($jam_keluar);
			
			$minutes=(intval($interval->format("%H")*60)+intval($interval->format("%i")));
		}
		return $minutes;
	}
	public function calculate_lembur_rupiah($nik,$bulan,$tahun){
		$keterangan=ARR_BULAN_NO_SEMUA[$bulan]." ".ARR_TAHUN[$tahun];
		$cutoff=$this->model->get_cutoff_by_keterangan($keterangan)->row();
		$karyawan=$this->karyawaN_model->get_by(Karyawan::$ID,$nik)->row();
		if(isset($cutoff)){
			//Perhitungan Gaji Lembur
			$lembur=0;
			$arr_lbr_karyawan[$nik]=0;
			
			$where="karyawan.nik=$nik";
			$where.=" and tanggal<='".$cutoff->tanggal_sampai."'";
			$where.=" and tanggal>='".$cutoff->tanggal_dari."'";
			$where.=" and upper(keterangan) like '%".LEGEND_LEMBUR."%'";
			$data_absensi=$this->model->get_absensi_where("$where");
			foreach($data_absensi->result() as $row2){
				if($row2->jam_masuk!==NULL){
					$interval=0;
					$minutes=0;
					$jam_masuk=new DateTime($row2->jam_masuk);
					$jam_keluar=new DateTime($row2->jam_keluar);
					if($this->str_contains($row2->keterangan,LEGEND_LEMBUR)){
						$interval = $jam_masuk->diff($jam_keluar);
						$minutes = intval($interval->format("%H"))*60+intval($interval->format("%i"));
						
					}
					
					if(!$this->is_holiday($row2->tanggal))
						$minutes-=480;
						
					$minutes-=30;

					/*if($nik==NIK_PAK_MAD){
						if($minutes>=120)
							$minutes=120;
					}*/
					
					$hour=$minutes/60;
					if(!$this->is_holiday($row2->tanggal)){
						if($hour<=JAM_PERTAMA_LEMBUR_NOT_HOLIDAY){
							$lembur+=($hour*($karyawan->gaji_pokok*3/2/173));
						}else{
							$lembur+=($hour*($karyawan->gaji_pokok*3/2/173));
							$lembur+=(($hour-JAM_PERTAMA_LEMBUR_NOT_HOLIDAY)*($karyawan->gaji_pokok*2/173));
						}
					}else {
						if($hour<=JAM_PERTAMA_LEMBUR_HOLIDAY){
							$lembur+=($hour*($karyawan->gaji_pokok*2/173));
							
						}else{
							$lembur+=($hour*($karyawan->gaji_pokok*2/173));
							$lembur+=(($hour-JAM_PERTAMA_LEMBUR_HOLIDAY)*($karyawan->gaji_pokok*3/173));
						}
					}
					
				}
			}
			return round($lembur,2);
		}
		return 0;
	}
	public function calculate_alpha_rupiah($nik,$ctr_alpha){
		$karyawan=$this->karyawan_model->get_by(Karyawan::$ID,$nik)->row();
		
		return round($karyawan->gaji_pokok*8*$ctr_alpha/173,2);
	}
	public function calculate_insentif_kehadiran_rupiah($nik,$bulan,$tahun){
		$insentif_kehadiran=0;
		$nik=$nik;
		$where="karyawan.nik=$nik";
		$keterangan=ARR_BULAN_NO_SEMUA[$bulan]." ".ARR_TAHUN[$tahun];
		$cutoff=$this->model->get_cutoff_by_keterangan($keterangan)->row_array();
		$where.=" and tanggal<='".$cutoff[Cutoff::$TANGGAL_DARI]."'";
		$where.=" and tanggal>='".$cutoff[Cutoff::$TANGGAL_SAMPAI]."'";
		$alpha=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_ALPHA."%'")->num_rows();
		$sakit=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_SAKIT."%'")->num_rows();
		$cuti=$this->model->get_absensi_where("$where and upper(keterangan) like '%".LEGEND_CUTI."%'")->num_rows();

		//tunjangan kehadiran
		$hadir=0;
		if($alpha==0 && $sakit==0){
			$is_i_kehadiran=$this->model->available_karyawan_insentif($nik,S_INSENTIF_T_KEHADIRAN);
			if($is_i_kehadiran)
				$hadir+=$this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_T_KEHADIRAN)->row_array()[Standar_Insentif::$NOMINAL];
		}

		//lembur
		$lembur=$this->calculate_lembur_rupiah($nik,$bulan,$tahun);

		//alpha
		$temp_alpha=0;
		if($alpha>0){
			$temp_alpha=$this->calculate_alpha_rupiah($nik,$alpha);
		}

		$insentif_kehadiran+=$hadir+$lembur-$temp_alpha;
		return $insentif_kehadiran;
	}

	//postman helper function
	public function insert_karyawan_insentif(){
		$nik=$this->i_p("nik");
		$this->model->insert_karyawan_insentif($nik);
	}
	public function insert_karyawan_obat(){
		$nik=$this->i_p("nik");
		$this->model->insert_karyawan_obat($nik);
	}
	public function insert_karyawan_insentif_1_year(){
		//$tipe=$this->i_p("id_tunjangan");
		$this->model->insert_karyawan_insentif_1_year(4);
	}
	public function insert_rak(){
		$this->model->insert_rak();
	}
}
