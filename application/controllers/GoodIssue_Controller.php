<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class GoodIssue_Controller extends Library_Controller {

    public function get_view_by_sj(){
        $nomor_sj = $this->i_p("i");
        $query = $this->model->get_good_issue_by_sj($nomor_sj);
        $data = [];
        $data["num_rows"] = $query->num_rows();
        if ($query->num_rows() > 0) {
            $rows = $query->row_array();
            $data[H_Good_Issue::$STO] = $rows[H_Good_Issue::$STO];
            $tanggal = date('d F Y H:i', strtotime($rows[H_Good_Issue::$TANGGAL]));
            $data[H_Good_Issue::$EKSPEDISI] = $rows[H_Good_Issue::$EKSPEDISI];
            $ctr = 0;
            $ctr_tampil = 0;
            $arr_tampil = [];
            $kal = '
				<thead>
					<tr>
						<td class="text-center font-sm f-aleo">Nomor</td>
						<td class="text-center font-sm f-aleo">Kode Barang</td>
						<td class="text-center font-sm f-aleo">Warna</td>
						<td class="text-center font-sm f-aleo">Kemasan</td>
						<td class="text-center font-sm f-aleo">Tin</td>
						<td class="text-center font-sm f-aleo">Dus</td>
						<td class="text-center font-sm f-aleo">Gudang</td>
						<td class="text-center font-sm f-aleo">Barang</td>
					</tr>
				</thead><tbody>';
            foreach ($query->result_array() as $row) {
                $idx = -1;
                for ($i = 0; $i < $ctr_tampil; $i++) {
                    if ($this->equals($row[H_Good_Issue::$ID], $arr_tampil[$i][H_Good_Issue::$ID]))
                        $idx = $i;
                }
                if ($idx == -1) {
                    $arr_tampil[$ctr_tampil][H_Good_Issue::$ID] = $row[H_Good_Issue::$ID];
                    $karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK])->row_array();
                    if ($row[H_Good_Issue::$KODE_REVISI] == null)
                        $arr_tampil[$ctr_tampil][Karyawan::$NAMA] = $karyawan["k_nama"];
                    else
                        $arr_tampil[$ctr_tampil][Karyawan::$NAMA] = $karyawan["k_nama"] . "(R)";
                    $ctr_tampil++;
                }
                $material = $this->model->get_material($row[Material::$KODE_BARANG], $row[Material::$KEMASAN], $row[Material::$WARNA])->row_array();
                $kal .= '
						<tr class="tr_detail_product_' . $ctr . '">
							<td style="width:1%" class="text-center font-sm f-aleo">' . ($ctr + 1) . '</td>
							<td style="width:10%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$KODE_BARANG] . '
							</td>
						
							<td style="width:10%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$WARNA] . '
							</td>

							<td style="width:5%" class="text-center font-sm f-aleo"> 
								' . $row[Material::$KEMASAN] . '
							</td>
						
							<td style="width:5%" class="text-right font-sm f-aleo"> 
								' . $row[D_Good_Issue::$TIN] . '
							</td>
							
							<td style="width:5%" class="text-right font-sm f-aleo"> 
								' . $row[D_Good_Issue::$DUS] . '
							</td>

							<td class="text-center font-sm f-aleo">';
                $temp = $this->model->get(Gudang::$TABLE_NAME, Gudang::$ID, $row[D_Good_Issue::$ID_GUDANG]);
                foreach ($temp->result_array() as $row2) {
                    if ($this->equals($row[D_Good_Issue::$ID_GUDANG], $row2[Gudang::$ID]))
                        $kal .= $row2[Gudang::$NAMA_GUDANG] . ' - RAK ' . $row2[Gudang::$RAK] . $row2[Gudang::$KOLOM] . ' ' . $row2[Gudang::$TINGKAT];
                }
                $kal .= '
							</td>

							<td class="text-center font-sm f-aleo"> 
								' . $material[Material::$DESCRIPTION] . '
							</td>
						</tr>';
                $ctr++;
            }
            $kal .= "</tbody>";

            $kode_gi = "";
            $nama = "";
            for ($i = 0; $i < $ctr_tampil; $i++) {
                $kode_gi .= "| " . $arr_tampil[$i][H_Good_Issue::$ID] . " |";
                $nama  .= "| " . $arr_tampil[$i][Karyawan::$NAMA] . " |";
            }
            $data["html"] = 'Kode GI : ' . $kode_gi . '<br> Dibuat oleh : ' . $nama . '<br>Tanggal: ' . $tanggal;
            $data["ctr"] = $ctr;
            $data["kode"] = $nomor_sj; //Surat Jalan
            $data["detail_product"] = $kal;
        }
        echo json_encode($data);
    }

    public function insert_gi(){
        $this->insert($this::$NAV_GOOD_ISSUE);
    }
    public function insert_adjustment(){
        $this->insert($this::$NAV_GOOD_ISSUE_ADJUSTMENT);
    }
}
