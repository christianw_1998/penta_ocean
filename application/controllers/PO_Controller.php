<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');

class PO_Controller extends Library_Controller{

    public function upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_purchase_order";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->po_model->update_batch($data);
    }
    public function insert_po(){
        $this->insert(Purchase_Order::$TABLE_NAME);
    }
    public function checker_insert(){
        $this->insert(Purchase_Order_Checker_Header::$TABLE_NAME);
    }
    public function revisi(){
        $idm = $this->i_p("am");
        $ids = $this->i_p("ad");
        $idv = $this->i_p("av");
        $iqy = $this->i_p("aq");
        $inp = $this->i_p("an");
        $inu = $this->i_p("au");
        $np = $this->i_p("np");
        $c = $this->i_p("c");
        $d = $this->i_p("d");
        $p = $this->i_p("p");

        echo MESSAGE[$this->po_model->revisi($d, $p, $np, $c, $inu, $idm, $ids, $idv, $iqy, $inp)];
    }
    public function cancel(){
        $nomor_gr = $this->i_p("n");
        echo MESSAGE[$this->po_model->cancel($nomor_gr)];
    }
    public function change_nomor_sj(){
		$old_sj = $this->i_p("os");
		$new_sj = $this->i_p("ns");
		echo MESSAGE[$this->po_model->change_no_sj($old_sj, $new_sj)];
	}
    public function change_status(){
        $nomor_po=$this->i_p("n");
        $is_deleted=$this->i_p("i");

        echo MESSAGE[$this->po_model->change_status($nomor_po,$is_deleted)];
    }

    public function get_tabel_create(){
        $kode_pr = $this->i_p("k");
        $temp = [];
        $temp["message"] = "";

        //Vendor Datalist
        $data_list = "<datalist id='list_vendor'>";
        $data_vendor = $this->vendor_model->get(Vendor::$TABLE_NAME);
        if ($data_vendor->num_rows() > 0) {
            foreach ($data_vendor->result_array() as $row_vendor) {
                $data_list .= "<option value=" . $row_vendor[Vendor::$ID] . ">" . $row_vendor[Vendor::$NAMA] . "</option>";
            }
        }
        $data_list .= "</datalist>";
        $kal = $data_list;

        //Payterm Datalist
        $data_list = "<datalist id='list_payterm'>";
        $data_vendor = $this->payterm_model->get(Payterm::$TABLE_NAME);
        if ($data_vendor->num_rows() > 0) {
            foreach ($data_vendor->result_array() as $row_payterm) {
                $data_list .= "<option value=" . $row_payterm[Payterm::$ID] . ">" . $row_payterm[Payterm::$DESKRIPSI] . "</option>";
            }
        }
        $data_list .= "</datalist>";
        $kal .= $data_list;

        $kal .= '<div class="d-flex justify-content-center">
                    <table>
                        <tr>
                            <td class="text-right fw-bold">Tanggal PO</td>
                            <td>:</td>
                            <td><input type="date" onchange="update_tanggal()" id="txt_tanggal" class="black-text"></td>
                        </tr>
                        <tr>
                            <td class="text-right fw-bold">Tanggal Kirim</td>
                            <td>:</td>
                            <td><input type="date" id="txt_tanggal_kirim" class="black-text"></td>
                        </tr>
                        <tr>
                            <td class="text-right fw-bold">Kode Payterm</td>
                            <td>:</td>
                            <td><input type="text" list="list_payterm" id="txt_kode_payterm" style="width:100%" onkeyup="get_payterm_by_id(this.value)" class="black-text"></td>
                        </tr>
                        <tr>
                            <td class="text-right fw-bold">Deskripsi</td>
                            <td>:</td>
                            <td><input type="text" id="txt_desc_payterm" style="width:200%" value="Input 6 digit kode Payterm" disabled class="red-text pt"></td>
                        </tr>
                        <tr>
                            <td class="text-right fw-bold">ID Vendor</td>
                            <td>:</td>
                            <td><input type="text" id="txt_id_vendor" list="list_vendor" style="width:100%" onkeyup="get_vendor_by_id(this.value)" class="black-text"></td>
                        </tr>
                        <tr>
                            <td class="text-right fw-bold">Vendor</td>
                            <td>:</td>
                            <td><input type="text" id="txt_desc_vendor" style="width:200%" value="Input 6 digit kode Vendor" disabled class="red-text pt"></td>
                        </tr>
                    </table>
                </div>
                <br>';

        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                    <thead>
                    <tr class="table-primary">
                        
                        <th scope="col" class="fw-bold align-middle text-center" style="width:10%">ID Material</th>
                        <th scope="col" class="fw-bold align-middle text-center" style="width:30%">Deskripsi</th>
                        <th scope="col" class="fw-bold align-middle text-center" style="width:10%">Qty</th>
                        <th scope="col" class="fw-bold align-middle text-center" style="width:10%">Net Price (Rupiah)</th>
                        <th scope="col" class="fw-bold align-middle text-center" style="width:20%">Total (Rupiah)</th>
                    </tr>

                    </thead><tbody>';
        /*<th scope="col" class="fw-bold align-middle text-center" style="width:10%">ID Vendor</th>
                        <th scope="col" class="fw-bold align-middle text-center" style="width:10%">Vendor</th>*/
        $data = $this->po_model->get_tabel_create($kode_pr);

        if ($data->num_rows() > 0) {
            $is_done = $data->row_array()[Purchase_Request_Header::$IS_DONE];
            $is_deleted = $data->row_array()[Purchase_Request_Header::$IS_DELETED];
            if ($is_deleted == 1) {
                $temp["message"] = MESSAGE[Purchase_Request::$MESSAGE_FAILED_SEARCH_IS_DELETED];
            } else if ($is_done == 1) {
                $temp["message"] = MESSAGE[Purchase_Request::$MESSAGE_FAILED_SEARCH_PO_ALREADY_MADE];
            } else if ($is_done == 0) {
                $ctr = 0;
                foreach ($data->result_array() as $row) {
                    $kal .= "<tr>";
                    $hidden = "<input type='hidden' id='txt_idm_" . $ctr . "' value='" . $row[Material_RM_PM::$ID] . "'>
                                <input type='hidden' id='txt_total_" . $ctr . "' value=0>";


                    /*$kal .= "<td class='align-middle text-center'>$hidden<input style='width:100%' class='text-center' onkeyup='get_vendor_by_id($ctr)' id='txt_idv_" . $ctr . "' type='text' value=" . $row[Purchase_Request_Detail::$ID_VENDOR]  . "></td>";

                    $vendor = "";
                    $qry_vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row[Purchase_Request_Detail::$ID_VENDOR]);
                    if ($qry_vendor->num_rows() > 0) {
                        $vendor = $qry_vendor->row_array()[Vendor::$NAMA];
                        $kal .= "<td class='align-middle text-left green-text'><span id='txt_desc_vendor_" . $ctr . "'>" . $vendor . "</span></td>";
                    } else {
                        $kal .= "<td class='align-middle text-left red-text'><span id='txt_desc_vendor_" . $ctr . "'>Vendor tidak ditemukan</span></td>";
                    }*/

                    $kal .= "<td class='align-middle text-center'>$hidden" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";
                    $kal .= "<td class='align-middle text-center'><input type='text' style='width:100%' id='txt_desc_" . $ctr . "' class='text-left' value='" . $row[Material_RM_PM::$DESCRIPTION] . "'></td>";

                    $qty = 0;
                    if ($row[Purchase_Request_Detail::$QTY_PR] > 0)
                        $qty = round($row[Purchase_Request_Detail::$QTY_PR]);

                    $kal .= "<td class='align-middle text-center'><input style='width:100%' class='text-right' min=0 onkeyup='get_active_price($ctr)' id='txt_qty_po_" . $ctr . "' type='number' placeholder=" . $qty . "></td>";


                    if ($row[Purchase_Request_Detail::$ID_VENDOR] != null) {
                        $price = 0;
                        $qry_price = $this->po_model->price_get_active($row[Purchase_Request_Detail::$ID_VENDOR], $row[Material_RM_PM::$ID]);

                        if ($qry_price->num_rows() > 0) {
                            $qry_price = $qry_price->row_array();
                            if ($this->equals($qry_price[Purchase_Order_Price_List::$CURRENCY], Purchase_Order_Price_List::$S_CURRENCY_USD)) {
                                //Kalau masih USD kalikan dengan kurs
                                $kurs = 0;
                                $qry_kurs = $this->kurs_model->get_last();
                                if ($qry_kurs->num_rows() > 0) {
                                    $kurs = $qry_kurs->row_array()[Kurs::$VALUE];
                                    $price = $qry_price[Purchase_Order_Price_List::$PRICE] * $kurs;
                                }
                            } else {
                                $price = $qry_price[Purchase_Order_Price_List::$PRICE];
                            }
                            $kal .= "<td class='align-middle text-right'><input type='hidden' id='txt_netprice_" . $ctr . "'><span style='width:100%' class='text-right' id='txt_tampil_netprice_" . $ctr . "' placeholder=''></span></td>";
                        } else
                            $kal .= "<td class='align-middle text-right'><input type='hidden' id='txt_netprice_" . $ctr . "'><span style='width:100%' class='text-right' id='txt_tampil_netprice_" . $ctr . "' placeholder=''></span></td>";

                    } else
                        $kal .= "<td class='align-middle text-right'><input type='hidden' id='txt_netprice_" . $ctr . "'><span style='width:100%' class='text-right' id='txt_tampil_netprice_" . $ctr . "' placeholder=''></span></td>";

                    $kal .= "<td class='align-middle text-right'><span class='red-text text-right' id='txt_tampil_total_" . $ctr . "'>Input Qty terlebih dahulu</span></td>";


                    $kal .= "</tr>";
                    $ctr++;
                }
            }
            
            $kal .= '<tr>';
            $kal .= '<td colspan=4 class="text-right fw-bold">TOTAL</td>';
            $kal .= '<td class="text-right"><span id="txt_tampil_total" class="text-right red-text">Pastikan tidak ada total yang salah</span></td>';
            $kal .= '</tr>';
            $kal .= '<tr>';
            $kal .= '<td colspan=4 class="align-middle text-right fw-bold">Diskon (Rupiah)</td>';
            $kal .= '<td class="text-right">Rp. <input type"number" id="txt_diskon" class="text-right" min=0 max=100 onkeyup="calculate_total_with_ppn()"></td>';
            $kal .= '</tr>';
            $kal .= '<tr>';
            $kal .= '<td colspan=4 class="align-middle text-right fw-bold">PPN (%)</td>';
            $kal .= '<td class="text-right"><input type"number" id="txt_ppn" style="width:15%" class="text-right" min=0 max=100 onkeyup="calculate_total_with_ppn()"> %</td>';
            $kal .= '</tr>';
            $kal .= '<tr>';
            $kal .= '<td colspan=4 class="text-right fw-bold">TOTAL (INC. PPN)</td>';
            $kal .= '<td class="text-right"><span id="txt_tampil_total_ppn" class="text-right red-text">Input Diskon(%) terlebih dahulu</span></td>';
            $kal .= '</tr>';
            $kal .= '	</tbody>
                    </table>';
            $kal .= "</div>";
            $kal .= "</div>";
            $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12 text-center">
                        <button type="button" class="center text-center btn btn-outline-success" onclick="generate_po()" data-mdb-ripple-color="dark">
                            CREATE
                        </button>
                    </div>';
            $kal .= "</div>";
        } else {
            $temp["message"] = MESSAGE[Purchase_Request::$MESSAGE_FAILED_SEARCH_NOT_FOUND];
        }
        $temp["kal"] = $kal;
        echo json_encode($temp);
    }
    public function get_last_date(){
        $tanggal = $this->po_model->get_last_date();
        echo $tanggal;
    }
    public function get_view_by_no(){
        $data = [];
        $table_detail = "";
        $data['detail_product'] = '
						<thead>
							<tr>
								<td class="text-center font-sm f-aleo">Nomor</td>
								<td class="text-center font-sm f-aleo">Material</td>
								<td class="text-center font-sm f-aleo">Deskripsi</td>
								<td class="text-center font-sm f-aleo">Qty PO</td>
								<td class="text-center font-sm f-aleo">Qty Surat Jalan</td>
								<td class="text-center font-sm f-aleo">Qty Diterima</td>
							</tr>
						</thead><tbody>';

        $po = $this->i_p("n");
        $qry = $this->po_model->get_by_no($po);
        $data['num_rows'] = $qry[Purchase_Order::$TABLE_NAME]->num_rows();
        $ctr = 0;

        if ($qry[Purchase_Order_Awal::$TABLE_NAME]->num_rows() > 0) {
            $data[Purchase_Order_Awal::$NOMOR_PO] = "PO Nomor " . $qry[Purchase_Order_Awal::$TABLE_NAME]->row_array()[Purchase_Order_Awal::$NOMOR_PO] . "<br>
														 Tanggal : " . date('d M Y', strtotime($qry[Purchase_Order::$TABLE_NAME]->row_array()[Purchase_Order::$TANGGAL]));
            $data[Purchase_Order::$TANGGAL]=$qry[Purchase_Order::$TABLE_NAME]->row_array()[Purchase_Order::$TANGGAL];
            foreach ($qry[Purchase_Order_Awal::$TABLE_NAME]->result_array() as $row_po_awal) {
                $material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_po_awal[Purchase_Order_Awal::$ID_MATERIAL])->row_array();
                $qty = $this->po_model->get_last_qty(
                    $row_po_awal[Purchase_Order::$ID],
                    $row_po_awal[Purchase_Order_Awal::$NOMOR_URUT],
                    $row_po_awal[Purchase_Order_Awal::$QTY]
                );
                if ($qty > 0) {
                    $table_detail .= "<tr>";
                    $table_detail .= '<td class="text-center font-sm f-aleo">' . $row_po_awal[Purchase_Order_Awal::$NOMOR_URUT] . '<input type="hidden" id="txt_no_urut_' . $ctr . '" value=' . $row_po_awal[Purchase_Order_Awal::$NOMOR_URUT]  . ' /></td>';
                    $table_detail .= '<td class="text-center font-sm f-aleo">' . $material[Material_RM_PM::$KODE_PANGGIL] . '<input type="hidden" id="txt_id_material_' . $ctr . '" value="' . $material[Material_RM_PM::$ID] . '" /></td>';
                    $table_detail .= '<td class="text-left font-sm f-aleo">' . $row_po_awal[Purchase_Order_Awal::$SHORT_TEXT] . '<input type="hidden" id="txt_id_material_' . $ctr . '" value="' . $material[Material_RM_PM::$ID] . '" /></td>';
                    $table_detail .= '<td class="text-right font-sm f-aleo">' . $this->decimal($qty, 3) . ' ' . $material[Material_RM_PM::$UNIT] . '<input type="hidden" id="txt_qty_' . $ctr . '" value=' . $qty . ' /></td>';
                    $table_detail .= '<td class="text-center font-sm f-aleo"><input type="number" class="text-right" placeholder=0 onkeyup="check_qty_sj(' . $ctr . ')" id="txt_qty_sj_' . $ctr . '"></input></td>';
                    $table_detail .= '<td class="text-center font-sm f-aleo"><input type="number" class="text-right" placeholder=0 onkeyup="check_qty(' . $ctr . ')" id="txt_qty_terima_' . $ctr . '"></input></td>';
                    $table_detail .= "</tr>";
                    $ctr += 1;
                }
            }
        }
        $data['ctr'] = $ctr;
        $data['detail_product'] .= $table_detail;
        $data['detail_product'] .= "</tbody>";

        echo json_encode($data);
    }
    public function get_view_revisi_by_no(){
        $data = [];
        $table_detail = "";

        //Cek apakah sudah di GR atau Belum

        //Vendor Datalist
        $data_list = "<datalist id='list_vendor'>";
        $data_vendor = $this->vendor_model->get(Vendor::$TABLE_NAME);
        if ($data_vendor->num_rows() > 0) {
            foreach ($data_vendor->result_array() as $row_vendor) {
                $data_list .= "<option value=" . $row_vendor[Vendor::$ID] . ">" . $row_vendor[Vendor::$NAMA] . "</option>";
            }
        }
        $data_list .= "</datalist>";
        $data['detail_product'] = $data_list . '
						<thead>
							<tr>
								<th class="text-center font-sm f-aleo" style="width:1%">Nomor</th>
								<th class="text-center font-sm f-aleo" style="width:10%">Kode Material</th>
								<th class="text-center font-sm f-aleo" style="width:30%">Deskripsi</th>
								<th class="text-center font-sm f-aleo" style="width:10%">Qty</th>
								<th class="text-center font-sm f-aleo" style="width:10%">Net Price</th>
								<th class="text-center font-sm f-aleo" style="width:10%">Total</th>
							</tr>
						</thead><tbody>';
        $po = $this->i_p("n");
        $gr = $this->po_model->get(Purchase_Order_Checker_Header::$TABLE_NAME, Purchase_Order_Checker_Header::$NOMOR_PO, $po, null, null, false);
        $disabled = "";
        if ($gr->num_rows() > 0) {
            $disabled = "disabled";
        }
        $qry = $this->po_model->get_by_no($po);
        $data['num_rows'] = $qry[Purchase_Order::$TABLE_NAME]->num_rows();
        $ctr = 0;
        $total = 0;
        if ($qry[Purchase_Order_Awal::$TABLE_NAME]->num_rows() > 0) {
            $data_po = $qry[Purchase_Order::$TABLE_NAME]->row_array();
            $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $qry[Purchase_Order_Awal::$TABLE_NAME]->row_array()[Purchase_Order_Awal::$ID_VENDOR],null,null,false)->row_array();
            $data[Purchase_Order_Awal::$NOMOR_PO] = '<table>
                                                        <tr>
                                                            <td class="text-right fw-bold">Nomor PO</td>
                                                            <td>:</td>
                                                            <td class="text-left">' . $qry[Purchase_Order_Awal::$TABLE_NAME]->row_array()[Purchase_Order_Awal::$NOMOR_PO] . '</td>
                                                        </tr>
                                                        <tr>
                                                        <tr>
                                                            <td class="text-right fw-bold">Tanggal PO</td>
                                                            <td>:</td>
                                                            <td><input type="date" style="width:100%" disabled id="txt_tanggal" class="text-left black-text" value="'. $data_po[Purchase_Order::$TANGGAL] . '"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right fw-bold">ID Vendor</td>
                                                            <td>:</td>
                                                            <td><input disabled type="text" value="'. $vendor[Vendor::$ID] .'"id="txt_id_vendor" list="list_vendor" style="width:100%" onkeyup="get_vendor_by_id(this.value)" class="black-text"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right fw-bold">Vendor</td>
                                                            <td>:</td>
                                                            <td><input type="text" value="'. $vendor[Vendor::$NAMA] . '"id="txt_desc_vendor" style="width:200%" value="Input 6 digit kode Vendor" disabled class="green-text pt"></td>
                                                        </tr>
                                                    </table>
                                                    <button type="button" ' . $disabled . ' class="center text-center btn btn-outline-info" onclick="refresh_active_price()" data-mdb-ripple-color="dark">
                                                        Refresh Price List
                                                    </button>';
            foreach ($qry[Purchase_Order_Awal::$TABLE_NAME]->result_array() as $row_po_awal) {
                $material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_po_awal[Purchase_Order_Awal::$ID_MATERIAL])->row_array();
                $table_detail .= "<tr>";
                $hidden= "<input type='hidden' id='txt_total_" . $ctr . "' value=". ($row_po_awal[Purchase_Order_Awal::$QTY] * round($row_po_awal[Purchase_Order_Awal::$NET_PRICE])) ."><input type='hidden' id='txt_idm_" . $ctr . "' value='" . $row_po_awal[Material_RM_PM::$ID] . "'>";
                $table_detail .= $hidden. '<td class="text-center font-sm f-aleo">' . $row_po_awal[Purchase_Order_Awal::$NOMOR_URUT] . '<input type="hidden" id="txt_no_urut_' . $ctr . '" value=' . $row_po_awal[Purchase_Order_Awal::$NOMOR_URUT]  . ' /></td>';
                $table_detail .= '<td class="text-center font-sm f-aleo">' . $material[Material_RM_PM::$KODE_MATERIAL] . '<input type="hidden" id="txt_id_material_' . $ctr . '" value="' . $material[Material_RM_PM::$ID] . '" /></td>';
                $table_detail .= '<td class="text-center font-sm f-aleo"><input style="width:100%" '. $disabled .' type="text" value="' . $row_po_awal[Purchase_Order_Awal::$SHORT_TEXT] . '" id="txt_desc_' . $ctr . '"/></td>';
                
                $table_detail .= "<td class='align-middle text-center'><input style='width:100%' class='text-right' min=0 onkeyup='calculate_total($ctr)' id='txt_qty_po_" . $ctr . "' type='number' value='" . ($row_po_awal[Purchase_Order_Awal::$QTY])  . "' placeholder='" . $this->decimal($row_po_awal[Purchase_Order_Awal::$QTY])  . "'></td>";
                $table_detail .= "<td class='align-middle text-right'><input style='width:100%' disabled class='text-right' type='number' id='txt_netprice_" . $ctr . "' value='" . round($row_po_awal[Purchase_Order_Awal::$NET_PRICE]) . "'/></td>";
                $table_detail .= "<td class='align-middle text-right'><span id='txt_tampil_total_".$ctr."' class='text-right'>" . $this->rupiah($row_po_awal[Purchase_Order_Awal::$QTY] * round($row_po_awal[Purchase_Order_Awal::$NET_PRICE])) . "</span></td>";
                $table_detail .= "</tr>";
                $ctr += 1;
                $total += ($row_po_awal[Purchase_Order_Awal::$QTY] * $row_po_awal[Purchase_Order_Awal::$NET_PRICE]);
            }

            $table_detail .= '<tr>';
            $table_detail .= '<td colspan=5 class="text-right fw-bold">TOTAL</td>';
            $table_detail .= '<td class="text-right"><span id="txt_tampil_total" class="text-right">'. $this->rupiah($total) .'</span></td>';
            $table_detail .= '</tr>';
            $table_detail .= '<tr>';
            $table_detail .= '<td colspan=5 class="align-middle text-right fw-bold">Diskon (Rupiah)</td>';
            // <span id="txt_tampil_diskon" class="text-right">' . $this->rupiah($data_po[Purchase_Order::$DISKON]) . '</span>

            if($data_po[Purchase_Order::$DISKON]==null){
                $data_po[Purchase_Order::$DISKON]=0;
            }
            $table_detail .= '<td class="text-right">
                                <input type="number" id="txt_diskon" ' . $disabled . ' class="text-right" onkeyup="calculate_total_with_ppn()"  value="' . $data_po[Purchase_Order::$DISKON] . '">
                                </td>';
            $table_detail .= '</tr>';
            $table_detail .= '<tr>';
            $table_detail .= '<td colspan=5 class="align-middle text-right fw-bold">PPN (%)</td>';
            //<span id="txt_tampil_ppn" class="text-right">' . $data_po[Purchase_Order::$PPN] . '</span>

            if ($data_po[Purchase_Order::$PPN] == null) {
                $data_po[Purchase_Order::$PPN] = 0;
            }
            $table_detail .= '<td class="text-right">
                                <input type="number" id="txt_ppn" style="width:15%" ' . $disabled . ' onkeyup="calculate_total_with_ppn()" class="text-right" value="' . $data_po[Purchase_Order::$PPN] . '">
                                </td>';
            $table_detail .= '</tr>';

            //Total
            $total_akhir = (($total - $data_po[Purchase_Order::$DISKON]) * (100 + $data_po[Purchase_Order::$PPN]) / 100);
            $table_detail .= '<tr>';
            $table_detail .= '<td colspan=5 class="text-right fw-bold">TOTAL (INC. PPN)</td>';
            $table_detail .= '<td class="text-right"><span id="txt_tampil_total_ppn" class="text-right">' . $this->rupiah($total_akhir) . '</td>';
            $table_detail .= '</tr>';
        }
        $data['ctr'] = $ctr;

        $data['detail_product'] .= $table_detail;
        $data['detail_product'] .= "</tbody>";
        echo json_encode($data);
    }
    public function get_view_print_by_no(){
        $data = [];
        $table_detail = "";
        $data['detail_product'] = '
						<thead>
							<tr class="text-center">
                                <td style="border: 1px solid black;" class="fw-bold">NO</td>
                                <td style="border: 1px solid black;" class="fw-bold">SCHEDULE</td>
                                <td style="border: 1px solid black;" class="fw-bold">DESCRIPTION</td>
                                <td style="border: 1px solid black;" class="fw-bold">UOM</td>
                                <td style="border: 1px solid black;" class="fw-bold">QUANTITY</td>
                                <td style="border: 1px solid black;" class="fw-bold">UNIT PRICE</td>
                                <td style="border: 1px solid black;" class="fw-bold">AMOUNT(IDR)</td>
							</tr>
						</thead><tbody>';

        $po = $this->i_p("n");
        $qry = $this->po_model->get_by_no($po);

        $data_po = $qry[Purchase_Order::$TABLE_NAME];
        $data['num_rows'] = $data_po->num_rows();
        if($data_po->num_rows()>0){
            $data_po = $data_po->row_array();
            $data[Purchase_Order::$ID] = $data_po[Purchase_Order::$ID];
            $data[Purchase_Order::$ID_PR] = $data_po[Purchase_Order::$ID_PR];
            $data[Purchase_Order::$TANGGAL] = date("d.m.Y",strtotime($data_po[Purchase_Order::$TANGGAL]));
            $ctr = 0;
            $total = 0;
            $tipe = "ZO";
            $vendor = "";
            if ($qry[Purchase_Order_Awal::$TABLE_NAME]->num_rows() > 0) {
                foreach ($qry[Purchase_Order_Awal::$TABLE_NAME]->result_array() as $row_po_awal) {
                    $vendor = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $row_po_awal[Purchase_Order_Awal::$ID_VENDOR])->row_array();
                    $material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_po_awal[Purchase_Order_Awal::$ID_MATERIAL])->row_array();
                    $table_detail .= "<tr style='font-family:'Times New Roman', Times, serif'>";
                        $table_detail .= '<td style="border: 1px solid black;" class="text-center">'. ($ctr+1) .'</td>';
                        $table_detail .= '<td style="border: 1px solid black;" class="text-center">'. date("d.m.Y",strtotime($data_po[Purchase_Order::$TANGGAL_KIRIM])) .'</td>';
                        $table_detail .= '<td style="padding-left:1%;border: 1px solid black;" class="text-left">' . $row_po_awal[Purchase_Order_Awal::$SHORT_TEXT].'</td>';
                        $table_detail .= '<td style="border: 1px solid black;" class="text-center">' . $material[Material_RM_PM::$UNIT].'</td>';
                        $table_detail .= '<td style="padding-right:1%;border: 1px solid black;" class="text-right">' . $this->decimal($row_po_awal[Purchase_Order_Awal::$QTY],0).'</td>';
                        $table_detail .= '<td style="padding-right:1%;border: 1px solid black;" class="text-right">' . $this->rupiah($row_po_awal[Purchase_Order_Awal::$NET_PRICE]).'</td>';
                        $total += ($row_po_awal[Purchase_Order_Awal::$QTY] * $row_po_awal[Purchase_Order_Awal::$NET_PRICE]);
                        $table_detail .= '<td style="padding-right:1%;border: 1px solid black;" class="text-right">' . $this->rupiah($row_po_awal[Purchase_Order_Awal::$QTY]*$row_po_awal[Purchase_Order_Awal::$NET_PRICE]).'</td>';
                    $table_detail .= "</tr>";
                    $ctr += 1;
                }
            }
            for ($i = 0; $i < 10 - $ctr; $i++) {
                $table_detail .= "<tr>";
                $table_detail .= '<td class="text-center">&nbsp</td>';
                $table_detail .= '<td class="text-center"></td>';
                $table_detail .= '<td class="text-center"></td>';
                $table_detail .= '<td class="text-center"></td>';
                $table_detail .= '<td class="text-center"></td>';
                $table_detail .= '<td class="text-center"></td>';
                $table_detail .= '<td class="text-center"></td>';
                $table_detail .= "</tr>";
            }
            $tipe .= $material[Material_RM_PM::$TIPE];
            $data[Material_RM_PM::$TIPE] = $tipe;

            $data[Vendor::$NAMA] = strtoupper($vendor[Vendor::$NAMA]) . " ( " . $vendor[Vendor::$ID] . " ) "; 

            $data['ctr'] = $ctr;
            $data['detail_product'] .= $table_detail;
            $data['detail_product'] .= "</tbody>";
            $data['subtotal'] = $this->rupiah($total);
            $data[Purchase_Order::$DISKON] = $this->rupiah($data_po[Purchase_Order::$DISKON]);
            $total_dpp = $total - $data_po[Purchase_Order::$DISKON];
            $data['total_dpp'] = $this->rupiah($total_dpp);
            $data[Purchase_Order::$PPN] = $data_po[Purchase_Order::$PPN]."%";
            $ppn_value = $total_dpp * $data_po[Purchase_Order::$PPN] / 100;
            $data['ppn_value'] = $this->rupiah($ppn_value);
            $data['total_all'] = $this->rupiah($total_dpp + $ppn_value);

            $data_payterm = $this->po_model->get(Payterm::$TABLE_NAME, Payterm::$ID, $data_po[Purchase_Order::$ID_PAYTERM],null,null,false)->row_array();
            $data[Payterm::$DESKRIPSI] = ucwords(strtolower($data_payterm[Payterm::$DESKRIPSI]));
        }


        echo json_encode($data);
    }
    public function get_view_cancel(){
        $kal = "";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");
        $data = $this->po_model->get_cancel_by_tanggal($dari_tanggal, $sampai_tanggal);

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">';

        $arr_header = ["Nomor Good Receipt", "Aksi"];

        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
							<thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';
        if (isset($data) && $data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$ID] . "</td>";
                $kal .= "<td class='text-center'>" .
                    '<button onclick="fill_detail_modal(' . "'" . $row[Purchase_Order_Checker_Header::$ID] . "'" . ')" class="btn btn-outline-primary" data-mdb-ripple-color="dark">Ubah</button>' .
                    "</td>";
                $kal .= "</tr>";
            }
        }
        $kal .= '</tbody>
                    </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";

        echo $kal;
    }
    public function get_view_detail_modal(){
        $table_detail = "";
        $arr_header = ["Nomor", "Material", "Qty"];

        $data['detail_product_checker'] = '
					<table id="tbl_detail_product_checker" class="table table-bordered table-sm">
						<thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';

        $gr = $this->i_p("n");
        $qry = $this->po_model->checker_get_by_gr($gr);
        if ($qry[Purchase_Order_Checker_Header::$TABLE_NAME]->num_rows() > 0) {
            $data[Purchase_Order_Checker_Header::$NOMOR_PO] = $qry[Purchase_Order_Checker_Header::$TABLE_NAME]->row_array()[Purchase_Order_Checker_Header::$NOMOR_PO];
            $data[Purchase_Order_Checker_Header::$ID] = $qry[Purchase_Order_Checker_Header::$TABLE_NAME]->row_array()[Purchase_Order_Checker_Header::$ID];
            foreach ($qry[Purchase_Order_Checker_Detail::$TABLE_NAME]->result_array() as $row_detail) {

                $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL]);
                $material = $qry_material->row_array();

                $table_detail .= "<tr>";
                $table_detail .= '<td class="text-center font-sm f-aleo">' . $row_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT] . '</td>';
                $table_detail .= '<td class="text-center font-sm f-aleo">' . $material[Material_RM_PM::$KODE_PANGGIL] . '</td>';
                $table_detail .= '<td class="text-right font-sm f-aleo"><span>' . $this->decimal($row_detail[Purchase_Order_Checker_Detail::$QTY], 3) . ' ' . $material[Material_RM_PM::$UNIT] . '</span></td>';
                $table_detail .= "</tr>";
            }
        }
        $data['detail_product_checker'] .= $table_detail;
        $data['detail_product_checker'] .= "</tbody>";

        //PO Awal
        $table_detail = "";
        $arr_header = ["Nomor", "Material", "Qty", "Net Price"];

        $data['detail_product_awal'] = '
					<table id="tbl_detail_product_awal" class="table table-bordered table-sm">
						<thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';
        $qry = $this->po_model->get_by_no($data[Purchase_Order_Checker_Header::$NOMOR_PO]);
        if ($qry[Purchase_Order_Awal::$TABLE_NAME]->num_rows() > 0) {
            foreach ($qry[Purchase_Order_Awal::$TABLE_NAME]->result_array() as $row_po_awal) {

                $qry_material = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_po_awal[Purchase_Order_Awal::$ID_MATERIAL]);
                $material = $qry_material->row_array();

                $table_detail .= "<tr>";
                $table_detail .= '<td class="text-center font-sm f-aleo">' . $row_po_awal[Purchase_Order_Awal::$NOMOR_URUT] . '</td>';
                $table_detail .= '<td class="text-center font-sm f-aleo">' . $material[Material_RM_PM::$KODE_PANGGIL] . '</td>';
                $table_detail .= '<td class="text-right font-sm f-aleo"><span>' . $this->decimal($row_po_awal[Purchase_Order_Awal::$QTY], 3) . ' ' . $material[Material_RM_PM::$UNIT] . '</span></td>';
                $table_detail .= '<td class="text-right font-sm f-aleo"><span>Rp. ' . $this->rupiah($row_po_awal[Purchase_Order_Awal::$NET_PRICE]) . '</span></td>';
                $table_detail .= "</tr>";
            }
        }
        $data['detail_product_awal'] .= $table_detail;
        $data['detail_product_awal'] .= "</tbody>";

        echo json_encode($data);
    }
    public function get_view_detail(){
        $kal = "";
        $data = $this->po_model->get(Purchase_Order_Checker_Header::$TABLE_NAME);

        $arr_header = ["Nomor Good Receipt", "Aksi"];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if (isset($data) && $data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$ID] . "</td>";
                $kal .= "<td class='text-center'>" .
                    '<button onclick="fill_detail_modal(' . "'" . $row[Purchase_Order_Checker_Header::$ID] . "'" . ')" class="btn btn-outline-primary" data-mdb-ripple-color="dark">Ubah</button>' .
                    "</td>";
                $kal .= "</tr>";
            }
        }
        $kal .= '</tbody>
            </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function get_view_tutup(){
        $kal = "";
        $dari_tanggal = $this->i_p("dt");
        $sampai_tanggal = $this->i_p("st");
        $data = $this->po_model->get_utuh($dari_tanggal,$sampai_tanggal);

        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-3"></div>';
            $kal .= '<div class="col-sm-6">';
                $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col" class="text-center">Tanggal</th>
                                    <th scope="col" class="text-center">Nomor PO</th>
                                    <th scope="col" class="text-center">Status</th>
                                    <th scope="col" class="text-center">Aksi</th>
                                </tr>
                            </thead><tbody>';
                if ($data->num_rows() > 0) {
                    foreach ($data->result_array() as $row_ws) {
                        $kal .= "<tr>";
                            $kal .= "<td class='text-center align-middle'>" . $row_ws[Purchase_Order::$TANGGAL] . "</td>";
                            $kal .= "<td class='text-center align-middle'>" . $row_ws[Purchase_Order::$ID] . "</td>";
                            if($row_ws[Purchase_Order::$IS_DELETED]==0){
                                $kal .= "<td class='text-center green-text align-middle'>AKTIF</td>";
                                $kal .= "<td class='text-center'><button type='button' class='center btn btn-outline-danger' data-mdb-ripple-color='dark' onclick='update_status(".$row_ws[Purchase_Order::$ID].",".($row_ws[Purchase_Order::$IS_DELETED]*-1+1).")'>TUTUP</button></td>";
                            }else{
                                $kal .= "<td class='text-center red-text align-middle'>NON-AKTIF</td>";
                                $kal .= "<td class='text-center'><button type='button' class='center btn btn-outline-success' data-mdb-ripple-color='dark' onclick='update_status(".$row_ws[Purchase_Order::$ID].",".($row_ws[Purchase_Order::$IS_DELETED]*-1+1).")'>KEMBALI</button></td>";
                            }
                        $kal .= "</tr>";
                    }
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    

    //1 RM PM, 2 LAIN LAIN
    public static $STATIC_HEADER_REKAP = [
        "Tanggal", "Nomor PO", "Vendor",
        "Vendor Desc.", "No", "Material / Asset", "Material Desc.", "Service", "Service Desc.", "Qty Awal (KG/PCS)", "Qty Sisa (KG/PCS)", "Net Price (Rupiah)", "Status"
    ];
    public static $STATIC_HEADER_REKAP_CLASS = [
        "cb_0", "cb_0", "cb_0", "cb_0", "cb_0", "cb_0", "cb_0", "cb_2", "cb_2", "cb_0", "cb_1", "cb_2", "cb_1"
    ];

    public function get_cb_kolom_tampil(){
        $data = [];

        $kal = "<div class='row'>";
        for ($i = 0; $i < count($this::$STATIC_HEADER_REKAP); $i++) {
            $kal .= "<div class='col-sm-3'>
                        <label>
                        <input class='rekap_class " . $this::$STATIC_HEADER_REKAP_CLASS[$i] . "' type='checkbox' value='tanggal' id='cb_" . $i . "'></input>
                        " . $this::$STATIC_HEADER_REKAP[$i] . "</label>
                    </div>";
        }
        $kal .= "</div>";
        $data["kal"] = $kal;
        $data["ctr"] = count($this::$STATIC_HEADER_REKAP);
        echo json_encode($data);
    }
    public function get_view_report(){
        $kal = "";
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $arr_header_class = $this->i_p("hc");
        $data = $this->po_model->get_report($tanggal_dari,$tanggal_sampai);
        
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>';
        $kal .= '<tr class="table-primary">';
        for ($i = 0; $i < count($this::$STATIC_HEADER_REKAP); $i++) {
            if ($arr_header_class[$i] == 1)
                $kal .= '<th scope="col" class="fw-bold align-middle text-center">' . $this::$STATIC_HEADER_REKAP[$i] . '</th>';
        }
        $kal .= '</tr></thead>';
        $kal .= '<tbody>';
        foreach ($data->result_array() as $row_po) {
            $ctr = 0;
            $kal .= "<tr>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Purchase_Order::$TANGGAL] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Purchase_Order::$ID] . "</td>";
            //$kal .= "<td class='text-center'>" . ($row_po[Purchase_Order_Awal::$NOMOR_URUT]/10) . "</td>";
            if ($arr_header_class[$ctr++] == 1)
            $kal .= "<td class='align-middle text-center'>" . $row_po[Vendor::$ID] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>" . $row_po[Vendor::$NAMA] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Purchase_Order_Awal::$NOMOR_URUT] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>" . $row_po[Material_RM_PM::$KODE_MATERIAL] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-left'>" . $row_po[Purchase_Order_Awal::$SHORT_TEXT] . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>-</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>-</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-right'>" . $this->decimal($row_po[Purchase_Order_Awal::$QTY], 3) . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-right'>" . $this->decimal($row_po[Purchase_Order_Awal::$S_QTY_SISA], 3) . "</td>";
            if ($arr_header_class[$ctr++] == 1)
                $kal .= "<td class='align-middle text-center'>-</td>";
            if ($arr_header_class[$ctr++] == 1){
                if($row_po[Purchase_Order::$IS_DELETED]==0)
                    $kal .= "<td class='align-middle green-text text-center'>AKTIF</td>";
                else
                    $kal .= "<td class='align-middle red-text text-center'>NON-AKTIF</td>";
            }
            $kal .= "</tr>";
        }
        $kal .= '   </tbody>
                        </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function get_sj_by_gr(){
        $gr = $this->i_p("i");
        $sj = "";
        $data = $this->po_model->get(Purchase_Order_Checker_Header::$TABLE_NAME,Purchase_Order_Checker_Header::$ID,$gr);
        if($data->num_rows()>0){
            $sj = $data->row_array()[Purchase_Order_Checker_Header::$NOMOR_SJ];
        }
        echo $sj;
    }

    //Evaluasi
    public static $ARR_EV_STATUS_PENGIRIMAN = [
        1 => "Barang Kosong",
        2 => "Terlambat",
        3 => "Tepat Waktu"/*,
        3 => "Hold (Stock masih Cukup)"*/
    ];
    public static $ARR_EV_KUALITAS = [
        1 => "Rusak/ Tidak Sesuai Standar",
        2 => "Baik"
    ];
    public static $ARR_EV_QTY_PENERIMAAN = [
        1 => "Tidak Sesuai SJ",
        2 => "Sesuai SJ"
    ];
    public static $ARR_EV_AFTER_SALES = [
        1 => "Tidak ada Follow Up",
        2 => "Ada Follow Up"
    ];
    public function evaluation_get_view_insert(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $kal = "";
        $data = $this->po_model->evaluation_get_insert($tanggal_dari, $tanggal_sampai);

        $arr_header = ["Nomor Surat Jalan", "Aksi"];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $text_color = "red-text";

                if ($row[Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN] != null)
                    $text_color = "";

                $kal .= "<tr class='" . $text_color . "'>";
                $kal .= "<td class='text-center'>" . $row[H_Good_Receipt::$NOMOR_SJ] . "</td>";
                $kal .= "<td class='text-center'>" .
                    '<button onclick="fill_detail_modal(' . "'" . $row[H_Good_Receipt::$NOMOR_SJ] . "'" . ')" class="btn btn-outline-primary" data-mdb-ripple-color="dark">Ubah</button>' .
                    "</td>";
                $kal .= "</tr>";
                //}
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function evaluation_get_view_report(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $kategori = $this->i_p("k");
        $kal = "";
        $data = $this->po_model->evaluation_get_report($kategori, $tanggal_dari, $tanggal_sampai);

        if($kategori == 2){
            $arr_header = [
                "Tanggal", "Nomor SJ", "Vendor", "Status Pengiriman", "Kualitas", "Quantity Penerimaan",
                "After Sales", "Total"
            ];
        }else{
            $arr_header = [
                "Vendor", "Total Poin", "Total SJ","Avg", "Standar Penilaian"
            ];
        }
        

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $header = "";
        if($kategori == 2){
            $header = "Detail";
        }else{
            $header = "Rekap";
        }
        $kal .= '<h3 class="text-center fw-bold"><span id="txt_file_name">'. $header .' Evaluasi Vendor '. date("d M Y",strtotime($tanggal_dari)) .' - '. date("d M Y",strtotime($tanggal_sampai)) . '</span></h3>';

        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                if($kategori == 2){
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$TANGGAL] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$NOMOR_SJ] . "</td>";
                }
                $kal .= "<td class='text-left'>" . $row[Vendor::$NAMA] . "</td>";
                if($kategori == 2){
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$EV_KUALITAS] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Checker_Header::$EV_AFTER_SALES] . "</td>";
                }else{
                    $total = $row["total"];
                    $jumlah = $row["jumlah"];
                    
                    $kal .= "<td class='text-center'>" . $total . "</td>";
                    $kal .= "<td class='text-center'>" . $jumlah . "</td>";
                    $kal .= "<td class='text-center'>" . round($total/$jumlah,1) . "</td>";
                    if($total > 7){
                        $kal .= "<td class='text-center'>Baik</td>";

                    } else if ($total == 7) {
                        $kal .= "<td class='text-center'>Cukup</td>";
                    } else{
                        $kal .= "<td class='text-center'>Kurang</td>";
                
                    }

                }
                if($kategori == 2){
                    //Hitung Total
                    $total = $row[Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN] + $row[Purchase_Order_Checker_Header::$EV_KUALITAS] +
                        $row[Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN] + $row[Purchase_Order_Checker_Header::$EV_AFTER_SALES];
                    $kal .= "<td class='text-center'>" . $total . "</td>";
                }
                $kal .= "</tr>";
                //}
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function evaluation_get_view_detail_modal(){
        $data = [];
        $sj = $this->i_p("n");
        $temp = $this->evaluation_get_by_sj($sj);
        if ($temp->num_rows() > 0) {
            $h_purchase_order = $temp->row_array();
            $div_qty_material = "";
            $data["tanggal"] = date("Y-m-d", strtotime($h_purchase_order[Purchase_Order_Checker_Header::$TANGGAL]));
            $data["tanggal_estimasi"] = date("Y-m-d", strtotime($h_purchase_order[Purchase_Order::$TANGGAL_ESTIMASI_SAMPAI]));
            $data["no_po"] = $h_purchase_order[Purchase_Order::$ID];

            //Array Evaluasi Status Pengiriman
            $kal = "";
            foreach ($this::$ARR_EV_STATUS_PENGIRIMAN as $key => $value) {
                $selected = "";
                if ($key == $h_purchase_order[Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN])
                    $selected = "selected";
                $kal .= '<option ' . $selected . ' value=' . $key . '>' . $value . '</option>';
            }
            $data['opt_ev_status_pengiriman'] = $kal;
            $secs = strtotime($data["tanggal"]) - strtotime($data["tanggal_estimasi"]);
            $days = floor($secs / 86400);
            if ($days > 0) //terlambat
                $data["c_status_pengiriman"] = "<span class='text-danger'>Terlambat $days hari</span>";
            else  //tepat waktu
                $data["c_status_pengiriman"] = "<span class='text-success'>Tepat Waktu</span>";

            //Array Evaluasi Kualitas
            $kal = "";
            foreach ($this::$ARR_EV_KUALITAS as $key => $value) {
                $selected = "";
                if ($key == $h_purchase_order[Purchase_Order_Checker_Header::$EV_KUALITAS])
                    $selected = "selected";
                $kal .= '<option ' . $selected . ' value=' . $key . '>' . $value . '</option>';
            }
            $data['opt_ev_kualitas'] = $kal;

            //Array Evaluasi Qty Penerimaan
            $kal = "";
            foreach ($this::$ARR_EV_QTY_PENERIMAAN as $key => $value) {
                $selected = "";
                if ($key == $h_purchase_order[Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN])
                    $selected = "selected";
                $kal .= '<option ' . $selected . ' value=' . $key . '>' . $value . '</option>';
            }
            $data['opt_ev_qty_penerimaan'] = $kal;

            $history_sj = "";
            $ctr = 1;
            $qry_history_sj = $this->po_model->get(Purchase_Order_Checker_Header::$TABLE_NAME, Purchase_Order_Checker_Header::$NOMOR_PO, $h_purchase_order[Purchase_Order::$ID], Purchase_Order_Checker_Header::$TANGGAL);
            foreach ($qry_history_sj->result_array() as $row_history_sj) {
                $bold = "";
                if ($this->equals($row_history_sj[Purchase_Order_Checker_Header::$NOMOR_SJ], $sj))
                    $bold = "fw-bold";
                $history_sj .= '<tr class="text-center">
                                    <td><span class="' . $bold . '">' . ($ctr++) . '</span></td>
                                    <td>
                                        <span class="' . $bold . '">' . $row_history_sj[Purchase_Order_Checker_Header::$NOMOR_SJ] . '</span>
                                    </td>
                                    <td>
                                        <span class="' . $bold . '">' . $row_history_sj[Purchase_Order_Checker_Header::$TANGGAL] . '</span>
                                    </td>
                                </tr>';
            }
            $data['div_history_sj'] = "<h5 class='text-center fw-bold'>History Surat Jalan</h5>" .
                '<div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-6">
                                            <table class="table table-bordered table-sm">
                                                <tr class="table-primary text-center fw-bold">
                                                    <th>No Urut</th>
                                                    <th>Nomor SJ</th>
                                                    <th>Tanggal</th>
                                                </tr>
                                                ' . $history_sj . '
                                            </table>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>';
            foreach ($temp->result_array() as $row_po) {

                $selisih = $row_po[Purchase_Order_Checker_Detail::$QTY_SJ] - $row_po[Purchase_Order_Checker_Detail::$QTY];
                if ($selisih != 0)
                    $data['c_qty_penerimaan'] = '<span class="text-danger">Selisih ' . abs($selisih) . '</span>';
                else
                    $data['c_qty_penerimaan'] = '<span class="text-success">Tidak Selisih</span>';

                $div_qty_material .= '
                    <div class="row">
                        <div class="col-sm-1 text-right"></div>
                        <div class="col-sm-4 text-right">
                            (Kode Material) Deskripsi:
                        </div>
                        <div class="col-sm-6 text-left"><span class="fw-bold">(' . $row_po[Material_RM_PM::$KODE_MATERIAL] . ')</span> ' . $row_po[Material_RM_PM::$DESCRIPTION] . '</div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 text-right"></div>
                        <div class="col-sm-4 text-right">
                            Qty Surat Jalan:
                        </div>
                        <div class="col-sm-6 text-left fw-bold">' . $row_po[Purchase_Order_Checker_Detail::$QTY_SJ] . '</div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 text-right"></div>
                        <div class="col-sm-4 text-right">
                            Qty Diterima:
                        </div>
                        <div class="col-sm-6 text-left fw-bold">' . $row_po[Purchase_Order_Checker_Detail::$QTY] . '</div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 text-right"></div>
                        <div class="col-sm-4 text-right">
                        </div>
                        <div class="col-sm-6 text-left fw-bold">' . $data['c_qty_penerimaan'] . '
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                ';
                $data['div_qty_material'] = $div_qty_material;
            }

            //Array Evaluasi After Sales
            $kal = "";

            foreach ($this::$ARR_EV_AFTER_SALES as $key => $value) {
                $selected = "";
                if ($key == $h_purchase_order[Purchase_Order_Checker_Header::$EV_AFTER_SALES])
                    $selected = "selected";
                $kal .= '<option ' . $selected . ' value=' . $key . '>' . $value . '</option>';
            }
            $data['opt_ev_after_sales'] = $kal;
        }
        echo json_encode($data);
    }
    public function evaluation_get_by_sj($id_sj){
        return $this->po_model->evaluation_get_by_sj($id_sj);
    }
    public function evaluation_insert(){
        $sj = $this->i_p("n");
        $tanggal = $this->i_p("t");
        $ev_status_pengiriman = $this->i_p("es");
        $ev_status_pengiriman = $this->i_p("es");
        $ev_qty_penerimaan = $this->i_p("eq");
        $ev_kualitas = $this->i_p("ek");
        $ev_after_sales = $this->i_p("ea");
        echo MESSAGE[$this->po_model->evaluation_insert($sj, $tanggal, $ev_status_pengiriman, $ev_qty_penerimaan, $ev_kualitas, $ev_after_sales)];
    }

    //Kurs
    public function kurs_update(){
        $kurs = $this->i_p("k");
        $tanggal_berlaku = $this->i_p("t");
        echo MESSAGE[$this->po_model->kurs_update($kurs,$tanggal_berlaku)];
    }
    public function kurs_get_view_history(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $kal = "";
        $data = $this->po_model->kurs_get_history($tanggal_dari, $tanggal_sampai);

        $arr_header = [
            "Tanggal Berlaku", "ID Kurs", "Kurs (Rp)"
        ];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-4"></div>';
        $kal .= '<div class="col-sm-4">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row[Kurs::$TANGGAL_BERLAKU] . "</td>";
                $kal .= "<td class='text-center'>" . $row[Kurs::$ID] . "</td>";
                $kal .= "<td class='text-right'>" . $this->rupiah($row[Kurs::$VALUE]) . "</td>";
                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-4"></div>';
        $kal .= "</div>";
        echo $kal;
    }

    //Price List
    public function price_insert(){
        $arr_id_material = $this->i_p("am");
        $arr_id_vendor = $this->i_p("av");
        $arr_valid_from = $this->i_p("af");
        $arr_valid_to = $this->i_p("at");
        $arr_currency = $this->i_p("ac");
        $arr_price = $this->i_p("ap");
        $ctr = $this->i_p("c");
        echo MESSAGE[$this->po_model->price_insert($ctr,$arr_id_material,$arr_id_vendor,$arr_valid_from,$arr_valid_to,
                                                    $arr_currency,$arr_price)];
    }
    public function price_get_view_history(){
        $vf_tanggal_dari = $this->i_p("df");
        $vf_tanggal_sampai = $this->i_p("sf");
        $vt_tanggal_dari = $this->i_p("dt");
        $vt_tanggal_sampai = $this->i_p("st");
        $id_vendor = $this->i_p("iv");
        $kal = "";
        $data = $this->po_model->price_get_history($vf_tanggal_dari, $vf_tanggal_sampai, $vt_tanggal_dari, $vt_tanggal_sampai,$id_vendor);

        $arr_header = [
            "Valid Dari", "Valid Sampai", "Material", "ID PL","Deskripsi", "Vendor", "Currency", "Price"
        ];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Price_List::$TANGGAL_MULAI] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Price_List::$TANGGAL_AKHIR] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Price_List::$ID] . "</td>";
                    $kal .= "<td class='text-left'>" . $row[Material_RM_PM::$DESCRIPTION] . "</td>";
                    $kal .= "<td class='text-center blue-text' title='" . $row[Vendor::$NAMA] . "'>" . $row[Purchase_Order_Price_List::$ID_VENDOR] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Purchase_Order_Price_List::$CURRENCY] . "</td>";
                    $kal .= "<td class='text-right'>" . $this->decimal($row[Purchase_Order_Price_List::$PRICE],3) . "</td>";
                $kal .= "</tr>";
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function price_get_active(){
        $id_vendor = $this->i_p("iv");
        $id_material = $this->i_p("im");
        $tanggal_po = $this->i_p("t");
        $price = 0;
        $qry_price = $this->po_model->price_get_active($id_vendor, $id_material, $tanggal_po);

        $data=[];
        $data["num_rows"]= $qry_price->num_rows();
        if ($qry_price->num_rows() > 0) {
            $qry_price = $qry_price->row_array();
            if ($this->equals($qry_price[Purchase_Order_Price_List::$CURRENCY], Purchase_Order_Price_List::$S_CURRENCY_USD)) {
                //Kalau masih USD kalikan dengan kurs
                $kurs = 0;
                $qry_kurs = $this->kurs_model->get_last($tanggal_po);
                if ($qry_kurs->num_rows() > 0) {
                    $kurs = $qry_kurs->row_array()[Kurs::$VALUE];
                    $price = $qry_price[Purchase_Order_Price_List::$PRICE] * $kurs;
                }
            } else {
                $price = $qry_price[Purchase_Order_Price_List::$PRICE];
            }
        }
        $data[Purchase_Order_Price_List::$PRICE]=$price;
        echo json_encode($data);
    }

    //Email
    public function email_send(){
        $this->load->library('mailer');

        $email_penerima = "sayachristian1233@gmail.com";
        $nomor_po = $this->i_p("n");
        $subject= "Purchase Order " . $nomor_po;
        if (isset($_FILES['attachment']))
            $attachment = $_FILES['attachment'];
        $sendmail = array(
            'email_penerima' => $email_penerima,
            'subjek' => $subject,
            'content' => " "
            //'attachment' => $attachment
        );
        //echo $attachment;
        //$send = $this->mailer->send($sendmail);
        if (empty($attachment['name'][0])) { // Jika tanpa attachment
            $send = $this->mailer->send($sendmail); // Panggil fungsi send yang ada di librari Mailer
        } else { // Jika dengan attachment
            echo "attachment";
            $attachment = $this->email_flip_row_col_array($attachment);
            $send = $this->mailer->send_with_attachment($sendmail,$attachment); // Panggil fungsi send_with_attachment yang ada di librari Mailer
        }
    }
    public function email_flip_row_col_array($array){
        $out = array();
        foreach ($array as  $rowkey => $row) {
            foreach ($row as $colkey => $col) {
                $out[$colkey][$rowkey] = $col;
            }
        }
        return $out;
    }

}
