<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Login_Controller extends Library_Controller {

	//general function
	public function index(){
		if ($this->session->userdata(SESSION_KARYAWAN_PENTA)) {
			$this->load->view('home');
		} else {
			$this->load->view('login');
		}
		
	}
	public function login(){
		$username = $this->i_p("u");
		$password = $this->i_p("p");
		$message = $this->karyawan_model->cek_login($username, $password);
		if ($this->str_contains(MESSAGE[$message], MESSAGE_KEY_SUCCESS)) {
			$arr = array();
			$karyawan = $this->karyawan_model->get_by(Karyawan::$USERNAME, $username)->row_array();
			$arr[Karyawan::$USERNAME] = $username;
			$arr[Karyawan::$NAMA] = $karyawan[Karyawan::$S_K_NAMA];
			$arr[Karyawan::$ID] = $karyawan[Karyawan::$ID];
			$this->session->set_userdata(SESSION_KARYAWAN_PENTA, $arr);
		}
		echo MESSAGE[$message];
	}
	public function logout(){
		$this->session->unset_userdata(SESSION_KARYAWAN_PENTA);
		$this->load->view("home");
	}
}
