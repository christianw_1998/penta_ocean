<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Forecast_Controller extends Library_Controller {
    public function upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '20000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_forecast";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->forecast_model->update_batch($data);
    }

    public function view_analysis(){
        $faktor_pengali = $this->i_p("f");
        $dd_fb = $this->i_p("b");

        $arr_header = ["ID Material", "Deskripsi", "Current Stock", "Sisa PO", "Kebutuhan", "Selisih"];

        $kal = "";
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="f-aleo table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead><tbody>';

        $arr_need = [];
        $arr_sisa = [];
        //Ambil Semua Material RM beserta Kebutuhan Stocknya
        $data_need=$this->forecast_model->get_needs($dd_fb,Material_RM_PM::$S_UNIT_KG);
        if($data_need->num_rows()>0){
            foreach($data_need->result_array() as $row_need){
                $arr_need[$row_need[Material_RM_PM::$ID]]= $row_need[Forecast::$S_NEEDS_KG];
            }
        }
        //Ambil Semua Material PM beserta Kebutuhan Stocknya
        $data_need = $this->forecast_model->get_needs($dd_fb, Material_RM_PM::$S_UNIT_PCS);
        if ($data_need->num_rows() > 0) {
            foreach ($data_need->result_array() as $row_need) {
                $arr_need[$row_need[Material_RM_PM::$ID]] = $row_need[Forecast::$S_NEEDS_KG];
            }
        }
        //Ambil Semua Sisa PO Outstanding
        $data_sisa = $this->po_model->get_report(null,null);
        if ($data_sisa->num_rows() > 0) {
            foreach ($data_sisa->result_array() as $row_sisa) {
                if(isset($arr_sisa[$row_sisa[Material_RM_PM::$ID]]))
                    $arr_sisa[$row_sisa[Material_RM_PM::$ID]] += $row_sisa[Purchase_Order_Awal::$S_QTY_SISA];
                else
                    $arr_sisa[$row_sisa[Material_RM_PM::$ID]] = $row_sisa[Purchase_Order_Awal::$S_QTY_SISA];

            }
        }
        $data = $this->forecast_model->get_table_needs(null);
        $ctr = 0;
        foreach ($data->result_array() as $row) {
            $curr_stock = $this->rmpm_model->stock_get_by($row[Material_RM_PM::$ID]);
            $kal .= "<tr>";
            $kal .= "<td class='align-middle text-center'>" . $row[Material_RM_PM::$KODE_MATERIAL] . "</td>";
            $kal .= "<td class='align-middle text-left'>" . $row[Material_RM_PM::$DESCRIPTION] . "</td>";
            $kal .= "<td class='align-middle text-right'>" . $this->decimal(round($curr_stock, 2)) . "</td>";
            
            //Perhitungan Sisa
            $sisa=0;
            if (isset($arr_sisa[$row[Material_RM_PM::$ID]]))
                $sisa= round($arr_sisa[$row[Material_RM_PM::$ID]], 2);
            $kal .= "<td class='align-middle text-right'>" . $this->decimal($sisa,2) . "</td>";
            
            //Perhitungan Kebutuhan
            $needs=0;
            if(isset($arr_need[$row[Material_RM_PM::$ID]]))
                $needs= round($arr_need[$row[Material_RM_PM::$ID]],2);
            $kal .= "<td class='align-middle text-right'>" . $this->decimal($needs,2) . "</td>";
            $selisih = $curr_stock + $sisa - $needs;
            $text_color="red-text";
            if($selisih>=0)
                $text_color="green-text";
            $kal .= "<td class='align-middle text-right $text_color'>" . $this->decimal(round($selisih, 2)) . "</td>";

            $kal .= "</tr>";
            $ctr++;
        }

        $kal .= '	</tbody>
						</table>';
        $kal .= "</div>";
        $kal .= "</div>";

        echo $kal;
    }
    public function view_history(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $kal = "";
        $total_batch=0;
        $arr_header = [
            "Nomor Forecast","Tanggal", "Kode Produk", "Deskripsi", "FG Code (Lama)", "FG Code (Baru)", "Tin", "KG"
        ];

        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        $data = $this->forecast_model->get_history($tanggal_dari, $tanggal_sampai);
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $kal .= "<tr>";
                    $kal .= "<td class='text-center'>" . $row[Forecast_Batch::$ID] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Forecast_Batch::$TANGGAL] . "</td>";
                    $kal .= "<td class='text-left'>" . $row[Forecast::$PRODUCT_CODE_DISPLAY] . "</td>";
                    $kal .= "<td class='text-left'>" . $row[Forecast_Batch::$DESCRIPTION] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Forecast::$OLD_CODE] . "</td>";
                    $kal .= "<td class='text-center'>" . $row[Forecast::$NEW_CODE] . "</td>";
                    $kal .= "<td class='text-right'>" . $this->decimal($row[Forecast::$TIN]) . "</td>";
                    $kal .= "<td class='text-right'>" . $this->decimal($row[Forecast::$KG]) . "</td>";
                $kal .= "</tr>";
                $total_batch += $row[Forecast_Batch::$QTY];
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        $temp=[];
        $temp["kal"] = $kal;
        $temp[Forecast_Batch::$QTY] = "<br><h4 class='text-center'>TOTAL BATCH: <span class='fw-bold'>$total_batch</span> BATCH</h4>";
        echo json_encode($temp);
    }

    public function analysis_get_dd_by_tanggal(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $kal ="";
        $arr_dd=[];

        $data_forecast = $this->forecast_model->batch_get_by_tanggal($tanggal_dari,$tanggal_sampai);
        if($data_forecast->num_rows()>0){
            foreach($data_forecast->result_array() as $row_forecast){
                if(!in_array(substr($row_forecast[Forecast_Batch::$ID],0,8),$arr_dd)){
                    array_push($arr_dd, substr($row_forecast[Forecast_Batch::$ID], 0, 8));
                }
            }
        }
        for ($i = 0; $i < count($arr_dd); $i++) {
            $kal .= "<option value='". $arr_dd[$i]."'>". $arr_dd[$i] ."</option>";
        }   
        echo $kal;
    }
}
