<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Kinerja_Controller extends Library_Controller {

    public static $ARR_RM_PM_MUL_PO_JENIS;
    public static $ARR_RM_PM_MUL_WS_CHECKER_JENIS;
    public static $ARR_RM_PM_MUL_WS_CM_JENIS;
    public static $ARR_RM_PM_NIK_CHECKER;
    public static $RM_PM_MUL_KEBERSIHAN;
    public static $S_BARANG_MASUK="barang_masuk";
    public static $S_BARANG_KELUAR="barang_keluar";
    public static $S_TOTAL_WS="total_ws";
    public static $S_STOCK_OPNAME="stock_opname";
    public static $S_KEBERSIHAN="kebersihan";
    public static $S_TOTAL="total";

    public function init_rmpm_report(){
        $this::$ARR_RM_PM_MUL_PO_JENIS=[
            Checker_RM_PM::$S_TYPE_CAIRAN => 10,
            Checker_RM_PM::$S_TYPE_KEMASAN => 1,
            Checker_RM_PM::$S_TYPE_POWDER => 0.5
        ];
        $this::$ARR_RM_PM_MUL_WS_CHECKER_JENIS = [
            Checker_RM_PM::$S_TYPE_CAIRAN => 0.5,
            Checker_RM_PM::$S_TYPE_KEMASAN => 1,
            Checker_RM_PM::$S_TYPE_POWDER => 0.5
        ];
        $this::$ARR_RM_PM_MUL_WS_CM_JENIS = [
            Worksheet_Connect::$NIK_PREMIX => 10000,
            Worksheet_Connect::$NIK_MAKEUP => 20000,
            Worksheet_Connect::$NIK_FILLING => 20000
        ];
        $this::$RM_PM_MUL_KEBERSIHAN = 1000;
        $this::$ARR_RM_PM_NIK_CHECKER =[
            13017,14063,14052,14039
        ];

    }
    public function rmpm_view_report(){
        $this->init_rmpm_report();
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $kal = "";
        $arr_karyawan = [];
        $arr_header = [];
        
        //Barang Masuk
        $arr_po = $this->po_model->checker_get_kinerja($dt, $st);
        if ($arr_po->num_rows() > 0) {
            foreach ($arr_po->result_array() as $row_po) {
                if (in_array($row_po[Purchase_Order_Checker_Header::$NIK], $this::$ARR_RM_PM_NIK_CHECKER)) {
                    if (!isset($arr_karyawan[$row_po[Purchase_Order_Checker_Header::$NIK]][$this::$S_BARANG_MASUK]))
                        $arr_karyawan[$row_po[Purchase_Order_Checker_Header::$NIK]][$this::$S_BARANG_MASUK] = ($row_po[Purchase_Order_Checker_Detail::$S_TOT_QTY] * $this::$ARR_RM_PM_MUL_PO_JENIS[$row_po[Material_RM_PM::$JENIS]]);
                }
            }
        }

        //Barang Keluar
        $arr_ws = $this->ws_model->checker_get_kinerja($dt, $st);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                if (in_array($row_ws[Worksheet_Checker_Header::$NIK], $this::$ARR_RM_PM_NIK_CHECKER)) {
                    if (!isset($arr_karyawan[$row_ws[Worksheet_Checker_Header::$NIK]][$this::$S_BARANG_KELUAR]))
                        $arr_karyawan[$row_ws[Worksheet_Checker_Header::$NIK]][$this::$S_BARANG_KELUAR] = ($row_ws[Worksheet_Checker_Detail::$S_TOT_QTY] * $this::$ARR_RM_PM_MUL_WS_CHECKER_JENIS[$row_ws[Material_RM_PM::$JENIS]]);
                }
            }
        }

        //Total WS
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_PREMIX);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                if (!isset($arr_karyawan[$row_ws[Worksheet_Connect::$NIK_PREMIX]][$this::$S_TOTAL_WS]))
                    $arr_karyawan[$row_ws[Worksheet_Connect::$NIK_PREMIX]][$this::$S_TOTAL_WS] = $row_ws[Worksheet_Connect::$S_COUNT_WS] * $this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_PREMIX];
                else
                    $arr_karyawan[$row_ws[Worksheet_Connect::$NIK_PREMIX]][$this::$S_TOTAL_WS] += ($row_ws[Worksheet_Connect::$S_COUNT_WS] * $this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_PREMIX]);

            }
        }
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_MAKEUP);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                if (!isset($arr_karyawan[$row_ws[Worksheet_Connect::$NIK_MAKEUP]][$this::$S_TOTAL_WS]))
                    $arr_karyawan[$row_ws[Worksheet_Connect::$NIK_MAKEUP]][$this::$S_TOTAL_WS] = $row_ws[Worksheet_Connect::$S_COUNT_WS] * $this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_MAKEUP];
                else
                    $arr_karyawan[$row_ws[Worksheet_Connect::$NIK_MAKEUP]][$this::$S_TOTAL_WS] += ($row_ws[Worksheet_Connect::$S_COUNT_WS] * $this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_MAKEUP]);

            }
        }
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_FILLING);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                if (!isset($arr_karyawan[$row_ws[Worksheet_Connect::$NIK_FILLING]][$this::$S_TOTAL_WS]))
                    $arr_karyawan[$row_ws[Worksheet_Connect::$NIK_FILLING]][$this::$S_TOTAL_WS] = $row_ws[Worksheet_Connect::$S_COUNT_WS] * $this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_FILLING];
                else
                    $arr_karyawan[$row_ws[Worksheet_Connect::$NIK_FILLING]][$this::$S_TOTAL_WS] += ($row_ws[Worksheet_Connect::$S_COUNT_WS] * $this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_FILLING]);

            }
        }

        //Stock Opname
        //Ulang 4x karena 4 Minggu
        $dari_tanggal = $dt;
        for ($i = 0; $i < 4; $i++) {
            $sampai_tanggal = date("Y-m-d", strtotime($dari_tanggal . "+ 7 day"));
            $arr_opname = $this->ws_model->checker_get_kinerja_opname($dt, $st);
            if ($arr_opname->num_rows() > 0) {
                foreach ($arr_opname->result_array() as $row_ws) {
                    if (!isset($arr_karyawan[$row_ws[Worksheet_Checker_Header::$NIK]][$this::$S_STOCK_OPNAME]))
                        $arr_karyawan[$row_ws[Worksheet_Checker_Header::$NIK]][$this::$S_STOCK_OPNAME] = ($row_ws["insentif"]);
                    else
                        $arr_karyawan[$row_ws[Worksheet_Checker_Header::$NIK]][$this::$S_STOCK_OPNAME] += ($row_ws["insentif"]);
                }
            }
            $dari_tanggal = $sampai_tanggal;
        }
        //Kebersihan
        $arr_kebersihan = $this->kebersihan_model->rm_pm_get_kinerja($dt, $st);
        if ($arr_kebersihan->num_rows() > 0) {
            foreach ($arr_kebersihan->result_array() as $row_kebersihan) {
                if($row_kebersihan[Kebersihan_Harian::$NIK]!=null){
                    if (!isset($arr_karyawan[$row_kebersihan[Kebersihan_Harian::$NIK]][$this::$S_KEBERSIHAN]))
                        $arr_karyawan[$row_kebersihan[Kebersihan_Harian::$NIK]][$this::$S_KEBERSIHAN] = ($row_kebersihan["total_bersih"])*$this::$RM_PM_MUL_KEBERSIHAN;
                    else
                        $arr_karyawan[$row_kebersihan[Kebersihan_Harian::$NIK]][$this::$S_KEBERSIHAN] += ($row_kebersihan["total_bersih"] * $this::$RM_PM_MUL_KEBERSIHAN);
                }
            }
        }

        //Kinerja
        $arr_header = ["NIK", "Barang Masuk", "Barang Keluar", "Total WS", "Kebersihan", "Stock Opname", "Total"];
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">
                 <h3 class="text-center f-aleo-bold">Total Kinerja</h3>';
        $kal .= '<table class="mastertable table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead>
                    <tbody>';

        foreach (array_keys($arr_karyawan) as $key => $value) {
            $total = 0;
            $kal .= "<tr>";
            $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $value)->row_array();
            $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $value . "</td>";
            if (isset($arr_karyawan[$value][$this::$S_BARANG_MASUK])) {
                $kal .= "<td class='text-right'>Rp. " . $this->rupiah($arr_karyawan[$value][$this::$S_BARANG_MASUK]) . "</td>";
                $total += $arr_karyawan[$value][$this::$S_BARANG_MASUK];
            } else
                $kal .= "<td class='text-center'> - </td>";

            if (isset($arr_karyawan[$value][$this::$S_BARANG_KELUAR])) {
                $kal .= "<td class='text-right'>Rp. " . $this->rupiah($arr_karyawan[$value][$this::$S_BARANG_KELUAR]) . "</td>";
                $total += $arr_karyawan[$value][$this::$S_BARANG_KELUAR];
            } else
                $kal .= "<td class='text-center'> - </td>";

            if (isset($arr_karyawan[$value][$this::$S_TOTAL_WS])) {
                $kal .= "<td class='text-right'>Rp. " . $this->rupiah($arr_karyawan[$value][$this::$S_TOTAL_WS]) . "</td>";
                $total += $arr_karyawan[$value][$this::$S_TOTAL_WS];
            } else
                $kal .= "<td class='text-center'> - </td>";

            if (isset($arr_karyawan[$value][$this::$S_KEBERSIHAN])) {
                $kal .= "<td class='text-right'>Rp. " . $this->rupiah($arr_karyawan[$value][$this::$S_KEBERSIHAN]) . "</td>";
                $total += $arr_karyawan[$value][$this::$S_KEBERSIHAN];
            } else
                $kal .= "<td class='text-center'> - </td>";

            if (isset($arr_karyawan[$value][$this::$S_STOCK_OPNAME])) {
                $kal .= "<td class='text-right'>Rp. " . $this->rupiah($arr_karyawan[$value][$this::$S_STOCK_OPNAME]) . "</td>";
                $total += $arr_karyawan[$value][$this::$S_STOCK_OPNAME];
            } else
                $kal .= "<td class='text-center'> - </td>";

            $kal .= "<td class='text-right'>Rp. " . $this->rupiah($total) . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '	</tbody>
					</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";

        //PO
        $arr_header = ["NIK", "Jenis", "Total (KG)", "Total"];
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">
                 <h3 class="text-center f-aleo-bold">Barang Masuk</h3>';
        $kal .= '<table class="mastertable table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead>
                    <tbody>';

        $arr_po = $this->po_model->checker_get_kinerja($dt, $st);
        if ($arr_po->num_rows() > 0) {
            foreach ($arr_po->result_array() as $row_po) {
                if(in_array($row_po[Purchase_Order_Checker_Header::$NIK],$this::$ARR_RM_PM_NIK_CHECKER)){
                    $kal .= "<tr>";
                        $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_po[Purchase_Order_Checker_Header::$NIK])->row_array();
                        $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row_po[Purchase_Order_Checker_Header::$NIK] . "</td>";
                        $kal .= "<td class='text-left'>" . $row_po[Material_RM_PM::$JENIS] . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($row_po[Purchase_Order_Checker_Detail::$S_TOT_QTY],3) . "</td>";
                        $kal .= "<td class='text-right'>Rp. " . $this->rupiah($row_po[Purchase_Order_Checker_Detail::$S_TOT_QTY]*$this::$ARR_RM_PM_MUL_PO_JENIS[$row_po[Material_RM_PM::$JENIS]]) . "</td>";
                    $kal .= "</tr>";
                }
            }
        }

        $kal .= '	</tbody>
					</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";

        //WS
        $arr_header = ["NIK", "Jenis", "Total (KG)", "Total"];
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">
                 <h3 class="text-center f-aleo-bold">Barang Keluar Checker</h3>';
        $kal .= '<table class="mastertable table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead>
                    <tbody>';

        //WS Checker
        $arr_ws = $this->ws_model->checker_get_kinerja($dt, $st);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                if (in_array($row_ws[Worksheet_Checker_Header::$NIK], $this::$ARR_RM_PM_NIK_CHECKER)) {
                    $kal .= "<tr>";
                        $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_ws[Worksheet_Checker_Header::$NIK])->row_array();
                        $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row_ws[Worksheet_Checker_Header::$NIK] . "</td>";
                        $kal .= "<td class='text-left'>" . $row_ws[Material_RM_PM::$JENIS] ."</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($row_ws[Worksheet_Checker_Detail::$S_TOT_QTY],3) . " (" . $row_ws[Material_RM_PM::$UNIT] . ")</td>";
                        $kal .= "<td class='text-right'>Rp. " . $this->rupiah($row_ws[Worksheet_Checker_Detail::$S_TOT_QTY] * $this::$ARR_RM_PM_MUL_WS_CHECKER_JENIS[$row_ws[Material_RM_PM::$JENIS]]) . "</td>";
                    $kal .= "</tr>";
                }
            }
        }
        $kal .= '	</tbody>
					</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";

        $arr_header = ["NIK", "WS Premix", "WS Makeup", "WS Filling", "Total"];
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">
                 <h3 class="text-center f-aleo-bold">Barang Keluar Color Matcher</h3>';
        $kal .= '<table class="mastertable table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead>
                    <tbody>';

        //WS Colour Matcher
        $arr_nik_colour_matcher = [];
        $arr_count_colour_matcher = [];
        $arr_rupiah_colour_matcher = [];
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st,Worksheet_Connect::$NIK_PREMIX);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                $arr_count_colour_matcher[$row_ws[Worksheet_Connect::$NIK_PREMIX]][Worksheet_Connect::$NIK_PREMIX] = $row_ws[Worksheet_Connect::$S_COUNT_WS];
                if (!in_array($row_ws[Worksheet_Connect::$NIK_PREMIX], $arr_nik_colour_matcher)) {
                    array_push($arr_nik_colour_matcher,$row_ws[Worksheet_Connect::$NIK_PREMIX]);
                    $arr_rupiah_colour_matcher[$row_ws[Worksheet_Connect::$NIK_PREMIX]] = $row_ws[Worksheet_Connect::$S_COUNT_WS]*$this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_PREMIX];
                }else{
                    $arr_rupiah_colour_matcher[$row_ws[Worksheet_Connect::$NIK_PREMIX]] += $row_ws[Worksheet_Connect::$S_COUNT_WS]*$this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_PREMIX];
                }
            }
        }
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_MAKEUP);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                $arr_count_colour_matcher[$row_ws[Worksheet_Connect::$NIK_MAKEUP]][Worksheet_Connect::$NIK_MAKEUP] = $row_ws[Worksheet_Connect::$S_COUNT_WS];

                if (!in_array($row_ws[Worksheet_Connect::$NIK_MAKEUP], $arr_nik_colour_matcher)) {
                    array_push($arr_nik_colour_matcher, $row_ws[Worksheet_Connect::$NIK_MAKEUP]);
                    $arr_rupiah_colour_matcher[$row_ws[Worksheet_Connect::$NIK_MAKEUP]] = $row_ws[Worksheet_Connect::$S_COUNT_WS]*$this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_MAKEUP];
                } else{
                    $arr_rupiah_colour_matcher[$row_ws[Worksheet_Connect::$NIK_MAKEUP]] += $row_ws[Worksheet_Connect::$S_COUNT_WS] * $this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_MAKEUP];
                }
            }
        }
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_FILLING);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                $arr_count_colour_matcher[$row_ws[Worksheet_Connect::$NIK_FILLING]][Worksheet_Connect::$NIK_FILLING] = $row_ws[Worksheet_Connect::$S_COUNT_WS];

                if (!in_array($row_ws[Worksheet_Connect::$NIK_FILLING], $arr_nik_colour_matcher)) {
                    array_push($arr_nik_colour_matcher, $row_ws[Worksheet_Connect::$NIK_FILLING]);
                    $arr_rupiah_colour_matcher[$row_ws[Worksheet_Connect::$NIK_FILLING]] = $row_ws[Worksheet_Connect::$S_COUNT_WS]*$this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_FILLING];
                } else{
                    $arr_rupiah_colour_matcher[$row_ws[Worksheet_Connect::$NIK_FILLING]] += $row_ws[Worksheet_Connect::$S_COUNT_WS]*$this::$ARR_RM_PM_MUL_WS_CM_JENIS[Worksheet_Connect::$NIK_FILLING];
                }
            }
        }
        for ($i = 0; $i < count($arr_nik_colour_matcher); $i++) {
            $kal .= "<tr>";
            $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $arr_nik_colour_matcher[$i])->row_array();
            $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $arr_nik_colour_matcher[$i] . "</td>";
            if (isset($arr_count_colour_matcher[$arr_nik_colour_matcher[$i]][Worksheet_Connect::$NIK_PREMIX]))
                $kal .= "<td class='text-right'>" . $this->decimal($arr_count_colour_matcher[$arr_nik_colour_matcher[$i]][Worksheet_Connect::$NIK_PREMIX]) . "</td>";
            else
                $kal .= "<td class='text-right'>" . $this->decimal(0) . "</td>";
            if (isset($arr_count_colour_matcher[$arr_nik_colour_matcher[$i]][Worksheet_Connect::$NIK_MAKEUP]))
                $kal .= "<td class='text-right'>" . $this->decimal($arr_count_colour_matcher[$arr_nik_colour_matcher[$i]][Worksheet_Connect::$NIK_MAKEUP]) . "</td>";
            else
                $kal .= "<td class='text-right'>" . $this->decimal(0) . "</td>";
            if (isset($arr_count_colour_matcher[$arr_nik_colour_matcher[$i]][Worksheet_Connect::$NIK_FILLING]))
                $kal .= "<td class='text-right'>" . $this->decimal($arr_count_colour_matcher[$arr_nik_colour_matcher[$i]][Worksheet_Connect::$NIK_FILLING]) . "</td>";
            else
                $kal .= "<td class='text-right'>" . $this->decimal(0) . "</td>";

            $kal .= "<td class='text-right'>Rp. " . $this->rupiah($arr_rupiah_colour_matcher[$arr_nik_colour_matcher[$i]]) . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '	</tbody>
					</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";

        //Stock Opname
        $arr_header = ["NIK", "Material", "Program (KG)", "Fisik (KG)", "Selisih", "Toleransi", "Tanggal", "Insentif"];
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-10">
                 <h3 class="text-center f-aleo-bold">Stock Opname</h3>';
        $kal .= '<table class="mastertable table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead>
                    <tbody>';
        $arr_opname = $this->ws_model->checker_get_last_opname($dt, $st);
        if ($arr_opname->num_rows() > 0) {
            foreach ($arr_opname->result_array() as $row_opname) {
                $text = "red-text";
                if($row_opname["selisih"] <= $row_opname["toleransi"]){
                    $text="green-text";
                }
                $kal .= "<tr>";
                    $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_opname[Material_RM_PM_Fisik::$NIK])->row_array();
                    $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row_opname[Material_RM_PM_Fisik::$NIK] . "</td>";
                    $kal .= "<td class='text-left'>" . $row_opname[Material_RM_PM::$DESCRIPTION] . "</td>";
                    $kal .= "<td class='text-right'>" . $this->decimal($row_opname[Material_RM_PM_Fisik::$QTY_PROGRAM],3) . "</td>";
                    $kal .= "<td class='text-right'>" . $this->decimal($row_opname[Material_RM_PM_Fisik::$QTY_FISIK],3) . "</td>";
                    $kal .= "<td class='text-right ". $text . "'>" . $this->decimal($row_opname["selisih"],3) . "</td>";
                    $kal .= "<td class='text-right'>" . $this->decimal($row_opname["toleransi"],3) . "</td>";
                    $kal .= "<td class='text-left'>" . $row_opname[Material_RM_PM_Fisik::$TANGGAL] . "</td>";
                    $kal .= "<td class='text-right'>Rp. " . $this->rupiah($row_opname["insentif"]) . "</td>";
                $kal .= "</tr>";
            }
        }
        $kal .= '	</tbody>
					</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";

        //Kebersihan
        $arr_header = ["Tanggal", "Gudang", "Detail", "NIK", "Kondisi"];
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= '<div class="col-sm-6">
                 <h3 class="text-center f-aleo-bold">Kebersihan</h3>';
        $kal .= '<table class="mastertable table table-bordered table-sm">
                    <thead>'. $this->gen_table_header($arr_header) .'</thead>
                    <tbody>';
        $arr_kebersihan = $this->kebersihan_model->harian_rmpm_get_by($dt, $st);
        if ($arr_kebersihan->num_rows() > 0) {
            foreach ($arr_kebersihan->result_array() as $row_kebersihan) {
                $text = "red-text";
                if ($row_kebersihan[Kebersihan_Harian::$KONDISI] == 1) { //Bersih
                    $text = "green-text";
                }
                $kal .= "<tr>";
                $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_kebersihan[Kebersihan_Harian::$NIK])->row_array();
                $kal .= "<td class='text-center'>" . $row_kebersihan[Kebersihan_Harian::$TANGGAL] . "</td>";
                $kal .= "<td class='text-center'>" . $row_kebersihan[Kebersihan_Harian::$GUDANG] . "</td>";
                $kal .= "<td class='text-center'>" . $row_kebersihan[Kebersihan_Gudang::$RAK] . "</td>";
                $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row_kebersihan[Kebersihan_Harian::$NIK] . "</td>";
                if ($row_kebersihan[Kebersihan_Harian::$KONDISI] == 1) { //Bersih
                    $kal .= "<td class='text-center " . $text . "'> Bersih </td>";
                }else{
                    $kal .= "<td class='text-center " . $text . "'> Kotor </td>";
                }
                $kal .= "</tr>";
            }
        }
        $kal .= '	</tbody>
					</table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-3"></div>';
        $kal .= "</div>";
        echo $kal;
    }

    public function rm_pm_view_harga_insentif(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $ws = $this->i_p("n");
        $arr_value = [];

        $kal = "";
        $arr_header = [
            "", "PM", "Cairan", "Tepung",  "Forklift", "Out Tep", "Out Fill", "Out Maint", "Admin",
            "CM", "Kabag", "Acc"
        ];
        $kal .= '<div class="row">';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        //Baris 1
        $arr_multiplier = [3, 15, 1.2, 1.2];
        $arr_field = [Checker_RM_PM::$S_TYPE_KEMASAN, Checker_RM_PM::$S_TYPE_CAIRAN, Checker_RM_PM::$S_TYPE_POWDER];
        $arr_po = $this->po_model->checker_get_kinerja($dt, $st, $ws);
        $arr_value_checker = [];
        $total = 0;
        if ($arr_po->num_rows() > 0) {
            foreach ($arr_po->result_array() as $row_po) {
                //Cek Apakah Subkategori dari Checker tersebut adalah RM PM Checker (13)
                if ($this->equals($row_po[Karyawan::$ID_SUB_KATEGORI], 13)) {
                    //echo $row_po[Purchase_Order_Checker_Header::$NIK] . "-" . $row_po[Material_RM_PM::$JENIS] . "-" . $row_po[Purchase_Order_Checker_Detail::$S_TOT_QTY] . "<br>";
                    $idx = array_search($row_po[Material_RM_PM::$JENIS], $arr_field);
                    if (isset($arr_value_checker[$row_po[Purchase_Order_Checker_Header::$NIK]])) {
                        $arr_value_checker[$row_po[Purchase_Order_Checker_Header::$NIK]] += $row_po[Purchase_Order_Checker_Detail::$S_TOT_QTY] * $arr_multiplier[$idx];
                    } else {
                        $arr_value_checker[$row_po[Purchase_Order_Checker_Header::$NIK]] = $row_po[Purchase_Order_Checker_Detail::$S_TOT_QTY] * $arr_multiplier[$idx];
                    }
                    $total += $row_po[Purchase_Order_Checker_Detail::$S_TOT_QTY];
                }
            }
        }

        //Pergi ke Checker RM PM dulu supaya tahu ini checker apa
        foreach ($arr_value_checker as $nik => $value) {
            $checker_rm_pm = $this->rmpmchecker_model->get(Checker_RM_PM::$TABLE_NAME, Checker_RM_PM::$NIK, $nik);
            $idx = array_search($checker_rm_pm->row_array()[Checker_RM_PM::$JENIS], $arr_field);
            $arr_value[$idx] = $value;
        }

        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">PENERIMAAN RM/PM</td>';
        for ($i = 0; $i < 11; $i++) {
            if (isset($arr_multiplier[$i])) {
                if (!isset($arr_value[$i]))
                    $arr_value[$i] = 0;
                if ($i == 3) {
                    $kal .= "<td class='text-right'>" . $this->rupiah($total) . "</>";
                } else {
                    $kal .= "<td class='text-right'>" . $this->rupiah($arr_value[$i]) . "</td>";
                }
            } else
                $kal .= "<td></td>";
        }
        $kal .= '</tr>';

        //Baris 2
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">PROSES PRODUKSI</td>';
        $kal .= $this->insert_empty_td(11);
        $kal .= '</tr>';

        //Baris 3
        $arr_multiplier = [3, 0.5, 0.5, 1.2, 12.5];
        $arr_ws = $this->ws_model->checker_get_kinerja($dt, $st, $ws);
        $arr_value_checker = [];
        $total = 0;
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                //Cek Apakah Subkategori dari Checker tersebut adalah RM PM Checker (13)
                if ($this->equals($row_ws[Karyawan::$ID_SUB_KATEGORI], 13)) {
                    $idx = array_search($row_ws[Material_RM_PM::$JENIS], $arr_field);
                    if (isset($arr_value_checker[$row_ws[Worksheet_Checker_Header::$NIK]])) {
                        $arr_value_checker[$row_ws[Worksheet_Checker_Header::$NIK]] += $row_ws[Worksheet_Checker_Detail::$S_TOT_QTY] * $arr_multiplier[$idx];
                    } else {
                        $arr_value_checker[$row_ws[Worksheet_Checker_Header::$NIK]] = $row_ws[Worksheet_Checker_Detail::$S_TOT_QTY] * $arr_multiplier[$idx];
                    }
                    $total += $row_ws[Worksheet_Checker_Detail::$S_TOT_QTY];
                }
            }
        }
        //Pergi ke Checker RM PM dulu supaya tahu ini checker apa
        foreach ($arr_value_checker as $nik => $value) {
            $checker_rm_pm = $this->rmpmchecker_model->get(Checker_RM_PM::$TABLE_NAME, Checker_RM_PM::$NIK, $nik);
            $idx = array_search($checker_rm_pm->row_array()[Checker_RM_PM::$JENIS], $arr_field);
            $arr_value[$idx] = $value;
        }

        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">PERSIAPAN</td>';
        for ($i = 0; $i < 11; $i++) {
            if (isset($arr_multiplier[$i])) {
                if (!isset($arr_value[$i]))
                    $arr_value[$i] = 0;
                if ($i == 3 || $i == 4) {
                    $kal .= "<td class='text-right'>" . $this->rupiah($total * $arr_multiplier[$i]) . "</td>";
                } else {
                    $kal .= "<td class='text-right'>" . $this->rupiah($arr_value[$i]) . "</td>";
                }
            } else
                $kal .= "<td></td>";
        }
        $kal .= '</tr>';

        //Baris 4
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">Sticker Dus</td>';
        $kal .= $this->insert_empty_td(4);
        //Hitung Dus untuk Perhitungan Out Tepung & Admin
        $this->db->select("floor(sum(" . Gudang_RM_PM_Transit::$QTY . "/" . Material::$BOX . ")) as " . Gudang_RM_PM_Transit::$QTY);
        $this->db->from(Gudang_RM_PM_Transit::$TABLE_NAME);
        $join = Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_MATERIAL_FG . "=" . Material::$TABLE_NAME . "." . Material::$ID;
        $this->db->join(Material::$TABLE_NAME,$join);
        $this->db->where(Material::$BOX . " >", 1, TRUE);
        if ($dt != null) {
            $until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' >=', $dt, TRUE);
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' <', $until, TRUE);
        }
        $row_gudang_transit=$this->db->get()->row_array();
        $kal .= '<td class="text-right">' . $this->rupiah($row_gudang_transit[Gudang_RM_PM_Transit::$QTY] * 262) . '</td>';
        $kal .= '<td class="text-right"></td>';
        $kal .= '<td class="text-right"></td>';
        $kal .= '<td class="text-right">' . $this->rupiah($row_gudang_transit[Gudang_RM_PM_Transit::$QTY] * 35) . '</td>';
        $kal .= $this->insert_empty_td(3);
        $kal .= '</tr>';

        //Baris 5
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">Sticker Pail Base Fina</td>';
        $this->db->select("floor(sum(" . Gudang_RM_PM_Transit::$QTY . "/" . Material::$BOX . ")) as " . Gudang_RM_PM_Transit::$QTY);
        $this->db->from(Gudang_RM_PM_Transit::$TABLE_NAME);
        $join = Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_MATERIAL_FG . "=" . Material::$TABLE_NAME . "." . Material::$ID;
        $this->db->join(Material::$TABLE_NAME, $join);
        $this->db->where(Material::$BOX, 1);
        $this->db->like(Material::$PRODUCT_CODE, "FINA.", 'after');
        if ($dt != null) {
            $until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' >=', $dt, TRUE);
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' <', $until, TRUE);
        }
        $row_gudang_transit = $this->db->get()->row_array();
        $kal .= $this->insert_empty_td(4);
        $kal .= '<td class="text-right">' . $this->rupiah($row_gudang_transit[Gudang_RM_PM_Transit::$QTY] * 125) . '</td>';
        $kal .= $this->insert_empty_td(6);
        $kal .= '</tr>';

        //Baris 6
        $arr_multiplier = [1000, 1000, 10000, 4000];
        //Total WS
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_PREMIX);
        $total_ws = 0;
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                $total_ws += $row_ws[Worksheet_Connect::$S_COUNT_WS];
            }
        }
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">CM Premix</td>';
        $kal .= $this->insert_empty_td(6);
        for ($i = 0; $i < 5; $i++) {
            if (isset($arr_multiplier[$i]))
                $kal .= "<td class='text-right'>" . $this->rupiah($total_ws * $arr_multiplier[$i]) . "</td>";
            else
                $kal .= "<td></td>";
        }
        $kal .= '</tr>';

        //Baris 7
        $total_ws = 0;
        $arr_multiplier = [1000, 2000, 20000, 8000];
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_MAKEUP);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                $total_ws += $row_ws[Worksheet_Connect::$S_COUNT_WS];
            }
        }
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">CM Makeup</td>';
        $kal .= $this->insert_empty_td(6);
        for ($i = 0; $i < 5; $i++) {
            if (isset($arr_multiplier[$i]))
                $kal .= "<td class='text-right'>" . $this->rupiah($total_ws * $arr_multiplier[$i]) . "</td>";
            else
                $kal .= "<td></td>";
        }
        $kal .= '</tr>';

        //Baris 8
        $total_ws = 0;
        $arr_multiplier = [1000, 2000, 20000, 8000];
        $arr_ws = $this->ws_model->colour_matcher_get_kinerja($dt, $st, Worksheet_Connect::$NIK_FILLING);
        if ($arr_ws->num_rows() > 0) {
            foreach ($arr_ws->result_array() as $row_ws) {
                $total_ws += $row_ws[Worksheet_Connect::$S_COUNT_WS];
            }
        }
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">CM Filling</td>';
        $kal .= $this->insert_empty_td(6);
        for ($i = 0; $i < 5; $i++) {
            if (isset($arr_multiplier[$i]))
                $kal .= "<td class='text-right'>" . $this->rupiah($total_ws * $arr_multiplier[$i]) . "</td>";
            else
                $kal .= "<td></td>";
        }
        $kal .= '</tr>';

        //Baris 8
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">Filling Pail</td>';
        $this->db->select("floor(sum(" . Gudang_RM_PM_Transit::$QTY . ")) as " . Gudang_RM_PM_Transit::$QTY);
        $this->db->from(Gudang_RM_PM_Transit::$TABLE_NAME);
        $join = Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_MATERIAL_FG . "=" . Material::$TABLE_NAME . "." . Material::$ID;
        $this->db->join(Material::$TABLE_NAME, $join);
        $this->db->where(Material::$BOX, 1);
        if ($dt != null) {
            $until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' >=', $dt, TRUE);
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' <', $until, TRUE);
        }
        $row_gudang_transit = $this->db->get()->row_array();
        $kal .= $this->insert_empty_td(5);
        $kal .= '<td class="text-right">' . $this->rupiah($row_gudang_transit[Gudang_RM_PM_Transit::$QTY] * 888) . '</td>';
        $kal .= $this->insert_empty_td(5);
        $kal .= '</tr>';

        //Baris 9
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">Filling Galon</td>';
        $this->db->select("floor(sum(" . Gudang_RM_PM_Transit::$QTY . ")) as " . Gudang_RM_PM_Transit::$QTY);
        $this->db->from(Gudang_RM_PM_Transit::$TABLE_NAME);
        $join = Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_MATERIAL_FG . "=" . Material::$TABLE_NAME . "." . Material::$ID;
        $this->db->join(Material::$TABLE_NAME, $join);
        $this->db->where(Material::$BOX, 4);
        if ($dt != null) {
            $until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' >=', $dt, TRUE);
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' <', $until, TRUE);
        }
        $row_gudang_transit = $this->db->get()->row_array();
        $kal .= $this->insert_empty_td(5);
        $kal .= '<td class="text-right">' . $this->rupiah($row_gudang_transit[Gudang_RM_PM_Transit::$QTY] * 392) . '</td>';
        $kal .= $this->insert_empty_td(5);
        $kal .= '</tr>';

        //Baris 10
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">Filling KG</td>';
        $this->db->select("floor(sum(" . Gudang_RM_PM_Transit::$QTY . ")) as " . Gudang_RM_PM_Transit::$QTY);
        $this->db->from(Gudang_RM_PM_Transit::$TABLE_NAME);
        $join = Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_MATERIAL_FG . "=" . Material::$TABLE_NAME . "." . Material::$ID;
        $this->db->join(Material::$TABLE_NAME, $join);
        $this->db->where(Material::$BOX, 12);
        if ($dt != null) {
            $until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' >=', $dt, TRUE);
            $this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' <', $until, TRUE);
        }
        $row_gudang_transit = $this->db->get()->row_array();
        $kal .= $this->insert_empty_td(5);
        $kal .= '<td class="text-right">' . $this->rupiah($row_gudang_transit[Gudang_RM_PM_Transit::$QTY] * 135) . '</td>';
        $kal .= $this->insert_empty_td(5);
        $kal .= '</tr>';

        //Baris 10
        $arr_multiplier = [5000, 5000, 5000, null, null, null, null, null, null, null, 2500];
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">Stock Harian</td>';
        for ($i = 0; $i < 11; $i++) {
            if (isset($arr_multiplier[$i]))
                $kal .= "<td class='text-right'>$arr_multiplier[$i]</td>";
            else
                $kal .= "<td></td>";
        }
        $kal .= '</tr>';

        //Baris 11
        $arr_multiplier = [5000, 5000, 5000, null, null, null, null, null, null, null, 2500];
        $kal .= '<tr>';
        $kal .= '<td class="text-right fw-bold table-primary">Kebersihan</td>';
        for ($i = 0; $i < 11; $i++) {
            if (isset($arr_multiplier[$i]))
                $kal .= "<td class='text-right'>$arr_multiplier[$i]</td>";
            else
                $kal .= "<td></td>";
        }
        $kal .= '</tr>';

        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    
    public function fg_get_good_issue(){
        $dt=$this->i_p("dt");
        $st=$this->i_p("st");
        $kal = "";
        $data = $this->kinerja_model->fg_get_good_issue($dt, $st);

        $arr_header=["Nomor Surat Jalan","Aksi"];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                //if(is_numeric($row[H_Good_Issue::$NOMOR_SJ])){
                $text_color = "red-text";

                if ($row[H_Good_Issue::$ID_EKSPEDISI] != null)
                    $text_color = "green-text";

                $kal .= "<tr class='" . $text_color . "'>";
                    $kal .= "<td class='text-center'>" . $row[H_Good_Issue::$NOMOR_SJ] . "</td>";
                    $kal .= "<td class='text-center'>" . 
                                '<button onclick="fill_detail_modal(' . "'" . $row[H_Good_Issue::$NOMOR_SJ] . "'" . ')" class="btn btn-outline-primary" data-mdb-ripple-color="dark">Ubah</button>' . 
                            "</td>";
                $kal .= "</tr>";
                //}
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function fg_get_good_receipt(){
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $kal = "";
        $data = $this->kinerja_model->fg_get_good_receipt($dt, $st);

        $arr_header = ["Nomor Surat Jalan", "Aksi"];

        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                //if(is_numeric($row[H_Good_Issue::$NOMOR_SJ])){
                $text_color = "red-text";

                if ($row[H_Good_Receipt::$NIK_SOPIR] != null)
                    $text_color = "green-text";

                $kal .= "<tr class='" . $text_color . "'>";
                $kal .= "<td class='text-center'>" . $row[H_Good_Receipt::$NOMOR_SJ] . "</td>";
                $kal .= "<td class='text-center'>" .
                '<button onclick="fill_detail_modal(' . "'" . $row[H_Good_Receipt::$NOMOR_SJ] . "'" . ')" class="btn btn-outline-primary" data-mdb-ripple-color="dark">Ubah</button>' .
                "</td>";
                $kal .= "</tr>";
                //}
            }
        }
        $kal .= '   </tbody>
                </table>';
        $kal .= "</div>";
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function fg_view_report(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $sub_kategori=Karyawan::$TABLE_NAME.".". Karyawan::$ID_SUB_KATEGORI;
        //$arr_header=["Jabatan","Nama","Container (KG)","LCL (KG)","Pail (KG)","Non Pail (KG)","Total (KG)",""];

        $kal = "";
        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                        <thead>
                            <tr class="table-primary">
                                <th scope="col" class="text-center">Jabatan</th>
                                <th scope="col" class="text-center">Nama</th>
                                <th scope="col" class="text-center">Container (KG)</th>
                                <th scope="col" class="text-center">LCL (KG)</th>
                                <th scope="col" class="text-center">Pail (KG)</th>
                                <th scope="col" class="text-center">Non Pail (KG)</th>
                                <th scope="col" class="text-center">Total (KG)</th>
                                <th scope="col" class="role-kepala text-center">Insentif</th>
                            </tr>
                        </thead><tbody>';

            //Outsource
            $data = $this->kinerja_model->fg_checker_calculate($tanggal_dari, $tanggal_sampai);
            /*$kal .= "<tr>";
                $kal .= "<td class='text-left'>" . $data[KINERJA_KEY_TIPE] . "</td>";
                $kal .= "<td class='text-center'> - </td>";
                $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_CONTAINER], 3) . "</td>";
                $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_LCL], 3) . "</td>";
                $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_PAIL], 3) . "</td>";
                $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL], 3) . "</td>";
                
                $insentif = 0;
                $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_DUS)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_NON_PAIL];
                $insentif += $temp;
                $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_PAIL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_PAIL];
                $insentif += $temp;
                $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_CONTAINER)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_CONTAINER];
                $insentif += $temp;
                $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_OUTSOURCE_LCL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
                $insentif += $temp;
                
                $total = $data[KINERJA_KEY_CONTAINER] + $data[KINERJA_KEY_LCL] + $data[KINERJA_KEY_PAIL] + $data[KINERJA_KEY_NON_PAIL];

                $kal .= "<td class='text-right'>" . $this->decimal($total, 3) . "</td>";
                $kal .= "<td class='role-kepala text-right'>Rp. " . $this->rupiah($insentif) . "</td>";
            $kal .= "</tr>";
            */
            //Checker
            $arr_karyawan = $this->karyawan_model->get_by($sub_kategori, KARYAWAN_SUB_KATEGORI_GENERAL_CHECKER);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row) {
                    $data = $this->kinerja_model->fg_checker_calculate($tanggal_dari, $tanggal_sampai, $row[Karyawan::$ID]);
                    $kal .= "<tr>";
                        $kal .= "<td class='text-left'>CHECKER</td>";
                        $kal .= "<td class='text-left'>" . $data[KINERJA_KEY_TIPE] . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_CONTAINER], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_LCL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_PAIL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL], 3) . "</td>";
                        
                        $insentif = 0;
                        $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_DUS)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_NON_PAIL];
                        $insentif += $temp;
                        $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_PAIL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_PAIL];
                        $insentif += $temp;
                        $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_CONTAINER)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_CONTAINER];
                        $insentif += $temp;
                        $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_CHECKER_LCL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
                        $insentif += $temp;
                        
                        $total = $data[KINERJA_KEY_CONTAINER] + $data[KINERJA_KEY_LCL] + $data[KINERJA_KEY_PAIL] + $data[KINERJA_KEY_NON_PAIL];
                        
                        $kal .= "<td class='text-right'>" . $this->decimal($total, 3) . "</td>";
                        $kal .= "<td class='role-kepala text-right'>Rp. " . $this->rupiah($insentif) . "</td>";
                    $kal .= "</tr>";
                }
            }

            //Kernet
            $arr_karyawan = $this->karyawan_model->get_by($sub_kategori, KARYAWAN_SUB_KATEGORI_KERNET);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row) {
                    $data = $this->kinerja_model->fg_checker_calculate($tanggal_dari, $tanggal_sampai, $row[Karyawan::$ID]);
                    $kal .= "<tr>";
                        $kal .= "<td class='text-left'>KERNET</td>";
                        $kal .= "<td class='text-left'>" . $data[KINERJA_KEY_TIPE] . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_CONTAINER], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_LCL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_PAIL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL], 3) . "</td>";

                        $insentif = 0;
                        $temp_lcl= $data[KINERJA_KEY_LCL];
                        if ($data[KINERJA_KEY_LCL] > 30000){
                            $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_KERNET_LESS_THAN_EQUAL_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * 30000;
                            $insentif += $temp;
                            $temp_lcl -= 30000;
                            $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_KERNET_HIGHER_THAN_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $temp_lcl;
                            $insentif += $temp;
                        }else{
                            $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_KERNET_LESS_THAN_EQUAL_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $temp_lcl;
                            $insentif += $temp;
                        }
                        
                        $total = $data[KINERJA_KEY_CONTAINER] + $data[KINERJA_KEY_LCL] + $data[KINERJA_KEY_PAIL] + $data[KINERJA_KEY_NON_PAIL];
                        
                        $kal .= "<td class='text-right'>" . $this->decimal($total, 3) . "</td>";
                        $kal .= "<td class='role-kepala text-right'>Rp. " . $this->rupiah($insentif) . "</td>";
                    $kal .= "</tr>";
                }
            }

            //Sopir
            $arr_karyawan = $this->karyawan_model->get_by($sub_kategori, KARYAWAN_SUB_KATEGORI_SOPIR);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row) {
                    $data = $this->kinerja_model->fg_checker_calculate($tanggal_dari, $tanggal_sampai, $row[Karyawan::$ID]);
                    $kal .= "<tr>";
                        $kal .= "<td class='text-left'>SOPIR</td>";
                        $kal .= "<td class='text-left'>" . $data[KINERJA_KEY_TIPE] . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_CONTAINER], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_LCL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_PAIL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL], 3) . "</td>";

                        $insentif = 0;
                        $temp_lcl = $data[KINERJA_KEY_LCL];
                        $boundary = 30000;
                        if ($data[KINERJA_KEY_LCL] > $boundary){
                            $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_SOPIR_LESS_THAN_EQUAL_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $boundary;
                            $insentif += $temp;
                            $temp_lcl -= $boundary;
                            
                            $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_SOPIR_HIGHER_THAN_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $temp_lcl;
                            $insentif += $temp;
                        }else{
                            $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_SOPIR_LESS_THAN_EQUAL_30000_KG)->row_array()[Standar_Insentif::$NOMINAL] * $temp_lcl;
                            $insentif += $temp;
                        }

                        $total = $data[KINERJA_KEY_CONTAINER] + $data[KINERJA_KEY_LCL] + $data[KINERJA_KEY_PAIL] + $data[KINERJA_KEY_NON_PAIL];

                        $kal .= "<td class='text-right'>" . $this->decimal($total, 3) . "</td>";
                        $kal .= "<td class='role-kepala text-right'>Rp. " . $this->rupiah($insentif) . "</td>";
                    $kal .= "</tr>";
                }
            }

            //Sopir FK
            $arr_karyawan = $this->karyawan_model->get_by($sub_kategori, KARYAWAN_SUB_KATEGORI_GENERAL_SOPIR_FK);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row) {
                    $data = $this->kinerja_model->fg_checker_calculate($tanggal_dari, $tanggal_sampai, $row[Karyawan::$ID]);
                    $kal .= "<tr>";
                        $kal .= "<td class='text-left'>SOPIR FORKLIFT</td>";
                        $kal .= "<td class='text-left'>" . $data[KINERJA_KEY_TIPE] . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_CONTAINER], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_LCL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_PAIL], 3) . "</td>";
                        $kal .= "<td class='text-right'>" . $this->decimal($data[KINERJA_KEY_NON_PAIL], 3) . "</td>";
                        
                        $insentif = 0;
                        $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_SOPIR_FK_DUS)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_NON_PAIL];
                        $insentif += $temp;
                        $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_SOPIR_FK_CONTAINER)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_CONTAINER];
                        $insentif += $temp;
                        $temp = $this->model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_KINERJA_GENERAL_SOPIR_FK_LCL)->row_array()[Standar_Insentif::$NOMINAL] * $data[KINERJA_KEY_LCL];
                        $insentif += $temp;

                        $total = $data[KINERJA_KEY_CONTAINER] + $data[KINERJA_KEY_LCL] + $data[KINERJA_KEY_PAIL] + $data[KINERJA_KEY_NON_PAIL];

                        $kal .= "<td class='text-right'>" . $this->decimal($total, 3) . "</td>";
                        $kal .= "<td class='role-kepala text-right'>Rp. " . $this->rupiah($insentif) . "</td>";
                    $kal .= "</tr>";
                }
            }
            $kal .= '	</tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function fg_view_kebersihan(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $arr_header=["Tanggal","Gudang","NIK","Kondisi"];

        $kal = "";
        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-2"></div>';
            $kal .= '<div class="col-sm-8">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                        <thead>'. $this->gen_table_header($arr_header) .'</thead>
                        <tbody>';

            $query=$this->kebersihan_model->get_report($tanggal_dari,$tanggal_sampai);
            if($query->num_rows()>0){
                foreach($query->result_array() as $row){
                    $kal .="<tr>";
                        $kal.="<td class='text-center'>". $row[Kebersihan_Harian::$TANGGAL] ."</td>";
                        $kal.="<td class='text-left'>". $row[Kebersihan_Gudang::$GUDANG] ." - ". $row[Kebersihan_Gudang::$RAK] ."</td>";
                        $kal .= "<td class='blue-text text-center' title='" . $row[Karyawan::$NAMA] . "'>" . $row[Karyawan::$ID] . "</td>";
                        if($row[Kebersihan_Harian::$KONDISI]==1)
                            $kal .= "<td class='text-center green-text'> BERSIH </td>";
                        else
                            $kal .= "<td class='text-center red-text'> KOTOR </td>";

                    $kal .="</tr>";
                }
            }
            $kal .= '	</tbody>
                    </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function fg_view_rekap(){
        $tanggal_dari = $this->i_p("dt");
        $tanggal_sampai = $this->i_p("st");
        $tipe = $this->i_p("t");
        $query = "";
        $arr_header=[];
        if ($this->equals($tipe, "OUT")) {
            $arr_header = ["Tanggal", "Surat Jalan", "Ekspedisi", "Tipe", "NIK Admin", "NIK Checker", "NIK Sopir", "NIK Kernet", "Qty (KG)"];
            $query = $this->goodissue_model->get_kinerja_out($tanggal_dari, $tanggal_sampai);
        } else {
            $arr_header = ["Tanggal", "Surat Jalan", "NIK Admin", "NIK Checker","NIK Sopir","NIK Kernet", "Pail (KG)", "Non Pail (KG)"];
            $query = $this->goodreceipt_model->get_kinerja_in($tanggal_dari, $tanggal_sampai);
        }

        $kal = "";
        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12">';
                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>'. $this->gen_table_header($arr_header) .'</thead>
                            <tbody>';
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {
                        if ($this->equals($tipe, "OUT")) {
                            $type = "";
                            $kal .= "<tr>";
                                $kal .= "<td class='text-center'>" . date('Y-m-d', strtotime($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN])) . "</td>";
                                $kal .= "<td class='text-center'>" . $row[H_Good_Issue::$NOMOR_SJ] . "</td>";

                                if ($row[H_Good_Issue::$ID_EKSPEDISI] != null) {
                                    $ekspedisi = $this->model->get(Ekspedisi::$TABLE_NAME, "id_ekspedisi", $row[H_Good_Issue::$ID_EKSPEDISI])->row_array();
                                    $tampil = $ekspedisi[Ekspedisi::$INISIAL];
                                    if ($ekspedisi[Ekspedisi::$TUJUAN] != NULL)
                                        $tampil .= " - " . $ekspedisi[Ekspedisi::$TUJUAN];
                                    $kal .= "<td class='text-left'>" . $tampil . "</td>";

                                    if ($this->equals($row[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_LCL))
                                        $type = KINERJA_KEY_LCL;
                                    else if ($this->equals($row[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_CONTAINER))
                                        $type = KINERJA_KEY_CONTAINER;
                                    else if ($this->equals($row[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_SIRCLO))
                                        $type = KINERJA_KEY_SIRCLO;
                                    $kal .= "<td class='text-center'>$type</td>";
                                } else {
                                    $kal .= "<td class='text-center'> - </td>";
                                    $kal .= "<td class='text-center'> - </td>";
                                }
                                if ($row[H_Good_Issue::$NIK_ADMIN] != null) {
                                    $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK_ADMIN])->row()->k_nama;
                                    $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Issue::$NIK_ADMIN] . "</td>";
                                } else {
                                    $kal .= "<td class='text-center'> - </td>";
                                }
                                
                                $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK])->row()->k_nama;
                                $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Issue::$NIK] . "</td>";

                                if ($row[H_Good_Issue::$NIK_SOPIR] != null) {
                                    $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK_SOPIR])->row()->k_nama;
                                    $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Issue::$NIK_SOPIR] . "</td>";
                                } else {
                                    $kal .= "<td class='text-center'> - </td>";
                                }
                                if ($row[H_Good_Issue::$NIK_KERNET] != null) {
                                    $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Issue::$NIK_KERNET])->row()->k_nama;
                                    $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Issue::$NIK_KERNET] . "</td>";
                                } else {
                                    $kal .= "<td class='text-center'> - </td>";
                                }

                                

                                $total_minus=0;
                                $query_minus = $this->goodissue_model->get_kinerja_in($row[H_Good_Issue::$NIK], $row[H_Good_Issue::$NOMOR_SJ]);
                                if($query_minus->num_rows()>0){
                                    $total_minus=$query_minus->row_array()[H_Good_Receipt::$S_TOT_QTY];
                                }
                                $kal .= "<td class='text-right'>" . $this->decimal($row[H_Good_Issue::$S_TOT_QTY]-$total_minus,3). "</td>";
                            $kal .= "</tr>";
                        } else {
                            $kal .= "<tr>";
                                $kal .= "<td class='text-center'>" . date('Y-m-d', strtotime($row[H_Good_Receipt::$TANGGAL])) . "</td>";
                                $kal .= "<td class='text-center'>" . $row[H_Good_Receipt::$NOMOR_SJ] . "</td>";
                                if ($row[H_Good_Receipt::$NIK_ADMIN] != null) {
                                    $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Receipt::$NIK_ADMIN])->row()->k_nama;
                                    $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Receipt::$NIK_ADMIN] . "</td>";
                                } else {
                                    $kal .= "<td class='text-center'> - </td>";
                                }

                                $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Receipt::$NIK])->row()->k_nama;
                                $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Receipt::$NIK] . "</td>";

                                if ($row[H_Good_Receipt::$NIK_SOPIR] != null) {
                                    $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Receipt::$NIK_SOPIR])->row()->k_nama;
                                    $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Receipt::$NIK_SOPIR] . "</td>";
                                } else {
                                    $kal .= "<td class='text-center'> - </td>";
                                }
                                if ($row[H_Good_Receipt::$NIK_KERNET] != null) {
                                    $nama = $this->karyawan_model->get_by(Karyawan::$ID, $row[H_Good_Receipt::$NIK_KERNET])->row()->k_nama;
                                    $kal .= "<td class='blue-text text-center' title='" . $nama . "'>" . $row[H_Good_Receipt::$NIK_KERNET] . "</td>";
                                } else {
                                    $kal .= "<td class='text-center'> - </td>";
                                }
                                
                                $query_minus = $this->goodreceipt_model->get_kinerja_out($row[H_Good_Receipt::$NIK], $row[H_Good_Receipt::$NOMOR_SJ]);
                                $total_minus=0;
                                if($this->equals($row[H_Good_Receipt::$S_JENIS],KINERJA_KEY_PAIL)){
                                    if ($query_minus->num_rows() > 0) {
                                        foreach ($query_minus->result_array() as $row_minus)
                                            if ($this->equals($row_minus[H_Good_Receipt::$S_JENIS], KINERJA_KEY_PAIL))
                                                $total_minus = $row_minus[H_Good_Issue::$S_TOT_QTY];
                                    }
                                    $kal .= "<td class='text-right'>" . $this->decimal($row[H_Good_Receipt::$S_TOT_QTY]-$total_minus,3) . "</td>";
                                    $kal .= "<td class='text-right'> - </td>";
                                }
                                if ($this->equals($row[H_Good_Receipt::$S_JENIS], KINERJA_KEY_NON_PAIL)){
                                    if ($query_minus->num_rows() > 0) {
                                        foreach ($query_minus->result_array() as $row_minus)
                                            if ($this->equals($row_minus[H_Good_Receipt::$S_JENIS], KINERJA_KEY_NON_PAIL))
                                                $total_minus = $row_minus[H_Good_Issue::$S_TOT_QTY];
                                    }
                                    $kal .= "<td class='text-right'> - </td>";
                                    $kal .= "<td class='text-right'>" . $this->decimal($row[H_Good_Receipt::$S_TOT_QTY] - $total_minus,3) . "</td>";
                                }
                            $kal .= "</tr>";
                        }
                    }
                }
                $kal .= '	</tbody>
                        </table>';
            $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function fg_view_detail_sj_out(){
        $data = [];
        $sj = $this->i_p("n");
        $gi = $this->get_good_issue_by_sj($sj);
        if ($gi->num_rows() > 0) {
            $h_good_issue = $gi->row_array();
            $data["tipe_ekspedisi"] = $h_good_issue[H_Good_Issue::$EKSPEDISI];
            if ($h_good_issue[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN] != null)
                $data["tanggal"] = date("Y-m-d", strtotime($h_good_issue[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]));
            else
                $data["tanggal"] = "";

            //array ekspedisi
            $kal = "";
            $arr_ekspedisi = $this->model->get(Ekspedisi::$TABLE_NAME, null, null, Ekspedisi::$INISIAL);
            $kal .= '<option value=-1>--PILIH EKSPEDISI--</option>';
            $kal_tujuan = '<option value=-1>--PILIH TUJUAN--</option>';
            if ($arr_ekspedisi->num_rows() > 0) {
                $ctr = 0;
                $arr_already_exists = [];
                foreach ($arr_ekspedisi->result_array() as $row2) {
                    $selected = "";
                    $tipe = "";
                    if ($row2[Ekspedisi::$ID] == $h_good_issue[H_Good_Issue::$ID_EKSPEDISI])$selected = "selected";
                    $tampilan = $row2[Ekspedisi::$INISIAL];
                    
                    if ($row2[Ekspedisi::$IS_TYPE_LCL]) {
                        $tipe = KINERJA_KEY_LCL;
                        $temp = $row2[Ekspedisi::$NAMA_EKSPEDISI] . "-" . $tipe;

                       if (in_array($temp, $arr_already_exists)) {
                            $idx_found = array_search($temp, $arr_already_exists);
                            $kal_tujuan .= "<option $selected class='opt_tujuan arr_" . $idx_found . "' value='" . $row2[Ekspedisi::$ID] . "'>" . $row2[Ekspedisi::$TUJUAN]  . "</option>";
                        } else {
                            $kal .= "<option $selected class='ekspedisi_" . $tipe . "' value=$ctr>" . $tampilan . "</option>";
                            $kal_tujuan .= "<option $selected class='opt_tujuan arr_" . $ctr . "' value='" . $row2[Ekspedisi::$ID] . "'>" . $row2[Ekspedisi::$TUJUAN]  . "</option>";
                        
                            array_push($arr_already_exists, $temp);
                            $ctr++;
                        }
                    }
                    if ($row2[Ekspedisi::$IS_TYPE_CONTAINER]) {
                        $tipe = KINERJA_KEY_CONTAINER;
                        $temp = $row2[Ekspedisi::$NAMA_EKSPEDISI] . "-" . $tipe;

                         if(in_array($temp, $arr_already_exists)){
                            $idx_found = array_search($temp, $arr_already_exists);
                            $kal_tujuan .= "<option $selected class='opt_tujuan arr_" . $idx_found . "' value='" . $row2[Ekspedisi::$ID] . "'>" . $row2[Ekspedisi::$TUJUAN]  . "</option>";
                        }else{
                            $kal .= "<option $selected class='ekspedisi_" . $tipe . "' value=$ctr>" . $tampilan . "</option>";
                            $kal_tujuan .= "<option $selected class='opt_tujuan arr_" . $ctr . "' value='" . $row2[Ekspedisi::$ID] . "'>" . $row2[Ekspedisi::$TUJUAN]  . "</option>";
                       
                            array_push($arr_already_exists, $temp);
                            $ctr++;

                        }
                    }
                    if ($row2[Ekspedisi::$IS_TYPE_SIRCLO]) {
                        $tipe = KINERJA_KEY_SIRCLO;
                        $temp = $row2[Ekspedisi::$NAMA_EKSPEDISI] . "-" . $tipe;

                        if (in_array($temp, $arr_already_exists)) {
                            $idx_found = array_search($temp, $arr_already_exists);
                            $kal_tujuan .= "<option $selected class='opt_tujuan arr_" . $idx_found . "' value='" . $row2[Ekspedisi::$ID] . "'>" . $row2[Ekspedisi::$TUJUAN]  . "</option>";
                        }else{
                            $kal .= "<option $selected class='ekspedisi_" . $tipe . "' value=$ctr>" . $tampilan . "</option>";
                            $kal_tujuan .= "<option $selected class='opt_tujuan arr_" . $ctr . "' value='" . $row2[Ekspedisi::$ID] . "'>" . $row2[Ekspedisi::$TUJUAN]  . "</option>";
                        
                            array_push($arr_already_exists, $temp);
                            $ctr++;
                        }

                    }
                }
            }
            $data['opt_ekspedisi'] = $kal;
            $data['opt_tujuan'] = $kal_tujuan;

            //array supir
            $kal = "";
            $kal .= '<option value=-1>TIDAK ADA</option>';
            $arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori", KARYAWAN_SUB_KATEGORI_SOPIR);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row2) {
                    $selected = "";
                    if ($row2[Karyawan::$ID] == $h_good_issue[H_Good_Issue::$NIK_SOPIR])
                    $selected = "selected";
                    $karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $row2[Karyawan::$ID])->row();
                    $kal .= "<option $selected value='" . $row2[Karyawan::$ID] . "'>" . $karyawan->k_nama . "</option>";
                }
            }
            $data['opt_sopir'] = $kal;

            //array kernet
            $kal = "";
            $kal .= '<option value=-1>TIDAK ADA</option>';
            $arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori", KARYAWAN_SUB_KATEGORI_KERNET);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row2) {
                    $selected = "";
                    if ($row2[Karyawan::$ID] == $h_good_issue[H_Good_Issue::$NIK_KERNET])
                    $selected = "selected";
                    $karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $row2[Karyawan::$ID])->row();
                    $kal .= "<option $selected value='" . $row2[Karyawan::$ID] . "'>" . $karyawan->k_nama . "</option>";
                }
            }
            $data['opt_kernet'] = $kal;
        }
        echo json_encode($data);
    }
    public function get_good_issue_by_sj($nomor_sj){
        $arr = [
            H_Good_Issue::$KODE_REVISI, H_Good_Issue::$NIK_SOPIR, H_Good_Issue::$NIK_KERNET,
            H_Good_Issue::$TANGGAL_CONFIRM_ADMIN, H_Good_Issue::$ID_EKSPEDISI, H_Good_Issue::$NIK,
            Material::$KODE_BARANG, Material::$KEMASAN, Material::$WARNA, Material::$DESCRIPTION,
            H_Good_Issue::$TABLE_NAME . "." . H_Good_Issue::$ID . " " . H_Good_Issue::$ID, H_Good_Issue::$NOMOR_SJ,
            H_Good_Issue::$STO, H_Good_Issue::$EKSPEDISI, H_Good_Issue::$QTY, H_Good_Issue::$TANGGAL,
            Material::$TABLE_NAME . "." . Material::$PRODUCT_CODE . " " . Material::$PRODUCT_CODE,
            D_Good_Issue::$DUS, D_Good_Issue::$TIN, D_Good_Issue::$ID_GUDANG
        ];
        $this->db->select($arr);
        $this->db->from(H_Good_Issue::$TABLE_NAME);
        $temp = H_Good_Issue::$TABLE_NAME . "." . H_Good_Issue::$ID . "=" . D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_H_GOOD_ISSUE;
        $this->db->join(D_Good_Issue::$TABLE_NAME, $temp);
        $temp = D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_MATERIAL . "=" . Material::$TABLE_NAME . "." . Material::$ID;
        $this->db->join(Material::$TABLE_NAME, $temp);
        $temp = H_Good_Issue::$TABLE_NAME . ".is_deleted=0";
        $this->db->where($temp);
        $this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);
        return $this->db->get();
        /*return $this->db->query("
			select kode_revisi,nik_sopir,nik_kernet,tanggal_confirm_admin,id_ekspedisi,nik,kode_barang,kemasan,warna,
			description,h.id_h_good_issue id,nomor_sj,sto,ekspedisi,tanggal,qty,d.product_code p_code,dus,
			tin,id_gudang
			from h_good_issue h,d_good_issue d,material m
			where h.id_h_good_issue=d.id_h_good_issue and
				h.is_deleted=0 and (nomor_sj='$id_sj') and
				m.product_code=d.product_code");*/
    }
    public function fg_view_detail_sj_in(){
        $data = [];
        $nomor_sj = $this->i_p("n");
        $temp = $this->get_good_receipt_by_sj($nomor_sj);
        if ($temp->num_rows() > 0) {
            $h_good_receipt = $temp->row_array();
            if ($h_good_receipt[H_Good_Receipt::$TANGGAL_CONFIRM_ADMIN] != null)
                $data["tanggal"] = date("Y-m-d", strtotime($h_good_receipt[H_Good_Receipt::$TANGGAL_CONFIRM_ADMIN]));
            else
                $data["tanggal"] = "";

            //array supir
            $kal = "";
            $kal .= '<option value=-1>TIDAK ADA</option>';
            $arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori", KARYAWAN_SUB_KATEGORI_SOPIR);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row2) {
                    $selected = "";
                    if ($row2[Karyawan::$ID] == $h_good_receipt[H_Good_Receipt::$NIK_SOPIR])
                        $selected = "selected";
                    $karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $row2[Karyawan::$ID])->row();
                    $kal .= "<option $selected value='" . $row2[Karyawan::$ID] . "'>" . $karyawan->k_nama . "</option>";
                }
            }
            $data['opt_sopir'] = $kal;

            //array kernet
            $kal = "";
            $kal .= '<option value=-1>TIDAK ADA</option>';
            $arr_karyawan = $this->karyawan_model->get_by("karyawan.id_sub_kategori", KARYAWAN_SUB_KATEGORI_KERNET);
            if ($arr_karyawan->num_rows() > 0) {
                foreach ($arr_karyawan->result_array() as $row2) {
                    $selected = "";
                    if ($row2[Karyawan::$ID] == $h_good_receipt[H_Good_Receipt::$NIK_KERNET])
                        $selected = "selected";
                    $karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $row2[Karyawan::$ID])->row();
                    $kal .= "<option $selected value='" . $row2[Karyawan::$ID] . "'>" . $karyawan->k_nama . "</option>";
                }
            }
            $data['opt_kernet'] = $kal;
        }
        echo json_encode($data);
    }
    public function get_good_receipt_by_sj($nomor_sj){
        $arr = [
            H_Good_Receipt::$KODE_REVISI, H_Good_Receipt::$NIK, Material::$KODE_BARANG, Material::$KEMASAN, Material::$WARNA,
            Material::$DESCRIPTION, H_Good_Receipt::$TABLE_NAME . "." . H_Good_Receipt::$ID . " " . H_Good_Receipt::$ID,
            H_Good_Receipt::$NOMOR_SJ, H_Good_Receipt::$STO, H_Good_Receipt::$TANGGAL, H_Good_Receipt::$QTY,
            Material::$TABLE_NAME . "." . Material::$PRODUCT_CODE . " " . Material::$PRODUCT_CODE,
            D_Good_Receipt::$DUS, D_Good_Receipt::$TIN, D_Good_Receipt::$ID_GUDANG, H_Good_Receipt::$NIK_SOPIR, H_Good_Receipt::$NIK_KERNET,
            H_Good_Receipt::$TANGGAL_CONFIRM_ADMIN
        ];
        $this->db->select($arr);
        $this->db->from(H_Good_Receipt::$TABLE_NAME);
        $temp = H_Good_Receipt::$TABLE_NAME . "." . H_Good_Receipt::$ID . "=" . D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_H_GOOD_RECEIPT;
        $this->db->join(D_Good_Receipt::$TABLE_NAME, $temp);
        $temp = D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_MATERIAL . "=" . Material::$TABLE_NAME . "." . Material::$ID;
        $this->db->join(Material::$TABLE_NAME, $temp);
        $temp = H_Good_Receipt::$TABLE_NAME . ".is_deleted=0";
        $this->db->where($temp);
        $this->db->where(H_Good_Receipt::$NOMOR_SJ, $nomor_sj);
        return $this->db->get();
        /*return $this->db->query("
			select kode_revisi,nik,kode_barang,kemasan,warna,description,h.id_h_good_receipt id,nomor_sj,sto,tanggal,
			qty,d.product_code p_code,dus,tin,id_gudang
			from h_good_receipt h,d_good_receipt d,material m
			where h.id_h_good_receipt=d.id_h_good_receipt and
				h.is_deleted=0 and (nomor_sj='$id_sj') and
				m.product_code=d.product_code");
        */
    }
    public function fg_kinerja_out_view_sisa(){
        $kal = "";
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $data = $this->goodissue_model->get_kinerja_sisa($dt, $st);
        $arr_header=["Tanggal","Nomor SJ","NIK"];

        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-1"></div>';
            $kal .= '<div class="col-sm-10">';
                $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                            <thead>'. $this->gen_table_header($arr_header) .'</thead>
                            <tbody>';
                foreach ($data->result_array() as $row_gi) {
                    if(is_numeric($row_gi[H_Good_Issue::$NOMOR_SJ])){
                        $kal .= "<tr>";
                            $kal .= "<td class='text-center'>" . $row_gi[H_Good_Issue::$TANGGAL] . "</td>";
                            $kal .= "<td class='text-center'>" . $row_gi[H_Good_Issue::$NOMOR_SJ] . "</td>";
                            $karyawan = $this->karyawan_model->get(Karyawan::$TABLE_NAME, Karyawan::$ID, $row_gi[H_Good_Issue::$NIK])->row_array();
                            $kal .= "<td class='text-center blue-text' title='" . $karyawan[Karyawan::$NAMA] . "'>" . $row_gi[H_Good_Issue::$NIK] . "</td>";
                            
                        $kal .= "</tr>";
                    }
                }
                $kal .= '   </tbody>
                        </table>';
            $kal .= "</div>";
            $kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    public function fg_ekspedisi_report(){
        $kal = "";
        $dt = $this->i_p("dt");
        $st = $this->i_p("st");
        $query = $this->model->get_good_issue_by_tanggal($dt, $st);
        $arr_header = ["Tanggal", "Surat Jalan", "Ekspedisi", "FG Code (Lama)", "FG Code (Baru)", "Kode Produk", "Dus", "Tin", "Jenis", "Qty"];

        $kal .= '<div class="row">';
            $kal .= '<div class="col-sm-12">';
            $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                        <thead>'. $this->gen_table_header($arr_header) .'</thead>
                        <tbody>';
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    if ($row[H_Good_Issue::$ID_EKSPEDISI] != null) {
                        $ekspedisi = $this->model->get(Ekspedisi::$TABLE_NAME, Ekspedisi::$ID, $row[H_Good_Issue::$ID_EKSPEDISI])->row_array();
                        $ctr = 0;
                        $data = [];
                        $qry = $this->model->get(D_Good_Issue::$TABLE_NAME, H_Good_Issue::$ID, $row[H_Good_Issue::$ID], null, null, 1);
                        if ($qry->num_rows() > 0) {
                            foreach ($qry->result_array() as $row_detail) {
                                $material = $this->model->get(Material::$TABLE_NAME, Material::$ID, $row_detail[D_Good_Issue::$ID_MATERIAL])->row_array();
                                $is_same = -1;
                                for ($i = 0; $i < $ctr; $i++) {
                                    if ($this->equals($row_detail[D_Good_Issue::$ID_MATERIAL], $data[$i][Material::$ID]))
                                        $is_same = $i;
                                }
                                if ($is_same == -1) {
                                    $data[$ctr][H_Good_Issue::$TANGGAL_CONFIRM_ADMIN] = date('Y-m-d', strtotime($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]));
                                    $data[$ctr][H_Good_Issue::$NOMOR_SJ] = $row[H_Good_Issue::$NOMOR_SJ];
                                    $tampilan = $ekspedisi[Ekspedisi::$NAMA_EKSPEDISI];
                                    if ($ekspedisi[Ekspedisi::$TUJUAN] != null)
                                        $tampilan .= " - " . $ekspedisi[Ekspedisi::$TUJUAN];
                                    $data[$ctr][H_Good_Issue::$EKSPEDISI] = $tampilan;
                                    $data[$ctr][Material::$PRODUCT_CODE] = $material[Material::$PRODUCT_CODE];
                                    $data[$ctr][Material::$ID] = $row_detail[D_Good_Issue::$ID_MATERIAL];
                                    $data[$ctr][Material::$ID_NEW] = $material[Material::$ID_NEW];

                                    $data[$ctr][D_Good_Issue::$TIN] = $row_detail[D_Good_Issue::$TIN];
                                    $data[$ctr][D_Good_Issue::$DUS] = $row_detail[D_Good_Issue::$DUS];
                                    if ($material[Material::$BOX] > 1) {
                                        $data[$ctr][D_Good_Issue::$DUS] += intdiv($data[$ctr][D_Good_Issue::$TIN], $material[Material::$BOX]);
                                        $data[$ctr][D_Good_Issue::$TIN] = $data[$ctr][D_Good_Issue::$TIN] % $material[Material::$BOX];
                                    }
                                } else {
                                    $data[$is_same][D_Good_Issue::$TIN] += $row_detail[D_Good_Issue::$TIN];
                                    if ($material[Material::$BOX] > 1) {
                                        $data[$is_same][D_Good_Issue::$DUS] += $row_detail[D_Good_Issue::$DUS] + intdiv($data[$is_same][D_Good_Issue::$TIN], $material[Material::$BOX]);
                                        $data[$is_same][D_Good_Issue::$TIN] = $data[$is_same][D_Good_Issue::$TIN] % $material[Material::$BOX];
                                    } else
                                        $data[$is_same][D_Good_Issue::$DUS] += $row_detail[D_Good_Issue::$DUS];
                                }

                                if ($is_same == -1)
                                    $ctr++;
                            }
                        }
                        for ($i = 0; $i < $ctr; $i++) {
                            //Pengecekan Revisi GR
                            $data_gr = $this->model->good_receipt_get_rev_good_issue_by_sj($data[$i][H_Good_Issue::$NOMOR_SJ]);
                            if ($data_gr->num_rows() > 0) {
                                foreach ($data_gr->result_array() as $row_gr) {
                                    if ($this->equals($row_gr[D_Good_Receipt::$ID_MATERIAL], $data[$i][Material::$ID])) {
                                        $material = $this->model->get(Material::$TABLE_NAME, Material::$ID, $row_gr[D_Good_Receipt::$ID_MATERIAL])->row_array();

                                        //Jadikan ke tin semua baru dikurangi
                                        $tin = ($data[$i][D_Good_Issue::$DUS] * $material[Material::$BOX]) + $data[$i][D_Good_Issue::$TIN];
                                        $tin_gr = ($row_gr[D_Good_Receipt::$DUS] * $material[Material::$BOX]) + $row_gr[D_Good_Receipt::$TIN];

                                        $tin -= $tin_gr;

                                        $data[$i][D_Good_Issue::$TIN] = $tin;
                                        if ($material[Material::$BOX] > 1) {
                                            $data[$i][D_Good_Issue::$DUS] = intdiv($data[$i][D_Good_Issue::$TIN], $material[Material::$BOX]);
                                            $data[$i][D_Good_Issue::$TIN] = $data[$i][D_Good_Issue::$TIN] % $material[Material::$BOX];
                                        }
                                        //	$data[$i][D_Good_Issue::$DUS] = $row_detail[D_Good_Issue::$DUS];
                                    }
                                }
                            }

                            $material = $this->model->get(Material::$TABLE_NAME, Material::$ID, $data[$i][Material::$ID])->row_array();
                            //Perhitungan Ekspedisi
                            if ($ekspedisi[Ekspedisi::$IS_SCALE_M3]) {
                                $m3 = 0;
                                if ($material[Material::$BOX] > 1) { //dus x kubik
                                    $m3 = ($data[$i][D_Good_Issue::$DUS] + (intdiv($data[$i][D_Good_Issue::$TIN], $material[Material::$BOX]))) * $material[Material::$KUBIK];
                                } else { //pil
                                    if ($data[$i][D_Good_Issue::$TIN] != 0)
                                        $m3 = ($data[$i][D_Good_Issue::$TIN]) * $material[Material::$KUBIK];
                                    else
                                        $m3 = ($data[$i][D_Good_Issue::$DUS]) * $material[Material::$KUBIK];
                                }
                                $data[$i]["QTY"] = $m3 / 1000;
                            } else if ($ekspedisi[Ekspedisi::$IS_SCALE_KG]) {
                                $kg = 0;
                                if ($material[Material::$BOX] > 1) { //dus+tin x box
                                    $kg = (($data[$i][D_Good_Issue::$DUS] * $material[Material::$BOX]) + $data[$i][D_Good_Issue::$TIN]) * $material[Material::$GROSS];
                                } else { //pil
                                    $kg = ($data[$i][D_Good_Issue::$TIN]) * $material[Material::$GROSS];
                                }
                                $data[$i]["QTY"] = $kg;
                            } else if ($ekspedisi[Ekspedisi::$IS_SCALE_COLLIE]) {
                                $collie = (($data[$i][D_Good_Issue::$DUS] * $material[Material::$BOX]) + $data[$i][D_Good_Issue::$TIN]);
                                $data[$i]["QTY"] = $collie;
                            } else {
                                $data[$ctr]["QTY"] = 0;
                            }

                            $kal .= "<tr>";
                                $kal .= "<td class='text-center'>" . $data[$i][H_Good_Issue::$TANGGAL_CONFIRM_ADMIN] . "</td>";
                                $kal .= "<td class='text-center'>" . $data[$i][H_Good_Issue::$NOMOR_SJ] . "</td>";
                                $kal .= "<td class='text-left'>" . $data[$i][H_Good_Issue::$EKSPEDISI] . "</td>";
                                $kal .= "<td class='text-center'>" . $data[$i][Material::$ID] . "</td>";
                                $kal .= "<td class='text-center'>" . $data[$i][Material::$ID_NEW] . "</td>";
                                $kal .= "<td class='text-center'>" . $data[$i][Material::$PRODUCT_CODE] . "</td>";
                                $kal .= "<td class='text-right'>" . $data[$i][D_Good_Issue::$DUS] . "</td>";
                                $kal .= "<td class='text-right'>" . $data[$i][D_Good_Issue::$TIN] . "</td>";
                            if ($ekspedisi[Ekspedisi::$IS_SCALE_M3]) {
                                $kal .= "<td class='text-center'> Kubik </td>";
                                $kal .= "<td class='text-right'>" . $this->decimal($data[$i]["QTY"], 3) . "</td>";
                            } else if ($ekspedisi[Ekspedisi::$IS_SCALE_KG]) {
                                $kal .= "<td class='text-center'> KG </td>";
                                $kal .= "<td class='text-right'>" . $this->decimal($data[$i]["QTY"], 3) . "</td>";
                            } else if ($ekspedisi[Ekspedisi::$IS_SCALE_COLLIE]) {
                                $kal .= "<td class='text-center'> Collie </td>";
                                $kal .= "<td class='text-right'>" . $this->decimal($data[$i]["QTY"], 3) . "</td>";
                            } else {
                                $kal .= "<td class='text-center'> - </td>";
                                $kal .= "<td class='text-right'> - </td>";
                            }
                            $kal .= "</tr>";
                        }
                    }
                }
            }
            $kal .= '	</tbody>
                    </table>';
            $kal .= "</div>";
        $kal .= "</div>";
        echo $kal;
    }
    public function fg_insert_good_issue(){
        $sopir = $this->i_p("ns");
        $kernet = $this->i_p("nk");
        $nomor_sj = $this->i_p("n");
        $ekspedisi = $this->i_p("ie");
        $tipe_ekspedisi = $this->i_p("te");
        $tanggal = $this->i_p("t");
        echo MESSAGE[$this->kinerja_model->fg_insert_good_issue($nomor_sj, $tanggal, $ekspedisi, $tipe_ekspedisi, $sopir, $kernet)];
    }
    public function fg_insert_good_receipt(){
        $sopir = $this->i_p("ns");
        $kernet = $this->i_p("nk");
        $nomor_sj = $this->i_p("n");
        $tanggal = $this->i_p("t");
        echo MESSAGE[$this->kinerja_model->fg_insert_good_receipt($nomor_sj, $tanggal, $sopir, $kernet)];
    }
}
