<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Vendor_Controller extends Library_Controller {


    public function upload(){
        $config[CONFIG_UPLOAD_PATH] = './asset/upload/';
        $config[CONFIG_ALLOWED_TYPES] = 'xlsx';
        $config[CONFIG_MAX_SIZE] = '5000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        $txt_file = "txt_upl_vendor";
        if ($this->upload->do_upload($txt_file)) {
            $upload_data = $this->upload->data();
            $filename = $upload_data[CONFIG_FILE_PATH] . '/' . $upload_data[CONFIG_FILE_NAME];

            $message = $this->update_db($filename);
            if (isset(MESSAGE[$message]))
                echo MESSAGE[$message];
            else
                echo $message;

            unlink($filename);
        } else {
            $data = $this->upload->display_errors();
            echo $this->get_message($data);
        }
    }
    public function update_db($filename){
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

        $row_header = 1;

        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $col = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            $cell = $objPHPExcel->getActiveSheet()->getCell($cell);

            if (PHPExcel_Shared_Date::isDateTime($cell)) {
                $cellValue = $cell->getValue();
                $dateValue = PHPExcel_Shared_Date::ExcelToPHP($cellValue);
                $data_value =  date('Y-m-d', $dateValue);
            }
            //The header will/should be in row 1 only. of course, this can be modified to suit your need.
            if ($row == $row_header) {
                $header[$row][$col] = $data_value;
            } else {
                $arr_data[$row][$col] = $data_value;
            }
        }

        $data = NULL;
        //send the data in an array format
        if (isset($header) && isset($arr_data)) {
            $data['header'] = $header;
            $data['values'] = $arr_data;
        }
        return $this->vendor_model->update_batch($data);
    }

    public function get(){
        $id = $this->i_p("iv");

        $qry = $this->vendor_model->get(Vendor::$TABLE_NAME, Vendor::$ID, $id);
        $num_rows = $qry->num_rows();
        $temp = [];
        if ($qry->num_rows() > 0) {
            $vendor = $qry->row_array();
            
        }
        $vendor["num_rows"] = $num_rows;
        echo json_encode($vendor);

    }
    public function get_all(){
        $query=$this->vendor_model->get(Vendor::$TABLE_NAME);
        $temp=[];
        $ctr=0;
        if($query->num_rows()>0){
            foreach($query->result_array() as $row){
                $temp[$ctr] = [];
                $vendor = $row;
                $temp[$ctr][Vendor::$NAMA] = $vendor[Vendor::$NAMA];
                $temp[$ctr][Vendor::$ID] = $vendor[Vendor::$ID];
                $ctr++;
            }
        }
            
        echo json_encode($temp);

    }

    public function view_insert(){
        $kal = "";
        $kal .= "<h1 class='f-aleo-bold text-center'>INPUT VENDOR</h1>";
		$kal .= "<h6 class='f-aleo-bold text-center red-text'>* : Wajib Diisi</h6>";

        $kal .= "<br>";
        $kal .= '<div class="row" style="margin-right:1%">';
            $kal .= '<div class="col-sm-2"></div>';
            $kal .= '<div class="col-sm-8"><form id="form_insert">';
                
                //GL Datalist
                $data_list_gl = "<datalist id='list_gl'>";
                $data_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, null, null, null, null, false);
                if ($data_gl->num_rows() > 0) {
                    foreach ($data_gl->result_array() as $row_gl) {
                        $data_list_gl .= "<option value=" . $row_gl[General_Ledger::$ID] . ">" . $row_gl[General_Ledger::$DESKRIPSI] . "</option>";
                    }
                }
                $data_list_gl .= "</datalist>";

                //Payterm Datalist
                $data_list_payterm = "<datalist id='list_payterm'>";
                $data_payterm = $this->payterm_model->get(Payterm::$TABLE_NAME);
                if ($data_payterm->num_rows() > 0) {
                    foreach ($data_payterm->result_array() as $row_payterm) {
                        $data_list_payterm .= "<option value=" . $row_payterm[Payterm::$ID] . ">" . $row_payterm[Payterm::$DESKRIPSI] . "</option>";
                    }
                }
                
                $data_list_payterm .= "</datalist>";

                //Country Datalist
                $data_list_country = "<datalist id='list_country'>";
                $data_country = $this->country_model->get(Country::$TABLE_NAME, null, null, null, null, false);
                if ($data_country->num_rows() > 0) {
                    foreach ($data_country->result_array() as $row_country) {
                        $data_list_country .= "<option value=" . $row_country[Country::$ID] . ">" . $row_country[Country::$NAMA_COUNTRY] . "</option>";
                    }
                }
                $data_list_country .= "</datalist>";

                //Tipe Vendor
                $select = '<select id="txt_tipe" style="font-size:larger">';
                $data_tipe = $this->vendor_model->get(Vendor_Tipe::$TABLE_NAME, null, null, null, null, false);
                if($data_tipe->num_rows()>0){
                    foreach($data_tipe->result_array() as $row_tipe){
                        $select .= "<option value=" . $row_tipe[Vendor_Tipe::$ID] . ">" . $row_tipe[Vendor_Tipe::$NAMA] . "</option>";
                    }
                }
                $select.="</select>";
                
                //Nama
                $kal .= $data_list_gl . $data_list_payterm . $data_list_country .
                        '<div class="row">
                            <div class="col-sm-9">
                                <table style="width:100%">
                                    <tbody>
                                        <tr>
                                            <td class="align-top" style="width:19%">* Nama</td>
                                            <td class="align-top" style="width:2%">:</td>
                                            <td class="align-top" style="width:100%"><input type="text" style="width:145%" id="txt_nama"/></td>
                                        </tr>
                                        <tr>
                                            <td class="align-top" style="width:19%">* Alamat</td>
                                            <td class="align-top" style="width:2%">:</td>
                                            <td class="align-top" style="width:100%"><input type="text" style="width:145%" id="txt_alamat"/></td>
                                        </tr>
                                        <tr>
                                            <td class="align-top" style="width:19%">* Tipe Vendor</td>
                                            <td class="align-top" style="width:2%">:</td>
                                            <td class="align-middle" style="width:100%">
                                                '. $select .'
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>';
                $kal .= "<br>";

                $kal .= '<div class="row">
                            <div class="col-sm-6">
                                <table style="width:100%">
                                    <tr>
                                        <td>RT</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_rt"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kelurahan"/></td>
                                    </tr>
                                    <tr>
                                        <td>* Negara</td>
                                        <td> : </td>
                                        <td><input style="width:100%" list="list_country" class="f-aleo" type="text" id="txt_negara"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kode Pos</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kodepos"/></td>
                                    </tr>
                                    <tr>
                                        <td>NPWP</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_npwp"/></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_email"/></td>
                                    </tr>
                                    <tr>
                                        <td>Akun Bank</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_bank_account"/></td>
                                    </tr>
                                    <tr>
                                        <td>ID Payterm</td>
                                        <td> : </td>
                                        <td><input style="width:100%" list="list_payterm" class="f-aleo" type="text" id="txt_id_payterm"/></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <table style="width:100%">
                                    <tr>
                                        <td>RW</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_rw"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kecamatan"/></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Kota</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kota"/></td>
                                    </tr>
                                    <tr>
                                        <td>Currency</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_currency"/></td>
                                    </tr>
                                    <tr>
                                        <td>Sales Person</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_sales_person"/></td>
                                    </tr>
                                    <tr>
                                        <td>No Telp</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_notelp"/></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Bank</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_bank_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>Klasifikasi</td>
                                        <td> : </td>
                                        <td><input style="width:100%" list="list_gl" class="f-aleo" type="text" id="txt_id_gl"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>';
                $kal .= "<br>";
                $kal .= "</div>";

            $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";
        $kal .= '<div class="row text-center">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-outline-primary btn_modify_karyawan" onclick="insert()" data-mdb-ripple-color="dark">
                            Tambah
                        </button>
                    </div>
                </div></form>';

        echo $kal;
    }
    public function view_update(){
        $kal = "";
        $kal .= "<h1 class='f-aleo-bold text-center'>UPDATE VENDOR</h1>";
		$kal .= "<h6 class='f-aleo-bold text-center red-text'>* : Wajib Diisi</h6>";

        $kal .= "<br>";
        $kal .= '<div class="row" style="margin-right:1%">';
        $kal .= '<div class="col-sm-2"></div>';
        $kal .= '<div class="col-sm-8">';

        //GL Datalist
        $data_list_gl = "<datalist id='list_gl'>";
        $data_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME, null, null, null, null, false);
        if ($data_gl->num_rows() > 0) {
            foreach ($data_gl->result_array() as $row_gl) {
                $data_list_gl .= "<option value=" . $row_gl[General_Ledger::$ID] . ">" . $row_gl[General_Ledger::$DESKRIPSI] . "</option>";
            }
        }
        $data_list_gl .= "</datalist>";

        //Payterm Datalist
        $data_list_payterm = "<datalist id='list_payterm'>";
        $data_payterm = $this->payterm_model->get(Payterm::$TABLE_NAME);
        if ($data_payterm->num_rows() > 0) {
            foreach ($data_payterm->result_array() as $row_payterm) {
                $data_list_payterm .= "<option value=" . $row_payterm[Payterm::$ID] . ">" . $row_payterm[Payterm::$DESKRIPSI] . "</option>";
            }
        }

        $data_list_payterm .= "</datalist>";

        //Country Datalist
        $data_list_country = "<datalist id='list_country'>";
        $data_country = $this->country_model->get(Country::$TABLE_NAME, null, null, null, null, false);
        if ($data_country->num_rows() > 0) {
            foreach ($data_country->result_array() as $row_country) {
                $data_list_country .= "<option value=" . $row_country[Country::$ID] . ">" . $row_country[Country::$NAMA_COUNTRY] . "</option>";
            }
        }
        $data_list_country .= "</datalist>";
        //Nama
        $kal .= $data_list_gl . $data_list_payterm . $data_list_country .
                        '<div class="row">
                            <div class="col-sm-9">
                                <table style="width:100%">
                                    <tbody>
                                        <tr>
                                            <td class="align-top" style="width:19%">ID Vendor</td>
                                            <td class="align-top" style="width:2%">:</td>
                                            <td class="align-top" style="width:100%"><input onkeyup="get_by_id(this.value)" type="text" style="width:20%" id="txt_id_vendor"/></td>
                                        </tr>
                                        <tr>
                                            <td class="align-top" style="width:19%">* Nama</td>
                                            <td class="align-top" style="width:2%">:</td>
                                            <td class="align-top" style="width:100%"><input type="text" style="width:145%" id="txt_nama"/></td>
                                        </tr>
                                        <tr>
                                            <td class="align-top" style="width:19%">* Alamat</td>
                                            <td class="align-top" style="width:2%">:</td>
                                            <td class="align-top" style="width:100%"><input type="text" style="width:145%" id="txt_alamat"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>';
        $kal .= "<br>";

        $kal .= '<form id="form_update"><div class="row">
                            <div class="col-sm-6">
                                <table style="width:100%">
                                    <tr>
                                        <td>RT</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_rt"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kelurahan"/></td>
                                    </tr>
                                    <tr>
                                        <td>* Negara</td>
                                        <td> : </td>
                                        <td><input style="width:100%" list="list_country" class="f-aleo" type="text" id="txt_negara"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kode Pos</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kodepos"/></td>
                                    </tr>
                                    <tr>
                                        <td>NPWP</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_npwp"/></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_email"/></td>
                                    </tr>
                                    <tr>
                                        <td>Akun Bank</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_bank_account"/></td>
                                    </tr>
                                    <tr>
                                        <td>ID Payterm</td>
                                        <td> : </td>
                                        <td><input style="width:100%" list="list_payterm" class="f-aleo" type="text" id="txt_id_payterm"/></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <table style="width:100%">
                                    <tr>
                                        <td>RW</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_rw"/></td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kecamatan"/></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Kota</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_kota"/></td>
                                    </tr>
                                    <tr>
                                        <td>Currency</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_currency"/></td>
                                    </tr>
                                    <tr>
                                        <td>Sales Person</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_sales_person"/></td>
                                    </tr>
                                    <tr>
                                        <td>No Telp</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_notelp"/></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Bank</td>
                                        <td> : </td>
                                        <td><input style="width:100%" class="f-aleo" type="text" id="txt_bank_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>Klasifikasi</td>
                                        <td> : </td>
                                        <td><input style="width:100%" list="list_gl" class="f-aleo" type="text" id="txt_id_gl"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>';
        $kal .= "<br>";
        $kal .= "</div>";

        $kal .= '<div class="col-sm-2"></div>';
        $kal .= "</div>";
        $kal .= '<div class="row text-center">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-outline-primary" onclick="update()" data-mdb-ripple-color="dark">
                            Update
                        </button>
                    </div>
                </div></form>';

        echo $kal;
    }
    public function view_supply(){
        $kal = "";
        $data = $this->vendor_model->get_supply();
        $arr_header = ["ID Vendor", "Nama", "Kode Material yang Disupply"];

        $kal .= '<h1 class="f-aleo-bold text-center">SUPPLY VENDOR</h1>
                <div class="row" style="margin-right:1%;margin-left:1%">';
        //$kal .= '<div class="col-sm-1"></div>';
        $kal .= '<div class="col-sm-12">';
        $kal .= '<table id="mastertable" class="table table-bordered table-sm">
                    <thead>' . $this->gen_table_header($arr_header) . '</thead>
                    <tbody>';
        foreach ($data->result_array() as $row_vendor) {
            $kal .= "<tr>";
                $kal .= "<td class='text-center'>" . $row_vendor[Vendor::$ID] . "</td>";
                $kal .= "<td class='text-left'>" . $row_vendor[Vendor::$NAMA] . "</td>";
                $kal .= "<td class='text-left'>" . $row_vendor[Material_RM_PM::$KODE_MATERIAL] . "</td>";
            $kal .= "</tr>";
        }
        $kal .= '	</tbody>
                </table>';
        $kal .= "</div>";
        //$kal .= '<div class="col-sm-1"></div>';
        $kal .= "</div>";
        echo $kal;
    }
    
    public function insert_vendor(){      
        $nama = $this->i_p("n");
        $alamat = $this->i_p("a");
        $rt = $this->i_p("rt");
        $rw = $this->i_p("rw");
        $kecamatan = $this->i_p("kec");
        $kelurahan = $this->i_p("kel");
        $kota = $this->i_p("k");
        $kode_pos = $this->i_p("kp");
        $negara = $this->i_p("ne");
        $currency = $this->i_p("c");
        $sales_person = $this->i_p("sp");
        $npwp = $this->i_p("np");
        $notelp = $this->i_p("nt");
        $email = $this->i_p("e");
        $bank_account = $this->i_p("ba");
        $bank_name = $this->i_p("bn");
        $id_payterm = $this->i_p("ip");
        $id_gl = $this->i_p("ig");
        $tipe = $this->i_p("t");
        echo MESSAGE[$this->vendor_model->insert($nama, $alamat,$rt,$rw,$kecamatan,$kelurahan,$kota,$kode_pos,$negara,$currency,
                        $sales_person,$npwp,$notelp,$email,$bank_account,$bank_name,$tipe,$id_payterm,$id_gl)];
    }
    public function update_vendor(){
        $id_vendor = $this->i_p("iv");
        $nama = $this->i_p("n");
        $alamat = $this->i_p("a");
        $rt = $this->i_p("rt");
        $rw = $this->i_p("rw");
        $kecamatan = $this->i_p("kec");
        $kelurahan = $this->i_p("kel");
        $kota = $this->i_p("k");
        $kode_pos = $this->i_p("kp");
        $negara = $this->i_p("ne");
        $currency = $this->i_p("c");
        $sales_person = $this->i_p("sp");
        $npwp = $this->i_p("np");
        $notelp = $this->i_p("nt");
        $email = $this->i_p("e");
        $bank_account = $this->i_p("ba");
        $bank_name = $this->i_p("bn");
        $id_payterm = $this->i_p("ip");
        $id_gl = $this->i_p("ig");
        echo MESSAGE[$this->vendor_model->update($id_vendor,$nama, $alamat,$rt,$rw,$kecamatan,$kelurahan,$kota,$kode_pos,$negara,$currency,
                        $sales_person,$npwp,$notelp,$email,$bank_account,$bank_name,$id_payterm,$id_gl)];
    }
}
