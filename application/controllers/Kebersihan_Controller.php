<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Kebersihan_Controller extends Library_Controller {

    public function harian_insert(){
		$this->insert(Kebersihan_Harian::$TABLE_NAME);
	}
}
