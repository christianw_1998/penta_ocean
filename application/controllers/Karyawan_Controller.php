<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . 'core/Library_Controller.php');

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Karyawan_Controller extends Library_Controller {

	public function get_by_nik(){
        $nik = $this->i_p("n");
        $num_rows = $this->karyawan_model->get_by(Karyawan::$ID, $nik)->num_rows();
        $temp = [];
        $temp['num_rows'] = $num_rows;
        if ($num_rows > 0) {
            $data = $this->karyawan_model->get_by(Karyawan::$ID, $nik)->row_array();
            $temp[Karyawan::$ID] = $data[Karyawan::$ID];
            $temp[Karyawan::$NAMA] = $data[Karyawan::$S_K_NAMA];
            $temp[Role::$ID] = $data[Role::$ID];
            $temp[Karyawan::$GAJI_POKOK] = $data[Karyawan::$GAJI_POKOK];
            $temp[Karyawan::$NPWP] = $data[Karyawan::$NPWP];
            $temp[Karyawan::$TUNJANGAN_JABATAN] = $data[Karyawan::$TUNJANGAN_JABATAN];
            $temp[Kategori_Karyawan::$ID] = $data[Kategori_Karyawan::$ID];
            $temp[Karyawan::$SALDO_CUTI] = $data[Karyawan::$SALDO_CUTI];
            $temp[Karyawan::$IS_OUTSOURCE] = $data[Karyawan::$IS_OUTSOURCE];
            $temp[Sub_Kategori::$ID] = $data[Sub_Kategori::$ID];
        }
        echo json_encode($temp);
    }

    public function get_role_by_nik_login(){
		$role=$this->karyawan_model->get_by(Karyawan::$ID,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array()[Role::$ID];
		echo $role;
	}
}
