<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Input Detail Payment Voucher</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
  <style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  </style>
</head>

<body class="f-aleo role-kepala role-accounting role-purchasing">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Costing</li>
        <li class="breadcrumb-item" aria-current="page">Payment Voucher</li>
        <li class="breadcrumb-item" aria-current="page">Detail</li>
        <li class="breadcrumb-item active" aria-current="page">Input</li>
      </ol>
    </nav>
    <hr style="margin-left:5%;margin-right:5%">

    <form id="form_payment">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-12" id="content">
          <h1 class="text-center fw-bold">INPUT DETAIL PAYMENT VOUCHER</h1>
          <hr style="margin-left:5%;margin-right:5%">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-2 text-right">ID Vendor</div>
        <datalist id='list_vendor'>
          <?php
          //Vendor Datalist
          $data_vendor = $this->vendor_model->get(Vendor::$TABLE_NAME);
          if ($data_vendor->num_rows() > 0) {
            foreach ($data_vendor->result_array() as $row_vendor) { ?>
              <option value="<?php echo $row_vendor[Vendor::$ID]; ?>"><?php echo $row_vendor[Vendor::$NAMA]; ?></option>
          <?php
            }
          } ?>
        </datalist>

        <div class="col-sm-2">
          <input class="black-text" onkeyup='get_vendor_by_id(this.value)' list='list_vendor' type="text" style="width:100%" id="txt_id_vendor">
        </div>
        <div class="col-sm-7">
          <span id="txt_desc_vendor" class="align-middle"></span>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-2 text-right">Tanggal</div>
        <div class="col-sm-2">
          <input class="black-text" type="date" style="width:100%" id="txt_tanggal">
        </div>
        <div class="col-sm-7"></div>
      </div>
      <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-2 text-right">Reference</div>
        <div class="col-sm-2">
          <datalist id='list_ref'>
            <?php
            //Reference Datalist
            $data_ref = $this->payment_model->get_newest(Payment_Voucher_Detail_H::$TABLE_NAME, Payment_Voucher_Detail_H::$REFERENCE, Payment_Voucher_Detail_H::$ID, 10);
            if ($data_ref->num_rows() > 0) {
              foreach ($data_ref->result_array() as $row_ref) { ?>
                <option value="<?php echo $row_ref[Payment_Voucher_Detail_H::$REFERENCE]; ?>"><?php echo $row_ref[Payment_Voucher_Detail_H::$REFERENCE]; ?></option>
            <?php }
            } ?>
          </datalist>
          <input class="black-text" list="list_ref" type="text" style="width:100%" id="txt_reference" />
        </div>
        <div class="col-sm-7"></div>
      </div>
      <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-2 text-right">Amount</div>
        <div class="col-sm-2">
          <input class="black-text text-right" type="text" style="width:100%" id="txt_total_amount" disabled />
        </div>
        <div class="col-sm-2 align-middle">
          <input type="checkbox" onchange="open_tax_type()" class="align-middle" id="cb_tax"> Calculate Tax
        </div>
        <div class="col-sm-2 align-middle" style="visibility:hidden" id="div_tax_amount">
          <select style="width:100%" id="dd_tax" onchange="switch_tax_type()">
            <?php
            //Ambil Pajak
            $data_tax = $this->payment_model->get(Taxes::$TABLE_NAME, null, null, null, null, false);
            if ($data_tax->num_rows() > 0) {
              foreach ($data_tax->result_array() as $row_tax) { ?>
                <option value="<?php echo $row_tax[Taxes::$ID]; ?>"><?php echo $row_tax[Taxes::$DESCRIPTION]; ?></option>
            <?php }
            } ?>
          </select>
          <select style="width:100%;visibility:hidden" id="dd_tax_value">
            <?php
            //Ambil Pajak
            $data_tax = $this->payment_model->get(Taxes::$TABLE_NAME, null, null, null, null, false);
            if ($data_tax->num_rows() > 0) {
              foreach ($data_tax->result_array() as $row_tax) { ?>
                <option value="<?php echo $row_tax[Taxes::$VALUE]; ?>"><?php echo $row_tax[Taxes::$DESCRIPTION]; ?></option>
            <?php }
            } ?>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-2 text-right">Text</div>
        <div class="col-sm-5">
          <datalist id='list_text'>
            <?php
            //Text Datalist
            $data_text = $this->payment_model->get_newest(Payment_Voucher_Detail_H::$TABLE_NAME, Payment_Voucher_Detail_H::$TEXT, Payment_Voucher_Detail_H::$ID, 10);
            if ($data_text->num_rows() > 0) {
              foreach ($data_text->result_array() as $row_text) { ?>
                <option value="<?php echo $row_text[Payment_Voucher_Detail_H::$TEXT]; ?>"><?php echo $row_text[Payment_Voucher_Detail_H::$TEXT]; ?></option>
            <?php }
            } ?>
          </datalist>
          <input class="black-text" list="list_text" type="text" style="width:100%" id="txt_text" />
        </div>
        <div class="col-sm-4 align-middle"></div>
      </div>
      <hr>
      <div class="row" class="text-center">
        <div class="col-sm-12">
          <table class="tabel_detail_payment" style="visibility:hidden">
            <tr class="fw-bold">
              <td class="text-center">No</td>
              <td class="text-center">Nomor GL</td>
              <td class="text-center">Deskripsi GL</td>
              <td class="text-center">Nominal</td>
              <td class="text-center">Cost Center</td>
              <td class="text-center">Tipe</td>
              <td class="text-center">Deskripsi</td>
              <td class="text-center">Assignment</td>
              <td class="text-center">Order</td>
              <td class="text-center">A</td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-center">
          <button type="button" id="btn_tambah" class=" center btn btn-outline-primary" onclick="plus_payment()" data-mdb-ripple-color="dark">
            Tambah
          </button>
          <button type="button" id="btn_generate" class=" center btn btn-outline-success" onclick="preview()" data-mdb-ripple-color="dark">
            Preview
          </button>
        </div>
      </div>
    </form>
  </div>
</body>

</html>
<!-- Modal Preview -->
<div class="f-aleo modal fade" id="mod_preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">PREVIEW DETAIL PAYMENT VOUCHER</h5>
      </div>
      <!--Body-->
      <div class="modal-body" id="modal_preview" style="margin:1%">

      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" onclick="generate()" class="btn btn-outline-success">Generate</button>
      </div>
    </div>
    </form>
    <!--/.Content-->
  </div>
</div>
<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';
  var ctr = 0;
  var total = 0;
  var idx_tax = -1;
  $(document).ready(function() {
    $('#mod_preview').appendTo("body");
  });

  function preview() {
    var arr_id_gl = new Array();
    var arr_nominal = new Array();
    var arr_id_cc = new Array();
    var arr_tipe = new Array();
    var arr_description = new Array();
    var arr_assignment = new Array();
    var arr_order = new Array();

    var id_vendor = $("#txt_id_vendor").val();
    var tanggal = $("#txt_tanggal").val();
    var reference = $("#txt_reference").val();
    var text = $("#txt_text").val();
    var tax = null;
    if ($("#dd_tax").css("visibility") != "hidden")
      tax = $("#dd_tax").val();

    var idx = 0;
    var is_exist = true;

    for (var i = 0; i < ctr; i++) {
      is_exist = true;
      //GL
      if ($("#txt_id_gl_" + i).length > 0) {
        if ($("#txt_id_gl_" + i).val() != "")
          arr_id_gl[idx] = parseInt($("#txt_id_gl_" + i).val());
        else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      } else {
        is_exist = false;
      }

      //Nominal
      if ($("#txt_nominal_" + i).length > 0) {
        if ($("#txt_nominal_" + i).val() != "")
          arr_nominal[idx] = parseInt($("#txt_nominal_" + i).val());
        else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      } else {
        is_exist = false;
      }

      //CC
      if ($("#txt_id_cc_" + i).length > 0) {
        if ($("#txt_id_cc_" + i).val() != "")
          arr_id_cc[idx] = parseInt($("#txt_id_cc_" + i).val());
        else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      } else {
        is_exist = false;
      }

      arr_tipe[idx] = $("#dd_tipe_" + i).val();
      arr_description[idx] = $("#txt_desc_" + i).val();
      arr_assignment[idx] = $("#txt_assignment_" + i).val();
      if ($("#txt_order_" + i).val() != "")
        arr_order[idx] = $("#txt_order_" + i).val();
      else
        arr_order[idx] = null;

      if (is_exist)
        idx++;
    }
    if (idx != 0) {
      $.ajax({
        type: "POST",
        url: site_url + "payment_voucher/detail/view_preview",
        data: {
          ag: arr_id_gl,
          an: arr_nominal,
          ac: arr_id_cc,
          at: arr_tipe,
          ad: arr_description,
          aa: arr_assignment,
          ao: arr_order,
          iv: id_vendor,
          t: tanggal,
          r: reference,
          te: text,
          ta: tax,
          to: total,
          c: idx
        },
        success: function(result) {
          if (!$('#mod_preview').is(':visible')) {
            $("#modal_preview").html(result);
            $('#mod_preview').modal('show');
          }
        }
      });
    }
  }

  function open_tax_type() {
    if ($("#cb_tax").prop("checked")) {
      $("#div_tax_amount").css("visibility", "visible");
      idx_tax = ctr;
      plus_payment($("#dd_tax").val());
    } else {
      $("#div_tax_amount").css("visibility", "hidden");
      minus_payment(idx_tax);
      idx_tax = -1;
    }
    calculate_total();
  }

  function switch_tax_type() {
    minus_payment(idx_tax);
    plus_payment($("#dd_tax").val());
    idx_tax = ctr;
    calculate_total();
  }

  function plus_payment(id_tax = null) {
    var selected_idx = [];

    //Simpan semua value index tipe account
    for (var i = 0; i < ctr; i++) {
      if ($("#dd_tipe_" + i).length > 0) {
        selected_idx[i] = $("#dd_tipe_" + i).prop("selectedIndex");
      }
    }

    var temp = $(".tabel_detail_payment");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    var url = "plus";
    if (id_tax != null) {
      url = "plus_tax"
    }
    if ((ctr >= 1 && id_tax == null && $("#txt_id_gl_" + (ctr - 1)).val() != "" &&
        $("#txt_nominal_" + (ctr - 1)).val() != "" && $("#txt_id_cc_" + (ctr - 1)).val() != "" &&
        $("#txt_desc_" + (ctr - 1)).val() != "" && $("#txt_assignment_" + (ctr - 1)).val() != "") ||
      id_tax != null || ctr == 0) {
      $.ajax({
        type: "POST",
        url: site_url + "payment_voucher/detail/" + url,
        data: {
          c: ctr,
          it: $("#dd_tax").val()
        },
        success: function(result) {
          $(".tabel_detail_payment").html(html + result);
          ctr++;

          $(".tabel_detail_payment").css("visibility", "visible");
          //Ubah kembali semua Select menjadi index awal
          for (var i = 0; i < ctr; i++) {
            if (typeof selected_idx[i] !== "undefined") {
              $("#dd_tipe_" + i).prop("selectedIndex", selected_idx[i]);
            }
          }
          if (id_tax != null) {
            total = 0;
            //Total PPN
            for (var i = 0; i < ctr; i++) {
              if ($("#txt_nominal_" + i).length > 0) {
                if ($("#txt_id_gl_" + i).val().match("^5") || $("#txt_id_gl_" + i).val().match("^6")) {
                  total += parseInt($("#txt_nominal_" + i).val());
                }
              }
            }
            $("#dd_tax_value").prop("selectedIndex", $("#dd_tax").prop("selectedIndex"));

            $("#txt_nominal_" + idx_tax).val(Math.round(total * parseFloat($("#dd_tax_value").val()) / 100));

            //Assignment & Tipe
            for (var i = 0; i < ctr; i++) {
              if ($("#txt_assignment_" + i).length > 0) {
                $("#txt_assignment_" + idx_tax).val($("#txt_assignment_" + i).val());
                $("#txt_desc_" + idx_tax).val($("#txt_text").val());
                $("#dd_tipe_" + idx_tax).val($("#dd_tipe_" + i).val());
                break;
              }
            }
            calculate_total();
          }
        }
      });
    } else {
      toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
    }
  }

  function minus_payment(params) {
    var temp = $(".tabel_detail_payment");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_payment_" + params).remove();
  }

  function get_vendor_by_id(iv) {
    $.ajax({
      type: "POST",
      url: site_url + "vendor/get",
      data: {
        iv: iv,
      },
      dataType: "json",
      success: function(result) {
        $("#txt_desc_vendor").removeClass('green-text red-text');
        if (result.num_rows != 0) {
          $("#txt_desc_vendor").html(result["<?php echo Vendor::$NAMA; ?>"]);
          $("#txt_desc_vendor").addClass("green-text");
        } else {
          $("#txt_desc_vendor").html("Input 6 digit kode Vendor");
          $("#txt_desc_vendor").addClass('red-text');
          //$("#txt_desc_vendor").prop("disabled", true);

        }
      }
    });
  }

  function calculate_total() {
    total = 0;
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_nominal_" + i).length > 0) {
        if ($("#txt_nominal_" + i).val() != "") {
          total += (parseInt($("#txt_nominal_" + i).val())) * $("#dd_tipe_" + i).val();
        }
      }
    }
    $("#txt_total_amount").val(add_decimal(Math.abs(total)));
  }

  function check_gl(params) {
    var id_gl = $("#txt_id_gl_" + params).val();
    if (id_gl != "") {
      $.ajax({
        type: "POST",
        url: site_url + "gl/get",
        data: {
          i: id_gl,
        },
        dataType: "JSON",
        success: function(result) {

          if (result.num_rows != 0) {
            $("#txt_desc_gl_" + params).val(result["<?php echo General_Ledger::$DESKRIPSI; ?>"]);
            $("#txt_desc_gl_" + params).addClass("green-text");
          } else {
            $("#txt_desc_gl_" + params).val("GL tidak ditemukan");
            $("#txt_desc_gl_" + params).removeClass('green-text');
            $("#txt_desc_gl_" + params).addClass('red-text');
          }
        }
      });
    }
  }

  function generate() {
    var arr_id_gl = new Array();
    var arr_nominal = new Array();
    var arr_id_cc = new Array();
    var arr_tipe = new Array();
    var arr_description = new Array();
    var arr_assignment = new Array();
    var arr_order = new Array();

    var id_vendor = $("#txt_id_vendor").val();
    var tanggal = $("#txt_tanggal").val();
    var reference = $("#txt_reference").val();
    var text = $("#txt_text").val();
    var tax = -1;
    if ($("#dd_tax").css("visibility") != "hidden")
      tax = $("#dd_tax").val();

    var idx = 0;
    var is_exist = true;

    for (var i = 0; i < ctr; i++) {
      is_exist = true;
      //GL
      if ($("#txt_id_gl_" + i).length > 0) {
        if ($("#txt_id_gl_" + i).val() != "")
          arr_id_gl[idx] = parseInt($("#txt_id_gl_" + i).val());
        else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      } else {
        is_exist = false;
      }

      //Nominal
      if ($("#txt_nominal_" + i).length > 0) {
        if ($("#txt_nominal_" + i).val() != "")
          arr_nominal[idx] = parseInt($("#txt_nominal_" + i).val());
        else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      }

      //CC
      if ($("#txt_id_cc_" + i).length > 0) {
        if ($("#txt_id_cc_" + i).val() != "")
          arr_id_cc[idx] = parseInt($("#txt_id_cc_" + i).val());
        else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      }



      arr_tipe[idx] = $("#dd_tipe_" + i).val();
      arr_description[idx] = $("#txt_desc_" + i).val();
      arr_assignment[idx] = $("#txt_assignment_" + i).val();
      if ($("#txt_order_" + i).val() != "")
        arr_order[idx] = $("#txt_order_" + i).val();
      else
        arr_order[idx] = null;

      if (is_exist)
        idx++;
    }

    //Total
    total = 0;
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_nominal_" + i).length > 0) {
        if ($("#txt_nominal_" + i).val() != "") {
          total += (parseInt($("#txt_nominal_" + i).val())) * $("#dd_tipe_" + i).val();
        }
      }
    }
    
    if (idx != 0) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "payment_voucher/detail/insert",
          data: {
            ag: arr_id_gl,
            an: arr_nominal,
            ac: arr_id_cc,
            at: arr_tipe,
            ad: arr_description,
            aa: arr_assignment,
            ao: arr_order,
            iv: id_vendor,
            t: tanggal,
            r: reference,
            te: text,
            ta: tax,
            to: total,
            c: idx
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              reset_form();
              $('#mod_preview').modal('hide');
              toast(result, Color.SUCCESS);
            } else
              toast(result, Color.DANGER);
          }
        });
      }
    } else
      toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
  }

  function reset_form() {
    var kal = '<tr class="fw-bold"><td class="text-center">No</td><td class="text-center">Nomor GL</td><td class="text-center">Description GL</td><td class="text-center">Nominal</td><td class="text-center">ID CC</td><td class="text-center">Tipe</td><td class="text-center">Description</td><td class="text-center">Assignment</td><td class="text-center">Order</td><td class="text-center">Aksi</td></tr>';
    $(".tabel_detail_payment").html(kal);
    $(".tabel_detail_payment").css("visibility", "hidden");
    $("#dd_tax").css("visibility", "hidden");
    $("#form_payment").trigger("reset");
    $("#txt_desc_vendor").html("");
    idx_tax = -1;
    ctr = 0;
  }
</script>