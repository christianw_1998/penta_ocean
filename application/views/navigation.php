<style>
    @media (min-width: 992px) {
        .dropdown-menu .dropdown-toggle:after {
            border-top: .3em solid transparent;
            border-right: 0;
            border-bottom: .3em solid transparent;
            border-left: .3em solid;
        }

        .dropdown-menu .dropdown-menu {
            margin-left: 0;
            margin-right: 0;
        }

        .dropdown-menu li {
            position: relative;
        }

        .nav-item .submenu {
            display: none;
            position: absolute;
            left: 100%;
            top: -7px;
        }

        .nav-item .submenu-left {
            right: 100%;
            left: auto;
        }

        .dropdown-menu>li:hover {
            background-color: #f1f1f1
        }

        .dropdown-menu>li:hover>.submenu {
            display: block;
        }
    }
</style>

<!-- Navbar -->
<nav style='visibility:hidden' class="f-aleo navbar navbar-expand-lg navbar-dark bg-primary">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?php echo site_url("home"); ?>">Welcome, <?php echo ucwords(strtolower($_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$NAMA])); ?>!</a>

    <div class="collapse navbar-collapse justify-content-end" id="main_nav">
        <ul class="navbar-nav">
            <!-- Master -->
            <li class="nav-item dropleft role-kepala role-ppic role-purchasing role-admin role-accounting role-kepala-gudang role-checker-fg role-checker-rm-pm">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Master </a>
                <ul class="dropdown-menu">
                    <li><a class="role-ppic role-checker-fg role-accounting role-purchasing role-checker-rm-pm fw-bold f-aleo dropdown-item" href="#"> Material &raquo </a>
                        <ul class="submenu dropdown-menu">
                            <li class="role-accounting role-checker-fg role-kepala role-kepala-gudang">
                                <a class="dropdown-item" href="#"> Mat. FG &raquo </a>
                                <ul class="submenu dropdown-menu">
                                    <li><a class="dropdown-item role-kepala role-accounting role-kepala-gudang role-admin role-checker-fg" href="<?php echo site_url("home/master/material_fg"); ?>">SAGE</a></li>
                                    <li><a class="dropdown-item role-kepala role-accounting role-kepala-gudang role-admin" href="<?php echo site_url("home/master/material_fg_sap"); ?>">SAP</a></li>
                                </ul>
                            </li>
                            <li></li>
                            <li><a class="dropdown-item role-kepala role-ppic role-purchasing role-accounting role-kepala-gudang role-admin role-checker-rm-pm" href="<?php echo site_url("home/master/material_rm_pm"); ?>">Mat. RM PM</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-kepala" href="<?php echo site_url("home/master/karyawan"); ?>">Karyawan</a></li>
                    <li><a class="dropdown-item role-kepala role-admin" href="<?php echo site_url("home/master/absensi"); ?>">Absensi</a></li>
                    <li><a class="dropdown-item role-kepala" href="<?php echo site_url("home/master/kalender"); ?>">Kalender</a></li>
                    <li><a class="dropdown-item role-kepala" href="<?php echo site_url("home/master/cutoff"); ?>">Cut Off</a></li>
                    <li><a class="dropdown-item role-kepala" href="<?php echo site_url("home/master/s_insentif"); ?>">Standar Insentif</a></li>
                    <li><a class="dropdown-item role-purchasing role-accounting" href="<?php echo site_url("home/master/payterm"); ?>">Payterm</a></li>
                    <li><a class="dropdown-item role-purchasing role-accounting" href="<?php echo site_url("home/master/vendor"); ?>">Vendor</a></li>
                    <li><a class="dropdown-item role-purchasing role-accounting" href="<?php echo site_url("home/master/general_ledger"); ?>">General Ledger</a></li>
                </ul>
            </li>

            <!-- Gaji -->
            <li class="nav-item dropleft role-kepala">
                <span class="nav-link dropdown-toggle" href="#" id="navbarDropdownGaji" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Gaji
                </span>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownGaji">
                    <a class="dropdown-item" href="<?php echo site_url("home/gaji/slip_gaji"); ?>">Generate Slip Gaji</a>
                    <a class="dropdown-item" href="<?php echo site_url("home/gaji/karyawan_insentif"); ?>">Insentif Karyawan</a>
                    <a class="dropdown-item" href="<?php echo site_url("home/gaji/rekap_gaji"); ?>">Rekap Gaji</a>
                </div>
            </li>

            <!-- Performance -->
            <li class="nav-item dropleft role-kepala">
                <span class="nav-link dropdown-toggle" href="#" id="navbarDropdownPerformance" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Performance
                </span>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownPerformance">
                    <a class="dropdown-item" href="<?php echo site_url("home/performance/rekap_performance"); ?>">Rekap Performance</a>
                </div>
            </li>

            <!-- Gudang Finished -->
            <li class="nav-item dropleft role-purchasing role-admin role-accounting role-checker-fg role-kepala role-kepala-gudang">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> G. FG </a>
                <ul class="dropdown-menu">
                    <li class="role-accounting role-checker-fg role-kepala role-kepala-gudang">
                        <a class="dropdown-item" href="#"> Barang Masuk &raquo </a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item" href="<?php echo site_url("home/finished/barang_masuk"); ?>">Dari Surat Jalan</a></li>
                            <li><a class="dropdown-item" href="<?php echo site_url("home/finished/barang_masuk_ws"); ?>">Dari Work Sheet</a></li>
                            <li><a class="dropdown-item role-accounting" href="<?php echo site_url("home/finished/barang_masuk_adjustment"); ?>">Adjustment</a></li>
                        </ul>
                    </li>

                    <li><a class="dropdown-item role-admin role-accounting role-checker-fg role-kepala-gudang" href="#"> Stock &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <!--<li><a class="dropdown-item role-accounting" data-toggle="modal" data-target="#mod_upl_stock_material" data-mdb-ripple-color="dark">Upload Stock Awal</a></li>-->
                            <li><a class="dropdown-item role-accounting role-checker-fg role-kepala-gudang" href="<?php echo site_url("home/finished/stock_history"); ?>">History</a></li>
                            <li><a class="dropdown-item role-accounting role-checker-fg role-kepala-gudang" href="<?php echo site_url("home/finished/stock_report"); ?>">Laporan</a></li>
                            <li><a class="dropdown-item role-admin role-accounting role-checker-fg" href="<?php echo site_url("home/finished/rekap_sj"); ?>">Rekap Surat Jalan</a></li>
                            <li><a class="dropdown-item role-accounting role-checker-fg" href="#"> Stock Opname &raquo</a>
                                <ul class="submenu dropdown-menu">
                                    <li><a class="dropdown-item role-accounting role-checker-fg" href="<?php echo site_url("home/finished/stock_opname"); ?>">Input</a></li>
                                    <li><a class="dropdown-item role-accounting role-checker-fg" href="<?php echo site_url("home/finished/stock_opname_history"); ?>">Laporan</a></li>
                                    <li><a class="dropdown-item role-accounting" href="<?php echo site_url("home/finished/stock_opname_template"); ?>">Template Tahunan</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-accounting" href="#"> Lainnya &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting" href="<?php echo site_url("home/finished/change_nomor_sj"); ?>">Ganti Nomor SJ</a></li>
                            <li><a class="dropdown-item role-accounting" href="<?php echo site_url("home/finished/gudang_transit_report"); ?>">Gudang Transit WS</a></li>
                        </ul>
                    </li>
                    <li class="role-accounting role-checker-fg role-kepala role-kepala-gudang">
                        <a class="dropdown-item" href="#"> Barang Keluar &raquo </a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item" href="<?php echo site_url("home/finished/barang_keluar"); ?>">Dari Surat Jalan</a></li>
                            <li><a class="dropdown-item role-accounting" href="<?php echo site_url("home/finished/barang_keluar_adjustment"); ?>">Adjustment</a></li>
                        </ul>
                    </li>
                    <h6 class="fw-bold text-center role-purchasing role-ppic role-checker-fg role-admin role-accounting">Kinerja</h6>
                    <li><a class="dropdown-item role-ppic role-admin role-accounting" href="#"> Input &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-admin" href="<?php echo site_url("home/finished/kinerja/input_in"); ?>">Masuk</a></li>
                            <li><a class="dropdown-item role-accounting role-admin" href="<?php echo site_url("home/finished/kinerja/input_out"); ?>">Keluar</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-kepala role-checker-fg role-ppic role-admin role-accounting role-purchasing" href="#"> Lihat Data &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-admin role-purchasing role-accounting role-kepala-gudang" href="<?php echo site_url("home/finished/kinerja/detail_surat_jalan"); ?>">Rekap SJ</a></li>
                            <li><a class="dropdown-item role-accounting role-kepala role-admin" href="<?php echo site_url("home/finished/kinerja_report"); ?>">Laporan FG</a></li>
                            <li><a class="dropdown-item role-kepala" href="<?php echo site_url("home/rmpm/kinerja_report"); ?>">Laporan RM PM</a></li>
                            <li><a class="dropdown-item role-accounting role-admin" href="<?php echo site_url("home/finished/kinerja/ekspedisi_report"); ?>">Laporan Ekspedisi</a></li>
                            <li><a class="dropdown-item role-accounting role-admin" href="<?php echo site_url("home/finished/kinerja/sisa_report"); ?>">Laporan Sisa</a></li>
                            <li><a class="dropdown-item role-checker-fg role-accounting role-admin" href="<?php echo site_url("home/finished/kinerja/kebersihan_report"); ?>">Laporan Kebersihan</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <!-- Gudang RM PM -->
            <li class="nav-item dropleft role-accounting role-ppic role-admin-rm-pm role-checker-rm-pm role-purchasing">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> G. RM PM </a>
                <ul class="dropdown-menu">
                    <li><a class="role-ppic role-accounting role-purchasing role-checker-rm-pm fw-bold f-aleo dropdown-item" href="#"> Barang Masuk &raquo </a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-ppic role-accounting role-purchasing role-checker-rm-pm" href="<?php echo site_url("home/rmpm/barang_masuk"); ?>">Dari Purchase Order</a></li>
                            <li><a class="dropdown-item role-purchasing role-accounting " href="<?php echo site_url("home/rmpm/barang_masuk_adjustment"); ?>">Dari Mutasi</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-ppic role-purchasing role-accounting role-admin-rm-pm role-checker-rm-pm fw-bold f-aleo" href="#"> Barang Keluar &raquo </a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-ppic role-purchasing role-checker-rm-pm role-accounting role-admin-rm-pm" href="<?php echo site_url("home/rmpm/barang_keluar"); ?>">Dari Work Sheet</a></li>
                            <li><a class="dropdown-item role-ppic role-purchasing role-accounting" href="<?php echo site_url("home/rmpm/barang_keluar_adjustment"); ?>">Dari Mutasi</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-ppic role-admin-rm-pm role-checker-rm-pm role-purchasing role-accounting" href="#"> Stock &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <!--<li><a class="dropdown-item role-accounting role-purchasing" data-toggle="modal" data-target="#mod_upl_stock_material_rm_pm" data-mdb-ripple-color="dark">Upload Stock Awal</a></li>-->
                            <li><a class="dropdown-item role-accounting role-ppic role-admin-rm-pm role-purchasing role-checker-rm-pm" href="<?php echo site_url("home/rmpm/stock_history"); ?>">History</a></li>
                            <li><a class="dropdown-item role-accounting role-ppic role-purchasing" href="<?php echo site_url("home/rmpm/stock_report"); ?>">Laporan</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing role-checker-rm-pm" href="<?php echo site_url("home/rmpm/rekap_po_ws"); ?>">Rekap PO & WS</a></li>
                            <li><a class="dropdown-item role-ppic role-checker-rm-pm role-accounting" href="#"> Stock Opname &raquo</a>
                                <ul class="submenu dropdown-menu">
                                    <li><a class="dropdown-item role-checker-rm-pm role-accounting" href="<?php echo site_url("home/rmpm/stock_opname"); ?>">Input</a></li>
                                    <li><a class="dropdown-item role-ppic role-checker-rm-pm role-accounting" href="<?php echo site_url("home/rmpm/stock_opname_history"); ?>">Laporan</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Lainnya &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-purchasing role-accounting" href="<?php echo site_url("home/rmpm/document_report"); ?>">Cek Dokumen</a></li>
                        </ul>
                    </li>
                    <h6 class="fw-bold text-center role-accounting role-purchasing">Kinerja</h6>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/rmpm/kinerja_insentif_price"); ?>">Harga Insentif</a></li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/rmpm/kinerja_vendor_evaluation_input"); ?>" data-mdb-ripple-color="dark">Evaluasi Vendor</a></li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/rmpm/kinerja_vendor_evaluation_report"); ?>" data-mdb-ripple-color="dark">Laporan Evalusi Vendor</a></li>
                </ul>
            </li>

            <!-- Gudang Lain Lain -->
            <li class="nav-item dropleft role-purchasing role-accounting">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> G. Lain Lain </a>
                <ul class="dropdown-menu">
                    <li><a class="role-purchasing role-accounting f-aleo dropdown-item" href="#"> Barang Masuk &raquo </a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-ppic role-accounting role-purchasing" href="<?php echo site_url("home/other_material/barang_masuk"); ?>">Dari Purchase Order</a></li>
                        </ul>
                    </li>
                    <li><a class="role-purchasing role-accounting f-aleo dropdown-item" href="#"> Barang Keluar &raquo </a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/other_material/barang_keluar"); ?>">Pemakaian</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-purchasing role-accounting" href="#"> Stock &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/other_material/stock_history"); ?>">History Stock</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/other_material/stock_report"); ?>">Laporan Stock</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <!-- Costing -->
            <li class="nav-item dropleft role-accounting role-purchasing">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Costing </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Material &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/other_material/create_new"); ?>">Buat Baru</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/other_material/rekap"); ?>">Rekap</a></li>

                        </ul>
                    </li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> FOH &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/foh/setting"); ?>">Setting</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/foh/history"); ?>">History</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <!-- Purchase Order -->
            <li class="nav-item dropleft role-accounting role-purchasing">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> PO </a>
                <ul class="dropdown-menu">
                    <h6 class="fw-bold text-center role-accounting role-purchasing">Purchase Request</h6>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_request/create_new"); ?>">Buat Baru</a></li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Edit &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_request/tutup"); ?>">Tutup</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_request/history"); ?>">History</a></li>
                    <h6 class="fw-bold text-center role-accounting role-purchasing">Purchase Order</h6>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Buat &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" onclick="gen_form_upload_po()" data-mdb-ripple-color="dark"><span class="fw-bold">[RM PM]</span> Upload Data</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/create_new"); ?>"><span class="fw-bold">[RM PM]</span> Buat Baru</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/other_po/create_new"); ?>"><span class="fw-bold">[Lain Lain]</span> Buat Baru</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Edit &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/revisi"); ?>">Revisi Data</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/rmpm/change_nomor_sj"); ?>">Ganti Nomor SJ</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/tutup"); ?>">Tutup</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/cancel"); ?>">Cancel Good Receipt</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Lihat Data &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/rekap"); ?>">Laporan</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/print"); ?>">Cetak</a></li>
                        </ul>
                    </li>
                    <h6 class="fw-bold text-center role-accounting role-purchasing">Costing</h6>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Price List &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/price_input"); ?>">Input</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/price_history"); ?>">History</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-accounting role-purchasing" href="#"> Kurs &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/kurs_update"); ?>">Update</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/purchase_order/kurs_history"); ?>">History</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <!-- Work Sheet -->
            <li class="nav-item dropleft role-accounting role-admin-rm-pm role-ppic role-purchasing">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> WS </a>
                <ul class="dropdown-menu">
                    <h6 class="fw-bold text-center role-purchasing role-accounting role-ppic">Forecast</h6>
                    <li><a class="dropdown-item role-accounting role-purchasing role-ppic" onclick="gen_form_upload_forecast()" data-mdb-ripple-color="dark">Upload Data</a></li>
                    <li><a class="dropdown-item role-accounting role-purchasing role-ppic" href="#"> Lihat Data &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-purchasing role-ppic" href="<?php echo site_url("home/forecast/history_report"); ?>">History</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing role-ppic" href="<?php echo site_url("home/forecast/rmpm_analysis"); ?>">Analisa Kebutuhan</a></li>
                        </ul>
                    </li>
                    <h6 class="fw-bold text-center role-purchasing role-accounting">BOM</h6>
                    <li><a class="dropdown-item role-purchasing role-accounting" href="<?php echo site_url("home/bom/data"); ?>">Data BOM</a></li>
                    <li><a class="dropdown-item role-purchasing role-accounting" href="#"> Production Version &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-purchasing role-accounting" href="<?php echo site_url("home/pv/create_new"); ?>">Input</a></li>
                            <li><a class="dropdown-item role-purchasing role-accounting" href="<?php echo site_url("home/pv/rekap"); ?>">Rekap</a></li>
                        </ul>
                    </li>
                    <h6 class="fw-bold text-center role-admin-rm-pm role-accounting role-ppic role-purchasing">Worksheet</h6>
                    <li><a class="dropdown-item role-accounting role-ppic role-purchasing" href="#"> Buat &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-ppic" onclick="gen_form_upload_ws()" data-mdb-ripple-color="dark">Upload Data</a></li>
                            <li><a class="dropdown-item role-accounting role-ppic role-purchasing" href="<?php echo site_url("home/work_sheet/create_new"); ?>">Buat Baru</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-accounting role-purchasing role-ppic" href="#">Edit &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-accounting role-ppic" href="<?php echo site_url("home/work_sheet/tutup"); ?>">Tutup</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/work_sheet/cancel"); ?>">Cancel Good Issue </a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-ppic role-admin-rm-pm role-accounting role-purchasing" href="#"> Lihat Data &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-ppic role-admin-rm-pm role-accounting role-purchasing" href="<?php echo site_url("home/work_sheet/report_selisih"); ?>">Cek Semi & FG</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/work_sheet/report_connect"); ?>">Rincian Connect</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/work_sheet/detail"); ?>">Detail</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/work_sheet/print"); ?>">Cetak</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/work_sheet/sisa"); ?>">Laporan Sisa</a></li>
                            <li><a class="dropdown-item role-accounting role-purchasing" href="<?php echo site_url("home/work_sheet/informasi"); ?>">Laporan Informasi</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item role-ppic role-accounting role-admin-rm-pm" href="#"> Admin &raquo</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item role-ppic role-accounting role-admin-rm-pm" href="<?php echo site_url("home/work_sheet/admin_proses"); ?>">Proses WS</a></li>
                            <li><a class="dropdown-item role-ppic role-accounting role-admin-rm-pm" href="<?php echo site_url("home/work_sheet/admin_proses_history"); ?>">History Proses WS</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <!-- Work Sheet Khusus Stephanus -->
            <?php if (isset($_SESSION[SESSION_KARYAWAN_PENTA])) {
                if ($_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID] == 14063) { ?>
                    <li class="nav-item dropleft">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> WS </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#"> Admin &raquo</a>
                                <ul class="submenu dropdown-menu">
                                    <li><a class="dropdown-item" href="<?php echo site_url("home/work_sheet/admin_proses"); ?>">Proses WS</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url("home/work_sheet/admin_proses_history"); ?>">History Proses WS</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
            <?php   }
            } ?>

            <!-- Pengaturan -->
            <li class="nav-item dropleft">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Pengaturan </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="<?php echo site_url("home/setting/change_password"); ?>">Ganti Kata Sandi</a></li>
                    <li><a class="dropdown-item role-kepala" href="<?php echo site_url("home/setting/user_access"); ?>">Hak Akses</a></li>
                    <li><a class="dropdown-item role-kepala-gudang role-accounting " href="<?php echo site_url("home/user_access"); ?>">Akses</a></li>

                </ul>
            </li>

            <!-- Logout -->
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="<?php echo site_url("logout"); ?>">Log Out</a>
            </li>

    </div> <!-- navbar-collapse.// -->
</nav>
<!-- Navbar -->

<!-- Modal Upload Stock Material FG-->
<div class="f-aleo modal fade" id="mod_upl_stock_material" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD STOCK MATERIAL FG</h5>
            </div>
            <!--Body-->
            <div class="modal-body" style="margin:1%">
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Perhatian</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-1">1.</div>
                            <div class="col-sm-11 text-justify">
                                <span class="red-text fw-bold">(PENTING)</span> Upload Stock ini hanya digunakan untuk mengupload <span class="red-text fw-bold">STOCK AWAL</span> saja, jika ingin update stock silahkan pergi ke menu <span class="fw-bold">Stock > Mat. FG > Stock Opname</span>.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">2.</div>
                            <div class="col-sm-11 text-justify">
                                Silahkan mendownload file template yang disediakan dibawah, disarankan untuk tidak membuat file sendiri untuk menghindari salah input.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">3.</div>
                            <div class="col-sm-11 text-justify">
                                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">4.</div>
                            <div class="col-sm-11 text-justify">
                                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
                    <div class="col-sm-12">
                        <figure class="figure d-flex justify-content-center">
                            <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_material_stock.png"); ?>" />
                        </figure>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
                    <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 ">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/stock_barang_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            Template (.xlsx)
                        </a>
                    </div>
                    <div class="text-center col-sm-6">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/stock_barang_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            File Benar (.xlsx)
                        </a>
                    </div>
                </div>
                <br>
                <form id="form_upload_stock_material" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
                    <div class="row">
                        <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <input type="file" onchange="change_label('lbl_upl_stock_material','txt_upl_stock_material')" accept=".xlsx" name="txt_upl_stock_material" class="txt_upl_stock_material custom-file-input" id="txt_upl_stock_material" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label lbl_upl_stock_material" for="txt_upl_stock_material">File Data Stock Material (.xlsx)</label>
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
                        <div class="col-sm-3"></div>
                    </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="submit" id="btn_upl_stock_material" class="btn btn-primary">Upload</button>
            </div>
        </div>
        </form>
        <!--/.Content-->
    </div>
</div>
<!-- modal -->

<!-- Modal Upload Stock Material RM PM -->
<div class="f-aleo modal fade" id="mod_upl_stock_material_rm_pm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD STOCK MATERIAL RM PM</h5>
            </div>
            <!--Body-->
            <div class="modal-body" style="margin:1%">
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Perhatian</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-1">1.</div>
                            <div class="col-sm-11 text-justify">
                                <span class="red-text fw-bold">(PENTING)</span> Upload Stock ini hanya digunakan untuk mengupload <span class="red-text fw-bold">STOCK AWAL</span> saja, jika ingin update stock silahkan pergi ke menu <span class="fw-bold">Stock > Mat. RM PM > Stock Opname</span>.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">2.</div>
                            <div class="col-sm-11 text-justify">
                                Silahkan mendownload file template yang disediakan dibawah, disarankan untuk tidak membuat file sendiri untuk menghindari salah input.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">3.</div>
                            <div class="col-sm-11 text-justify">
                                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">4.</div>
                            <div class="col-sm-11 text-justify">
                                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">5.</div>
                            <div class="col-sm-11 text-justify">
                                Kolom "S" disini artinya adalah Konsinyasi. Jika Konsinyasi, tuliskan <span class='fw-bold'>K</span> pada kolom tersebut.
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
                    <div class="col-sm-12">
                        <figure class="figure d-flex justify-content-center">
                            <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_material_rm_pm_stock.png"); ?>" />
                        </figure>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
                    <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 ">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/stock_barang_rm_pm_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            Template (.xlsx)
                        </a>
                    </div>
                    <div class="text-center col-sm-6">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/stock_barang_rm_pm_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            File Benar (.xlsx)
                        </a>
                    </div>
                </div>
                <br>
                <form id="form_upload_stock_material_rm_pm" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
                    <div class="row">
                        <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <input type="file" onchange="change_label('lbl_upl_stock_material_rm_pm','txt_upl_stock_material_rm_pm')" accept=".xlsx" name="txt_upl_stock_material_rm_pm" class="txt_upl_stock_material_rm_pm custom-file-input" id="txt_upl_stock_material_rm_pm" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label lbl_upl_stock_material_rm_pm" for="txt_upl_stock_material_rm_pm">File Data Stock Material (.xlsx)</label>
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
                        <div class="col-sm-3"></div>
                    </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="submit" id="btn_upl_stock_material" class="btn btn-outline-success">Upload</button>
            </div>
        </div>
        </form>
        <!--/.Content-->
    </div>
</div>
<!-- modal -->

<!-- Modal Upload Purchase Order -->
<div class="f-aleo modal fade" id="mod_upl_purchase_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD PURCHASE ORDER</h5>
            </div>
            <!--Body-->
            <div class="modal-body" style="margin:1%">
                <div class="row">
                    <h2 class="text-center col-sm-12 fw-bold" id='txt_last_po_date'>Tanggal PO Terakhir: </h2>
                </div>
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Perhatian</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-1">1.</div>
                            <div class="col-sm-11 text-justify">
                                <span class="red-text fw-bold">(PENTING)</span> Upload ini digunakan untuk mengupload <span class="fw-bold">PO Baru</span> (Bisa banyak PO dalam 1x upload). Jika ingin merubah PO Quantity, maka pergi ke menu <span class="fw-bold">PO > Revisi Qty</span>.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">2.</div>
                            <div class="col-sm-11 text-justify">
                                Jika Material pada PO tersebut adalah <span class='fw-bold'>Konsinyasi</span>, nilai untuk kolom <span class='fw-bold'>Net Price</span> adalah 0.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">3.</div>
                            <div class="col-sm-11 text-justify">
                                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">4.</div>
                            <div class="col-sm-11 text-justify">
                                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
                    <div class="col-sm-12">
                        <figure class="figure d-flex justify-content-center">
                            <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_purchase_order.png"); ?>" />
                        </figure>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
                    <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 ">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/purchase_order_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            Template (.xlsx)
                        </a>
                    </div>
                    <div class="text-center col-sm-6">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/purchase_order_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            File Benar (.xlsx)
                        </a>
                    </div>
                </div>
                <br>
                <form id="form_upload_purchase_order" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
                    <div class="row">
                        <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <input type="file" onchange="change_label('lbl_upl_purchase_order','txt_upl_purchase_order')" accept=".xlsx" name="txt_upl_purchase_order" class="txt_upl_purchase_order custom-file-input" id="txt_upl_purchase_order" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label lbl_upl_purchase_order" for="txt_upl_purchase_order">File Data PO (.xlsx)</label>
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
                        <div class="col-sm-3"></div>
                    </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="submit" id="btn_upl_purchase_order" class="btn btn-outline-success">Upload</button>
            </div>
        </div>
        </form>
        <!--/.Content-->
    </div>
</div>
<!-- modal -->

<!-- Modal Upload Work Sheet -->
<div class="f-aleo modal fade" id="mod_upl_work_sheet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD WORK SHEET</h5>
            </div>
            <!--Body-->
            <div class="modal-body" style="margin:1%">
                <div class="row">
                    <h2 class="text-center col-sm-12 fw-bold" id='txt_last_ws_number'>Nomor WS Terakhir: </h2>
                </div>
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Perhatian</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-1">1.</div>
                            <div class="col-sm-11 text-justify">
                                <span class="red-text fw-bold">(PENTING)</span> Upload ini digunakan untuk mengupload <span class="fw-bold">WS Baru</span> (Bisa banyak WS dalam 1x upload)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">2.</div>
                            <div class="col-sm-11 text-justify">
                                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">3.</div>
                            <div class="col-sm-11 text-justify">
                                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
                    <div class="col-sm-12">
                        <figure class="figure d-flex justify-content-center">
                            <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_work_sheet.png"); ?>" />
                        </figure>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
                    <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 ">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/work_sheet_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            Template (.xlsx)
                        </a>
                    </div>
                    <div class="text-center col-sm-6">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/work_sheet_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                            File Benar (.xlsx)
                        </a>
                    </div>
                </div>
                <br>
                <form id="form_upload_work_sheet" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
                    <div class="row">
                        <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <input type="file" onchange="change_label('lbl_upl_work_sheet','txt_upl_work_sheet')" accept=".xlsx" name="txt_upl_work_sheet" class="txt_upl_work_sheet custom-file-input" id="txt_upl_work_sheet" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label lbl_upl_work_sheet" for="txt_upl_work_sheet">File Data WS (.xlsx)</label>
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
                        <div class="col-sm-3"></div>
                    </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="submit" id="btn_upl_work_sheet" class="btn btn-outline-success">Upload</button>
            </div>
        </div>
        </form>
        <!--/.Content-->
    </div>
</div>

<!-- Modal Upload Forecast -->
<div class="f-aleo modal fade" id="mod_upl_forecast" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD FORECAST</h5>
            </div>
            <!--Body-->
            <div class="modal-body" style="margin:1%">
                <!-- <div class="row">
                    <h2 class="text-center col-sm-12 fw-bold" id='txt_last_ws_number'>Nomor WS Terakhir: </h2>
                </div> -->
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Perhatian</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-1">1.</div>
                            <div class="col-sm-11 text-justify">
                                <span class="red-text fw-bold">(PENTING)</span> Upload ini digunakan untuk mengupload <span class="fw-bold">Forecast Baru</span> (Bisa banyak Forecast dalam 1x upload)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">2.</div>
                            <div class="col-sm-11 text-justify">
                                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-1">3.</div>
                            <div class="col-sm-11 text-justify">
                                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
                    <div class="col-sm-12">
                        <figure class="figure d-flex justify-content-center">
                            <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_forecast.png"); ?>" />
                        </figure>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
                    <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
                </div>
                <div class="row">
                    <div class="text-center col-sm-6 ">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/forecast_template.xlsx"); ?>" data-mdb-toggle="modal" data-mdb-ripple-color="dark">
                            Template (.xlsx)
                        </a>
                    </div>
                    <div class="text-center col-sm-6">
                        <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/forecast_file_benar.xlsx"); ?>" data-mdb-toggle="modal" data-mdb-ripple-color="dark">
                            File Benar (.xlsx)
                        </a>
                    </div>
                </div>
                <br>
                <form id="form_upload_forecast" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
                    <div class="row">
                        <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <input type="file" onchange="change_label('lbl_upl_forecast','txt_upl_forecast')" accept=".xlsx" name="txt_upl_forecast" class="txt_upl_forecast custom-file-input" id="txt_upl_forecast" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label lbl_upl_forecast" for="txt_upl_forecast">File Data Forecast (.xlsx)</label>
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
                        <div class="col-sm-3"></div>
                    </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="submit" id="btn_upl_forecast" class="btn btn-outline-success">Upload</button>
            </div>
        </div>
        </form>
        <!--/.Content-->
    </div>
</div>
<!-- modal -->
<script language='javascript'>
    $(document).ready(function() {
        $('#mod_upl_stock_material').appendTo("body");
        $('#mod_upl_stock_material_rm_pm').appendTo("body");
        $('#mod_upl_purchase_order').appendTo("body");
        $('#mod_upl_work_sheet').appendTo("body");
        $('#mod_upl_forecast').appendTo("body");
        $('.dropdown-submenu a.test').on("click", function(e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
    $(document).on('click', '.dropdown-menu', function(e) {
        e.stopPropagation();
    });

    // make it as accordion for smaller screens
    if ($(window).width() < 992) {
        $('.dropdown-menu a').click(function(e) {
            e.preventDefault();
            if ($(this).next('.submenu').length) {
                $(this).next('.submenu').toggle();
            }
            $('.dropdown').on('hide.bs.dropdown', function() {
                $(this).find('.submenu').hide();
            })
        });
    }
    $("#mod_upl_stock_material").on("hidden.bs.modal", function() {
        refresh_modal_stock_material();
    });
    $("#mod_upl_stock_material_rm_pm").on("hidden.bs.modal", function() {
        refresh_modal_stock_material();
    });
    $("#mod_upl_purchase_order").on("hidden.bs.modal", function() {
        refresh_modal_purchase_order();
    });
    $("#mod_upl_work_sheet").on("hidden.bs.modal", function() {
        refresh_modal_work_sheet();
    });
    $("#mod_upl_forecast").on("hidden.bs.modal", function() {
        refresh_modal_forecast();
    });

    function gen_form_upload_po() {
        $.ajax({
            url: site_url + "purchase_order/get_last_date",
            type: "POST",
            success: function(data) {
                $("#txt_last_po_date").html("Tanggal PO Terakhir: " + data);
                if (!$('#mod_upl_purchase_order').is(':visible'))
                    $('#mod_upl_purchase_order').modal('show');
            }
        });
    }

    function gen_form_upload_ws() {
        $.ajax({
            url: site_url + "work_sheet/get_last_number",
            type: "POST",
            success: function(data) {
                $("#txt_last_ws_number").html("Nomor WS Terakhir: " + data);
                if (!$('#mod_upl_work_sheet').is(':visible'))
                    $('#mod_upl_work_sheet').modal('show');
            }
        });
    }

    function gen_form_upload_forecast() {
        if (!$('#mod_upl_forecast').is(':visible'))
            $('#mod_upl_forecast').modal('show');
    }

    function refresh_modal_purchase_order() {
        $('#form_upload_purchase_order').trigger("reset"); // this will reset the form fields
        $(".txt_upl_purchase_order").val("");
        $(".lbl_upl_purchase_order").html("File Data PO (.xlsx)");
        $("#btn_upl_purchase_order").prop("disabled", false);
        $(".txt_desc_upload").html("");
    }

    function refresh_modal_work_sheet() {
        $('#form_upload_work_sheet').trigger("reset"); // this will reset the form fields
        $(".txt_upl_work_sheet").val("");
        $(".lbl_upl_work_sheet").html("File Data WS (.xlsx)");
        $("#btn_upl_work_sheet").prop("disabled", false);
        $(".txt_desc_upload").html("");
    }

    function refresh_modal_forecast() {
        $('#form_upload_forecast').trigger("reset"); // this will reset the form fields
        $(".txt_upl_forecast").val("");
        $(".lbl_upl_forecast").html("File Data Forecast (.xlsx)");
        $("#btn_upl_forecast").prop("disabled", false);
        $(".txt_desc_upload").html("");
    }

    function refresh_modal_stock_material() {
        $('#form_upload_stock_material').trigger("reset"); // this will reset the form fields
        $(".txt_upl_stock_material").val("");
        $(".lbl_upl_stock_material").html("File Data Stock Material (.xlsx)");
        $("#btn_upl_stock_material").prop("disabled", false);
        $(".txt_desc_upload").html("");
    }

    function refresh_modal_stock_material_rm_pm() {
        $('#form_upload_stock_material_rm_pm').trigger("reset"); // this will reset the form fields
        $(".txt_upl_stock_material_rm_pm").val("");
        $(".lbl_upl_stock_material_rm_pm").html("File Data Stock Material (.xlsx)");
        $("#btn_upl_stock_material").prop("disabled", false);
        $(".txt_desc_upload").html("");
    }

    $('#form_upload_stock_material').submit(function(e) {
        $("#btn_upl_stock_material").prop("disabled", true);
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: site_url + "material/upload_stock",
            type: "POST",
            data: form_data, //this is formData
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                $(".txt_desc_upload").removeClass("text-danger text-success");
                if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
                    $(".txt_desc_upload").addClass("text-success");
                } else {
                    $(".txt_desc_upload").addClass("text-danger");
                }
                $(".txt_desc_upload").html(data);
                $("#content").html("");
            },
            complete: function(data) {
                setTimeout(refresh_modal_stock_material, 1500);
            }
        });
    });
    $('#form_upload_stock_material_rm_pm').submit(function(e) {
        $("#btn_upl_stock_material").prop("disabled", true);
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: site_url + "material_rm_pm/upload_stock",
            type: "POST",
            data: form_data, //this is formData
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                $(".txt_desc_upload").removeClass("text-danger text-success");
                if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
                    $(".txt_desc_upload").addClass("text-success");
                } else {
                    $(".txt_desc_upload").addClass("text-danger");
                }
                $(".txt_desc_upload").html(data);
                $("#content").html("");
            },
            complete: function(data) {
                setTimeout(refresh_modal_stock_material_rm_pm, 1500);
            }
        });
    });
    $('#form_upload_purchase_order').submit(function(e) {
        $("#btn_upl_purchase_order").prop("disabled", true);
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: site_url + "purchase_order/upload",
            type: "POST",
            data: form_data, //this is formData
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                $(".txt_desc_upload").removeClass("text-danger text-success");
                if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
                    toast(data, Color.SUCCESS);

                    //$(".txt_desc_upload").addClass("text-success");
                } else {
                    toast(data, Color.DANGER);

                    $(".txt_desc_upload").addClass("text-danger");
                }
                $(".txt_desc_upload").html(data);
                $("#content").html("");
            },
            complete: function(data) {
                setTimeout(refresh_modal_purchase_order, 1500);
                gen_form_upload_po();
            }
        });
    });
    $('#form_upload_work_sheet').submit(function(e) {
        $("#btn_upl_work_sheet").prop("disabled", true);
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: site_url + "work_sheet/upload",
            type: "POST",
            data: form_data, //this is formData
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                $(".txt_desc_upload").removeClass("text-danger text-success");
                if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
                    //toast(data, Color.SUCCESS);

                    $(".txt_desc_upload").addClass("text-success");
                } else {
                    //toast(data, Color.DANGER);

                    $(".txt_desc_upload").addClass("text-danger");
                }
                $(".txt_desc_upload").html(data);
                $("#content").html("");
            },
            complete: function(data) {
                setTimeout(refresh_modal_work_sheet, 3000);
                gen_form_upload_ws();
            }
        });
    });
    $('#form_upload_forecast').submit(function(e) {
        $("#btn_upl_forecast").prop("disabled", true);
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: site_url + "forecast/upload",
            type: "POST",
            data: form_data, //this is formData
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                $(".txt_desc_upload").removeClass("text-danger text-success");
                if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
                    //toast(data, Color.SUCCESS);

                    $(".txt_desc_upload").addClass("text-success");
                } else {
                    //toast(data, Color.DANGER);

                    $(".txt_desc_upload").addClass("text-danger");
                }
                $(".txt_desc_upload").html(data);
                $("#content").html("");
            },
            complete: function(data) {
                setTimeout(refresh_modal_forecast, 3000);
                gen_form_upload_forecast();
            }
        });
    });
</script>