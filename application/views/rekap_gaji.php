<!DOCTYPE html>
<html lang="en"  class="role-kepala full-height">
<head>
  <title>Slip Gaji</title>
  <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
  ?>
</head>
<body class='f-aleo role-kepala'>
	<?php
    include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home");?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Rekap Gaji</li>
      </ol>
    </nav>
    <div id="content" style="margin-bottom:5%">
    </div>
  </div>
</body>
</html>

<script language="javascript">
  var site_url='<?php echo site_url();?>';
  
  function laporan_rekap_gaji(){
    $.ajax({
			type : "POST",
			url : site_url+"laporan_f/rekap_gaji",
			success:function(result){
				$("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });
    $("#content").removeClass("animated fadeInDown");
  }
  
  $(document).ready(function(){
    check_role();
    laporan_rekap_gaji();
  });
</script>


