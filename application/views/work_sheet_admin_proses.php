<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Proses Work Sheet</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Work Sheet</li>
        <li class="breadcrumb-item">Admin</li>
        <li class="breadcrumb-item active" aria-current="page">Proses</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>PROSES WORK SHEET</h1>
    <h5 class='f-aleo-bold text-center red-text'>Jika ingin mengubah nomor WS yang sudah terkunci, silahkan refresh halaman dengan menekan tombol <kbd>F5</kbd> pada keyboard</h4>
      <br>
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
          <div class='row'>
            <div class='col-sm-2 text-right'></div>
            <div class='col-sm-2 text-right'>No. WS Semi</div>
            <div class='col-sm-1 text-left '>:</div>
            <div class='col-sm-3 text-left'><input style='width:100%' type="text" id="txt_nomor_ws_semi" /></div>
            <div class='col-sm-4 text-right'></div>
          </div>
        </div>
        <div class="col-sm-1"></div>
      </div>
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 text-center">
          <button type="button" id="btn_generate_semi" class="center text-center btn btn-outline-primary" onclick="gen_detail_ws('SEMI')" data-mdb-ripple-color="dark">
            GENERATE
          </button>
        </div>
        <div class="col-sm-1"></div>
      </div>
      <br>
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
          <div id="content_semi"></div>
          <div id="content_fg"></div>
        </div>
        <div class="col-sm-1"></div>
      </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  var ctr_arr = 0;
  var product_code = "";
  var total_checker = 0;
  $(document).ready(function() {
    check_role();
  });

  /*function plus_product() {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "material/plus_admin_rm_pm",
      data: {
        c: ctr,
        p: product_code
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;
        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }
  */
  function roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2");
  }

  function gen_ws_admin_proses() {
    var kemasan = ($("#txt_kemasan").length) ? $("#txt_kemasan").val() : null;
    var total_tin = ($("#txt_tin").length) ? $("#txt_tin").val() : null;
    var berat = ($("#txt_berat").length) ? roundToTwo(parseFloat($("#txt_berat").val())) : null;
    var sg = 0;
    var ws_fg = ($("#txt_nomor_ws_fg").length) ? $("#txt_nomor_ws_fg").val() : null
    var ws_semi = $("#txt_nomor_ws_semi").val();
    var premix = $("#txt_premix").val();
    var makeup = $("#txt_makeup").val();
    var filling = $("#txt_filling").val();
    var is_true = true;

    if ($("#txt_sg").is(":visible")) {
      sg = 0;
      if ($("#txt_sg").val().length > 0) {
        sg = $("#txt_sg").val();
      }
      if (sg > 1.53 || sg < 1.40) {
        toast("Inputan SG melebihi batas yang ada (1.40-1.53)", Color.DANGER);
        return;
      }
    }

    //Salah satu kosong
    if ((premix == -1 && makeup != -1 && filling != -1) || (premix != -1 && makeup == -1 && filling != -1) ||
      (premix != -1 && makeup != -1 && filling == -1)) {
      toast("Ada 2 Checker yang sudah diisi, harap isi ketiganya!", Color.DANGER);
      is_true = false;
    }

    var tgl_persiapan = [];
    var tgl_premix = [];
    var tgl_makeup = [];
    var tgl_cm = [];
    var tgl_qc = [];
    var tgl_filling = [];

    var arr_tanggal = ["p", "pr", "m", "c", "q", "f"];
    var from_to = "";
    var consecutive_day = [];

    //Tanggal Tahapan
    for (var i = 0; i < 2; i++) {
      if (i == 0) {
        from_to = "f";
      } else
        from_to = "t";
      if ($("#txt_p" + from_to).val() != "") {
        tgl_persiapan[i] = $("#txt_p" + from_to).val();
      } else {
        toast("[PERSIAPAN] Ada tanggal ada yang masih kosong, harap dicek lagi!", Color.DANGER);
        is_true = false;
        return;
      }

      if ($("#txt_pr" + from_to).val() != "") tgl_premix[i] = $("#txt_pr" + from_to).val();
      else {
        toast("[PREMIX] Ada tanggal yang masih kosong, harap dicek lagi!", Color.DANGER);
        is_true = false;
        break;
      }

      if ($("#txt_m" + from_to).val() != "") tgl_makeup[i] = $("#txt_m" + from_to).val();
      else {
        toast("[MAKEUP] Ada tanggal yang masih kosong, harap dicek lagi!", Color.DANGER);
        is_true = false;
        break;
      }

      if ($("#txt_c" + from_to).val() != "") tgl_cm[i] = $("#txt_c" + from_to).val();
      else {
        toast("[CM] Ada tanggal yang masih kosong, harap dicek lagi!", Color.DANGER);
        is_true = false;
        break;
      }

      if ($("#txt_q" + from_to).val() != "") tgl_qc[i] = $("#txt_q" + from_to).val();
      else {
        toast("[QC] Ada tanggal yang masih kosong, harap dicek lagi!", Color.DANGER);
        is_true = false;
        break;
      }

      if ($("#txt_f" + from_to).val() != "") tgl_filling[i] = $("#txt_f" + from_to).val();
      else {
        toast("[FILLING] Ada tanggal yang masih kosong, harap dicek lagi!", Color.DANGER);
        is_true = false;
        break;
      }

    }
    if (tgl_persiapan[0] > tgl_persiapan[1]) {
      toast("[PERSIAPAN] Tanggal dari tidak boleh lebih besar dari tanggal sampai, harap dicek lagi!", Color.DANGER);
      is_true = false;
      return;
    }
    if (tgl_premix[0] > tgl_premix[1]) {
      toast("[PREMIX] Tanggal dari tidak boleh lebih besar dari tanggal sampai, harap dicek lagi!", Color.DANGER);
      is_true = false;
      return;
    }
    if (tgl_makeup[0] > tgl_makeup[1]) {
      toast("[MAKEUP] Tanggal dari tidak boleh lebih besar dari tanggal sampai, harap dicek lagi!", Color.DANGER);
      is_true = false;
      return;
    }
    if (tgl_cm[0] > tgl_cm[1]) {
      toast("[COLOURMATCHER] Tanggal dari tidak boleh lebih besar dari tanggal sampai, harap dicek lagi!", Color.DANGER);
      is_true = false;
      return;
    }
    if (tgl_qc[0] > tgl_qc[1]) {
      toast("[QC] Tanggal dari tidak boleh lebih besar dari tanggal sampai, harap dicek lagi!", Color.DANGER);
      is_true = false;
      return;
    }
    if (tgl_filling[0] > tgl_filling[1]) {
      toast("[FILLING] Tanggal dari tidak boleh lebih besar dari tanggal sampai, harap dicek lagi!", Color.DANGER);
      is_true = false;
      return;
    }

    //Push semua ke array cek apakah tanggal sudah berurutan
    consecutive_day.push(tgl_persiapan, tgl_premix, tgl_makeup, tgl_cm, tgl_qc, tgl_filling);
    for (var i = 0; i < consecutive_day.length - 1; i++) {
      if (consecutive_day[i] > consecutive_day[i + 1]) {
        toast("Ada tanggal yang tidak urut berdasarkan tahapannya, harap dicek lagi!!", Color.DANGER);
        is_true = false;
      }


    }

    if (!is_true) {

    } else if ((berat!=null)&&(berat > total_checker)) {
      toast("Total KG yang diinput lebih besar dengan total checker, harap dicek lagi!", Color.DANGER);
    } else {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "work_sheet/insert_admin_proses",
          data: {
            k: kemasan,
            t: total_tin,
            b: berat,
            wss: ws_semi,
            wsf: ws_fg,
            p: premix,
            m: makeup,
            f: filling,
            tp: tgl_persiapan,
            tpr: tgl_premix,
            tm: tgl_makeup,
            tc: tgl_cm,
            tq: tgl_qc,
            tf: tgl_filling
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              alert(result);
              //toast(result, Color.SUCCESS);
              reset_form();
              $("#txt_nomor_ws_semi").val("");
              $("#txt_nomor_ws_semi").prop("disabled", false);

            } else
              alert(result);
            //toast(result, Color.DANGER);
          }
        });
      }
    }
  }
  /*function gen_ws_admin_proses() {
    var arr_kemasan = new Array();
    var arr_tin = new Array();
    var arr_berat = new Array();
    var total_input = 0;
    var is_true = true;
    var ws_fg = $("#txt_nomor_ws_fg").val();
    var ws_semi = $("#txt_nomor_ws_semi").val();
    ctr_arr = 0;
    for (var i = 0; i < ctr; i++) {
      if ($("#dd_kemasan_" + i).length > 0) {
        ctr_arr++;
        arr_kemasan[i] = $("#dd_kemasan_" + i).val();
        if ($("#txt_tin_" + i).length > 0) {
          arr_tin[i] = $("#txt_tin_" + i).val();
        } else {
          is_true = false;
        }
        if ($("#txt_berat_" + i).length > 0) {
          arr_berat[i] = $("#txt_berat_" + i).val();
          total_input += parseFloat(arr_berat[i]);
        } else {
          is_true = false;
        }
      }
    }

    if (!is_true) {
      toast("Ada data yang masih kosong, harap dicek lagi!", Color.DANGER);
    } else if (total_input > total_checker) {
      toast("Total KG yang diinput lebih besar dengan total checker, harap dicek lagi!", Color.DANGER);
    } else {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "work_sheet/insert_admin_proses",
          data: {
            ak: arr_kemasan,
            at: arr_tin,
            ab: arr_berat,
            c: ctr_arr,
            wss: ws_semi,
            wsf: ws_fg
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
              $("#txt_nomor_ws").val("");
            } else
              toast(result, Color.DANGER);
          }
        });
      }
    }
  }*/

  function reset_form() {
    $("#content_semi").html("");
    $("#content_fg").html("");
    $("#btn_generate_semi").prop("disabled", false);
  }

  function update_berat(value) {
    $total_tin = $("#txt_tin").val();
    $kg = $("#txt_kg").val();
    $("#txt_berat").val(roundToTwo($total_tin * $kg * value));
  }

  function gen_detail_ws(tipe) {
    var nomor_ws = "";
    var nomor_ws_semi = "";
    if (tipe == "SEMI")
      nomor_ws = $("#txt_nomor_ws_semi").val();
    else {
      nomor_ws = $("#txt_nomor_ws_fg").val();
      nomor_ws_semi = $("#txt_nomor_ws_semi").val();
    }

    if (nomor_ws == "") {
      toast("Nomor WS tidak boleh kosong!", Color.DANGER);
      return;
    }

    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/view_admin_proses_detail",
      data: {
        n: nomor_ws,
        ns: nomor_ws_semi,
        t: tipe
      },
      dataType: "JSON",
      success: function(result) {
        if ((result.status).includes(Status.MESSAGE_KEY_SUCCESS)) {
          if (tipe == "<?php echo Worksheet::$S_SEMI; ?>") {
            $("#txt_nomor_ws_semi").prop("disabled", true);
            $("#btn_generate_semi").prop("disabled", true);
            $("#content_semi").html(result.content);
            total_checker = roundToTwo(parseFloat(result.total_checker));
          } else if (tipe == "<?php echo Worksheet::$S_FG; ?>") {
            $("#txt_nomor_ws_fg").prop("disabled", true);
            $("#btn_generate_fg").prop("disabled", true);
            $("#content_fg").html(result.content);
            product_code = result.product_code;
          }
          //$("#content").addClass("animated fadeInDown");
        } else {
          if (tipe == "<?php echo Worksheet::$S_SEMI; ?>") {
            $("#content_semi").html("");
          } else if (tipe == "<?php echo Worksheet::$S_FG; ?>") {
            $("#content_fg").html("");
          }
          toast(result.status, Color.DANGER);
        }
      }
    });
    //$("#content").removeClass("animated fadeInDown");
  }
</script>