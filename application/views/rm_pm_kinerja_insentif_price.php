<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
    <title>Harga Insentif Worksheet</title>
    <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
    ?>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

</head>

<body>
    <?php
    include("navigation.php");
    ?>
    <br>
    <div class="f-aleo animated fadeInDown">
        <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
                <li class="breadcrumb-item">RM PM</li>
                <li class="breadcrumb-item">Kinerja</li>
                <li class="breadcrumb-item active" aria-current="page">Harga Insentif</li>
            </ol>
        </nav>
        <h1 class='f-aleo-bold text-center'>HARGA INSENTIF RM PM</h1>
        <hr style="margin-left:5%;margin-right:5%">
        <br>
        <div class="row" style="margin-right:1%">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <h3 class='f-aleo-bold'>Filter</h1>
                    <div class="row f-aleo-bold">
                        <div class="col-sm-1 text-right">
                            <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
                        </div>
                        <div class="col-sm-5">
                            <span onclick="toggle_checkbox('tanggal')">Dari Tanggal</span><br>
                            <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
                        </div>
                        <div class="col-sm-5">
                            Sampai Tanggal<br>
                            <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                    <div class="row f-aleo-bold">
                        <div class="col-sm-1 text-right">
                            <input type="checkbox" onchange="toggle(value)" value="nomor_ws" id="cb_nomor_ws">
                        </div>
                        <div class="col-sm-4">
                            <span onclick="toggle_checkbox('nomor_ws')">Nomor WS </span>
                            <input type='text' disabled class="form-control f-aleo font-sm" id="dd_nomor_ws"> </input>
                        </div>
                        <div class="col-sm-6 text-right"></div>
                    </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <br>
        <div class="row" style="margin-right:1%">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 text-center">
                <button type="button" class="center text-center btn btn-outline-success" onclick="get_by_id()" data-mdb-ripple-color="dark">
                    GENERATE
                </button>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <hr style="margin-left:5%;margin-right:5%">
        <div id="content">
        </div>
    </div>
</body>

</html>
<script>
    $(document).ready(function() {
        check_role();
    });

    function toggle_checkbox(value) {
        if ($("#cb_" + value).prop('checked'))
            $("#cb_" + value).prop('checked', false);
        else
            $("#cb_" + value).prop('checked', true);
        toggle(value);
    }

    function toggle(value) {
        if (equal(value, "tanggal")) {
            if ($("#txt_tanggal_dari").prop('disabled') == true) {
                $("#txt_tanggal_dari").prop('disabled', false);
                $("#txt_tanggal_sampai").prop('disabled', false);
            } else {
                $("#txt_tanggal_dari").prop('disabled', true);
                $("#txt_tanggal_sampai").prop('disabled', true);
            }
        } else {
            if ($("#dd_" + value).prop('disabled') == true) {
                $("#dd_" + value).prop('disabled', false);
            } else {
                $("#dd_" + value).prop('disabled', true);
            }
        }
    }

    function get_by_id() {
        var dari_tanggal = null;
        var sampai_tanggal = null;
        var is_filtered = false;
        var nomor_ws = null;

        //dari tanggal sampai tanggal filter
        if ($("#cb_tanggal").prop("checked")) {
            if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
                if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
                    toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
                    return;
                } else {
                    dari_tanggal = $("#txt_tanggal_dari").val();
                    sampai_tanggal = $("#txt_tanggal_sampai").val();
                    is_filtered = true;
                }
            } else {
                toast("Ada tanggal yang masih kosong", Color.DANGER);
                return;
            }
        }
        //nomor_ws_from filter
        if ($("#cb_nomor_ws").prop("checked")) {
            if ($("#dd_nomor_ws").val() != "") {
                nomor_ws = $("#dd_nomor_ws").val();
                is_filtered = true;
            } else {
                toast("Nomor WS masih belum diisi", Color.DANGER);
                return;
            }
        }
        if (is_filtered) {
            $("#content").removeClass("animated fadeInDown");
            $.ajax({
                type: "POST",
                url: site_url + "kinerja/rmpm/view_harga_insentif",
                data: {
                    dt: dari_tanggal,
                    st: sampai_tanggal,
                    n: nomor_ws
                },
                success: function(result) {
                    $("#content").html(result);
                    $('#mastertable').DataTable({
                        "drawCallback": function(settings) {
                            check_role();
                        },
                        paging: true,
                        "pagingType": "full",
                        dom: 'Bfrtip',
                        buttons: [
                            'excel',
                        ],
                        "ordering": false,
                        "pageLength": 50
                    });

                    $(".buttons-excel span").text('Export ke Excel');
                    $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
                    $("#content").addClass("animated fadeInDown");

                }
            });
        } else
            toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
    }
</script>