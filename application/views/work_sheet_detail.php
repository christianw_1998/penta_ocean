<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Detail WS</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">WS</li>
        <li class="breadcrumb-item">Worksheet</li>
        <li class="breadcrumb-item">Lihat Data</li>
        <li class="breadcrumb-item active" aria-current="page">Detail</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>DETAIL WORKSHEET</h1>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 text-right'>No. Work Sheet</div>
          <div class='col-sm-1 text-left '>:</div>
          <div class='col-sm-3 text-left'><input style='width:100%' type="text" id="txt_nomor_ws" /></div>
          <div class='col-sm-2 text-right'></div>
        </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <button type="button" id="btn_generate" class="center text-center btn btn-outline-success" onclick="gen_detail_ws()" data-mdb-ripple-color="dark">
          GENERATE
        </button>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <br>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div id="content"></div>
      </div>
      <div class="col-sm-1"></div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function gen_detail_ws() {
    var nomor_ws = $("#txt_nomor_ws").val();
    if (nomor_ws == "") {
      toast("Nomor WS tidak boleh kosong!", Color.DANGER);
      return;
    }
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/view_detail",
      data: {
        n: nomor_ws
      },
      dataType: "JSON",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result.content);
        $('#tbl_detail_ws').DataTable({
          paging: false,
          orderable: false,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $(".buttons-excel").remove();
        /*if ((result.status).includes(Status.MESSAGE_KEY_SUCCESS)) {

        } else
          toast(result.status, Color.DANGER);*/
      }
    });
    $("#content").removeClass("animated fadeInDown");
  }

  function reset_form() {
    var kal = '<tr><td class="text-center font-sm f-aleo">Nomor</td><td class="text-center font-sm f-aleo">Kode Barang</td><td class="text-center font-sm f-aleo">Warna</td><td class="text-center font-sm f-aleo">Kemasan</td><td class="text-center font-sm f-aleo">Tin</td><td class="text-center font-sm f-aleo">Dus</td><td class="text-center font-sm f-aleo">Status</td></tr>'
    //$(".tabel_detail_product").dataTable.destroy();
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    $("#txt_title_po").html("");
    $("#txt_tanggal_terima").prop("disabled", true);
    $("#txt_tanggal_terima").val("");
    $("#txt_nomor_sj").val("");
    $("#txt_nomor_sj").prop("disabled", true);
    $("#btn_generate").prop("disabled", true);
    $(".buttons-excel").remove();
  }
</script>