<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Rekap GL</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Costing</li>
        <li class="breadcrumb-item">Payment</li>
        <li class="breadcrumb-item active" aria-current="page">Rekap GL</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>REKAP GL</h1>
    <hr style="margin-left:5%;margin-right:5%">

    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span> <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="gl" id="cb_gl">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('gl')">[Range] Nomor GL </span> <br>
              <input class="black-text" placeholder="Dari" disabled type="text" id="txt_gl_dari" />
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('gl')">[Range] Nomor GL </span> <br>
              <input class="black-text" placeholder="Sampai" disabled type="text" id="txt_gl_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="gl_list" id="cb_gl_list">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('gl_list')">[List] Nomor GL </span> <br>
              <!-- <select style="width:100%" id="dd_gl" disabled class="form-select" size="5" multiple aria-label="size 3 select example">

              </select> -->
              <button type="button" disabled id="dd_gl" class="center btn btn-outline-primary" onclick="open_gl_select()" data-mdb-ripple-color="dark">
                Cari GL
              </button><br>
              <textarea class="black-text" style='width:100%' disabled rows="4" type="text" id="txt_dd_gl"></textarea>

            </div>
            <div class="col-sm-5"></div>
            <div class="col-sm-1"></div>
          </div>
          <div class='text-center'>
            <button type="button" class="center btn btn-outline-primary" onclick="get_history()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
            <button type="button" id="btn_export" class="center btn btn-outline-primary" disabled onclick="export_to_txt()" data-mdb-ripple-color="dark">
              EXPORT
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">

    </div>
  </div>
</body>

</html>
<!-- Modal Preview -->
<div class="f-aleo modal fade" id="mod_select_gl" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">Filter GL</h5>
      </div>
      <!--Body-->
      <div class="modal-body" id="modal_table_gl" style="margin:1%">

      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" onclick="save_gl_select()" class="btn btn-outline-success">OK</button>
      </div>
    </div>
    </form>
    <!--/.Content-->
  </div>
</div>
<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';
  var selected = [];

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    } else if (equal(value, "gl")) {
      if ($("#txt_gl_dari").prop('disabled') == true) {
        $("#txt_gl_dari").prop('disabled', false);
        $("#txt_gl_sampai").prop('disabled', false);
      } else {
        $("#txt_gl_dari").prop('disabled', true);
        $("#txt_gl_sampai").prop('disabled', true);
      }
    } else if (equal(value, "gl_list")) {
      if ($("#dd_gl").prop('disabled') == true) {
        $("#dd_gl").prop('disabled', false);
      } else {
        $("#dd_gl").prop('disabled', true);
      }
    }
  }

  function get_history() {
    var dari_tanggal = null;
    var sampai_tanggal = null;

    var dari_gl = null;
    var sampai_gl = null;

    var list_gl = null;

    var is_filtered = false;

    //dari tanggal sampai tanggal filter
    if ($("#cb_tanggal").prop("checked")) {
      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }

    //dari GL sampai GL filter
    if ($("#cb_gl").prop("checked")) {
      if ($("#txt_gl_dari").val() != "" && $("#txt_gl_sampai").val() != "") {
        if (parseInt($("#txt_gl_dari").val()) > parseInt($("#txt_gl_sampai").val())) {
          toast("GL sampai harus lebih besar dari GL dari", Color.DANGER);
          return;
        } else {
          dari_gl = $("#txt_gl_dari").val();
          sampai_gl = $("#txt_gl_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("[Range] Ada GL yang masih kosong", Color.DANGER);
        return;
      }
    }

    //list GL filter
    if ($("#cb_gl_list").prop("checked")) {
      if ($("#dd_gl").val() != "") {
        list_gl = selected;
        is_filtered = true;

      } else {
        toast("[List] Ada GL yang masih kosong", Color.DANGER);
        return;
      }
    }

    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "payment/view_rekap_gl",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal,
          dg: dari_gl,
          sg: sampai_gl,
          lg: list_gl
        },
        success: function(result) {
          $("#content").html(result);
          $("#content").addClass("animated fadeInDown");
          $("#btn_export").prop("disabled", false);

          var divId = "content";
          var $divElement = $("#" + divId);
          if ($divElement.length === 0) {
            alert("Div tidak ditemukan!");
            return;
          }
          // Cari semua tabel dalam div dan hanya ambil tabel dengan data
          $divElement.find("table").each(function(index) {
            $(this).dataTable({
              "paging": false,
              "info": false,
              "ordering": false
            });
          });
        }
      });
    } else {
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
      $("#btn_export").prop("disabled", true);
    }
  }

  function export_to_txt() {
    var divId = "content";
    var fileNamePrefix = "Rekap GL " + new Date().toISOString().slice(0, 10);
    // Ambil elemen div berdasarkan ID menggunakan jQuery
    var $divElement = $("#" + divId);
    if ($divElement.length === 0) {
      alert("Div tidak ditemukan!");
      return;
    }
    var textContent = "";

    // Ambil semua teks dalam div, kecuali tabel mentah
    var clonedDiv = $divElement.clone();
    clonedDiv.find("table").remove();

    // Cari semua tabel dalam div dan hanya ambil tabel dengan data
    $divElement.find("table").each(function(index) {
      if ($(this).find("tr td").length > 0) { // Cek apakah tabel memiliki data
        textContent += "GL Account \t" + $("#txt_gl_" + index).val() + "\n";

        $(this).find("tr").each(function() {
          var rowText = [];
          $(this).find("th, td").each(function() {
            rowText.push($(this).text().trim());
          });
          textContent += rowText.join("\t") + "\n";
        });

        textContent += "\n"; // Tambahkan newline antar tabel
      }
    });

    var blob = new Blob([textContent], {
      type: "text/plain"
    });
    var url = URL.createObjectURL(blob);

    var $a = $("<a>")
      .attr("href", url)
      .attr("download", fileNamePrefix + ".txt")
      .appendTo("body");

    $a[0].click();
    $a.remove();
    URL.revokeObjectURL(url);
  }

  function fill_dd_gl() {
    $.ajax({
      type: "POST",
      url: site_url + "gl/view_table",
      success: function(result) {
        if (!$('#mod_select_gl').is(':visible')) {
          $("#modal_table_gl").html(result);
          $('#mastertable').DataTable({
            "drawCallback": function(settings) {
              check_role();
            },
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          $(".buttons-excel").remove();

        }
      }
    });

  }

  function open_gl_select() {
    //Restore GL
    $('#mastertable input').each(function() {
      if (selected.includes($(this).val())) {
        $(this).prop('checked', true);
      }
    });
    $('#mod_select_gl').modal('show');
  }

  function save_gl_select() {
    selected = [];
    var table = $('#mastertable').DataTable();
    table.rows().nodes().to$().find('input[type="checkbox"]').each(function() {
      if ($(this).prop("checked")) {
        selected.push($(this).val());
      }
    });
    $("#dd_gl").val(selected);
    $('#mod_select_gl').modal('hide');
    $("#txt_dd_gl").val(selected.join(", "));
  }

  $(document).ready(function() {
    check_role();
    fill_dd_gl();
    $('#mod_select_gl').appendTo("body");

  });
</script>