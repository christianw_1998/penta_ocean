<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Produksi</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInUp">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Master Produksi</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" id="btn_mod_karyawan" class="btn btn-outline-primary" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
        Upload Data (Batch)
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="insert_f_karyawan()" data-mdb-ripple-color="dark">
        Data Karyawan
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="get_master('karyawan')" data-mdb-ripple-color="dark">
        Tabel Karyawan
      </button>
      <!--<button type="button" class="btn btn-outline-primary role-kepala" onclick="laporan_gaji()" data-mdb-ripple-color="dark">
        Laporan Gaji
      </button>-->
    </div>
    <div id="content">
    </div>
    <div class="row" style="margin-top:-2%;margin-right:1%;margin-bottom:5%">
      <div class="col-sm-3"></div>
      <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
      <div class="col-sm-3"></div>
    </div>
  </div>
</body>

</html>
<!-- Modal Upload Data Karyawan -->
<div class="modal fade" id="mod_upl_karyawan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD DATA KARYAWAN</h5>
      </div>
      <!--Body-->
      <div class="modal-body" style="margin:1%">
        <div class="row">
          <div class="text-center col-sm-12 fw-bold">Perhatian</div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-1">1.</div>
              <div class="col-sm-11 text-justify">
                Silahkan mendownload file template yang disediakan dibawah, jangan membuat file sendiri untuk menghindari salah input.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">2.</div>
              <div class="col-sm-11 text-justify">
                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">3.</div>
              <div class="col-sm-11 text-justify">
                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">4.</div>
              <div class="col-sm-11 text-justify">
                Kolom NIK <span class="fw-bold">wajib diisi</span> agar bisa menjadi identifier karyawan.
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
          <div class="col-sm-12">
            <figure class="figure d-flex justify-content-center">
              <img class="border border-info figure-img rounded-0 img-fluid role-kepala" src="<?php echo base_url("asset/img/photo/example_file_excel_karyawan_kepala.png"); ?>" />
              <img class="border border-info figure-img rounded-0 img-fluid role-admin" src="<?php echo base_url("asset/img/photo/example_file_excel_karyawan_admin.png"); ?>" />
            </figure>
          </div>
        </div>
        <div class="row">
          <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
          <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
        </div>
        <div class="row">
          <div class="text-center col-sm-6 ">
            <a class="role-kepala btn btn-outline-success" download href="<?php echo base_url("asset/download/karyawan_kepala_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              Template (.xlsx)
            </a>
            <a class="role-admin btn btn-outline-success" download href="<?php echo base_url("asset/download/karyawan_admin_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              Template (.xlsx)
            </a>
          </div>
          <div class="text-center col-sm-6">
            <a class="role-kepala btn btn-outline-success" download href="<?php echo base_url("asset/download/karyawan_kepala_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              File Benar (.xlsx)
            </a>
            <a class="role-admin btn btn-outline-success" download href="<?php echo base_url("asset/download/karyawan_admin_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              File Benar (.xlsx)
            </a>
          </div>
        </div>
        <br>
        <form id="form_upload_karyawan" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
          <div class="row">
            <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
              <input type="file" onchange="change_label('lbl_upl_karyawan','txt_upl_karyawan')" accept=".xlsx" name="txt_upl_karyawan" class="txt_upl_karyawan custom-file-input" id="txt_upl_karyawan" aria-describedby="inputGroupFileAddon01">
              <label class="custom-file-label lbl_upl_karyawan" for="txt_upl_karyawan">File Data Karyawan (.xlsx)</label>
            </div>
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
            <div class="col-sm-3"></div>
          </div>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="submit" id="btn_upl_data_karyawan" class="btn btn-primary">Upload</button>
      </div>
    </div>
    </form>
    <!--/.Content-->
  </div>
</div>
<!-- modal -->


<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $("#mod_upl_karyawan").on("hidden.bs.modal", function() {
    refresh_modal();
  });

  function insert_f_karyawan() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "insert_f/karyawan",
      success: function(result) {
        $("#content").html(result);
        $("#content").addClass("animated fadeInDown");
        check_role();
        empty_insert_karyawan();
      }
    });
    check_role();
  }

  function check_nik() {
    var nik = $("#txt_nik").val();
    if (nik != "") {
      $.ajax({
        type: "POST",
        url: site_url + "karyawan/get_by_nik",
        data: {
          n: nik
        },
        dataType: "JSON",
        success: function(result) {
          $("#txt_status_nik").removeClass("green-text red-text");
          if (result.num_rows == 0) {
            $("#txt_status_nik").addClass("green-text");
            $("#txt_status_nik").val("NIK belum ada");

            empty_insert_karyawan();
          } else {
            $("#txt_status_nik").addClass("red-text");
            $("#txt_status_nik").val("NIK sudah ada!");
            $("#txt_nama").val(result.nama);
            $("#txt_npwp").val(result.npwp);
            $("#txt_s_cuti").val(result.saldo_cuti);
            if (typeof $("#txt_gp") !== 'undefined')
              $("#txt_gp").val(result.gaji_pokok);

            if (typeof $("#txt_tunjangan") !== 'undefined')
              $("#txt_tunjangan").val(result.tunjangan_jabatan);

            if (result.kat_id !== null)
              $("#dd_kategori").val("" + result.kat_id);
            else
              $("#dd_kategori").val("-1");

            if (result.id_role !== null)
              $("#dd_role").val("" + result.id_role);
            else
              $("#dd_role").val("-1");


            if (result.id_sub_kategori !== null)
              $("#dd_sub_kategori").val("" + result.id_sub_kategori);
            else
              $("#dd_sub_kategori").val("-1");

            $(".btn_modify_karyawan").html("UPDATE");
            if (result.is_outsource == 1)
              $("#cb_outsource").prop('checked', true);

            check_sub_kategori();
          }

        }
      });
    } else {
      $("#txt_status_nik").val("");
      empty_insert_karyawan();
    }
  }

  function check_sub_kategori() {
    var opt_kat = $("#dd_kategori").val();
    $(".opt_sub_kategori").hide();
    if (opt_kat != -1)
      $(".opt_sub_kat_" + opt_kat).show();
    else
      $("#dd_sub_kategori").val("-1");
    /*$("#dd_sub_kategori").empty();
    var kategori=$("#dd_kategori").val();
    var select="";
    select+="<option disabled value='-1'>TIDAK ADA</option>";
    $("#dd_sub_kategori").append(select);
    $.ajax({
      type : "POST",
      url : site_url+"get_sub_kat_by_id_kat",
      data : {i:kategori},
      dataType: "JSON",
      success:function(result){
          $("#dd_sub_kategori").empty();
          for(var i=0;i<result.length;i++){
            select+="<option value='"+result[i].id_sub_kategori+"'>"+result[i].nama.toUpperCase()+"</option>";
          }
          $("#dd_sub_kategori").append(select);
          if(id_sub_kategori!=null){
            $("#dd_sub_kategori").val(id_sub_kategori);
          }else{
            $("#dd_sub_kategori").val("-1");
          }
      }
    });
    */
  }

  function empty_insert_karyawan() {
    $("#txt_nama").val("");
    $("#txt_npwp").val("");
    if (typeof $("#txt_gp") !== 'undefined')
      $("#txt_gp").val(0);

    if (typeof $("#txt_tunjangan") !== 'undefined')
      $("#txt_tunjangan").val(0);

    $("#txt_s_cuti").val(0);

    $("#dd_role").val("-1");

    $("#dd_kategori").val("-1");

    $("#dd_sub_kategori").val("-1");
    check_sub_kategori();

    $("#cb_outsource").prop("checked", false);

    $(".btn_modify_karyawan").html("TAMBAH");

  }

  function refresh_page() {
    $(".txt_desc_upload").html("");
  }

  function insert_karyawan() {
    var nik = $("#txt_nik").val();
    var nama = $("#txt_nama").val();
    var npwp = $("#txt_npwp").val();

    var gp = -1;
    if (typeof $("#txt_gp") !== 'undefined')
      gp = $("#txt_gp").val();

    var tunjangan = -1;
    if (typeof $("#txt_tunjangan") !== 'undefined')
      tunjangan = $("#txt_tunjangan").val();

    var cuti = $("#txt_s_cuti").val();
    var role = $("#dd_role").val();
    var kategori = $("#dd_kategori").val();

    var cb_outsource = $("#cb_outsource").is(":checked");
    var is_outsource = -1;
    if (cb_outsource) is_outsource = 1;
    else is_outsource = 0;

    var sub_kategori = $("#dd_sub_kategori").val();
    $.ajax({
      type: "POST",
      data: {
        ni: nik,
        na: nama,
        np: npwp,
        g: gp,
        t: tunjangan,
        c: cuti,
        r: role,
        k: kategori,
        io: is_outsource,
        sk: sub_kategori
      },
      url: site_url + "insert/karyawan",
      success: function(result) {
        if (result.includes(Status.MESSAGE_KEY_SUCCESS))
          toast(result, Color.SUCCESS);
        else
          toast(result, Color.DANGER);
        //$(".txt_desc_upload").html(result);
        //$(".txt_desc_upload").addClass("text-success");
        $("#txt_nik").val("");
        $("#txt_status_nik").val("");
        empty_insert_karyawan();
        check_role();
      },
      complete: function(data) {
        //setTimeout(refresh_page,1500);
        check_role();
      }
    });
    check_role();
  }

  function refresh_modal() {
    //data karyawan
    $('#form_upload_karyawan').trigger("reset"); // this will reset the form fields
    $(".txt_upl_karyawan").val("");
    $(".lbl_upl_karyawan").html("File Data Karyawan (.xlsx)");

    $(".txt_desc_upload").html("");
  }

  $('#form_upload_karyawan').submit(function(e) {
    e.preventDefault();
    var form_data = new FormData(this);
    $.ajax({
      url: site_url + "home/master/karyawan/upload_file",
      type: "POST",
      data: form_data, //this is formData
      processData: false,
      contentType: false,
      cache: false,
      success: function(data) {
        $(".txt_desc_upload").removeClass("text-danger text-success");
        if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
          $(".txt_desc_upload").addClass("text-success");
        } else {
          $(".txt_desc_upload").addClass("text-danger");
        }
        $(".txt_desc_upload").html(data);
        $("#content").html("");
      },
      complete: function(data) {
        setTimeout(refresh_modal, 1500);
      }
    });
  });

  function laporan_gaji() {
    $.ajax({
      type: "POST",
      url: site_url + "laporan_f/gaji_karyawan",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });

    $("#content").removeClass("animated fadeInDown");
  }

  function kenaikan_gaji() {
    $.ajax({
      type: "POST",
      url: site_url + "kenaikan_gaji",
      success: function(result) {
        $("#content").html(result);
        $("#content").removeClass("animated fadeInDown");

        $('#mastertable').DataTable({
          paging: true,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $("#content").addClass("animated fadeInDown");
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
      }
    });
    check_role();
  }

  $(document).ready(function() {
    check_role();
    check_sub_kategori();
    $('#btn_mod_karyawan').click(function() {
      $('#mod_upl_karyawan').modal({
        show: true
      });
    });
    $('#mastertable').on('click', function() {
      check_role();
    });

  });
</script>