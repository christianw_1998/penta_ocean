<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Buat WS</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
  <style>
    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  </style>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">WS</li>
        <li class="breadcrumb-item">Worksheet</li>
        <li class="breadcrumb-item">Buat</li>
        <li class="breadcrumb-item active" aria-current="page">Buat Baru</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[RM PM]</h3>
    <h1 class='f-aleo-bold text-center'>BUAT WORKSHEET BARU</h1>
    <h2 class="text-center col-sm-12 fw-bold">Nomor WS Terakhir: <span id="txt_last_ws_number" class="red-text"></span></h2>
    <hr style="margin-left:5%;margin-right:5%">
    <br>
    <form id="form_test">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
          <h4 class="f-aleo-bold">Semi</h4>
          <datalist id='list_bom_semi'>
            <?php
            //BOM Datalist
            $data_bom = $this->bom_model->get(BOM::$TABLE_NAME,null,null,null,null,false);
            if ($data_bom->num_rows() > 0) {
                foreach ($data_bom->result_array() as $row_bom) {
                    if(substr($row_bom[BOM::$ID], 0, 4) != '8000'){ ?>
                      <option value="<?php echo $row_bom[BOM::$ID]; ?>"><?php echo $row_bom[BOM::$DESCRIPTION]; ?></option>
                <?php }
                }
            } ?>
          </datalist>
          <table>
            <tr>
              <td class="fw-bold text-right">Kode Produk</td>
              <td>:</td>
              <td><input type="text" list="list_bom_semi" onkeyup="get_active_bom_by_semi_id(this.value)" id="txt_kode_material" class="form-control"></td>
            </tr>
            <tr>
              <td class="fw-bold text-right">Alt BOM yang dipakai</td>
              <td>:</td>
              <td><input type="hidden" id="txt_id_pv">
                <input type="text" id="txt_alt_bom" value="Alt BOM belum ditemukan" disabled class="text-right red-text form-control">
              </td>
            </tr>
            <tr>
              <td class="fw-bold text-right">Qty (KG)</td>
              <td>:</td>
              <td><input type="number" onkeyup="calculate_total()" id="txt_qty" class="text-right form-control"></td>
            </tr>
          </table>
        </div>
        <div class="col-sm-1"></div>
      </div>

      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
          <h4 class="f-aleo-bold">FG</h4>
          <table class="tabel_detail_product" style="visibility:hidden">
            <tr>
              <td class="text-center font-sm f-aleo">Nomor</td>
              <td class="text-center font-sm f-aleo">Kode FG</td>
              <td class="text-center font-sm f-aleo">Deskripsi</td>
              <td class="text-center font-sm f-aleo">Alt BOM yang Aktif</td>
              <td class="text-center font-sm f-aleo">Net</td>
              <td class="text-center font-sm f-aleo">Tin</td>
              <td class="text-center font-sm f-aleo">Total KG</td>
            </tr>
          </table>
          <h5 class="fw-bold text-right">Total: <span id="txt_total" class="sp green-text"></span> KG</h5>
          <div class='text-center'>
            <button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
              Tambah
            </button>
            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="generate()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
        <div class="col-sm-1"></div>
      </div>
      <hr style="margin-left:5%;margin-right:5%">

      <div id="content" style="margin-left:1%;margin-right:1%">
      </div>
    </form>
  </div>
</body>

</html>

<script>
  var ctr = 0;
  var is_rm = "";
  $(document).ready(function() {
    check_role();
    calculate_total();
    get_last_number();
  });

  function get_active_bom_by_semi_id(kode_pro) {
    if (!kode_pro.match("^800") && !kode_pro.match("^900")) {
      $.ajax({
        type: "POST",
        url: site_url + "pv/semi_get_active_by_product_code",
        data: {
          pc: kode_pro
        },
        dataType: "json",
        success: function(result) {
          $("#txt_alt_bom").removeClass("green-text red-text");
          if (result.num_rows != 0) {
            $("#txt_alt_bom").addClass("green-text");
            $("#txt_id_pv").val(result["<?php echo BOM_Production_Version::$ID; ?>"]);
            $("#txt_alt_bom").val(result["<?php echo BOM_Alt::$ALT_NO; ?>"]);
            is_rm = result["is_rm"];

          } else {
            $("#txt_alt_bom").addClass("red-text");
            $("#txt_alt_bom").val("Alt BOM belum ditemukan");
          }
        }
      });
    }
  }

  function get_detail_fg(ctr) {
    var kode_pro = $("#txt_kode_fg_" + ctr).val();
    if (kode_pro.match("^800") || kode_pro.match("^900")) {
      $.ajax({
        type: "POST",
        url: site_url + "pv/fg_get_by_product_code",
        data: {
          pc: kode_pro
        },
        dataType: "json",
        success: function(result) {
          $("#txt_desc_fg_" + ctr).removeClass("green-text red-text");
          $("#txt_bom_fg_" + ctr).removeClass("green-text red-text");
          if (result.num_rows != 0) {
            $("#txt_desc_fg_" + ctr).addClass("green-text");
            $("#txt_desc_fg_" + ctr).val(result["<?php echo BOM::$DESCRIPTION; ?>"]);

            $("#txt_pv_fg_" + ctr).val(result["<?php echo BOM_Alt::$ID; ?>"]);
            $("#txt_kg_fg_" + ctr).val(result["<?php echo BOM_Alt_Recipe::$QTY ?>"]);

            $("#txt_bom_fg_" + ctr).addClass("green-text");
            $("#txt_bom_fg_" + ctr).val(result["<?php echo BOM_Alt::$ALT_NO; ?>"]);

          }
        }
      });
    }
  }

  function is_number_key(evt, ctr) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    //console.log(evt.keyCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      evt.target.value = event.target.value.replace('.', '');
      return false;
    }
    calculate_total_kg_per_product(ctr);
    return true;
  }

  function get_last_number() {
    $.ajax({
      url: site_url + "work_sheet/get_last_number",
      type: "POST",
      success: function(data) {
        $("#txt_last_ws_number").html(data);
      }
    });
  }

  function plus_product() {

    var selected_idx = [];

    //Simpan semua value index tipe harga
    for (var i = 0; i < ctr; i++) {
      if ($("#dd_currency_" + i).length > 0) {
        selected_idx[i] = $("#dd_currency_" + i).prop("selectedIndex");
      }
    }

    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/plus",
      data: {
        c: ctr,
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;

        //Ubah kembali semua Select menjadi index awal
        for (var i = 0; i < ctr; i++) {
          /*if (typeof selected_idx[i] !== "undefined") {
            $("#dd_currency_" + i).prop("selectedIndex", selected_idx[i]);
          }*/
        }

        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function calculate_total_kg_per_product(ctr) {
    var kg = $("#txt_kg_fg_" + ctr).val();
    var tin = $("#txt_tin_" + ctr).val();

    var tampil = parseFloat(kg * tin).toFixed(2);
    $("#txt_kg_" + ctr).val(tampil);

    calculate_total();
  }

  function calculate_total() {
    var total = 0;
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_kg_" + i).length > 0) {
        total += parseFloat($("#txt_kg_" + i).val());
      }
    }
    var tampil_total = total;
    $("#txt_total").removeClass("red-text green-text");
    $("#txt_total").html(parseFloat(tampil_total).toFixed(2));

    if ($("#txt_qty").val() != "") {
      var semi_qty = $("#txt_qty").val();
      if (semi_qty >= parseFloat(total) && ctr > 0) {
        $("#txt_total").addClass("green-text");
      } else {
        $("#txt_total").addClass("red-text");
      }
    }

  }

  function generate() {
    var total = 0;
    var semi_id = $("#txt_kode_material").val();
    var semi_qty = $("#txt_qty").val();
    var fg_id = [];
    var fg_qty = [];
    var fg_ctr = 0;

    //if (!$("#txt_total").hasClass("green-text")) {
    //  toast("Total WS ada yang salah, silahkan cek lagi", Color.DANGER);
    //  return;
    //} else {
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_kg_" + i).length > 0) {
        fg_id[fg_ctr] = $("#txt_kode_fg_" + i).val();
        if ($("#txt_tin_" + i).val() != "" || $("#txt_tin_" + i).val() != 0)
          fg_qty[fg_ctr] = parseFloat($("#txt_tin_" + i).val());
        else {
          toast("Ada WS FG yang tin nya tidak valid, silahkan cek lagi", Color.DANGER);
          return;
        }

        total += parseFloat($("#txt_kg_" + i).val());
        fg_ctr += 1;
      }
      //Cek Deskripsi FG harus mengandung Semi
      if (!$("#txt_desc_fg_" + i).val().includes(semi_id)) {
        toast("Ada WS FG yang materialnya tidak cocok dengan WS Semi, silahkan cek lagi", Color.DANGER);
        return;
      }
    }
    if (!is_rm && fg_ctr == 0) {
      toast("WS Semi bukan RM, silahkan buat WS FG terlebih dahulu", Color.DANGER);
      return;
    }
    if (semi_qty % 100 == 0 || (is_rm && semi_qty % 50 == 0)) {
      var c = confirm("Apakah anda yakin ingin membuat WS? terdapat selisih sebesar " + Math.round(semi_qty - total) + " KG");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "work_sheet/create_new",
          data: {
            si: semi_id,
            sq: semi_qty,
            fi: fg_id,
            fq: fg_qty,
            c: fg_ctr,
          },
          success: function(result) {
            toast(result, Color.SUCCESS);
            reset_form();
          }
        });
      }
    } else
      toast("Qty WS Semi harus kelipatan 100", Color.DANGER);
  }

  function reset_form() {
    $("#form_test").trigger("reset");
    $("#txt_alt_bom").removeClass("green-text");
    $("#txt_alt_bom").addClass("red-text");

    var kal = '<tr><td class="text-center font-sm f-aleo">Nomor</td><td class="text-center font-sm f-aleo">Kode FG</td><td class="text-center font-sm f-aleo">Deskripsi</td><td class="text-center font-sm f-aleo">Alt BOM yang Aktif</td><td class="text-center font-sm f-aleo">Net</td><td class="text-center font-sm f-aleo">Tin</td><td class="text-center font-sm f-aleo">Total KG</td></tr>';
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    ctr = 0;
    $("#txt_total").removeClass("red-text green-text");
    $("#txt_total").html("0.00");
    get_last_number();
  }
</script>