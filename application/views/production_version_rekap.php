<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Rekap PV</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">BOM</li>
        <li class="breadcrumb-item">Production Version</li>
        <li class="breadcrumb-item active" aria-current="page">Rekap</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>REKAP PRODUCTION VERSION</h1>
    <hr style="margin-left:5%;margin-right:5%">

    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-12">Valid dari</div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal_vf" id="cb_tanggal_vf">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal_vf')"> Dari Tanggal </span><br>
              <input class="black-text" disabled type="date" id="txt_tanggal_vf_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_vf_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>


          <div class="row f-aleo-bold">
            <div class="col-sm-12">Valid sampai</div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal_vt" id="cb_tanggal_vt">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal_vt')"> Dari Tanggal</span> <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_vt_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_vt_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>

          <div class="row f-aleo-bold">
            <div class="col-sm-12">Kode Produk</div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="kode_produk" id="cb_kode_produk">
            </div>
            <div class="col-sm-4">
              <input type="text" class="form-control f-aleo font-sm" id="txt_kode_produk" disabled placeholder="Kode Produk BOM">
            </div>
            <div class="col-sm-1"></div>
          </div>
          <br>
          <div class='text-center'>
            <button type="button" class="center btn btn-outline-primary" onclick="get_history()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <hr style="margin-left:5%;margin-right:5%">

    <div id="content">
    </div>
  </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal_vf")) {
      if ($("#txt_tanggal_vf_dari").prop('disabled') == true) {
        $("#txt_tanggal_vf_dari").prop('disabled', false);
        $("#txt_tanggal_vf_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_vf_dari").prop('disabled', true);
        $("#txt_tanggal_vf_sampai").prop('disabled', true);
      }
    } else if (equal(value, "tanggal_vt")) {
      if ($("#txt_tanggal_vt_dari").prop('disabled') == true) {
        $("#txt_tanggal_vt_dari").prop('disabled', false);
        $("#txt_tanggal_vt_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_vt_dari").prop('disabled', true);
        $("#txt_tanggal_vt_sampai").prop('disabled', true);
      }
    } else {
      if ($("#txt_" + value).prop('disabled') == true) {
        $("#txt_" + value).prop('disabled', false);
      } else {
        $("#txt_" + value).prop('disabled', true);
      }
    }
  }

  function get_history() {
    var vf_dari_tanggal = null;
    var vf_sampai_tanggal = null;
    var vt_dari_tanggal = null;
    var vt_sampai_tanggal = null;
    var is_filtered = false;
    var kode_produk = null;

    //dari tanggal_VF sampai tanggal_VF filter
    if ($("#cb_tanggal_vf").prop("checked")) {
      if ($("#txt_tanggal_vf_dari").val() != "" && $("#txt_tanggal_vf_sampai").val() != "") {
        if ($("#txt_tanggal_vf_dari").val() > $("#txt_tanggal_vf_sampai").val()) {
          toast("(Valid dari) Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          vf_dari_tanggal = $("#txt_tanggal_vf_dari").val();
          vf_sampai_tanggal = $("#txt_tanggal_vf_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }

    //dari tanggal_VT sampai tanggal_VT filter
    if ($("#cb_tanggal_vt").prop("checked")) {
      if ($("#txt_tanggal_vt_dari").val() != "" && $("#txt_tanggal_vt_sampai").val() != "") {
        if ($("#txt_tanggal_vt_dari").val() > $("#txt_tanggal_vt_sampai").val()) {
          toast("(Valid sampai) Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          vt_dari_tanggal = $("#txt_tanggal_vt_dari").val();
          vt_sampai_tanggal = $("#txt_tanggal_vt_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }

    //Kode Produk Filter
    if ($("#cb_kode_produk").prop("checked")) {
      if ($("#txt_kode_produk").val() != "") {
        kode_produk = $("#txt_kode_produk").val();
        is_filtered = true;
      } else {
        toast("Kode produk masih belum diisi", Color.DANGER);
        return;
      }
    }

    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "pv/view_rekap",
        data: {
          df: vf_dari_tanggal,
          sf: vf_sampai_tanggal,
          dt: vt_dari_tanggal,
          st: vt_sampai_tanggal,
          kp: kode_produk
        },
        success: function(result) {
          $("#content").html(result);
          $('#mastertable').DataTable({
            "order": [
              [0, "desc"]
            ],
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          //$(".buttons-excel").remove();
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");
        }
      });
    } else
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
  }


  $(document).ready(function() {
    check_role();

    $("#txt_tanggal_vf_dari").css("color", "black");
    $("#txt_tanggal_vf_sampai").css("color", "black");

    $("#txt_tanggal_vt_dari").css("color", "black");
    $("#txt_tanggal_vt_sampai").css("color", "black");

  });
</script>