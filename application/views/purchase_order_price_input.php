<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Input Price List</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Costing</li>
        <li class="breadcrumb-item">Price List</li>
        <li class="breadcrumb-item active" aria-current="page">Input</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>INPUT PRICE LIST</h1>
    <hr style="margin-left:5%;margin-right:5%">

    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-12">
          <table class="tabel_detail_product" style="visibility:hidden">
            <tr>
              <td class="text-center font-sm f-aleo">Nomor</td>
              <td class="text-center font-sm f-aleo">ID Vendor</td>
              <td class="text-center font-sm f-aleo">Vendor</td>
              <td class="text-center font-sm f-aleo">Kode Material</td>
              <td class="text-center font-sm f-aleo">Deskripsi</td>
              <td class="text-center font-sm f-aleo">Tipe</td>
              <td class="text-center font-sm f-aleo">Valid</td>
              <td class="text-center font-sm f-aleo">Harga</td>
              <td class="text-center font-sm f-aleo">Aksi</td>
            </tr>'
          </table>
          <div class='text-center'>
            <button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
              Tambah
            </button>
            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="generate()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
    var today = new Date();

    var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
    $("#txt_tanggal").val(date);
  });

  function plus_product() {

    var selected_idx = [];
    var selected_idx_konsinyasi = [];

    //Simpan semua value index tipe harga
    for (var i = 0; i < ctr; i++) {
      if ($("#dd_currency_" + i).length > 0) {
        selected_idx[i] = $("#dd_currency_" + i).prop("selectedIndex");
      }
      if ($("#dd_konsinyasi_" + i).length > 0) {
        selected_idx_konsinyasi[i] = $("#dd_konsinyasi_" + i).prop("selectedIndex");
      }
    }

    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "material_rm_pm/plus",
      data: {
        c: ctr,
        t: "<?php echo RMPM_PLUS_TYPE_INPUT_PRICE ?>"
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;

        //Ubah kembali semua Select menjadi index awal
        for (var i = 0; i < ctr; i++) {
          if (typeof selected_idx[i] !== "undefined") {
            $("#dd_currency_" + i).prop("selectedIndex", selected_idx[i]);
          }
          if (typeof selected_idx_konsinyasi[i] !== "undefined") {
            $("#dd_konsinyasi_" + i).prop("selectedIndex", selected_idx_konsinyasi[i]);
          }
        }

        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function check_product(params) {
    var kode_panggil = $("#txt_kode_panggil_" + params).val();
    $("#td_konsinyasi_" + params).html("");
    if (kode_panggil != "") {
      $.ajax({
        type: "POST",
        url: site_url + "material_rm_pm/get",
        data: {
          kp: kode_panggil,
          t: "<?php echo RMPM_GET_TYPE_MUTASI; ?>"
        },
        dataType: "JSON",
        success: function(result) {
          if (result.num_rows != 0) {
            $("#txt_desc_mat_" + params).val(result.description);
            $("#txt_desc_mat_" + params).addClass("green-text");
            $("#txt_id_material_" + params).val(result.id_material_rm_pm);
            if (result.ctr_konsinyasi > 0) { //ada konsinyasi
              var select = "<select onchange=change_stock_konsinyasi(" + params + ") class='form-control f-aleo' id='dd_konsinyasi_" + params + "'>";
              for (var i = 0; i < result.ctr_konsinyasi; i++) {
                select += "<option value='" + result.konsinyasi_qty[i] + "'>" + result.konsinyasi_nama[i] + "</option>";
              }
              select += "</select>";
              for (var i = 0; i < result.ctr_konsinyasi; i++) {
                select += '<input type="hidden" id="txt_id_konsinyasi_' + params + '_' + i + '" value="' + result.konsinyasi_id[i] + '"/>';
              }
              $("#td_konsinyasi_" + params).html(select);
            } else {
              $("#td_konsinyasi_" + params).html("");
            }
          } else {
            $("#txt_desc_mat_" + params).val("Material belum ditemukan!");
            $("#txt_desc_mat_" + params).removeClass('green-text');
            $("#txt_desc_mat_" + params).addClass('red-text');
          }

        }
      });
    }
  }

  function change_stock_konsinyasi(ctr) {
    //$("#txt_view_stock_" + ctr).html(value);
    $("#txt_stock_" + ctr).val($("#dd_konsinyasi_" + ctr).val());
    //check_adjustment_qty(ctr);
  }

  function check_vendor(params) {
    var id_vendor = $("#txt_kode_vendor_" + params).val();
    if (id_vendor != "") {
      $.ajax({
        type: "POST",
        url: site_url + "vendor/get",
        data: {
          iv: id_vendor,
        },
        dataType: "JSON",
        success: function(result) {
          if (result.num_rows != 0) {
            $("#txt_desc_vendor_" + params).val(result["<?php echo Vendor::$NAMA; ?>"]);
            $("#txt_desc_vendor_" + params).addClass("green-text");
            $("#txt_id_vendor_" + params).val(result["<?php echo Vendor::$ID; ?>"]);
          } else {
            $("#txt_desc_vendor_" + params).val("Vendor tidak ditemukan");
            $("#txt_desc_vendor_" + params).removeClass('green-text');
            $("#txt_desc_vendor_" + params).addClass('red-text');

          }
        }
      });
    }
  }

  function reset_form() {
    var kal = '<tr><td class="text-center font-sm f-aleo">Nomor</td><td class="text-center font-sm f-aleo">ID Vendor</td><td class="text-center font-sm f-aleo">Vendor</td><td class="text-center font-sm f-aleo">Kode Material</td><td class="text-center font-sm f-aleo">Material</td><td class="text-center font-sm f-aleo">Valid</td><td class="text-center font-sm f-aleo">Harga</td><td class="text-center font-sm f-aleo"> Aksi </td></tr>';
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    ctr = 0;
    $("#rbSO").prop("checked", true);
    $("#txt_kode_GR").html("");
    $("#btn_generate").prop("disabled", false);
    $("#btn_tambah_barang").prop("disabled", false);
    $(".buttons-excel").remove();
    $("#txt_keterangan").val("");
  }

  function generate() {
    var arr_id_mat = new Array();
    var arr_id_vendor = new Array();
    var arr_price = new Array();
    var arr_validfrom = new Array();
    var arr_validto = new Array();
    var arr_currency = new Array();
    var idx = 0;
    var is_true = true;


    for (var i = 0; i < ctr; i++) {
      is_true = true;
      if ($("#txt_kode_panggil_" + i).length > 0) {
        if ($("#dd_konsinyasi_" + i).length > 0) {
          arr_id_mat[idx] = $("#txt_id_konsinyasi_" + i + "_" + $("#dd_konsinyasi_" + i)[0].selectedIndex).val();
        } else
          arr_id_mat[idx] = parseInt($("#txt_id_material_" + i).val());
      } else {
        is_true = false;
      }

      if ($("#txt_kode_vendor_" + i).length > 0) {
        arr_id_vendor[idx] = parseInt($("#txt_id_vendor_" + i).val());
      } else {
        is_true = false;
      }

      if ($("#txt_valid_from_" + i).val() != "" && $("#txt_valid_to_" + i).val() != "") {
        if ($("#txt_valid_from_" + i).val() > $("#txt_valid_to_ " + i).val()) {
          toast("Ada tanggal yang tidak valid, silahkan cek lagi", Color.DANGER);
          return;
        }
        arr_validfrom[idx] = $("#txt_valid_from_" + i).val();
        arr_validto[idx] = $("#txt_valid_to_" + i).val();
      } else {
        is_true = false;
      }
      if ($("#txt_price_" + i).val() != "") {
        arr_currency[idx] = $("#dd_currency_" + i).val();
        arr_price[idx] = parseFloat($("#txt_price_" + i).val());
      } else {
        is_true = false;
      }
      if (is_true)
        idx++;
    }
    if (is_true && idx != 0) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "purchase_order/price_insert",
          data: {
            am: arr_id_mat,
            av: arr_id_vendor,
            af: arr_validfrom,
            at: arr_validto,
            ac: arr_currency,
            ap: arr_price,
            c: idx
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
            } else
              toast(result, Color.DANGER);
          }
        });
      }
    } else
      toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
  }
</script>