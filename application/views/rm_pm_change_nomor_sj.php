<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Ganti Nomor SJ PO</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class='f-aleo'>
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Purchase Order</li>
        <li class="breadcrumb-item">Edit</li>
        <li class="breadcrumb-item active" aria-current="page">Ganti Nomor SJ</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[Purchase Order]</h3>
    <h1 class='f-aleo-bold text-center'>GANTI NOMOR SJ</h1>
    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <table class="table table-borderless">
            <tr>
              <td class='align-middle text-right font-sm'>Nomor GR Lama (50xxxxxxxx)</td>
              <td class='align-middle text-right font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <input style="width:100%" type="text" onkeyup="check_no_sj(this.value)" id="txt_old_sj" />
              </td>
            </tr>
            <tr class='align-middle text-left font-sm'>
              <td class='align-middle text-right font-sm'>Nomor Surat Jalan Baru</td>
              <td class='align-middle text-right font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <input style="width:100%" type="text" id="txt_new_sj" />
              </td>
            </tr>
          </table>
          <div class="text-center" style="margin-bottom:2%">
            <button type="button" id="btn_change_nomor_sj" onclick='change_nomor_sj()' class="btn btn-outline-primary">
              Ganti Nomor Surat Jalan
            </button>
          </div>
        </div>
        <div class="col-sm-2"></div>

      </div>
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {
    check_role();
  });

  function reset_form() {
    $("#txt_old_sj").val("");
    $("#txt_new_sj").val("");
  }

  function check_no_sj(no_gr) {
    $.ajax({
      type: "POST",
      url: site_url + "purchase_order/get_sj_by_gr",
      data: {
        i: no_gr
      },
      success: function(result) {
        if (result.length > 0) {
          $("#txt_new_sj").val(result);
        } else {
          $("#txt_new_sj").val("");

        }
      }
    });
  }

  function change_nomor_sj() {
    var old_sj = $("#txt_old_sj").val();
    var new_sj = $("#txt_new_sj").val();
    var c = confirm("Apakah anda yakin?");
    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "purchase_order/change_nomor_sj",
        data: {
          os: old_sj,
          ns: new_sj
        },
        success: function(result) {
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            toast(result, Color.SUCCESS);
            reset_form();
          } else {
            toast(result, Color.DANGER);
          }
        }
      });
    }
  }
</script>