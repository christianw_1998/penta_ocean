<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sistem Manajemen Stock</title>
	<?php
		include("library.php");
		include("library_materialize.php");
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<style>
		body,html{
			height: 100%;
			animation-name: anim-intro;
			animation-duration: 1.5s;
		}
		@keyframes anim-intro {
			from {opacity: 0;}
			to {opacity: 1;}
		}
		@keyframes bg-intro {
			from {background-color: rgba(0,0,0,0.0);}
			to {background-color: rgba(0,0,0,0.50);}
		}
		.bg{
			background-image: url('<?php echo base_url('asset/img/background/bg-login4.jpg');?>');
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			height:100%;
		}
		.bg:before {
			content: "";
			position: absolute;
			top: 0px;
			right: 0px;
			bottom: 0px;
			left: 0px;
			background-color: rgba(0,0,0,0.50);
			animation-name: bg-intro;
			animation-duration: 1.5s;
		}
		input[type=text]:focus{
			background-color: rgba(0, 0, 0, 0);
		}
		input[type=text]{
			background-color: rgba(0, 0, 0, 0);
		}
		input[type=password]{
			background-color: rgba(0, 0, 0, 0);
		}
		input[type=password]:focus{
			background-color: rgba(0, 0, 0, 0);
		}
		form{
			background-color: rgba(0, 0, 0, 0.70); 
		}
		.container{
			opacity: 0.8 !important;
		}
		img{
			width:50%;
			height:auto;
		}
		::placeholder {
			color: white !important;
			opacity: 0.6 !important;
		}
		
	</style>
</head>
<body id="enter">
	<div class="bg">
		<div class='container pt-5'>
			<div class='row'>
				<div class='col-sm-3'></div>
				<div class='col-sm-6'>
					<?php
						$attr=array(
							"class" => "morpheus-den-gradient z-depth-5 text-center animated slideInDown slow rounded-lg mt-5 pt-5 pr-5 pl-5 pb-3"
						);
						echo form_open("welcome/login/",$attr);
					?>
				
					<img class="img-fluid pb-4 animated zoomIn delay-1s" src="<?php echo base_url('asset/img/other/logo_penta_ocean.png');?>">
					
					<input type="text" id='txt_username' placeholder="Username" class="animated fadeInLeft delay-1s white-text form-control mb-3">
					<input type="password"  id='txt_password' class="animated fadeInRight delay-1s white-text form-control mb-4" placeholder="Password">

					<!-- Sign in button -->
					<button onclick="cek_login()" class="animated zoomIn delay-1s z-depth-5 font-weight-bold btn blue-gradient color-block btn-block my-4" type="button">Log in</button>
					<?php
						echo form_close();
					?>
				</div>
				<div class='col-sm-3'></div>
			</div>
		</div>
	</div>
</body>
</html>
<script>
	var site_url='<?php echo site_url();?>';
	function cek_login(){
		var username=$("#txt_username").val();
		var password=$("#txt_password").val();
		$.ajax({
			type : "POST",
			data:{u:username,p:password},
			url : site_url+"login",
			success:function(result){
				if(result.includes("SUKSES")){
					window.location = site_url+"home";
				}else{
					M.toast({html: result.toLowerCase()});
					$("#txt_password").val("");
				}
			}
		});
	}
	$("#enter").keyup(function(e){ 
		var code = e.key; // recommended to use e.key, it's normalized across devices and languages
		if(code==="Enter") cek_login();
	});
</script>
