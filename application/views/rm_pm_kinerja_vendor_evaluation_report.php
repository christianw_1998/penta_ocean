<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Laporan Evaluasi Vendor</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">RM PM</li>
        <li class="breadcrumb-item">Kinerja</li>
        <li class="breadcrumb-item active" aria-current="page">Laporan Evaluasi</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>LAPORAN EVALUASI VENDOR</h1>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span> <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br> 
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
            </div>
            <div class="col-sm-4">
              <span>Kategori </span>
              <select class="form-control f-aleo font-sm" id="dd_kategori">
                <option value="1">REKAP</option>
                <option value="2">DETAIL</option>
              </select>
            </div>
            <div class="col-sm-1"></div>
          </div>
          <br>
          <div class='text-center'>
            <button type="button" class="center btn btn-outline-primary" onclick="get_all_sj_po()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {
    check_role();
    $('#mod_detail_kinerja').appendTo("body");
  });

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    }
  }

  function get_all_sj_po() {
    var dari_tanggal = null;
    var sampai_tanggal = null;
    var kategori = $("#dd_kategori").val();
    var is_filtered = false;

    //dari tanggal sampai tanggal filter
    if ($("#cb_tanggal").prop("checked")) {
      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }
    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "purchase_order/view_evaluation_report",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal,
          k: kategori
        },
        success: function(result) {
          $("#content").html(result);
          var filename = $("#txt_file_name").html();
          var currentdate = new Date();
          var datetime = "[" + currentdate.getDate() + "/" +
            (currentdate.getMonth() + 1) + "/" +
            currentdate.getFullYear() + "] @ " +
            currentdate.getHours() + ":" +
            currentdate.getMinutes() + ":" +
            currentdate.getSeconds();

          $('#mastertable').DataTable({
            "order": [
              [1, "desc"]
            ], //Tanggal Dibuat
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [{
              extend: 'excel',
              title: filename + " - " + datetime
            }]
          });
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');

          $("#content").addClass("animated fadeInDown");

        }
      });
    } else
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
  }

  function fill_detail_modal(no_sj) {
    $("#txt_no_sj").html(no_sj);
    $.ajax({
      type: "POST",
      url: site_url + "purchase_order/view_detail_modal_evaluasi",
      data: {
        n: no_sj
      },
      dataType: "json",
      success: function(result) {

        $("#txt_no_po").html(result.no_po);

        $("#txt_tanggal").val(result.tanggal);
        $("#txt_tanggal").css("color", "black");

        $("#txt_tanggal_estimasi").val(result.tanggal_estimasi);
        $("#txt_tanggal_estimasi").css("color", "black");

        $("#txt_c_status_pengiriman").html(result.c_status_pengiriman);

        $("#div_evaluasi_status_pengiriman").css("visibility", "visible");
        $("#dd_evaluasi_status_pengiriman").html(result.opt_ev_status_pengiriman);

        $("#div_evaluasi_kualitas").css("visibility", "visible");
        $("#dd_evaluasi_kualitas").html(result.opt_ev_kualitas);

        $("#div_evaluasi_qty_penerimaan").css("visibility", "visible");
        $("#dd_evaluasi_qty_penerimaan").html(result.opt_ev_qty_penerimaan);

        $("#div_qty_material").html(result.div_qty_material);
        $("#div_history_sj").html(result.div_history_sj);

        $("#div_evaluasi_after_sales").css("visibility", "visible");
        $("#dd_evaluasi_after_sales").html(result.opt_ev_after_sales);

        $('#mod_detail_kinerja').modal('show');
      }
    });
  }

  function confirm_kinerja_sj() {
    var no_sj = $("#txt_no_sj").html();
    var ev_status_pengiriman = $("#dd_evaluasi_status_pengiriman").val();
    var ev_qty_penerimaan = $("#dd_evaluasi_qty_penerimaan").val();
    var ev_kualitas = $("#dd_evaluasi_kualitas").val();
    var ev_after_sales = $("#dd_evaluasi_after_sales").val();

    var c = confirm("Apakah anda yakin?");
    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "purchase_order/insert_evaluasi",
        data: {
          n: no_sj,
          es: ev_status_pengiriman,
          eq: ev_qty_penerimaan,
          ek: ev_kualitas,
          ea: ev_after_sales
        },
        success: function(result) {
          alert(result);
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            $('#mod_detail_kinerja').modal('hide');
            get_all_sj_po();
          }
        }
      });

    }

  }
</script>
<!-- Modal Detail SJ Kinerja -->
<div class="modal fade" id="mod_detail_kinerja" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">Detail Surat Jalan</h5>
      </div>
      <!--Body-->
      <div class="modal-body" style="margin:1%">
        <div class="row">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right">
            Nomor PO:
          </div>
          <div class="col-sm-5 text-left">
            <span id='txt_no_po' class="fw-bold"></span>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right">
            Nomor Surat Jalan:
          </div>
          <div class="col-sm-5 text-left">
            <span id='txt_no_sj' class="fw-bold"></span>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <hr />
        <div id="div_history_sj"></div>
        <hr />
        <div class="row">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right">
            Estimasi Sampai:
          </div>
          <div class="col-sm-5 text-left">
            <input class="f-aleo" type="date" id="txt_tanggal_estimasi" disabled />
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right">
            Tanggal Terima:
          </div>
          <div class="col-sm-5 text-left">
            <input class="f-aleo" type="date" id="txt_tanggal" disabled />
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right">

          </div>
          <div class="col-sm-5 text-left fw-bold" id="txt_c_status_pengiriman">
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div id='div_evaluasi_status_pengiriman' style='visibility:visible' class="row align-middle">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right align-middle">
            Status Pengiriman:
          </div>
          <div class="col-sm-5 text-left">
            <select class="form-control f-aleo" id='dd_evaluasi_status_pengiriman' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <hr />
        <div id="div_qty_material"> </div>
        <div id='div_evaluasi_qty_penerimaan' style='visibility:visible' class="row align-middle">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-4 text-right align-middle">
            Quantity Penerimaan:
          </div>
          <div class="col-sm-6 text-left">
            <select class="form-control f-aleo" id='dd_evaluasi_qty_penerimaan' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <hr />
        <div id='div_evaluasi_kualitas' style='visibility:visible' class="row align-middle">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right align-middle">
            Kualitas:
          </div>
          <div class="col-sm-5 text-left">
            <select class="form-control f-aleo" id='dd_evaluasi_kualitas' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div id='div_evaluasi_after_sales' style='visibility:visible' class="row align-middle">
          <div class="col-sm-1 text-right"></div>
          <div class="col-sm-5 text-right align-middle">
            After Sales:
          </div>
          <div class="col-sm-5 text-left">
            <select class="form-control f-aleo" id='dd_evaluasi_after_sales' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-1"></div>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button onclick="confirm_kinerja_sj()" id="btn_insert_detail_sj_kinerja" class="btn btn-outline-success">SUBMIT</button>
      </div>
      <!--/.Content-->
    </div>
  </div>
</div>
<!-- modal -->