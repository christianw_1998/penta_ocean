<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Barang Masuk (WS)</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Barang Masuk</li>
        <li class="breadcrumb-item active" aria-current="page">Dari WS</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[Worksheet]</h3>
    <h1 class='f-aleo-bold text-center'>BARANG MASUK</h1>
    <br>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class='row'>
          <div class='col-sm-2 f-aleo text-right'></div>
          <div class='col-sm-2 f-aleo text-right'>No. Worksheet</div>
          <div class='col-sm-1 f-aleo text-left '>:</div>
          <div class='col-sm-3 text-left'><input class="f-aleo" onkeyup="reset_form()" style='width:100%' type="text" id="txt_nomor_ws" /></div>
          <div class='col-sm-4 f-aleo text-right'></div>
        </div>
      </div>
      <div class="col-sm-1"></div>
    </div>

    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <button type="button" id="btn_cari_ws" class="center text-center btn btn-outline-primary" onclick="gen_detail_ws()" data-mdb-ripple-color="dark">
          Cari WS
        </button>
        <button type="button" id="btn_generate" disabled class="center text-center btn btn-outline-success" onclick="insert_good_receipt()" data-mdb-ripple-color="dark">
          Generate
        </button>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <table class="tabel_detail_product" style="visibility:hidden">
          <tr>
            <td class="text-center font-sm">Nomor</td>
            <td class="text-center font-sm">Kode Barang</td>
            <td class="text-center font-sm">Warna</td>
            <td class="text-center font-sm">Kemasan</td>
            <td class="text-center font-sm">Tin</td>
            <td class="text-center font-sm">Dus</td>
            <td class="text-center font-sm">Barang</td>
          </tr>
        </table>
      </div>
      <div class="col-sm-1"></div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function gen_detail_ws() {
    var nomor_ws = "";
    nomor_ws = $("#txt_nomor_ws").val();

    if (nomor_ws == "") {
      toast("Nomor WS tidak boleh kosong!", Color.DANGER);
      return;
    }
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/view_checker_fg_gr",
      data: {
        n: nomor_ws
      },
      dataType: "JSON",
      success: function(result) {
        if ((result.status).includes(Status.MESSAGE_KEY_SUCCESS)) {
          if (!$.fn.DataTable.isDataTable('.tabel_detail_product')) {
            $(".tabel_detail_product").html(result.detail_product);
            $(".tabel_detail_product").css("visibility", "visible");
            $(".tabel_detail_product").addClass("animated fadeInUp");
            $('.tabel_detail_product').dataTable({
              paging: false,
              searching: false,
              info: false,
              "pagingType": "full",
              dom: 'Bfrtip',
              buttons: [{
                extend: 'excel'
              }]
            });
            $(".buttons-excel").remove();
          }
          $("#btn_generate").prop("disabled", false);

        } else {
          reset_form();
          toast(result.status, Color.DANGER);
        }
      }
    });
  }

  function reset_form() {
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }

    $(".tabel_detail_product").removeClass("animated fadeInUp");
    $(".tabel_detail_product").css("visibility", "hidden");
    $("#btn_generate").prop("disabled", true);

  }

  function insert_good_receipt() {
    var no_ws = $("#txt_nomor_ws").val();
    var c = confirm("Apakah Anda Yakin?");
    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "work_sheet/checker_fg_insert_gr",
        data: {
          n: no_ws
        },
        success: function(result) {
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            toast(result, Color.SUCCESS);
            reset_form();
          } else {
            toast(result, Color.DANGER);
          }
        }
      });
    }
  }
</script>