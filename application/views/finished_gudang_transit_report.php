<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Laporan Sisa Gudang Transit</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Lainnya</li>
        <li class="breadcrumb-item active" aria-current="page">Gudang Transit WS</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>LAPORAN SISA GUDANG TRANSIT</h1>
    <br>
    <div id="content">
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
    get_all_sisa();
  });

  function get_all_sisa() {
    $.ajax({
      type: "POST",
      url: site_url + "material/view_sisa_transit",
      success: function(result) {
        $("#content").html(result);
        $('#mastertable').DataTable({
          "drawCallback": function(settings) {
            check_role();
          },
          paging: true,
          "order": [
            [0, "desc"]
          ], //tanggal
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
      }
    });
  }
</script>