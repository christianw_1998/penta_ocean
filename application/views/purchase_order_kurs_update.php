<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Update Kurs</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Costing</li>
        <li class="breadcrumb-item">Kurs</li>
        <li class="breadcrumb-item active" aria-current="page">Update</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>UPDATE KURS</h1>
    <h6 class='f-aleo-bold red-text text-center'>Menu ini digunakan untuk update kurs USD Rupiah </h1>

      <hr style="margin-left:5%;margin-right:5%">

      <div class="d-flex justify-content-center" style="margin-right:1%">
        <table>
          <tr>
            <td class="text-right">Tanggal Berlaku</td>
            <td>:</td>
            <td><input style="width:100%" class="f-aleo text-left black-text" type="date" id="txt_tanggal_berlaku" /></td>
          </tr>
          <tr>
            <td class="text-right">Kurs (USD)</td>
            <td>:</td>
            <td><input type="number" class='text-right' min=0 placeholder="Dalam Rupiah" id="txt_kurs" /></td>
          </tr>
        </table>
      </div>

      <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 text-center">
          <button type="button" class="center text-center btn btn-outline-success" onclick="update_kurs()" data-mdb-ripple-color="dark">
            Update
          </button>
        </div>
        <div class="col-sm-1"></div>
      </div>
      <div id="content">
      </div>

  </div>
</body>

</html>

<script>
  $(document).ready(function() {
    check_role();
  });

  function update_kurs() {
    var kurs = $("#txt_kurs").val();
    var tanggal_berlaku = $("#txt_tanggal_berlaku").val();
    if (tanggal_berlaku !== "") {
      if (kurs <= 0) {
        toast(("NILAI KURS HARUS LEBIH BESAR DARI 0"), Color.DANGER);
        return;
      } else {
        $("#content").removeClass("animated fadeInDown");

        $.ajax({
          type: "POST",
          url: site_url + "purchase_order/update_kurs",
          data: {
            k: kurs,
            t: tanggal_berlaku
          },
          success: function(result) {
            toast(result, Color.SUCCESS);

          }
        });
      }
    } else {
      toast(("TANGGAL TIDAK BOLEH KOSONG"), Color.DANGER);
    }

  }
</script>