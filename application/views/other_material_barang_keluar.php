<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Barang Keluar Lain Lain</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">G. Lain Lain</li>
        <li class="breadcrumb-item active" aria-current="page">Barang Keluar</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>BARANG KELUAR</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <br>
    <div class="d-flex justify-content-center" style="margin-right:1%">
      <table style="width:30%">
        <form id="form_po">
          <tr>
            <td class="text-right fw-bold">Tanggal </td>
            <td>:</td>
            <td class="text-left"><input type='date' id='txt_tanggal' class='black-text form-control'></td>
          </tr>
          <tr>
            <td class="text-right fw-bold">Kategori</td>
            <td>:</td>
            <td class="text-left"><select class="form-control" id="dd_kategori" onchange="get_table_header()" style="width:100%"></select></td>
          </tr>
        </form>
      </table>
    </div>
    <br>
    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-12">
          <table class="tabel_detail_product" style="visibility:hidden">
            <tr>
              <td class="text-center font-sm f-aleo">Nomor</td>
              <td class="text-center font-sm f-aleo">ID Vendor</td>
              <td class="text-center font-sm f-aleo">Vendor</td>
              <td class="text-center font-sm f-aleo">Kode Material</td>
              <td class="text-center font-sm f-aleo">Material</td>
              <td class="text-center font-sm f-aleo">Valid</td>
              <td class="text-center font-sm f-aleo">Harga</td>
              <td class="text-center font-sm f-aleo">Aksi</td>
            </tr>
          </table>
          <div class='text-center'>
            <button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
              Tambah Barang
            </button>
            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="generate()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>

<script language="javascript">
  var ctr = 0;
  $(document).ready(function() {
    check_role();
    get_all_kategori();

  });

  function get_all_kategori() {

    $.ajax({
      type: "POST",
      url: site_url + "other_kategori/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";

        for (var i = 0; i < result.length; i++) {
          kal += "<option value=" + result[i]['<?php echo Other_Kategori::$ID; ?>'] + ">" + result[i]['<?php echo Other_Kategori::$NAMA; ?>'] + "</option>";
        }
        $("#dd_kategori").html(kal);
        $('#dd_kategori option:last').remove(); //Hapus Penambahan Asset

        get_table_header();

      }
    });
  }

  function get_table_header() {
    reset_filter();
    var kategori = $("#dd_kategori").val();
    if (kategori > 0) {
      $.ajax({
        type: "POST",
        url: site_url + "other_gi/get_table_header",
        data: {
          k: kategori
        },
        success: function(result) {
          $(".tabel_detail_product").html(result);
          //$(".tabel_detail_product").css("visibility", "visible");


        }
      });
    }
  }

  function cek_stock(ctr) {
    var qty = $("#txt_qty_" + ctr).val();
    var qty_stock = $("#txt_qty_stock_" + ctr).val();
    $("#txt_qty_stock_" + ctr).removeClass("red-text green-text");
    if (qty != "" && qty_stock != "") {
      if (parseInt(qty_stock) >= parseInt(qty)) {
        $("#txt_qty_stock_" + ctr).addClass("green-text");
      } else {
        $("#txt_qty_stock_" + ctr).addClass("red-text");

      }
    }
  }

  function get_payterm_by_id(ip) {

    if (ip.length == 4) {
      $.ajax({
        type: "POST",
        url: site_url + "payterm/get_by_id",
        data: {
          ip: ip,
        },
        dataType: "json",
        success: function(result) {
          $("#txt_desc_payterm").removeClass('green-text red-text');
          if (result.num_rows != 0 || result.num_rows != "") {
            $("#txt_desc_payterm").val(result["<?php echo Payterm::$DESKRIPSI; ?>"]);
            $("#txt_desc_payterm").addClass("green-text");
          } else {
            $("#txt_desc_payterm").val("Input 6 digit kode Payterm");
            $("#txt_desc_payterm").addClass('red-text');
          }
        }
      });
    } else {
      $("#txt_desc_payterm").removeClass('green-text red-text');
      $("#txt_desc_payterm").val("Input 6 digit kode Payterm");
      $("#txt_desc_payterm").addClass('red-text');
    }
  }

  function plus_product() {

    var temp = $(".tabel_detail_product");
    var kategori = $("#dd_kategori").val();
    var clone = temp.clone();

    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "other_gi/insert_plus",
      data: {
        c: ctr,
        t: "<?php echo OTHER_GI_PLUS_TYPE_INSERT ?>",
        k: kategori
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;

        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function reset_filter() {
    $(".tabel_detail_product").css("visibility", "hidden");
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    var kategori = $("#dd_kategori").val();
    if (kategori > 0) {
      $.ajax({
        type: "POST",
        url: site_url + "other_gi/get_table_header",
        data: {
          k: kategori
        },
        success: function(result) {
          $(".tabel_detail_product").html(result);
          //$(".tabel_detail_product").css("visibility", "visible");

        }
      });
    }
  }

  function reset_form() {
    $(".tabel_detail_product").css("visibility", "hidden");
    ctr = 0;
    $('#form_po').trigger("reset");
    $('#txt_desc_payterm').addClass("red-text");
    $('#txt_desc_payterm').removeClass("green-text");

    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    var kategori = $("#dd_kategori").val();
    if (kategori > 0) {
      $.ajax({
        type: "POST",
        url: site_url + "other_gi/get_table_header",
        data: {
          k: kategori
        },
        success: function(result) {
          $(".tabel_detail_product").html(result);
          //$(".tabel_detail_product").css("visibility", "visible");

        }
      });
    }


  }

  function cek_material(id_kategori, ctr) {
    var id = $("#txt_id_material_" + ctr).val();

    if (id != "") {
      $.ajax({
        type: "POST",
        url: site_url + "other_material/get_by_id",
        dataType: "json",
        data: {
          im: id,
          ik: id_kategori
        },
        success: function(result) {
          $("#txt_desc_material_" + ctr).removeClass("green-text red-text");

          if (result.length != 0) {
            $("#txt_desc_material_" + ctr).val(result['<?php echo Other_Material::$DESKRIPSI; ?>']);
            $("#txt_desc_material_" + ctr).addClass("green-text");
            $("#txt_qty_stock_" + ctr).val(result['<?php echo Other_Material_Stock::$QTY; ?>']);

          } else {
            $("#txt_desc_material_" + ctr).val("Material belum ditemukan");
            $("#txt_desc_material_" + ctr).addClass("red-text");
            $("#txt_qty_stock_" + ctr).val("");


          }

        }
      });
    }

  }

  function cek_service(id_kategori, ctr) {
    var id = $("#txt_id_service_" + ctr).val();

    if (id != "") {
      $.ajax({
        type: "POST",
        url: site_url + "other_material/get_by_id",
        dataType: "json",
        data: {
          im: id,
          ik: id_kategori
        },
        success: function(result) {
          $("#txt_desc_service_" + ctr).removeClass("green-text red-text");
          if (result.length != 0) {
            $("#txt_desc_service_" + ctr).val(result['<?php echo Other_Material::$DESKRIPSI; ?>']);
            $("#txt_desc_service_" + ctr).addClass("green-text");

          } else {
            $("#txt_desc_service_" + ctr).val("Service belum ditemukan");
            $("#txt_desc_service_" + ctr).addClass("red-text");
          }

        }
      });
    }

  }

  function get_gl_by_id(ctr) {
    var id = $("#txt_kode_gl_" + ctr).val();
    $.ajax({
      type: "POST",
      url: site_url + "gl/get",
      dataType: "json",
      data: {
        i: id
      },
      success: function(result) {
        $("#txt_desc_gl_" + ctr).removeClass("red-text green-text");
        if (result.length != 0) {
          $("#txt_desc_gl_" + ctr).val(result['<?php echo General_Ledger::$DESKRIPSI; ?>']);
          $("#txt_desc_gl_" + ctr).addClass("green-text");

        } else {
          $("#txt_desc_gl_" + ctr).addClass("red-text");
          $("#txt_desc_gl_" + ctr).val("Input Kode GL");
        }

      }
    });

  }

  function get_cc_by_id(ctr) {
    var id = $("#txt_kode_cc_" + ctr).val();
    $.ajax({
      type: "POST",
      url: site_url + "cc/get_by_id",
      dataType: "json",
      data: {
        i: id
      },
      success: function(result) {
        $("#txt_desc_cc_" + ctr).removeClass("red-text green-text");
        if (result.length != 0) {
          $("#txt_desc_cc_" + ctr).val(result['<?php echo Cost_Center::$NAMA; ?>']);
          $("#txt_desc_cc_" + ctr).addClass("green-text");
          //$("#txt_hidden_id_cc").val(result['<?php echo Cost_Center::$ID; ?>']);

        } else {
          $("#txt_desc_cc_" + ctr).addClass("red-text");
          $("#txt_desc_cc_" + ctr).val("Input Kode CC");
        }

      }
    });

  }

  function generate() {
    var table = $('#mastertable').DataTable();

    var idx = 0;
    //var ids = [];
    //var ds = [];
    var idm = [];
    var ikc = [];
    var ikg = [];
    var iqy = [];
    var ial = [];
    var tanggal = "";
    var kat = "";

    tanggal = $("#txt_tanggal").val();
    kat = $("#dd_kategori").val();

    do {
      if ($("#txt_desc_gl_" + idx).hasClass("green-text") &&
        $("#txt_desc_cc_" + idx).hasClass("green-text") && $("#txt_qty_stock_" + idx).hasClass("green-text") &&
        $("#txt_alasan_" + idx).val() != "") {
        /*if ($("#txt_id_service_" + idx).length > 0) {
          ids[idx] = $("#txt_id_service_" + idx).val();
          ds[idx] = $("#txt_desc_service_" + idx).val();
        }*/
        idm[idx] = $("#txt_id_material_" + idx).val();
        iqy[idx] = $("#txt_qty_" + idx).val();
        ikg[idx] = $("#txt_kode_gl_" + idx).val();
        ikc[idx] = $("#txt_kode_cc_" + idx).val();
        ial[idx] = $("#txt_alasan_" + idx).val();
      } else {
        toast("Ada data yang masih salah, silahkan cek lagi", Color.DANGER);
        return;
      }
      idx++;
    } while ($("#txt_id_material_" + idx).length > 0);
    if (tanggal == "") {
      toast("Tanggal Pemakaian masih kosong, silahkan cek lagi", Color.DANGER);
    } else {
      var c = confirm("Apakah anda yakin?");
      if (c) {

        $.ajax({
          type: "POST",
          url: site_url + "other_gi/insert",
          data: {
            c: idx,
            am: idm,
            ac: ikc,
            ag: ikg,
            aq: iqy,
            al: ial,
            t: tanggal,
            k: kat
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
            } else {
              toast(result, Color.DANGER);
            }

          }
        });
      }

    }

  }
</script>