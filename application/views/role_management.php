<script>
	class Role {
		static KEPALA = "role-kepala";
		static ADMIN = "role-admin";
		static CHECKER_FG = "role-checker-fg";
		static KEPALA_GUDANG = "role-kepala-gudang";
		static ACCOUNTING = "role-accounting";
		static CHECKER_RM_PM = "role-checker-rm-pm";
		static PURCHASING = "role-purchasing";
		static PPIC = "role-ppic";
		static ADMIN_RM_PM = "role-admin-rm-pm";

		static ARR_ROLE = [this.KEPALA, this.ADMIN, this.CHECKER_FG,
			this.KEPALA_GUDANG, this.ACCOUNTING, this.CHECKER_RM_PM,
			this.PURCHASING, this.PPIC, this.ADMIN_RM_PM
		];
	}
	check_role();

	function check_role() {
		$(".navbar").css("visibility", "hidden");
		$.ajax({
			type: "POST",
			url: site_url + "karyawan/get_role_by_nik_login",
			success: function(result) {
				if (result == 1) { //Kepala
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.KEPALA) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.KEPALA))
									$(this).remove();
							});
						}
					}
				} else if (result == 2) { //Admin
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.ADMIN) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.ADMIN))
									$(this).remove();
							});
						}
					}
				} else if (result == 3) { //Checker FG
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.CHECKER_FG) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.CHECKER_FG))
									$(this).remove();
							});
						}
					}
				} else if (result == 4) { //Kepala Gudang
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.KEPALA_GUDANG) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.KEPALA_GUDANG))
									$(this).remove();
							});
						}
					}
				} else if (result == 5) { //Accounting
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.ACCOUNTING) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.ACCOUNTING))
									$(this).remove();
							});
						}
					}
				} else if (result == 6) { //Checker RM PM
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.CHECKER_RM_PM) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.CHECKER_RM_PM))
									$(this).remove();
							});
						}
					}
				} else if (result == 7) { //Purchasing
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.PURCHASING) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.PURCHASING))
									$(this).remove();
							});
						}
					}
				} else if (result == 8) { //PPIC
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.PPIC) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.PPIC))
									$(this).remove();
							});
						}
					}
				} else if (result == 9) { //Admin RM PM
					for (var i = 0; i < Role.ARR_ROLE.length; i++) {
						if (Role.ARR_ROLE[i] != Role.ADMIN_RM_PM) {
							$("." + Role.ARR_ROLE[i]).each(function(i, obj) {
								if (!$(this).hasClass(Role.ADMIN_RM_PM))
									$(this).remove();
							});
						}
					}
				} else {
					$("." + Role.ADMIN).remove();
					$("." + Role.KEPALA).remove();
					$("." + Role.CHECKER_FG).remove();
					$("." + Role.KEPALA_GUDANG).remove();
					$("." + Role.ACCOUNTING).remove();
					$("." + Role.CHECKER_RM_PM).remove();
					$("." + Role.PURCHASING).remove();
					$("." + Role.PPIC).remove();
					$("." + Role.ADMIN_RM_PM).remove();
				}
				$(".navbar").css("visibility", "visible");
			}
		});
	}
</script>