<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Analisa Kebutuhan RM PM</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Forecast</li>
        <li class="breadcrumb-item">Lihat Data</li>
        <li class="breadcrumb-item active" aria-current="page">Analisa Kebutuhan</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>[In KG]</h1>
    <h1 class='f-aleo-bold text-center'>ANALISA KEBUTUHAN RM PM</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <br>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class="row f-aleo-bold">
          <div class="col-sm-1 text-right">
          </div>
          <div class="col-sm-5">
            <span>Dari Tanggal </span><br>
            <input onchange="get_dd_fb()" class="black-text" type="date" id="txt_tanggal_dari" />
          </div>
          <div class="col-sm-5">
            Sampai Tanggal <br>
            <input onchange="get_dd_fb()" class="black-text" type="date" id="txt_tanggal_sampai" />
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row f-aleo-bold">
          <div class="col-sm-1 text-right">
          </div>
          <div class="col-sm-5">
            <span>Forecast Batch</span><br>
            <select id="dd_fb" class="form-control" disabled></select>
          </div>
          <div class="col-sm-5">

          </div>
          <div class="col-sm-1"></div>
        </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-4"></div>
      <div class="col-sm-4 text-center">
        Faktor Pengali <input type="number" style="width:15%" class='text-right' min=0 value=0 id="txt_faktor_pengali" />
      </div>
      <div class="col-sm-4"></div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <button type="button" class="center text-center btn btn-outline-success" onclick="gen_table()" data-mdb-ripple-color="dark">
          GENERATE
        </button>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
  </div>
</body>

</html>

<script>
  $(document).ready(function() {
    check_role();
  });

  function gen_table() {
    var faktor_pengali = $("#txt_faktor_pengali").val();
    var dd_fb = null;
    if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
      if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
        toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
        return;
      } else {
        dd_fb = $("#dd_fb").val();
        if (faktor_pengali <= 0) {
          toast("Nilai faktor pengali harus lebih besar dari 0", Color.DANGER);
          return;
        } else {
          $("#content").removeClass("animated fadeInDown");

          $.ajax({
            type: "POST",
            url: site_url + "forecast/view_analysis",
            data: {
              f: faktor_pengali,
              b: dd_fb
            },
            success: function(result) {

              $("#content").html(result);
              $('#mastertable').DataTable({
                "drawCallback": function(settings) {
                  check_role();
                },
                "order": [
                  [0, "asc"]
                ],
                paging: true,
                "pagingType": "full",
                dom: 'Bfrtip',
                buttons: [
                  'excel',
                ]
              });
              //$(".buttons-excel").remove();
              $(".buttons-excel span").text('Export ke Excel');
              $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
              $("#content").addClass("animated fadeInDown");
            }
          });
        }
      }
    } else {
      toast("Ada tanggal yang masih kosong", Color.DANGER);
      return;
    }

  }

  function reset_form() {
    $("#content").html("");
    $("#txt_faktor_pengali").val(0);
    $("#rbRM").prop("checked", false);
    $("#rbPM").prop("checked", false);
  }

  function get_dd_fb() {
    $("#dd_fb").prop("disabled", true);
    $("#dd_fb").html("");
    if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
      if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
        toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
        return;
      }
      dari_tanggal = $("#txt_tanggal_dari").val();
      sampai_tanggal = $("#txt_tanggal_sampai").val();
      $.ajax({
        type: "POST",
        url: site_url + "forecast/analysis_get_dd_by_tanggal",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal
        },
        success: function(result) {
          $("#dd_fb").prop("disabled", false);
          $("#dd_fb").html(result);
        }
      });
    }
  }
</script>