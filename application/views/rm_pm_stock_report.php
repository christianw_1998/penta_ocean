<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Laporan Stock</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">RM PM</li>
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item active" aria-current="page">Laporan</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>LAPORAN STOCK</h1>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function get_all_stock() {
    $.ajax({
      type: "POST",
      url: site_url + "material_rm_pm/view_stock",
      success: function(result) {
        $("#content").html(result);
        $('#mastertable').DataTable({
          "drawCallback": function(settings) {
            check_role();
          },
          paging: true,
          "order": [
            [3, "desc"]
          ], //qty
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
      }
    });
  }
  $(document).ready(function() {
    check_role();
    get_all_stock();
  });
</script>