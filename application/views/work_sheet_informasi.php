<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Laporan Informasi WS</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">WS</li>
        <li class="breadcrumb-item">Worksheet</li>
        <li class="breadcrumb-item">Lihat Data</li>
        <li class="breadcrumb-item active" aria-current="page">Laporan Informasi</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>LAPORAN INFORMASI WORKSHEET</h1>
    <hr style="margin-right:5%;margin-left:5%">
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h3>
        <div class="row f-aleo-bold">
          <div class="col-sm-1 text-right">
          </div>
          <div class="col-sm-4">
            <span>Kategori </span>
            <select class="form-control f-aleo font-sm" id="dd_kategori">
              <option value="1">HEADER</option>
              <option value="2">DETAIL</option>
            </select>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row f-aleo-bold">
          <div class="col-sm-1 text-right">
            <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
          </div>
          <div class="col-sm-5">
            <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span> <br>
            <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
          </div>
          <div class="col-sm-5">
            Sampai Tanggal <br>
            <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row f-aleo-bold">
          <div class="col-sm-1 text-right">
            <input type="checkbox" onchange="toggle(value)" value="nomor_ws_from" id="cb_nomor_ws_from">
          </div>
          <div class="col-sm-4">
            <span onclick="toggle_checkbox('nomor_ws_from')">Nomor WS </span>
            <input type='text' placeholder="Dari" disabled class="form-control f-aleo font-sm" id="dd_nomor_ws_from"> </input>
          </div>
          <div class="col-sm-1 text-right">
            <input type="checkbox" onchange="toggle(value)" value="nomor_ws_to" id="cb_nomor_ws_to">
          </div>
          <div class="col-sm-4">
            <span onclick="toggle_checkbox('nomor_ws_to')">Nomor WS </span>
            <input type='text' placeholder="Sampai" disabled class="form-control f-aleo font-sm" id="dd_nomor_ws_to"> </input>
          </div>
          <div class="col-sm-1"></div>
        </div>
        <div class="row f-aleo-bold">
          <div class="col-sm-1 text-right">
            <input type="checkbox" onchange="toggle(value)" value="hirarki" id="cb_hirarki">
          </div>
          <div class="col-sm-4">
            <span onclick="toggle_checkbox('hirarki')">Kode Hirarki </span>
            <input type='text' disabled class="form-control f-aleo font-sm" id="dd_hirarki"> </input>
          </div>
          <div class="col-sm-1"></div>
        </div>


        <div class="row f-aleo-bold">
          <div class="col-sm-1 text-right">
            <input type="checkbox" id="cb_total">
          </div>
          <div class="col-sm-4">
            <span>Tampilkan Total</span>
          </div>
          <div class="col-sm-1"></div>
        </div>

      </div>
      <div class="col-sm-1"></div>
    </div>
    <div class='text-center'>
      <button type="button" class="center btn btn-outline-primary" onclick="get_history()" data-mdb-ripple-color="dark">
        GUNAKAN FILTER
      </button>
    </div>
    <br>
    <div id="content">
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    } else {
      if ($("#dd_" + value).prop('disabled') == true) {
        $("#dd_" + value).prop('disabled', false);
      } else {
        $("#dd_" + value).prop('disabled', true);
      }
    }
  }

  function get_history() {
    var dari_tanggal = null;
    var sampai_tanggal = null;
    var id_kategori = $("#dd_kategori").val();
    var hirarki = null;
    var is_filtered = false;
    var nomor_ws_from = null;
    var nomor_ws_to = null;
    var show_total = "n";

    //dari tanggal sampai tanggal filter
    if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
      if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
        toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
        return;
      } else {
        dari_tanggal = $("#txt_tanggal_dari").val();
        sampai_tanggal = $("#txt_tanggal_sampai").val();
        is_filtered = true;
      }
    }

    //nomor_ws_from filter
    if ($("#cb_nomor_ws_from").prop("checked")) {
      if ($("#dd_nomor_ws_from").val() != "") {
        nomor_ws_from = $("#dd_nomor_ws_from").val();
        is_filtered = true;
      } else {
        toast("Nomor WS [Dari] masih belum diisi", Color.DANGER);
        return;
      }
    }
    //nomor_ws_to filter
    if ($("#cb_nomor_ws_to").prop("checked")) {
      if ($("#dd_nomor_ws_to").val() != "") {
        nomor_ws_to = $("#dd_nomor_ws_to").val();
        is_filtered = true;
      } else {
        toast("Nomor WS [Sampai] masih belum diisi", Color.DANGER);
        return;
      }
    }

    //hirarki filter
    if ($("#cb_hirarki").prop("checked")) {
      if ($("#dd_hirarki").val() != "") {
        hirarki = $("#dd_hirarki").val();
        is_filtered = true;
      } else {
        toast("Hirarki masih belum diisi", Color.DANGER);
        return;
      }
    }

    //total show
    if ($("#cb_total").prop("checked")) {
      show_total = "y";
    }

    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "work_sheet/view_informasi",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal,
          ik: id_kategori,
          nf: nomor_ws_from,
          nt: nomor_ws_to,
          h: hirarki,
          s: show_total
        },
        success: function(result) {
          $("#content").html(result);
          if (id_kategori == 1) {
            $('#mastertable').DataTable({
              "drawCallback": function(settings) {
                check_role();
              },
              paging: true,
              "order": [
                [0, "desc"],
              ],
              "pagingType": "full",
              dom: 'Bfrtip',
              buttons: [
                'excel',
              ],

            });
          } else {
            $('#mastertable').DataTable({
              "drawCallback": function(settings) {
                check_role();
              },
              paging: true,
              "pagingType": "full",
              dom: 'Bfrtip',
              buttons: [
                'excel',
              ],
              "ordering": false,
              "pageLength": 50
            });
          }

          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");

        }
      });
    } else
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
  }
</script>