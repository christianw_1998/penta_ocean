<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Data BOM</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInUp">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">WS</li>
        <li class="breadcrumb-item">BOM</li>
        <li class="breadcrumb-item active" aria-current="page">Data BOM</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="btn btn-outline-primary" onclick="gen_form_upload_bom()" data-mdb-ripple-color="dark">
        Upload Data
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="get_all()" data-mdb-ripple-color="dark">
        Rekap Data
      </button>
    </div>
    <div id="content">
    </div>
  </div>
</body>
<div class="div_modal">

</div>

</html>
<div class="f-aleo modal fade" id="mod_upl_bom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD BOM</h5>
      </div>
      <!--Body-->
      <div class="modal-body" style="margin:1%">
        <div class="row">
          <div class="text-center col-sm-12 fw-bold">Perhatian</div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-1">1.</div>
              <div class="col-sm-11 text-justify">
                <span class="red-text fw-bold">(PENTING)</span> Upload ini digunakan untuk mengupload <span class="fw-bold">BOM Baru</span> (Bisa banyak BOM dalam 1x upload)
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">2.</div>
              <div class="col-sm-11 text-justify">
                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">3.</div>
              <div class="col-sm-11 text-justify">
                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
          <div class="col-sm-12">
            <figure class="figure d-flex justify-content-center">
              <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_bom.png"); ?>" />
            </figure>
          </div>
        </div>
        <!--<div class="row">
          <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
          <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
        </div>
        <div class="row">
          <div class="text-center col-sm-6 ">
            <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/work_sheet_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              Template (.xlsx)
            </a>
          </div>
          <div class="text-center col-sm-6">
            <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/work_sheet_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              File Benar (.xlsx)
            </a>
          </div>
        </div>-->
        <br>
        <form id="form_upload_bom" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
          <div class="row">
            <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
              <input type="file" onchange="change_label('lbl_upl_bom','txt_upl_bom')" accept=".xlsx" name="txt_upl_bom" class="txt_upl_bom custom-file-input" id="txt_upl_bom" aria-describedby="inputGroupFileAddon01">
              <label class="custom-file-label lbl_upl_bom" for="txt_upl_bom">File Data BOM (.xlsx)</label>
            </div>
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
            <div class="col-sm-3"></div>
          </div>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="submit" id="btn_upl_bom" class="btn btn-outline-success">Upload</button>
      </div>
    </div>
    </form>
    <!--/.Content-->
  </div>
</div>
<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function gen_form_upload_bom() {
    if (!$('#mod_upl_bom').is(':visible'))
      $('#mod_upl_bom').modal('show');
  }
  $(document).ready(function() {
    check_role();
    $(".buttons-excel").remove();
    $('#mod_upl_bom').appendTo("body");

  });

  function change_label(label, file) {
    $("." + label).html(get_file_path(file));
  }
  $('#form_upload_bom').submit(function(e) {
    $("#btn_upl_bom").prop("disabled", true);
    e.preventDefault();
    var form_data = new FormData(this);
    $.ajax({
      url: site_url + "bom/upload",
      type: "POST",
      data: form_data, //this is formData
      processData: false,
      contentType: false,
      cache: false,
      success: function(data) {
        $(".txt_desc_upload").removeClass("text-danger text-success");
        if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
          //toast(data, Color.SUCCESS);

          $(".txt_desc_upload").addClass("text-success");
        } else {
          //toast(data, Color.DANGER);

          $(".txt_desc_upload").addClass("text-danger");
        }
        $(".txt_desc_upload").html(data);
        $("#content").html("");
      },
      complete: function(data) {
        setTimeout(refresh_modal_bom, 3000);
        gen_form_upload_bom();
      }
    });
  });

  function refresh_modal_bom() {
    $('#form_upload_bom').trigger("reset"); // this will reset the form fields
    $(".txt_upl_bom").val("");
    $(".lbl_upl_bom").html("File Data BOM (.xlsx)");
    $("#btn_upl_bom").prop("disabled", false);
    $(".txt_desc_upload").html("");
  }

  function get_all() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "bom/view_master",
      success: function(result) {
        $("#content").html(result);
        //$(".div_modal").html(result.modal);
        $('#mastertable').DataTable({
          "drawCallback": function(settings) {
            check_role();
          },
          paging: true,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
        $("#content").addClass("animated fadeInDown");

      }
    });
  }

  function gen_detail(ctr) {
    var ib = $("#txt_bom_id_" + ctr).val();
    var ia = $("#txt_alt_id_" + ctr).val();
    $.ajax({
      type: "POST",
      url: site_url + "bom/view_modal_by_bom_alt",
      data: {
        ia: ia,
        ib: ib
      },
      success: function(result) {
        $("#content").html(result.kal);
        $(".div_modal").html(result);
        $('#mod_detail').modal('show');


      }
    });
  }
</script>