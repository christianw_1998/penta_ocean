<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Master Vendor</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo role-kepala role-accounting role-purchasing">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Master</li>
        <li class="breadcrumb-item active" aria-current="page">Vendor</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="btn btn-outline-primary" onclick="get_master('vendor')" data-mdb-ripple-color="dark">
        Tabel Vendor
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="get_supply()" data-mdb-ripple-color="dark">
        Supply Vendor
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="insert_form()" data-mdb-ripple-color="dark">
        Input Vendor
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="update_form()" data-mdb-ripple-color="dark">
        Update Vendor
      </button>
    </div>
    <div id="content">
    </div>
</body>

</html>


<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function insert_form() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "vendor/view_insert",
      success: function(result) {
        $("#content").html(result);
        $("#content").addClass("animated fadeInDown");
        check_role();
      }
    });
  }

  function update_form() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "vendor/view_update",
      success: function(result) {
        $("#content").html(result);
        $("#content").addClass("animated fadeInDown");
        check_role();
        $("#form_update").each(function() {
          $(this).find(":input").prop("disabled", true);
          $("#txt_id_vendor").prop("disabled", false);
          $("#txt_nama").val("").prop("disabled", true);
          $("#txt_alamat").val("").prop("disabled", true);
        });
      }
    });
  }

  function insert() {
    var nama = $("#txt_nama").val();
    var alamat = $("#txt_alamat").val();
    var rt = $("#txt_rt").val();
    var rw = $("#txt_rw").val();
    var kecamatan = $("#txt_kecamatan").val();
    var kelurahan = $("#txt_kelurahan").val();
    var kota = $("#txt_kota").val();
    var kode_pos = $("#txt_kodepos").val();
    var negara = $("#txt_negara").val();
    var currency = $("#txt_currency").val();
    var sales_person = $("#txt_sales_person").val();
    var npwp = $("#txt_npwp").val();
    var notelp = $("#txt_notelp").val();
    var email = $("#txt_email").val();
    var bank_account = $("#txt_bank_account").val();
    var bank_name = $("#txt_bank_name").val();
    var id_payterm = $("#txt_id_payterm").val();
    var id_gl = $("#txt_id_gl").val();
    var tipe = $("#txt_tipe").val();

    if (nama == "" || alamat == "" || negara == "") {
      toast("Ada data wajib yang masih kosong, silahkan cek lagi", Color.DANGER);
      return;
    }
    var c = confirm("Apakah anda yakin?");
    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "vendor/insert",
        data: {
          n: nama,
          a: alamat,
          rt: rt,
          rw: rw,
          kec: kecamatan,
          kel: kelurahan,
          k: kota,
          kp: kode_pos,
          ne: negara,
          c: currency,
          sp: sales_person,
          np: npwp,
          nt: notelp,
          ba: bank_account,
          bn: bank_name,
          ip: id_payterm,
          ig: id_gl,
          e: email,
          t: tipe
        },
        success: function(result) {
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            toast(result, Color.SUCCESS);
            $("#form_insert").trigger("reset");
          }
        }
      });
    }
  }

  function update() {
    var id_vendor = $("#txt_id_vendor").val();
    var nama = $("#txt_nama").val();
    var alamat = $("#txt_alamat").val();
    var rt = $("#txt_rt").val();
    var rw = $("#txt_rw").val();
    var kecamatan = $("#txt_kecamatan").val();
    var kelurahan = $("#txt_kelurahan").val();
    var kota = $("#txt_kota").val();
    var kode_pos = $("#txt_kodepos").val();
    var negara = $("#txt_negara").val();
    var currency = $("#txt_currency").val();
    var sales_person = $("#txt_sales_person").val();
    var npwp = $("#txt_npwp").val();
    var notelp = $("#txt_notelp").val();
    var email = $("#txt_email").val();
    var bank_account = $("#txt_bank_account").val();
    var bank_name = $("#txt_bank_name").val();
    var id_payterm = $("#txt_id_payterm").val();
    var id_gl = $("#txt_id_gl").val();

    if (!$("#txt_nama").prop("disabled")) {
      var c = confirm("Apakah anda yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "vendor/update",
          data: {
            iv: id_vendor,
            n: nama,
            a: alamat,
            rt: rt,
            rw: rw,
            kec: kecamatan,
            kel: kelurahan,
            k: kota,
            kp: kode_pos,
            ne: negara,
            c: currency,
            sp: sales_person,
            np: npwp,
            nt: notelp,
            ba: bank_account,
            bn: bank_name,
            ip: id_payterm,
            ig: id_gl,
            e: email
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              $("#form_update").trigger("reset");
              $("#form_update").each(function() {
                $(this).find(":input").prop("disabled", true);
                $("#txt_id_vendor").val("").prop("disabled", false);
                $("#txt_nama").val("").prop("disabled", true);
                $("#txt_alamat").val("").prop("disabled", true);
              });
            }
          }
        });
      }
    } else {
      toast("Silahkan input ID Vendor terlebih dahulu", Color.DANGER);
    }
  }

  function get_by_id(iv) {
    $.ajax({
      type: "POST",
      url: site_url + "vendor/get",
      data: {
        iv: iv,
      },
      dataType: "json",
      success: function(result) {
        if (result.num_rows != 0) {
          $("#form_update").each(function() {
            $(this).find(":input").prop("disabled", false);
            $("#txt_nama").prop("disabled", false);
            $("#txt_alamat").prop("disabled", false);

            $("#txt_nama").val(result['<?php echo Vendor::$NAMA; ?>']);
            $("#txt_alamat").val(result['<?php echo Vendor::$ALAMAT; ?>']);

            $("#txt_rt").val(result['<?php echo Vendor::$RT; ?>']);
            $("#txt_kelurahan").val(result['<?php echo Vendor::$KELURAHAN; ?>']);
            $("#txt_negara").val(result['<?php echo Vendor::$COUNTRY; ?>']);
            $("#txt_kodepos").val(result['<?php echo Vendor::$KODE_POS; ?>']);
            $("#txt_npwp").val(result['<?php echo Vendor::$NPWP; ?>']);
            $("#txt_email").val(result['<?php echo Vendor::$EMAIL; ?>']);
            $("#txt_bank_account").val(result['<?php echo Vendor::$BANK_ACCOUNT; ?>']);
            $("#txt_id_payterm").val(result['<?php echo Vendor::$ID_PAYTERM; ?>']);

            $("#txt_rw").val(result['<?php echo Vendor::$RW; ?>']);
            $("#txt_kecamatan").val(result['<?php echo Vendor::$KECAMATAN; ?>']);
            $("#txt_kota").val(result['<?php echo Vendor::$KOTA; ?>']);
            $("#txt_currency").val(result['<?php echo Vendor::$CURRENCY; ?>']);
            $("#txt_sales_person").val(result['<?php echo Vendor::$SALES_PERSON; ?>']);
            $("#txt_notelp").val(result['<?php echo Vendor::$NO_TELP; ?>']);
            $("#txt_bank_name").val(result['<?php echo Vendor::$BANK_NAME; ?>']);
            $("#txt_id_gl").val(result['<?php echo Vendor::$ID_GL; ?>']);
          });
        } else {
          $("#form_update").trigger("reset");
          $("#form_update").each(function() {
            $(this).find(":input").prop("disabled", true);
          });
          $("#txt_nama").val("").prop("disabled", true);
          $("#txt_alamat").val("").prop("disabled", true);

        }
      }
    });
  }

  function get_supply() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "vendor/view_supply",
      success: function(result) {
        $("#content").html(result);
        $('#mastertable').DataTable({
          "drawCallback": function(settings) {
            check_role();
          },
          paging: true,
          "order": [
            [1, "desc"]
          ],
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $("#content").addClass("animated fadeInDown");

        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
      }
    });
  }

  $(document).ready(function() {

  });
</script>