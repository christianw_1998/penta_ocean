<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Cancel Good Issue</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">WS</li>
        <li class="breadcrumb-item">Work Sheet</li>
        <li class="breadcrumb-item">Edit</li>
        <li class="breadcrumb-item active" aria-current="page">Cancel Good Issue</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>CANCEL GOOD ISSUE</h1>
    <h6 class='f-aleo-bold red-text text-center'>WS yang muncul disini hanya WS yang pernah diinput oleh checker</h6>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span><br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <br>
          <div class='text-center'>
            <button type="button" class="center btn btn-outline-primary" onclick="get_all_work_sheet()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <br>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    }
  }

  function get_all_work_sheet() {
    var is_filtered = false;
    var dari_tanggal = null;
    var sampai_tanggal = null;

    //dari tanggal sampai tanggal filter
    if ($("#cb_tanggal").prop("checked")) {
      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }
    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "work_sheet/view_cancel_get_all",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal
        },
        success: function(result) {
          $("#content").html(result);
          $('#mastertable').DataTable({
            paging: true,
            "order": [
              [0, "desc"]
            ], //GI terbaru
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          $(".buttons-excel").remove();
          $("#content").addClass("animated fadeInDown");
        }
      });
    } else
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
  }

  $(document).ready(function() {
    check_role();
    $('#mod_detail_ws').appendTo("body");
  });

  function fill_detail_modal(no_gi) {
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/view_detail_modal",
      data: {
        n: no_gi
      },
      "dataType": "JSON",
      success: function(result) {
        $('.div_detail_product_checker').html(result.detail_product_checker);
        $('.div_detail_product_awal').html(result.detail_product_awal);
        $("#txt_no_ws").html(result.nomor_worksheet);
        $("#txt_no_gi").html(result.id_worksheet_checker_header);
        $('#mod_detail_ws').modal('show');
        if (!$.fn.DataTable.isDataTable('#tbl_detail_product_checker')) {
          $('#tbl_detail_product_checker').dataTable({
            paging: false,
            info: false,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          $(".buttons-excel").remove();

        }
        if (!$.fn.DataTable.isDataTable('#tbl_detail_product_awal')) {
          $('#tbl_detail_product_awal').dataTable({
            paging: false,
            info: false,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          $(".buttons-excel").remove();

        }
      }
    });
  }

  function cancel_ws() {
    var no_gi = $("#txt_no_gi").html();
    var c = prompt("Apakah anda yakin? Aksi ini tidak dapat dikembalikkan. Jika yakin, tuliskan kata 'CONFIRM' untuk melanjutkan");
    if (c == Status.MESSAGE_KEY_CONFIRM) {
      $.ajax({
        type: "POST",
        url: site_url + "work_sheet/cancel",
        data: {
          n: no_gi
        },
        success: function(result) {
          alert(result);
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            $('#mod_detail_ws').modal('hide');
            get_all_work_sheet();
          }
        }
      });
    } else
      alert("Kata yang diinputkan tidak sesuai dengan yang diminta, tuliskan kata CONFIRM (Huruf besar semua)");


  }
</script>
<!-- Modal Detail Worksheet -->
<div class="modal fade" id="mod_detail_ws" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">DETAIL WORKSHEET</h5>
      </div>
      <!--Body-->
      <div class="modal-body" style="margin:1%">
        <h5 class="f-aleo fw-bold red-text text-center">Keterangan: Qty negatif adalah qty kelebihan</h5>
        <div class="row">
          <div class="col-sm-4 text-right"></div>
          <div class="col-sm-2 text-right">
            Nomor WS:
          </div>
          <div class="col-sm-2 text-right">
            <span id='txt_no_ws' class="fw-bold"></span>
          </div>
          <div class="col-sm-4"></div>
        </div>
        <h5 class="f-aleo">WS Awal</h5>
        <div class="row">
          <div class="col-sm-12 text-center div_detail_product_awal">

          </div>
        </div>
        <br>
        <h5 class="f-aleo">WS Checker</h5>
        <div class="row">
          <div class="col-sm-4 text-right"></div>
          <div class="col-sm-2 text-right">
            Nomor GI:
          </div>
          <div class="col-sm-2 text-right">
            <span id='txt_no_gi' class="fw-bold"></span>
          </div>
          <div class="col-sm-4"></div>
        </div>
        <div class="row">
          <div class="col-sm-12 text-center div_detail_product_checker">

          </div>
        </div>

      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button onclick="cancel_ws()" id="btn_cancel_ws" class="btn btn-outline-danger">CANCEL</button>
      </div>
      <!--/.Content-->
    </div>
  </div>
</div>
<!-- modal -->