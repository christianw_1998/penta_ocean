<?php
	include("library.php");
	include("library_materialize.php");
?>
<head>
	<title>Add File</title>
</head>
<body>
	<h1>Add File</h1>
	
	<div style='margin-left:15%;margin-right:15%;padding-right:5%;size:100%'>
		<?php
			echo form_open_multipart('Welcome/upload_file');
		?>
			<input type='file' name='files[]' multiple> <br/><br/>
      		<input type='submit' value='Upload' name='upload' />
		<?php
			echo form_close();
		?>
	</div>
	<strong><?php if(isset($totalFiles)) echo "Successfully uploaded ".count($totalFiles)." files"; ?></strong>
</body>