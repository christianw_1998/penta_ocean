<!DOCTYPE html>
<html lang="en" class="full-height">

<head>

	<title>Home</title>
	<?php
	include("library.php");
	include("redirect_login.php");
	include("role_management.php");
	?>
</head>

<body class="f-aleo">
	<?php
	include("navigation.php");
	?>
	<div id="content" style="visibility:hidden">

	</div>
</body>

</html>
<script language="javascript">
	$(document).ready(function() {
		check_role();
		get_today_info();
	});

	function input_kebersihan() {
		var arr_kondisi = new Array();
		var arr_gudang = new Array();
		var ctr = 0;
		for (var i = 0; i < 100; i++) {
			if ($("#dd_kebersihan_" + i).length) {
				arr_kondisi[ctr] = $("#dd_kebersihan_" + i).val();
				arr_gudang[ctr] = $("#txt_gudang_" + i).val();
				ctr++;
			}
		}
		$.ajax({
            type: "POST",
            url: site_url + "kebersihan/insert",
            data: {
              c: ctr,
              k: arr_kondisi,
              g: arr_gudang
            },
            success: function(result) {
              if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
                toast(result, Color.SUCCESS);
              } else {
                toast(result, Color.DANGER);
              }
            }
		});
	}

	function get_today_info() {
		$.ajax({
			type: "POST",
			url: site_url + "master/today_info",
			success: function(result) {
				$("#content").html(result);
				check_role();
				//$("#content").removeClass("black-text");
			},
			complete: function(data) {
				setTimeout(remove_white_text, 500);
			}
		});
	}

	function remove_white_text() {
		$("#content").css("visibility", "visible");
	}
</script>