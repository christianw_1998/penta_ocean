<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Buat Purchase Request</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Purchase Request</li>
        <li class="breadcrumb-item active" aria-current="page">Buat Baru</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>BUAT PURCHASE REQUEST</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <br>
    <div class="row">
      <div class="col-sm-4"></div>
      <div class="col-sm-4 text-center">
        Faktor Pengali <input type="number" style="width:15%" class='text-right' min=0 value=0 id="txt_faktor_pengali" />
      </div>
      <div class="col-sm-4"></div>
    </div>
    <div class="row">
      <div class="col-sm-2"></div>
      <div class="col-sm-8 align-middle text-center">
        <div class="row">
          <div class="col-sm-6 text-right "><input class="form-check-input" type="radio" name="rbJenis" id="rbRM">
            <label class="form-check-label" for="rbRM">
              <strong class='fw-bold'>RM</strong>
            </label>
          </div>
          <div class="col-sm-6 text-left"><input class="form-check-input" type="radio" name="rbJenis" id="rbPM">
            <label class="form-check-label" for="rbPM">
              <strong class='fw-bold'>PM</strong>
            </label>
          </div>
        </div>
      </div>
      <div class="col-sm-4"></div>
    </div>

    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <button type="button" class="center text-center btn btn-outline-success" onclick="gen_table()" data-mdb-ripple-color="dark">
          GENERATE
        </button>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
  </div>
</body>

</html>

<script>
  $(document).ready(function() {
    check_role();
  });

  function toggle_check(e) {
    var check = false;
    var table = $('#mastertable').DataTable();

    if ($(e).is(":checked"))
      check = true;
    table.rows().nodes().to$().find('input[type="checkbox"]').each(function() {
      $(this).prop("checked", check);
    });
  }

  function gen_table() {
    var tipe = "";
    var faktor_pengali = $("#txt_faktor_pengali").val();

    if ($("#rbRM").is(":checked"))
      tipe = "<?php echo Purchase_Request::$S_TYPE_RM; ?>";
    else if ($("#rbPM").is(":checked"))
      tipe = "<?php echo Purchase_Request::$S_TYPE_PM; ?>";

    if (faktor_pengali <= 0) {
      toast("Nilai faktor pengali harus lebih besar dari 0", Color.DANGER);
      return;
    } else if (tipe == "") {
      toast("Setidaknya harus memilih 1 tipe PR", Color.DANGER);
      return;
    } else {
      $("#content").removeClass("animated fadeInDown");

      $.ajax({
        type: "POST",
        url: site_url + "purchase_request/get_tabel_awal",
        data: {
          f: faktor_pengali,
          t: tipe
        },
        success: function(result) {

          $("#content").html(result);
          $('#mastertable').DataTable({
            "drawCallback": function(settings) {
              check_role();
            },
            "order": [
              [1, "asc"]
            ],
            "columnDefs": [{
              "orderable": false,
              "targets": 0
            }], //no urut
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          //$(".buttons-excel").remove();
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");
        }
      });
    }
  }

  function reset_form() {
    $("#content").html("");
    $("#txt_faktor_pengali").val(0);
    $("#rbRM").prop("checked", false);
    $("#rbPM").prop("checked", false);
  }

  function gen_pr() {
    var table = $('#mastertable').DataTable();
    var length = table.page.info().recordsTotal;

    var ctr = 0;
    var idm = [];
    var idv = [];
    var qpr = [];
    table.rows().nodes().to$().find('input[type="checkbox"]').each(function() {
      if ($(this).prop("checked")) {
        var idx = $(this).val();
        idm[ctr] = table.rows().nodes().to$().find('input[id="txt_idm_' + idx + '"]').val();

        idv[ctr] = table.rows().nodes().to$().find('input[id="txt_idv_' + idx + '"]').val();
        if (idv[ctr] == "") {
          idv[ctr] = null;
        }
        qpr[ctr] = table.rows().nodes().to$().find('input[id="txt_qpr_' + idx + '"]').val();
        ctr++;
      }
    });

    if (ctr > 0) {
      var c = confirm("Apakah anda yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "purchase_request/insert",
          data: {
            am: idm,
            av: idv,
            aq: qpr,
            c: ctr
          },
          success: function(result) {
            toast(result, Color.SUCCESS);
            reset_form();

          }
        });
      }
    } else {
      toast("Setidaknya 1 material harus dicentang untuk generate PR", Color.DANGER);
    }
  }
</script>