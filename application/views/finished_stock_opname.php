<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Stock Opname</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item">Stock Opname</li>
        <li class="breadcrumb-item active" aria-current="page">Input</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>STOCK OPNAME</h1>
    <div class="row" style="margin-right:1%;margin-left:1%">
      <div class="col-sm-12 text-center">
        <span class="form-check">
          <input class="form-check-input" onclick='toggle_radiobutton("<?php echo STOCK_OPNAME_TYPE_HARIAN; ?>")' type="radio" name="rbJenis" id="rb_harian" checked>
          <label class="form-check-label" for="rbHarian">
            Harian
          </label>
        </span>
        <span class="form-check role-accounting">
          <input class="form-check-input" onclick='toggle_radiobutton("<?php echo STOCK_OPNAME_TYPE_TAHUNAN; ?>")' type="radio" name="rbJenis" id="rb_tahunan">
          <label class="form-check-label" for="rbTahunan">
            Tahunan
          </label>
        </span>
        <div id='div_button' class='text-center'>

        </div>
        <div id='content' class='text-center'>

        </div>
      </div>
    </div>
    <!-- Modal Upload Stock Material -->
    <div class="modal fade" id="mod_upl_stock_opname_tahunan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg " role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header">
            <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD STOCK OPNAME</h5>
          </div>
          <!--Body-->
          <div class="modal-body" style="margin:1%">
            <div class="row">
              <div class="text-center col-sm-12 fw-bold">Perhatian</div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-sm-1">1.</div>
                  <div class="col-sm-11 text-justify">
                    <span class="red-text fw-bold">(PENTING)</span> Upload Stock ini akan menyesuaikan <span class="red-text fw-bold">VARIAN</span> saja, program akan secara otomatis menentukan lokasi yang diisi/diambil.
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-1">2.</div>
                  <div class="col-sm-11 text-justify">
                    <span class="red-text fw-bold">(PENTING)</span> Saat Export Stock Opname Tahunan ke Excel, pastikan checkbox untuk "Tampilkan Lokasi Gudang" <span class="red-text fw-bold">TIDAK DI CENTANG</span> untuk menghindari inkonsistensi jumlah kolom.
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-1">3.</div>
                  <div class="col-sm-11 text-justify">
                    Silahkan mendownload file template yang disediakan dibawah, disarankan untuk tidak membuat file sendiri untuk menghindari salah input.
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-1">4.</div>
                  <div class="col-sm-11 text-justify">
                    Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-1">5.</div>
                  <div class="col-sm-11 text-justify">
                    Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
              <div class="col-sm-12">
                <figure class="figure d-flex justify-content-center">
                  <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_material_stock_opname_tahunan.png"); ?>" />
                </figure>
              </div>
            </div>
            <div class="row">
              <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
              <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
            </div>
            <div class="row">
              <div class="text-center col-sm-6 ">
                <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/stock_barang_opname_tahunan_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                  Template (.xlsx)
                </a>
              </div>
              <div class="text-center col-sm-6">
                <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/stock_barang_opname_tahunan_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
                  File Benar (.xlsx)
                </a>
              </div>
            </div>
            <br>
            <form id="form_upload_stock_opname_tahunan" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
              <div class="row">
                <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                  <input type="file" onchange="change_label('lbl_upl_stock_opname_tahunan','txt_upl_stock_opname_tahunan')" accept=".xlsx" name="txt_upl_stock_opname_tahunan" class="txt_upl_stock_opname_tahunan custom-file-input" id="txt_upl_stock_opname_tahunan" aria-describedby="inputGroupFileAddon01">
                  <label class="custom-file-label lbl_upl_stock_opname_tahunan" for="txt_upl_stock_opname_tahunan">File Stock Opname Tahunan (.xlsx)</label>
                </div>
                <div class="col-sm-3"></div>
              </div>
              <div class="row">
                <div class="col-sm-3"></div>
                <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
                <div class="col-sm-3"></div>
              </div>
          </div>
          <!--Footer-->
          <div class="modal-footer">
            <button type="submit" id="btn_upl_stock_opname_tahunan" class="btn btn-primary">Upload</button>
          </div>
        </div>
        </form>
        <!--/.Content-->
      </div>
    </div>
    <!-- modal -->
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';
  var ctr = 0;

  function toggle_checkbox_gudang() {
    if ($("#cb_gudang").prop("checked")) {
      $(".data_gudang").show();
    } else {
      $(".data_gudang").hide();
    }
  }

  function toggle_radiobutton(value) {
    reset_form();
    var jenis = value;
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "material/view_stock_opname",
      data: {
        j: jenis
      },
      dataType: "json",
      success: function(result) {
        $("#div_button").html(result.button_view);
        $("#content").html(result.content_view);
        if (jenis == "<?php echo STOCK_OPNAME_TYPE_TAHUNAN; ?>") {
          $('#mastertable').DataTable({
            "drawCallback": function(settings) {
              toggle_checkbox_gudang();
            },
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [{
              extend: 'excelHtml5',
              exportOptions: {
                columns: ':visible'
              },
            }]
          });

          toggle_checkbox_gudang();
          $("#content").addClass("animated fadeInDown");
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
        }
      }

    });

  }

  function plus_product() {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "material/plus",
      data: {
        c: ctr,
        t: "<?php echo FG_PLUS_TYPE_OPNAME; ?>"
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;
        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function check_varian_color(params) {
    $("#txt_varian_" + params).removeClass('red-text green-text');
    var stock_program = parseInt($("#txt_stock_program_" + params).val());
    var stock_fisik = parseInt($("#txt_stock_fisik_" + params).val());

    $("#txt_varian_" + params).val(stock_fisik - stock_program);
    if ($("#txt_varian_" + params).val() != 0) {
      $("#txt_varian_" + params).addClass('red-text');
    } else {
      $("#txt_varian_" + params).addClass('green-text');
    }
  }

  function check_product(params) {
    var kode_barang = $("#txt_kode_barang_" + params).val();
    var kemasan = $("#txt_kemasan_" + params).val();
    var warna = $("#txt_warna_" + params).val();
    if (kode_barang != "" && kemasan != "" && warna != "") {
      $.ajax({
        type: "POST",
        url: site_url + "material/get",
        data: {
          kb: kode_barang,
          k: kemasan,
          w: warna
        },
        dataType: "JSON",
        success: function(result) {

          if (result.num_rows != 0) {
            if ($("#txt_stock_program_" + params).length)
              $("#txt_stock_program_" + params).val(result.stock_program);
            if ($("#txt_stock_fisik_" + params).length)
              $("#txt_stock_fisik_" + params).val(result.stock_fisik);

            $("#txt_varian_" + params).val(Math.abs(result.stock_program - result.stock_fisik));

            $("#txt_barang_desc_" + params).val(result.description);
            $("#txt_barang_desc_" + params).val(result.description);
            $("#txt_barang_desc_" + params).addClass('green-text');
            $("#txt_barang_desc_" + params).removeClass('red-text');
          } else {
            if ($("#txt_stock_program_" + params).length)
              $("#txt_stock_program_" + params).val(0);
            if ($("#txt_stock_fisik_" + params).length)
              $("#txt_stock_fisik_" + params).val(0);
            $("#txt_barang_desc_" + params).val("barang belum ditemukan!");
            $("#txt_barang_desc_" + params).removeClass('green-text');
            $("#txt_barang_desc_" + params).addClass('red-text');
          }
        }
      });
    }
  }

  function insert_fisik_stock() {
    var arr_kode_barang = new Array();
    var arr_kemasan_barang = new Array();
    var arr_stock_fisik_barang = new Array();
    var arr_warna_barang = new Array();
    var ctr_arr = 0;
    var is_true = 1;
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_kode_barang_" + i).length > 0) {
        var kode_barang = $("#txt_kode_barang_" + i).val();
        var kemasan = $("#txt_kemasan_" + i).val();
        var stock_fisik = 0;
        if ($("#txt_stock_fisik_" + i).val() != "")
          stock_fisik = parseInt($("#txt_stock_fisik_" + i).val());
        else {
          toast("ada stock yang kosong, silahkan cek lagi!", Color.DANGER);
          return;
        }
        var warna = $("#txt_warna_" + i).val();
        if (kode_barang != "" && kemasan != "" && warna != "") {
          arr_kode_barang[ctr_arr] = kode_barang;
          arr_kemasan_barang[ctr_arr] = kemasan;
          arr_stock_fisik_barang[ctr_arr] = stock_fisik;
          arr_warna_barang[ctr_arr] = warna;
          ctr_arr++;
        } else {
          is_true = false;
          break;
        }
      }
    }
    if (ctr_arr == 0)
      is_true = false;
    if (is_true) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "material/insert_fisik_stock",
          data: {
            c: ctr_arr,
            kb: arr_kode_barang,
            keb: arr_kemasan_barang,
            sfb: arr_stock_fisik_barang,
            wb: arr_warna_barang
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
            } else {
              toast(result, Color.DANGER);
            }
          }
        });
      }
    } else {
      toast("Ada barang yang salah, pastikan barang terdaftar!", Color.DANGER);
    }
  }

  function reset_form() {
    var kal = ' <tr> <td class = "text-center font-sm f-aleo" > Nomor </td> <td class = "text-center font-sm f-aleo" > Kode Barang </td> <td class = "text-center font-sm f-aleo" > Warna </td> <td class = "text-center font-sm f-aleo" > Kemasan </td> <td class = "text-center font-sm f-aleo" > Fisik </td> <td class = "text-center font-sm f-aleo" > Program </td> <td class = "text-center font-sm f-aleo" > Varian </td> <td class = "text-center font-sm f-aleo" > Barang </td> </tr>';
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    ctr = 0;
  }
  $(document).ready(function() {
    check_role();
    toggle_radiobutton("<?php echo STOCK_OPNAME_TYPE_HARIAN; ?>");
    $('#mod_upl_stock_opname_tahunan').appendTo("body");
  });

  $("#mod_upl_stock_opname_tahunan").on("hidden.bs.modal", function() {
    refresh_modal_stock_opname_tahunan();
  });

  function refresh_modal_stock_opname_tahunan() {
    //data stock_opname_tahunan
    $('#form_upload_stock_opname_tahunan').trigger("reset"); // this will reset the form fields
    $(".txt_upl_stock_opname_tahunan").val("");
    $(".lbl_upl_stock_opname_tahunan").html("File Data Stock Material (.xlsx)");

    $(".txt_desc_upload").html("");
  }

  $('#form_upload_stock_opname_tahunan').submit(function(e) {
    e.preventDefault();
    var form_data = new FormData(this);
    $.ajax({
      url: site_url + "material/upload_stock_opname_tahunan",
      type: "POST",
      data: form_data, //this is formData
      processData: false,
      contentType: false,
      cache: false,
      success: function(data) {
        $(".txt_desc_upload").removeClass("text-danger text-success");
        if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
          $(".txt_desc_upload").addClass("text-success");
        } else {
          $(".txt_desc_upload").addClass("text-danger");
        }
        $(".txt_desc_upload").html(data);
        $("#content").html("");
      },
      complete: function(data) {
        setTimeout(refresh_modal_stock_opname_tahunan, 1500);
      }
    });
  });
</script>