
<!-- Alternative Web
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
-->
<script type="text/javascript" src="<?php echo base_url("asset/js/jquery.js");?>"></script>
<!-- Font Awesome -->
<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">-->
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url("asset/css/bootstrap.min.css");?>" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="<?php echo base_url("asset/css/mdb.min.css");?>" rel="stylesheet">
<!-- Your custom styles (optional) -->
<script type="text/javascript" src="<?php echo base_url("asset/js/bootstrap.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("asset/js/mdb.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("asset/js/jquery.dataTables.min.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("asset/js/dataTables.buttons.min.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("asset/js/buttons.flash.min.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("asset/js/jszip.min.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("asset/js/buttons.html5.min.js");?>"></script>

<link rel="stylesheet" href=<?php echo base_url("asset/css/jquery.dataTables.min.css");?> />
<link rel="stylesheet" href="<?php echo base_url("asset/css/font.css");?>">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url("asset/img/other/penta_ocean_icon.ico");?>">
<!-- Excel Reader -->
<?php
    include("./asset/library/SimpleXLSX.php");
    include("master_library.php");
?>