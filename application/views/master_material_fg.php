<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Master FG</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo role-kepala role-accounting role-kepala-gudang role-admin role-checker-fg">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Master</li>
        <li class="breadcrumb-item" aria-current="page">Material</li>
        <li class="breadcrumb-item" aria-current="page">Mat. FG</li>
        <li class="breadcrumb-item active" aria-current="page">SAGE</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="f-aleo btn btn-outline-primary" onclick="get_master('material_fg')" data-mdb-ripple-color="dark">
        Tabel Material
      </button>
    </div>
    <div id="content">
    </div>
</body>

</html>


<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function insert_f_material() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "insert_f/material",
      success: function(result) {
        $("#content").html(result);
        $("#content").addClass("animated fadeInDown");
      }
    });
  }

  /*function delete_material(id){
    var confirmation=confirm("Apakah anda yakin ingin menghapus? Aksi ini tidak dapat dikembalikan!");
    if(confirmation){
      $.ajax({
        type : "POST",
        data : {id:id},
        url : site_url+"delete/material",
        success:function(result){
          toast(result,Color.SUCCESS);
          get_master('material');
        }
      });
      check_role();
    }
  }*/

  $(document).ready(function() {});
</script>