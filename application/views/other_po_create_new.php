<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[Lain Lain] Buat PO Baru</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Purchase Order</li>
        <li class="breadcrumb-item">Buat</li>
        <li class="breadcrumb-item active" aria-current="page">[Lain Lain] Buat Baru</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[Lain Lain]</h3>
    <h1 class='f-aleo-bold text-center'>BUAT PO BARU</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <br>
    <div class="d-flex justify-content-center" style="margin-right:1%">
      <table style="width:30%">
        <form id="form_po">
          <tr>
            <td class="text-right fw-bold">PPN (%)</td>
            <td>:</td>
            <td class="text-right"><input type='number' style='width:50%' id='txt_ppn' min=0 placeholder=0 class='text-right black-text form-control'></td>
          </tr>
          <tr>
            <td class="text-right fw-bold">Tanggal PO</td>
            <td>:</td>
            <td class="text-left"><input type='date' id='txt_tanggal' class='black-text form-control'></td>
          </tr>
          <tr>
            <td class="text-right fw-bold">Kategori</td>
            <td>:</td>
            <td class="text-left"><select class="form-control" id="dd_kategori" onchange="get_table_header()" style="width:100%"></select></td>
          </tr>
          <tr>
            <td class="text-right fw-bold">Kode Payterm</td>
            <td>:</td>
            <td><input type="text" id="txt_kode_payterm" style="width:100%" onkeyup="get_payterm_by_id(this.value)" class="black-text"></td>
          </tr>
          <tr>
            <td class="text-right fw-bold">Deskripsi</td>
            <td>:</td>
            <td><input type="text" id="txt_desc_payterm" style="width:100%" value="Input 6 digit kode Payterm" disabled class="red-text pt"></td>
          </tr>
        </form>
      </table>
    </div>
    <br>
    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-12">
          <table class="tabel_detail_product" style="visibility:hidden">
            <tr>
              <td class="text-center font-sm f-aleo">Nomor</td>
              <td class="text-center font-sm f-aleo">ID Vendor</td>
              <td class="text-center font-sm f-aleo">Vendor</td>
              <td class="text-center font-sm f-aleo">Kode Material</td>
              <td class="text-center font-sm f-aleo">Material</td>
              <td class="text-center font-sm f-aleo">Valid</td>
              <td class="text-center font-sm f-aleo">Harga</td>
              <td class="text-center font-sm f-aleo">Aksi</td>
            </tr>
          </table>
          <div class='text-center'>
            <button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
              Tambah
            </button>
            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="generate()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>

<script language="javascript">
  var ctr = 0;
  $(document).ready(function() {
    check_role();
    get_all_kategori();

  });

  function get_all_kategori() {

    $.ajax({
      type: "POST",
      url: site_url + "other_kategori/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";

        for (var i = 0; i < result.length; i++) {
          kal += "<option value=" + result[i]['<?php echo Other_Kategori::$ID; ?>'] + ">" + result[i]['<?php echo Other_Kategori::$NAMA; ?>'] + "</option>";
        }
        $("#dd_kategori").html(kal);
        //$('#dd_kategori option:last').remove(); //Hapus Penambahan Asset

        get_table_header();

      }
    });
  }

  function get_table_header() {
    reset_filter();
    var kategori = $("#dd_kategori").val();
    if (kategori > 0) {
      $.ajax({
        type: "POST",
        url: site_url + "other_po/insert_get_table_header",
        data: {
          k: kategori
        },
        success: function(result) {
          $(".tabel_detail_product").html(result);
          //$(".tabel_detail_product").css("visibility", "visible");


        }
      });
    }
  }

  function calculate_total(ctr) {
    var qty = $("#txt_qty_" + ctr).val();
    var netprice = $("#txt_netprice_" + ctr).val();
    $("#txt_harga_total_" + ctr).removeClass("red-text green-text");
    if (qty != "" && netprice != "") {
      var total = qty * netprice;
      $("#txt_harga_total_" + ctr).val("Rp. " + add_decimal(total));
      $("#txt_harga_total_" + ctr).addClass("green-text");

    } else {
      $("#txt_harga_total_" + ctr).val("Input Qty & Net Price");
      $("#txt_harga_total_" + ctr).addClass("red-text");

    }
  }

  function get_payterm_by_id(ip) {

    if (ip.length == 4) {
      $.ajax({
        type: "POST",
        url: site_url + "payterm/get_by_id",
        data: {
          ip: ip,
        },
        dataType: "json",
        success: function(result) {
          $("#txt_desc_payterm").removeClass('green-text red-text');
          if (result.num_rows != 0 || result.num_rows != "") {
            $("#txt_desc_payterm").val(result["<?php echo Payterm::$DESKRIPSI; ?>"]);
            $("#txt_desc_payterm").addClass("green-text");
          } else {
            $("#txt_desc_payterm").val("Input 6 digit kode Payterm");
            $("#txt_desc_payterm").addClass('red-text');
          }
        }
      });
    } else {
      $("#txt_desc_payterm").removeClass('green-text red-text');
      $("#txt_desc_payterm").val("Input 6 digit kode Payterm");
      $("#txt_desc_payterm").addClass('red-text');
    }
  }

  function plus_product() {

    var temp = $(".tabel_detail_product");
    var kategori = $("#dd_kategori").val();
    var clone = temp.clone();

    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "other_po/insert_plus",
      data: {
        c: ctr,
        t: "<?php echo OTHER_PO_PLUS_TYPE_INSERT ?>",
        k: kategori
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;

        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function reset_filter() {
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    var kategori = $("#dd_kategori").val();
    if (kategori > 0) {
      $.ajax({
        type: "POST",
        url: site_url + "other_po/insert_get_table_header",
        data: {
          k: kategori
        },
        success: function(result) {
          $(".tabel_detail_product").html(result);
          //$(".tabel_detail_product").css("visibility", "visible");

        }
      });
    }
  }

  function reset_form() {
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    var kategori = $("#dd_kategori").val();
    if (kategori > 0) {
      $.ajax({
        type: "POST",
        url: site_url + "other_po/insert_get_table_header",
        data: {
          k: kategori
        },
        success: function(result) {
          $(".tabel_detail_product").html(result);
          //$(".tabel_detail_product").css("visibility", "visible");

        }
      });
    }

    $(".tabel_detail_product").css("visibility", "hidden");
    ctr = 0;
    $('#form_po').trigger("reset");
    $('#txt_desc_payterm').addClass("red-text");
    $('#txt_desc_payterm').removeClass("green-text");
  }

  function cek_material(id_kategori, ctr) {
    var id = $("#txt_id_material_" + ctr).val();

    if (id != "") {
      $.ajax({
        type: "POST",
        url: site_url + "other_material/get_by_id",
        dataType: "json",
        data: {
          im: id,
          ik: id_kategori
        },
        success: function(result) {
          $("#txt_desc_material_" + ctr).removeClass("green-text red-text");

          if (result.length != 0) {
            $("#txt_desc_material_" + ctr).val(result['<?php echo Other_Material::$DESKRIPSI; ?>']);
            $("#txt_desc_material_" + ctr).addClass("green-text");
            //$("#txt_desc_material_" + ctr).prop("disabled", false);


          } else {
            $("#txt_desc_material_" + ctr).val("Material belum ditemukan");
            $("#txt_desc_material_" + ctr).addClass("red-text");
            //$("#txt_desc_material_" + ctr).prop("disabled", true);

          }

        }
      });
    }

  }

  function cek_service(id_kategori, ctr) {
    var id = $("#txt_id_service_" + ctr).val();

    if (id != "") {
      $.ajax({
        type: "POST",
        url: site_url + "other_material/get_by_id",
        dataType: "json",
        data: {
          im: id,
          ik: id_kategori
        },
        success: function(result) {
          $("#txt_desc_service_" + ctr).removeClass("green-text red-text");
          if (result.length != 0) {
            $("#txt_desc_service_" + ctr).val(result['<?php echo Other_Material::$DESKRIPSI; ?>']);
            $("#txt_desc_service_" + ctr).addClass("green-text");

          } else {
            $("#txt_desc_service_" + ctr).val("Service belum ditemukan");
            $("#txt_desc_service_" + ctr).addClass("red-text");
          }

        }
      });
    }

  }

  function generate() {
    var table = $('#mastertable').DataTable();

    var idx = 0;
    var ids = [];
    var ds = [];
    var idm = [];
    var dm = [];
    var inp = [];
    var iqy = [];
    var ppn = 0;
    var tanggal = "";
    var pt = "";
    var kat = "";

    ppn = $("#txt_ppn").val();
    tanggal = $("#txt_tanggal").val();
    pt = $("#txt_kode_payterm").val();
    kat = $("#dd_kategori").val();

    do {
      if ($("#txt_harga_total_" + idx).html() != "Input Qty & Net Price" || $("#txt_netprice_" + idx).val() != "") {
        if ($("#txt_id_service_" + idx).length > 0) {
          ids[idx] = $("#txt_id_service_" + idx).val();
          ds[idx] = $("#txt_desc_service_" + idx).val();
        }
        idm[idx] = $("#txt_id_material_" + idx).val();
        dm[idx] = $("#txt_desc_material_" + idx).val();
        iqy[idx] = $("#txt_qty_" + idx).val();
        inp[idx] = $("#txt_netprice_" + idx).val();
      } else {
        toast("Ada data yang masih salah, silahkan cek lagi", Color.DANGER);
        return;
      }
      idx++;
    } while ($("#txt_id_material_" + idx).length > 0);
    if (tanggal == "") {
      toast("Tanggal PO masih kosong, silahkan cek lagi", Color.DANGER);
    } else if ($("#txt_tampil_total_ppn").hasClass("red-text")) {
      toast("Ada data yang masih salah, silahkan cek lagi", Color.DANGER);
    } else if ($("#txt_desc_payterm").hasClass("red-text")) {
      toast("Kode Payterm tidak ditemukan, silahkan cek lagi", Color.DANGER);
    } else if ($("#txt_desc_vendor").hasClass("red-text")) {
      toast("Vendor tidak ditemukan, silahkan cek lagi", Color.DANGER);
    } else {
      var c = confirm("Apakah anda yakin?");
      if (c) {

        $.ajax({
          type: "POST",
          url: site_url + "other_po/insert",
          data: {
            c: idx,
            am: idm,
            dm: dm,
            ds: ds,
            as: ids,
            an: inp,
            aq: iqy,
            pp: ppn,
            pt: pt,
            t: tanggal,
            k: kat
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
            } else {
              toast(result, Color.DANGER);
            }

          }
        });
      }

    }

  }
</script>