<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Ganti Nomor SJ</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class='f-aleo'>
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Lainnya</li>
        <li class="breadcrumb-item active" aria-current="page">Ganti Nomor SJ</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>GANTI NOMOR SJ</h1>
    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <table class="table table-borderless">
            <tr>
              <td></td>
              <td class='align-middle text-right font-sm'>Kode GR atau GI Lama</td>
              <td class='align-middle text-right font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <input style="width:100%" type="text" id="txt_old_sj" />
              </td>
            </tr>
            <h5 class='f-aleo-bold red-text text-center'>Perhatian: nomor SJ yang dimasukkan hanya digit belakang (max 6 digit) dan yang 0 tidak perlu diinput</h5>
            <h6 class='f-aleo-bold red-text text-center'>Misal nomor SJ: 82100000154, hanya perlu diisi 154 (nol tidak ditulis)</h6>
            <tr class='align-middle text-left font-sm'>
              <td>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbSO" checked>
                  <label class="form-check-label" for="rbSO">
                    SO <strong class='fw-bold'>(21xx)</strong>
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbSTO">
                  <label class="form-check-label" for="rbSTO">
                    STO <strong class='fw-bold'>(8xx)</strong>
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbSirclo">
                  <label class="form-check-label" for="rbSirclo">
                    SIRCLO <strong class='fw-bold'>(OUTxx)</strong>
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbRetur">
                  <label class="form-check-label" for="rbRetur">
                    RETUR <strong class='fw-bold'>(RTxx)</strong>
                  </label>
                </div>
              </td>
              <td class='align-middle text-right font-sm'>Nomor Surat Jalan Baru</td>
              <td class='align-middle text-right font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <input style="width:100%" type="text" id="txt_new_sj" />
              </td>
            </tr>
          </table>
          <div class="text-center" style="margin-bottom:2%">
            <button type="button" id="btn_change_nomor_sj" onclick='change_nomor_sj()' class="btn btn-outline-primary">
              Ganti Nomor Surat Jalan
            </button>
          </div>
        </div>
        <div class="col-sm-2"></div>

      </div>
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {
    check_role();
  });

  function reset_form() {
    $("#txt_old_sj").val("");
    $("#txt_new_sj").val("");
    $("#rbSO").prop("checked", true);
  }

  function change_nomor_sj() {
    var old_sj = $("#txt_old_sj").val();
    var new_sj = $("#txt_new_sj").val();
    var sto = "";
    if ($("#rbSO").is(":checked"))
      sto = "SO";
    else if ($("#rbSTO").is(":checked"))
      sto = "STO";
    else if ($("#rbSirclo").is(":checked"))
      sto = "SIRCLO";
    else if ($("#rbRetur").is(":checked"))
      sto = "RETUR";
    var c = confirm("Apakah anda yakin?");
    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "settings/change_nomor_sj",
        data: {
          os: old_sj,
          ns: new_sj,
          s: sto
        },
        success: function(result) {
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            toast(result, Color.SUCCESS);
            reset_form();
          } else {
            toast(result, Color.DANGER);
          }
        }
      });
    }
  }
</script>