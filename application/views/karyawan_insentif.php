<!DOCTYPE html>
<html lang="en"  class="role-kepala full-height">
<head>
  <title>Insentif Karyawan</title>
  <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
  ?>
</head>
<body class='f-aleo role-kepala'>
	<?php
    include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home");?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Insentif Karyawan</li>
      </ol>
    </nav>
    <div id="content" style="margin-bottom:5%">
    </div>
  </div>
</body>
</html>

<script language="javascript">
  var site_url='<?php echo site_url();?>';
  
  function rincian_insentif_karyawan(){
    $.ajax({
			type : "POST",
			url : site_url+"rincian_f/karyawan_insentif",
			success:function(result){
				$("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });
    $("#content").removeClass("animated fadeInDown");
  }
  function gen_karyawan_insentif(){
    $(".div_laporan").remove();
    var temp=	$("#content").html();
    var tahun=$("#dd_tahun").val();
    var nik=$("#dd_tipe").val();
    var bulan=$("#dd_bulan").val();
    $.ajax({
			type : "POST",
			data:{t:"karyawan_insentif",ta:tahun,td:nik,bu:bulan},
			url : site_url+"gen_rincian",
			success:function(result){
				$("#content").html(temp+result);
        $(".buttons-excel").remove();
      }
			
    });
    check_role();
  }
  function update_karyawan_insentif(ctr){
    var temp=	$("#content").html();
    var tahun=$("#txt_tahun").val();
    var nik=$("#txt_nik").val();
    var bulan=$("#txt_bulan").val();
    var tunjangan=new Array();
    var nama=new Array();
    for(var i=0;i<ctr;i++){
      tunjangan[i]=$("#txt_ctr_"+i).val();
      nama[i]=$("#txt_nama_"+i).val();
    }
    $.ajax({
			type : "POST",
      dataType: "JSON",
			data:{ta:tahun,td:nik,bu:bulan,tu:tunjangan,na:nama,c:ctr},
			url : site_url+"update_karyawan_insentif",
			success:function(result){
        var kal="";
        for(var i=0;i<result.length;i++){
          console.log(result[i]);
          if(result[i].includes(Status.MESSAGE_KEY_SUCCESS)){
            kal+=result[i]+"<br>";
          }else{
            if(!$("#txt_ctr_"+i).is(':disabled')){
              kal+=result[i]+"<br>";
            }
          }
        }
        if(kal==""){
          toast("TIDAK ADA YANG BISA DIUPDATE!",Color.DANGER);
        }else{
          toast(kal,Color.SUCCESS);
        }
        $(".div_laporan").remove();
        //$("#content").html(temp);
       
      }
			
    });
    check_role();
  }
  $(document).ready(function(){
    check_role();
    rincian_insentif_karyawan();
    
  });
</script>
