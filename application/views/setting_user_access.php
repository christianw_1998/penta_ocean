<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Akses User</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Pengaturan</li>
        <li class="breadcrumb-item active" aria-current="page">Akses User</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>AKSES USER</h1>
    <div class="row">
      <div class="col-sm-2"></div>
      <div class="col-sm-8">
        <div class="text-center" style="margin-bottom:2%">
          <button type="button" class="btn btn-outline-primary" onclick="get_all_user_access()" data-mdb-ripple-color="dark">
            List Hak Akses yang diberikan
          </button>
          <button type="button" class="btn btn-outline-primary" onclick="insert_f_user_access()" data-mdb-ripple-color="dark">
            Tambah Hak Akses
          </button>
          <div id="content"></div>
        </div>
        <div class="col-sm-2"></div>
      </div>

</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {
    check_role();
  });

  function get_all_user_access() {
    //$("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "user_access/get_all_no_restriction",
      success: function(result) {
        $("#content").html(result);
        $('#mastertable').DataTable({
          paging: true,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: []
        });
        //$("#content").addClass("animated fadeInDown");

      }
    });
  }

  function insert_user_access() {
    var nb = $("#dd_nik_bawahan").val();
    var i = $("#dd_list_access").val();
    var na = $("#dd_nik_atasan").val();
    var t = $("#dd_nik_atasan").val();
    if (nb != na) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        set_user_access(nb, i, na, "while_insert");
      }
    } else
      toast("NIK Bawahan tidak boleh sama dengan NIK Atasan", Color.DANGER);
  }

  function set_user_access(nb, i, na, t) {
    $.ajax({
      type: "POST",
      url: site_url + "user_access/change_status",
      data: {
        nb: nb,
        na: na,
        i: i,
        t: t
      },
      success: function(result) {
        if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
          toast(result, Color.SUCCESS);
          get_all_user_access();
        } else {
          toast(result, Color.DANGER);
        }
      }
    });
  }

  function change_desc_text() {
    $("#txt_deskripsi").val($("#dd_list_access").find('option:selected').attr('id'));
  }

  function insert_f_user_access() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "user_access/insert_form",
      success: function(result) {
        $("#content").html(result);
        $("#content").addClass("animated fadeInDown");
        check_role();
        change_desc_text();
      }
    });
    check_role();
  }
</script>