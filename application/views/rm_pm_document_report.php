<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Cek Dokumen</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">RM PM</li>
        <li class="breadcrumb-item">Lainnya</li>
        <li class="breadcrumb-item active" aria-current="page">Cek Dokumen</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>CEK DOKUMEN</h1>
    <br>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 text-right'>No. GI / GR</div>
          <div class='col-sm-1 text-left '>:</div>
          <div class='col-sm-3 text-left'><input style='width:100%' type="text" id="txt_nomor_doc" /></div>
          <div class='col-sm-2 text-right'></div>
        </div>
        <br>
        <div class='text-center'>
          <button type="button" class="center btn btn-outline-primary" onclick="get_document_by_id()" data-mdb-ripple-color="dark">
            CARI DOKUMEN
          </button>
        </div>
        <br>
        <h4 style="width:100%" id="txt_title_document" class="text-center green-text"></h4>
        <br>
        <div id="content">
        </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function get_document_by_id() {
    var nomor_doc = $("#txt_nomor_doc").val();
    $("#content").removeClass("animated fadeInDown");

    $.ajax({
      type: "POST",
      url: site_url + "material_rm_pm/get_document_view_by_id",
      data: {
        i: nomor_doc
      },
      dataType: "JSON",
      success: function(result) {
        if (result.message.includes(Status.MESSAGE_KEY_SUCCESS)) {
          $("#content").html(result.content);
          $("#txt_title_document").html(result.title);
          $('#mastertable').DataTable({
            "drawCallback": function(settings) {
              check_role();
            },
            "order": [
              [0, "asc"]
            ], //no urut
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          //$(".buttons-excel").remove();
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");
        } else {
          reset_form();
          toast(result.message, Color.DANGER);
        }

      }
    });
  }

  function reset_form() {
    $("#content").html("");
    $("#txt_title_document").html("");
  }
</script>