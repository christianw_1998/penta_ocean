<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Akses User</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Pengaturan</li>
        <li class="breadcrumb-item active" aria-current="page">Akses User</li>
      </ol>
    </nav>
    <h1 class='text-center f-aleo-bold'>AKSES USER</h1>
    <div id="content">
    </div>
  </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {
    check_role();
    get_list_user_access();
  });

  function get_list_user_access() {
    $.ajax({
      type: "POST",
      url: site_url + "user_access/get_all",
      success: function(result) {

        //$("#content").addClass("animated fadeInDown");
        $("#content").html(result);
        $('#mastertable').DataTable({
          paging: true,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: []
        });
      }
    });

    //$("#content").removeClass("animated fadeInDown");
  }

  function update_role_access(nik, id_access, is_true) {
    $.ajax({
      type: "POST",
      url: site_url + "user_access/insert",
      data: {
        ni: nik,
        ia: id_access,
        it: is_true
      },
      success: function(result) {
        get_list_user_access();
      }
    });
  }
</script>