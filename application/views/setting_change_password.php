<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Ganti Kata Sandi</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Pengaturan</li>
        <li class="breadcrumb-item active" aria-current="page">Ganti Kata Sandi</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>GANTI KATA SANDI</h1>
    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <table class="table table-borderless">
            <tr>
              <td class='align-middle text-right font-sm f-aleo'>Kata Sandi Lama</td>
              <td class='align-middle text-right font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <input style="width:50%" class="f-aleo" type="password" id="txt_cur_password" />
              </td>
            </tr>
            <tr>
              <td class='align-middle text-right font-sm f-aleo'>Kata Sandi Baru</td>
              <td class='align-middle text-right font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <input style="width:50%" class="f-aleo" type="password" id="txt_new_password" />
              </td>
            </tr>
            <tr>
              <td class='align-middle text-right font-sm f-aleo'>(Confirm) Kata Sandi Baru</td>
              <td class='align-middle text-right font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <input style="width:50%" class="f-aleo" type="password" id="txt_c_new_password" />
              </td>
            </tr>
          </table>
          <div class="text-center" style="margin-bottom:2%">
            <button type="button" id="btn_change_password" onclick='change_password()' class="btn btn-outline-success">
              Ganti Kata Sandi
            </button>
          </div>
        </div>
        <div class="col-sm-2"></div>

      </div>
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {
    check_role();
  });

  function reset_form() {
    $("#txt_cur_password").val("");
    $("#txt_new_password").val("");
    $("#txt_c_new_password").val("");
  }

  function change_password() {
    var cur_pw = $("#txt_cur_password").val();
    var new_pw = $("#txt_new_password").val();
    var c_new_pw = $("#txt_c_new_password").val();
    var c = confirm("apakah anda yakin?");
    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "settings/change_password",
        data: {
          cp: cur_pw,
          np: new_pw,
          cnp: c_new_pw
        },
        success: function(result) {
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            toast(result, Color.SUCCESS);
            reset_form();
          } else {
            toast(result, Color.DANGER);
          }
        }
      });
    }
  }
</script>