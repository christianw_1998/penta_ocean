<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Setting FOH</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
  <style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  </style>
</head>

<body class="f-aleo role-kepala role-accounting role-purchasing">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">FOH</li>
        <li class="breadcrumb-item active" aria-current="page">Setting</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="f-aleo btn btn-outline-primary" onclick="view_insert()" data-mdb-ripple-color="dark">
        Input
      </button>
      <button type="button" class="f-aleo btn btn-outline-primary" onclick="view_update()" data-mdb-ripple-color="dark">
        Update
      </button>
      <button type="button" class="f-aleo btn btn-outline-primary" onclick="view_validasi()" data-mdb-ripple-color="dark">
        Validasi
      </button>
    </div>
    <hr style="margin-left:5%;margin-right:5%">

    <form id="form_foh">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10" id="content" style="visibility:hidden">
          <h1 class="text-center fw-bold" id="txt_header"></h1>
          <hr style="margin-left:5%;margin-right:5%">
          <div class="text-center div_btn_update div_btn_validasi">
            <div class="row">
              <div class="col-sm-2 text-right"></div>
              <div class="col-sm-2 text-right">ID FOH</div>
              <div class="col-sm-1 text-left ">:</div>
              <div class="col-sm-3 text-left"><input type="text" id="txt_id_foh"></div>
              <div class="col-sm-2 text-right"></div>
            </div>
            <button type="button" id="btn_cari" class="center btn btn-outline-success" onclick="get_foh_by_id()" data-mdb-ripple-color="dark">
              Cari
            </button>
          </div>
          <table class="tabel_detail_gl">
            <tr class="fw-bold">
              <td class="text-center">No</td>
              <td class="text-center">Nomor GL</td>
              <td class="text-center">Deskripsi</td>
              <td class="text-center">Harga</td>
              <td class="text-center">Aksi</td>
            </tr>
          </table>
          <div class="text-center div_btn_insert div_btn_update div_btn_validasi">
            <div class="row">
              <div class="col-sm-6">
                <h5 class="text-right fw-bold">Total COGS (Rp):</h5>
              </div>
              <div class="col-sm-5 text-right">
                <input type="hidden" class="fw-bold" id="txt_total_cogs"></input>
                <h5 class="fw-bold" id="txtt_total_cogs"></h5>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <h5 class="text-right fw-bold">Total KG (KG):</h5>
              </div>
              <div class="col-sm-5 text-right">
                <input type="hidden" class="fw-bold" id="txt_total_kg" value="616919,170"></input>
                <h5 class="fw-bold" id="txtt_total_kg">616,919.170</h5>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <h5 class="text-right fw-bold">FOH (Rp):</h5>
              </div>
              <div class="col-sm-5 text-right">
                <h5 lass="fw-bold" id="txt_foh">xxxxxxxxx</h5>
              </div>
            </div>

            <button type="button" id="btn_tambah" class=" center btn btn-outline-primary" onclick="plus_gl()" data-mdb-ripple-color="dark">
              Tambah
            </button>
            <button type="button" id="btn_generate" class=" center btn btn-outline-success" onclick="generate()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
        <div class="col-sm-1"></div>
      </div>
    </form>
</body>

</html>


<script language="javascript">
  class PAGE_TYPE {
    static UPDATE = "UPDATE";
    static VALIDATION = "VALIDASI";
    static INSERT = "INSERT";
  }
  var site_url = '<?php echo site_url(); ?>';
  var ctr = 0;
  var mode = "";
  var id_foh = "";

  $(document).ready(function() {});

  function plus_gl() {

    var temp = $(".tabel_detail_gl");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "foh/plus",
      data: {
        c: ctr
      },
      success: function(result) {
        $(".tabel_detail_gl").html(html + result);
        ctr++;
      }
    });
  }

  function minus_gl(params) {
    var temp = $(".tabel_detail_gl");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_gl_" + params).remove();
  }

  function view_insert() {
    mode = PAGE_TYPE.INSERT;
    reset_form(mode);
    $("#txt_header").html("INPUT HARGA");
    $("#content").css("visibility", "visible");

    var clone = $(".tabel_detail_gl").clone();
    $.ajax({
      type: "POST",
      url: site_url + "foh/view_insert",
      dataType: "json",
      success: function(result) {

        $(".tabel_detail_gl").html(clone.html() + result.html);
        $(".tabel_detail_gl").css("visibility", "visible");
        $("#btn_tambah").prop("disabled", false);
        $("#btn_generate").prop("disabled", false);
        ctr = result.ctr;
      }
    });
  }

  function view_update() {
    mode = PAGE_TYPE.UPDATE;
    reset_form(mode);
    $("#txt_header").html("UPDATE HARGA");
    $("#content").css("visibility", "visible");
    $("#btn_tambah").prop("disabled", true);
    $("#btn_generate").prop("disabled", true);
  }

  function view_validasi() {
    mode = PAGE_TYPE.VALIDATION;
    reset_form(mode);
    $("#txt_header").html("VALIDASI");
    $("#content").css("visibility", "visible");
    $("#btn_tambah").prop("disabled", true);
    $("#btn_generate").prop("disabled", true);
  }

  function calculate_total() {
    var total = 0;
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_harga_" + i).length > 0) {
        if ($("#txt_harga_" + i).val() != "")
          total += parseInt($("#txt_harga_" + i).val());
      }
    }
    $("#txtt_total_cogs").html(add_decimal(total));
    $("#txt_total_cogs").val(total);
    calculate_foh();
  }

  function calculate_foh() {
    var total_cogs = parseInt($("#txt_total_cogs").val());
    var total_kg = parseInt($("#txt_total_kg").val());
    $("#txt_foh").html(add_decimal(total_cogs / total_kg));
  }

  function get_foh_by_id() {
    var id_foh = $("#txt_id_foh").val();
    $.ajax({
      type: "POST",
      url: site_url + "foh/view_update_get_by_id",
      data: {
        i: id_foh,
        m: mode
      },
      dataType: "json",
      success: function(result) {
        if (result.ctr == 0) {
          toast("Pencarian FOH gagal karena sudah divalidasi sebelumnya", Color.DANGER);

        } else {
          $(".tabel_detail_gl").html(result.html);
          $(".tabel_detail_gl").css("visibility", "visible");
          ctr = result.ctr;
          if (mode == PAGE_TYPE.UPDATE)
            $("#btn_tambah").prop("disabled", false);
          $("#btn_generate").prop("disabled", false);
          calculate_total();
        }
      }
    });
  }

  function check_gl(params) {
    var id_gl = $("#txt_id_gl_" + params).val();
    if (id_gl != "") {
      $.ajax({
        type: "POST",
        url: site_url + "gl/get",
        data: {
          i: id_gl,
        },
        dataType: "JSON",
        success: function(result) {

          if (result.num_rows != 0) {
            $("#txt_desc_gl_" + params).val(result["<?php echo General_Ledger::$DESKRIPSI; ?>"]);
            $("#txt_desc_gl_" + params).addClass("green-text");
          } else {
            $("#txt_desc_gl_" + params).val("GL tidak ditemukan");
            $("#txt_desc_gl_" + params).removeClass('green-text');
            $("#txt_desc_gl_" + params).addClass('red-text');
          }
        }
      });
    }
  }

  function generate() {
    var arr_id_gl = new Array();
    var arr_harga = new Array();
    var idx = 0;
    var is_exist = true;
    var id_foh = "";
    if (mode != PAGE_TYPE.INSERT)
      id_foh = $("#txt_id_foh").val();

    for (var i = 0; i < ctr; i++) {
      is_exist = true;
      if ($("#txt_id_gl_" + i).length > 0) {
        if ($("#txt_id_gl_" + i).val() != "")
          arr_id_gl[idx] = parseInt($("#txt_id_gl_" + i).val());
        else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      } else {
        is_exist = false;
      }

      if ($("#txt_harga_" + i).length > 0) {
        if ($("#txt_harga_" + i).val() != "")
          arr_harga[idx] = parseInt($("#txt_harga_" + i).val());
        else
          arr_harga[idx] = 0;
      } else {
        is_exist = false;
      }

      if (is_exist)
        idx++;
    }
    if (idx != 0) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "foh/" + mode.toLowerCase(),
          data: {
            ag: arr_id_gl,
            ah: arr_harga,
            c: idx,
            if: id_foh
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              reset_form();
              toast(result, Color.SUCCESS);
            } else
              toast(result, Color.DANGER);
          }
        });
      }
    } else
      toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
  }

  function reset_form(mode = null) {
    $(".div_btn_insert").css("visibility", "hidden");
    $(".div_btn_update").css("visibility", "hidden");
    $(".div_btn_validasi").css("visibility", "hidden");
    $("#txt_id_foh").val("");
    var kal = '<tr> <td class = "fw-bold text-center" > No </td> <td class = "fw-bold text-center" > Nomor GL </td> <td class = "fw-bold text-center" > Deskripsi </td> <td class = "fw-bold text-center" > Harga </td> <td class = "fw-bold text-center" > Aksi </td> </tr>';
    $(".tabel_detail_gl").html(kal);
    $("#content").css("visibility", "hidden");
    $(".tabel_detail_gl").css("visibility", "hidden");
    $("#txt_total").html("");
    if (mode != null) {
      $(".div_btn_" + mode.toLowerCase()).css("visibility", "visible");

    }
    ctr = 0;
  }
</script>