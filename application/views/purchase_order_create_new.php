<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Buat PO Baru</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Purchase Order</li>
        <li class="breadcrumb-item">Buat</li>
        <li class="breadcrumb-item active" aria-current="page">[RM PM] Buat Baru</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[RM PM]</h3>
    <h1 class='f-aleo-bold text-center'>BUAT PO BARU</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <br>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 text-right'>Kode PR</div>
          <div class='col-sm-1 text-left '>:</div>
          <div class='col-sm-3 text-left'><input type="text" id="txt_kode_pr" /></div>
          <div class='col-sm-2 text-right'></div>
        </div>

      </div>
      <div class="col-sm-1"></div>
    </div>

    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <button type="button" class="center text-center btn btn-outline-success" onclick="gen_table()" data-mdb-ripple-color="dark">
          GENERATE
        </button>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <hr style="margin-left:5%;margin-right:5%">

    <div id="content" style="margin-left:1%;margin-right:1%">
    </div>
  </div>
</body>

</html>

<script>
  $(document).ready(function() {
    check_role();
  });

  function calculate_total(ctr) {
    $("#txt_tampil_total_" + ctr).removeClass("red-text");

    if ($("#txt_qty_po_" + ctr).val() != "") {
      var qty = $("#txt_qty_po_" + ctr).val();
      try {
        var netprice = $("#txt_netprice_" + ctr).val();
        if (netprice == "") {
          netprice = parseInt(netprice);
          $("#txt_tampil_total_" + ctr).html("Input Pricelist terlebih dahulu");
          $("#txt_tampil_total_" + ctr).addClass("red-text");
          $("#txt_total_" + ctr).val(0);
        } else {
          var total = qty * netprice;
          $("#txt_tampil_total_" + ctr).html(add_decimal(Math.round(total)));
          $("#txt_total_" + ctr).val(Math.round(total));
        }
      } catch (error) {
        $("#txt_tampil_total_" + ctr).html("Input Pricelist terlebih dahulu");
        $("#txt_tampil_total_" + ctr).addClass("red-text");
        $("#txt_total_" + ctr).val(0);
      }
    } else {
      $("#txt_tampil_total_" + ctr).html("Input Qty terlebih dahulu");
      $("#txt_tampil_total_" + ctr).addClass("red-text");
      $("#txt_total_" + ctr).val(0);
    }
    calculate_total_all();
  }

  function calculate_total_all() {
    var idx = 0;
    var total = 0;
    $("#txt_tampil_total").removeClass("red-text");

    do {
      if (!$("#txt_tampil_total_" + idx).hasClass("red-text")) {
        total += parseInt($("#txt_total_" + idx).val());
      } else {
        $("#txt_tampil_total").html("Pastikan tidak ada total yang salah");
        $("#txt_tampil_total").addClass("red-text");
        return;
      }
      idx++;
    } while ($("#txt_tampil_total_" + idx).length > 0);
    $("#txt_tampil_total").html("Rp. " + add_decimal(Math.round(total)));

    calculate_total_with_ppn($("#txt_ppn").val());
  }

  function calculate_total_with_ppn() {
    $("#txt_tampil_total_ppn").removeClass("red-text");
    var diskon = $("#txt_diskon").val();
    var ppn = $("#txt_ppn").val();
    if (diskon == "" || diskon < 0) {
      $("#txt_tampil_total_ppn").addClass("red-text");
      $("#txt_tampil_total_ppn").html("Input Diskon(Rupiah) terlebih dahulu");
    } else if (ppn == "") {
      $("#txt_tampil_total_ppn").addClass("red-text");
      $("#txt_tampil_total_ppn").html("Input PPN(%) terlebih dahulu");
    } else {
      var idx = 0;
      var total = 0;
      do {
        if ($("#txt_tampil_total_" + idx).html() != "Input Qty terlebih dahulu") {
          total += parseInt($("#txt_total_" + idx).val());
        } else {
          $("#txt_tampil_total_ppn").html("Pastikan tidak ada total yang salah");
          $("#txt_tampil_total_ppn").addClass("red-text");
          return;
        }
        idx++;
      } while ($("#txt_tampil_total_" + idx).length > 0);
      total -= diskon;
      var total_ppn = (total * (parseInt(ppn) + 100) / 100);

      $("#txt_tampil_total_ppn").html("Rp. " + add_decimal(total_ppn));

    }
  }

  function get_payterm_by_id(ip) {

    if (ip.length == 4) {
      $.ajax({
        type: "POST",
        url: site_url + "payterm/get_by_id",
        data: {
          ip: ip,
        },
        dataType: "json",
        success: function(result) {
          $("#txt_desc_payterm").removeClass('green-text red-text');
          if (result.num_rows != 0 || result.num_rows != "") {
            $("#txt_desc_payterm").val(result["<?php echo Payterm::$DESKRIPSI; ?>"]);
            $("#txt_desc_payterm").addClass("green-text");
          } else {
            $("#txt_desc_payterm").val("Input 6 digit kode Payterm");
            $("#txt_desc_payterm").addClass('red-text');
          }
        }
      });
    } else {
      $("#txt_desc_payterm").removeClass('green-text red-text');
      $("#txt_desc_payterm").val("Input 6 digit kode Payterm");
      $("#txt_desc_payterm").addClass('red-text');
    }
  }

  function get_active_price(ctr) {
    //var id_vendor = $("#txt_idv_" + ctr).val();
    var id_vendor = $("#txt_id_vendor").val();
    var id_material = $("#txt_idm_" + ctr).val();
    var tanggal_po = $("#txt_tanggal").val();
    if (tanggal_po != "") {
      $.ajax({
        type: "POST",
        url: site_url + "purchase_order/price_get_active",
        data: {
          iv: id_vendor,
          im: id_material,
          t: tanggal_po
        },
        dataType: "json",
        success: function(result) {
          $("#txt_tampil_netprice_" + ctr).removeClass("red-text");
          if (result.num_rows != 0) {
            $("#txt_netprice_" + ctr).val(result["<?php echo Purchase_Order_Price_List::$PRICE; ?>"]);
            $("#txt_tampil_netprice_" + ctr).html(add_decimal(Math.round(result["<?php echo Purchase_Order_Price_List::$PRICE; ?>"])));

          } else {
            $("#txt_netprice_" + ctr).val("");
            $("#txt_tampil_netprice_" + ctr).addClass("red-text");
            $("#txt_tampil_netprice_" + ctr).val("");
          }
          calculate_total(ctr);
        }
      });
    } else {
      toast("Pastikan Tanggal PO valid untuk menentukan Price List nya", Color.DANGER);
    }

  }

  function update_tanggal() {
    var ctr_temp = 0;
    do {
      get_active_price(ctr_temp);
      ctr_temp++;
    } while ($("#txt_tampil_total_" + ctr_temp).length > 0);
  }

  /*function get_vendor_by_id(ctr) {
    var id_vendor = $("#txt_idv_" + ctr).val();

    $.ajax({
      type: "POST",
      url: site_url + "vendor/get",
      data: {
        iv: id_vendor,
      },
      dataType: "json",
      success: function(result) {
        $("#txt_desc_vendor_" + ctr).removeClass('green-text red-text');
        if (result.num_rows != 0) {
          $("#txt_desc_vendor_" + ctr).html(result["<?php echo Vendor::$NAMA; ?>"]);
          $("#txt_desc_vendor_" + ctr).addClass("green-text");
        } else {
          $("#txt_desc_vendor_" + ctr).html("Vendor tidak ditemukan");
          $("#txt_desc_vendor_" + ctr).addClass('red-text');
        }
        get_active_price(ctr);
      }
    });
  }*/

  function get_vendor_by_id(iv) {
    $.ajax({
      type: "POST",
      url: site_url + "vendor/get",
      data: {
        iv: iv,
      },
      dataType: "json",
      success: function(result) {
        $("#txt_desc_vendor").removeClass('green-text red-text');
        if (result.num_rows != 0) {
          $("#txt_desc_vendor").val(result["<?php echo Vendor::$NAMA; ?>"]);
          $("#txt_desc_vendor").addClass("green-text");
          //$("#txt_desc_vendor").prop("disabled", false);
          var ctr_temp = 0;
          do {
            get_active_price(ctr_temp);
            ctr_temp++;
          } while ($("#txt_tampil_total_" + ctr_temp).length > 0);
        } else {
          $("#txt_desc_vendor").val("Input 6 digit kode Vendor");
          $("#txt_desc_vendor").addClass('red-text');
          //$("#txt_desc_vendor").prop("disabled", true);

        }
      }
    });
  }

  function gen_table() {
    var tipe = "";
    var kode_pr = $("#txt_kode_pr").val();

    if (kode_pr == "") {
      toast("Kode PR tidak boleh kosong", Color.DANGER);
      return;
    } else {
      $("#content").removeClass("animated fadeInDown");

      $.ajax({
        type: "POST",
        url: site_url + "purchase_order/get_tabel_create",
        data: {
          k: kode_pr
        },
        dataType: "json",
        success: function(result) {
          if (result.message.includes(Status.MESSAGE_KEY_FAILED)) {
            toast(result.message, Color.DANGER);
          } else {

            $("#content").html(result.kal);
            $('#mastertable').DataTable({
              "drawCallback": function(settings) {
                check_role();
              },
              "order": [
                [8, "asc"]
              ],
              "columnDefs": [{
                "orderable": false,
                "targets": 0
              }], //no urut
              paging: false,
              "pagingType": "full",
              dom: 'Bfrtip',
              buttons: [
                'excel',
              ]
            });
            $(".buttons-excel").remove();
            //$(".buttons-excel span").text('Export ke Excel');
            //$(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
            $("#content").addClass("animated fadeInDown");
          }
        }
      });
    }
  }

  function reset_form() {
    $("#content").html("");
    $("#txt_kode_pr").val("");
  }

  function generate_po() {
    var idx = 0;
    var idv = "";
    var ids = [];
    var idm = [];
    var iqy = [];
    var inp = [];
    var ppn = 0;
    var tanggal = "";
    var tanggal_kirim = "";
    var pr = "";
    var pt = "";
    var diskon = 0;

    pt = $("#txt_kode_payterm").val();
    pr = $("#txt_kode_pr").val();
    ppn = $("#txt_ppn").val();
    diskon = $("#txt_diskon").val();
    tanggal = $("#txt_tanggal").val();
    tanggal_kirim = $("#txt_tanggal_kirim").val();
    pt = $("#txt_kode_payterm").val();
    idv = $("#txt_id_vendor").val();

    do {
      if ($("#txt_tampil_total_" + idx).html() != "Input Qty terlebih dahulu") {
        //idv[idx] = $("#txt_idv_" + idx).val();
        ids[idx] = $("#txt_desc_" + idx).val();
        idm[idx] = $("#txt_idm_" + idx).val();
        iqy[idx] = $("#txt_qty_po_" + idx).val();
        inp[idx] = $("#txt_netprice_" + idx).val();
      } else {
        toast("Ada data yang masih salah, silahkan cek lagi", Color.DANGER);
        return;
      }
      idx++;
    } while ($("#txt_tampil_total_" + idx).length > 0);

    if (tanggal == "" || tanggal_kirim == "") {
      toast("Ada tanggal yang masih kosong, silahkan cek lagi", Color.DANGER);
    } else if (tanggal > tanggal_kirim) {
      toast("Tanggal PO tidak boleh lebih dari tanggal kirim, silahkan cek lagi", Color.DANGER);
    } else if ($("#txt_desc_payterm").hasClass("red-text")) {
      toast("Kode Payterm tidak ditemukan, silahkan cek lagi", Color.DANGER);
    } else if ($("#txt_desc_vendor").hasClass("red-text")) {
      toast("Vendor tidak ditemukan, silahkan cek lagi", Color.DANGER);
    } else if ($("#txt_tampil_total_ppn").hasClass("red-text")) {
      toast("Total masih salah, silahkan cek lagi", Color.DANGER);
    } else {
      var c = confirm("Apakah anda yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "purchase_order/insert",
          data: {
            am: idm,
            av: idv,
            ad: ids,
            aq: iqy,
            an: inp,
            p: ppn,
            c: idx,
            t: tanggal,
            tk: tanggal_kirim,
            pr: pr,
            pt: pt,
            d: diskon
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
            } else {
              toast(result, Color.DANGER);
            }

          }
        });
      }

    }
  }
</script>