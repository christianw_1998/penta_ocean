<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Buat PV</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">WS</li>
        <li class="breadcrumb-item">BOM</li>
        <li class="breadcrumb-item">Production Version</li>
        <li class="breadcrumb-item active" aria-current="page">Input</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>INPUT PRODUCTION VERSION</h1>
    <hr style="margin-left:5%;margin-right:5%">

    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
          <table class="tabel_detail_product" style="visibility:hidden">
            <tr>
              <td class="text-center font-sm f-aleo">Nomor</td>
              <td class="text-center font-sm f-aleo">Kode Produk</td>
              <td class="text-center font-sm f-aleo">Deskripsi</td>
              <td class="text-center font-sm f-aleo">Alt Bom yang Dipakai</td>
              <td class="text-center font-sm f-aleo">Tanggal Valid</td>
              <td class="text-center font-sm f-aleo">Aksi</td>
            </tr>
          </table>
          <div class='text-center'>
            <button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
              Tambah
            </button>
            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="generate()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
        <div class="col-sm-1"></div>

      </div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function plus_product() {

    var selected_idx = [];

    //Simpan semua value index tipe harga
    for (var i = 0; i < ctr; i++) {
      if ($("#dd_currency_" + i).length > 0) {
        selected_idx[i] = $("#dd_currency_" + i).prop("selectedIndex");
      }
    }

    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "pv/plus",
      data: {
        c: ctr,
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;

        //Ubah kembali semua Select menjadi index awal
        for (var i = 0; i < ctr; i++) {
          /*if (typeof selected_idx[i] !== "undefined") {
            $("#dd_currency_" + i).prop("selectedIndex", selected_idx[i]);
          }*/
        }

        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }


  function reset_form() {
    var kal = '<tr> <td class = "text-center font-sm f-aleo" > Kode Produk </td> <td class = "text-center font-sm f-aleo" > Deskripsi </td> <td class = "text-center font-sm f-aleo" > Alt Bom yang Dipakai </td><td class = "text-center font-sm f-aleo" > Tanggal Valid </td><td class = "text-center font-sm f-aleo" > Aksi </td></tr> ';
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    ctr = 0;

  }

  function cek_product(params) {
    var kp = $("#txt_bom_" + params).val();
    $("#txt_desc_bom_" + params).removeClass('green-text red-text');
    $.ajax({
      type: "POST",
      url: site_url + "bom/get_desc_by_product_code",
      data: {
        kp: kp
      },
      dataType: "json",
      success: function(result) {
        if (result.num_rows != 0) {
          $("#txt_desc_bom_" + params).val(result['<?php echo BOM::$DESCRIPTION; ?>']);
          $("#txt_desc_bom_" + params).addClass("green-text");
          get_opt_alt_bom(params, kp);
        } else {
          $("#txt_desc_bom_" + params).val("Produk belum ditemukan");
          $("#txt_desc_bom_" + params).addClass('red-text');
          $("#dd_pv_" + params).html("");
        }
        $("#txt_last_date_" + params).removeClass("red-text green-text");
        if (result['<?php echo BOM_Production_Version::$CHECK_DATE; ?>'] != "") {
          $("#txt_last_date_" + params).addClass("green-text");
          $("#txt_last_date_" + params).val(result['<?php echo BOM_Production_Version::$CHECK_DATE; ?>']);
        } else {
          $("#txt_last_date_" + params).addClass("red-text");
          $("#txt_last_date_" + params).val("");

        }
      }
    });
  }

  function get_opt_alt_bom(params, kp) {
    $.ajax({
      type: "POST",
      url: site_url + "bom/alt_get_opt",
      data: {
        kp: kp
      },
      dataType: "json",
      success: function(result) {
        var option = "";
        for (var i = 0; i < result.ctr; i++) {
          option += "<option value=" + result[i]['<?php echo BOM_Alt::$ID; ?>'] + ">" + result[i]['<?php echo BOM_Alt::$ALT_NO; ?>'] + "</option>";
        }
        $("#dd_pv_" + params).html(option);
      }
    });
  }

  function generate() {
    var arr_kp = new Array();
    var arr_ba = new Array();
    var arr_validfrom = new Array();
    var arr_validto = new Array();
    var idx = 0;
    var is_true = true;


    for (var i = 0; i < ctr; i++) {
      is_true = true;
      if ($("#txt_bom_" + i).length > 0) {
        arr_kp[idx] = $("#txt_bom_" + i).val();
      } else {
        is_true = false;
      }

      if ($("#dd_pv_" + i).length > 0) {
        arr_ba[idx] = $("#dd_pv_" + i).val();
      } else {
        is_true = false;
      }

      if ($("#txt_valid_from_" + i).val() != "" && $("#txt_valid_to_" + i).val() != "") {
        if ($("#txt_valid_from_" + i).val() > $("#txt_valid_to_ " + i).val()) {
          toast("Ada tanggal yang tidak valid, silahkan cek lagi", Color.DANGER);
          return;
        }
        arr_validfrom[idx] = $("#txt_valid_from_" + i).val();
        arr_validto[idx] = $("#txt_valid_to_" + i).val();
      } else {
        is_true = false;
      }
      if (is_true)
        idx++;
    }
    if (is_true && idx != 0) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "pv/insert",
          data: {
            ak: arr_kp,
            ab: arr_ba,
            af: arr_validfrom,
            at: arr_validto,
            c: idx
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
            } else
              toast(result, Color.DANGER);
          }
        });
      }
    } else
      toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
  }
</script>