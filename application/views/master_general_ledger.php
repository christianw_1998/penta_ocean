<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Master General Ledger</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo role-kepala role-accounting role-purchasing">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Master</li>
        <li class="breadcrumb-item active" aria-current="page">General Ledger</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="f-aleo btn btn-outline-primary" onclick="get_master('general_ledger')" data-mdb-ripple-color="dark">
        Tabel General Ledger
      </button>
    </div>
    <div id="content">
    </div>
</body>

</html>


<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {});
</script>