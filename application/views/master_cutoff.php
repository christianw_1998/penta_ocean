<!DOCTYPE html>
<html lang="en"  class="full-height">
<head>
  <title>Cut Off</title>
  <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
  ?>
</head>
<body class="f-aleo role-kepala">
	<?php
    include("navigation.php");
  ?>
  <br>
  <div class=" animated fadeInUp">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home");?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Master Cut Off</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="btn btn-outline-primary" onclick="insert_f_cutoff()" data-mdb-ripple-color="dark">
        Input Periode Cut Off
      </button>
      <button type="button" class="btn btn-outline-primary role-kepala" onclick="get_master('cutoff')" data-mdb-ripple-color="dark">
        Tabel Cut Off
      </button>
    </div>
    <div id="content">
    </div>
  </div>
</body>
</html>

<script language="javascript">
  function insert_cutoff(){
    var tgl_from=$("#txt_tanggal_from").val();
    var tgl_to=$("#txt_tanggal_to").val();
   
    $.ajax({
      type : "POST",
      data : {tf:tgl_from,tt:tgl_to},
      url : site_url+"insert/cutoff",
      success:function(result){
        if(result.includes(Status.MESSAGE_KEY_SUCCESS))
          toast(result,Color.SUCCESS);
        else
          toast(result,Color.DANGER);
        $("#txt_tanggal_from").val("");
        $("#txt_tanggal_to").val("");
      }
    });
    check_role();
    
    
  }
  
  function insert_f_cutoff(){
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
			type : "POST",
			url : site_url+"insert_f/cutoff",
			success:function(result){
				$("#content").html(result);
        $("#content").addClass("animated fadeInDown");
      }
    });
    check_role();
  }
  function delete_cutoff(id){
    var confirmation=confirm("Apakah anda yakin ingin menghapus? Aksi ini tidak dapat dikembalikan!");
    if(confirmation){
      $.ajax({
        type : "POST",
        data : {id:id},
        url : site_url+"delete/cutoff",
        success:function(result){
          toast(result,Color.SUCCESS);
          get_master('cutoff');
        }
      });
      check_role();
    }
  }
  $(document).ready(function(){
    check_role();
    $(".buttons-excel").remove();
    
  });
</script>