<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Input Payment Voucher</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
  <style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  </style>
</head>

<body class="f-aleo role-kepala role-accounting role-purchasing">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item" aria-current="page">Costing</li>
        <li class="breadcrumb-item" aria-current="page">Payment Voucher</li>
        <li class="breadcrumb-item active" aria-current="page">Input</li>
      </ol>
    </nav>
    <hr style="margin-left:5%;margin-right:5%">
    <div class="row" style="margin-right:1%">
      <div class="col-sm-12">
        <h1 class="text-center fw-bold">INPUT PAYMENT VOUCHER</h1>
        <hr style="margin-left:5%;margin-right:5%">
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="row" style="margin-right:1%">
          <div class="col-sm-12">
            <h3 class="text-center fw-bold">INPUT</h3>
            <hr style="margin-left:5%;margin-right:5%">
          </div>
        </div>
        <form id="form_payment">
          <datalist id='list_vendor'>
            <?php
            //Vendor Datalist
            $data_vendor = $this->vendor_model->get(Vendor::$TABLE_NAME);
            if ($data_vendor->num_rows() > 0) {
              foreach ($data_vendor->result_array() as $row_vendor) { ?>
                <option value="<?php echo $row_vendor[Vendor::$ID]; ?>"><?php echo $row_vendor[Vendor::$NAMA]; ?></option>
            <?php
              }
            } ?>
          </datalist>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 text-right">Tanggal</div>
            <div class="col-sm-5">
              <input class="black-text" type="date" style="width:100%" id="txt_tanggal">
            </div>
            <div class="col-sm-2"></div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 text-right">Reference</div>
            <div class="col-sm-5">
              <input class="black-text" type="text" style="width:100%" id="txt_reference" />
            </div>
            <div class="col-sm-2"></div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 text-right">Text</div>
            <div class="col-sm-6">
              <datalist id='list_text'>
                <?php
                //Text Datalist
                $data_text = $this->payment_model->get_newest(Payment_Voucher_H::$TABLE_NAME, Payment_Voucher_H::$TEXT, Payment_Voucher_H::$ID, 10);
                if ($data_text->num_rows() > 0) {
                  foreach ($data_text->result_array() as $row_text) { ?>
                    <option value="<?php echo $row_text[Payment_Voucher_H::$TEXT]; ?>"><?php echo $row_text[Payment_Voucher_H::$TEXT]; ?></option>
                <?php }
                } ?>
              </datalist>
              <input class="black-text" list="list_text" type="text" style="width:100%" id="txt_text" />
            </div>
            <div class="col-sm-1 align-middle"></div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 text-right">Account</div>
            <div class="col-sm-5">
              <datalist id='list_gl'>
                <?php
                //Text Datalist
                $data_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME);
                if ($data_gl->num_rows() > 0) {
                  foreach ($data_gl->result_array() as $row_gl) { ?>
                    <option value="<?php echo $row_gl[General_Ledger::$ID]; ?>"><?php echo $row_gl[General_Ledger::$DESKRIPSI]; ?></option>
                <?php }
                } ?>
              </datalist>
              <input class="black-text" list="list_gl" onkeyup="check_gl()" onchange="check_gl()" type="text" style="width:100%" id="txt_id_gl" />
            </div>
            <div class="col-sm-2">
              <!-- <span id="txt_desc_gl" class="red-text align-middle">GL Tidak Ditemukan</span> -->
            </div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 text-right">Deskripsi GL</div>
            <div class="col-sm-5">
              <span class="red-text" style="width:100%" id="txt_desc_gl">GL tidak ditemukan</span>
            </div>
            <div class="col-sm-2"></div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 text-right">Assignment</div>
            <div class="col-sm-5">
              <input class="black-text" type="text" style="width:100%" id="txt_assignment" />
            </div>
            <div class="col-sm-2"></div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 text-right">Amount</div>
            <div class="col-sm-5">
              <input class="black-text text-right" type="text" style="width:100%" id="txt_total_amount" disabled />
            </div>
            <div class="col-sm-2"></div>

          </div>
        </form>
      </div>
      <div class="col-sm-6">
        <div class="row" style="margin-right:1%">
          <div class="col-sm-12">
            <h3 class="text-center fw-bold">PEMBULATAN</h3>
            <hr style="margin-left:5%;margin-right:5%">
          </div>
        </div>
        <form id="form_pembulatan">
          <div class="row">
            <div class="col-sm-4 text-right">Account</div>
            <div class="col-sm-5">
              <datalist id='list_gl'>
                <?php
                //Text Datalist
                $data_gl = $this->gl_model->get(General_Ledger::$TABLE_NAME);
                if ($data_gl->num_rows() > 0) {
                  foreach ($data_gl->result_array() as $row_gl) { ?>
                    <option value="<?php echo $row_gl[General_Ledger::$ID]; ?>"><?php echo $row_gl[General_Ledger::$DESKRIPSI]; ?></option>
                <?php }
                } ?>
              </datalist>
              <input class="black-text" onkeyup="check_gl()" onchange="check_gl()" list="list_gl" type="text" style="width:100%" id="txt_id_gl_pembulatan" />
            </div>
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-4 text-right">Deskripsi GL</div>
            <div class="col-sm-5">
              <span class="red-text" style="width:100%" id="txt_desc_gl_pembulatan">GL tidak ditemukan</span>
            </div>
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-4 text-right">Tipe</div>
            <div class="col-sm-2">
              <select style="width:100%;font-size:120%" id="dd_tipe_pembulatan" onchange="">
                <option value=1>D</option>
                <option value=-1>K</option>
              </select>
            </div>
            <div class="col-sm-6"></div>
          </div>
          <div class="row">
            <div class="col-sm-4 text-right">Amount</div>
            <div class="col-sm-5">
              <input class="black-text text-left" type="number" onkeyup="calculate_total()" style="width:100%" value=0 id="txt_amount_pembulatan" />
            </div>
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-4 text-right">Cost Center</div>
            <div class="col-sm-5">
              <datalist id='list_cc'>
                <?php
                //CC Datalist
                $data_cc = $this->cc_model->get(Cost_Center::$TABLE_NAME);
                if ($data_cc->num_rows() > 0) {
                  foreach ($data_cc->result_array() as $row_cc) { ?>
                    <option value="<?php echo $row_cc[Cost_Center::$ID]; ?>"><?php echo $row_cc[Cost_Center::$NAMA]; ?></option>
                <?php }
                } ?>
              </datalist>
              <input class="text-left" type="text" list="list_cc" style="width:100%" id="txt_id_cc_pembulatan" />
            </div>
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-4 text-right">Deskripsi</div>
            <div class="col-sm-5">
              <input class="text-left" type="text" style="width:100%" id="txt_desc_pembulatan" />
            </div>
            <div class="col-sm-3"></div>
          </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 text-center">
        <button type="button" id="btn_tambah" class="center btn btn-outline-primary" onclick="plus_vendor()" data-mdb-ripple-color="dark">
          Tambah
        </button>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <table class="tabel_detail_vendor" style="width:100%;visibility:hidden">
          <tr class="fw-bold">
            <td class="text-center">No</td>
            <td class="text-center">ID Vendor</td>
            <td class="text-center">Deskripsi Vendor</td>
            <td class="text-center">Cari</td>
            <td class="text-center">Total</td>
            <td class="text-center">A</td>
          </tr>
        </table>
      </div>
      <div class="col-sm-1"></div>

    </div>
    <div class="row">
      <div class="col-sm-12 text-center">
        <button type="button" id="btn_generate" class=" center btn btn-outline-success" onclick="preview()" data-mdb-ripple-color="dark">
          Preview
        </button>
      </div>
    </div>
  </div>
</body>

</html>
<div class="div_modal_vendor">

</div>
<div class="f-aleo modal fade" id="mod_preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">PREVIEW PAYMENT VOUCHER</h5>
      </div>
      <!--Body-->
      <div class="modal-body" id="modal_preview" style="margin:1%">

      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" onclick="generate()" class="btn btn-outline-success">Generate</button>
      </div>
    </div>
    </form>
    <!--/.Content-->
  </div>
</div>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';
  var ctr_vendor = 0;
  var total = 0;
  var status_class = [];

  $(document).ready(function() {
    $('#mod_preview').appendTo("body");
    $('.div_modal_vendor').appendTo("body");
    $("#form_pembulatan").prop("disabled", "disabled");
  });

  function preview() {
    var arr_id_vendor = new Array();
    var arr_total = new Array();

    var tanggal = $("#txt_tanggal").val();
    var reference = $("#txt_reference").val();
    var text = $("#txt_text").val();
    var assignment = $("#txt_assignment").val();
    var id_gl = $("#txt_id_gl").val();

    var pembulatan_id_gl = $("#txt_id_gl_pembulatan").val();
    var pembulatan_tipe = $("#dd_tipe_pembulatan").val();
    var pembulatan_amount = $("#txt_amount_pembulatan").val();
    var pembulatan_id_cc = $("#txt_id_cc_pembulatan").val();
    var pembulatan_desc = $("#txt_desc_pembulatan").val();


    var idx = 0;
    var is_exist = true;

    for (var i = 0; i < ctr_vendor; i++) {
      is_exist = true;
      //ID Vendor
      if ($("#txt_id_vendor_" + i).length > 0) {
        if ($("#txt_id_vendor_" + i).val() != "") {
          arr_id_vendor[idx] = parseInt($("#txt_id_vendor_" + i).val());
          arr_total[idx] = parseInt($("#txt_total_" + i).val());
        } else {
          toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
          return;
        }
      } else {
        is_exist = false;
      }

      if (is_exist)
        idx++;
    }
    if (idx != 0) {
      $.ajax({
        type: "POST",
        url: site_url + "payment_voucher/view_preview",
        data: {
          av: arr_id_vendor,
          at: arr_total,
          t: tanggal,
          r: reference,
          te: text,
          as: assignment,
          ig: id_gl,
          pg: pembulatan_id_gl,
          pt: pembulatan_tipe,
          pa: pembulatan_amount,
          pc: pembulatan_id_cc,
          pd: pembulatan_desc,
          c: idx
        },
        success: function(result) {
          if (!$('#mod_preview').is(':visible')) {
            $("#modal_preview").html(result);
            $('#mod_preview').modal('show');
          }
        }
      });
    }
  }

  function check_vendor(id) {
    var id_vendor = $("#txt_id_vendor_" + id).val();
    $.ajax({
      type: "POST",
      url: site_url + "vendor/get",
      data: {
        iv: id_vendor,
      },
      dataType: "JSON",
      success: function(result) {
        if (result.num_rows != 0) {
          $("#txt_desc_vendor_" + id).html(result["<?php echo Vendor::$NAMA; ?>"]);
          $("#txt_desc_vendor_" + id).addClass("green-text");
        } else {
          $("#txt_desc_vendor_" + id).html("Vendor tidak ditemukan");
          $("#txt_desc_vendor_" + id).removeClass('green-text');
          $("#txt_desc_vendor_" + id).addClass('red-text');
        }
      }
    });
  }

  function plus_vendor() {
    var temp = $(".tabel_detail_vendor");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();

    $.ajax({
      type: "POST",
      url: site_url + "payment_voucher/plus_vendor",
      data: {
        c: ctr_vendor
      },
      dataType: "json",
      success: function(result) {
        $(".tabel_detail_vendor").html(html + result.kal);
        $(".tabel_detail_vendor").css("visibility", "visible");
        $(".div_modal_vendor").html($(".div_modal_vendor").html() + result.modal);
        ctr_vendor++;
      }
    });
  }

  function minus_vendor(id) {
    var temp = $(".tabel_detail_vendor");

    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_vendor_" + id).remove();
  }

  function is_found_vendor(value) {
    // Gunakan jQuery untuk memeriksa elemen option
    return $("#list_vendor").find('option').filter(function() {
      return $(this).val() === value; // Periksa apakah value cocok
    }).length > 0; // Jika panjangnya > 0, maka nilai ditemukan
  }

  function preview_vendor(id) {
    var id_vendor = $("#txt_id_vendor_" + id).val();
    if (is_found_vendor(id_vendor)) {
      $.ajax({
        type: "POST",
        url: site_url + "payment_voucher/get_vendor_payment_detail",
        data: {
          c: ctr_vendor,
          iv: id_vendor,
        },

        success: function(result) {
          $('#div_payment_vendor_' + id).html(result);
          $('#mod_vendor_' + id).modal('show');

          //Kembalikan semua status class
          var idx_temp = 0;
          do {
            if ($("#txt_action_" + id_vendor + "_" + idx_temp).length > 0) {
              if (status_class[id_vendor][idx_temp] != "") {
                $("#txt_action_" + id_vendor + "_" + idx_temp).addClass(status_class[id_vendor][idx_temp]);
              } else {
                $("#txt_action_" + id_vendor + "_" + idx_temp).removeClass("red-text");

              }
            }
            idx_temp++;
          } while ($("#txt_action_" + id_vendor + "_" + idx_temp).length > 0);
          update_modal_vendor_total(id, id_vendor);

        }
      });
    } else {
      toast("Pastikan vendor yang diinput terdaftar, silahkan cek lagi", Color.DANGER);

    }
  }

  function calculate_total() {
    total = 0;
    for (var i = 0; i < ctr_vendor; i++) {
      if ($("#txt_total_" + i).length > 0) {
        if ($("#txt_total_" + i).val() != "") {
          total += parseInt($("#txt_total_" + i).val());
        }
      }
    }
    var pembulatan = 0;
    if ($("#txt_amount_pembulatan").val() != "") {
      pembulatan = parseInt($("#txt_amount_pembulatan").val() * $("#dd_tipe_pembulatan").val());
    }
    total += pembulatan;
    $("#txt_total_amount").val(add_decimal(Math.abs(total)));

  }

  function calculate_vendor_total(id_vendor) {
    var total = 0;
    for (var i = 0; i < 1000; i++) {
      if ($("#txt_amount_" + id_vendor + "_" + i).length > 0) {
        // if ($("#ck_" + id_vendor + "_" + i).prop("checked")) { //ini diubah
        //   total += parseInt($("#txt_amount_" + id_vendor + "_" + i).val());
        // }
        if ($("#txt_action_" + id_vendor + "_" + i).hasClass("red-text")) {
          total += parseInt($("#txt_amount_" + id_vendor + "_" + i).val());
        }
      } else
        break;
    }
    return total;
  }

  function update_modal_vendor_total(idx, id_vendor) {
    if ($("#ck_" + id_vendor + "_" + idx).prop("checked")) {
      $("#txt_amount_" + id_vendor + "_" + idx).prop("disabled", false);
    } else {
      $("#txt_amount_" + id_vendor + "_" + idx).prop("disabled", true);
      $("#txt_amount_" + id_vendor + "_" + idx).val($("#txt_hidden_amount_" + id_vendor + "_" + idx).val());

    }
    $("#txt_modal_total_" + id_vendor).html(add_decimal(calculate_vendor_total(id_vendor)));
  }

  var ctr_click = 0;

  function toggle_payment_detail(idx, id_vendor) {
    if ($("#txt_action_" + id_vendor + "_" + idx).hasClass("red-text")) {
      $("#txt_action_" + id_vendor + "_" + idx).removeClass("red-text");
    } else {
      $("#txt_action_" + id_vendor + "_" + idx).addClass("red-text");
    }
    update_modal_vendor_total(idx, id_vendor);
  }

  function update_vendor_total(idx) {
    var id_vendor = $("#txt_id_vendor_" + idx).val();
    var total = calculate_vendor_total(id_vendor);
    $('#mod_vendor_' + idx).modal('hide');
    $("#txt_tampil_total_" + idx).html(add_decimal(total));
    $("#txt_total_" + idx).val(total);
    calculate_total();
    save_class_vendor(id_vendor);
  }

  function save_class_vendor(id_vendor) {
    status_class[id_vendor] = [];
    var idx_temp = 0;
    do {
      if ($("#txt_action_" + id_vendor + "_" + idx_temp).length > 0) {
        if ($("#txt_action_" + id_vendor + "_" + idx_temp).hasClass("red-text"))
          status_class[id_vendor][idx_temp] = true;
        else
          status_class[id_vendor][idx_temp] = false;

      }
      idx_temp++;
    } while ($("#txt_action_" + id_vendor + "_" + idx_temp).length > 0);
  }

  function check_gl() {
    if ($("#txt_id_gl_pembulatan").val() != "") {
      var id_gl = $("#txt_id_gl_pembulatan").val();
      if (id_gl != "") {
        $.ajax({
          type: "POST",
          url: site_url + "gl/get",
          data: {
            i: id_gl,
          },
          dataType: "JSON",
          success: function(result) {
            if (result.num_rows != 0) {
              $("#txt_desc_gl_pembulatan").html(result["<?php echo General_Ledger::$DESKRIPSI; ?>"]);
              $("#txt_desc_gl_pembulatan").addClass("green-text");
            } else {
              $("#txt_desc_gl_pembulatan").html("GL tidak ditemukan");
              $("#txt_desc_gl_pembulatan").removeClass('green-text');
              $("#txt_desc_gl_pembulatan").addClass('red-text');
            }
          }
        });
      }
    }
    if ($("#txt_id_gl").val() != "") {
      var id_gl = $("#txt_id_gl").val();
      if (id_gl != "") {
        $.ajax({
          type: "POST",
          url: site_url + "gl/get",
          data: {
            i: id_gl,
          },
          dataType: "JSON",
          success: function(result) {
            if (result.num_rows != 0) {
              $("#txt_desc_gl").html(result["<?php echo General_Ledger::$DESKRIPSI; ?>"]);
              $("#txt_desc_gl").addClass("green-text");
            } else {
              $("#txt_desc_gl").html("GL tidak ditemukan");
              $("#txt_desc_gl").removeClass('green-text');
              $("#txt_desc_gl").addClass('red-text');
            }
          }
        });
      }
    }

  }

  function generate() {
    var arr_id_vendor = new Array();
    var arr_total = new Array();

    var arr_idx_detail_h = new Array();
    var arr_detail_h = new Array();
    var arr_amount_detail_h = new Array();

    var tanggal = $("#txt_tanggal").val();
    var reference = $("#txt_reference").val();
    var text = $("#txt_text").val();
    var assignment = $("#txt_assignment").val();
    var id_gl = $("#txt_id_gl").val();

    var pembulatan_id_gl = $("#txt_id_gl_pembulatan").val();
    var pembulatan_tipe = $("#dd_tipe_pembulatan").val();
    var pembulatan_amount = $("#txt_amount_pembulatan").val();
    var pembulatan_id_cc = $("#txt_id_cc_pembulatan").val();
    var pembulatan_desc = $("#txt_desc_pembulatan").val();

    var idx = 0;
    var idx_loop = 0;
    var is_exist = true;

    for (var i = 0; i < ctr_vendor; i++) {
      is_exist = true;
      //Vendor
      if ($("#txt_id_vendor_" + i).length > 0) {
        if ($("#txt_id_vendor_" + i).val() != "") {
          arr_id_vendor[idx] = parseInt($("#txt_id_vendor_" + i).val());
          arr_total[idx] = parseInt($("#txt_total_" + i).val());
          idx_loop = 0;
          arr_detail_h[idx] = new Array();
          arr_amount_detail_h[idx] = new Array();
          //Ambil id_payment_detail_h
          do {
            if ((status_class[arr_id_vendor[idx]][idx_loop])) {
              if (status_class[arr_id_vendor[idx]][idx_loop] == true) {
                arr_detail_h[idx][idx_loop] = $("#ck_" + arr_id_vendor[idx] + "_" + idx_loop).val();
                arr_amount_detail_h[idx][idx_loop] = $("#txt_amount_" + arr_id_vendor[idx] + "_" + idx_loop).val();
              } else {
                arr_detail_h[idx][idx_loop] = "";
                arr_amount_detail_h[idx][idx_loop] = 0;
              }
            }
            idx_loop++;
          } while ($("#ck_" + arr_id_vendor[idx] + "_" + idx_loop).length > 0);
          arr_idx_detail_h[idx] = idx_loop;
        }
      } else {
        is_exist = false;
      }
      if (is_exist)
        idx++;
    }

    if (idx != 0) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "payment_voucher/insert",
          data: {
            av: arr_id_vendor,
            at: arr_total,
            ad: arr_detail_h,
            ai: arr_idx_detail_h,
            aa: arr_amount_detail_h,
            t: tanggal,
            r: reference,
            te: text,
            as: assignment,
            ig: id_gl,
            pg: pembulatan_id_gl,
            pt: pembulatan_tipe,
            pa: pembulatan_amount,
            pc: pembulatan_id_cc,
            pd: pembulatan_desc,
            cv: idx
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              reset_form();
              $('#mod_preview').modal('hide');
              toast(result, Color.SUCCESS);
            } else
              toast(result, Color.DANGER);
          }
        });
      }
    } else
      toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);
  }

  function reset_form() {
    var kal = '<tr class="fw-bold"><td class="text-center">No</td><td class="text-center">ID Vendor</td><td class="text-center">Deskripsi Vendor</td><td class="text-center">Cari</td><td class="text-center">Total</td><td class="text-center">A</td></tr>';
    $(".tabel_detail_vendor").html(kal);
    $(".tabel_detail_vendor").css("visibility", "hidden");

    $("#form_payment").trigger("reset");
    $("#form_pembulatan").trigger("reset");
    $(".div_vendor").html("");
    $("#txt_desc_gl").html("GL tidak ditemukan");
    $("#txt_desc_gl").removeClass("green-text red-text").addClass("red-text");
    $("#txt_desc_gl_pembulatan").html("GL tidak ditemukan");
    $("#txt_desc_gl_pembulatan").removeClass("green-text red-text").addClass("red-text");
    ctr_vendor = 0;
  }
</script>