<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" style="z-index:1000;width:20%;position: absolute; top: -140px; right: 2%;">
  <div class="toast-body">
    Hello, world! This is a toast message.
  </div>
</div>
<script language="javascript">
  function Export2Word(element, filename = '') {
    $(".btn_print").remove();
    if (!window.Blob) {
      alert('Your legacy browser does not support this action.');
      return;
    }

    var html, link, blob, url, css;

    // EU A4 use: size: 841.95pt 595.35pt;
    // US Letter use: size:11.0in 8.5in;

    css = (
      '<style>' +
      '@page div_print{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
      'div.div_print {page: div_print;}' +
      '</style>'
    );

    html = $("#" + element).html();
    blob = new Blob(['\ufeff', css + html], {
      type: 'application/msword'
    });
    url = URL.createObjectURL(blob);
    link = document.createElement('A');
    link.href = url;
    // Set default file name. 
    // Word will append file extension - do not add an extension here.
    link.download = filename;
    document.body.appendChild(link);
    if (navigator.msSaveOrOpenBlob) navigator.msSaveOrOpenBlob(blob, 'Document.doc'); // IE10-11
    else link.click(); // other browsers
    document.body.removeChild(link);
  };
  $.fn.setNow = function(onlyBlank) {
    var now = new Date($.now());

    var year = now.getFullYear();

    var month = (now.getMonth() + 1).toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
    var date = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate();
    var hours = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
    var minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes();
    var seconds = now.getSeconds().toString().length === 1 ? '0' + now.getSeconds().toString() : now.getSeconds();

    var formattedDateTime = year + '-' + month + '-' + date;

    if (onlyBlank === true && $(this).val()) {
      return this;
    }

    $(this).val(formattedDateTime);

    return this;
  }
  String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  }
  class Color {
    static DEFAULT = "BLACK";
    static SUCCESS = "#00c851";
    static DANGER = "#ff3547";
  }
  class Status {
    static MESSAGE_KEY_SUCCESS = "SUKSES";
    static MESSAGE_KEY_FAILED = "GAGAL";
    static MESSAGE_KEY_CONFIRM = "CONFIRM";
  }

  var eTop;
  var eTopHidden = -140;

  //General Function
  function toast(message, color) {
    if ($(window).scrollTop() <= 10) {
      eTop = 70;
      $('.toast').css("background-color", "rgba(255, 255, 255, 1)");
    } else {
      eTop = 10;
      $('.toast').css("background-color", "rgba(0, 0, 0, 0.1)");
    }
    $('.toast').css("top", eTop + $(window).scrollTop() + "px");
    $('.toast').toast({
      delay: 4000
    });
    $('.toast-body').html(message);
    $('.toast').toast("show");
    $('.toast').css("color", color);
    $('.toast').addClass("f-aleo-bold");

  }

  $(window).scroll(function() {
    console.log($(window).scrollTop());
  });

  $('#myToast').on('hidden.bs.toast', function() {

    $('.toast').css("top", eTopHidden + "px");
    $('.toast').css("color", Color.DEFAULT);
  })

  var empty_excel = ["kalender", "absensi", "cutoff", "absensi_performance_rincian"];
  var remove_datatable = ["absensi_performance_rincian"];
  var site_url = '<?php echo site_url(); ?>';

  function get_master(tipe) {
    $.ajax({
      type: "POST",
      url: site_url + "master/" + tipe,
      success: function(result) {
        $("#content").html(result);
        $("#content").removeClass("animated fadeInDown");

        $('#mastertable').DataTable({
          "order": [],
          paging: true,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $("#content").addClass("animated fadeInDown");
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
        if (empty_excel.includes(tipe))
          $(".buttons-excel").remove();
      }
    });
    check_role();
  }

  function get_file_path(k) {
    var fullPath = $("." + k).val();
    if (fullPath) {
      var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
      var filename = fullPath.substring(startIndex);
      if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
      }
    }
    return filename;
  }

  function change_label(label, file) {
    $("." + label).html(get_file_path(file));
  }

  function gen_laporan(tipe) {
    $(".div_laporan").remove();
    var temp = $("#content").html();
    var bulan = $("#dd_bulan").val();
    var tahun = $("#dd_tahun").val();
    $.ajax({
      type: "POST",
      data: {
        t: tipe,
        b: bulan,
        ta: tahun
      },
      url: site_url + "gen_laporan",
      success: function(result) {
        $("#content").html(temp + result);


        $('#mastertable').DataTable({
          "order": [],
          paging: true,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [{
            extend: 'excelHtml5',
            title: tipe.capitalize()
          }]
        });
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
        tipe += "_laporan";
        if (empty_excel.includes(tipe))
          $(".buttons-excel").remove();
      }
    });
    check_role();
  }

  function gen_rincian(tipe) {
    $(".div_laporan").remove();
    var temp = $("#content").html();
    var tahun = $("#dd_tahun").val();
    var tipe_dd = $("#dd_tipe").val();
    $.ajax({
      type: "POST",
      data: {
        t: tipe,
        ta: tahun,
        td: tipe_dd
      },
      url: site_url + "gen_rincian",
      success: function(result) {
        $("#content").html(temp + result);
        tipe += "_rincian";
        if (!remove_datatable.includes(tipe)) {
          $('#mastertable').DataTable({
            "order": [],
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [{
              extend: 'excelHtml5',
              title: tipe.capitalize()
            }]
          });
        }
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');

        if (empty_excel.includes(tipe))
          $(".buttons-excel").remove();
      }
    });
    check_role();
  }

  function equal($string1, $string2) {
    if ($string1 != null && $string2 != null) {
      if ($string1.toLowerCase() == $string2.toLowerCase()) {
        return true;
      }
    }
    return false;
  }

  function add_decimal(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
  }
</script>