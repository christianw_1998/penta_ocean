<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Standar Insentif</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo role-kepala">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class=" animated fadeInUp">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Master Standar Insentif</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="btn btn-outline-primary" onclick="insert_f_s_insentif()" data-mdb-ripple-color="dark">
        Update Standar Insentif
      </button>
    </div>
    <div id="content">
    </div>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-3"></div>
      <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
      <div class="col-sm-3"></div>
    </div>
  </div>
</body>

</html>

<script language="javascript">
  function update_s_insentif() {
    var standar_tunjangan_kehadiran = $("#txt_t_kehadiran").val();
    var standar_insentif_luar_kota = $("#txt_i_luar_kota").val();

    $.ajax({
      type: "POST",
      data: {
        stk: standar_tunjangan_kehadiran,
        silk: standar_insentif_luar_kota
      },
      url: site_url + "insert/s_insentif",
      success: function(result) {
        if (result.includes(Status.MESSAGE_KEY_SUCCESS))
          toast(result, Color.SUCCESS);
        else
          toast(result, Color.DANGER);

      }
    });
    check_role();


  }

  function refresh_page() {
    $(".txt_desc_upload").html("");
  }

  function insert_f_s_insentif() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "insert_f/s_insentif",
      success: function(result) {
        $("#content").html(result);
        $("#content").addClass("animated fadeInDown");
      }
    });
    check_role();
  }

  $(document).ready(function() {
    check_role();

  });
</script>