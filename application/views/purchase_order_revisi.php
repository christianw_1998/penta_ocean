<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Revisi Data PO</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
  <style>
    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  </style>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Purchase Order</li>
        <li class="breadcrumb-item">Edit</li>
        <li class="breadcrumb-item active" aria-current="page">Revisi Data</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>REVISI DATA</h1>
    <hr style="margin-left:5%;margin-right:5%">

    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 fw-bold text-right'>Nomor PO</div>
          <div class='col-sm-1 text-left '>:</div>
          <div class='col-sm-3 text-left'><input style='width:100%' onkeyup="get_po_by_no()" type="text" id="txt_nomor_po" /></div>
          <div class='col-sm-2 text-right'></div>
        </div>

      </div>
      <div class="col-sm-1"></div>
    </div>
    <div class="row">
      <div class="col-sm-4"></div>
      <div class="col-sm-4 text-center">
        <hr style="margin-left:5%;margin-right:5%">
        <div id="txt_title_po">

        </div>
      </div>
      <div class="col-sm-4"></div>
    </div>
    <div class="row">
      <div class="col-sm-12 text-center">
        <hr style="margin-left:5%;margin-right:5%">
        <table class="tabel_detail_product" style="visibility:hidden">

        </table>
        <button type="button" id="btn_generate" disabled class="center text-center btn btn-outline-success" onclick="gen_po()" data-mdb-ripple-color="dark">
          GENERATE
        </button>
      </div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function gen_po() {
    var idx = 0;

    var inu = [];
    var idm = [];
    var ids = [];
    var idv = [];
    var iqy = [];
    var inp = [];
    var is_true = true;
    var diskon = $("#txt_diskon").val();
    var ppn = $("#txt_ppn").val();
    var nomor_po = $("#txt_nomor_po").val();

    for (var i = 0; i < ctr; i++) {
      if ($("#txt_desc_vendor").hasClass("red-text") || $("#txt_netprice_" + i).hasClass("red-text")) {
        is_true = false;
        break;
      } else
        idv[i] = $("#txt_id_vendor").val();

      idm[i] = parseInt($("#txt_idm_" + i).val());
      if ($("#txt_qty_po_" + i).val() == "")
        is_true = false;
      else
        iqy[i] = parseFloat($("#txt_qty_po_" + i).val());

      ids[i] = $("#txt_desc_" + i).val();
      inu[i] = parseInt($("#txt_no_urut_" + i).val());
      inp[i] = parseFloat($("#txt_netprice_" + i).val());
    }
    if ((diskon == "" || diskon < 0) || ppn == "") {
      is_true = false;
    }
    if (is_true) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "purchase_order/revisi",
          data: {
            am: idm,
            av: idv,
            ad: ids,
            aq: iqy,
            an: inp,
            au: inu,
            np: nomor_po,
            c: ctr,
            d: diskon,
            p: ppn
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
              $("#txt_nomor_po").val("");

            } else
              toast(result, Color.DANGER);

          }
        });
      }
    } else {
      toast("Ada data yang salah, silahkan cek lagi", Color.DANGER);
    }
  }

  function get_po_by_no() {
    var nomor_po = $("#txt_nomor_po").val();
    $.ajax({
      type: "POST",
      url: site_url + "purchase_order/get_view_revisi_by_no",
      data: {
        n: nomor_po
      },
      dataType: "JSON",
      success: function(result) {
        if (result.num_rows != 0) {
          reset_form();
          ctr = result.ctr;
          $("#txt_title_po").html(result.nomor_po);
          $("#txt_tanggal_terima").prop("disabled", false);
          $("#txt_nomor_sj").prop("disabled", false);
          $("#btn_generate").prop("disabled", false);
          $(".tabel_detail_product").html(result.detail_product);
          $(".tabel_detail_product").css("visibility", "visible");
          if (!$.fn.DataTable.isDataTable('.tabel_detail_product')) {
            $('.tabel_detail_product').dataTable({
              paging: false,
              searching: false,
              info: false,
              "pagingType": "full",
              dom: 'Bfrtip',
              buttons: [{
                extend: 'excel',
                title: 'RM PM Masuk - PO no ' + nomor_po
              }]
            });
            $(".buttons-excel").remove();
            $(".buttons-excel span").text('Export ke Excel');
            $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          }
        } else {
          reset_form();
        }
      }
    });
  }

  function get_vendor_by_id(iv) {
    $.ajax({
      type: "POST",
      url: site_url + "vendor/get",
      data: {
        iv: iv,
      },
      dataType: "json",
      success: function(result) {
        $("#txt_desc_vendor").removeClass('green-text red-text');
        if (result.num_rows != 0) {
          $("#txt_desc_vendor").val(result["<?php echo Vendor::$NAMA; ?>"]);
          $("#txt_desc_vendor").addClass("green-text");
          //$("#txt_desc_vendor").prop("disabled", false);

        } else {
          $("#txt_desc_vendor").val("Input 6 digit kode Vendor");
          $("#txt_desc_vendor").addClass('red-text');
          //$("#txt_desc_vendor").prop("disabled", true);

        }
      }
    });
  }

  function refresh_active_price() {
    var ctr_temp = 0;
    do {
      get_active_price(ctr_temp);
      ctr_temp++;
    } while ($("#txt_idm_" + ctr_temp).length > 0);
  }

  function get_active_price(ctr) {
    var id_vendor = $("#txt_id_vendor").val();
    var id_material = $("#txt_idm_" + ctr).val();
    var tanggal_po = $("#txt_tanggal").val();

    $.ajax({
      type: "POST",
      url: site_url + "purchase_order/price_get_active",
      data: {
        iv: id_vendor,
        im: id_material,
        t: tanggal_po
      },
      dataType: "json",
      success: function(result) {
        $("#txt_netprice_" + ctr).removeClass("red-text green-text");

        if (result.num_rows != 0) {
          $("#txt_netprice_" + ctr).val(Math.round(result["<?php echo Purchase_Order_Price_List::$PRICE; ?>"]));
          $("#txt_netprice_" + ctr).addClass('green-text');
        } else {
          $("#txt_netprice_" + ctr).val("");
          $("#txt_netprice_" + ctr).addClass('red-text');

        }
        calculate_total(ctr);

      }
    });
  }

  function calculate_total(ctr) {
    $("#txt_tampil_total_" + ctr).removeClass("red-text");
    if ($("#txt_qty_po_" + ctr).val() != "") {
      var qty = $("#txt_qty_po_" + ctr).val();
      try {
        var netprice = $("#txt_netprice_" + ctr).val();
        if (netprice == "") {
          netprice = parseInt(netprice);
          $("#txt_tampil_total_" + ctr).html("Input Pricelist terlebih dahulu");
          $("#txt_tampil_total_" + ctr).addClass("red-text");
          $("#txt_total_" + ctr).val(0);
        } else {
          var total = qty * netprice;
          $("#txt_tampil_total_" + ctr).html(add_decimal(Math.round(total)));
          $("#txt_total_" + ctr).val(Math.round(total));
        }
      } catch (error) {
        $("#txt_tampil_total_" + ctr).html("Input Pricelist terlebih dahulu");
        $("#txt_tampil_total_" + ctr).addClass("red-text");
        $("#txt_total_" + ctr).val(0);
      }
    } else {
      $("#txt_tampil_total_" + ctr).html("Input Qty terlebih dahulu");
      $("#txt_tampil_total_" + ctr).addClass("red-text");
      $("#txt_total_" + ctr).val(0);
    }
    calculate_total_all();
  }

  function calculate_total_all() {
    var idx = 0;
    var total = 0;
    $("#txt_tampil_total").removeClass("red-text");

    do {
      if (!$("#txt_tampil_total_" + idx).hasClass("red-text")) {
        total += parseInt($("#txt_total_" + idx).val());
      } else {
        $("#txt_tampil_total").html("Pastikan tidak ada total yang salah");
        $("#txt_tampil_total").addClass("red-text");
        calculate_total_with_ppn();

        return;
      }
      idx++;
    } while ($("#txt_tampil_total_" + idx).length > 0);
    $("#txt_tampil_total").html(add_decimal(Math.round(total)));

    calculate_total_with_ppn();
  }

  function calculate_total_with_ppn() {

    $("#txt_tampil_total_ppn").removeClass("red-text");
    var diskon = $("#txt_diskon").val();
    var ppn = $("#txt_ppn").val();

    if (diskon == "") {
      $("#txt_tampil_total_ppn").addClass("red-text");
      $("#txt_tampil_total_ppn").html("Input Diskon(Rupiah) terlebih dahulu");
      return;
    } else if (ppn == "") {
      $("#txt_tampil_total_ppn").addClass("red-text");
      $("#txt_tampil_total_ppn").html("Input PPN(%) terlebih dahulu");
      return;
    } else {
      var idx = 0;
      var total = 0;
      do {
        if (!$("#txt_tampil_total_" + idx).hasClass("red-text")) {
          total += parseInt($("#txt_total_" + idx).val());
        } else {
          $("#txt_tampil_total_ppn").html("Pastikan tidak ada total yang salah");
          $("#txt_tampil_total_ppn").addClass("red-text");
          return;
        }
        idx++;
      } while ($("#txt_tampil_total_" + idx).length > 0);
      total -= parseInt(diskon);
      var total_ppn = (total * (parseInt(ppn) + 100) / 100);

      $("#txt_tampil_total_ppn").html(add_decimal(Math.round(total_ppn)));

    }
  }

  function reset_form() {

    $(".tabel_detail_product").html("");
    $(".tabel_detail_product").css("visibility", "hidden");
    $("#txt_title_po").html("");
    $("#txt_tanggal_terima").prop("disabled", true);
    $("#txt_tanggal_terima").val("");
    $("#txt_nomor_sj").val("");
    $("#txt_nomor_sj").prop("disabled", true);
    $("#btn_generate").prop("disabled", true);
    $(".buttons-excel").remove();
  }
</script>