<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Barang Masuk (Mutasi)</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">RM PM</li>
        <li class="breadcrumb-item">Barang Masuk</li>
        <li class="breadcrumb-item active" aria-current="page">Mutasi</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[MUTASI]</h3>
    <h1 class='f-aleo-bold text-center'>BARANG MASUK</h1>
    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
          <table class="tabel_header_product table">
            <tr>

              <td class='align-middle text-right font-sm'>Tanggal</td>
              <td class='align-middle text-left font-sm'>:</td>
              <td class='align-middle text-left font-sm' style='width:80%'>
                <input value="" disabled type="text" id="txt_tanggal" />
              </td>

              <td class='align-middle text-right font-sm'>Keterangan</td>
              <td class='align-middle text-left font-sm'>:</td>
              <td class='align-middle text-left font-sm'>
                <textarea rows=5 style="resize: none" type="text" id="txt_keterangan"></textarea>
              </td>
            </tr>
          </table>
          <hr style="margin-left:5%;margin-right:5%">
          <table class="tabel_detail_product" style="visibility:hidden">
            <tr>
              <td class="text-center font-sm">Nomor</td>
              <td class="text-center font-sm">Kode Panggil</td>
              <td class="text-center font-sm">Deskripsi</td>
              <td class="text-center font-sm">Vendor</td>
              <td class="text-center font-sm">Stock</td>
              <td class="text-center font-sm">Qty Masuk</td>
              <td class="text-center font-sm">Aksi</td>
            </tr>
          </table>
          <div class='text-center'>
            <button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
              Tambah
            </button>
            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="gen_adjustment_ws()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
        <div class="col-sm-1"></div>
      </div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
    var today = new Date();

    var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
    $("#txt_tanggal").val(date);
  });

  function plus_product() {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "material_rm_pm/plus",
      data: {
        c: ctr,
        t: "<?php echo RMPM_PLUS_TYPE_IN ?>"
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;
        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function check_product(params) {
    var kode_panggil = $("#txt_kode_panggil_" + params).val();
    if (kode_panggil != "") {
      $.ajax({
        type: "POST",
        url: site_url + "material_rm_pm/get",
        data: {
          kp: kode_panggil,
          t: "<?php echo RMPM_GET_TYPE_MUTASI; ?>"

        },
        dataType: "JSON",
        success: function(result) {
          if (result.num_rows != 0) {
            $("#txt_barang_desc_" + params).val(result.description);
            $("#txt_barang_desc_" + params).addClass("green-text");
            $("#txt_id_material_" + params).val(result.id_material_rm_pm);
            $("#txt_stock_" + params).val(result.stock);

            if (result.ctr_vendor > 0) { //ada vendor
              var select = "<select onchange=change_stock(" + params + ") class='form-control f-aleo' id='cb_vendor_" + params + "'>";
              for (var i = 0; i < result.ctr_vendor; i++) {
                select += "<option value='" + result.vendor_qty[i] + "'>" + result.vendor_nama[i] + "</option>";
              }
              select += "</select>";
              for (var i = 0; i < result.ctr_vendor; i++) {
                select += '<input type="hidden" id="txt_id_vendor_' + params + '_' + i + '" value="' + result.vendor_id[i] + '"/>';
              }
              $("#td_vendor_" + params).html(select);
            }
            if (result.ctr_konsinyasi > 0) { //ada konsinyasi
              var select = "<select onchange=change_stock_konsinyasi(" + params + ") class='form-control f-aleo' id='cb_konsinyasi_" + params + "'>";
              for (var i = 0; i < result.ctr_konsinyasi; i++) {
                select += "<option value='" + result.konsinyasi_qty[i] + "'>" + result.konsinyasi_nama[i] + "</option>";
              }
              select += "</select>";
              for (var i = 0; i < result.ctr_konsinyasi; i++) {
                select += '<input type="hidden" id="txt_id_konsinyasi_' + params + '_' + i + '" value="' + result.konsinyasi_id[i] + '"/>';
              }
              $("#td_vendor_" + params).html(select);
            }

          } else {
            $("#txt_barang_desc_" + params).val("barang belum ditemukan!");
            $("#txt_barang_desc_" + params).removeClass('green-text');
            $("#txt_barang_desc_" + params).addClass('red-text');

            $("#td_vendor_" + params).html("");

            $("#txt_stock_" + params).removeClass('green-text red-text');
            $("#txt_stock_" + params).val(0);

          }
        }
      });
    }
  }


  function reset_form() {
    var kal = '<tr><td class="text-center font-sm f-aleo">Nomor</td><td class="text-center font-sm f-aleo">Kode Panggil</td><td class="text-center font-sm f-aleo">Deskripsi</td><td class="text-center font-sm f-aleo">Vendor</td><td class="text-center font-sm f-aleo">Stock</td><td class="text-center font-sm f-aleo">Qty Masuk</td><td class="text-center font-sm f-aleo">Aksi</td></tr>'
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    ctr = 0;
    $("#rbSO").prop("checked", true);
    $("#txt_kode_GR").html("");
    $("#btn_generate").prop("disabled", false);
    $("#btn_tambah_barang").prop("disabled", false);
    $(".buttons-excel").remove();
    $("#txt_keterangan").val("");
  }

  function change_stock(ctr) {
    //$("#txt_view_stock_" + ctr).html(value);
    $("#txt_stock_" + ctr).val($("#cb_vendor_" + ctr).val());
    //check_adjustment_qty(ctr);
  }

  function change_stock_konsinyasi(ctr) {
    //$("#txt_view_stock_" + ctr).html(value);
    $("#txt_stock_" + ctr).val($("#cb_konsinyasi_" + ctr).val());
    //check_adjustment_qty(ctr);
  }

  function check_adjustment_qty(params) {
    /*$("#txt_stock_" + params).removeClass("red-text green-text");
    var total_inp_stock = 0;
    if ($("#txt_qty_proses_" + params).length > 0)
      total_inp_stock = parseFloat($("#txt_qty_proses_" + params).val());
    var total_stock = parseFloat($("#txt_stock_" + params).val());
    if (total_stock < total_inp_stock) {
      $("#txt_stock_" + params).addClass("red-text");
    } else {
      $("#txt_stock_" + params).addClass("green-text");
    }*/
  }

  function gen_adjustment_ws() {
    var arr_id = new Array();
    var arr_qty = new Array();
    var arr_vendor = new Array();
    var idx = 0;
    var is_true = true;
    var qty_proses = 0;
    var qty_stock = 0;
    var qty_ws = 0;

    var keterangan = $("#txt_keterangan").val();

    for (var i = 0; i < ctr; i++) {
      if ($("#txt_kode_panggil_" + i).length > 0) {
        if ($("#txt_stock_" + i).hasClass("red-text")) {
          is_true = false;
          break;
        }
        arr_id[idx] = parseInt($("#txt_id_material_" + i).val());

        if ($("#txt_qty_proses_" + i).val() == "")
          qty_proses = 0;
        else
          qty_proses = parseFloat($("#txt_qty_proses_" + i).val());
        qty_stock = parseFloat($("#txt_stock_" + i).val());

        arr_qty[idx] = qty_proses;

        if ($("#cb_vendor_" + i).length > 0)
          arr_vendor[idx] = $("#txt_id_vendor_" + i + "_" + $("#cb_vendor_" + i)[0].selectedIndex).val();
        else
          arr_vendor[idx] = null;

        if ($("#cb_konsinyasi_" + i).length > 0)
          arr_id[idx] = $("#txt_id_konsinyasi_" + i + "_" + $("#cb_konsinyasi_" + i)[0].selectedIndex).val();

        idx++;
      }
    }
    if (is_true) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "material_rm_pm/mutasi_masuk",
          data: {
            ai: arr_id,
            aq: arr_qty,
            av: arr_vendor,
            c: idx,
            k: keterangan
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
            } else
              toast(result, Color.DANGER);
          }
        });
      }
    } else
      toast("Ada material yang qty diproses kosong atau melebihi qty WS, silahkan cek lagi", Color.DANGER);
  }
</script>