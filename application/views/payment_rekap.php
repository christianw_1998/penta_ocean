<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Rekap Payment</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Costing</li>
        <li class="breadcrumb-item">Payment</li>
        <li class="breadcrumb-item active" aria-current="page">Rekap</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>REKAP PAYMENT</h1>
    <br>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 text-right'>No. Header</div>
          <div class='col-sm-1 text-left '>:</div>
          <?php
          $data_order = $this->payment_model->get_newest(Payment_Voucher_Detail_H::$TABLE_NAME, Payment_Voucher_Detail_H::$ID, Payment_Voucher_Detail_H::$ID, 1);
          if ($data_order->num_rows() > 0) { ?>
            <div class='col-sm-3 text-left'><input style='width:100%' type="text" id="txt_id_header" value="<?php echo $data_order->row_array()[Payment_Voucher_Detail_H::$ID]; ?>" /></div>

          <?php } else { ?>
            <div class='col-sm-3 text-left'><input style='width:100%' type="text" id="txt_id_header"/></div>
          <?php }
          ?>
          <div class='col-sm-2 text-right'></div>
        </div>
        <br>
        <div class='text-center'>
          <button type="button" class="center btn btn-outline-primary" onclick="get_rekap_by_id()" data-mdb-ripple-color="dark">
            GENERATE
          </button>
        </div>
        <br>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function get_rekap_by_id() {
    var id_header = $("#txt_id_header").val();
    $("#content").removeClass("animated fadeInDown");

    $.ajax({
      type: "POST",
      url: site_url + "payment/view_rekap",
      data: {
        i: id_header
      },
      dataType: "JSON",
      success: function(result) {
        if (result.message.includes(Status.MESSAGE_KEY_SUCCESS)) {
          $("#content").html(result.kal);
          $('#mastertable').DataTable({
            "pageLength": -1,
            "drawCallback": function(settings) {
              check_role();
            },
            "order": [],
            "columnDefs": [{
              "orderable": false,
              "targets": 0
            }],
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          //$(".buttons-excel").remove();
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");
        } else {
          reset_form();
          toast(result.message, Color.DANGER);
        }

      }
    });
  }

  function edit() {
    if ($("#txt_text").prop("disabled")) {
      $("#txt_text").prop("disabled", false);
      $("#btn_update").prop("disabled", false);
    } else {
      $("#txt_text").prop("disabled", true);
      $("#btn_update").prop("disabled", true);
    }
    ctr = parseInt($("#txt_ctr").val());
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_assignment_" + i).prop("disabled")) {
        $("#txt_assignment_" + i).prop("disabled", false);
        $("#txt_desc_" + i).prop("disabled", false);
      } else {
        $("#txt_assignment_" + i).prop("disabled", true);
        $("#txt_desc_" + i).prop("disabled", true);
      }

    }
  }

  function update() {
    var header = $("#txt_header").html();
    var text = $("#txt_text").val();
    var arr_assignment = [];
    var arr_id_detail = [];
    var arr_desc = [];
    for (var i = 0; i < ctr; i++) {
      arr_assignment[i] = $("#txt_assignment_" + i).val();
      arr_id_detail[i] = $("#txt_detail_" + i).val();
      arr_desc[i] = $("#txt_desc_" + i).val();
    }
    var c = confirm("Apakah Anda Yakin?");

    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "payment_voucher/detail/update",
        data: {
          te: text,
          h: header,
          aa: arr_assignment,
          ad: arr_id_detail,
          an: arr_desc,
          c: ctr
        },
        success: function(result) {
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            toast(result, Color.SUCCESS);
            reset_form();
          } else {
            toast(result, Color.DANGER);
          }

        }
      });
    }
  }

  function cancel() {
    var header = $("#txt_header").html();

    var p = prompt("Silahkan Input Alasan Cancel");
    var c = confirm("Apakah Anda Yakin?");
    if (c) {
      $.ajax({
        type: "POST",
        url: site_url + "payment/cancel",
        data: {
          i: header,
          r: p
        },
        success: function(result) {
          if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
            toast(result, Color.SUCCESS);
            reset_form();
          } else {
            toast(result, Color.DANGER);
          }

        }
      });
    }
  }

  function reset_form() {
    $("#content").html("");

  }
</script>