<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Cek WS Semi & WS FG</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">WS</li>
        <li class="breadcrumb-item">Worksheet</li>
        <li class="breadcrumb-item">Lihat Data</li>
        <li class="breadcrumb-item active" aria-current="page">Cek Semi & FG</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[In KG]</h3>
    <h1 class='f-aleo-bold text-center'>PENGECEKAN WS SEMI & FG</h1>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span> <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <br>
          <div class='text-center'>
            <button type="button" class="center btn btn-outline-primary" onclick="get_selisih()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
    <div class="row" style="margin-top:-2%;margin-right:1%;margin-bottom:5%">
      <div class="col-sm-3"></div>
      <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
      <div class="col-sm-3"></div>
    </div>
  </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    } else {
      if ($("#txt_" + value).prop('disabled') == true)
        $("#txt_" + value).prop('disabled', false);
      else
        $("#txt_" + value).prop('disabled', true);
    }
  }

  function get_selisih() {
    var dari_tanggal = null;
    var sampai_tanggal = null;
    var is_done = true;
    var nomor_sj = null;
    var material = null;

    //dari tanggal sampai tanggal filter
    if ($("#cb_tanggal").prop("checked")) {
      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari!!", Color.DANGER);
          is_done = false;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
        }
      } else {
        toast("Ada tanggal yang masih kosong!!", Color.DANGER);
        is_done = false;
      }
    }
    if (is_done) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "work_sheet/view_report_selisih",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal
        },
        success: function(result) {
          $("#content").html(result);
          $('#mastertable').DataTable({
            "drawCallback": function(settings) {
              check_role();
            },
            "order": [
              [2, "desc"]
            ], //selisih
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          //$(".buttons-excel").remove();
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");
        }
      });
    }
  }



  $(document).ready(function() {
    check_role();
  });
</script>