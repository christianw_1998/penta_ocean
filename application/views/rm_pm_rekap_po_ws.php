<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Rekap PO & WS</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">RM PM</li>
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item active" aria-current="page">Rekap PO & WS</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>REKAP PO & WS</h1>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span> <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="nomor_sj" id="cb_nomor_sj">
            </div>
            <div class="col-sm-4">
              <span onclick="toggle_checkbox('nomor_sj')">Nomor PO / WS </span>
              <input type='text' disabled class="form-control f-aleo font-sm" id="txt_nomor_sj"> </input>
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="material" id="cb_material">
            </div>
            <div class="col-sm-4">
              <span onclick="toggle_checkbox('material')">Material </span>
              <select disabled class="form-control f-aleo font-sm" id="txt_material">

              </select>
            </div>
            <div class="col-sm-1"></div>
          </div>
          <br>
          <div class='text-center'>
            <button type="button" class="f-aleo center btn btn-outline-primary" onclick="gen_rekap_po_ws()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    } else if (equal(value, "kode_barang")) {
      $("#txt_kode_barang").val('');
      $("#txt_kemasan").val('');
      $("#txt_warna").val('');

      if ($("#txt_kode_barang").prop('disabled') == true)
        $("#txt_kode_barang").prop('disabled', false);
      else
        $("#txt_kode_barang").prop('disabled', true);
      $("#txt_kemasan").prop('disabled', true);
      $("#txt_warna").prop('disabled', true);
    } else {
      if ($("#txt_" + value).prop('disabled') == true) {
        $("#txt_" + value).prop('disabled', false);

      } else {
        $("#txt_" + value).prop('disabled', true);

      }
    }
  }

  function generate_other_combo_box(tipe) {
    $("#txt_warna").prop("disabled", true);
    if (!equal($("#txt_" + tipe).val(), "") && !$("#txt_" + tipe).prop('disabled')) {
      if (equal(tipe, "kode_barang")) {
        $("#txt_kemasan").prop("disabled", false);
        $("#txt_kemasan").val("");
        $("#txt_warna").val("");
        get_all_material('KEMASAN');
      } else {

        $("#txt_warna").prop("disabled", false);
        get_all_material('WARNA');
      }
    } else {
      if (equal(tipe, "kode_barang")) {
        $("#txt_kemasan").prop("disabled", true);
      }

    }
  }

  function get_all_material() {
    $.ajax({
      type: "POST",
      url: site_url + "material_rm_pm/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";
        for (var i = 0; i < result.length; i++) {
          kal += "<option value='" + result[i].id + "'>" + result[i].description + "</option>";
        }
        $("#txt_material").html(kal);
      }
    });
  }

  function gen_rekap_po_ws() {
    var dari_tanggal = null;
    var sampai_tanggal = null;
    var is_filtered = false;
    var nomor_sj = null;
    var material = null;

    //dari tanggal sampai tanggal filter
    if ($("#cb_tanggal").prop("checked")) {
      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }

    //material filter
    if ($("#cb_material").prop("checked")) {
      if ($("#txt_material").val() != "") {
        material = $("#txt_material").val();
        is_filtered = true;
      } else {
        toast("Material masih belum dipilih", Color.DANGER);
        return;
      }
    }

    //nomor_sj filter
    if ($("#cb_nomor_sj").prop("checked")) {
      if ($("#txt_nomor_sj").val() != "") {
        nomor_sj = $("#txt_nomor_sj").val();
        is_filtered = true;
      } else {
        toast("Nomor PO / WS masih belum diisi", Color.DANGER);
        return;
      }
    }
    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "material_rm_pm/view_rekap_po_ws",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal,
          n: nomor_sj,
          m: material
        },
        success: function(result) {
          $("#content").html(result);
          $('#mastertable').DataTable({
            "order": [
              [0, "desc"]
            ], //Tanggal PO WS
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");
        }
      });
    } else
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
  }
  $(document).ready(function() {
    check_role();
    get_all_material();
    <?php
    if (isset($today_info)) {
    ?>
      $("#cb_tanggal").prop("checked", true);
      toggle("tanggal");
      $("#txt_tanggal_dari").setNow();
      $("#txt_tanggal_sampai").setNow();
    <?php } ?>
  });
</script>