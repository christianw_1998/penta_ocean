<!DOCTYPE html>
<html lang="en"  class="full-height">
<head>
  <title>Kalender</title>
  <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
  ?>
</head>
<body class="f-aleo role-kepala">
	<?php
    include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInUp">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home");?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Master Kalender</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="btn btn-outline-primary" onclick="insert_f_calendar()" data-mdb-ripple-color="dark">
        Input Hari Libur
      </button>
      <button type="button" class="btn btn-outline-primary role-kepala" onclick="get_master('kalender')" data-mdb-ripple-color="dark">
        Tabel Kalender - Libur
      </button>
    </div>
    <div id="content">
    </div>
  </div>
</body>
</html>

<script language="javascript">
  var site_url='<?php echo site_url();?>';
  function insert_calendar(){
    var tgl=$("#txt_tanggal").val();
    var keterangan=$("#txt_keterangan").val();
   
    $.ajax({
      type : "POST",
      data : {t:tgl,k:keterangan},
      url : site_url+"insert/kalender",
      success:function(result){
        if(result.includes(Status.MESSAGE_KEY_SUCCESS))
          toast(result,Color.SUCCESS);
        else
          toast(result,Color.DANGER);
        $("#txt_tanggal").val("");
        $("#txt_keterangan").val("");
      }
    });
    check_role();
  
    
  }
  function insert_f_calendar(){
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
			type : "POST",
			url : site_url+"insert_f/kalender",
			success:function(result){
				$("#content").html(result);
        $("#content").addClass("animated fadeInDown");
      }
    });
    check_role();
  }
  function delete_kalender(id){
    var confirmation=confirm("Apakah anda yakin ingin menghapus? Aksi ini tidak dapat dikembalikan!");
    if(confirmation){
      $.ajax({
        type : "POST",
        data : {id:id},
        url : site_url+"delete/kalender",
        success:function(result){
          toast(result,Color.SUCCESS);
          get_master('kalender');
        }
      });
      check_role();
    }
  }
  $(document).ready(function(){
    check_role();
    $(".buttons-excel").remove();
    
  });
</script>