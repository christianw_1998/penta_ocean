<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Input Kinerja Keluar</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Kinerja</li>
        <li class="breadcrumb-item">Input</li>
        <li class="breadcrumb-item active" aria-current="page">Keluar</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[Keluar]</h3>
    <h1 class='f-aleo-bold text-center'>INPUT KINERJA</h1>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span> <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <br>
          <div class='text-center'>
            <button type="button" class="center btn btn-outline-primary" onclick="get_all_good_issue()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  $(document).ready(function() {
    check_role();
    $('#mod_detail_kinerja').appendTo("body");
  });

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    }
  }

  function get_all_good_issue() {
    var dari_tanggal = null;
    var sampai_tanggal = null;
    var is_filtered = false;

    //dari tanggal sampai tanggal filter
    if ($("#cb_tanggal").prop("checked")) {
      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }
    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");

      $.ajax({
        type: "POST",
        url: site_url + "kinerja/finished/get_good_issue",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal
        },
        success: function(result) {
          $("#content").html(result);
          $('#mastertable').DataTable({
            "order": [],
            "columnDefs": [{
              "orderable": false,
              "targets": 0
            }],
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          $(".buttons-excel").remove();
          $("#content").addClass("animated fadeInDown");

        }
      });
    } else
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
  }

  function fill_detail_modal(no_sj) {
    $("#txt_no_sj").html(no_sj);
    $.ajax({
      type: "POST",
      url: site_url + "kinerja/finished/view_detail_sj_out",
      data: {
        n: no_sj
      },
      dataType: "json",
      success: function(result) {

        $("#dd_tipe_ekspedisi").val(result.tipe_ekspedisi);
        $("#dd_ekspedisi").html(result.opt_ekspedisi);
        $("#dd_tujuan").html(result.opt_tujuan);
        $("#txt_tanggal").val(result.tanggal);
        $("#txt_tanggal").css("color", "black");

        $(".ekspedisi_CONTAINER").hide();
        $(".ekspedisi_LCL").hide();
        $(".ekspedisi_SIRCLO").hide();

        if (result.tipe_ekspedisi.toUpperCase() == "CONTAINER") {
          $("#div_sopir").css("visibility", "hidden");
          $("#div_kernet").css("visibility", "hidden");
          $(".ekspedisi_CONTAINER").show();
        } else if (result.tipe_ekspedisi.toUpperCase() == "LCL") {
          $("#div_sopir").css("visibility", "visible");
          $("#div_kernet").css("visibility", "visible");
          $(".ekspedisi_LCL").show();
        } else {
          $("#div_sopir").css("visibility", "visible");
          $("#div_kernet").css("visibility", "hidden");
          $(".ekspedisi_SIRCLO").show();
        }
        $("#dd_sopir").html(result.opt_sopir);
        $("#dd_kernet").html(result.opt_kernet);
        show_tujuan(true);

        $('#mod_detail_kinerja').modal('show');
      }
    });
  }

  function show_tujuan(first_load) {
    $(".opt_tujuan").hide();
    $(".tujuan_CONTAINER").hide();
    $(".tujuan_LCL").hide();
    $(".tujuan_SIRCLO").hide();

    if ($("#dd_ekspedisi").val() != -1) {
      $("#div_tujuan").css("visibility", "visible");
      $(".arr_" + $("#dd_ekspedisi").val()).show();

      if (!first_load)
        $("#dd_tujuan").val(-1);

    } else {
      $("#div_tujuan").css("visibility", "hidden");
    }
  }

  function check_view_lcl_container(params) {
    $(".ekspedisi_CONTAINER").hide();
    $(".ekspedisi_LCL").hide();
    $(".ekspedisi_SIRCLO").hide();

    $("#dd_ekspedisi").val(-1);
    if (params.toUpperCase() == "CONTAINER") {
      $("#div_sopir").css("visibility", "hidden");
      $("#div_kernet").css("visibility", "hidden");
      $(".ekspedisi_CONTAINER").show();
    } else if (params.toUpperCase() == "LCL") {
      $("#div_sopir").prop("selectedIndex", 0);
      $("#div_kernet").prop("selectedIndex", 0);
      $("#div_sopir").css("visibility", "visible");
      $("#div_kernet").css("visibility", "visible");
      $(".ekspedisi_LCL").show();
    } else {
      $("#div_sopir").prop("selectedIndex", 0);
      $("#div_kernet").prop("selectedIndex", 0);
      $("#div_sopir").css("visibility", "visible");
      $("#div_kernet").css("visibility", "hidden");
      $(".ekspedisi_SIRCLO").show();
    }
    show_tujuan(false);
  }

  function confirm_kinerja_sj() {
    var tipe_ekspedisi = $("#dd_tipe_ekspedisi").val();
    var sopir = $("#dd_sopir").val();
    var kernet = $("#dd_kernet").val();
    var no_sj = $("#txt_no_sj").html();
    var id_ekspedisi = $("#dd_tujuan").val();
    var tanggal = $("#txt_tanggal").val();

    if (tanggal == "")
      alert("Ubah detail gagal karena tanggal masih kosong");
    else if (id_ekspedisi == null || id_ekspedisi == -1)
      alert("Ubah detail gagal karena ekspedisi belum dipilih");
    else {
      var c = confirm("Apakah anda yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "kinerja/finished/insert_good_issue",
          data: {
            n: no_sj,
            t: tanggal,
            ns: sopir,
            nk: kernet,
            te: tipe_ekspedisi,
            ie: id_ekspedisi
          },
          success: function(result) {
            alert(result);
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              $('#mod_detail_kinerja').modal('hide');
              $("#txt_tanggal").val("");
              get_all_good_issue();
            }
          }
        });
      }
    }

  }
</script>
<!-- Modal Detail SJ Kinerja -->
<div class="modal fade" id="mod_detail_kinerja" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">Detail Surat Jalan</h5>
      </div>
      <!--Body-->
      <div class="modal-body" style="margin:1%">
        <div class="row">
          <div class="col-sm-3 text-right"></div>
          <div class="col-sm-3 text-right">
            Nomor Surat Jalan:
          </div>
          <div class="col-sm-4 text-left">
            <span id='txt_no_sj' class="fw-bold"></span>
          </div>
          <div class="col-sm-3"></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-right"></div>
          <div class="col-sm-3 text-right">
            Tanggal Pengiriman:
          </div>
          <div class="col-sm-4 text-left">
            <input class="f-aleo" type="date" id="txt_tanggal" />
          </div>
          <div class="col-sm-3"></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-right"></div>
          <div class="col-sm-3 text-right align-middle">
            Tipe Ekspedisi:
          </div>
          <div class="col-sm-4 text-left">
            <select class="form-control f-aleo" onchange="check_view_lcl_container(this.value)" id='dd_tipe_ekspedisi' style='width:100%'>
              <?php
              for ($i = 0; $i < count(ARR_EKSPEDISI); $i++) {

              ?>
                <option value=<?php echo ARR_EKSPEDISI[$i] ?>><?php echo ARR_EKSPEDISI[$i] ?></option>
              <?php
              }
              ?>
            </select>
          </div>
          <div class="col-sm-3"></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-right"></div>
          <div class="col-sm-3 text-right align-middle">
            Nama Ekspedisi:
          </div>
          <div class="col-sm-4 text-left">
            <select class="form-control f-aleo" onchange='show_tujuan(false)' id='dd_ekspedisi' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-3"></div>
        </div>
        <div class="row" id='div_tujuan'>
          <div class="col-sm-3 text-right"></div>
          <div class="col-sm-3 text-right align-middle">
            Tujuan:
          </div>
          <div class="col-sm-4 text-left">
            <select class="form-control f-aleo" id='dd_tujuan' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-3"></div>
        </div>
        <div id='div_sopir' style='visibility:visible' class="row">
          <div class="col-sm-3 text-right"></div>
          <div class="col-sm-3 text-right align-middle">
            Sopir:
          </div>
          <div class="col-sm-4 text-left">
            <select class="form-control f-aleo" id='dd_sopir' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-3"></div>
        </div>
        <div id='div_kernet' style='visibility:visible' class="row">
          <div class="col-sm-3 text-right"></div>
          <div class="col-sm-3 text-right align-middle">
            Kernet:
          </div>
          <div class="col-sm-4 text-left">
            <select class="form-control f-aleo" id='dd_kernet' style='width:100%'>

            </select>
          </div>
          <div class="col-sm-3"></div>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button onclick="confirm_kinerja_sj()" id="btn_insert_detail_sj_kinerja" class="btn btn-outline-success">SUBMIT</button>
      </div>
      <!--/.Content-->
    </div>
  </div>
</div>
<!-- modal -->