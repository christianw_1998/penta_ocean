<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Laporan Stock</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item active" aria-current="page">Laporan</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>LAPORAN STOCK</h1>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="btn btn-outline-primary" onclick="get_all_stock()" data-mdb-ripple-color="dark">
        Stock Keseluruhan
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="get_all_stock_by_rak()" data-mdb-ripple-color="dark">
        Stock Per Rak
      </button>
    </div>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function get_all_stock() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "material/view_stock",
      success: function(result) {
        $("#content").html(result);
        $('#mastertable').DataTable({
          paging: true,
          "order": [
            [3, "desc"]
          ], //qty
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
        $("#content").addClass("animated fadeInDown");

      }
    });

  }

  function get_all_stock_by_rak() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "material/view_stock_by_rak",
      success: function(result) {
        $("#content").html(result);
        $('#mastertable').DataTable({
          paging: true,
          "order": [
            [0, "asc"]
          ], //FG Code
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
        $("#content").addClass("animated fadeInDown");

      }
    });

  }
  $(document).ready(function() {
    check_role();
  });
</script>