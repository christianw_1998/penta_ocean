<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Laporan PO</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");

  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">PO</li>
        <li class="breadcrumb-item">Purchase Order</li>
        <li class="breadcrumb-item">Lihat Data</li>
        <li class="breadcrumb-item active" aria-current="page">Laporan</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>LAPORAN PURCHASE ORDER</h1>
    <hr style="margin-right:5%;margin-left:5%">
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span> <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br> 
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">

            </div>
            <div class="col-sm-4">
              <span>Kategori </span>
              <select class="form-control f-aleo font-sm" onchange="check_checkbox(this.value)" id="dd_kategori">
                <option value="1">RM PM</option>
                <option value="2">LAIN LAIN</option>
              </select>
            </div>
            <div class="col-sm-1"></div>
          </div>

      </div>
      <div class="col-sm-1"></div>
    </div>
    <br>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h5 class='f-aleo-bold'>Kolom yang ditampilkan</h5>
        <div class="row">
          <div class="col-sm-1"></div>
          <div class="col-sm-10" id="div_kolom">
          </div>
        </div>
        <div class="col-sm-1"></div>

      </div>
      <div class="col-sm-1"></div>
    </div>
    <div class='text-center'>
      <button type="button" class="center btn btn-outline-primary" onclick="get_history()" data-mdb-ripple-color="dark">
        GUNAKAN FILTER
      </button>
    </div>
    <br>
    <div id="content">
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
    get_all_kolom_tampil();
  });

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    } else {
      if ($("#dd_" + value).prop('disabled') == true) {
        $("#dd_" + value).prop('disabled', false);
        check_checkbox($("#dd_" + value).val());
      } else {
        $("#dd_" + value).prop('disabled', true);
      }
    }
  }

  function check_checkbox(id) {
    $(".rekap_class").prop("checked", false);
    if ($(".rekap_class").hasClass("cb_" + id)) {
      $(".cb_" + id).prop("checked", true);
    }

    $(".cb_0").prop("checked", true);
  }

  function get_all_kolom_tampil() {
    $.ajax({
      type: "POST",
      dataType: "json",
      url: site_url + "purchase_order/get_cb_kolom_tampil",
      success: function(result) {
        $("#div_kolom").html(result.kal);
        ctr = result.ctr;
        check_checkbox(1);

      }
    });
  }

  function get_history() {
    var dari_tanggal = null;
    var sampai_tanggal = null;
    var id_kategori = null;
    var is_filtered = false;
    var arr_header_class = [];
    //dari tanggal sampai tanggal filter

    if ($("#cb_tanggal").prop("checked")) {

      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }


      //kategori filter
      //if ($("#cb_kategori").prop("checked")) {
      if ($("#dd_kategori").val() != "") {
        id_kategori = $("#dd_kategori").val();
        is_filtered = true;
      } else {
        toast("Kategori masih belum dipilih!!", Color.DANGER);
        return;
      }
      //}

      //kolom tampil filter
      for (var i = 0; i < ctr; i++) {
        if ($("#cb_" + i).prop("checked")) {
          arr_header_class[i] = 1;
        } else
          arr_header_class[i] = 0;
      }
      var url = "";
      if (id_kategori == "1") { //RM PM
        url = "purchase_order/view_report";
      } else {
        url = "other_po/view_report";

      }
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + url,
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal,
          ik: id_kategori,
          hc: arr_header_class
        },
        success: function(result) {
          $("#content").html(result);
          $('#mastertable').DataTable({
            "drawCallback": function(settings) {
              check_role();
            },
            paging: true,
            "order": [
              [0, "desc"],
              [1, "desc"]
            ],
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");

        }
      });
    }
</script>