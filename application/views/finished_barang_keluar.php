<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Barang Keluar (SJ)</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Barang Keluar</li>
        <li class="breadcrumb-item active" aria-current="page">Dari SJ</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[Surat Jalan]</h3>
    <h1 class='f-aleo-bold text-center'>BARANG KELUAR</h1>

    <div id="content">
      <div class="row" style="margin-right:1%">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
          <hr>
          <h5 class='f-aleo-bold red-text text-center'>Nomor SJ yang dimasukkan hanya digit belakang (max 6 digit) dan yang 0 tidak perlu diinput (Jika Revisi tetap harus ditulis lengkap)</h5>
          <h6 class='f-aleo-bold red-text text-center'>Misal nomor SJ: 82100000154, hanya perlu diisi 154 (nol tidak ditulis)</h6>
          <h6 class='f-aleo-bold red-text text-center'>*(R): Revisi</h6>
          <table class="tabel_header_product table">
            <tr>
              <td class='align-middle text-left font-sm f-aleo' style="width:40%">
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbSO">
                  <label class="form-check-label" for="rbSO">
                    SO <strong class='fw-bold'>(21xx)</strong>
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbSTO">
                  <label class="form-check-label" for="rbSTO">
                    STO <strong class='fw-bold'>(8xx)</strong>
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbSirclo">
                  <label class="form-check-label" for="rbSirclo">
                    SIRCLO <strong class='fw-bold'>(OUTxx)</strong>
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="rbJenis" id="rbRT">
                  <label class="form-check-label" for="rbRT">
                    RETUR <strong class='fw-bold'>(RTxx)</strong>
                  </label>
                </div>
              </td>
              <td class='align-middle text-right font-sm f-aleo'>Eks</td>
              <td class='align-middle text-left font-sm f-aleo'>:</td>
              <td class='align-middle text-right font-sm f-aleo' style='width:80%'>
                <select class="form-control f-aleo" id='txt_ekspedisi' style='width:100%'>
                  <?php
                  for ($i = 0; $i < count(ARR_EKSPEDISI); $i++) {

                  ?>
                    <option value=<?php echo ARR_EKSPEDISI[$i] ?>><?php echo ARR_EKSPEDISI[$i] ?></option>
                  <?php
                  }
                  ?>
                </select>

              </td>


              <td class='align-middle text-right font-sm f-aleo'>SJ</td>
              <td class='align-middle text-left font-sm f-aleo'>:</td>
              <td class='align-middle text-left font-sm f-aleo' style='width:80%'>
                <input onkeyup="check_surat_jalan()" class="f-aleo" type="text" id="txt_nomor_SJ" />
              </td>

              <td class='align-middle text-right font-sm f-aleo'>Tgl</td>
              <td class='align-middle text-left font-sm f-aleo'>:</td>
              <td class='align-middle text-left font-sm f-aleo' style='width:20%'>
                <input class="f-aleo" disabled type="text" id="txt_tanggal" />
              </td>

              <?php
              if (isset($access_revision) && $access_revision) {
              ?>
                <td class='align-middle text-right font-sm f-aleo'>
                  <div class="form-check f-aleo font-sm">
                    <input class="form-check-input" onchange="check_surat_jalan()" type="checkbox" value="" id="cb_revisi_sj">
                    <label class="form-check-label" for="cb_revisi_sj">
                      Revisi
                    </label>
                  </div>
                </td>
              <?php
              }
              ?>
            </tr>
          </table>
          <h4 style="width:100%" id="txt_kode_GI" class="text-center f-aleo green-text"></h4>
          <table class="tabel_detail_product" style="visibility:hidden">
            <tr>
              <td class="text-center font-sm f-aleo">Nomor</td>
              <td class="text-center font-sm f-aleo">Kode Barang</td>
              <td class="text-center font-sm f-aleo">Warna</td>
              <td class="text-center font-sm f-aleo">Kemasan</td>
              <td class="text-center font-sm f-aleo">Tin</td>
              <td width=10% class="text-center font-sm f-aleo">Dus</td>
              <td class="text-center font-sm f-aleo">Sisa Stock (Tin)</td>
              <td width=10% class="text-center font-sm f-aleo">Sisa Stock (Dus)</td>
              <td class="text-center font-sm f-aleo">Barang</td>
            </tr>
          </table>
          <div class='text-center'>
            <button type="button" id="btn_tambah_barang" class="center btn btn-outline-primary" onclick="plus_product()" data-mdb-ripple-color="dark">
              Tambah Barang
            </button>
            <button type="button" id="btn_generate" class="center btn btn-outline-success" onclick="insert_good_issue()" data-mdb-ripple-color="dark">
              Generate
            </button>
          </div>
        </div>
        <div class="col-sm-1"></div>
      </div>
    </div>
    <div class="row" style="margin-top:-2%;margin-right:1%;margin-bottom:5%">
      <div class="col-sm-3"></div>
      <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
      <div class="col-sm-3"></div>
    </div>
  </div>
</body>

</html>
<script>
  class SJ_TYPE {
    static SO = "SO";
    static STO = "STO";
    static RETUR = "RETUR";
    static SIRCLO = "SIRCLO";
  }
  var ctr = 0;
  $(document).ready(function() {
    check_role();
    var today = new Date();

    var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
    $("#txt_tanggal").val(date);
    setInterval(refresh_product, 2000);
  });

  function refresh_product() {
    for (var i = 0; i < ctr; i++) {
      check_product(i);
    }
  }

  function plus_product() {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });


    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "material/plus",
      data: {
        c: ctr,
        t: "<?php echo FG_PLUS_TYPE_OUT; ?>"
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;
        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function check_product(params) {
    var kode_barang = $("#txt_kode_barang_" + params).val();
    var kemasan = $("#txt_kemasan_" + params).val();
    var warna = $("#txt_warna_" + params).val();

    if (kode_barang != "" && kemasan != "" && warna != "") {
      $.ajax({
        type: "POST",
        url: site_url + "material/get",
        data: {
          kb: kode_barang,
          k: kemasan,
          w: warna
        },
        dataType: "JSON",
        success: function(result) {
          if (result.num_rows != 0) {

            $("#txt_barang_desc_" + params).val(result.description);

            if ($("#txt_stock_dus_" + params).length)
              $("#txt_stock_dus_" + params).val(result.stock_dus);

            if ($("#txt_stock_tin_" + params).length) {
              $("#txt_stock_tin_" + params).val(result.stock_tin);
            }

            $("#txt_box_" + params).val(result.box);
            $("#txt_barang_desc_" + params).addClass('green-text');
            $("#txt_barang_desc_" + params).removeClass('red-text');

            var inp_stock_dus = 0;
            var inp_stock_tin = 0;
            if ($("#txt_dus_" + params).val())
              inp_stock_dus = parseInt($("#txt_dus_" + params).val());

            if ($("#txt_tin_" + params).val())
              inp_stock_tin = parseInt($("#txt_tin_" + params).val());

            var box = parseInt($("#txt_box_" + params).val());

            $("#txt_dus_" + params).removeClass("red-text green-text");
            $("#txt_tin_" + params).removeClass("red-text green-text");

            var total_inp_stock = (inp_stock_dus * box) + inp_stock_tin;
            var total_stock = (result.stock_dus * box) + result.stock_tin;
            if ($("#txt_stock_tin_" + params).length) {
              if (total_stock < total_inp_stock) {
                $("#txt_dus_" + params).addClass("red-text");
                $("#txt_tin_" + params).addClass("red-text");
              } else {
                $("#txt_dus_" + params).addClass("green-text");
                $("#txt_tin_" + params).addClass("green-text");
              }
            }
          } else {
            $("#txt_barang_desc_" + params).val("barang belum ditemukan!");
            $("#txt_barang_desc_" + params).removeClass('green-text');
            $("#txt_barang_desc_" + params).addClass('red-text');
          }
        }
      });
    }
  }

  function check_surat_jalan() {
    var nomor_sj = $("#txt_nomor_SJ").val();
    var cb_revisi_sj = $("#cb_revisi_sj");
    if ((cb_revisi_sj.length > 0 && !cb_revisi_sj.prop("checked")) || cb_revisi_sj.length <= 0) {
      $.ajax({
        type: "POST",
        url: site_url + "good_issue/get_view_by_sj",
        data: {
          i: nomor_sj
        },
        dataType: "JSON",
        success: function(result) {
          if (result.num_rows != 0) {
            if (result.sto == SJ_TYPE.SO)
              $("#rbSO").prop("checked", true);
            else if (result.sto == SJ_TYPE.STO)
              $("#rbSTO").prop("checked", true);
            else if (result.sto == SJ_TYPE.SIRCLO)
              $("#rbSirclo").prop("checked", true);
            else if (result.sto == SJ_TYPE.RETUR)
              $("#rbRT").prop("checked", true);

            ctr = result.ctr;
            $("#txt_ekspedisi").val(result.ekspedisi);

            if (!$.fn.DataTable.isDataTable('.tabel_detail_product')) {
              $(".tabel_detail_product").html(result.detail_product);
              $(".tabel_detail_product").css("visibility", "visible");
              $('.tabel_detail_product').dataTable({
                paging: false,
                searching: false,
                info: false,
                "pagingType": "full",
                dom: 'Bfrtip',
                buttons: [{
                  extend: 'excel',
                  title: 'Finished Good Keluar - SJ no ' + result.kode
                }]
              });
            }
            $(".buttons-excel span").text('Export ke Excel');
            $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
            $("#btn_generate").prop("disabled", true);
            $("#btn_tambah_barang").prop("disabled", true);
            $("#txt_kode_GI").html(result.html);
          } else {
            reset_form();
          }
        }
      });
    } else {
      reset_form();
    }

  }

  function reset_form() {
    var kal = '<tr><td class="text-center font-sm f-aleo">Nomor</td><td class="text-center font-sm f-aleo">Kode Barang</td><td class="text-center font-sm f-aleo">Warna</td><td class="text-center font-sm f-aleo">Kemasan</td><td class="text-center font-sm f-aleo">Tin</td><td class="text-center font-sm f-aleo">Dus</td><td class="text-center font-sm f-aleo">Sisa Stock (Tin)</td><td class="text-center font-sm f-aleo">Sisa Stock (Dus)</td></tr>'
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    ctr = 0;
    $("#txt_ekspedisi").val("LCL");
    $("#txt_kode_GI").html("");
    $("#btn_generate").html("Generate");
    $("#btn_generate").prop("disabled", false);
    $("#btn_tambah_barang").prop("disabled", false);
    $(".buttons-excel").remove();
    $("#rbSO").prop("checked", false);
    $("#rbSTO").prop("checked", false);
    $("#rbSirclo").prop("checked", false);
    $("#rbRT").prop("checked", false);

  }

  function insert_good_issue() {
    if ($("#txt_nomor_SJ").val() == "")
      toast("Nomor SJ kosong, silahkan dicek lagi!", Color.DANGER);
    else if (ctr == 0)
      toast("Tidak ada barang yang dipilih, silahkan buat terlebih dahulu!", Color.DANGER);
    else if ($("#txt_nomor_SJ").val().toUpperCase().includes("REVGI") || $("#txt_nomor_SJ").val().toUpperCase().includes("REVGR"))
      toast("Nomor SJ tidak boleh diisi revisi, silahkan arahkan ke nomor sj aslinya!", Color.DANGER);
    else {
      var revisi = "n";
      if ($("#cb_revisi_sj").length > 0) {
        if ($("#cb_revisi_sj").prop("checked"))
          revisi = "y";
      }
      var sto = "";
      if ($("#rbSO").is(":checked"))
        sto = SJ_TYPE.SO;
      else if ($("#rbSTO").is(":checked"))
        sto = SJ_TYPE.STO;
      else if ($("#rbSirclo").is(":checked"))
        sto = SJ_TYPE.SIRCLO;
      else if ($("#rbRT").is(":checked"))
        sto = SJ_TYPE.RETUR;

      var ekspedisi = $("#txt_ekspedisi").val();
      var no_sj = $("#txt_nomor_SJ").val();
      var is_true = true;
      var arr_kode_barang = new Array();
      var arr_kemasan_barang = new Array();
      var arr_dus_barang = new Array();
      var arr_tin_barang = new Array();
      var arr_warna_barang = new Array();
      var ctr_arr = 0;
      for (var i = 0; i < ctr; i++) {
        if ($("#txt_kode_barang_" + i).length > 0) {
          var kode_barang = $("#txt_kode_barang_" + i).val();
          var kemasan = $("#txt_kemasan_" + i).val();
          var dus = 0;
          var tin = 0;
          if ($("#txt_dus_" + i).val() != "") {
            dus = parseInt($("#txt_dus_" + i).val());
          }
          if ($("#txt_tin_" + i).val() != "") {
            tin = parseInt($("#txt_tin_" + i).val());
          }
          var stock_dus = parseInt($("#txt_stock_dus_" + i).val());
          var stock_tin = parseInt($("#txt_stock_tin_" + i).val());
          var warna = $("#txt_warna_" + i).val();
          if (kode_barang != "" && kemasan != "" && (dus != 0 || tin != 0) && warna != "" && !$("#txt_barang_desc_" + i).hasClass("red-text") && (!$("#txt_dus_" + i).hasClass("red-text") || !$("#txt_tin_" + i).hasClass("red-text"))) {
            arr_kode_barang[ctr_arr] = kode_barang;
            arr_kemasan_barang[ctr_arr] = kemasan;
            arr_dus_barang[ctr_arr] = dus;
            arr_tin_barang[ctr_arr] = tin;
            arr_warna_barang[ctr_arr] = warna;
            ctr_arr++;
          } else {
            is_true = false;
            break;
          }
        }
      }
      if (ctr_arr == 0 || sto == "")
        is_true = false;
      if (is_true) {
        var c = confirm("Apakah Anda Yakin?");
        if (c) {
          $.ajax({
            type: "POST",
            url: site_url + "good_issue/insert",
            data: {
              c: ctr_arr,
              r: revisi,
              e: ekspedisi,
              s: sto,
              kb: arr_kode_barang,
              keb: arr_kemasan_barang,
              db: arr_dus_barang,
              tb: arr_tin_barang,
              wb: arr_warna_barang,
              n: no_sj
            },
            success: function(result) {
              if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
                toast(result, Color.SUCCESS);
                //$("#rbSO").prop("checked", true);
                reset_form();
                $("#txt_nomor_SJ").val("");
              } else {
                toast(result, Color.DANGER);
              }
            }
          });
        }
      } else {
        toast("Ada barang yang salah atau data yang kosong, pastikan barang terdaftar dan stock mencukupi!", Color.DANGER);
      }
    }
  }
</script>