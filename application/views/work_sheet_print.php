<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
    <title>[RM PM] Cetak WS</title>
    <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
    ?>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

    <style>
        .page {
            page-break-before: always;
        }

        td {
            font-size: 80%;
        }
    </style>
</head>

<body>
    <?php
    include("navigation.php");
    ?>
    <br>
    <div class="f-aleo animated fadeInDown">
        <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
                <li class="breadcrumb-item">WS</li>
                <li class="breadcrumb-item">Worksheet</li>
                <li class="breadcrumb-item">Lihat Data</li>
                <li class="breadcrumb-item active" aria-current="page">Cetak</li>
            </ol>
        </nav>
        <h1 class='f-aleo-bold text-center'>CETAK WORKSHEET</h1>
        <hr style="margin-left:5%;margin-right:5%">
        <br>
        <div class="row" style="margin-right:1%">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class='row'>
                    <div class='col-sm-2 text-right'></div>
                    <div class='col-sm-2 text-right'>Nomor WS</div>
                    <div class='col-sm-1 text-left '>:</div>
                    <div class='col-sm-3 text-left'><input type="text" id="txt_nomor_ws" /></div>
                    <div class='col-sm-2 text-right'></div>
                </div>

            </div>
            <div class="col-sm-1"></div>
        </div>

        <div class="row" style="margin-right:1%">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 text-center">
                <button type="button" class="center text-center btn btn-outline-success" onclick="get_cetak_by_id()" data-mdb-ripple-color="dark">
                    GENERATE
                </button>
                <button type="button" id="btn_cetak" class="center text-center btn btn-outline-info" onclick="create_pdf()" data-mdb-ripple-color="dark">
                    CETAK PDF
                </button>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <hr style="margin-left:5%;margin-right:5%">
    </div>

    <div class="html-content-semi" style="display:none;font-family:'Times New Roman', Times, serif">
        <div style="margin-left:5%;margin-right:5%;">
            <!-- HEADER -->
            <div class="row" style="margin-bottom:1%;">
                <div class="col-sm-6 text-left">
                    <h3 class="fw-bold">WORKSHEET</h3>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <h3 class="fw-bold">WS SEMI : <span class="txt_no_ws"></span></h3>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h5 class="txt_ctr_print"></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-center align-middle" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-8">
                            <table>
                                <tr>
                                    <td class="text-left">PRODUCT</td>
                                    <td>:</td>
                                    <td class="txt_id_bom text-left" style="padding-left:5%"></td>
                                </tr>
                                <tr>
                                    <td class="text-left align-top">PRODNAME</td>
                                    <td class="align-top">:</td>
                                    <td class="txt_desc_bom text-left align-top" style="width:100%;padding-left:5%">240700001</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-4" class="d-flex justify-content-right">
                            <table>
                                <tr>
                                    <td class="text-left">STATUS</td>
                                    <td>:</td>
                                    <td class="txt_alt_bom text-left" style="padding-left:5%">3,500.000</td>
                                </tr>
                                <tr>
                                    <td>MACHINE ID</td>
                                    <td>:</td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-8">
                            <table>
                                <tr>
                                    <td class="text-left">DATE PRINTED</td>
                                    <td>:</td>
                                    <td style="padding-left:5%"><?php echo date("d.m.Y"); ?></td>
                                </tr>
                                <tr>
                                    <td class="text-left">NO BATCH</td>
                                    <td>:</td>
                                    <td class="txt_batch_number text-left" style="padding-left:5%">240700001</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-4" class="d-flex justify-content-right">
                            <table>
                                <tr>
                                    <td class="text-left">BATCH SIZE</td>
                                    <td>:</td>
                                    <td class="txt_total_qty text-right" style="padding-left:5%">3,500.000</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- JI & FORMULA AWAL -->
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-center" style="border:2px solid black;">
                    <h6 class="fw-bold"><u>JOB INSTRUCTION</u></h6>
                    <p class="text-left txt_job_instruction" style="font-size:110%;white-space: pre-line;text-transform: lowercase;">
                    </p>
                </div>

                <div class="col-sm-6 text-center" style="border:2px solid black;padding-bottom:1%">
                    <h6 class="fw-bold"><u>FORMULA AWAL</u></h6>
                    <table style="border:1px solid black;width:100%;" class="tabel_ws">

                    </table>
                </div>
            </div>
            <!-- ADJUSTMENT & NOMOR DOKUMEN-->
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>ADJUSTMENT</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;" class="fw-bold">KODE RM</th>
                                <th style="border:1px solid black;" class="fw-bold">QUANTITY</th>
                                <th style="border:1px solid black;" class="fw-bold">KODE RM</th>
                                <th style="border:1px solid black;" class="fw-bold">QUANTITY</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 3; $i++) {
                            ?>
                                <tr>
                                    <td style="border:1px solid black;line-height: 50px;" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;padding-right:2%" class="text-right">&nbsp;</td>
                                    <td style="border:1px solid black;padding-right:2%" class="text-right">&nbsp;</td>
                                    <td style="border:1px solid black;padding-right:2%" class="text-right">&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td style="line-height: 50px;border:1px solid black;padding-left:1%;font-size:90%" colspan=2 class="fw-bold text-left">NO.&nbsp;</td>
                                <td style="border:1px solid black;padding-left:1%;font-size:90%" colspan=2 class="fw-bold text-left">NO.&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1.25%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>NOMOR DOKUMEN</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <tr>
                            <td style="border:1px solid black;width:30%" class="fw-bold text-center">DESKRIPSI</td>
                            <td style="border:1px solid black;width:70%" class="fw-bold text-center">NOMOR & TANGGAL</td>
                            <!--<td style="border:1px solid black;width:30%" class="fw-bold text-center">TANGGAL</td> -->
                        </tr>
                        <tr style="line-height: 100px;">
                            <td style="border:1px solid black;width:40%;padding-left:1%" class="text-left fw-bold">WS AWAL</td>
                            <td style="border:1px solid black;width:30%" class="text-left">&nbsp;</td>
                            <!--<td style="border:1px solid black;width:30%" class="text-left">&nbsp;</td>-->
                        </tr>
                        <?php
                        $arr_no_dokumen = ["LATEX", "ULTRACAL - HI BLOW"];
                        for ($i = 0; $i < count($arr_no_dokumen); $i++) {
                        ?>
                            <tr style="line-height: 50px;">
                                <td style="border:1px solid black;width:40%;padding-left:1%" class="text-left fw-bold"><?php echo $arr_no_dokumen[$i]; ?></td>
                                <td style="border:1px solid black;width:30%" class="text-left">&nbsp;</td>
                                <!--<td style="border:1px solid black;width:30%" class="text-left">&nbsp;</td>-->
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <!-- KONFIRMASI & QC -->
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-center" style="padding-bottom:1.25%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>KONFIRMASI</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;" class="fw-bold">TAHAP</th>
                                <th style="border:1px solid black;" class="fw-bold">TANGGAL</th>
                                <th style="border:1px solid black;" class="fw-bold">MULAI - SELESAI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $arr_konfirmasi = ["PERSIAPAN", "PREMIX", "MAKEUP", "COLOUR MATCHER", "QC"];
                            for ($i = 0; $i < count($arr_konfirmasi); $i++) {
                            ?>
                                <tr style="line-height: 50px;">
                                    <td style="border:1px solid black;width:30%;padding-left:1%" class="text-left"><?php echo $arr_konfirmasi[$i]; ?></td>
                                    <td style="border:1px solid black;width:35%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:35%" class="text-center">&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>QUALITY CONTROL</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;font-size:110%" class="fw-bold">NO</th>
                                <th style="border:1px solid black;font-size:110%" class="fw-bold">QC ITEM</th>
                                <th style="border:1px solid black;font-size:110%" class="fw-bold">TM</th>
                                <th style="border:1px solid black;font-size:110%" class="fw-bold">SPECS</th>
                                <th style="border:1px solid black;font-size:110%" class="fw-bold">HASIL</th>
                            </tr>
                        </thead>
                        <tbody class="tabel_qc">

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- TT -->
            <div class="row" style="margin-bottom:0.5%;border:2px solid black;">
                <div class="col-sm-12 text-center">
                    <h6 class="fw-bold"><u>OPERATOR CHECK</u></h6>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;">

                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;width:40%" class="fw-bold">NAMA</th>
                                <th style="border:1px solid black;width:30%" class="fw-bold">NIK</th>
                                <th style="border:1px solid black;width:30%" class="fw-bold">TANDA TANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 2; $i++) {
                            ?>
                                <tr style="line-height: 50px;">
                                    <td style="border:1px solid black;width:40%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:30%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:30%" class="text-center">&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;">
                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;width:40%" class="fw-bold">NAMA</th>
                                <th style="border:1px solid black;width:30%" class="fw-bold">NIK</th>
                                <th style="border:1px solid black;width:30%" class="fw-bold">TANDA TANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 2; $i++) {
                            ?>
                                <tr style="line-height: 50px;">
                                    <td style="border:1px solid black;width:40%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:30%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:30%" class="text-center">&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <div class="html-content-fg" style="display:none;font-family:'Times New Roman', Times, serif">

        <div style="margin-left:5%;margin-right:5%;">
            <!-- HEADER -->
            <div class="row">
                <div class="col-sm-6 text-left">
                    <h4 class="fw-bold">WORKSHEET</h4>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <h3 class="fw-bold">WS FG : <span class="txt_no_ws"></span></h3>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h5 class="txt_ctr_print"></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-left" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-8">
                            <table>
                                <tr>
                                    <td class="text-left">PRODUCT</td>
                                    <td>:</td>
                                    <td class="txt_id_bom text-left" style="padding-left:5%"></td>
                                </tr>
                                <tr>
                                    <td class="text-left align-top">PRODNAME</td>
                                    <td class="align-top">:</td>
                                    <td class="txt_name_bom text-left align-top" style="width:100%;padding-left:5%">240700001</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-4" class="d-flex justify-content-right">
                            <table>
                                <tr>
                                    <td class="text-left">STATUS</td>
                                    <td>:</td>
                                    <td class="txt_alt_bom text-left" style="padding-left:5%">3,500.000</td>
                                </tr>
                                <tr>
                                    <td>MACHINE ID</td>
                                    <td>:</td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-5 text-left">
                            <table>
                                <tr>
                                    <td class="text-left">WS SEMI</td>
                                    <td>:</td>
                                    <td style="padding-left:5%" class="txt_no_ws_semi"></td>
                                </tr>
                                <tr>
                                    <td class="text-left">DATE PRINTED</td>
                                    <td>:</td>
                                    <td style="padding-left:5%"><?php echo date("d.m.Y"); ?></td>
                                </tr>
                                <tr>
                                    <td class="text-left">NO BATCH</td>
                                    <td>:</td>
                                    <td style="padding-left:5%" class="txt_batch_number text-left">240700001</td>
                                </tr>

                            </table>
                        </div>
                        <div class="col-sm-7 text-left">
                            <table style="width:100%">
                                <tr>
                                    <td class="text-left">BATCH SIZE</td>
                                    <td>:</td>
                                    <td style="padding-left:5%" class="text-left txt_total_qty_tin">700</td>
                                </tr>
                                <tr>
                                    <td class="text-left">BATCH KG</td>
                                    <td>:</td>
                                    <td style="padding-left:5%" class="text-left txt_total_qty">3,500.000</td>
                                </tr>
                                <tr>
                                    <td class="text-left">PRODUCTION CODE</td>
                                    <td>:</td>
                                    <td style="padding-left:5%" class="txt_desc_bom text-left"> 5200.00000 - 3 LTR</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FORMULA AWAL & CHECKING PART-->
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-center" style="border:2px solid black;padding-bottom:1%">
                    <h6 class="fw-bold"><u>FORMULA AWAL</u></h6>
                    <table style="border:1px solid black;width:100%" class="tabel_ws">

                    </table>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>CHECKING PART</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <?php
                        $arr_checking_part = ["TOTAL OF RM (KG)", "NET PRODUCTION (TIN)", "NET PRODUCTION (DUS)", "KEMASAN"];
                        for ($i = 0; $i < count($arr_checking_part); $i++) {
                        ?>
                            <tr style="line-height: 50px;">
                                <td style="border:1px solid black;width:40%" class="text-left fw-bold"><?php echo $arr_checking_part[$i]; ?></td>
                                <td style="border:1px solid black;width:60%" class="text-left"></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <!-- ADJUSTMENT & NOMOR DOKUMEN-->
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-center" style="padding-bottom:1%;border:2px solid black;">
                    <h6 class="fw-bold"><u>ADJUSTMENT</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;" class="fw-bold">KODE PM</th>
                                <th style="border:1px solid black;" class="fw-bold">QUANTITY</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 6; $i++) {
                            ?>
                                <tr style="line-height: 50px;">
                                    <td style="border:1px solid black;" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;padding-right:2%" class="text-right">&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>NOMOR DOKUMEN</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <tr>
                            <td style="border:1px solid black;width:40%" class="fw-bold text-center">DESKRIPSI</td>
                            <td style="border:1px solid black;width:30%" class="fw-bold text-center">NOMOR</td>
                            <td style="border:1px solid black;width:30%" class="fw-bold text-center">TANGGAL</td>
                        </tr>
                        <?php
                        $arr_no_dokumen = ["WS AWAL", "ADJUSTMENT", "ADJUSTMENT", "GR", "&nbsp;", "&nbsp;"];
                        for ($i = 0; $i < count($arr_no_dokumen); $i++) {
                        ?>
                            <tr style="line-height:50px;">
                                <td style="padding-left:1%;border:1px solid black;width:40%" class="text-left fw-bold"><?php echo $arr_no_dokumen[$i]; ?></td>
                                <td style="border:1px solid black;width:30%" class="text-left">&nbsp;</td>
                                <td style="border:1px solid black;width:30%" class="text-left">&nbsp;</td>
                            </tr>
                        <?php
                        };
                        ?>
                    </table>
                </div>
            </div>
            <!-- KONFIRMASI & QC -->
            <div class="row" style="margin-bottom:0.5%;">
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>KONFIRMASI</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;" class="fw-bold">DESKRIPSI</th>
                                <th style="border:1px solid black;" class="fw-bold">TANGGAL</th>
                                <th style="border:1px solid black;" class="fw-bold">MULAI - SELESAI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 1; $i++) {
                            ?>
                                <tr style="line-height: 50px;">
                                    <td style="border:1px solid black;width:30%;padding-left:1%" class="text-left">FILLING</td>
                                    <td style="border:1px solid black;width:35%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:35%" class="text-center">&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6 text-center" style="padding-bottom:1%;height:fit-content;border:2px solid black;">
                    <h6 class="fw-bold"><u>OPERATOR CHECK</u></h6>
                    <table style="border:1px solid black;width:100%">
                        <thead>
                            <tr>
                                <th style="border:1px solid black;width:40%" class="fw-bold">NAMA</th>
                                <th style="border:1px solid black;width:30%" class="fw-bold">NIK</th>
                                <th style="border:1px solid black;width:30%" class="fw-bold">TANDA TANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 3; $i++) {
                            ?>
                                <tr style="line-height: 50px;">
                                    <td style="border:1px solid black;width:40%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:30%" class="text-center">&nbsp;</td>
                                    <td style="border:1px solid black;width:30%" class="text-center">&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<script>
    var tipe = "";

    $(document).ready(function() {
        check_role();
    });

    function get_cetak_by_id() {
        var nomor_ws = $("#txt_nomor_ws").val();
        $.ajax({
            type: "POST",
            url: site_url + "work_sheet/get_view_print_by_no",
            data: {
                n: nomor_ws
            },
            dataType: "JSON",
            success: function(result) {
                if (result.num_rows != 0) {
                    $(".html-content-fg").css("display", "none");
                    $(".html-content-semi").css("display", "none");
                    tipe = result.tipe;
                    //WS
                    $(".txt_no_ws").html(result['<?php echo Worksheet::$ID; ?>']);
                    $(".txt_ctr_print").html(result['<?php echo Worksheet::$CTR_PRINT; ?>']);
                    $(".txt_batch_number").html(result['<?php echo Worksheet::$BATCH_NUMBER; ?>']);
                    $(".txt_total_qty").html(result['<?php echo Worksheet_Awal::$QTY; ?>']);

                    //BOM
                    $(".txt_id_bom").html(result['<?php echo BOM::$ID; ?>']);
                    $(".txt_desc_bom").html(result['<?php echo BOM::$DESCRIPTION; ?>']);
                    $(".txt_name_bom").html(result['prodname']);
                    if (result.tipe == '<?php echo Worksheet::$S_SEMI; ?>') {
                        $(".txt_job_instruction").html(result['<?php echo BOM::$JOB_INSTRUCTION; ?>']);
                        $(".tabel_qc").html(result['<?php echo BOM::$QC; ?>']);
                    }

                    //BOM Alt
                    $(".txt_alt_bom").html(result['<?php echo BOM_Alt::$ALT_NO; ?>']);

                    //WS Awal
                    $(".tabel_ws").html(result.detail_product);

                    if (result.tipe == '<?php echo Worksheet::$S_FG; ?>') {
                        $(".txt_no_ws_semi").html(result['<?php echo Worksheet::$S_SEMI; ?>']);
                        $(".txt_total_qty_tin").html(result['batch_size']);

                        $(".html-content-fg").css("display", "block");
                    } else {
                        $(".html-content-semi").css("display", "block");
                    }
                    toast("Nomor WS ditemukan", Color.SUCCESS);
                } else {
                    toast("Nomor WS tidak ditemukan", Color.DANGER);
                    $(".html-content-fg").css("display", "none");
                    $(".html-content-semi").css("display", "none");

                }
            }
        });
    }

    function create_pdf() {
        var nomor_ws = $("#txt_nomor_ws").val();
        var html = $(".html-content-fg");;
        if (tipe == '<?php echo Worksheet::$S_FG; ?>') {
            html = $(".html-content-fg");
        } else {
            html = $(".html-content-semi");
        }
        var HTML_Width = html.width();
        var HTML_Height = html.height();
        var top_left_margin = 0;
        var PDF_Width = HTML_Width + (top_left_margin * 3);
        var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 3);
        var canvas_image_width = HTML_Width;
        var canvas_image_height = HTML_Height;

        var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
        html2canvas(html[0]).then(function(canvas) {
            var imgData = canvas.toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
            pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
            for (var i = 1; i <= totalPDFPages; i++) {
                pdf.addPage(PDF_Width, PDF_Height);
                pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
            }
            pdf.save(nomor_ws + ".pdf");

        });
        $.ajax({
            type: "POST",
            url: site_url + "work_sheet/print_plus_count",
            data: {
                n: nomor_ws
            },
            success: function(result) {}
        });
    }
</script>