<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Barang Masuk Lain Lain</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">G. Lain Lain</li>
        <li class="breadcrumb-item active" aria-current="page">Barang Masuk</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>BARANG MASUK</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 text-right'>No. Purchase Order</div>
          <div class='col-sm-1 text-left '>:</div>
          <div class='col-sm-3 text-left'><input style='width:100%' onkeyup="get_po_by_no()" type="text" id="txt_nomor_po" /></div>
          <div class='col-sm-2 text-right'></div>
        </div>
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 text-right'>Nomor SJ</div>
          <div class='col-sm-1 text-left '>:</div>
          <div class='col-sm-3 text-left'><input style='width:100%' disabled type="text" id="txt_nomor_sj" /></div>
          <div class='col-sm-2 text-right'></div>
        </div>
        <div class='row'>
          <div class='col-sm-2 text-right'></div>
          <div class='col-sm-2 text-right'>Tanggal Terima</div>
          <div class='col-sm-1 text-left '>:</div>
          <div class='col-sm-3'><input style='width:100%' disabled type="date" id="txt_tanggal_terima" /></div>
          <div class='col-sm-2 text-right'></div>
        </div>
        <br>
        <hr style="margin-left:5%;margin-right:5%">
        <h4 style="width:100%" id="txt_title_po" class="text-center green-text"></h4>
        <br>
        <table class="tabel_detail_product" style="visibility:hidden">

        </table>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <button type="button" id="btn_generate" disabled class="center text-center btn btn-outline-success" onclick="gen_po()" data-mdb-ripple-color="dark">
          GENERATE
        </button>
      </div>
      <div class="col-sm-1"></div>
    </div>
  </div>
</body>

</html>
<script>
  var ctr = 0;
  $(document).ready(function() {
    check_role();
  });

  function check_qty(idx) {
    $("#txt_qty_" + idx).removeClass("red-text green-text");
    $("#txt_qty_terima_" + idx).removeClass("red-text green-text");

    var qty = parseFloat($("#txt_qty_" + idx).val());
    var qty_terima = parseFloat($("#txt_qty_terima_" + idx).val());

    if (qty - qty_terima >= 0 && qty_terima > 0)
      $("#txt_qty_terima_" + idx).addClass("green-text");
    else
      $("#txt_qty_terima_" + idx).addClass("red-text");

  }

  function gen_po() {
    var arr_id = new Array();
    var arr_qty_sj = new Array();
    var arr_qty = new Array();
    var arr_no_urut = new Array();
    var is_true = true;
    var nomor_po = $("#txt_nomor_po").val();
    var tanggal = $("#txt_tanggal_terima").val();
    var nomor_sj = $("#txt_nomor_sj").val();
    if (tanggal == "") {
      toast("Tanggal tidak boleh kosong!", Color.DANGER);
      return;
    }
    if (nomor_sj == "") {
      toast("Nomor SJ tidak boleh kosong!", Color.DANGER);
      return;
    }
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_qty_terima_" + i).hasClass("red-text") || $("#txt_qty_terima_" + i).val() < 0){
        is_true = false;
        break;
      }
      arr_id[i] = $("#txt_id_material_" + i).val();
      if ($("#txt_qty_terima_" + i).val() == "")
        arr_qty[i] = 0;
      else
        arr_qty[i] = parseFloat($("#txt_qty_terima_" + i).val());
      arr_no_urut[i] = parseInt($("#txt_no_urut_" + i).val());
    }
    if (is_true) {
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "other_po/insert_gr",
          data: {
            ai: arr_id,
            aq: arr_qty,
            an: arr_no_urut,
            np: nomor_po,
            ns: nomor_sj,
            t: tanggal,
            c: ctr
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
              $("#txt_nomor_po").val("");

            } else
              toast(result, Color.DANGER);

          }
        });
      }
    } else
      toast("Ada material yang qty terima salah, silahkan cek lagi", Color.DANGER);
  }

  function get_po_by_no() {
    var nomor_po = $("#txt_nomor_po").val();
    $.ajax({
      type: "POST",
      url: site_url + "other_po/get_view_by_no",
      data: {
        n: nomor_po
      },
      dataType: "JSON",
      success: function(result) {
        if (result.num_rows != 0) {
          reset_form();
          ctr = result.ctr;
          $("#txt_title_po").html(result['<?php echo Other_Purchase_Order_Awal::$NOMOR_PO; ?>']);
          $("#txt_tanggal_terima").prop("disabled", false);
          $("#txt_nomor_sj").prop("disabled", false);
          $("#btn_generate").prop("disabled", false);
          $(".tabel_detail_product").html(result.detail_product);
          $(".tabel_detail_product").css("visibility", "visible");
          if (!$.fn.DataTable.isDataTable('.tabel_detail_product')) {
            $('.tabel_detail_product').dataTable({
              paging: false,
              searching: false,
              info: false,
              "pagingType": "full",
              dom: 'Bfrtip',
              buttons: [{
                extend: 'excel',
                title: 'Lain Lain Masuk - PO no ' + nomor_po
              }]
            });
            $(".buttons-excel span").text('Export ke Excel');
            $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
            $(".buttons-excel").remove();
          }
        } else {
          reset_form();
        }
      }
    });
  }

  function reset_form() {
    var kal = '<tr><td class="text-center font-sm f-aleo">Nomor</td><td class="text-center font-sm f-aleo">Material</td><td class="text-center font-sm f-aleo">Qty PO</td><td class="text-center font-sm f-aleo">Qty Terima</td></tr>'
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    $(".tabel_detail_product").html(kal);
    $(".tabel_detail_product").css("visibility", "hidden");
    $("#txt_title_po").html("");
    $("#txt_tanggal_terima").prop("disabled", true);
    $("#txt_tanggal_terima").val("");
    $("#txt_nomor_sj").val("");
    $("#txt_nomor_sj").prop("disabled", true);
    $("#btn_generate").prop("disabled", true);
    $(".buttons-excel").remove();
  }
</script>