<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Standar Jam Kerja</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInUp">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Master Standar Jam Kerja</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="btn btn-outline-primary" onclick="insert_f_s_jam_kerja()" data-mdb-ripple-color="dark">
        Input Standar Jam Kerja
      </button>
      <button type="button" class="btn btn-outline-primary role-kepala" onclick="get_master('standar_jam_kerja')" data-mdb-ripple-color="dark">
        Tabel Standar Jam Kerja
      </button>
    </div>
    <div id="content">
    </div>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-3"></div>
      <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
      <div class="col-sm-3"></div>
    </div>
  </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function insert_s_jam_kerja() {
    var bulan = $("#dd_bulan").val();
    var tahun = $("#dd_tahun").val();
    var jam_kerja = $("#txt_jam_kerja").val();
    if (!bulan || !tahun || !jam_kerja) {
      $(".txt_desc_upload").html("Ada data yang kosong, silahkan dicek lagi!");
      $(".txt_desc_upload").addClass("text-danger");
      setTimeout(refresh_page, 1500);
    } else {
      $.ajax({
        type: "POST",
        data: {
          b: bulan,
          t: tahun,
          j: jam_kerja
        },
        url: site_url + "insert/s_jam_kerja",
        success: function(result) {
          $(".txt_desc_upload").html(result);
          $(".txt_desc_upload").addClass("text-success");
          $("#txt_jam_kerja").val("");
        },
        complete: function(data) {
          setTimeout(refresh_page, 1500);
        }
      });
      check_role();
    }

  }

  function refresh_page() {
    $(".txt_desc_upload").html("");
  }

  function insert_f_s_jam_kerja() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "insert_f/s_jam_kerja",
      success: function(result) {
        $("#content").html(result);
        $("#content").addClass("animated fadeInDown");
      }
    });
    check_role();
  }

  function delete_s_jam_kerja(id) {
    $.ajax({
      type: "POST",
      data: {
        id: id
      },
      url: site_url + "delete/s_jam_kerja",
      success: function(result) {
        get_master('standar_jam_kerja');
      }
    });
    check_role();
  }
  $(document).ready(function() {
    check_role();
    $(".buttons-excel").remove();

  });
</script>