<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Rekap Surat Jalan</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" class="f-aleo" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item active" aria-current="page">Rekap Surat Jalan</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>REKAP SURAT JALAN</h1>
    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        <h3 class='f-aleo-bold'>Filter</h1>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="tanggal" id="cb_tanggal">
            </div>
            <div class="col-sm-5">
              <span onclick="toggle_checkbox('tanggal')">Dari Tanggal </span><br>
              <input class="black-text" disabled type="date" id="txt_tanggal_dari" />
            </div>
            <div class="col-sm-5">
              Sampai Tanggal <br>
              <input class="black-text" disabled type="date" id="txt_tanggal_sampai" />
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="kode_barang" id="cb_kode_barang">
            </div>
            <div class="col-sm-4">
              <span onclick="toggle_checkbox('kode_barang')">Kode </span>
              <select onchange="generate_other_combo_box('kode_barang')" disabled class="form-control f-aleo font-sm" id="txt_kode_barang">

              </select>
            </div>
            <div class="col-sm-3">
              <span>Kemasan </span>
              <select onchange="generate_other_combo_box('kemasan')" disabled class="form-control f-aleo font-sm" id="txt_kemasan">

              </select>
            </div>
            <div class="col-sm-3">
              <span>Warna </span>
              <select disabled class="form-control f-aleo font-sm" id="txt_warna">

              </select>
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="nomor_sj" id="cb_nomor_sj">
            </div>
            <div class="col-sm-4">
              <span onclick="toggle_checkbox('nomor_sj')">Nomor SJ</span>
              <input type='text' disabled class="form-control f-aleo font-sm" id="txt_nomor_sj"> </input>
            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo-bold">
            <div class="col-sm-1 text-right">
              <input type="checkbox" onchange="toggle(value)" value="jenis" id="cb_jenis_sj">
            </div>
            <div class="col-sm-10">Tipe Surat Jalan</div>
            <div class="col-sm-1"></div>
          </div>
          <div class="row f-aleo">
            <div class="col-sm-1 text-right">

            </div>
            <div class="col-sm-4 text-left" style="padding-left:3%">
              <input class="form-check-input txt_jenis" type="radio" name="rbJenis" id="rbSO" checked disabled>
              <label class="form-check-label" for="rbSO">
                Stock Out <strong class='fw-bold'>(SO)</strong>
              </label><br>
              <input class="form-check-input txt_jenis" type="radio" name="rbJenis" id="rbSTO" disabled>
              <label class="form-check-label" for="rbSTO">
                Stock Transport Order <strong class='fw-bold'>(STO)</strong>
              </label><br>
              <input class="form-check-input txt_jenis" type="radio" name="rbJenis" id="rbSirclo" disabled>
              <label class="form-check-label" for="rbSirclo">
                Sirclo <strong class='fw-bold'>(OUT)</strong>
              </label><br>
              <input class="form-check-input txt_jenis" type="radio" name="rbJenis" id="rbRetur" disabled>
              <label class="form-check-label" for="rbRetur">
                Retur <strong class='fw-bold'>(RT)</strong>
              </label>
            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="form-check">

          </div>
          <div class="form-check">

          </div>
          <div class="form-check">

          </div>
          </td>
          <br>
          <div class='text-center'>
            <button type="button" class="f-aleo center btn btn-outline-primary" onclick="gen_rekap_sj()" data-mdb-ripple-color="dark">
              GUNAKAN FILTER
            </button>
          </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';

  function toggle_checkbox(value) {
    if ($("#cb_" + value).prop('checked'))
      $("#cb_" + value).prop('checked', false);
    else
      $("#cb_" + value).prop('checked', true);
    toggle(value);
  }

  function toggle(value) {
    if (equal(value, "tanggal")) {
      if ($("#txt_tanggal_dari").prop('disabled') == true) {
        $("#txt_tanggal_dari").prop('disabled', false);
        $("#txt_tanggal_sampai").prop('disabled', false);
      } else {
        $("#txt_tanggal_dari").prop('disabled', true);
        $("#txt_tanggal_sampai").prop('disabled', true);
      }
    } else if (equal(value, "kode_barang")) {
      $("#txt_kode_barang").val('');
      $("#txt_kemasan").val('');
      $("#txt_warna").val('');

      if ($("#txt_kode_barang").prop('disabled') == true)
        $("#txt_kode_barang").prop('disabled', false);
      else
        $("#txt_kode_barang").prop('disabled', true);
      $("#txt_kemasan").prop('disabled', true);
      $("#txt_warna").prop('disabled', true);
    } else if (equal(value, "jenis")) {
      if ($(".txt_" + value).prop('disabled') == true) {
        $(".txt_" + value).prop('disabled', false);

      } else {
        $(".txt_" + value).prop('disabled', true);
      }
    } else {
      if ($("#txt_" + value).prop('disabled') == true) {
        $("#txt_" + value).prop('disabled', false);

      } else {
        $("#txt_" + value).prop('disabled', true);

      }
    }
  }

  function generate_other_combo_box(tipe) {
    $("#txt_warna").prop("disabled", true);
    if (!equal($("#txt_" + tipe).val(), "") && !$("#txt_" + tipe).prop('disabled')) {
      if (equal(tipe, "kode_barang")) {
        $("#txt_kemasan").prop("disabled", false);
        $("#txt_kemasan").val("");
        $("#txt_warna").val("");
        get_all_material('KEMASAN');
      } else {

        $("#txt_warna").prop("disabled", false);
        get_all_material('WARNA');
      }
    } else {
      if (equal(tipe, "kode_barang")) {
        $("#txt_kemasan").prop("disabled", true);
      }

    }
  }

  function get_all_material(tipe) {
    var kb = "";
    var k = "";

    if (!$("#txt_kemasan").prop("disabled") && !equal($("#txt_kemasan").val(), "") && !equal(tipe, "kode_barang")) {
      k = $("#txt_kemasan").val();
    }
    if (!$("#txt_kode_barang").prop("disabled")) {
      kb = $("#txt_kode_barang").val();
    }
    $.ajax({
      type: "POST",
      url: site_url + "material/get_filter",
      dataType: "json",
      data: {
        t: tipe,
        k: k,
        kb: kb
      },
      success: function(result) {
        var kal = "";
        kal += "<option value=''>SEMUA</option>";
        for (var i = 0; i < result.length; i++) {
          kal += "<option value='" + result[i].value + "'>" + result[i].value + "</option>";
        }
        $("#txt_" + tipe.toLowerCase()).html(kal);

      }
    });
  }

  function gen_rekap_sj() {
    var dari_tanggal = null;
    var sampai_tanggal = null;
    var is_filtered = false;
    var nomor_sj = null;
    var kode_barang = null;
    var kemasan = null;
    var warna = null;
    var jenis_sj = null;

    //dari tanggal sampai tanggal filter
    if ($("#cb_tanggal").prop("checked")) {
      if ($("#txt_tanggal_dari").val() != "" && $("#txt_tanggal_sampai").val() != "") {
        if ($("#txt_tanggal_dari").val() > $("#txt_tanggal_sampai").val()) {
          toast("Tanggal sampai harus lebih besar dari tanggal dari", Color.DANGER);
          return;
        } else {
          dari_tanggal = $("#txt_tanggal_dari").val();
          sampai_tanggal = $("#txt_tanggal_sampai").val();
          is_filtered = true;
        }
      } else {
        toast("Ada tanggal yang masih kosong", Color.DANGER);
        return;
      }
    }

    //material filter
    if ($("#cb_kode_barang").prop("checked")) {
      if ($("#txt_kode_barang").val() != "") {
        kode_barang = $("#txt_kode_barang").val();
        is_filtered = true;
      } else {
        toast("Barang masih belum dipilih", Color.DANGER);
        return;
      }
      if ($("#txt_kemasan").val() != "") {
        kemasan = $("#txt_kemasan").val();
        is_filtered = true;
      }
      if ($("#txt_warna").val() != "") {
        warna = $("#txt_warna").val();
        is_filtered = true;
      }
    }

    //nomor_sj filter
    if ($("#cb_nomor_sj").prop("checked")) {
      if ($("#txt_nomor_sj").val() != "") {
        nomor_sj = $("#txt_nomor_sj").val();
        is_filtered = true;
      } else {
        toast("Nomor SJ masih belum diisi", Color.DANGER);
        return;
      }
    }
    //jenis_sj filter
    if ($("#cb_jenis_sj").prop("checked")) {
      is_filtered = true;
      if ($("#rbSO").is(":checked"))
        jenis_sj = "SO";
      else if ($("#rbSTO").is(":checked"))
        jenis_sj = "STO";
      else if ($("#rbSirclo").is(":checked"))
        jenis_sj = "SIRCLO";
      else if ($("#rbRetur").is(":checked"))
        jenis_sj = "RETUR";
    }
    if (is_filtered) {
      $("#content").removeClass("animated fadeInDown");
      $.ajax({
        type: "POST",
        url: site_url + "material/view_rekap_sj",
        data: {
          dt: dari_tanggal,
          st: sampai_tanggal,
          n: nomor_sj,
          kb: kode_barang,
          k: kemasan,
          w: warna,
          j: jenis_sj
        },
        success: function(result) {
          $("#content").html(result);
          $('#mastertable').DataTable({
            "order": [
              [0, "desc"]
            ], //Tanggal Kirim
            paging: true,
            "pagingType": "full",
            dom: 'Bfrtip',
            buttons: [
              'excel',
            ]
          });
          //$(".buttons-excel").remove();
          $(".buttons-excel span").text('Export ke Excel');
          $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
          $("#content").addClass("animated fadeInDown");
        }
      });
    } else
      toast("Setidaknya 1 filter harus digunakan", Color.DANGER);
  }
  $(document).ready(function() {
    check_role();
    get_all_material('KODE_BARANG');
    get_all_material('KEMASAN');
    get_all_material('WARNA');

    <?php
    if (isset($today_info)) {
    ?>
      $("#cb_tanggal").prop("checked", true);
      toggle("tanggal");
      $("#txt_tanggal_dari").setNow();
      $("#txt_tanggal_sampai").setNow();
    <?php } ?>
  });
</script>