<!DOCTYPE html>
<html lang="en"  class="role-kepala full-height">
<head>
  <title>Slip Gaji</title>
  <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
  ?>
</head>
<body class='f-aleo role-kepala'>
	<?php
    include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home");?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Generate Slip Gaji</li>
      </ol>
    </nav>
    <div id="content" style="margin-bottom:5%">
    </div>
  </div>
</body>
</html>

<script language="javascript">
  var site_url='<?php echo site_url();?>';
  
  function rincian_slip_gaji(){
    $.ajax({
			type : "POST",
			url : site_url+"rincian_f/slip_gaji",
			success:function(result){
				$("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });
    $("#content").removeClass("animated fadeInDown");
  }
  function gen_slip_gaji(){
    $(".div_laporan").remove();
    var temp=	$("#content").html();
    var tahun=$("#dd_tahun").val();
    var nik=$("#dd_tipe").val();
    var bulan=$("#dd_bulan").val();
    $.ajax({
			type : "POST",
			data:{t:"slip_gaji",ta:tahun,td:nik,bu:bulan},
			url : site_url+"gen_rincian",
			success:function(result){
				$("#content").html(temp+result);
        $(".buttons-excel").remove();
      }
			
    });
    check_role();
  }
  function gen_slip_gaji_all_karyawan(){
    $(".div_laporan").remove();
    var temp=	$("#content").html();
    var tahun=$("#dd_tahun").val();
    var bulan=$("#dd_bulan").val();
    $.ajax({
			type : "POST",
			data:{t:"slip_gaji",ta:tahun,bu:bulan},
			url : site_url+"gen_slip_gaji_all_karyawan",
			success:function(result){
				$("#content").html(temp+result);
        Export2Word("div_laporan","Slip_Gaji_All");
      }
			
    });
    check_role();
  }
  $(document).ready(function(){
    check_role();
    rincian_slip_gaji();
  });
</script>
<script src="<?php echo base_url("asset/js/FileSaver.js");?>"></script>
<script src="<?php echo base_url("asset/js/jquery.wordexport.js");?>">
</script>


