<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
    <title>[RM PM] Cetak Purchase Order</title>
    <?php
    include("library.php");
    include("redirect_login.php");
    include("role_management.php");
    ?>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

    <style>
        table td,
        table th {
            font-size: 85%;
        }
    </style>
</head>

<body>
    <?php
    include("navigation.php");
    ?>
    <br>
    <?php echo form_open('purchase_order/email_send', ['method' => 'post', 'enctype' => 'multipart/form-data']) ?>
    <div class="f-aleo animated fadeInDown">
        <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
                <li class="breadcrumb-item">PO</li>
                <li class="breadcrumb-item">Purchase Order</li>
                <li class="breadcrumb-item">Lihat Data</li>
                <li class="breadcrumb-item active" aria-current="page">Cetak</li>
            </ol>
        </nav>
        <h1 class='f-aleo-bold text-center'>CETAK PURCHASE ORDER</h1>
        <hr style="margin-left:5%;margin-right:5%">
        <br>
        <div class="row" style="margin-right:1%">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class='row'>
                    <div class='col-sm-2 text-right'></div>
                    <div class='col-sm-2 text-right'>Nomor PO</div>
                    <div class='col-sm-1 text-left '>:</div>
                    <div class='col-sm-3 text-left'><input type="text" id="txt_nomor_po" /></div>
                    <div class='col-sm-2 text-right'></div>
                </div>

            </div>
            <div class="col-sm-1"></div>
        </div>

        <div class="row" style="margin-right:1%">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 text-center">
                <button type="button" class="center text-center btn btn-outline-success" onclick="get_cetak_by_id()" data-mdb-ripple-color="dark">
                    GENERATE
                </button>
                <button type="button" disabled id="btn_cetak" class="center text-center btn btn-outline-info" onclick="CreatePDFfromHTML()" data-mdb-ripple-color="dark">
                    CETAK PDF
                </button>
                <button type="submit" disabled id="btn_email" class="center text-center btn btn-outline-info" data-mdb-ripple-color="dark">
                    KIRIM EMAIL
                </button>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <hr style="margin-left:5%;margin-right:5%">
    </div>

    <div class="html-content" style="display:none;font-size:150%;font-family:'Times New Roman', Times, serif">
        <br>
        <br>
        <!-- Header -->
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5 fw-bold">PT. Penta Ocean<br>Jl. Margomulyo Indah A, Kav 2-7, No. 17<br>Manukan Wetan Tandes, Kota Surabaya</div>
            <div class="col-sm-5 text-right">
                <h2 class="fw-bold ">PURCHASE ORDER</h2>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <br>
        <!-- Telp Fax NPWP -->
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5 fw-bold">
                <table>
                    <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td>(031) 7484680</td>
                    </tr>
                    <tr>
                        <td>Fax</td>
                        <td>:</td>
                        <td>(031) 7495638</td>
                    </tr>
                    <tr>
                        <td>NPWP</td>
                        <td>:</td>
                        <td>02.825.201.3-631.000 / 04 April 2012</td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-5">

            </div>
            <div class="col-sm-1"></div>
        </div>
        <br>
        <!-- Alamat Vendor dan PO -->
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
                <table id="tabel_vendor" style="margin-left:-3%;width:100%;border:3px solid black;">
                    <tr>
                        <td style="padding-top:2%">TO</td>
                        <td style="padding-top:2%">:</td>
                        <td style="padding-top:2%" id="txt_vendor">PT TANSRI GANI (.......)</td>
                    </tr>
                    <tr>
                        <td class="align-top">Address</td>
                        <td class="align-top">:</td>
                        <td><?php echo ucwords(strtolower("JALAN RAYA SERANG KM. 12, GRIYA IDOLA INDUSTRIAL PARK BLOK I NO. 01, KELURAHAN BITUNG JAYA, KECAMATAN CIKUPA, KABUPATEN TANGERANG 15710.")); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td>(031) 7484680</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom:2%">Fax</td>
                        <td style="padding-bottom:2%">:</td>
                        <td style="padding-bottom:2%">(031) 7495638</td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-5">
                <table id="tabel_vendor" style="margin-left:3%;width:100%;border:3px solid black;">
                    <tr>
                        <td style="padding-top:2%">Date</td>
                        <td style="padding-top:2%">:</td>
                        <td style="padding-top:2%" id="txt_tanggal">14.06.2024</td>
                    </tr>
                    <tr>
                        <td>No. PO</td>
                        <td>:</td>
                        <td id="txt_no_po"></td>
                    </tr>
                    <tr>
                        <td>No. PR</td>
                        <td>:</td>
                        <td id="txt_no_pr"></td>
                    </tr>

                </table>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10" style="padding-bottom:5%;border:1px solid black">
                <!-- Detail PO -->
                <div class="row">
                    <div class="col-sm-12">
                        <table id="tabel_po" style="margin-top:1%;width:100%">

                        </table>
                    </div>
                </div>
                <br>
                <!-- Total & Sebagainya -->
                <div class="row">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-4">
                        <table style="width:100%">
                            <tr>
                                <td class="fw-bold text-left" style="padding-bottom:5%">Sub Total</td>
                                <td style="padding-bottom:5%"></td>
                                <td class="fw-bold text-right" style="padding-bottom:5%" id="txt_subtotal">2000000</td>
                            </tr>

                            <tr class="padding-top:5%">
                                <td style="padding-bottom:5%" class="fw-bold text-left">Potongan</td>
                                <td style="padding-bottom:5%"></td>
                                <td style="padding-bottom:5%" class="fw-bold text-right" id="txt_diskon">2000000</td>
                            </tr>

                            <tr class="padding-top:5%">
                                <td style="padding-bottom:5%" class="fw-bold text-left">Total DPP</td>
                                <td style="padding-bottom:5%"></td>
                                <td style="padding-bottom:5%" class="fw-bold text-right" id="txt_dpp">2000000</td>
                            </tr>

                            <tr class="padding-top:5%">
                                <td style="padding-bottom:5%" class="fw-bold text-left">PPN</td>
                                <td style="padding-bottom:5%" class="fw-bold" id="txt_ppn"></td>
                                <td style="padding-bottom:5%" class="fw-bold text-right" id="txt_ppn_value"></td>
                            </tr>

                            <tr class="padding-top:5%">
                                <td class="fw-bold text-left">Total</td>
                                <td></td>
                                <td class="fw-bold text-right" id="txt_total_all"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10" style="padding-bottom:1%;border:1px solid black">
                <table style="width:100%">
                    <tr>
                        <td style="padding-top:1%">Term Of Payment</td>
                        <td style="padding-top:1%">:</td>
                        <td style="padding-top:1%" id="txt_payterm">(031) 7484680</td>
                    </tr>
                    <tr>
                        <td>Remark</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="align-top">Shipping Address</td>
                        <td class="align-top">:</td>
                        <td>PT. Penta Ocean<br>Jl. Margomulyo Indah A, Kav 2-7, No. 17<br>Surabaya</td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <br>
        <!-- TTD Supplier dan Order By -->
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-3 text-left" style="border:1px solid black">
                <div class="text-center">
                    Approved By Supplier
                </div>
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-3 text-right" style="margin-left:16.7%;border:1px solid black">
                <div class="text-center">
                    Ordered By
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-3" style="padding-bottom:10%;border:1px solid black">

            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-3" style="margin-left:16.7%;padding-bottom:10%;border:1px solid black">

            </div>
            <div class="col-sm-1"></div>
        </div>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-3" style="padding-bottom:2%;border:1px solid black">

            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-3 text-center" style="margin-left:16.7%;border:1px solid black">
                Purchasing
            </div>
            <div class="col-sm-1"></div>
        </div>
        <?php echo form_close() ?>
    </div>
</body>

</html>
<script>
    function get_cetak_by_id() {
        var nomor_po = $("#txt_nomor_po").val();
        $.ajax({
            type: "POST",
            url: site_url + "purchase_order/get_view_print_by_no",
            data: {
                n: nomor_po
            },
            dataType: "JSON",
            success: function(result) {
                if (result.num_rows != 0) {
                    //PO
                    $("#txt_no_po").html(result['<?php echo Purchase_Order::$ID; ?>']);
                    $("#txt_no_pr").html(result['<?php echo Purchase_Order::$ID_PR; ?>']);
                    $("#txt_vendor").html(result['<?php echo Vendor::$NAMA ?>']);
                    $("#txt_tanggal").html(result['<?php echo Purchase_Order::$TANGGAL; ?>']);
                    $("#txt_subtotal").html(result['subtotal']);
                    $("#txt_diskon").html(result['<?php echo Purchase_Order::$DISKON; ?>']);
                    $("#txt_tipe").html(result['<?php echo Material_RM_PM::$TIPE; ?>']);
                    $("#txt_dpp").html(result['total_dpp']);
                    $("#txt_ppn").html(result['<?php echo Purchase_Order::$PPN ?>']);
                    $("#txt_ppn_value").html(result['ppn_value']);
                    $("#txt_total_all").html(result['total_all']);

                    $("#txt_payterm").html(result['<?php echo Payterm::$DESKRIPSI ?>']);

                    //PO Awal
                    $("#tabel_po").html(result.detail_product);

                    $(".html-content").css("display", "block");
                    //$(".html-content").css("visibility", "visible");
                    $("#btn_cetak").prop("disabled", false);
                    $("#btn_email").prop("disabled", false);
                    toast("Nomor PO ditemukan", Color.SUCCESS);


                } else {
                    toast("Nomor PO tidak ditemukan", Color.DANGER);

                    $(".html-content").css("display", "none");
                    $("#btn_cetak").prop("disabled", true);
                    $("#btn_email").prop("disabled", true);

                }
            }
        });
    }

    function CreatePDFfromHTML() {
        var nomor_po = $("#txt_nomor_po").val();

        var HTML_Width = $(".html-content").width();
        var HTML_Height = $(".html-content").height();
        var top_left_margin = 0;
        var PDF_Width = HTML_Width + (top_left_margin * 2);
        var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
        var canvas_image_width = HTML_Width;
        var canvas_image_height = HTML_Height;

        var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

        html2canvas($(".html-content")[0]).then(function(canvas) {
            var imgData = canvas.toDataURL("image/jpeg", 1.0);
            var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
            pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
            for (var i = 1; i <= totalPDFPages; i++) {
                pdf.addPage(PDF_Width, PDF_Height);
                pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
            }
            pdf.save(nomor_po + ".pdf");
            pdf_blob = pdf.output("blob");
            //$(".html-content").hide();
        });
    }
    var pdf_blob = null;

    function email_send() {
        var nomor_po = $("#txt_nomor_po").val();
        var form_data = new FormData();
        form_data.append("attachment", pdf_blob);
        form_data.append("n", nomor_po);
        $.ajax({
            type: 'POST',
            url: site_url + 'purchase_order/email_send',
            data: form_data,
            processData: false,

            success: function(response) {
                // Handle the response from the controller
            },
            error: function(xhr, status, error) {
                // Handle errors
            }
        });
    }
    $('form').submit(function(e) {
        e.preventDefault();
        var formData = new FormData($(this));
        var nomor_po = $("#txt_nomor_po").val();
        formData.append("attachment", pdf_blob);
        formData.append("n", nomor_po);
        $.ajax({
            type: 'POST',
            url: site_url + 'purchase_order/email_send',
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                // Handle the response from the controller
            },
            error: function(xhr, status, error) {
                // Handle errors
            }
        });
    });
</script>