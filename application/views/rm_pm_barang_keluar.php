<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[RM PM] Barang Keluar (WS)</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">RM PM</li>
        <li class="breadcrumb-item">Barang Keluar</li>
        <li class="breadcrumb-item active" aria-current="page">Dari Worksheet</li>
      </ol>
    </nav>
    <h3 class='f-aleo-bold text-center'>[Worksheet]</h3>
    <h1 class='f-aleo-bold text-center'>BARANG KELUAR</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" class="role-accounting role-checker-rm-pm role-ppic btn btn-outline-primary" onclick="get_view_input_ws()" data-mdb-ripple-color="dark">
        Input WS
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="get_view_adjust_ws()" data-mdb-ripple-color="dark">
        Adjustment WS
      </button>
      <button type="button" class="role-purchasing role-accounting btn btn-outline-primary" onclick="get_view_gr_ws()" data-mdb-ripple-color="dark">
        GR Semi
      </button>
    </div>
    <hr style="margin-left:5%;margin-right:5%">
    <div id="content"></div>
  </div>
</body>

</html>
<script>
  class PAGE_TYPE {
    static GR_SEMI = "<?php echo RMPM_GET_TYPE_GR_SEMI ?>";
    static ADJUSTMENT = "<?php echo RMPM_GET_TYPE_ADJUSTMENT ?>";
    static INSERT = "<?php echo RMPM_GET_TYPE_INSERT ?>";
  }
  var ctr = 0;
  var tipe = "";
  var tanggal_ws = null;
  var th_adjustment = '<tr><td class="text-center font-sm f-aleo">Nomor</td><td class="text-center font-sm f-aleo">Kode Panggil</td><td class="text-center font-sm f-aleo">Deskripsi</td><td class="text-center font-sm f-aleo">Vendor</td><td class="text-center font-sm f-aleo">Stock (KG/PCS)</td><td class="text-center font-sm f-aleo">Qty Diproses</td></tr>';

  $(document).ready(function() {
    check_role();
  });

  function get_view_input_ws() {
    tipe = PAGE_TYPE.INSERT;
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/view_insert_checker",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });
  }

  function get_view_adjust_ws() {
    tipe = PAGE_TYPE.ADJUSTMENT;
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/view_adjust_checker",
      data: {
        t: tipe
      },
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
        $(".tabel_detail_product").html(th_adjustment);
      }
    });
  }

  function get_view_gr_ws() {
    tipe = PAGE_TYPE.GR_SEMI;
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/view_adjust_checker",
      data: {
        t: tipe
      },
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
        $(".tabel_detail_product").html(th_adjustment);
      }
    });
  }

  function plus_product() {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });

    var html = clone.html();
    $.ajax({
      type: "POST",
      url: site_url + "material_rm_pm/plus",
      data: {
        c: ctr,
        t: "<?php echo RMPM_PLUS_TYPE_OUT; ?>"
      },
      success: function(result) {
        $(".tabel_detail_product").html(html + result);
        ctr++;
        $(".tabel_detail_product").css("visibility", "visible");
      }
    });
  }

  function minus_product(params) {
    var temp = $(".tabel_detail_product");
    var clone = temp.clone();
    clone.find(':input').each(function() {
      var input = $(this);
      // start an attribute object later use with attr()
      var attrs = {
        value: input.val()
      }

      // add the attributes to element
      input.attr(attrs);

    });
    $(".tr_detail_product_" + params).remove();
  }

  function check_qty(ctr) {
    $("#txt_stock_" + ctr).removeClass("red-text green-text");
    $("#txt_qty_proses_" + ctr).removeClass("red-text green-text");
    $("#txt_view_qty_ws_" + ctr).removeClass("red-text green-text");

    var qty_stock = parseFloat($("#txt_stock_" + ctr).val());
    var qty_ws = parseFloat($("#txt_qty_ws_" + ctr).val());
    var qty_proses = parseFloat($("#txt_qty_proses_" + ctr).val());

    if (qty_stock - qty_proses >= 0) {
      $("#txt_stock_" + ctr).addClass("green-text");
      //$("#txt_qty_proses_" + ctr).addClass("green-text");
    } else {
      $("#txt_stock_" + ctr).addClass("red-text");
      //$("#txt_qty_proses_" + ctr).addClass("red-text");
    }

    if (qty_ws - qty_proses >= 0) {
      $("#txt_view_qty_ws_" + ctr).addClass("green-text");
      //$("#txt_qty_proses_" + ctr).addClass("red-text");
    } else {
      $("#txt_view_qty_ws_" + ctr).addClass("red-text");
      //$("#txt_qty_proses_" + ctr).addClass("green-text");
    }

  }

  function toggle_checkbox_qty(ctr) {
    if ($("#cb_qty_" + ctr).prop("checked")) {
      $("#txt_qty_proses_" + ctr).prop("disabled", false);
      if ($("#cb_vendor_" + ctr).length > 0) {
        $("#cb_vendor_" + ctr).prop("disabled", false);
      }
    } else {
      $("#txt_qty_proses_" + ctr).prop("disabled", true);
      if ($("#cb_vendor_" + ctr).length > 0) {
        $("#cb_vendor_" + ctr).prop("disabled", true);
      }
    }
    check_qty(ctr);

  }

  function toggle_all() {
    var enabled = $("#cb_toggle").prop("checked");
    if (enabled) {
      $(".checkbox_ws").prop("checked", true);
    } else
      $(".checkbox_ws").prop("checked", false);

    for (var i = 0; i < ctr; i++) {
      toggle_checkbox_qty(i);
    }
  }

  function gen_ws() {
    var arr_id = new Array();
    var arr_qty = new Array();
    var arr_no_urut = new Array();
    var arr_vendor = new Array();
    var idx = 0;
    var is_true = true;
    var qty_proses = 0;
    var qty_stock = 0;
    var qty_ws = 0;
    var nomor_ws = $("#txt_nomor_ws").val();
    var tanggal = $("#txt_tanggal_buat").val();

    if (tanggal == "") {
      toast("Tanggal tidak boleh kosong", Color.DANGER);
      return;
    } else {
      if (tanggal_ws != null) {
        if (tanggal < tanggal_ws) {
          toast("Tanggal transaksi tidak boleh lebih kecil dari tanggal WS", Color.DANGER);
          return;
        }

      }
    }
    for (var i = 0; i < ctr; i++) {
      if ($("#cb_qty_" + i).is(":checked")) {
        if ($("#txt_view_qty_ws_" + i).hasClass("red-text") || $("#txt_stock_" + i).hasClass("red-text")) {
          is_true = false;
          break;
        }
        arr_id[idx] = parseInt($("#txt_id_material_" + i).val());

        qty_ws = parseFloat($("#txt_qty_ws_" + i).val());
        qty_stock = parseFloat($("#txt_stock_" + i).val());
        if ($("#txt_qty_proses_" + i).val() == "")
          qty_proses = 0;
        else
          qty_proses = parseFloat($("#txt_qty_proses_" + i).val());

        arr_qty[idx] = qty_proses;

        if ((qty_stock - qty_proses < 0) || (qty_ws - qty_proses < 0)) {
          is_true = false;
        }

        arr_no_urut[idx] = parseInt($("#txt_no_urut_" + i).val());

        if ($("#cb_vendor_" + i).length > 0)
          arr_vendor[idx] = $("#txt_id_vendor_" + i + "_" + $("#cb_vendor_" + i)[0].selectedIndex).val();
        else
          arr_vendor[idx] = null;
        idx++;
      }
    }
    if (idx == 0) {
      toast("Pastikan setidaknya ada 1 material yang terpilih", Color.DANGER);
    } else if (is_true) {
      $("#btn_generate").prop("disabled", true);
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "work_sheet/insert_checker",
          data: {
            ai: arr_id,
            aq: arr_qty,
            av: arr_vendor,
            an: arr_no_urut,
            nw: nomor_ws,
            t: tanggal,
            c: idx
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
              $("#txt_nomor_ws").val("");
            } else {
              toast(result, Color.DANGER);
              $("#btn_generate").prop("disabled", false);
            }
          }
        });
      }
    } else {
      toast("Ada material yang qty diproses kosong atau melebihi qty WS, silahkan cek lagi", Color.DANGER);
      $("#btn_generate").prop("disabled", false);
    }
  }

  function gen_adjustment_ws() {
    var arr_id = new Array();
    var arr_qty = new Array();
    var arr_vendor = new Array();
    var idx = 0;
    var is_true = true;
    var qty_proses = 0;
    var qty_stock = 0;
    var qty_ws = 0;
    var nomor_ws = $("#txt_nomor_ws").val();
    var tanggal = $("#txt_tanggal_buat").val();

    if (tanggal == "") {
      toast("Tanggal tidak boleh kosong", Color.DANGER);
      return;
    } else {
      if (tanggal_ws != null) {
        if (tanggal < tanggal_ws) {
          toast("Tanggal transaksi tidak boleh lebih kecil dari tanggal WS, pastikan WS sudah diinput dulu", Color.DANGER);
          return;
        }

      }
    }
    for (var i = 0; i < ctr; i++) {
      if ($("#txt_kode_panggil_" + i).length > 0 && $("#txt_kode_panggil_" + i).val() != "") {
        if ($("#txt_stock_" + i).hasClass("red-text")) {
          is_true = false;
          break;
        }
        arr_id[idx] = parseInt($("#txt_id_material_" + i).val());

        if ($("#txt_qty_proses_" + i).val() == "")
          qty_proses = 0;
        else
          qty_proses = parseFloat($("#txt_qty_proses_" + i).val());
        qty_stock = parseFloat($("#txt_stock_" + i).val());

        arr_qty[idx] = qty_proses;
        if ((qty_stock - qty_proses < 0) || qty_proses == 0) {
          is_true = false;
          break;
        }
        if ($("#cb_vendor_" + i).length > 0)
          arr_vendor[idx] = $("#txt_id_vendor_" + i + "_" + $("#cb_vendor_" + i)[0].selectedIndex).val();
        else
          arr_vendor[idx] = null;
        idx++;
      }
    }
    if (is_true) {
      $("#btn_generate").prop("disabled", true);
      var c = confirm("Apakah Anda Yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "work_sheet/adjustment_checker",
          data: {
            ai: arr_id,
            aq: arr_qty,
            av: arr_vendor,
            nw: nomor_ws,
            t: tanggal,
            c: idx
          },
          success: function(result) {
            if (result.includes(Status.MESSAGE_KEY_SUCCESS)) {
              toast(result, Color.SUCCESS);
              reset_form();
              $("#txt_nomor_ws").val("");
            } else {
              toast(result, Color.DANGER);
            }
            $("#btn_generate").prop("disabled", false);

          }
        });
      } else {
        $("#btn_generate").prop("disabled", false);
      }
    } else {
      toast("Ada material yang qty diproses kosong atau melebihi qty WS, silahkan cek lagi", Color.DANGER);
      $("#btn_generate").prop("disabled", false);
    }
  }

  function get_ws_by_no() {
    var nomor_ws = $("#txt_nomor_ws").val();
    $.ajax({
      type: "POST",
      url: site_url + "work_sheet/get_view_by_no",
      data: {
        n: nomor_ws
      },
      dataType: "JSON",
      success: function(result) {
        if (result.num_rows != 0) {
          $("#txt_tanggal_buat").prop("disabled", false);
          $("#btn_generate").prop("disabled", false);
          tanggal_ws = result['<?php echo Worksheet::$TANGGAL; ?>'];
          if (tipe == PAGE_TYPE.INSERT) {
            ctr = result.ctr;
            $("#txt_title_ws").html(result.nomor_worksheet);
            $(".tabel_detail_product").html(result.detail_product);
            $(".tabel_detail_product").css("visibility", "visible");
            if (!$.fn.DataTable.isDataTable('.tabel_detail_product')) {
              $('.tabel_detail_product').dataTable({
                "order": [
                  [3, "desc"]
                ], //status
                paging: false,
                searching: false,
                info: false,
                "pagingType": "full",
                dom: 'Bfrtip',
                buttons: [{
                  extend: 'excel',
                  title: 'RM PM Keluar - WS no ' + nomor_ws
                }]
              });
              $(".buttons-excel span").text('Export ke Excel');
              $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
            }
          } else if (tipe == PAGE_TYPE.ADJUSTMENT || tipe == PAGE_TYPE.GR_SEMI) {
            ctr = 0;
            $("#btn_plus_material").prop("disabled", false);
            $("#div_status").html("WS Ditemukan");
            $("#div_status").addClass("green-text");

          }
        } else {
          reset_form();
          if (tipe == PAGE_TYPE.ADJUSTMENT || tipe == PAGE_TYPE.GR_SEMI) {
            $("#div_status").html("WS Tidak Ditemukan");
            $("#div_status").addClass("red-text");
          }
        }
      }
    });
  }

  function change_stock(ctr) {
    //$("#txt_view_stock_" + ctr).html(value);
    $("#txt_stock_" + ctr).val($("#cb_vendor_" + ctr).val());
    check_adjustment_qty(ctr);
  }

  function check_adjustment_qty(params) {
    $("#txt_stock_" + params).removeClass("red-text green-text");
    var total_inp_stock = 0;
    if ($("#txt_qty_proses_" + params).length > 0)
      total_inp_stock = parseFloat($("#txt_qty_proses_" + params).val());
    var total_stock = parseFloat($("#txt_stock_" + params).val());
    if (total_stock < total_inp_stock) {
      $("#txt_stock_" + params).addClass("red-text");
    } else {
      $("#txt_stock_" + params).addClass("green-text");
    }
  }

  function check_product(params) {
    var kode_panggil = $("#txt_kode_panggil_" + params).val();
    var nomor_ws = $("#txt_nomor_ws").val();
    if (kode_panggil != "") {
      $.ajax({
        type: "POST",
        url: site_url + "material_rm_pm/get",
        data: {
          kp: kode_panggil,
          t: tipe,
          n: nomor_ws
        },
        dataType: "JSON",
        success: function(result) {

          if ((tipe == PAGE_TYPE.GR_SEMI && result.is_semi_product && result.num_rows != 0) || (result.num_rows != 0 && tipe == PAGE_TYPE.ADJUSTMENT)) {
            $("#txt_barang_desc_" + params).val(result.description);
            $("#txt_barang_desc_" + params).addClass("green-text");
            $("#txt_id_material_" + params).val(result.id_material_rm_pm);
            $("#txt_stock_" + params).val(result.stock);

            if (result.ctr_vendor > 0) { //ada vendor
              var select = "<select onchange=change_stock(" + params + ") class='form-control f-aleo' id='cb_vendor_" + params + "'>";
              for (var i = 0; i < result.ctr_vendor; i++) {
                select += "<option value='" + result.vendor_qty[i] + "'>" + result.vendor_nama[i] + "</option>";
              }
              select += "</select>";
              for (var i = 0; i < result.ctr_vendor; i++) {
                select += '<input type="hidden" id="txt_id_vendor_' + params + '_' + i + '" value="' + result.vendor_id[i] + '"/>';
              }
              $("#td_vendor_" + params).html(select);
            }
            if (result.stock_proses != 0) {
              $("#txt_qty_proses_" + params).prop("disabled", "disabled");
              $("#txt_qty_proses_" + params).val(parseFloat(result.stock_proses * -1).toFixed(2));
            }


          } else {
            $("#txt_barang_desc_" + params).val("barang belum ditemukan!");
            $("#txt_barang_desc_" + params).removeClass('green-text');
            $("#txt_barang_desc_" + params).addClass('red-text');

            $("#td_vendor_" + params).html("");

            $("#txt_stock_" + params).removeClass('green-text red-text');
            $("#txt_stock_" + params).val(0);

            $("#txt_qty_proses_" + params).removeClass('green-text red-text');

          }
        }
      });
    }
  }

  function reset_form() {
    //$(".tabel_detail_product").dataTable.destroy();
    if ($.fn.DataTable.isDataTable('.tabel_detail_product')) {
      $(".tabel_detail_product").DataTable().destroy();
    }
    $(".tabel_detail_product").css("visibility", "hidden");
    $("#txt_title_ws").html("");
    $("#txt_tanggal_buat").prop("disabled", true);
    $("#txt_tanggal_buat").val("");
    $("#txt_nomor_sj").prop("disabled", true);
    $("#btn_generate").prop("disabled", true);
    $("#div_status").html("");
    ctr = 0;

    //Adjustment WS
    if (tipe == PAGE_TYPE.ADJUSTMENT) {
      $("#div_status").removeClass("red-text green-text");
      $("#div_status").html("");
      $(".tabel_detail_product").html(th_adjustment);
      $("#btn_plus_material").prop("disabled", true);

    }
    $(".buttons-excel").remove();
  }
</script>