<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>[FG] Template Stock Opname</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">FG</li>
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item">Stock Opname</li>
        <li class="breadcrumb-item active" aria-current="page">Template Tahunan</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>TEMPLATE STOCK OPNAME</h1>
    <div class="row" style="margin-right:1%;margin-left:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <span class="form-check">
          <input class="form-check-input" onclick='toggle_checkbox()' type="checkbox" name="rbHide" id="rb_hide_0">
          <label class="form-check-label" for="rbHide">
            Sembunyikan Rak yang Kosong
          </label>
        </span>
        <h6 class='f-aleo-bold text-center'></h6>
        <div class='text-center'>
          <button type="button" class="center btn btn-outline-primary" onclick="get_template_stock_opname()" data-mdb-ripple-color="dark">
            GENERATE
          </button>
        </div>
      </div>
      <div class="col-sm-1"></div>
    </div>
    <div id="content">
    </div>
</body>

</html>

<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';
  var hide = "n";

  function get_template_stock_opname() {
    $("#content").removeClass("animated fadeInDown");
    $.ajax({
      type: "POST",
      url: site_url + "material/view_template_stock_opname",
      data: {
        h: hide
      },
      success: function(result) {
        $("#content").html(result);
        $('#mastertable').DataTable({
          "order": [
            [2, "asc"],
            [3, "asc"],
            [4, "asc"]
          ], //Rak, Kolom, Tingkat
          paging: true,
          "pagingType": "full",
          dom: 'Bfrtip',
          buttons: [
            'excel',
          ]
        });
        $(".buttons-excel span").text('Export ke Excel');
        $(".buttons-excel").addClass('btn btn-md float-left btn-outline-success');
        $("#content").addClass("animated fadeInDown");
      }
    });

  }

  function toggle_checkbox() {
    if (hide == "n")
      hide = "y";
    else {
      hide = "n";
    }
  }
</script>