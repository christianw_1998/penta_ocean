<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Absensi</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo role-kepala role-admin">
  <?php
  include("navigation.php");
  ?>
  <br>
  <div class=" animated fadeInUp">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Master Absensi</li>
      </ol>
    </nav>
    <div class="text-center" style="margin-bottom:2%">
      <button type="button" id="btn_mod_abs_karyawan" class="btn btn-outline-primary" data-mdb-toggle="modal" data-mdb-target="#mod_upl_abs_karyawan" data-mdb-ripple-color="dark">
        Upload Batch
      </button>
      <button type="button" class="btn btn-outline-primary" onclick="tabel_absensi()" data-mdb-ripple-color="dark">
        Tabel Absensi
      </button>
      <button type="button" class="btn btn-outline-primary role-kepala" onclick="laporan_absensi()" data-mdb-ripple-color="dark">
        Performa Kehadiran Bulanan
      </button>
      <!--<button type="button" class="btn btn-outline-primary" onclick="rincian_absensi()" data-mdb-ripple-color="dark">
        Rincian Absensi
      </button>-->
      <button type="button" class="btn btn-outline-primary role-kepala" onclick="performance_kehadiran()" data-mdb-ripple-color="dark">
        Performa Kehadiran Per Karyawan
      </button>
      <button type="button" class="btn btn-outline-primary role-kepala" onclick="laporan_gaji()" data-mdb-ripple-color="dark">
        Laporan Gaji
      </button>
    </div>
    <div id="content" style="margin-bottom:5%">
    </div>
  </div>
</body>

</html>
<!-- Modal Upload Data Absensi Karyawan-->
<div class="modal fade" id="mod_upl_abs_karyawan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h5 class="modal-title w-100 f-aleo-bold text-center font-lg" id="myModalLabel">UPLOAD ABSENSI KARYAWAN</h5>
      </div>
      <!--Body-->
      <div class="modal-body" style="margin:1%">
        <div class="row">
          <div class="text-center col-sm-12 fw-bold">Perhatian</div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-1">1.</div>
              <div class="col-sm-11 text-justify">
                Silahkan mendownload file template yang disediakan dibawah, jangan membuat file sendiri untuk menghindari salah input.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">2.</div>
              <div class="col-sm-11 text-justify">
                Hanya ubah isi dari file excel (<span class="fw-bold">.xlsx</span>) yang diberikan, header jangan diubah-ubah.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">3.</div>
              <div class="col-sm-11 text-justify">
                Jika ada kolom data yang memang kosong, <span class="fw-bold">tidak perlu diisikan apa-apa</span> pada kolom tersebut.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">4.</div>
              <div class="col-sm-11 text-justify">
                Kolom NIK <span class="fw-bold">wajib diisi</span> agar bisa menjadi identifier karyawan.
              </div>
            </div>
            <div class="row">
              <div class="col-sm-1">5.</div>
              <div class="col-sm-11 text-justify">
                Keterangan yang diijinkan hanya: <span class="fw-bold">(Kosong), Tukar Hari, Sakit, Alpha, Luar Kota, Cuti, Lembur.</span> Jika ada keterangan diluar hal diatas, <span class="fw-bold">silahkan menghubungi admin.</span>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="text-center col-sm-12 fw-bold">Contoh Tampilan dari File Benar</div>
          <div class="col-sm-12">
            <figure class="figure d-flex justify-content-center">
              <img class="border border-info figure-img rounded-0 img-fluid" src="<?php echo base_url("asset/img/photo/example_file_excel_karyawan_all_absensi.png"); ?>" />
            </figure>
          </div>
        </div>
        <div class="row">
          <div class="text-center col-sm-6 fw-bold">Template yang Disediakan</div>
          <div class="text-center col-sm-6 fw-bold">Contoh File Benar yang Ada Isinya</div>
        </div>
        <div class="row">
          <div class="text-center col-sm-6 ">
            <a class=" btn btn-outline-success" download href="<?php echo base_url("asset/download/karyawan_all_absensi_template.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              Template (.xlsx)
            </a>
          </div>
          <div class="text-center col-sm-6">
            <a class="btn btn-outline-success" download href="<?php echo base_url("asset/download/karyawan_all_absensi_file_benar.xlsx"); ?>" id="btn_mod_karyawan" data-mdb-toggle="modal" data-mdb-target="#mod_upl_karyawan" data-mdb-ripple-color="dark">
              File Benar (.xlsx)
            </a>
          </div>
        </div>
        <br>
        <form id="form_upload_abs_karyawan" action="" method="post" accept-charset='utf-8' enctype="multipart/form-data">
          <div class="row">
            <div class="text-center col-sm-12 fw-bold">Jika file Excel (.xlsx) sudah berhasil dibuat, silahkan upload dengan menekan tombol upload berikut:</div>
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
              <input type="file" onchange="change_label('lbl_upl_abs_karyawan','txt_upl_abs_karyawan')" accept=".xlsx" name="txt_upl_abs_karyawan" class="txt_upl_abs_karyawan custom-file-input" id="txt_upl_abs_karyawan" aria-describedby="inputGroupFileAddon01">
              <label class="custom-file-label lbl_upl_abs_karyawan" for="txt_upl_abs_karyawan">File Absensi Karyawan (.xlsx)</label>
            </div>
            <div class="col-sm-3"></div>
          </div>
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="text-center txt_desc_upload fw-bold col-sm-6"></div>
            <div class="col-sm-3"></div>
          </div>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="submit" id="btn_upl_abs_karyawan" class="btn btn-primary">Upload</button>
      </div>
    </div>
    </form>
    <!--/.Content-->
  </div>
</div>
<!-- modal -->


<script language="javascript">
  var site_url = '<?php echo site_url(); ?>';
  $("#mod_upl_karyawan").on("hidden.bs.modal", function() {
    refresh_modal();
  });
  $("#mod_upl_abs_karyawan").on("hidden.bs.modal", function() {
    refresh_modal();
  });

  function refresh_modal() {
    //absensi karyawan
    $('#form_upload_abs_karyawan').trigger("reset"); // this will reset the form fields
    $(".txt_upl_abs_karyawan").val("");
    $(".lbl_upl_abs_karyawan").html("File Absensi Karyawan (.xlsx)");

    $(".txt_desc_upload").html("");
  }
  $('#form_upload_abs_karyawan').submit(function(e) {
    e.preventDefault();
    var form_data = new FormData(this);
    $.ajax({
      url: site_url + "karyawan/upload_absensi",
      type: "POST",
      data: form_data, //this is formData
      processData: false,
      contentType: false,
      cache: false,
      success: function(data) {
        $(".txt_desc_upload").removeClass("text-danger text-success");
        if (data.includes(Status.MESSAGE_KEY_SUCCESS)) {
          $(".txt_desc_upload").addClass("text-success");
        } else {
          $(".txt_desc_upload").addClass("text-danger");
        }
        $(".txt_desc_upload").html(data);
        $("#content").html("");
      },
      complete: function(data) {
        setTimeout(refresh_modal, 1500);
      }
    });
  });

  function laporan_absensi() {
    $.ajax({
      type: "POST",
      url: site_url + "laporan_f/absensi",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });

    $("#content").removeClass("animated fadeInDown");
  }

  function tabel_absensi() {
    $.ajax({
      type: "POST",
      url: site_url + "laporan_f/absensi_tabel",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });

    $("#content").removeClass("animated fadeInDown");
  }

  function rincian_absensi() {
    $.ajax({
      type: "POST",
      url: site_url + "rincian_f/absensi",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });
    $("#content").removeClass("animated fadeInDown");
  }

  function performance_kehadiran() {
    $.ajax({
      type: "POST",
      url: site_url + "rincian_f/absensi_performance",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });
    $("#content").removeClass("animated fadeInDown");
  }

  function laporan_gaji() {
    $.ajax({
      type: "POST",
      url: site_url + "laporan_f/gaji_absensi",
      success: function(result) {
        $("#content").addClass("animated fadeInDown");
        $("#content").html(result);
      }
    });

    $("#content").removeClass("animated fadeInDown");
  }

  $(document).ready(function() {
    check_role();
    $('#btn_mod_abs_karyawan').click(function() {
      $('#mod_upl_abs_karyawan').modal({
        show: true
      });
    });
    $('#mastertable').on('click', function() {
      check_role();
    });

  });
</script>