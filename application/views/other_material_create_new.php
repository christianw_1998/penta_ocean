<!DOCTYPE html>
<html lang="en" class="full-height">

<head>
  <title>Buat Material Baru</title>
  <?php
  include("library.php");
  include("redirect_login.php");
  include("role_management.php");
  ?>
</head>

<body class="f-aleo">
  <?php
  include("navigation.php");
  ?>
  <br>

  <div class="animated fadeInDown">
    <nav aria-label="breadcrumb" style="margin-left:1%;margin-right:1%">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url("home"); ?>">Home</a></li>
        <li class="breadcrumb-item">Costing</li>
        <li class="breadcrumb-item">Material</li>
        <li class="breadcrumb-item active" aria-current="page">Buat Baru</li>
      </ol>
    </nav>
    <h1 class='f-aleo-bold text-center'>BUAT MATERIAL BARU</h1>
    <hr style="margin-left:5%;margin-right:5%">
    <br>

    <div class="d-flex justify-content-center" style="margin-right:1%">
      <form id="form_create" style="width:60%">
        <table style="width:100%">
          <tr>
            <td class="text-right">Kategori</td>
            <td>:</td>
            <td class="text-left"><select class="form-control" id="dd_kategori" onchange="cek_asset()" style="width:100%"></select></td>
          </tr>
          <tr>
            <th colspan=3 class="fw-bold">Material</td>
          </tr>
          <tr>
            <td class="text-right">Kode Panggil</td>
            <td>:</td>
            <td class="text-left"><input type="text" style="width:100%" id="txt_cek_kode_material"></input></td>
            <td class="text-left"><button type="button" onclick="get_last_kode_mat()">Cek</button></td>
          </tr>
          <tr>
            <td class="text-right">Kode</td>
            <td>:</td>
            <td class="text-left"><input type="text" class="km red-text" style="width:100%" disabled id="txt_kode_material" value="Input Kode Panggil terlebih dulu dan cek"></td>
          </tr>
          <tr>
            <td class="text-right">Deskripsi</td>
            <td>:</td>
            <td class="text-left"><textarea type="text" rows=5 style="width:100%;resize: none" id="txt_deskripsi"></textarea></td>
          </tr>
          <tr>
            <th colspan=3 class="fw-bold">Asset Class</td>
          </tr>
          <tr>
            <td class="text-right">Kode</td>
            <td>:</td>
            <td class="text-left"><select class="form-control" onchange="get_gl_by_id_ac()" disabled id="dd_ac" style="width:100%"></select></td>
          </tr>
          <tr>
            <th colspan=3 class="fw-bold">Cost Center</td>
          </tr>
          <tr>
            <td class="text-right">Kode</td>
            <td>:</td>
            <td class="text-left"><input type="hidden" style="width:100%" id="txt_hidden_id_cc"></input><input type="text" style="width:100%" id="txt_kode_cc"></input></td>
            <td class="text-left"><button type="button" id="btn_cc" onclick="get_cc_by_id()">Cek</button></td>
          </tr>
          <tr>
            <td class="text-right">Deskripsi</td>
            <td>:</td>
            <td class="text-left"><input type="text" style="width:100%" disabled id="txt_desc_kode_cc" class="cc red-text" value="Input Kode CC terlebih dulu dan cek"></td>
          </tr>
          <tr>
            <th colspan=3 class="fw-bold">Vendor</td>
          </tr>
          <tr>
            <td class="text-right">Kode</td>
            <td>:</td>
            <td class="text-left"><input type="text" style="width:100%" onkeyup="get_vendor_by_id()" id="txt_kode_vendor"></input></td>
          </tr>
          <tr>
            <td class=" text-right">Deskripsi</td>
            <td>:</td>
            <td class="text-left"><input type="text" style="width:100%" disabled id="txt_nama_vendor" class="vn red-text" value="Input 6 digit Kode Vendor terlebih dahulu"></td>
          </tr>
          <!--<tr>
            <td class="text-right">Vendor</td>
            <td>:</td>
            <td class="text-left"><select class="form-control" id="dd_vendor" style="width:100%"></select></td>
          </tr>-->
          <tr>
            <th colspan=3 class="fw-bold">General Ledger</td>
          </tr>
          <tr>
            <td class="text-right">Kode</td>
            <td>:</td>
            <td class="text-left"><input type="text" style="width:100%" id="txt_kode_gl"></input></td>
            <td class="text-left"><button type="button" id="btn_gl" onclick="get_gl_by_id()">Cek</button></td>
          </tr>
          <tr>
            <td class=" text-right">Deskripsi</td>
            <td>:</td>
            <td class="text-left"><input type="text" style="width:100%" disabled id="txt_desc_kode_gl" class="gl red-text" value="Input Kode GL terlebih dulu dan cek"></td>
          </tr>
          <tr>
            <th colspan=3 class="fw-bold">Penyusutan</td>
          </tr>
          <tr>
            <td class="text-right">Metode</td>
            <td>:</td>
            <td class="text-left"><select class="form-control" disabled id="dd_mp" style="width:100%"></select></td>
          </tr>
          <!--<tr>
            <td class="text-right">Masa Manfaat</td>
            <td>:</td>
            <td class="text-left"><input class="text-right" style="width:15%" type="number" disabled min=0 value=0 id="txt_masa_manfaat"></input> Tahun</td>
            <td class="text-left"></td>
          </tr>-->

        </table>
      </form>
    </div>

    <div class="row" style="margin-right:1%">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 text-center">
        <button type="button" onclick="generate()" class="center text-center btn btn-outline-success" data-mdb-ripple-color="dark">
          Create
        </button>
      </div>
      <div class="col-sm-1"></div>
    </div>
  </div>
</body>

</html>

<script>
  $(document).ready(function() {
    check_role();
    get_all_kategori();
    get_all_ac();
    get_all_metode_penyusutan();
    //get_all_cc();
    //get_all_vendor();
  });

  function get_all_kategori() {

    $.ajax({
      type: "POST",
      url: site_url + "other_kategori/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";
        kal += "<option value='-1'>-- PILIH KATEGORI --</option>";

        for (var i = 0; i < result.length; i++) {
          kal += "<option value='" + result[i]['<?php echo Other_Kategori::$ID; ?>'] + "'>" + result[i]['<?php echo Other_Kategori::$NAMA; ?>'] + "</option>";
        }
        $("#dd_kategori").html(kal);
        $('#dd_kategori option:last').remove(); //Hapus Penambahan Asset
        cek_asset();

      }
    });
  }

  function get_all_ac() {
    $.ajax({
      type: "POST",
      url: site_url + "other_ac/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";
        kal += "<option value='-1'>-- PILIH ASSET CLASS --</option>";
        for (var i = 0; i < result.length; i++) {
          kal += "<option value='" + result[i]['<?php echo Other_Asset_Class::$ID; ?>'] + "'>" + result[i]['<?php echo Other_Asset_Class::$ID; ?>'] + " - " + result[i]['<?php echo Other_Asset_Class::$DESKRIPSI; ?>'] + "</option>";
        }
        $("#dd_ac").html(kal);

      }
    });
  }

  function get_gl_by_id_ac() {
    var id = $("#dd_ac").val();
    $.ajax({
      type: "POST",
      url: site_url + "other_ac/get_gl_by_id",
      data: {
        i: id
      },
      success: function(result) {
        $("#dd_mp").prop("disabled", false);
        $("#txt_kode_gl").val(result);
        if ($("#dd_ac").val() == "N800") //Special Low Value Asset
          $("#dd_mp").prop("disabled", true);
        get_gl_by_id();
      }
    });
  }

  /*function get_all_cc() {
    $.ajax({
      type: "POST",
      url: site_url + "cc/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";
        kal += "<option value='-1'>-- PILIH COST CENTER --</option>";

        for (var i = 0; i < result.length; i++) {
          kal += "<option value='" + result[i]['<?php echo Cost_Center::$ID; ?>'] + "'>" + result[i]['<?php echo Cost_Center::$ID; ?>'] + " - " + result[i]['<?php echo Cost_Center::$NAMA; ?>'] + "</option>";
        }
        $("#dd_cc").html(kal);

      }
    });
  }*/

  function get_all_vendor() {
    $.ajax({
      type: "POST",
      url: site_url + "vendor/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";
        kal += "<option value='-1'>-- PILIH VENDOR --</option>";
        for (var i = 0; i < result.length; i++) {
          kal += "<option value='" + result[i]['<?php echo Vendor::$ID; ?>'] + "'>" + result[i]['<?php echo Vendor::$ID; ?>'] + " - " + result[i]['<?php echo Vendor::$NAMA; ?>'] + "</option>";
        }
        $("#dd_vendor").html(kal);

      }
    });
  }

  function get_all_metode_penyusutan() {
    $.ajax({
      type: "POST",
      url: site_url + "other_metode_penyusutan/get_all",
      dataType: "json",
      success: function(result) {
        var kal = "";
        kal += "<option value='-1'>-- METODE PENYUSUTAN - MASA MANFAAT - TARIF --</option>";

        for (var i = 0; i < result.length; i++) {
          kal += "<option value='" + result[i]['<?php echo Other_Metode_Penyusutan::$ID; ?>'] + "'>" + result[i]['<?php echo Other_Metode_Penyusutan::$METODE; ?>'] + " - " + result[i]['<?php echo Other_Metode_Penyusutan::$MASA_MANFAAT; ?>'] + " TAHUN - " + result[i]['<?php echo Other_Metode_Penyusutan::$TARIF; ?>'] + "% </option>";
        }
        $("#dd_mp").html(kal);

      }
    });
  }

  function cek_asset() {
    var id = $("#dd_kategori").prop("selectedIndex");
    if (id == 1) { //Asset
      $("#dd_ac").prop("disabled", false);
      $("#dd_mp").prop("disabled", false);
      $("#txt_masa_manfaat").prop("disabled", false);
      $("#txt_kode_gl").prop("disabled", true);
      $("#btn_gl").prop("disabled", true);

      get_gl_by_id_ac();
    } else {
      $("#dd_ac").prop("disabled", true);
      $("#dd_mp").prop("disabled", true);
      $("#txt_masa_manfaat").prop("disabled", true);
      $("#txt_kode_gl").prop("disabled", false);
      $("#btn_gl").prop("disabled", false);
      $("#txt_kode_gl").val("");
      get_gl_by_id();

    }
  }

  function get_last_kode_mat() {
    var kp = $("#txt_cek_kode_material").val();

    if (kp.length == 3) {
      $("#txt_kode_material").removeClass("red-text green-text");

      if (kp != "") {
        $.ajax({
          type: "POST",
          url: site_url + "other_material/get_last_code",
          data: {
            kp: kp
          },
          success: function(result) {
            $("#txt_kode_material").val(result);
            $("#txt_kode_material").addClass("green-text");

          }
        });
      } else {
        $("#txt_kode_material").addClass("red-text");
        $("#txt_kode_material").val("Input Kode Panggil terlebih dulu dan cek");
      }
    } else {
      toast("Kode panggil harus 3 digit", Color.DANGER);

    }
  }

  function get_gl_by_id() {
    var id = $("#txt_kode_gl").val();
    $.ajax({
      type: "POST",
      url: site_url + "gl/get",
      dataType: "json",
      data: {
        i: id
      },
      success: function(result) {
        $("#txt_desc_kode_gl").removeClass("red-text green-text");
        if (result.length != 0) {
          $("#txt_desc_kode_gl").val(result['<?php echo General_Ledger::$DESKRIPSI; ?>']);
          $("#txt_desc_kode_gl").addClass("green-text");

        } else {
          $("#txt_desc_kode_gl").addClass("red-text");
          $("#txt_desc_kode_gl").val("Input Kode GL terlebih dulu dan cek");
        }

      }
    });

  }

  function get_cc_by_id() {
    var id = $("#txt_kode_cc").val();
    $.ajax({
      type: "POST",
      url: site_url + "cc/get_by_id",
      dataType: "json",
      data: {
        i: id
      },
      success: function(result) {
        $("#txt_desc_kode_cc").removeClass("red-text green-text");
        if (result.length != 0) {
          $("#txt_desc_kode_cc").val(result['<?php echo Cost_Center::$NAMA; ?>']);
          $("#txt_desc_kode_cc").addClass("green-text");
          $("#txt_hidden_id_cc").val(result['<?php echo Cost_Center::$ID; ?>']);

        } else {
          $("#txt_desc_kode_cc").addClass("red-text");
          $("#txt_desc_kode_cc").val("Input Kode CC terlebih dulu dan cek");
        }

      }
    });

  }

  function get_vendor_by_id() {
    var id = $("#txt_kode_vendor").val();
    if (id.length == 6) {
      $.ajax({
        type: "POST",
        url: site_url + "vendor/get",
        dataType: "json",
        data: {
          iv: id
        },
        success: function(result) {
          $("#txt_nama_vendor").removeClass("red-text green-text");
          if (result.length != 0) {
            $("#txt_nama_vendor").prop("disabled", false);
            $("#txt_nama_vendor").val(result['<?php echo Vendor::$NAMA; ?>']);
            $("#txt_nama_vendor").addClass("green-text");

          } else {
            $("#txt_nama_vendor").prop("disabled", true);
            $("#txt_nama_vendor").addClass("red-text");
            $("#txt_nama_vendor").val("Input 6 digit Kode Vendor terlebih dahulu");
          }

        }
      });
    } else {
      $("#txt_nama_vendor").prop("disabled", true);
      $("#txt_nama_vendor").removeClass("red-text green-text");
      $("#txt_nama_vendor").addClass("red-text");
      $("#txt_nama_vendor").val("Input 6 digit Kode Vendor terlebih dahulu");
    }

  }

  function generate() {
    var id_kat = $("#dd_kategori").val();
    var id_mat = $("#txt_kode_material").val();
    var deskripsi = $("#txt_deskripsi").val();
    var id_ac = $("#dd_ac").val();
    var id_cc = $("#txt_hidden_id_cc").val();
    var id_mp = $("#dd_mp").val();
    var id_vendor = $("#txt_kode_vendor").val();
    var nama_vendor = $("#txt_nama_vendor").val();
    var id_gl = $("#txt_kode_gl").val();
    //var masa_manfaat = $("#txt_masa_manfaat").val();

    if (id_kat != -1 && $("#txt_kode_material").hasClass("green-text") && $("#txt_nama_vendor").hasClass("green-text") && deskripsi != "" && id_cc != -1 &&
      $("#txt_desc_kode_gl").hasClass("green-text")) {
      if (id_kat == 1) { //Kalau Asset cek Asset Class
        if (id_ac == -1) {
          toast("Silahkan pilih Asset Class terlebih dahulu", Color.DANGER);
          return;
        }
        if (id_mp == -1) {
          toast("Silahkan pilih Metode Penyusutan terlebih dahulu", Color.DANGER);
          return;
        }
        /*if (masa_manfaat < 0 || masa_manfaat == "") {
          toast("Masa Manfaat belum diinput, silahkan input terlebih dahulu", Color.DANGER);
          return;
        }*/
      }

      if ($("#dd_ac").prop("disabled")) {
        id_ac = null;
      }
      if ($("#dd_mp").prop("disabled")) {
        id_mp = null;
      }
      
      var c = confirm("Apakah anda yakin?");
      if (c) {
        $.ajax({
          type: "POST",
          url: site_url + "other_material/insert",
          data: {
            ik: id_kat,
            im: id_mat,
            d: deskripsi,
            ia: id_ac,
            ic: id_cc,
            iv: id_vendor,
            ig: id_gl,
            nv: nama_vendor,
            mp: id_mp //,
            //mm: masa_manfaat
          },
          success: function(result) {
            toast(result, Color.SUCCESS);
            reset_form();
          }
        });
      }
    } else {
      toast("Ada data yang masih kosong, silahkan cek lagi", Color.DANGER);

    }

  }

  function reset_form() {
    $("#form_create").trigger("reset");
    $("#txt_kode_material").removeClass("green-text").addClass("red-text");
    $("#txt_desc_kode_gl").removeClass("green-text").addClass("red-text");
    $("#txt_desc_kode_cc").removeClass("green-text").addClass("red-text");
    $("#txt_desc_kode_cc").removeClass("green-text").addClass("red-text");
    $("#txt_nama_vendor").removeClass("green-text").addClass("red-text");

    cek_asset();
    $("#txt_nama_vendor").prop("disabled", true);
  }
</script>