<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Login_Controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//home
$route['home'] = 'Navigation_Controller';
$route['home/master'] = 'Navigation_Controller/master';

//karyawan
// $route['karyawan/upload_file']='Welcome/upload_data/karyawan';
// $route['karyawan/upload_absensi']='Welcome/upload_data/absensi';
$route['karyawan/get_by_nik'] = 'Karyawan_Controller/get_by_nik';
$route['karyawan/get_role_by_nik_login'] = 'Karyawan_Controller/get_role_by_nik_login';

//master
$route['home/master/karyawan'] = 'Navigation_Controller/master/karyawan';
$route['home/master/cutoff'] = 'Navigation_Controller/master/cutoff';
$route['home/master/kalender'] = 'Navigation_Controller/master/kalender';
$route['home/master/absensi'] = 'Navigation_Controller/master/absensi';
$route['home/master/s_jam_kerja'] = 'Navigation_Controller/master/standar_jam_kerja';
$route['home/master/s_insentif'] = 'Navigation_Controller/master/standar_insentif';

$route['home/master/material_fg'] = 'Navigation_Controller/master/material_fg';
$route['home/master/material_fg_sap'] = 'Navigation_Controller/master/material_fg_sap';
$route['home/master/material_rm_pm'] = 'Navigation_Controller/master/material_rm_pm';
$route['home/master/payterm'] = 'Navigation_Controller/master/payterm';
$route['home/master/vendor'] = 'Navigation_Controller/master/vendor';
$route['home/master/general_ledger'] = 'Navigation_Controller/master/general_ledger';
//$route['home/master/produksi'] = 'Navigation_Controller/master/produksi';

$route['master/material_fg'] = 'Master_Controller/material_fg';
$route['master/material_fg_sap'] = 'Master_Controller/material_fg_sap';
$route['master/material_rm_pm'] = 'Master_Controller/material_rm_pm';
$route['master/payterm'] = 'Master_Controller/payterm';
$route['master/vendor'] = 'Master_Controller/vendor';
$route['master/general_ledger'] = 'Master_Controller/general_ledger';


$route['master/karyawan'] = 'Master_Controller/karyawan';
$route['master/kalender'] = 'Master_Controller/kalender';
$route['master/absensi'] = 'Master_Controller/absensi';
$route['master/cutoff'] = 'Master_Controller/cutoff';
$route['master/today_info'] = 'Master_Controller/today_info';

//delete
// $route['delete/kalender'] = 'Welcome/delete/kalender';
// $route['delete/cutoff'] = 'Welcome/delete/cutoff';
// $route['delete/s_jam_kerja'] = 'Welcome/delete/standar_jam_kerja';
// $route['delete/material'] = 'Welcome/delete/material';

//form insert
// $route['insert_f/kalender'] = 'Welcome/insert_f/kalender';
// $route['insert_f/cutoff'] = 'Welcome/insert_f/cutoff';
// $route['insert_f/s_jam_kerja'] = 'Welcome/insert_f/standar_jam_kerja';
// $route['insert_f/karyawan'] = 'Welcome/insert_f/karyawan';
// $route['insert_f/s_insentif'] = 'Welcome/insert_f/standar_insentif';
// $route['insert_f/material'] = 'Welcome/insert_f/material';

//insert
// $route['insert/kalender'] = 'Welcome/insert/kalender';
// $route['insert/cutoff'] = 'Welcome/insert/cutoff';
// $route['insert/s_jam_kerja'] = 'Welcome/insert/standar_jam_kerja';
// $route['insert/karyawan'] = 'Welcome/insert/karyawan';
// $route['insert/s_insentif'] = 'Welcome/insert/standar_insentif';
// $route['insert/material'] = 'Welcome/insert/material';
// $route['insert/user_access'] = 'Welcome/insert/user_access';

//form laporan
// $route['laporan_f/absensi'] = 'Welcome/laporan_f/absensi';
// $route['laporan_f/gaji_absensi'] = 'Welcome/laporan_f/gaji_absensi';
// $route['laporan_f/absensi_tabel'] = 'Welcome/laporan_f/absensi_tabel';
// $route['laporan_f/rekap_gaji'] = 'Welcome/laporan_f/rekap_gaji';
// $route['laporan_f/rekap_performance'] = 'Welcome/laporan_f/rekap_performance';

//form rincian
// $route['rincian_f/absensi'] = 'Welcome/rincian_f/absensi';
// $route['rincian_f/absensi_performance'] = 'Welcome/rincian_f/absensi_performance';
// $route['rincian_f/slip_gaji'] = 'Welcome/rincian_f/slip_gaji';
// $route['rincian_f/karyawan_insentif'] = 'Welcome/rincian_f/karyawan_insentif';

//generate rincian
// $route['gen_rincian'] = 'Welcome/gen_rincian';

//gaji
// $route['home/gaji/slip_gaji']='Welcome/home/gaji/slip_gaji';
// $route['home/gaji/rekap_gaji']='Welcome/home/gaji/rekap_gaji';
// $route['home/gaji/karyawan_insentif']='Welcome/home/gaji/karyawan_insentif';
// $route['gen_slip_gaji_all_karyawan']='Welcome/gen_slip_gaji_all_karyawan';

//user access
$route['home/user_access']='Navigation_Controller/user_access';

$route['user_access/get_all'] = 'UserAccess_Controller/get';
$route['user_access/get_all_no_restriction'] = 'UserAccess_Controller/get_all_no_restriction';
$route['user_access/change_status'] = 'UserAccess_Controller/change_status';
$route['user_access/insert_form'] = 'UserAccess_Controller/insert_form';
$route['user_access/insert'] = 'UserAccess_Controller/insert_ua';

//performance
// $route['home/performance/rekap_performance']='Welcome/home/performance/rekap_performance';

//material
$route['home/finished/barang_masuk'] = 'Navigation_Controller/finished/barang_masuk';
$route['home/finished/barang_masuk_ws'] = 'Navigation_Controller/finished/barang_masuk_ws';
$route['home/finished/barang_masuk_adjustment'] = 'Navigation_Controller/finished/barang_masuk_adjustment';
$route['home/finished/barang_keluar'] = 'Navigation_Controller/finished/barang_keluar';
$route['home/finished/barang_keluar_adjustment'] = 'Navigation_Controller/finished/barang_keluar_adjustment';
$route['home/finished/stock_opname_history'] = 'Navigation_Controller/finished/stock_opname_history';
$route['home/finished/stock_history'] = 'Navigation_Controller/finished/stock_history';
$route['home/finished/stock_report'] = 'Navigation_Controller/finished/stock_report';
$route['home/finished/stock_opname'] = 'Navigation_Controller/finished/stock_opname';
$route['home/finished/stock_opname_template'] = 'Navigation_Controller/finished/stock_opname_template';
$route['home/finished/rekap_sj'] = 'Navigation_Controller/finished/rekap_sj';
$route['home/finished/rekap_sj/today_info'] = 'Navigation_Controller/finished/rekap_sj/today_info';
$route['home/finished/gudang_transit_report'] = 'Navigation_Controller/finished/gudang_transit_report';
$route['home/finished/change_nomor_sj'] = 'Navigation_Controller/finished/change_nomor_sj';

//$route['material/upload'] = 'FG_Controller/upload';
$route['material/get'] = 'FG_Controller/get';
$route['material/plus'] = 'FG_Controller/plus';
$route['material/get_filter'] = 'FG_Controller/get_filter';
$route['material/insert_fisik_stock'] = 'FG_Controller/insert_fisik_stock';

$route['material/upload_stock'] = 'FG_Controller/upload/u_stock';
$route['material/upload_stock_opname_tahunan'] = 'FG_Controller/upload/u_stock_tahunan';

$route['material/view_history_stock'] = 'FG_Controller/view_history_stock';
$route['material/view_history_stock_opname'] = 'FG_Controller/view_history_stock_opname';
$route['material/view_template_stock_opname'] = 'FG_Controller/view_template_stock_opname';
$route['material/view_stock'] = 'FG_Controller/view_stock';
$route['material/view_stock_by_rak'] = 'FG_Controller/view_stock_by_rak';
$route['material/view_stock_opname'] = 'FG_Controller/view_stock_opname';
$route['material/view_rekap_sj'] = 'FG_Controller/view_rekap_sj';
$route['material/view_sisa_transit'] = 'FG_Controller/view_sisa_transit';

//good receipt fg
$route['good_receipt/insert'] = 'GoodReceipt_Controller/insert_gr';
$route['good_receipt/insert_adjustment'] = 'GoodReceipt_Controller/insert_adjustment';
$route['good_receipt/get_view_by_sj'] = 'GoodReceipt_Controller/get_view_by_sj';

//good issue fg
$route['good_issue/insert'] = 'GoodIssue_Controller/insert_gi';
$route['good_issue/insert_adjustment'] = 'GoodIssue_Controller/insert_adjustment';
$route['good_issue/get_view_by_sj'] = 'GoodIssue_Controller/get_view_by_sj';

//material fg sap
$route['material_fg_sap/upload'] = 'Welcome/material_fg_sap/material_fg_sap_upload';

//material rm pm
$route['home/rmpm/barang_masuk'] = 'Navigation_Controller/rm_pm/barang_masuk';
$route['home/rmpm/barang_masuk_adjustment'] = 'Navigation_Controller/rm_pm/barang_masuk_adjustment';
$route['home/rmpm/barang_keluar'] = 'Navigation_Controller/rm_pm/barang_keluar';
$route['home/rmpm/barang_keluar_adjustment'] = 'Navigation_Controller/rm_pm/barang_keluar_adjustment';
$route['home/rmpm/stock_history'] = 'Navigation_Controller/rm_pm/stock_history';
$route['home/rmpm/stock_report'] = 'Navigation_Controller/rm_pm/stock_report';
$route['home/rmpm/stock_opname'] = 'Navigation_Controller/rm_pm/stock_opname';
$route['home/rmpm/document_report'] = 'Navigation_Controller/rm_pm/document_report';
$route['home/rmpm/create_pr'] = 'Navigation_Controller/rm_pm/create_pr';
$route['home/rmpm/stock_opname_history'] = 'Navigation_Controller/rm_pm/stock_opname_history';
$route['home/rmpm/rekap_po_ws'] = 'Navigation_Controller/rm_pm/rekap_po_ws';
$route['home/rmpm/rekap_po_ws/today_info'] = 'Navigation_Controller/rm_pm/rekap_po_ws/today_info';
$route['home/rmpm/change_nomor_sj'] = 'Navigation_Controller/rm_pm/change_nomor_sj';

$route['material_rm_pm/upload'] = 'RMPM_Controller/upload';
$route['material_rm_pm/upload_stock'] = 'RMPM_Controller/upload_stock';
$route['material_rm_pm/upload_stock_opname'] = 'RMPM_Controller/upload_stock_opname';
$route['material_rm_pm/view_history_stock'] = 'RMPM_Controller/get_view_history_stock';
$route['material_rm_pm/view_stock'] = 'RMPM_Controller/get_view_stock';
$route['material_rm_pm/view_rekap_po_ws'] = 'RMPM_Controller/get_view_rekap_po_ws';
$route['material_rm_pm/mutasi_masuk'] = 'RMPM_Controller/insert_gr_mutasi';
$route['material_rm_pm/mutasi_keluar'] = 'RMPM_Controller/insert_gi_mutasi';
$route['material_rm_pm/plus'] = 'RMPM_Controller/plus';
$route['material_rm_pm/insert_fisik_stock'] = 'RMPM_Controller/insert_fisik_stock';

$route['material_rm_pm/get'] = 'RMPM_Controller/get';
$route['material_rm_pm/get_all'] = 'RMPM_Controller/get_all';
$route['material_rm_pm/get_document_view_by_id'] = 'RMPM_Controller/document_get_view_by_id';
$route['material_rm_pm/view_stock_opname'] = 'RMPM_Controller/get_view_stock_opname';
$route['material_rm_pm/history_stock_opname'] = 'RMPM_Controller/get_view_history_stock_opname';

//kinerja
$route['home/rmpm/kinerja_report'] = 'Navigation_Controller/rm_pm/kinerja_report';
$route['home/rmpm/kinerja_insentif_price'] = 'Navigation_Controller/rm_pm/kinerja_insentif_price';
$route['home/rmpm/kinerja_vendor_evaluation_input'] = 'Navigation_Controller/rm_pm/kinerja_vendor_evaluation_input';
$route['home/rmpm/kinerja_vendor_evaluation_report'] = 'Navigation_Controller/rm_pm/kinerja_vendor_evaluation_report';
$route['home/finished/kinerja_report'] = 'Navigation_Controller/finished/kinerja_report';

$route['home/finished/kinerja/input_in'] = 'Navigation_Controller/finished/kinerja_input_in';
$route['home/finished/kinerja/input_out'] = 'Navigation_Controller/finished/kinerja_input_out';
$route['home/finished/kinerja/detail_surat_jalan'] = 'Navigation_Controller/finished/kinerja_detail_surat_jalan';
$route['home/finished/kinerja/ekspedisi_report'] = 'Navigation_Controller/finished/kinerja_ekspedisi_report';
$route['home/finished/kinerja/sisa_report'] = 'Navigation_Controller/finished/kinerja_sisa_report';
$route['home/finished/kinerja/kebersihan_report'] = 'Navigation_Controller/finished/kinerja_kebersihan_report';


$route['kinerja/rmpm/report'] = 'Kinerja_Controller/rmpm_view_report';
$route['kinerja/rmpm/view_harga_insentif'] = 'Kinerja_Controller/rm_pm_view_harga_insentif';

$route['kinerja/finished/view_report'] = 'Kinerja_Controller/fg_view_report';
$route['kinerja/finished/view_sisa'] = 'Kinerja_Controller/fg_kinerja_out_view_sisa';
$route['kinerja/finished/view_kebersihan'] = 'Kinerja_Controller/fg_view_kebersihan';
$route['kinerja/finished/view_rekap_sj'] = 'Kinerja_Controller/fg_view_rekap';
$route['kinerja/finished/view_ekspedisi_report'] = 'Kinerja_Controller/fg_ekspedisi_report';
$route['kinerja/finished/get_good_issue'] = 'Kinerja_Controller/fg_get_good_issue';
$route['kinerja/finished/get_good_receipt'] = 'Kinerja_Controller/fg_get_good_receipt';

$route['kinerja/finished/insert_good_issue'] = 'Kinerja_Controller/fg_insert_good_issue';
$route['kinerja/finished/insert_good_receipt'] = 'Kinerja_Controller/fg_insert_good_receipt';
$route['kinerja/finished/view_detail_sj_out'] = 'Kinerja_Controller/fg_view_detail_sj_out';
$route['kinerja/finished/view_detail_sj_in'] = 'Kinerja_Controller/fg_view_detail_sj_in';

//ekspedisi
$route['ekspedisi/upload'] = 'Welcome/upload_data/ekspedisi';
$route['ekspedisi/get_all'] = 'Welcome/ekspedisi/get_all_ekspedisi';

//gudang
$route['gudang/upload'] = 'Welcome/upload_data/gudang';
$route['gudang/get_all'] = 'Welcome/gudang/get_all_gudang';

//gudang_rm_pm
$route['gudang_rm_pm/upload'] = 'Welcome/upload_data/gudang_rm_pm';

//vendor
$route['vendor/upload'] = 'Vendor_Controller/upload';
$route['vendor/insert'] = 'Vendor_Controller/insert_vendor';
$route['vendor/update'] = 'Vendor_Controller/update_vendor';
$route['vendor/get'] = 'Vendor_Controller/get';
$route['vendor/get_all'] = 'Vendor_Controller/get_all';
$route['vendor/view_insert'] = 'Vendor_Controller/view_insert';
$route['vendor/view_update'] = 'Vendor_Controller/view_update';
$route['vendor/view_supply'] = 'Vendor_Controller/view_supply';

//Country
$route['country/upload'] = 'Country_Controller/upload';

//purchase_order
$route['home/purchase_order/cancel'] = 'Navigation_Controller/po/cancel';
$route['home/purchase_order/revisi'] = 'Navigation_Controller/po/revisi';
$route['home/purchase_order/rekap'] = 'Navigation_Controller/po/rekap';
$route['home/purchase_order/kurs_update'] = 'Navigation_Controller/po/kurs_update';
$route['home/purchase_order/kurs_history'] = 'Navigation_Controller/po/kurs_history';
$route['home/purchase_order/price_input'] = 'Navigation_Controller/po/price_input';
$route['home/purchase_order/price_history'] = 'Navigation_Controller/po/price_history';
$route['home/purchase_order/tutup'] = 'Navigation_Controller/po/tutup';
$route['home/purchase_order/create_new'] = 'Navigation_Controller/po/create_new';
$route['home/purchase_order/print'] = 'Navigation_Controller/po/print';

$route['purchase_order/upload'] = 'PO_Controller/upload';
$route['purchase_order/get_last_date'] = 'PO_Controller/get_last_date';
$route['purchase_order/get_cb_kolom_tampil'] = 'PO_Controller/get_cb_kolom_tampil';
$route['purchase_order/get_view_by_no'] = 'PO_Controller/get_view_by_no';
$route['purchase_order/get_view_revisi_by_no'] = 'PO_Controller/get_view_revisi_by_no';
$route['purchase_order/get_view_print_by_no'] = 'PO_Controller/get_view_print_by_no';
$route['purchase_order/get_sj_by_gr'] = 'PO_Controller/get_sj_by_gr';
$route['purchase_order/get_tabel_create'] = 'PO_Controller/get_tabel_create';
$route['purchase_order/view_report'] = 'PO_Controller/get_view_report';
$route['purchase_order/view_cancel_get_all'] = 'PO_Controller/get_view_cancel';
$route['purchase_order/view_detail_modal'] = 'PO_Controller/get_view_detail_modal';
$route['purchase_order/view_evaluation'] = 'PO_Controller/evaluation_get_view_insert';
$route['purchase_order/view_detail_modal_evaluation'] = 'PO_Controller/evaluation_get_view_detail_modal';
$route['purchase_order/view_evaluation_report'] = 'PO_Controller/evaluation_get_view_report';
$route['purchase_order/view_history_kurs'] = 'PO_Controller/kurs_get_view_history';
$route['purchase_order/view_history_price'] = 'PO_Controller/price_get_view_history';
$route['purchase_order/view_tutup'] = 'PO_Controller/get_view_tutup';
$route['purchase_order/insert'] = 'PO_Controller/insert_po';
$route['purchase_order/insert_evaluation'] = 'PO_Controller/evaluation_insert';
$route['purchase_order/insert_checker'] = 'PO_Controller/checker_insert';
$route['purchase_order/cancel'] = 'PO_Controller/cancel';
$route['purchase_order/revisi'] = 'PO_Controller/revisi';
$route['purchase_order/update_kurs'] = 'PO_Controller/kurs_update';
$route['purchase_order/price_get_active'] = 'PO_Controller/price_get_active';
$route['purchase_order/price_insert'] = 'PO_Controller/price_insert';
$route['purchase_order/change_nomor_sj'] = 'PO_Controller/change_nomor_sj';
$route['purchase_order/change_status'] = 'PO_Controller/change_status';
$route['purchase_order/email_send'] = 'PO_Controller/email_send';

//purchase request
$route['home/purchase_request/create_new'] = 'Navigation_Controller/pr/create_new';
$route['home/purchase_request/history'] = 'Navigation_Controller/pr/history';
$route['home/purchase_request/tutup'] = 'Navigation_Controller/pr/tutup';

$route['purchase_request/get_tabel_awal'] = 'PR_Controller/get_tabel_awal';
$route['purchase_request/insert'] = 'PR_Controller/insert_pr';
$route['purchase_request/view_history'] = 'PR_Controller/get_view_history';
$route['purchase_request/view_tutup'] = 'PR_Controller/get_view_tutup';
$route['purchase_request/change_status'] = 'PR_Controller/change_status';

//kebersihan
$route['kebersihan/insert'] = 'Kebersihan_Controller/harian_insert';

//forecast
$route['forecast/upload'] = 'Forecast_Controller/upload';

$route['home/forecast/history_report'] = 'Navigation_Controller/forecast/history';
$route['home/forecast/rmpm_analysis'] = 'Navigation_Controller/forecast/analysis';

$route['forecast/view_analysis'] = 'Forecast_Controller/view_analysis';
$route['forecast/view_history'] = 'Forecast_Controller/view_history';
$route['forecast/analysis_get_dd_by_tanggal'] = 'Forecast_Controller/analysis_get_dd_by_tanggal';

//work_sheet
$route['home/work_sheet/cancel'] = 'Navigation_Controller/ws/cancel';
$route['home/work_sheet/detail'] = 'Navigation_Controller/ws/detail';
$route['home/work_sheet/report_connect'] = 'Navigation_Controller/ws/report_connect';
$route['home/work_sheet/sisa'] = 'Navigation_Controller/ws/sisa';
$route['home/work_sheet/tutup'] = 'Navigation_Controller/ws/tutup';
$route['home/work_sheet/report_selisih'] = 'Navigation_Controller/ws/report_selisih';
$route['home/work_sheet/admin_proses'] = 'Navigation_Controller/ws/admin_proses';
$route['home/work_sheet/admin_proses_history'] = 'Navigation_Controller/ws/admin_proses_history';
$route['home/work_sheet/create_new'] = 'Navigation_Controller/ws/create_new';
$route['home/work_sheet/informasi'] = 'Navigation_Controller/ws/informasi';
$route['home/work_sheet/print'] = 'Navigation_Controller/ws/print';
$route['home/work_sheet/insentif_price'] = 'Navigation_Controller/ws/insentif_price';

$route['work_sheet/get_last_number'] = 'WS_Controller/get_last_number';
$route['work_sheet/upload'] = 'WS_Controller/upload';
$route['work_sheet/create_new'] = 'WS_Controller/create_new';
$route['work_sheet/get_view_by_no'] = 'WS_Controller/get_view_by_no';
$route['work_sheet/view_sisa'] = 'WS_Controller/get_view_sisa';
$route['work_sheet/view_tutup'] = 'WS_Controller/get_view_tutup';
$route['work_sheet/change_status'] = 'WS_Controller/change_status';
$route['work_sheet/insert_checker'] = 'WS_Controller/checker_insert';
$route['work_sheet/insert_admin_proses'] = 'WS_Controller/admin_proses_insert';
$route['work_sheet/adjustment_checker'] = 'WS_Controller/checker_adjustment';
$route['work_sheet/view_insert_checker'] = 'WS_Controller/get_view_checker_insert';
$route['work_sheet/view_adjust_checker'] = 'WS_Controller/get_view_checker_adjustment';
$route['work_sheet/view_cancel_get_all'] = 'WS_Controller/get_view_cancel';
$route['work_sheet/view_detail_modal'] = 'WS_Controller/get_view_detail_modal';
$route['work_sheet/view_detail'] = 'WS_Controller/get_view_detail';
$route['work_sheet/cancel'] = 'WS_Controller/cancel';
$route['work_sheet/view_report_selisih'] = 'WS_Controller/get_view_report_selisih';
$route['work_sheet/view_report_connect'] = 'WS_Controller/get_view_report_connect';
$route['work_sheet/view_admin_proses_detail'] = 'WS_Controller/get_view_admin_proses';
$route['work_sheet/view_checker_fg_gr'] = 'WS_Controller/get_view_fg_gr';
$route['work_sheet/checker_fg_insert_gr'] = 'WS_Controller/checker_fg_insert_gr';
$route['work_sheet/plus'] = 'WS_Controller/plus';

$route['work_sheet/get_view_print_by_no'] = 'WS_Controller/get_view_print_by_no';
$route['work_sheet/print_plus_count'] = 'WS_Controller/print_plus_count';
$route['work_sheet/view_informasi'] = 'WS_Controller/get_view_informasi';
$route['work_sheet/view_admin_proses_history'] = 'WS_Controller/get_view_admin_proses_history';

//Cost Center
$route['cc/upload'] = 'CC_Controller/upload';
$route['cc/get_all'] = 'CC_Controller/get_all';
$route['cc/get_by_id'] = 'CC_Controller/get_by_id';

//Other Asset Class
$route['other_ac/get_all'] = 'OtherAC_Controller/get_all';
$route['other_ac/get_gl_by_id'] = 'OtherAC_Controller/get_gl_by_id';

//Other Material
$route['home/other_material/create_new'] = 'Navigation_Controller/other/material/create_new';
$route['home/other_material/rekap'] = 'Navigation_Controller/other/material/rekap';
$route['home/other_material/stock_report'] = 'Navigation_Controller/other/material/stock_report';
$route['home/other_material/barang_masuk'] = 'Navigation_Controller/other/material/barang_masuk';
$route['home/other_material/barang_keluar'] = 'Navigation_Controller/other/material/barang_keluar';
$route['home/other_material/stock_history'] = 'Navigation_Controller/other/material/stock_history';

$route['other_material/get_last_code'] = 'OtherMaterial_Controller/get_last_code';
$route['other_material/insert'] = 'OtherMaterial_Controller/material_insert';
$route['other_material/view_rekap'] = 'OtherMaterial_Controller/get_view_rekap';
$route['other_material/view_history'] = 'OtherMaterial_Controller/get_view_history';
$route['other_material/view_stock'] = 'OtherMaterial_Controller/get_view_stock';
$route['other_material/get_cb_kolom_tampil'] = 'OtherMaterial_Controller/get_cb_kolom_tampil';
$route['other_material/history_get_cb_kolom_tampil'] = 'OtherMaterial_Controller/history_get_cb_kolom_tampil';
$route['other_material/get_by_id'] = 'OtherMaterial_Controller/get_by_id';

//Other Kategori
$route['other_kategori/get_all'] = 'OtherKategori_Controller/get_all';

//Other Metode Penyusutan
$route['other_metode_penyusutan/get_all'] = 'OtherMP_Controller/get_all';

//Other Purchase Order
$route['home/other_po/create_new'] = 'Navigation_Controller/other/po/create_new';

$route['other_po/insert_get_table_header'] = 'OtherPO_Controller/insert_get_table_header';
$route['other_po/insert_plus'] = 'OtherPO_Controller/insert_plus';
$route['other_po/insert'] = 'OtherPO_Controller/insert_po';
$route['other_po/view_report'] = 'OtherPO_Controller/get_view_report';
$route['other_po/get_view_by_no'] = 'OtherPO_Controller/get_view_by_no';
$route['other_po/insert_gr'] = 'OtherPO_Controller/gr_insert';

//Other Good Issue
$route['other_gi/get_table_header'] = 'OtherGI_Controller/get_table_header';
$route['other_gi/insert_plus'] = 'OtherGI_Controller/insert_plus';
//$route['other_gi/material_get_by_id'] = 'OtherGI_Controller/material_get_by_id';
$route['other_gi/insert'] = 'OtherGI_Controller/insert_gi';

//Payterm
$route['payterm/upload'] = 'Payterm_Controller/upload';
$route['payterm/get_by_id'] = 'Payterm_Controller/get_by_id';

//BOM
$route['home/bom/data'] = 'Navigation_Controller/bom/data';

$route['bom/view_modal_by_bom_alt'] = 'BOM_Controller/view_modal_by_bom_alt';
$route['bom/view_master'] = 'BOM_Controller/view_master';
$route['bom/upload'] = 'BOM_Controller/upload';
$route['bom/jd_qc_upload'] = 'BOM_Controller/jd_qc_upload';
$route['bom/get_desc_by_product_code'] = 'BOM_Controller/get_desc_by_product_code';
$route['bom/alt_get_opt'] = 'BOM_Controller/alt_get_opt';

$route['bom/pv_set_all_now'] = 'BOM_Controller/pv_set_all_now';

//Production Version
$route['home/pv/rekap'] = 'Navigation_Controller/pv/rekap';
$route['home/pv/create_new'] = 'Navigation_Controller/pv/create_new';

$route['pv/plus'] = 'PV_Controller/plus';
$route['pv/insert'] = 'PV_Controller/insert_pv';
$route['pv/view_rekap'] = 'PV_Controller/get_view_rekap';
$route['pv/semi_get_active_by_product_code'] = 'PV_Controller/semi_get_active_by_product_code';
$route['pv/fg_get_by_product_code'] = 'PV_Controller/fg_get_by_product_code';

//Settings
$route['home/setting/change_password'] = 'Navigation_Controller/setting/change_password';
$route['home/setting/user_access'] = 'Navigation_Controller/setting/user_access';

$route['settings/change_password'] = 'Settings_Controller/change_password';
$route['settings/change_nomor_sj'] = 'Settings_Controller/change_nomor_sj';

//General Ledger
$route['gl/upload'] = 'GL_Controller/upload';
$route['gl/get'] = 'GL_Controller/get';
$route['gl/view_table'] = 'GL_Controller/view_table';

//FOH
$route['home/foh/setting'] = 'Navigation_Controller/foh/setting';
$route['home/foh/history'] = 'Navigation_Controller/foh/history';

$route['foh/view_insert'] = 'FOH_Controller/view_insert';
$route['foh/view_update'] = 'FOH_Controller/view_update';
$route['foh/view_history'] = 'FOH_Controller/view_history';
$route['foh/view_update_get_by_id'] = 'FOH_Controller/view_update_get_by_id';
$route['foh/plus'] = 'FOH_Controller/plus';
$route['foh/insert'] = 'FOH_Controller/foh_insert';
$route['foh/update'] = 'FOH_Controller/foh_update';
$route['foh/validasi'] = 'FOH_Controller/foh_validasi';

//Payment Voucher
$route['home/payment_voucher/input'] = 'Navigation_Controller/payment_voucher/input';

$route['payment_voucher/plus_vendor'] = 'Payment_Controller/plus_vendor';
$route['payment_voucher/get_vendor_payment_detail'] = 'Payment_Controller/get_vendor_payment_detail';
$route['payment_voucher/view_preview'] = 'Payment_Controller/view_modal_preview';
$route['payment_voucher/insert'] = 'Payment_Controller/payment_insert';


//Payment Voucher Detail
$route['home/payment/rekap_gl'] = 'Navigation_Controller/payment/rekap_gl';
$route['home/payment/rekap'] = 'Navigation_Controller/payment/rekap';
$route['home/payment/jurnal_lain_input'] = 'Navigation_Controller/payment/jurnal_lain_input';

$route['home/payment_voucher/mrko/input'] = 'Navigation_Controller/payment_voucher/mrko/input';
$route['home/payment_voucher/detail/input'] = 'Navigation_Controller/payment_voucher/detail/input';

$route['payment_voucher/detail/plus'] = 'Payment_Controller/voucher_detail_plus';
$route['payment_voucher/detail/plus_tax'] = 'Payment_Controller/voucher_detail_plus_tax';
$route['payment_voucher/detail/insert'] = 'Payment_Controller/voucher_detail_insert';
$route['payment_voucher/detail/update'] = 'Payment_Controller/voucher_detail_update';
$route['payment_voucher/detail/view_preview'] = 'Payment_Controller/voucher_detail_view_modal_preview';

$route['payment_voucher/mrko/insert'] = 'Payment_Controller/jurnal_lain_insert';

$route['payment/view_rekap_gl'] = 'Payment_Controller/gl_view_rekap';
$route['payment/view_rekap'] = 'Payment_Controller/check_view_rekap';
$route['payment/cancel'] = 'Payment_Controller/check_cancel';
$route['payment/jurnal_lain_insert'] = 'Payment_Controller/jurnal_lain_insert';


//Other
// $route['update_karyawan_insentif']='Welcome/update_karyawan_insentif';
// $route['insert_karyawan_insentif']='Welcome/insert_karyawan_insentif';
// $route['insert_karyawan_obat']='Welcome/insert_karyawan_obat';
// $route['master'] = 'Library_Controller/master';
// $route['gen_laporan'] = 'Welcome/gen_laporan';
// $route['kenaikan_gaji'] = 'Welcome/kenaikan_gaji';
// $route['upload'] = 'Welcome/upload_file';
// $route['tes'] = 'Test_Controller';

$route['logout'] = 'Login_Controller/logout';
$route['login'] = 'Login_Controller/login';