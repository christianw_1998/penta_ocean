<?php
class Vendor{
	public static $TABLE_NAME = "vendor";
	public static $ID = "id_vendor";
	
	public static $NAMA = "nama";

	public static $ALAMAT = "alamat";
	public static $RT = "RT";
	public static $RW = "RW";
	public static $KECAMATAN = "kecamatan";
	public static $KELURAHAN = "kelurahan";
	public static $COUNTRY = "country";
	public static $KODE_POS = "kode_pos";
	public static $KOTA = "kota";
	public static $CURRENCY = "currency";
	public static $SALES_PERSON = "sales_person";
	public static $NO_TELP = "no_telp";
	public static $NPWP = "npwp";
	public static $EMAIL = "email";
	public static $BANK_ACCOUNT = "bank_account";
	public static $BANK_NAME = "bank_name";
	public static $TIPE = "tipe";

	//Foreign Key
	public static $ID_PAYTERM = "id_payterm";
	public static $ID_GL = "id_gl";

	public static $MESSAGE_SUCCESS_UPLOAD = "VNQ001";
	public static $MESSAGE_SUCCESS_INSERT = "VNQ002";
	public static $MESSAGE_SUCCESS_UPDATE = "VNQ003";
}
class Vendor_Tipe{
	public static $TABLE_NAME = "vendor_tipe";
	public static $ID = "id_vendor_tipe";
	
	public static $NAMA = "nama_vendor_tipe";
	public static $KODE_AWAL = "kode_awal_vendor_tipe";
}
class Vendor_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		$arr_vendor = array();
		$ctr = 0;

		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika ID Vendor tidak kosong
					$id_vendor = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$nama = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$alamat = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$negara = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					$kode_pos = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$kota = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$payterm = isset($data['values'][$i]["I"]) ? $data['values'][$i]["I"] : NULL;
					$currency = isset($data['values'][$i]["K"]) ? $data['values'][$i]["K"] : NULL;
					$sales_person = isset($data['values'][$i]["L"]) ? $data['values'][$i]["L"] : NULL;
					$no_telp = isset($data['values'][$i]["M"]) ? $data['values'][$i]["M"] : NULL;
					$npwp = isset($data['values'][$i]["O"]) ? $data['values'][$i]["O"] : NULL;
					$email = isset($data['values'][$i]["P"]) ? $data['values'][$i]["P"] : NULL;

					//Insert Tabel Vendor
					$this->db->where(Vendor::$ID, $id_vendor);
					$qry = $this->db->get(Vendor::$TABLE_NAME);

					$arr_vendor = [
						Vendor::$NAMA => strtoupper($nama),
						Vendor::$ALAMAT => strtoupper($alamat),
						Vendor::$COUNTRY => strtoupper($negara),
						Vendor::$KOTA => strtoupper($kota),
						Vendor::$CURRENCY => strtoupper($currency),
						Vendor::$SALES_PERSON => strtoupper($sales_person),
						Vendor::$NO_TELP => $no_telp
					];

					if($npwp != 0){
						$arr_vendor[Vendor::$NPWP] = $npwp;
					}
					if ($email != "xxx") {
						$arr_vendor[Vendor::$EMAIL] = $email;
					}
					if ($kode_pos != "") {
						$arr_vendor[Vendor::$KODE_POS] = $kode_pos;
					}
					//Cek Payterm 
					$this->db->where(Payterm::$ID, $payterm);
					$qry_payterm = $this->db->get(Payterm::$TABLE_NAME);
					if ($qry_payterm->num_rows() > 0) {
						$arr_vendor[Vendor::$ID_PAYTERM] = $payterm;
					}

					if ($qry->num_rows() > 0) { //Vendor sudah ada, update data
						$this->db->where(Vendor::$ID,$id_vendor);
						$this->db->update(Vendor::$TABLE_NAME,$arr_vendor);
					} else {
						$arr_vendor[Vendor::$ID] = $id_vendor;
						$this->db->insert(Vendor::$TABLE_NAME,$arr_vendor);
					}
				}
			}
		}
		return Vendor::$MESSAGE_SUCCESS_UPLOAD;
	}

	public function insert($nama, $alamat,$rt,$rw,$kecamatan,$kelurahan,$kota,$kode_pos,$negara,$currency,
                        $sales_person,$npwp,$no_telp,$email,$bank_account,$bank_name,$tipe,$id_payterm,$id_gl){

		$arr_vendor = [
			Vendor::$NAMA => strtoupper($nama),
			Vendor::$ALAMAT => strtoupper($alamat),
			Vendor::$COUNTRY => strtoupper($negara),
			Vendor::$KOTA => strtoupper($kota),
			Vendor::$CURRENCY => strtoupper($currency),
			Vendor::$SALES_PERSON => strtoupper($sales_person),
			Vendor::$NO_TELP => $no_telp,
			Vendor::$EMAIL => $email
		];
		$vendor_tipe=$this->get(Vendor_Tipe::$TABLE_NAME,Vendor_Tipe::$ID,$tipe,null,null,false)->row_array();
		$max = $this->db->query("select max(substr(id_vendor,3)) as max from " . Vendor::$TABLE_NAME . "
								where id_vendor like '". $vendor_tipe[Vendor_Tipe::$KODE_AWAL] ."%' ")->row_array()["max"];
		if ($max == NULL)
			$max = 0;
		$max += 1;
		$arr_vendor[Vendor::$ID] = $vendor_tipe[Vendor_Tipe::$KODE_AWAL] . str_pad($max, 4, "0", STR_PAD_LEFT);

		if($kode_pos != "" || $kode_pos != null){
			$arr_vendor[Vendor::$KODE_POS] = $kode_pos;
		}
		if ($kecamatan != "" || $kecamatan != null) {
			$arr_vendor[Vendor::$KECAMATAN] = strtoupper($kecamatan);
		}
		if ($kelurahan != "" || $kelurahan != null) {
			$arr_vendor[Vendor::$KELURAHAN] =  strtoupper($kelurahan);
		}
		if ($rt != "" || $rt != null) {
			$arr_vendor[Vendor::$RT] = $rt;
		}
		if ($rw != "" || $rw != null) {
			$arr_vendor[Vendor::$RW] = $rw;
		}
		if ($npwp != "" || $npwp != null) {
			$arr_vendor[Vendor::$NPWP] = $npwp;
		}
		if ($bank_account != "" || $bank_account != null) {
			$arr_vendor[Vendor::$BANK_ACCOUNT] = $bank_account;
		}
		if ($bank_name != "" || $bank_name != null) {
			$arr_vendor[Vendor::$BANK_NAME] = $bank_name;
		}
		if ($id_payterm != "" || $id_payterm != null) {
			$arr_vendor[Vendor::$ID_PAYTERM] = $id_payterm;
		}
		if ($id_gl != "" || $id_gl != null) {
			$arr_vendor[Vendor::$ID_GL] = $id_gl;
		}
		$arr_vendor[Vendor::$TIPE] = $tipe;
		
		$this->db->insert(Vendor::$TABLE_NAME,$arr_vendor);
		return Vendor::$MESSAGE_SUCCESS_INSERT;
	}
	public function update($id_vendor, $nama, $alamat,$rt,$rw,$kecamatan,$kelurahan,$kota,$kode_pos,$negara,$currency,
                        $sales_person,$npwp,$no_telp,$email,$bank_account,$bank_name,$id_payterm,$id_gl){

		if ($kode_pos == "") {
			$kode_pos = null;
		}
		if ($kecamatan == "") {
			$kecamatan = null;
		}
		if ($kelurahan == "") {
			$kelurahan = null;
		}
		if ($rt == "") {
			$rt = null;
		}
		if ($rw == "") {
			$rw = null;
		}
		if ($npwp == "") {
			$npwp = null;
		}
		if ($bank_account == "") {
			$bank_account = null;
		}
		if ($bank_name == "") {
			$bank_name = null;
		}
		if ($id_payterm == "") {
			$id_payterm = null;
		}
		if ($id_gl == "") {
			$id_gl = null;
		}
		
		$arr_vendor = [
			Vendor::$NAMA => strtoupper($nama),
			Vendor::$ALAMAT => strtoupper($alamat),
			Vendor::$COUNTRY => strtoupper($negara),
			Vendor::$KOTA => strtoupper($kota),
			Vendor::$CURRENCY => strtoupper($currency),
			Vendor::$SALES_PERSON => strtoupper($sales_person),
			Vendor::$NO_TELP => $no_telp,
			Vendor::$EMAIL => $email,
			Vendor::$KODE_POS => $kode_pos,
			Vendor::$KECAMATAN => $kecamatan,
			Vendor::$KELURAHAN => $kelurahan,
			Vendor::$RT => $rt,
			Vendor::$RW => $rw,
			Vendor::$NPWP => $npwp,
			Vendor::$BANK_ACCOUNT => $bank_account,
			Vendor::$BANK_NAME => $bank_name,
			Vendor::$ID_PAYTERM => $id_payterm,
			Vendor::$ID_GL => $id_gl
		];
		
		$this->db->where(Vendor::$ID,$id_vendor);
		$this->db->update(Vendor::$TABLE_NAME,$arr_vendor);
		return Vendor::$MESSAGE_SUCCESS_UPDATE;
	}
	public function get_supply(){
		$select=[
			Vendor::$TABLE_NAME . "." . Vendor::$ID . " " . Vendor::$ID,Vendor::$NAMA,
			"GROUP_CONCAT(DISTINCT material_rm_pm.kode_material ORDER BY material_rm_pm.kode_material SEPARATOR ', ') AS ". Material_RM_PM::$KODE_MATERIAL
		];
		$this->db->select($select);
		$this->db->from(Vendor::$TABLE_NAME);

		$join = Vendor::$TABLE_NAME . "." . Vendor::$ID . "=" . Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$ID_VENDOR;
		$this->db->join(Purchase_Order_Awal::$TABLE_NAME, $join);

		$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . "=" . Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$ID_MATERIAL;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);

		$this->db->group_by(Vendor::$TABLE_NAME.".". Vendor::$ID);
		return $this->db->get();
	}
}
