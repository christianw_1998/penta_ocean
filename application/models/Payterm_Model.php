<?php

class Payterm{
	public static $TABLE_NAME = "payterm";
	public static $ID = "id_payterm";

	public static $DESKRIPSI = "deskripsi";
	public static $DURASI = "durasi";
	public static $IS_DELETED = "is_deleted";

}

class Payterm_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { 
					$id = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$deskripsi = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$durasi = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;

					//Insert Tabel GL
					$data_query = array(
						Payterm::$ID => $id,
						Payterm::$DESKRIPSI => strtoupper($deskripsi),
						Payterm::$DURASI => $durasi,
						Payterm::$IS_DELETED => 0
					);

					$this->db->where(Payterm::$ID, $id);
					$qry = $this->db->get(Payterm::$TABLE_NAME);
					if ($qry->num_rows() > 0) { //Update PO
						//$this->db->where(Purchase_Order::$ID,$nomor_po);
						//$this->db->update(Purchase_Order::$TABLE_NAME,$data_query);
					} else {
						$this->db->insert(Payterm::$TABLE_NAME, $data_query);
					}
				}
			}
		}
	}
}
