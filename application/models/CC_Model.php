<?php
class Cost_Center{
	public static $TABLE_NAME = "cost_center";
	public static $ID = "id_cc";
	
	public static $ID_OLD = "id_cc_old";
	public static $NAMA = "nama_cc";
	public static $CCTC = "cctc";
	public static $CTR = "ctr";
	public static $VALID_FROM = "valid_from";
	public static $VALID_TO = "valid_to";
}
class CC_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}
	public function update_batch($data){
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika CC tidak kosong
					$nama = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$cctc = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$id_old = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;
					$id = isset($data['values'][$i]["H"]) ? $data['values'][$i]["H"] : NULL;
					$valid_from = isset($data['values'][$i]["I"]) ? $data['values'][$i]["I"] : NULL;
					$valid_to = isset($data['values'][$i]["J"]) ? $data['values'][$i]["J"] : NULL;

					//Insert Tabel GL
					$data_query = array(
						Cost_Center::$NAMA => $nama,
						Cost_Center::$CCTC => $cctc,
						Cost_Center::$ID => $id,
						Cost_Center::$ID_OLD => $id_old,
						Cost_Center::$VALID_FROM => date("Y-m-d", strtotime($valid_from)),
						Cost_Center::$VALID_TO => date("Y-m-d", strtotime($valid_to)),
					);


					$this->db->insert(Cost_Center::$TABLE_NAME, $data_query);
				}
			}
		}
	}
	public function get_active()
	{
		$today = date("Y-m-d");
		$this->db->where(Cost_Center::$VALID_FROM . ' <=', $today, TRUE);
		$this->db->where(Cost_Center::$VALID_TO . ' >=', $today, TRUE);
		$this->db->where($this::$IS_DELETED, 0);
		return $this->db->get(Cost_Center::$TABLE_NAME);
	}
}
