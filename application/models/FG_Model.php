<?php
class Material{
	public static $TABLE_NAME = "material";
	public static $ID = "id_material";

	public static $ID_NEW = "id_material_new";
	public static $MAT_GROUP = "mat_group";
	public static $PRODUCT_CODE = "product_code";
	public static $DESCRIPTION = "description";
	public static $MAT_TYPE = "mat_type";
	public static $PACKING = "packing";
	public static $BOX = "box";
	public static $NET = "net";
	public static $GROSS = "gross";
	public static $KUBIK = "kubik";
	public static $MEMO = "memo";
	public static $KODE_BARANG = "kode_barang";
	public static $KEMASAN = "kemasan";
	public static $WARNA = "warna";
	public static $IS_DELETED="is_deleted";

	public static $MESSAGE_SUCCESS_DELETE = "MAQ001";
	public static $MESSAGE_FAILED_DELETE = "MAQ002";
}
class Material_FG_SAP{
	public static $TABLE_NAME = "material_fg_sap";
	public static $ID = "id_material";
	public static $MAT_GROUP = "mat_group";
	public static $PRODUCT_MEMO = "product_memo";
	public static $DESCRIPTION = "description";
	public static $PACKING = "packing";
	public static $BOX = "box";
	public static $NET = "net";
	public static $GROSS = "gross";

	//Foreign Key
	public static $ID_SAGE = "id_material_sage";

	public static $MESSAGE_SUCCESS_UPLOAD = "MFSQ001";
}
class Gudang{
	public static $TABLE_NAME = "gudang";
	public static $ID = "id_gudang";

	public static $NAMA_GUDANG = "nama_gudang";
	public static $RAK = "rak";
	public static $KOLOM = "kolom";
	public static $TINGKAT = "tingkat";
	public static $KAPASITAS = "kapasitas";

	//Static Variable
	public static $S_CADANGAN = "CADANGAN";
	public static $S_TRANSIT = "TRANSIT";

	//Foreign Key
	public static $ID_MATERIAL = "id_material";
}
class Fifo_Stock_Gudang{
	public static $TABLE_NAME = "fifo_stock_gudang";
	public static $ID = "id_fifo";
	public static $QTY = "qty";

	//Foreign Key
	public static $ID_MATERIAL = "id_material";
	public static $ID_GUDANG = "id_gudang";

	//Static Variable
	public static $S_TERISI = "kapasitas_terisi";

	public static $MESSAGE_SUCCESS_UPLOAD = "FSGQ001";
	public static $MESSAGE_SUCCESS_UPLOAD_STOCK_OPNAME = "FSGQ002";
}
class Stock_Gudang{
	public static $TABLE_NAME="stock_gudang";
	public static $ID= "id_stock_gudang";

	public static $TANGGAL="tanggal";
	public static $STOCK_AWAL="stock_awal";
	public static $KREDIT="kredit";
	public static $DEBIT="debit";
	public static $STOCK_AKHIR="stock_akhir";
	public static $PESAN="pesan";
	public static $IS_DELETED="is_deleted";

	//Foreign Key
	public static $ID_GUDANG="id_gudang";
	public static $ID_MATERIAL="id_material";

	public static function GEN_CALIBRATION_MESSAGE(){
		return "Upload Stock Opname Tahunan";
	}
	public static function GEN_MOVE_FROM_CADANGAN_MESSAGE(){
		return "Pemindahan dari Rak Cadangan";
	}
	public static function GEN_MOVE_TO_RAK_MESSAGE($gudang){
		return "Pemindahan ke Gudang ".$gudang;
	}
	public static function GEN_MOVE_MATERIAL_FROM_CADANGAN_MESSAGE($material,$qty,$to_rak){
		return "Pindahkan $material sebanyak $qty ke rak $to_rak";
	}
}
class Material_Fisik{
	public static $TABLE_NAME = "material_fisik";
	public static $ID = "id_material_fisik";
	public static $QTY_FISIK = "qty_fisik";
	public static $QTY_PROGRAM = "qty_program";
	public static $TANGGAL = "tanggal";

	//Foreign Key
	public static $ID_MATERIAL = "id_material";
	public static $NIK = "nik";

	public static $MESSAGE_SUCCESS_INSERT = "MFIQ001";
}
class FG_Stock_Opname{
	public static $S_PCS = "pcs";
	public static $S_QTY_DUS = "qty_dus";
	public static $S_QTY_TIN = "qty_tin";

}
class FG_Model extends Library_Model {
	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		$kal = "SUKSES";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$fg_code = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$net = isset($data['values'][$i]["P"]) ? $data['values'][$i]["P"] : NULL;
					$gross = isset($data['values'][$i]["Q"]) ? $data['values'][$i]["Q"] : NULL;


					$data_query = array(
						Material::$GROSS => $gross,
						Material::$NET => $net
					);


					//INSERT MATERIAL RM PM
					$this->db->where(Material::$ID, $fg_code);
					$this->db->update(Material::$TABLE_NAME,$data_query);
				}
			}
		}
		return $kal;
	}
	public function stock_update_batch($data){
		//kosongkan semua stock
		$this->db->empty_table(Fifo_Stock_Gudang::$TABLE_NAME);

		for ($i = 0; $i < count($data['values']) + 100; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$id_material = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$qty = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;

					$qry_material = $this->get(Material::$TABLE_NAME,Material::$ID,$id_material);

					if ($qry_material->num_rows() > 0) {
						$material = $qry_material->row_array();

						$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $material[Material::$ID]);
						$stock_in = $qty;
						if ($qry_gudang->num_rows() > 0) {
							foreach ($qry_gudang->result_array() as $row_gudang) {
								if ($stock_in <= 0) {
									break;
								} else {
									$kapasitas_total = $row_gudang[Gudang::$KAPASITAS];
									//if ($box>0) { //jika tidak 0 berarti kapasitas masih box, harus dijadikan tin
									$kapasitas_total *= $material[Material::$BOX];
									//}
									$kapasitas_terisi = $this->stock_get_by($id_material, $row_gudang[Gudang::$ID]);
									$kapasitas_sisa = $kapasitas_total - $kapasitas_terisi;

									if ($kapasitas_sisa > 0) {
										$total_tin = 0;
										if ($kapasitas_sisa - $stock_in >= 0) //jika kapasitas masih mencukupi
											$total_tin = $stock_in;
										else
											$total_tin = $kapasitas_sisa;

										$stock_in -= $total_tin;


										$id_gudang_loop = $row_gudang[Gudang::$ID];
										//Fifo Stock Gudang
										//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
										$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
										$this->db->order_by(Fifo_Stock_Gudang::$ID, "desc");

										$qry_fifo = $this->db->get(Fifo_Stock_Gudang::$TABLE_NAME);
										$id_gudang = "";

										if ($qry_fifo->num_rows() > 0) {
											$fifo = $qry_fifo->row_array();
											$id_gudang = $fifo[Fifo_Stock_Gudang::$ID_GUDANG];
											$arr_fifo = [];
											
										}
										if ($id_gudang != $id_gudang_loop) {
											$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $id_material;
											$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
											$arr_fifo[Fifo_Stock_Gudang::$QTY] = $total_tin;
											$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
										} else {
											$this->db->where(Fifo_Stock_Gudang::$ID, $fifo[Fifo_Stock_Gudang::$ID]);
											$arr_fifo[Fifo_Stock_Gudang::$QTY] = $fifo[Fifo_Stock_Gudang::$QTY] + $total_tin;
											$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
										}
										
									}
								}
							}
							if ($stock_in > 0) {
								//Fifo Stock Gudang
								//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
								$this->db->where(Gudang::$RAK, Gudang::$S_CADANGAN);
								$this->db->where(Gudang::$NAMA_GUDANG, $qry_gudang->row_array()[Gudang::$NAMA_GUDANG]);
								$qry_temp_gudang = $this->db->get(Gudang::$TABLE_NAME);
								$id_gudang_loop = $qry_temp_gudang->row_array()[Gudang::$ID];

								$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
								$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG, $id_gudang_loop);
								$qry_fifo = $this->db->get(fifo_stock_gudang::$TABLE_NAME);

								$arr_fifo = [];
								if ($qry_fifo->num_rows() == 0) {
									$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $id_material;
									$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $stock_in;
									$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								} else {
									$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo->row_array()[Fifo_Stock_Gudang::$ID]);
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo->row_array()[Fifo_Stock_Gudang::$QTY] + $stock_in;
									$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								}
							}
						} else { //cek material yang tidak ada gudang
							echo $id_material . "- GUDANG TIDAK ADA<br>";
						}
					} else { //cek material yang tidak ada
						echo $id_material . "- MATERIAL TIDAK ADA<br>";
					}
				} else
					break;
			}
		}
		return Fifo_Stock_Gudang::$MESSAGE_SUCCESS_UPLOAD;
	}
	public function stock_tahunan_update_batch($data){
		for ($i = 0; $i < count($data['values']) + 100; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$id_material = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$program = $this->stock_get_by($id_material);
					$fisik = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;

					$qry_material = $this->get(Material::$TABLE_NAME,Material::$ID,$id_material);
					if ($qry_material->num_rows() > 0) {
						$material = $qry_material->row_array();

						if (($fisik != NULL || $fisik >= 0)) { //program tidak perlu dicek karena sudah di cek pada method get_stock_by
							$fisik = intval($fisik);
							$program = intval($program);
							$varian = $fisik - $program;

							//Insert Material Fisik
							$arr_material_fisik = [];
							//$this->db->where(Material_Fisik::$ID_MATERIAL,$id_material);
							$arr_material_fisik[Material_Fisik::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA]['nik'];
							$arr_material_fisik[Material_Fisik::$QTY_FISIK] = $fisik;
							$arr_material_fisik[Material_Fisik::$QTY_PROGRAM] = $this->stock_get_by($material[Material::$ID]);
							$this->db->set(Material_Fisik::$TANGGAL, 'NOW()', FALSE);
							$arr_material_fisik[Material_Fisik::$ID_MATERIAL] = $id_material;
							$this->db->insert(Material_Fisik::$TABLE_NAME, $arr_material_fisik);

							if ($fisik > $program || $varian > 0) { //Barang Masuk
								$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $id_material);
								$stock_in = abs(intval($varian));
								if ($qry_gudang->num_rows() > 0) {
									foreach ($qry_gudang->result_array() as $row_gudang) {
										if ($stock_in <= 0) {
											break;
										} else {
											$kapasitas_total = $row_gudang[Gudang::$KAPASITAS];
											//if($box>0){ //jika tidak 0 berarti kapasitas masih box, harus dijadikan tin
											$kapasitas_total *= $material[Material::$BOX];
											//}
											$kapasitas_terisi = $this->stock_get_by($id_material, $row_gudang[Gudang::$ID]);
											$kapasitas_sisa = $kapasitas_total - $kapasitas_terisi;

											if ($kapasitas_sisa > 0) {
												$total_tin = 0;
												if ($kapasitas_sisa - $stock_in >= 0) //jika kapasitas masih mencukupi
													$total_tin = $stock_in;
												else
													$total_tin = $kapasitas_sisa;

												$stock_in -= $total_tin;
												$id_gudang_loop = $row_gudang[Gudang::$ID];

												//Penambahan Stock Gudang
												$this->insert_history_stock($id_material, $id_gudang_loop, $total_tin, 0, Stock_Gudang::GEN_CALIBRATION_MESSAGE());

												//Fifo Stock Gudang
												//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
												$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
												$this->db->order_by(Fifo_Stock_Gudang::$ID, "desc");

												$qry_fifo = $this->db->get(Fifo_Stock_Gudang::$TABLE_NAME);
												$id_gudang = "";

												if ($qry_fifo->num_rows() > 0) {
													$fifo = $qry_fifo->row_array();
													$id_gudang = $fifo[Fifo_Stock_Gudang::$ID_GUDANG];
													$arr_fifo = [];
												}
												if ($id_gudang != $id_gudang_loop) {
													$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $id_material;
													$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
													$arr_fifo[Fifo_Stock_Gudang::$QTY] = $total_tin;
													$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
												} else {
													$this->db->where(Fifo_Stock_Gudang::$ID, $fifo[Fifo_Stock_Gudang::$ID]);
													$arr_fifo[Fifo_Stock_Gudang::$QTY] = $fifo[Fifo_Stock_Gudang::$QTY] + $total_tin;
													$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
												}
												
											}
										}
									}
									if ($stock_in > 0) {
										$this->db->where(Gudang::$RAK, Gudang::$S_CADANGAN);
										$this->db->where(Gudang::$NAMA_GUDANG, $qry_gudang->row_array()[Gudang::$NAMA_GUDANG]);
										$qry_temp_gudang = $this->db->get(Gudang::$TABLE_NAME);
										$id_gudang_loop = $qry_temp_gudang->row_array()[Gudang::$ID];

										//Penambahan Stock Gudang
										$this->insert_history_stock($id_material, $id_gudang_loop, $total_tin, 0, Stock_Gudang::GEN_CALIBRATION_MESSAGE());

										//Fifo Stock Gudang
										//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
										$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
										$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG, $id_gudang_loop);
										$qry_fifo = $this->db->get(Fifo_Stock_Gudang::$TABLE_NAME);

										$arr_fifo = [];
										if ($qry_fifo->num_rows() == 0) {
											$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $id_material;
											$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
											$arr_fifo[Fifo_Stock_Gudang::$QTY] = $stock_in;
											$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
										} else {
											$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo->row_array()[Fifo_Stock_Gudang::$ID]);
											$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo->row_array()[Fifo_Stock_Gudang::$QTY] + $stock_in;
											$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
										}
									}
								}
							} else if ($fisik < $program || $varian < 0) { //Barang Keluar
								$stock_out = abs(intval($varian));
								$mode = "";
								do {
									$this->db->from(Fifo_Stock_Gudang::$TABLE_NAME);
									if (!$this->equals($mode, Gudang::$S_CADANGAN)) {
										$this->db->select("fifo_stock_gudang.id_gudang id_gudang,id_fifo,qty");
										$this->db->join(Gudang::$TABLE_NAME, "fifo_stock_gudang.id_gudang=gudang.id_gudang");
										$this->db->where(Gudang::$RAK . " !=", Gudang::$S_CADANGAN);
									}
									$this->db->where(Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
									$this->db->where(Fifo_Stock_Gudang::$QTY . " !=", 0);

									$qry_fifo = $this->db->get();
									foreach ($qry_fifo->result_array() as $row_fifo) {
										$this->db->set(Stock_Gudang::$TANGGAL, 'NOW()', FALSE);
										$selisih = 0;
										if ($row_fifo[Fifo_Stock_Gudang::$QTY] - $stock_out >= 0) { //jika stock di gudang masih cukup
											$selisih = $stock_out;
											$stock_out = 0;
										} else {
											$selisih = $row_fifo[Fifo_Stock_Gudang::$QTY];
											$stock_out -= $selisih;
										}

										//Pengurangan Stock Gudang
										$this->insert_history_stock($id_material, $row_fifo[Fifo_Stock_Gudang::$ID_GUDANG], 0, $selisih, Stock_Gudang::GEN_CALIBRATION_MESSAGE());

										//Fifo Stock Gudang
										$arr_fifo = [];
										$arr_fifo[Fifo_Stock_Gudang::$QTY] = $row_fifo[Fifo_Stock_Gudang::$QTY] - $selisih;
										$this->db->where(Fifo_Stock_Gudang::$ID, $row_fifo[Fifo_Stock_Gudang::$ID]);
										$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);

										if ($stock_out <= 0) {
											break;
										}
									}
									if ($stock_out > 0)
										$mode = Gudang::$S_CADANGAN;
								} while ($stock_out > 0);
								//hapus semua fifo yang qty 0
								$this->db->where(Fifo_Stock_Gudang::$QTY, 0);
								$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

								//Pengecekan stock 0 pada gudang untuk pemindahan material
								$qry_gudang = $this->db->query("select fifo_stock_gudang.id_material id_material,gudang.id_gudang id_gudang,qty,id_fifo from gudang,fifo_stock_gudang where gudang.rak='CADANGAN' and qty>0 and gudang.id_gudang=fifo_stock_gudang.id_gudang");

								if ($qry_gudang->num_rows() > 0) {
									foreach ($qry_gudang->result_array() as $row) {
										$stock_now = $row[Fifo_Stock_Gudang::$QTY];
										$id_material = $row[Fifo_Stock_Gudang::$ID_MATERIAL];
										$qry_fifo = $this->db->query("select id_gudang,nama_gudang,id_material,kapasitas,rak,kolom,tingkat from gudang where gudang.id_gudang not in (select id_gudang from fifo_stock_gudang) and gudang.id_material=" . $id_material);
										$this->db->where(Material::$ID, $row[Fifo_Stock_Gudang::$ID_MATERIAL]);
										$material = $this->db->get(Material::$TABLE_NAME)->row_array();
										$box = $material[Material::$BOX];
										if ($qry_fifo->num_rows() > 0) {
											foreach ($qry_fifo->result_array() as $row2) {
												$kapasitas = $row2[Gudang::$KAPASITAS];

												//if($box!=0){
												$kapasitas *= $box;
												//}
												$stock_move = 0;
												if ($kapasitas - $stock_now >= 0) { //jika kapasitas masih cukup
													$stock_move = $stock_now;
												} else {
													$stock_move = $kapasitas;
												}

												//Pemindahan Barang dari Cadangan
												$gudang_to = $row2[Gudang::$NAMA_GUDANG] . ' Rak ' . $row2[Gudang::$RAK] . $row2[Gudang::$KOLOM] . ' ' . $row2[Gudang::$TINGKAT];
												$this->insert_history_stock($id_material, $row[Gudang::$ID], 0, $stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

												//Pemindahan Barang ke Rak
												$this->insert_history_stock($id_material, $row2[Gudang::$ID], $stock_move, 0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());

												$stock_now -= $stock_move;
												$arr_fifo_move_from = [];
												$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY] = $stock_now;
												$this->db->where(Fifo_Stock_Gudang::$ID, $row[Fifo_Stock_Gudang::$ID]);
												$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_from);

												$arr_fifo_move_to = [];
												$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL] = $row2[Gudang::$ID_MATERIAL];
												$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row2[Gudang::$ID];
												$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
												$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_to);
											}
										}
									}
								}
							}
						}
					}
				} else
					break;
			}
		}
		return Fifo_Stock_Gudang::$MESSAGE_SUCCESS_UPLOAD_STOCK_OPNAME;
	}
	public function insert_history_stock($id_material, $id_gudang, $debit, $kredit, $pesan){
		$arr_stock = [];
		$arr_stock[Stock_Gudang::$IS_DELETED] = 0;
		$arr_stock[Stock_Gudang::$ID_GUDANG] = $id_gudang;
		$arr_stock[Stock_Gudang::$STOCK_AWAL] = $this->stock_get_by($id_material, $id_gudang);
		$arr_stock[Stock_Gudang::$DEBIT] = $debit;
		$arr_stock[Stock_Gudang::$KREDIT] = $kredit;
		$arr_stock[Stock_Gudang::$ID_MATERIAL] = $id_material;
		$arr_stock[Stock_Gudang::$STOCK_AKHIR] = $arr_stock[Stock_Gudang::$STOCK_AWAL] + $arr_stock[Stock_Gudang::$DEBIT] - $arr_stock[Stock_Gudang::$KREDIT];
		$arr_stock[Stock_Gudang::$PESAN] = $pesan;
		$this->db->set(Stock_Gudang::$TANGGAL, 'NOW()', FALSE);
		$this->db->insert(Stock_Gudang::$TABLE_NAME, $arr_stock);
	}
	
	public function get_all_sisa_transit(){
		$arr_select = [
			Material::$PRODUCT_CODE, Material::$DESCRIPTION, Gudang_RM_PM_Transit::$QTY,
			Gudang_RM_PM_Transit::$TANGGAL_GENERATE, Material::$BOX, Worksheet_Connect::$NO_FG
		];

		$this->db->select($arr_select);
		$this->db->from(Gudang_RM_PM_Transit::$TABLE_NAME);
		$this->db->join(Material::$TABLE_NAME, Material::$TABLE_NAME . "." . Material::$ID . "=" . Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_MATERIAL_FG);
		$this->db->join(Worksheet_Connect::$TABLE_NAME, Worksheet_Connect::$TABLE_NAME . "." . Worksheet_Connect::$ID . "=" . Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_CONNECT_WS);
		$this->db->where(Gudang_RM_PM_Transit::$IS_DONE, 0);
		return $this->db->get();
	}
	public function get_distinct($tipe, $kode_barang, $kemasan){
		$this->db->select("distinct($tipe) as value");
		$this->db->where(Material::$IS_DELETED, 0);
		$this->db->where($tipe . Library_Model::$WHERE_IS_NOT_NULL, NULL, FALSE);

		if ($kode_barang != "")
			$this->db->where(Material::$KODE_BARANG, $kode_barang);
		if ($kemasan != "")
			$this->db->where(Material::$KEMASAN, $kemasan);
		$this->db->order_by("1");

		return $this->db->get(Material::$TABLE_NAME);
	}
	public function get_by_hirarki($kode_barang = NULL, $kemasan = NULL, $warna = NULL){
		if ($kode_barang != NULL)
			$this->db->where("UPPER(" . Material::$KODE_BARANG . ")", strtoupper($kode_barang));
		if ($kemasan != NULL)
			$this->db->where("UPPER(" . Material::$KEMASAN . ")", strtoupper($kemasan));
		if ($warna != NULL)
			$this->db->where("UPPER(" . Material::$WARNA . ")", strtoupper($warna));
		$this->db->where($this::$IS_DELETED, 0);
		
		return $this->db->get(Material::$TABLE_NAME);
	}
	public function get_history_stock($nomor_sj, $dt, $st, $kode_barang, $kemasan, $warna, $gudang){
		$arr = [
			Stock_Gudang::$ID,
			Stock_Gudang::$TABLE_NAME . "." . Stock_Gudang::$ID_MATERIAL . " " . Stock_Gudang::$ID_MATERIAL,
			Stock_Gudang::$TANGGAL, Gudang::$TABLE_NAME . "." . Gudang::$ID . " " . Gudang::$ID,
			Stock_Gudang::$STOCK_AWAL, Stock_Gudang::$DEBIT, Stock_Gudang::$KREDIT, Stock_Gudang::$STOCK_AKHIR,
			Stock_Gudang::$PESAN
		];
		$this->db->select($arr);
		$this->db->from(Stock_Gudang::$TABLE_NAME);
		
		$join = Stock_Gudang::$TABLE_NAME . "." . Stock_Gudang::$ID_MATERIAL . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $join);

		$join = Stock_Gudang::$TABLE_NAME . "." . Stock_Gudang::$ID_GUDANG . "=" . Gudang::$TABLE_NAME . "." . Gudang::$ID;
		$this->db->join(Gudang::$TABLE_NAME, $join);

		if ($dt != null) {
			$until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
			$this->db->where(Stock_Gudang::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Stock_Gudang::$TANGGAL . ' <', $until, TRUE);
		}
		if ($nomor_sj != null)
			$this->db->like(Stock_Gudang::$PESAN, $nomor_sj, $this::$WHERE_LIKE_BOTH);
		if ($kode_barang != null)
			$this->db->where(Material::$KODE_BARANG,strtoupper($kode_barang));
		if ($kemasan != null)
			$this->db->where(Material::$KEMASAN, strtoupper($kemasan));
		if ($warna != null)
			$this->db->where(Material::$WARNA, strtoupper($warna));
		if ($gudang != null)
			$this->db->where(Gudang::$NAMA_GUDANG, strtoupper($gudang));
		
		return $this->db->get();
	}
	public function get_history_stock_opname($dt, $st){
		/*
		$arr = [
			Material_Fisik::$ID, Material_Fisik::$TANGGAL,
			Material_Fisik::$TABLE_NAME . "." . Material_Fisik::$ID_MATERIAL . " " . Material::$ID,
			Material_Fisik::$TABLE_NAME . "." . Material_Fisik::$NIK . " " . Material_Fisik::$NIK,
			Karyawan::$NAMA, Material::$PRODUCT_CODE, Material_Fisik::$QTY_PROGRAM, Material_Fisik::$QTY_FISIK
		];
		$this->db->select($arr);
		$this->db->from(Material_Fisik::$TABLE_NAME);
		$join = Material_Fisik::$TABLE_NAME . "." . Material_Fisik::$ID_MATERIAL . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $join);
		$join = Material_Fisik::$TABLE_NAME . "." . Material_Fisik::$NIK . "=" . Karyawan::$TABLE_NAME . "." . Karyawan::$ID;
		$this->db->join(Karyawan::$TABLE_NAME, $join);
		if ($dt != null) {
			$until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
			$this->db->where(Material_Fisik::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Material_Fisik::$TANGGAL . ' <', $until, TRUE);
		}
		return $this->db->get();*/
		$where="";
		if ($dt != null) {
			$until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
			$where = " and tanggal>='$dt' and tanggal<'$until'";
		}
		return $this->db->query("select id_material_fisik,tanggal,id_material_new,material_fisik.id_material id_material, material_fisik.nik nik,
									nama,product_code,qty_program,qty_fisik
									from material,material_fisik,karyawan
									where material.id_material=material_fisik.id_material and
									material_fisik.nik=karyawan.nik $where
									union
								 select null,null,id_material_new,id_material,null,null,product_code,0,0
								 	from material where id_material not in (
										select material_fisik.id_material id_material
										from material,material_fisik,karyawan
										where material.id_material=material_fisik.id_material and
										material_fisik.nik=karyawan.nik $where
									) and mat_group!='AUTO' and id_material in (select id_material
										from fifo_stock_gudang)");
		//echo $this->db->last_query();
	}
	public function get_template_stock_opname($h){
		if($h=="y"){
			return $this->db->query("select material.id_material id_material, id_material_new, gudang.rak rak ,gudang.kolom kolom ,gudang.tingkat tingkat,gudang.kapasitas kapasitas,material.product_code product_code,material.box box, (gudang.kapasitas*material.box) as pcs, sum(qty) as qty, (case when material.box=1 then 0 else (qty div box) end) as qty_dus, (case when material.box=1 then (qty div box) else (qty mod box) end) as qty_tin
										from material,gudang,fifo_stock_gudang
										where material.id_material=fifo_stock_gudang.id_material and gudang.is_deleted=0 and gudang.id_gudang=fifo_stock_gudang.id_gudang and material.id_material and gudang.rak != 'TRANSIT'
										group by material.id_material,gudang.id_gudang");
		}else{
			return $this->db->query("select material.id_material id_material, id_material_new, gudang.rak rak ,gudang.kolom kolom ,gudang.tingkat tingkat,gudang.kapasitas kapasitas,material.product_code product_code,material.box box, (gudang.kapasitas*material.box) as pcs, sum(qty) as qty, (case when material.box=1 then 0 else (qty div box) end) as qty_dus, (case when material.box=1 then (qty div box) else (qty mod box) end) as qty_tin
										from material,gudang,fifo_stock_gudang
										where material.id_material=fifo_stock_gudang.id_material and gudang.is_deleted=0 and gudang.id_gudang=fifo_stock_gudang.id_gudang and material.id_material and gudang.rak != 'TRANSIT'
										group by material.id_material,gudang.id_gudang
									union
									select material.id_material, id_material_new, gudang.rak,gudang.kolom,gudang.tingkat,gudang.kapasitas,material.product_code,material.box, (gudang.kapasitas*material.box) as pcs, 0,0,0
										from material, gudang
										where gudang.id_material=material.id_material and gudang.is_deleted=0 AND gudang.id_gudang not in (select fifo_stock_gudang.id_gudang
										from material,gudang,fifo_stock_gudang
										where material.id_material=fifo_stock_gudang.id_material and gudang.id_gudang=fifo_stock_gudang.id_gudang and material.id_material) and gudang.rak !='TRANSIT'  
										ORDER BY `rak` ASC,kolom,tingkat");
		}
	}
	
	public function stock_get_by($id_material, $id_gudang = null){
		$this->db->select("sum(qty) as ".Stock_Gudang::$STOCK_AKHIR);
		$this->db->where(Fifo_Stock_Gudang::$QTY . " !=", 0);
		$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
		if ($id_gudang != null) {
			$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG, $id_gudang);
		}
		$this->db->group_by(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
		$qry = $this->db->get(Fifo_Stock_Gudang::$TABLE_NAME);
		if ($qry->num_rows() > 0) {
			return $qry->row_array()[Stock_Gudang::$STOCK_AKHIR];
		}
		return 0;
	}
	public function stock_get_all($with_fisik = NULL){
		$this->db->where(Material::$IS_DELETED, 0);
		$qry = $this->db->get(Material::$TABLE_NAME);
		$data = [];
		$ctr = 0;
		foreach ($qry->result_array() as $row) {
			$id_material = $row[Material::$ID];
			$stock = $this->stock_get_by($id_material);
			if (($with_fisik != NULL) || ($with_fisik == NULL && $stock != 0)) {
				$data[$ctr][Material::$ID_NEW] = $row[Material::$ID_NEW];
				$data[$ctr][Material::$ID] = $id_material;
				$data[$ctr][Material::$PRODUCT_CODE] = $row[Material::$PRODUCT_CODE];
				$data[$ctr][Material::$DESCRIPTION] = $row[Material::$DESCRIPTION];
				$data[$ctr][Fifo_Stock_Gudang::$QTY] = $stock;

				$stock_fisik = 0;
				$this->db->where(Material_Fisik::$ID_MATERIAL, $id_material);
				$this->db->order_by(Material_Fisik::$ID, $this::$ORDER_TYPE_DESC);
				$qry_material_fisik = $this->db->get(Material_Fisik::$TABLE_NAME);
				if ($qry_material_fisik->num_rows() > 0) {
					$stock_fisik = $qry_material_fisik->row_array()[Material_Fisik::$QTY_FISIK];
				}

				$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $id_material);
				$data[$ctr][Gudang::$TABLE_NAME] = "";
				if ($qry_gudang->num_rows() > 0) {
					foreach ($qry_gudang->result_array() as $row2) {
						$data[$ctr][Gudang::$TABLE_NAME] .= $row2[Gudang::$NAMA_GUDANG] . '-R ' . $row2[Gudang::$RAK] . $row2[Gudang::$KOLOM] . ' ' . $row2[Gudang::$TINGKAT] . '<br>';
					}
				} else
					$data[$ctr][Gudang::$TABLE_NAME] = '-';

				$data[$ctr][Material_Fisik::$QTY_FISIK] = $stock_fisik;

				$ctr++;
			}
		}
		return $data;
	}
	public function stock_get_by_rak(){
		$select=[
			Material::$TABLE_NAME . "." . Material::$ID . " " . Material::$ID,
			Material::$ID_NEW,
			Material::$PRODUCT_CODE,
			Material::$DESCRIPTION,
			Gudang::$NAMA_GUDANG,
			Gudang::$RAK,
			Gudang::$KOLOM,
			Gudang::$TINGKAT,
			Fifo_Stock_Gudang::$QTY
		];
		$this->db->select($select);
		$this->db->from(Material::$TABLE_NAME);
		
		$join = Material::$TABLE_NAME . "." . Material::$ID . "=" . Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_MATERIAL;
		$this->db->join(Fifo_Stock_Gudang::$TABLE_NAME, $join);
		
		$join = Gudang::$TABLE_NAME . "." . Gudang::$ID . "=" . Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_GUDANG;
		$this->db->join(Gudang::$TABLE_NAME, $join);

		return $this->db->get();
	}
	public function fifo_get_terisi_by_rak($rak, $kolom, $tingkat){
		$this->db->select("sum(". Fifo_Stock_Gudang::$QTY.") as ". Fifo_Stock_Gudang::$S_TERISI);
		$this->db->from(Fifo_Stock_Gudang::$TABLE_NAME);
		
		$join = Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_GUDANG . "=" . Gudang::$TABLE_NAME . "." . Gudang::$ID;
		$this->db->join(Gudang::$TABLE_NAME, $join);

		$this->db->where(Gudang::$RAK, $rak);
		$this->db->where(Gudang::$KOLOM, $kolom);
		$this->db->where(Gudang::$TINGKAT, $tingkat);

		return $this->db->get();
	}

	public function gudang_get_by_last_fifo($id_gudang, $id_material){
		$this->db->where(Material::$ID, $id_material);
		$this->db->where($this::$IS_DELETED, 0);
		$this->db->where(Gudang::$ID . " > ", $id_gudang, false);
		return $this->db->get(Gudang::$TABLE_NAME);
	}
	public function gudang_get_cadangan(){
		$select = [
			Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_MATERIAL . " " . Fifo_Stock_Gudang::$ID_MATERIAL,
			Gudang::$TABLE_NAME . "." . Gudang::$ID . " " . Gudang::$ID,
			Fifo_Stock_Gudang::$QTY, Fifo_Stock_Gudang::$ID
		];
		$this->db->select($select);
		$this->db->from(Gudang::$TABLE_NAME);
		$join = Gudang::$TABLE_NAME . "." . Gudang::$ID . "=" . Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_GUDANG;
		$this->db->join(Fifo_Stock_Gudang::$TABLE_NAME, $join);
		$this->db->where(Gudang::$RAK, Gudang::$S_CADANGAN);
		$this->db->where(Fifo_Stock_Gudang::$QTY . "> ", 0, true);

		return $this->db->get();
	}

	public function sage_get_by_sap_id($id_material_fg_sap){
		//Cari di FG SAP Dulu
		$fg = null;
		$fg_sap = $this->fg_model->get(Material_FG_SAP::$TABLE_NAME, Material_FG_SAP::$ID, $id_material_fg_sap);
		if ($fg_sap->num_rows() > 0) {
			$fg = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $fg_sap->row_array()[Material_FG_SAP::$ID_SAGE]);
		}
		return $fg;
	}
	
}
