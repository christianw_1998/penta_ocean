<?php
require_once(APPPATH . 'core/Library_Model.php');

class Karyawan{
	public static $TABLE_NAME="karyawan";
	public static $ID="nik";
	public static $NAMA="nama";
	public static $NPWP="npwp";
	public static $GAJI_POKOK="gaji_pokok";
	public static $TUNJANGAN_JABATAN="tunjangan_jabatan";
	public static $TUNJANGAN_OBAT="tunjangan_obat";
	public static $USERNAME="username";
	public static $PASSWORD="password";
	public static $SALDO_CUTI="saldo_cuti";
	public static $IS_OUTSOURCE="is_outsource";
	public static $IS_DELETED="is_deleted";

	//FOREIGN KEY
	public static $ID_ROLE="id_role";
	public static $ID_KATEGORI="id_kategori";
	public static $ID_SUB_KATEGORI="id_sub_kategori";

	//STATIC
	public static $S_K_NAMA="k_nama";

	//MESSAGE
	public static $MESSAGE_SUCCESS_INSERT="KAQ001";
	public static $MESSAGE_SUCCESS_UPDATE="KAQ002";
	public static $MESSAGE_SUCCESS_DELETE="KAQ003";
	public static $MESSAGE_FAILED_DELETE="KAQ004";
	public static $MESSAGE_FAILED_INSERT_UPDATE_NAME_CONTAINS_NUMBER_SYMBOL="KAQ005";
	public static $MESSAGE_FAILED_INSERT_UPDATE_NIK_CONTAINTS_NOT_NUMBER_SYMBOL="KAQ007";
	public static $MESSAGE_FAILED_INSERT_UPDATE_NIK_EMPTY="KAQ006";
	
	public static $MESSAGE_NOT_FOUND="KAS404";
	public static $MESSAGE_FOUND="KAS001";
	public static $MESSAGE_FOUND_IS_DELETED="KAS002";
	public static $MESSAGE_FOUND_ROLE_NOT_FOUND="KAS003";

	public static $MESSAGE_UPLOAD_FAILED="KAU002";
	public static $MESSAGE_UPLOAD_SUCCESS="KAU001";
	public static $MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE="KAU003";
}
class Sub_Kategori{
	public static $TABLE_NAME = "sub_kategori";
	public static $ID = "id_sub_kategori";
	public static $NAMA_EKSPEDISI = "nama";

	//Foreign Keys
	public static $ID_KATEGORI = "id_kategori";

}
class Kategori_Karyawan{
	public static $TABLE_NAME = "kategori_karyawan";
	public static $ID = "id_kategori";
	public static $TIPE = "tipe";
}
class Role{
	public static $TABLE_NAME = "role";
	public static $ID = "id_role";
	public static $NAMA = "nama";

	//Static Variables
	public static $S_R_NAMA="r_nama";
}
class Karyawan_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		if (isset($data)) {
			//upload karyawan kepala
			$is_change = false;
			if (count($data['header'][2]) > 7) {
				for ($i = 3; $i < 3 + count($data['values']); $i++) {
					if (isset($data['values'][$i])) {
						if ($data['values'][$i]["A"] != "") { //jika nik tidak kosong
							$nik = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
							$nama = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : "";
							$npwp = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : "";
							$role = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
							$kategori = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;

							$temp = $this->get(Role::$TABLE_NAME, Role::$NAMA, $role);
							$id_role = NULL;
							if ($temp != NULL) $id_role = $temp->row()->id_role;

							$temp2 = $this->get(Kategori_Karyawan::$TABLE_NAME, Kategori_Karyawan::$TIPE, $kategori);
							$id_kategori = NULL;
							if ($temp2 != NULL) $id_kategori = $temp2->row()->id_kategori;
							$gp = isset($data['values'][$i]["F"]) ? str_replace(".", "", $data['values'][$i]["F"]) : NULL;
							$tunj = isset($data['values'][$i]["G"]) ? str_replace(".", "", $data['values'][$i]["G"]) : NULL;
							$s_cuti = isset($data['values'][$i]["H"]) ? $data['values'][$i]["H"] : NULL;

							$data_query = array(
								Karyawan::$NAMA => $nama,
								Karyawan::$NPWP => $npwp
							);
							if (isset($id_role)) $data_query[Karyawan::$ID_ROLE] = $id_role;
							else $data_query[Karyawan::$ID_ROLE] = NULL;
							if (isset($id_kategori)) $data_query[Karyawan::$ID_KATEGORI] = $id_kategori;
							else $data_query[Karyawan::$ID_KATEGORI] = NULL;
							if (isset($gp)) $data_query[Karyawan::$GAJI_POKOK] = $gp;
							else $data_query[Karyawan::$GAJI_POKOK] = NULL;
							if (isset($tunj)) $data_query[Karyawan::$TUNJANGAN_JABATAN] = $tunj;
							else $data_query[Karyawan::$TUNJANGAN_JABATAN] = NULL;
							if (isset($s_cuti)) $data_query[Karyawan::$SALDO_CUTI] = $s_cuti;
							else $data_query[Karyawan::$SALDO_CUTI] = 0;

							if ($this->cek_nik($nik)) { //jika sudah ada nik, update
								$is_change = true;
								$this->db->where(Karyawan::$ID, $nik);
								$this->db->update(Karyawan::$TABLE_NAME, $data_query);
							} else {
								$data_query[Karyawan::$IS_OUTSOURCE] = 0;
								$data_query[Karyawan::$ID] = $nik;
								$this->db->insert(Karyawan::$TABLE_NAME, $data_query);
								if ($this->db->affected_rows() > 0)
									$is_change = true;
							}
						} else
							break;
					}
				}
				if ($is_change)
					return Karyawan::$MESSAGE_UPLOAD_SUCCESS;
				return Karyawan::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
			} else if (count($data['header'][2]) == 6) {
				for ($i = 3; $i < 3 + count($data['values']); $i++) {
					if (isset($data['values'][$i])) {
						if ($data['values'][$i]["A"] != "") { //jika nik tidak kosong
							$nik = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
							$nama = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
							$npwp = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
							//$s_cuti=isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
							$data_query = array(
								Karyawan::$NAMA => $nama,
								Karyawan::$NPWP => $npwp
							);
							//if(isset($s_cuti))$data_query["saldo_cuti"]=$s_cuti;
							//else $data_query["saldo_cuti"]=0;

							if ($this->cek_nik($nik)) { //jika sudah ada nik, update
								$is_change = true;
								$this->db->where(Karyawan::$ID, $nik);
								$this->db->update(Karyawan::$TABLE_NAME, $data_query);
							} else {
								$data_query[Karyawan::$ID] = $nik;
								$this->db->insert(Karyawan::$TABLE_NAME, $data_query);
								if ($this->db->affected_rows() > 0)
									$is_change = true;
							}
						}
					}
				}
				if ($is_change)
					return Karyawan::$MESSAGE_UPLOAD_SUCCESS;
				return Karyawan::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
			}
		}
		return Karyawan::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
	}
	
	public function cek_login($u,$p){
		$this->db->from(Karyawan::$TABLE_NAME)
					->where("upper(".Karyawan::$USERNAME.")",strtoupper($u))
					->where(Karyawan::$PASSWORD,md5($p))
					->where(Karyawan::$IS_DELETED,0)
					->where(Karyawan::$ID_ROLE.Library_Model::$WHERE_IS_NULL, NULL, FALSE);
		$qry=$this->db->get();
		if($qry->num_rows()>0){
			return Karyawan::$MESSAGE_FOUND_ROLE_NOT_FOUND; 
		}
	
		$this->db->from(Karyawan::$TABLE_NAME)
					->where("upper(".Karyawan::$USERNAME.")",strtoupper($u))
					->where(Karyawan::$PASSWORD,md5($p))
					->where(Karyawan::$IS_DELETED,0);
		$qry=$this->db->get();
		if($qry->num_rows()>0){
			return Karyawan::$MESSAGE_FOUND; 
		}
		
		$this->db->from(Karyawan::$TABLE_NAME)
					->where("upper(".Karyawan::$USERNAME.")",strtoupper($u))
					->where(Karyawan::$PASSWORD,md5($p))
					->where(Karyawan::$IS_DELETED,1);
		$qry=$this->db->get();
		if($qry->num_rows()>0){
			return Karyawan::$MESSAGE_FOUND_IS_DELETED; 
		}
		
		return Karyawan::$MESSAGE_NOT_FOUND;
	}
	public function cek_nik($nik){
		$this->db->where(Karyawan::$ID,$nik);
		$qry=$this->get(Karyawan::$TABLE_NAME);
		if($qry->num_rows()>0)
			return true;
		return false;
	}
	public function insert($ni,$na,$np,$g,$t,$c,$r,$k,$io,$sk){
		$data_query=array();
		$data_query[Karyawan::$IS_OUTSOURCE]=$io;
		$data_query[Karyawan::$NAMA]=strtoupper($na);
		$data_query[Karyawan::$NPWP]=$np;
		$data_query[Karyawan::$GAJI_POKOK]=$g;
		if($ni=="")
			return Karyawan::$MESSAGE_FAILED_INSERT_UPDATE_NIK_EMPTY;
		if($this->contains_number($na)||preg_match('/[\'^£$%&*()}{@#~?><>,.|=_+¬-]/', $na))
			return Karyawan::$MESSAGE_FAILED_INSERT_UPDATE_NAME_CONTAINS_NUMBER_SYMBOL;
		if(preg_match('/[\'^£$%&*()}{@#~?><>,.|=_+¬-]/', $ni))
			return Karyawan::$MESSAGE_FAILED_INSERT_UPDATE_NIK_CONTAINTS_NOT_NUMBER_SYMBOL;

		if($g==-1)
			$data_query[Karyawan::$GAJI_POKOK]=NULL;
		
		$data_query[Karyawan::$TUNJANGAN_JABATAN]=$t;
		if($t==-1)
			$data_query[Karyawan::$TUNJANGAN_JABATAN]=NULL;

		//$data_query['saldo_cuti']=$c;
		
		$data_query[Karyawan::$IS_DELETED]=0;
		
		$data_query[Karyawan::$ID_ROLE]=$r;
		if($r==-1)
			$data_query[Karyawan::$ID_ROLE]=NULL;

		$data_query[Karyawan::$ID_KATEGORI]=$k;
		if($k==-1)
			$data_query[Karyawan::$ID_KATEGORI]=NULL;

		$data_query[Karyawan::$ID_SUB_KATEGORI]=$sk;
		if($sk==-1)
		$data_query[Karyawan::$ID_SUB_KATEGORI]=NULL;

		$qry=$this->get_by(Karyawan::$ID,$ni);
		if($qry->num_rows()>0){
			$this->db->where(Karyawan::$ID,$ni);
			$this->db->update(Karyawan::$TABLE_NAME,$data_query);
			return Karyawan::$MESSAGE_SUCCESS_UPDATE;
		}else{
			$data_query[Karyawan::$ID]=$ni;
			$data_query[Karyawan::$SALDO_CUTI]=0;
			$this->db->insert(Karyawan::$TABLE_NAME,$data_query);
			return Karyawan::$MESSAGE_SUCCESS_INSERT;
		}
	}

	public function get_by($field,$value){
		$qry=$this->db->query("select tunjangan_obat,role.id_role id_role,kategori_karyawan.id_kategori id_kategori,karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,role.nama r_nama,saldo_cuti,is_outsource,karyawan.id_sub_kategori id_sub_kategori
								from karyawan, role, kategori_karyawan,sub_kategori
								where upper($field)='$value' and sub_kategori.id_kategori=kategori_karyawan.id_kategori and karyawan.id_role=role.id_role and karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								union
								select tunjangan_obat,id_role,kategori_karyawan.id_kategori id_kategori,karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,'TIDAK ADA' r_nama,saldo_cuti,is_outsource,karyawan.id_sub_kategori
								from karyawan, kategori_karyawan,sub_kategori
								where karyawan.id_kategori=kategori_karyawan.id_kategori and sub_kategori.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0 and id_role is null
								and upper($field)='$value'
								union
								select tunjangan_obat,id_role,NULL,karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,'TIDAK ADA' tipe, 'TIDAK ADA' r_nama, saldo_cuti,is_outsource,NULL
								from karyawan
								where upper($field)='$value' and karyawan.id_kategori is null and karyawan.is_deleted=0");
		return $qry;
	}	
	public function get_all(){
		return $this->db->query("select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,role.nama r_nama,saldo_cuti,is_outsource
								 from karyawan, role, kategori_karyawan
								 where karyawan.id_role=role.id_role and karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								 union
								 select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,'TIDAK ADA' r_nama,saldo_cuti,is_outsource
								 from karyawan, kategori_karyawan
								 where karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								 and karyawan.nik not in (
									select nik
									from karyawan, role, kategori_karyawan
									where karyawan.id_role=role.id_role and karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								 )
								 union
								 select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,'TIDAK ADA' tipe, role.nama r_nama, saldo_cuti,is_outsource
								 from karyawan,role
								 where karyawan.id_kategori is null and karyawan.id_role=role.id_role and karyawan.is_deleted=0
								 union
								 select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,'TIDAK ADA' tipe, 'TIDAK ADA' r_nama, saldo_cuti,is_outsource
								 from karyawan
								 where karyawan.id_kategori is NULL and karyawan.id_role is NULL
								 order by nik");
	}
	public function get_colour_matcher(){
		$this->db->where(Karyawan::$ID_SUB_KATEGORI,15); 
		$this->db->where(Karyawan::$IS_DELETED,0);
		return $this->get(Karyawan::$TABLE_NAME);
	}
	
	
	
	
	
	
	

}
