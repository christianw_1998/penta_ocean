<?php
class Worksheet{
	public static $TABLE_NAME = "worksheet";
	public static $ID = "nomor_worksheet";

	public static $BATCH_NUMBER = "batch_number";
	public static $TANGGAL = "tanggal";
	public static $IS_DONE = "is_done";
	public static $CTR_PRINT = "ctr_print";
	public static $IS_DELETED = "is_deleted";

	//Foreign Key
	public static $ID_PV = "id_bom_pv";

	//Static
	public static $S_CODE = "49";
	public static $S_MUT_CODE = "ADJ"; //mutasi
	public static $S_SEMI = "SEMI";
	public static $S_FG = "FG";
	public static $S_ARR_POTONG_PEREKAT = ["2317O", "2317G", "2424C"];

	public static $MESSAGE_SUCCESS_CREATE = "WRPQ012";
	public static $MESSAGE_SUCCESS_MUTASI = "WRPQ010";
	public static $MESSAGE_SUCCESS_UPLOAD = "WRPQ001";
	public static $MESSAGE_FAILED_UPLOAD_MATERIAL_NOT_FOUND = "WRPQ002";
	public static $MESSAGE_FAILED_UPLOAD_WS_ALREADY_EXISTS = "WRPQ003";
	public static $MESSAGE_SUCCESS_CANCEL = "WRPQ004";
	public static $MESSAGE_FAILED_CANCEL_QTY_NOT_ENOUGH = "WRPQ005";
	public static $MESSAGE_SUCCESS_CHANGE_STATUS = "WRPQ011";

	public static $MESSAGE_FAILED_SEARCH_NOT_FOUND = "WRPQ006";
	public static $MESSAGE_FAILED_SEARCH_NOT_SEMI = "WRPQ007";
	public static $MESSAGE_FAILED_SEARCH_QTY_CHECKER_STILL_ZERO = "WRPQ008";
	public static $MESSAGE_FAILED_SEARCH_NOT_FG = "WRPQ009";

	public static function GEN_CANCEL_MESSAGE($params, $params2)
	{
		return "Cancel GI " . $params . " oleh " . $params2;
	}
}
class Worksheet_Awal{
	public static $TABLE_NAME = "worksheet_awal";
	public static $ID = "id_worksheet_awal";

	public static $NOMOR_URUT = "nomor_urut";
	public static $QTY = "qty";
	public static $KODE_MATERIAL_FG = "kode_material_fg";
	public static $KODE_MATERIAL_RM_PM = "kode_material_rm_pm";

	//Foreign Key
	public static $NIK = "nik";
	public static $NOMOR_WS = "nomor_worksheet";
}
class Worksheet_Checker_Header{
	public static $TABLE_NAME = "worksheet_checker_header";
	public static $ID = "id_worksheet_checker_header";

	public static $IS_DELETED = "is_deleted";
	public static $TANGGAL = "tanggal";

	//Foreign Key
	public static $NOMOR_WS = "nomor_worksheet";
	public static $NIK = "nik";

	//Static
	public static $S_ADJUSTMENT_CHECKER = "ADJUSTMENT_CHECKER";
}
class Worksheet_Checker_Detail{
	public static $TABLE_NAME = "worksheet_checker_detail";
	public static $ID = "id_worksheet_checker";

	public static $NOMOR_URUT = "nomor_urut";
	public static $QTY = "qty";
	public static $VALUE = "value";

	//Foreign Key
	public static $ID_MATERIAL = "id_material_rm_pm";
	public static $ID_VENDOR = "id_vendor";
	public static $ID_HEADER = "id_worksheet_checker_header";

	public static $MESSAGE_SUCCESS_INSERT = "WSCQ001";
	public static $MESSAGE_SUCCESS_ADJUST = "WSCQ003";
	public static $MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND = "WSCQ002";
	public static $MESSAGE_FAILED_INSERT_KEMASAN_IS_NOT_MATCHED = "WSCQ004";
	public static $MESSAGE_FAILED_INSERT_KEMASAN_IS_PM = "WSCQ005";

	//Static Variable
	public static $S_TOT_QTY = "qty_total";

	public static function GEN_INSERT_MESSAGE($params, $params2, $params3)
	{
		return "WS " . $params . " oleh " . $params3 . " (Kode: " . $params2 . ")";
	}
	public static function GEN_ADJUSTMENT_MESSAGE($params, $params2, $params3)
	{
		return "WS (Adjustment) " . $params . " oleh " . $params3 . " (Kode: " . $params2 . ")";
	}
	public static function GEN_MUTASI_MESSAGE($params, $params2, $params3)
	{
		return "WS (Mutasi) oleh " . $params . " (Kode: " . $params2 . ", Pesan: " . $params3 . ")";
	}
}
class Worksheet_Connect{
	public static $TABLE_NAME = "worksheet_connect";
	public static $ID = "id_worksheet_connect";

	public static $NO_FG = "no_ws_fg";
	public static $NO_SEMI = "no_ws_semi";
	public static $IS_DELETED = "is_deleted";
	public static $PERSIAPAN_FROM = "persiapan_from";
	public static $PERSIAPAN_TO = "persiapan_to";
	public static $PREMIX_FROM = "premix_from";
	public static $PREMIX_TO = "premix_to";
	public static $MAKEUP_FROM = "makeup_from";
	public static $MAKEUP_TO = "makeup_to";
	public static $CM_FROM = "cm_from";
	public static $CM_TO = "cm_to";
	public static $QC_FROM = "qc_from";
	public static $QC_TO = "qc_to";
	public static $FILLING_FROM = "filling_from";
	public static $FILLING_TO = "filling_to";
	public static $S_TOT_QTY = "s_tot_qty";
	public static $S_COUNT_WS = "s_count_ws";

	//Foreign Key
	public static $NIK_PREMIX = "nik_premix";
	public static $NIK_MAKEUP = "nik_makeup";
	public static $NIK_FILLING = "nik_filling";
	public static $NIK = "nik";

	public static $MESSAGE_SUCCESS_INSERT = "WCOQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "WCOQ004";
	public static $MESSAGE_FAILED_INSERT_ALREADY_CONNECTED = "WCOQ002";
	public static $MESSAGE_FAILED_SEARCH_WS_FG_NOT_FOUND = "WCOQ003";
	public static $MESSAGE_FAILED_SEARCH_WS_FG_ALREADY_INPUT = "WCOQ005";
}
class Worksheet_Connect_History{
	public static $TABLE_NAME = "worksheet_connect_history";
	public static $ID = "id_worksheet_connect_history";

	public static $NO_FG = "no_ws_fg";
	public static $NO_SEMI = "no_ws_semi";
	public static $TANGGAL = "tanggal";
	public static $IS_DELETED = "is_deleted";
	public static $PERSIAPAN_FROM = "persiapan_from";
	public static $PERSIAPAN_TO = "persiapan_to";
	public static $PREMIX_FROM = "premix_from";
	public static $PREMIX_TO = "premix_to";
	public static $MAKEUP_FROM = "makeup_from";
	public static $MAKEUP_TO = "makeup_to";
	public static $CM_FROM = "cm_from";
	public static $CM_TO = "cm_to";
	public static $QC_FROM = "qc_from";
	public static $QC_TO = "qc_to";
	public static $FILLING_FROM = "filling_from";
	public static $FILLING_TO = "filling_to";
	public static $S_TOT_QTY = "s_tot_qty";
	public static $S_COUNT_WS = "s_count_ws";

	//Foreign Key
	public static $NIK_PREMIX = "nik_premix";
	public static $NIK_MAKEUP = "nik_makeup";
	public static $NIK_FILLING = "nik_filling";
	public static $NIK = "nik";
}
class Worksheet_Link{
	public static $TABLE_NAME = "worksheet_link";
	public static $ID = "id_worksheet_link";

	public static $NO_FG = "no_ws_fg";
	public static $NO_SEMI = "no_ws_semi";
	public static $IS_DELETED = "is_deleted";
}
class Gudang_RM_PM{
	public static $TABLE_NAME = "gudang_rm_pm";
	public static $ID = "id_gudang_rm_pm";
	public static $NAMA_GUDANG = "nama_gudang";

	//Foreign Key
	public static $ID_MATERIAL_RM_PM = "id_material_rm_pm";
}
class Gudang_RM_PM_Transit{
	public static $TABLE_NAME = "gudang_rm_pm_transit";
	public static $ID = "id_gudang_rm_pm_transit";
	public static $QTY = "qty";
	public static $BERAT = "berat";
	public static $TANGGAL_GENERATE = "tanggal_generate";
	public static $IS_DONE = "is_done";

	//Foreign Key
	public static $ID_CONNECT_WS = "id_connect_ws";
	public static $ID_MATERIAL_FG = "id_material_fg";

	public static $MESSAGE_FAILED_SEARCH_WS_ALREADY_INPUT = "GRPQ001";

}

class WS_Model extends RMPM_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		$data_ws_rm_pm = array();
		$data_ws_rm_pm_awal = array();
		$ctr = 0;
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika WS tidak kosong
					$nomor_ws = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$nomor_urut = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$id_material = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$tanggal = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					$qty = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;

					//$data_ws_rm_pm[$ctr] = array();
					//Insert Tabel WS
					$this->db->where(Worksheet::$ID, $nomor_ws);
					$qry = $this->db->get(Worksheet::$TABLE_NAME);

					$skip_ws=false;
					if ($qry->num_rows() > 0) {
						$skip_ws=true;
					} else {
						$data_ws_rm_pm[$ctr] = array(
							Worksheet::$ID => $nomor_ws,
							Worksheet::$TANGGAL => date('Y-m-d', strtotime($tanggal)),
							Worksheet::$IS_DONE => 0,
							Worksheet::$IS_DELETED => 0
						);
						//$this->db->insert(Worksheet_RM_PM::$TABLE_NAME, $data_ws_rm_pm[$ctr]);
					}

					if(!$skip_ws){
						$this->db->where(Worksheet_Awal::$NOMOR_WS, $nomor_ws);
						$this->db->where(Worksheet_Awal::$NOMOR_URUT, $nomor_urut);
						$qry = $this->db->get(Worksheet_Awal::$TABLE_NAME);
						if ($qry->num_rows() > 0) {
						} else {
							//Insert Tabel WS Awal
							$data_ws_rm_pm_awal[$ctr] = array(
								Worksheet_Awal::$NOMOR_URUT => $nomor_urut,
								Worksheet_Awal::$QTY => $qty,
								Worksheet_Awal::$NOMOR_WS => $nomor_ws,
								Worksheet_Awal::$NIK => 14048
							);

							//Cek dulu apakah Material yang ada adalah FG atau RM PM
							$this->db->where(Material_RM_PM::$KODE_MATERIAL, $id_material);
							$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
							if ($qry->num_rows() > 0) { //Adalah Material RM PM
								$data_ws_rm_pm_awal[$ctr][Worksheet_Awal::$KODE_MATERIAL_RM_PM] = $id_material;
							} else {
								//$this->db->where(Material::$PRODUCT_CODE, $id_material);
								//$qry = $this->db->get(Material::$TABLE_NAME);
								//if ($qry->num_rows() > 0) { //Adalah Material FG
								$data_ws_rm_pm_awal[$ctr][Worksheet_Awal::$KODE_MATERIAL_FG] = $id_material;
								//}else{
								//	return Worksheet_RM_PM::$MESSAGE_FAILED_UPLOAD_MATERIAL_NOT_FOUND;
								//}
							}
						}
						$ctr++;
					}
				} else
					break;
			}
		}
		for ($i = 0; $i < $ctr; $i++) {
			//Insert Tabel WS
			if (isset($data_ws_rm_pm[$i])) {
				$this->db->where(Worksheet::$ID, $data_ws_rm_pm[$i][Worksheet::$ID]);
				$qry = $this->db->get(Worksheet::$TABLE_NAME);
				if ($qry->num_rows() > 0) {
				} else {
					$this->db->insert(Worksheet::$TABLE_NAME, $data_ws_rm_pm[$i]);
				}
			}

			//Insert Tabel RM PM
			if (isset($data_ws_rm_pm_awal[$i]))
				$this->db->insert(Worksheet_Awal::$TABLE_NAME, $data_ws_rm_pm_awal[$i]);
		}
		return Worksheet::$MESSAGE_SUCCESS_UPLOAD;
	}
	public function create_new($ctr, $semi_id, $semi_qty, $fg_id, $fg_qty){
		$message="";

		//Insert WS Semi
		$last_number = $this->get_last_number();
		$no_ws_semi = $last_number + 1;

		$batch_number = substr(date("Y"), 2) . date("m") . substr($no_ws_semi, 3);

		$data_pv = $this->pv_model->get_active_by_product_code($semi_id)->row_array();
		$id_pv = $data_pv[BOM_Production_Version::$ID];

		$data_ws_semi=[
			Worksheet::$BATCH_NUMBER => $batch_number,
			Worksheet::$ID => $no_ws_semi,
			Worksheet::$IS_DONE => 0,
			Worksheet::$IS_DELETED => 0,
			Worksheet::$TANGGAL => date("Y-m-d"),
			Worksheet::$ID_PV => $id_pv,
			Worksheet::$CTR_PRINT => 0
		];

		$this->db->insert(Worksheet::$TABLE_NAME, $data_ws_semi);
		$message .= "WS Semi: ". $data_ws_semi[Worksheet::$ID]."";

		//Insert WS Awal Semi
		$id_bom_alt = $data_pv[BOM_Alt::$ID];
		$data_recipe = $this->bom_model->get(BOM_Alt_Recipe::$TABLE_NAME, BOM_Alt_Recipe::$ID_ALT, $id_bom_alt,null,null,false);

		$multiply = $semi_qty / 100; //Base 100
		foreach($data_recipe->result_array() as $row_recipe){
			$mat_rm_pm=$this->rmpm_model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$ID,$row_recipe[BOM_Alt_Recipe::$ID_MATERIAL])->row_array();
			$data_ws_awal=[
				Worksheet_Awal::$NOMOR_URUT => $row_recipe[BOM_Alt_Recipe::$NOMOR_URUT],
				Worksheet_Awal::$QTY => round(($row_recipe[BOM_Alt_Recipe::$QTY] * $multiply),3),
				Worksheet_Awal::$KODE_MATERIAL_RM_PM => $mat_rm_pm[Material_RM_PM::$KODE_MATERIAL],
				Worksheet_Awal::$NOMOR_WS => $data_ws_semi[Worksheet::$ID],
				Worksheet_Awal::$NIK => $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
			];
			$this->db->insert(Worksheet_Awal::$TABLE_NAME,$data_ws_awal);
		}

		for ($i = 0; $i < $ctr; $i++) {
			//Insert WS FG
			$no_ws_fg = $data_ws_semi[Worksheet::$ID] + $i + 1;

			$batch_number = substr(date("Y"), 2) . date("m") . substr($no_ws_fg, 3);

			$data_pv = $this->pv_model->get_active_by_product_code($fg_id[$i])->row_array();
			$id_pv = $data_pv[BOM_Production_Version::$ID];

			$data_ws_fg = [
				Worksheet::$BATCH_NUMBER => $batch_number,
				Worksheet::$ID => $no_ws_fg,
				Worksheet::$IS_DONE => 0,
				Worksheet::$IS_DELETED => 0,
				Worksheet::$TANGGAL => date("Y-m-d"),
				Worksheet::$ID_PV => $id_pv

			];

			$this->db->insert(Worksheet::$TABLE_NAME, $data_ws_fg);
			$message .= ", WS FG: ". $data_ws_fg[Worksheet::$ID] . " ";

			//Insert WS Awal FG
			$id_bom_alt = $data_pv[BOM_Alt::$ID];
			$data_recipe = $this->bom_model->get(BOM_Alt_Recipe::$TABLE_NAME, BOM_Alt_Recipe::$ID_ALT, $id_bom_alt, null, null, false);
			$fg = $this->fg_model->get(Material::$TABLE_NAME, Material::$ID, $fg_id[$i]);
			if($fg->num_rows()>0){
				$fg = $fg->row_array();
			}else{
				$fg = $this->fg_model->sage_get_by_sap_id($fg_id[$i])->row_array();
			}

			foreach ($data_recipe->result_array() as $row_recipe) {
				if($row_recipe[BOM_Alt_Recipe::$ID_MATERIAL]!=null){ //PMnya
					$mat_rm_pm = $this->rmpm_model->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $row_recipe[BOM_Alt_Recipe::$ID_MATERIAL])->row_array();

					$qty = ceil(($fg_qty[$i] / $fg[Material::$BOX]) * $row_recipe[BOM_Alt_Recipe::$QTY]); 
					
					$data_ws_awal = [
						Worksheet_Awal::$NOMOR_URUT => $row_recipe[BOM_Alt_Recipe::$NOMOR_URUT],
						Worksheet_Awal::$QTY => $qty,
						Worksheet_Awal::$KODE_MATERIAL_RM_PM => $mat_rm_pm[Material_RM_PM::$KODE_MATERIAL],
						Worksheet_Awal::$NOMOR_WS => $data_ws_fg[Worksheet::$ID],
						Worksheet_Awal::$NIK => $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
					];
				}else{ //FGnya
					$qty = round($fg_qty[$i] * $fg[Material::$NET], 2); 

					$data_ws_awal = [
						Worksheet_Awal::$NOMOR_URUT => $row_recipe[BOM_Alt_Recipe::$NOMOR_URUT],
						Worksheet_Awal::$QTY => $qty,
						Worksheet_Awal::$KODE_MATERIAL_FG => $row_recipe[BOM_Alt_Recipe::$KODE_FG],
						Worksheet_Awal::$NOMOR_WS => $data_ws_fg[Worksheet::$ID],
						Worksheet_Awal::$NIK => $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
					];
				}
				$this->db->insert(Worksheet_Awal::$TABLE_NAME, $data_ws_awal);

			}

			//Insert WS Link
			$data_ws_link = [
				Worksheet_Link::$NO_SEMI => $no_ws_semi,
				Worksheet_Link::$NO_FG => $no_ws_fg,
				Worksheet_Link::$IS_DELETED => 0
			];
			$this->db->insert(Worksheet_Link::$TABLE_NAME, $data_ws_link);

		}
		echo $this->alert($message);

		return Worksheet::$MESSAGE_SUCCESS_CREATE;
	}

	public function get_by($dt = null, $st = null, $n = null){
		if ($dt != null) {
			$until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet::$TANGGAL . ' <', $until, TRUE);
		}
		if ($n != null) {
			$this->db->where(Worksheet::$ID, $n);
		}
		$this->db->where(Worksheet::$IS_DELETED,0);
		return $this->db->get(Worksheet::$TABLE_NAME);
	}
	public function get_last_number(){
		$this->db->order_by(Worksheet::$ID, $this::$ORDER_TYPE_DESC);
		$qry = $this->db->get(Worksheet::$TABLE_NAME);
		if ($qry->num_rows() > 0)
			return $qry->row_array()[Worksheet::$ID];
			
		return null;
	}
	public function get_by_no($nomor_ws){
		$data = [];

		$this->db->where(Worksheet::$ID, $nomor_ws);
		$this->db->where(Worksheet::$IS_DELETED,0);
		$data[Worksheet::$TABLE_NAME] = $this->db->get(Worksheet::$TABLE_NAME);

		$this->db->select("*");
		$this->db->from(Worksheet_Awal::$TABLE_NAME);
		$this->db->join(Worksheet::$TABLE_NAME,Worksheet::$TABLE_NAME.".".Worksheet::$ID."=".Worksheet_Awal::$TABLE_NAME.".".Worksheet_Awal::$NOMOR_WS);
		$this->db->where(Worksheet::$IS_DELETED,0);
		$this->db->where(Worksheet_Awal::$KODE_MATERIAL_RM_PM . Library_Model::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->where(Worksheet_Awal::$TABLE_NAME.".".Worksheet_Awal::$NOMOR_WS, $nomor_ws);
		$data[Worksheet_Awal::$TABLE_NAME] = $this->db->get();

		$this->db->where(Worksheet_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws);
		$data[Worksheet_Checker_Header::$TABLE_NAME] = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);

		$arr_header = array();
		$ctr = 0;
		foreach ($data[Worksheet_Checker_Header::$TABLE_NAME]->result_array() as $row_header) {
			$arr_header[$ctr++] = $row_header[Worksheet_Checker_Header::$ID];
		}
		if ($ctr != 0)
			$this->db->where_in(Worksheet_Checker_Detail::$ID_HEADER, $arr_header);

		$data[Worksheet_Checker_Detail::$TABLE_NAME] = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);

		return $data;
	}
	public function get_print_by_no($nomor_ws){
		$data = [];

		$this->db->where(Worksheet::$ID, $nomor_ws);
		$this->db->where(Worksheet::$IS_DELETED,0);
		$data[Worksheet::$TABLE_NAME] = $this->db->get(Worksheet::$TABLE_NAME);

		$this->db->select("*");
		$this->db->from(Worksheet_Awal::$TABLE_NAME);
		$this->db->join(Worksheet::$TABLE_NAME,Worksheet::$TABLE_NAME.".".Worksheet::$ID."=".Worksheet_Awal::$TABLE_NAME.".".Worksheet_Awal::$NOMOR_WS);
		$this->db->where(Worksheet::$IS_DELETED,0);
		$this->db->where(Worksheet_Awal::$TABLE_NAME.".".Worksheet_Awal::$NOMOR_WS, $nomor_ws);
		$data[Worksheet_Awal::$TABLE_NAME] = $this->db->get();

		$this->db->where(Worksheet_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws);
		$data[Worksheet_Checker_Header::$TABLE_NAME] = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);

		$arr_header = array();
		$ctr = 0;
		foreach ($data[Worksheet_Checker_Header::$TABLE_NAME]->result_array() as $row_header) {
			$arr_header[$ctr++] = $row_header[Worksheet_Checker_Header::$ID];
		}
		if ($ctr != 0)
			$this->db->where_in(Worksheet_Checker_Detail::$ID_HEADER, $arr_header);

		$data[Worksheet_Checker_Detail::$TABLE_NAME] = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);

		return $data;
	}
	public function get_last_qty($nomor_ws, $nomor_urut, $qty_awal){
		$qty = $qty_awal;
		$this->db->where(Worksheet_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws);
		$qry = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);
		if ($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row) {
				$this->db->where(Worksheet_Checker_Detail::$ID_HEADER, $row[Worksheet_Checker_Header::$ID]);
				$this->db->where(Worksheet_Checker_Detail::$NOMOR_URUT, $nomor_urut);
				$qry_detail = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);
				if ($qry_detail->num_rows() > 0) {
					foreach ($qry_detail->result_array() as $row_detail) {
						$qty_potong = $row_detail[Worksheet_Checker_Detail::$QTY];
						$qty -= $qty_potong;
					}
				}
			}
		}
		return $qty;
	}
	public function get_product_code_by_nows($nomor_ws){
		$this->db->where(Worksheet_Awal::$NOMOR_WS, $nomor_ws);
		$query = $this->db->get(Worksheet_Awal::$TABLE_NAME);
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row) {
				if ($row[Worksheet_Awal::$KODE_MATERIAL_FG] != null)
					return $row[Worksheet_Awal::$KODE_MATERIAL_FG];
			}
		}
		return null;
	}
	public function get_tin_kemasan($nomor_ws, $id_material){
		/* lama sebelum bug pak Benny
		$total_tin = 0;
		$arr_ws_checker_h = $this->get(Worksheet_Checker_Header::$TABLE_NAME, Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws, null, null, false);
		if ($arr_ws_checker_h->num_rows() > 0) {
			foreach ($arr_ws_checker_h->result_array() as $row_ws_checker_h) {
				$this->db->where(Worksheet_Checker_Detail::$ID_HEADER, $row_ws_checker_h[Worksheet_Checker_Header::$ID]);
				$this->db->where(Worksheet_Checker_Detail::$ID_MATERIAL, $id_material);
				$arr_ws_checker_d = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);
				if ($arr_ws_checker_d->num_rows() > 0) {
					$arr_ws_checker_d = $arr_ws_checker_d->row_array();
					$total_tin += $arr_ws_checker_d[Worksheet_Checker_Detail::$QTY];
				}
			}
		}*/
		$total_tin = 0;
		$arr_ws_checker_h = $this->get(Worksheet_Checker_Header::$TABLE_NAME, Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws, null, null,true);
		if ($arr_ws_checker_h->num_rows() > 0) {
			foreach ($arr_ws_checker_h->result_array() as $row_ws_checker_h) {
				$this->db->where(Worksheet_Checker_Detail::$ID_HEADER, $row_ws_checker_h[Worksheet_Checker_Header::$ID]);
				$this->db->where(Worksheet_Checker_Detail::$ID_MATERIAL, $id_material);
				$arr_ws_checker_d = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);
				if ($arr_ws_checker_d->num_rows() > 0) {
					foreach($arr_ws_checker_d->result_array() as $row_ws_checker_d){
						$total_tin += $row_ws_checker_d[Worksheet_Checker_Detail::$QTY];
					}
				}
			}
		}
		return $total_tin;
	}
	public function get_tin_kemasan_adjustment($nomor_ws, $id_material){
		$total_tin = 0;
		$arr_ws_checker_h = $this->get(Worksheet_Checker_Header::$TABLE_NAME, Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws, null, null,true);
		if ($arr_ws_checker_h->num_rows() > 0) {
			foreach ($arr_ws_checker_h->result_array() as $row_ws_checker_h) {
				$this->db->where(Worksheet_Checker_Detail::$ID_HEADER, $row_ws_checker_h[Worksheet_Checker_Header::$ID]);
				$this->db->where(Worksheet_Checker_Detail::$ID_MATERIAL, $id_material);
				$this->db->where(Worksheet_Checker_Detail::$NOMOR_URUT . $this::$WHERE_IS_NULL, NULL, FALSE);

				$arr_ws_checker_d = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);
				if ($arr_ws_checker_d->num_rows() > 0) {
					foreach($arr_ws_checker_d->result_array() as $row_ws_checker_d){
						$total_tin += $row_ws_checker_d[Worksheet_Checker_Detail::$QTY];
					}
				}
			}
		}
		return $total_tin;
	}
	public function get_utuh($dt, $st){
		$select=[
			Worksheet::$ID,
			Worksheet::$TANGGAL,
			Worksheet::$IS_DELETED
		];
		$this->db->select($select);
		$this->db->from(Worksheet::$TABLE_NAME);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet::$TANGGAL . ' <', $until, TRUE);
		}
		$this->db->where("nomor_worksheet not in (select nomor_worksheet from worksheet_checker_header
																where nomor_worksheet is not null)");
		return $this->db->get();
	}
	public function get_rekap($dt, $st, $nomor_ws, $id_material){
		$select = array(
			Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$ID . " " . Worksheet_Checker_Header::$ID,
			Worksheet_Checker_Header::$TANGGAL,
			Worksheet_Checker_Header::$NOMOR_WS,
			Worksheet_Checker_Header::$NIK,
			Material_RM_PM::$KODE_PANGGIL,
			"sum(" . Worksheet_Checker_Detail::$QTY . ") as " . Worksheet_Checker_Detail::$S_TOT_QTY,
			Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL . " " . Worksheet_Checker_Detail::$ID_MATERIAL,
			Worksheet_Checker_Detail::$ID_VENDOR
		);
		$group_by = array(
			Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$ID,
			Worksheet_Checker_Header::$TANGGAL,
			Worksheet_Checker_Header::$NOMOR_WS,
			Worksheet_Checker_Header::$NIK,
			Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL,
			Material_RM_PM::$KODE_PANGGIL,
			Worksheet_Checker_Detail::$ID_VENDOR
		);
		$this->db->select($select);
		$this->db->from(Worksheet_Checker_Header::$TABLE_NAME);
		$join = Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_HEADER . "=" . Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$ID;
		$this->db->join(Worksheet_Checker_Detail::$TABLE_NAME, $join);
		$join = Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL  . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);
		
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		if ($id_material != NULL) {
			$this->db->where(Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL, $id_material);
		}
		if ($nomor_ws != NULL) {
			$this->db->where(Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws);
		}
		$this->db->where(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$IS_DELETED, 0);

		$this->db->group_by($group_by);
		//$data[H_Good_Receipt::$TABLE_NAME]=$this->get(H_Good_Receipt::ta)
		return $this->db->get();
	}
	public function get_today_info(){
		$today = date("Y-m-d");
		$until = date("Y-m-d", strtotime($today . "+ 1 day"));
		$data = [];

		//WS FG
		$this->db->distinct();
		$this->db->select(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS);
		$join = Worksheet_Awal::$TABLE_NAME . "." . Worksheet_Awal::$NOMOR_WS . "=" . Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS;
		$this->db->join(Worksheet_Awal::$TABLE_NAME, $join);
		$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$KODE_MATERIAL . "=" . Worksheet_Awal::$TABLE_NAME . "." . Worksheet_Awal::$KODE_MATERIAL_RM_PM;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);
		$this->db->where(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS . Library_Model::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' >=', $today, TRUE);
		$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		$this->db->where(Worksheet_Awal::$KODE_MATERIAL_RM_PM . Library_Model::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->where(Material_RM_PM::$JENIS,Checker_RM_PM::$S_TYPE_KEMASAN);
		$data[Worksheet::$S_FG] = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME)->num_rows();

		//WS RM PM
		$this->db->distinct();
		$this->db->select(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS);
		$join = Worksheet_Awal::$TABLE_NAME . "." . Worksheet_Awal::$NOMOR_WS . "=" . Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS;
		$this->db->join(Worksheet_Awal::$TABLE_NAME, $join);
		$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$KODE_MATERIAL . "=" . Worksheet_Awal::$TABLE_NAME . "." . Worksheet_Awal::$KODE_MATERIAL_RM_PM;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);
		$this->db->where(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS . Library_Model::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' >=', $today, TRUE);
		$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		$this->db->where(Worksheet_Awal::$KODE_MATERIAL_RM_PM . Library_Model::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->where(Material_RM_PM::$JENIS."!= ",Checker_RM_PM::$S_TYPE_KEMASAN);
		$data[Worksheet::$S_SEMI] = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME)->num_rows();

		return $data;
	}
	public function get_cancel_by_tanggal($dt, $st){
		$this->db->where(Worksheet_Checker_Header::$IS_DELETED,0);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		return $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);
	}

	public function report_selisih_get_by_tanggal($dt, $st){
		$arr_select = array(
			Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS . " as " . Worksheet_Checker_Header::$NOMOR_WS,
			"sum(" . Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$QTY . ") as " . Worksheet_Checker_Detail::$S_TOT_QTY
		);
		$this->db->select($arr_select);
		$this->db->from(Worksheet_Checker_Header::$TABLE_NAME);
		$this->db->join(Worksheet_Checker_Detail::$TABLE_NAME, Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_HEADER . "=" .
			Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$ID);
		$this->db->where(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS . Library_Model::$WHERE_IS_NOT_NULL, NULL, FALSE);

		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		$this->db->group_by(Worksheet_Checker_Header::$NOMOR_WS);
		return $this->db->get();
	}
	public function connect_transit_get_by($field, $value){
		$arr_select = array(
			Worksheet_Connect::$NO_SEMI, Worksheet_Connect::$NO_FG, Gudang_RM_PM_Transit::$BERAT, Gudang_RM_PM_Transit::$TANGGAL_GENERATE
		);
		$this->db->select($arr_select);
		$this->db->from(Worksheet_Connect::$TABLE_NAME);
		$this->db->join(
			Gudang_RM_PM_Transit::$TABLE_NAME,
			Worksheet_Connect::$TABLE_NAME . "." . Worksheet_Connect::$ID . "=" . Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_CONNECT_WS
		);

		$this->db->where($field, $value);
		return $this->db->get();
	}
	public function connect_get_by_tanggal($dt,$st){
		$arr_select=[
			Gudang_RM_PM_Transit::$TANGGAL_GENERATE,
			Gudang_RM_PM_Transit::$BERAT,
			Worksheet_Connect::$NO_SEMI,
			Worksheet_Connect::$NO_FG,
			Worksheet_Connect::$NIK_PREMIX,
			Worksheet_Connect::$NIK_MAKEUP,
			Worksheet_Connect::$NIK_FILLING,
			Material::$DESCRIPTION,
			Gudang_RM_PM_Transit::$IS_DONE
		];
		$this->db->select($arr_select);
		$this->db->from(Worksheet_Connect::$TABLE_NAME);
		$join = Worksheet_Connect::$TABLE_NAME . "." . Worksheet_Connect::$ID . "=" . Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_CONNECT_WS;
		$this->db->join(Gudang_RM_PM_Transit::$TABLE_NAME, $join);
		$join = Material::$TABLE_NAME . "." . Material::$ID . "=" . Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_MATERIAL_FG;
		$this->db->join(Material::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' >=', $dt, TRUE);
			$this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' <', $until, TRUE);
		}
		return $this->db->get();
	}
	public function is_fg($nomor_ws){
		$data = $this->get(Worksheet_Awal::$TABLE_NAME, Worksheet_Awal::$NOMOR_WS, $nomor_ws, null, null, false);
		if ($data->num_rows() > 0) {
			foreach ($data->result_array() as $row_ws) {
				if ($row_ws[Worksheet_Awal::$KODE_MATERIAL_FG] == NULL) {
					$material_rm_pm = $this->get(
						Material_RM_PM::$TABLE_NAME,
						Material_RM_PM::$KODE_MATERIAL,
						$row_ws[Worksheet_Awal::$KODE_MATERIAL_RM_PM]
					)->row_array();
					if ($this->equals($material_rm_pm[Material_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_KEMASAN))
						return true;
					else if (
						$this->equals($material_rm_pm[Material_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_CAIRAN) ||
						$this->equals($material_rm_pm[Material_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_POWDER)
					)
						return false;
				}
			}
		}
	}
	public function is_semi_single($nomor_ws){
		$this->db->from(Worksheet::$TABLE_NAME);
		$this->db->where(Worksheet::$ID,$nomor_ws);

		$join = Worksheet::$TABLE_NAME.".". Worksheet::$ID_PV."=". BOM_Production_Version::$TABLE_NAME.".". BOM_Production_Version::$ID;
		$this->db->join(BOM_Production_Version::$TABLE_NAME,$join);

		$data_bom=$this->db->get();
		$product_code=$data_bom->row_array()[BOM_Production_Version::$PRODUCT_CODE];
		
		$this->db->where(Material_RM_PM::$KODE_MATERIAL,$product_code);
		$data_material=$this->db->get(Material_RM_PM::$TABLE_NAME);
		if($data_material->num_rows()>0){
			return true;
		}
		return false;

	}
	public function cancel($nomor_gi){
		$this->db->where(Worksheet_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Worksheet_Checker_Header::$ID, $nomor_gi);
		$qry_header = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);

		$ctr = 0;
		if ($qry_header->num_rows() > 0) {
			$header = $qry_header->row_array();

			$this->db->where(Worksheet_Checker_Detail::$ID_HEADER, $header[Worksheet_Checker_Header::$ID]);
			$qry_detail = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);

			$arr_insert = array();
			if ($qry_detail->num_rows() > 0) {
				foreach ($qry_detail->result_array() as $row_detail) {
					if ($row_detail[Worksheet_Checker_Detail::$ID_VENDOR] != null)
						$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $row_detail[Material_RM_PM_Stock::$ID_VENDOR]);

					$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $row_detail[Worksheet_Checker_Detail::$ID_MATERIAL]);
					$material_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME)->row_array();
					//if ($material_stock[Material_RM_PM_Stock::$QTY] > 0) {
						$value_per_unit = 0;
						if($material_stock[Material_RM_PM_Stock::$QTY] > 0)
							$value_per_unit = $material_stock[Material_RM_PM_Stock::$VALUE] / $material_stock[Material_RM_PM_Stock::$QTY];
						$new_stock = $material_stock[Material_RM_PM_Stock::$QTY] + $row_detail[Worksheet_Checker_Detail::$QTY];
						$new_value = $new_stock * $value_per_unit;
						if ($new_stock < 0) {
							return Worksheet::$MESSAGE_FAILED_CANCEL_QTY_NOT_ENOUGH;
						}
						$arr_insert[$ctr] = array(
							Worksheet_Checker_Detail::$ID_MATERIAL => $row_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL],
							Worksheet_Checker_Detail::$ID_VENDOR => $row_detail[Material_RM_PM_Stock::$ID_VENDOR],
							Worksheet_Checker_Detail::$QTY => $row_detail[Purchase_Order_Checker_Detail::$QTY],
							Material_RM_PM_Stock::$ID => $material_stock[Material_RM_PM_Stock::$ID]
						);
						$ctr++;
					//} else
					//	return Worksheet::$MESSAGE_FAILED_CANCEL_QTY_NOT_ENOUGH;
				}
				for ($i = 0; $i < $ctr; $i++) {
					$debit = 0;
					$kredit = 0;
					$net_price = 0;
					$material_rm_pm_stock = $this->get(Material_RM_PM_Stock::$TABLE_NAME, Material_RM_PM_Stock::$ID, $arr_insert[$i][Material_RM_PM_Stock::$ID], null . null, null, false)->row_array();
					if ($material_rm_pm_stock[Material_RM_PM_Stock::$QTY] > 0)
						$net_price=$material_rm_pm_stock[Material_RM_PM_Stock::$VALUE] / $material_rm_pm_stock[Material_RM_PM_Stock::$QTY];
					$new_value=$material_rm_pm_stock[Material_RM_PM_Stock::$VALUE] + ($arr_insert[$i][Worksheet_Checker_Detail::$QTY]*$net_price);
					if ($arr_insert[$i][Worksheet_Checker_Detail::$QTY] < 0) {
						$kredit = abs($arr_insert[$i][Worksheet_Checker_Detail::$QTY]);
					} else {
						$debit = abs($arr_insert[$i][Worksheet_Checker_Detail::$QTY]);
					}
					//$tanggal=date("Y-m-d");
					$tanggal=date("Y-m-d",strtotime($header[Worksheet_Checker_Header::$TANGGAL]));
					//Insert History Stock RM PM
					$this->insert_history_stock(
						$header[Worksheet_Checker_Header::$NOMOR_WS],
						$header[Worksheet_Checker_Header::$NOMOR_WS],
						$tanggal,
						$arr_insert[$i][Worksheet_Checker_Detail::$ID_MATERIAL],
						$arr_insert[$i][Worksheet_Checker_Detail::$ID_VENDOR],
						$material_rm_pm_stock[Material_RM_PM_Stock::$VALUE],
						$debit,
						$kredit,
						$new_value,
						Worksheet::GEN_CANCEL_MESSAGE($header[Worksheet_Checker_Header::$ID], $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])
					);
					$data = array(
						Material_RM_PM_Stock::$QTY => $material_rm_pm_stock[Material_RM_PM_Stock::$QTY] + $arr_insert[$i][Worksheet_Checker_Detail::$QTY],
						Material_RM_PM_Stock::$VALUE => $new_value
					);
					$this->db->where(Material_RM_PM_Stock::$ID, $arr_insert[$i][Material_RM_PM_Stock::$ID]);
					$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
				}
			}
		}
		$data = array(Worksheet_Checker_Header::$IS_DELETED => 1);
		$this->db->where(Worksheet_Checker_Header::$ID, $nomor_gi);
		$this->db->update(Worksheet_Checker_Header::$TABLE_NAME, $data);
		return Worksheet::$MESSAGE_SUCCESS_CANCEL;
	}
	public function change_status($nomor_ws,$is_deleted){
		$this->db->where(Worksheet::$ID,$nomor_ws);
		$data=[];
		$data[Worksheet::$IS_DELETED]=$is_deleted;
		$this->db->update(Worksheet::$TABLE_NAME,$data);
		return Worksheet::$MESSAGE_SUCCESS_CHANGE_STATUS;
	}
	public function plus_print_count($nomor_ws){
		$this->db->where(Worksheet::$ID,$nomor_ws);
		$data_ws = $this->db->get(Worksheet::$TABLE_NAME)->row_array();

		$data = [Worksheet::$CTR_PRINT => $data_ws[Worksheet::$CTR_PRINT]+1];
		$this->db->where(Worksheet::$ID, $nomor_ws);
		$this->db->update(Worksheet::$TABLE_NAME, $data);
	}

	//Kinerja
	public function checker_get_kinerja($dt, $st, $ws = null){
		
		$select = array(
			Worksheet_Checker_Header::$TABLE_NAME . ".". Worksheet_Checker_Header::$NIK." ". Worksheet_Checker_Header::$NIK,
			Material_RM_PM::$JENIS,
			Karyawan::$ID_SUB_KATEGORI, Material_RM_PM::$UNIT,
			"sum(" . Worksheet_Checker_Detail::$QTY . ") as " . Worksheet_Checker_Detail::$S_TOT_QTY,
		);
		$group_by = array(
			Worksheet_Checker_Header::$NIK,
			Material_RM_PM::$JENIS,
			Material_RM_PM::$UNIT,
			Karyawan::$ID_SUB_KATEGORI
		);
		$this->db->select($select);
		$this->db->from(Worksheet_Checker_Header::$TABLE_NAME.",". Worksheet_Connect::$TABLE_NAME);
		$join = Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_HEADER . "=" . Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$ID;
		$this->db->join(Worksheet_Checker_Detail::$TABLE_NAME, $join);
		$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . "=" . Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);
		$join = Karyawan::$TABLE_NAME . "." . Karyawan::$ID . "=" . Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NIK;
		$this->db->join(Karyawan::$TABLE_NAME, $join);

		$this->db->where("(".Worksheet_Connect::$TABLE_NAME.".". Worksheet_Connect::$NO_SEMI."=".Worksheet_Checker_Header::$NOMOR_WS." OR ". Worksheet_Connect::$TABLE_NAME . "." . Worksheet_Connect::$NO_FG."=". Worksheet_Checker_Header::$NOMOR_WS.")");
		
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		if ($ws != NULL) {
			$this->db->where(Worksheet_Checker_Header::$NOMOR_WS, $ws);
		}
		$this->db->group_by($group_by);
		return $this->db->get();
	}
	public function checker_get_kinerja_opname($dt,$st){
		$until = date("Y-m-d", strtotime($st . "+ 1 day"));
		if ($dt != NULL && $st != NULL) {
			return $this->db->query("SELECT nik,sum(case when round(abs(qty_program-qty_fisik))<(0.1*box) then 5000
											else 0 end) as insentif
								FROM (
									SELECT nik,id_material, MAX(tanggal) AS tanggal,qty_program,qty_fisik,material_rm_pm.box as box
									FROM material_rm_pm_fisik,material_rm_pm
									where material_rm_pm.id_material_rm_pm=material_rm_pm_fisik.id_material and material_rm_pm.jenis!='KEMASAN' and
									material_rm_pm_fisik.tanggal >= '" . $dt . "' and material_rm_pm_fisik.tanggal<'" . $until . "'
									GROUP BY id_material 
									order by tanggal desc) as id
								group by nik
								union
								SELECT nik,sum(case when round(abs(qty_program-qty_fisik))=0 then 5000
											else 0 end) as insentif
								FROM (
									SELECT nik,id_material, MAX(tanggal) AS tanggal,qty_program,qty_fisik,material_rm_pm.box as box
									FROM material_rm_pm_fisik,material_rm_pm
									where material_rm_pm.id_material_rm_pm=material_rm_pm_fisik.id_material and material_rm_pm.jenis='KEMASAN' and
									material_rm_pm_fisik.tanggal >= '" . $dt . "' and material_rm_pm_fisik.tanggal<'" . $until . "'
									GROUP BY id_material 
									order by tanggal desc) as id
								group by nik");
		}else{
			return $this->db->query("SELECT nik,sum(case when round(abs(qty_program-qty_fisik))<(0.1*box) then 7500
											else 0 end) as insentif
								FROM (
									SELECT nik,id_material, MAX(tanggal) AS tanggal,qty_program,qty_fisik,material_rm_pm.box as box
									FROM material_rm_pm_fisik,material_rm_pm
									where material_rm_pm.id_material_rm_pm=material_rm_pm_fisik.id_material and material_rm_pm.jenis!='KEMASAN'
									GROUP BY id_material 
									order by tanggal desc) as id
								group by nik
								union
								SELECT nik,sum(case when round(abs(qty_program-qty_fisik))=0 then 7500
											else 0 end) as insentif
								FROM (
									SELECT nik,id_material, MAX(tanggal) AS tanggal,qty_program,qty_fisik,material_rm_pm.box as box
									FROM material_rm_pm_fisik,material_rm_pm
									where material_rm_pm.id_material_rm_pm=material_rm_pm_fisik.id_material and material_rm_pm.jenis='KEMASAN'
									GROUP BY id_material 
									order by tanggal desc) as id
								group by nik");
		}
	}
	public function checker_get_last_opname($dt,$st){
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			return $this->db->query("SELECT material_rm_pm_fisik.nik as nik,description,material_rm_pm_fisik.qty_program qty_program,material_rm_pm_fisik.qty_fisik qty_fisik,(abs(qty_program-qty_fisik)) as selisih,(0.1*box) as toleransi,max_fisik.tanggal tanggal,(case when (abs(qty_program-qty_fisik))<(0.1*box) then 7500
											else 0 end) as insentif
									FROM (
										SELECT max(id_material_rm_pm_fisik) as id_material_rm_pm_fisik,nik,id_material,material_rm_pm.description as description, MAX(tanggal) AS tanggal,material_rm_pm.box as box
										FROM material_rm_pm_fisik mf,material_rm_pm
										where material_rm_pm.id_material_rm_pm=mf.id_material and material_rm_pm.jenis!='KEMASAN' and
										mf.tanggal >= '" . $dt . "' and mf.tanggal<'" . $until ."'
										GROUP BY id_material 
										order by mf.tanggal desc) as max_fisik,material_rm_pm_fisik
										where max_fisik.id_material_rm_pm_fisik=material_rm_pm_fisik.id_material_rm_pm_fisik
									union
									SELECT material_rm_pm_fisik.nik as nik,description,material_rm_pm_fisik.qty_program qty_program,material_rm_pm_fisik.qty_fisik qty_fisik,(abs(qty_program-qty_fisik)) as selisih,(0) as toleransi,max_fisik.tanggal tanggal,(case when (abs(qty_program-qty_fisik))=0 then 7500
											else 0 end) as insentif
									FROM (
										SELECT max(id_material_rm_pm_fisik) as id_material_rm_pm_fisik,nik,id_material,material_rm_pm.description as description, MAX(tanggal) AS tanggal,material_rm_pm.box as box
										FROM material_rm_pm_fisik mf,material_rm_pm
										where material_rm_pm.id_material_rm_pm=mf.id_material and material_rm_pm.jenis='KEMASAN' and
										mf.tanggal >= '" . $dt . "' and mf.tanggal<'" . $until . "'
										GROUP BY id_material 
										order by mf.tanggal desc) as max_fisik,material_rm_pm_fisik
										where max_fisik.id_material_rm_pm_fisik=material_rm_pm_fisik.id_material_rm_pm_fisik
									");
		}
		return $this->db->query("SELECT material_rm_pm_fisik.nik as nik,description,material_rm_pm_fisik.qty_program qty_program,material_rm_pm_fisik.qty_fisik qty_fisik,(abs(qty_program-qty_fisik)) as selisih,(0.1*box) as toleransi,max_fisik.tanggal tanggal,(case when (abs(qty_program-qty_fisik))<(0.1*box) then 7500
											else 0 end) as insentif
									FROM (
										SELECT max(id_material_rm_pm_fisik) as id_material_rm_pm_fisik,nik,id_material,material_rm_pm.description as description, MAX(tanggal) AS tanggal,material_rm_pm.box as box
										FROM material_rm_pm_fisik mf,material_rm_pm
										where material_rm_pm.id_material_rm_pm=mf.id_material and material_rm_pm.jenis!='KEMASAN'
										GROUP BY id_material 
										order by mf.tanggal desc) as max_fisik,material_rm_pm_fisik
										where max_fisik.id_material_rm_pm_fisik=material_rm_pm_fisik.id_material_rm_pm_fisik
									union
									SELECT material_rm_pm_fisik.nik as nik,description,material_rm_pm_fisik.qty_program qty_program,material_rm_pm_fisik.qty_fisik qty_fisik,(abs(qty_program-qty_fisik)) as selisih,(0) as toleransi,max_fisik.tanggal tanggal,(case when (abs(qty_program-qty_fisik))=0 then 7500
											else 0 end) as insentif
									FROM (
										SELECT max(id_material_rm_pm_fisik) as id_material_rm_pm_fisik,nik,id_material,material_rm_pm.description as description, MAX(tanggal) AS tanggal,material_rm_pm.box as box
										FROM material_rm_pm_fisik mf,material_rm_pm
										where material_rm_pm.id_material_rm_pm=mf.id_material and material_rm_pm.jenis='KEMASAN'
										GROUP BY id_material 
										order by mf.tanggal desc) as max_fisik,material_rm_pm_fisik
										where max_fisik.id_material_rm_pm_fisik=material_rm_pm_fisik.id_material_rm_pm_fisik
									");
	}
	public function colour_matcher_get_kinerja($dt, $st,$jenis_nik){
		$select = array(
			$jenis_nik,
			"count(distinct ". Worksheet_Connect::$NO_SEMI .") as " . Worksheet_Connect::$S_COUNT_WS,
		);
		$group_by = array(
			$jenis_nik
		);
		$this->db->select($select);
		$this->db->from(Worksheet_Connect::$TABLE_NAME);
		$join = Gudang_RM_PM_Transit::$TABLE_NAME . "." . Gudang_RM_PM_Transit::$ID_CONNECT_WS . "=" . Worksheet_Connect::$TABLE_NAME . "." . Worksheet_Connect::$ID;
		$this->db->join(Gudang_RM_PM_Transit::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' >=', $dt, TRUE);
			$this->db->where(Gudang_RM_PM_Transit::$TANGGAL_GENERATE . ' <', $until, TRUE);
		}
		$this->db->group_by($group_by);
		return $this->db->get();
		//$this->db->last_query();
	}

	//WS Checker & Admin
	public function checker_get_by_gi($nomor_gi){
		$data = [];

		$this->db->where(Worksheet_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Worksheet_Checker_Header::$ID, $nomor_gi);
		$data[Worksheet_Checker_Header::$TABLE_NAME] = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);


		$this->db->where(Worksheet_Checker_Detail::$ID_HEADER, $nomor_gi);
		$data[Worksheet_Checker_Detail::$TABLE_NAME] = $this->db->get(Worksheet_Checker_Detail::$TABLE_NAME);

		return $data;
	}
	public function checker_insert($ctr, $nomor_ws, $tanggal, $arr_id_material, $arr_vendor, $arr_no_urut, $arr_qty){
		$data_header = [];
		$idx = 1;
		$max = "";

		$max = intval($this->db->query("select max(substr(id_worksheet_checker_header,3)) as max from " . Worksheet_Checker_Header::$TABLE_NAME."
									where ". Worksheet_Checker_Header::$ID." like '". Worksheet::$S_CODE ."%'")->row()->max);
		if ($max == NULL)
			$max = 0;
		$max += 1;

		//Insert WS Checker Header
		$kode = Worksheet::$S_CODE . str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Worksheet_Checker_Header::$ID] = $kode;
		$data_header[Worksheet_Checker_Header::$TANGGAL] = date('Y-m-d', strtotime($tanggal));
		$data_header[Worksheet_Checker_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Worksheet_Checker_Header::$NOMOR_WS] = $nomor_ws;
		$data_header[Worksheet_Checker_Header::$IS_DELETED] = 0;

		//Cek dulu apakah Stock Konsinyasi ada
		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];

			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
			$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($temp->num_rows() > 0) {
				$this->db->where(Material_RM_PM::$ID, $arr_id_material[$i]);
				$kode_material = $this->db->get(Material_RM_PM::$TABLE_NAME)->row_array()[Material_RM_PM::$KODE_MATERIAL];

				$this->db->where(Material_RM_PM::$KODE_MATERIAL, $kode_material);
				$temp_material = $this->db->get(Material_RM_PM::$TABLE_NAME);

				if ($temp_material->num_rows() > 1) {
					$idx = $i;
					$this->db->where(Worksheet_Awal::$NOMOR_URUT, $arr_no_urut[$i]);
					$this->db->where(Worksheet_Awal::$NOMOR_WS, $data_header[Worksheet_Checker_Header::$NOMOR_WS]);
					$qty_awal = $this->db->get(Worksheet_Awal::$TABLE_NAME)->row_array()[Worksheet_Awal::$QTY];
					foreach ($temp_material->result_array() as $row_material) {
						if ($idx == $i) {
							$qty_awal -= $arr_qty[$i];
						} else {
							$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $row_material[Material_RM_PM::$ID]);
							$qry_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
							if ($qry_stock->num_rows() > 0) {
								$stock = $qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
								if ($stock - $qty_awal < 0) //Stock tidak cukup
									return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
							} else {
								return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
							}
						}
						$idx++;
					}
				}
			} else {
				return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
			}
		}

		$this->db->insert(Worksheet_Checker_Header::$TABLE_NAME, $data_header);
		echo $this->alert('Kode GI: ' . $kode);

		$idx_detail_checker = 0;
		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];

			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
			$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($temp->num_rows() > 0) {
				$this->db->where(Material_RM_PM::$ID, $arr_id_material[$i]);
				$kode_material = $this->db->get(Material_RM_PM::$TABLE_NAME)->row_array()[Material_RM_PM::$KODE_MATERIAL];

				$this->db->where(Material_RM_PM::$KODE_MATERIAL, $kode_material);
				$temp_material = $this->db->get(Material_RM_PM::$TABLE_NAME);

				if ($temp_material->num_rows() > 0) {
					$idx = $i;
					foreach ($temp_material->result_array() as $row_material) {
						//Insert PO Checker
						$idx_detail_checker += 1;
						$kode = $data_header[Worksheet_Checker_Header::$ID] . str_pad(($idx_detail_checker), 3, "0", STR_PAD_LEFT);
						$data_detail[Worksheet_Checker_Detail::$ID] = $kode;
						$data_detail[Worksheet_Checker_Detail::$NOMOR_URUT] = $arr_no_urut[$i];
						if ($idx == $i)
							$data_detail[Worksheet_Checker_Detail::$QTY] = $arr_qty[$i];
						else {
							$this->db->where(Worksheet_Awal::$NOMOR_URUT, $data_detail[Worksheet_Checker_Detail::$NOMOR_URUT]);
							$this->db->where(Worksheet_Awal::$NOMOR_WS, $data_header[Worksheet_Checker_Header::$NOMOR_WS]);

							$qty_awal = $this->db->get(Worksheet_Awal::$TABLE_NAME)->row_array()[Worksheet_Awal::$QTY];

							$data_detail[Worksheet_Checker_Detail::$QTY] = $this->get_last_qty(
								$data_header[Worksheet_Checker_Header::$NOMOR_WS],
								$data_detail[Worksheet_Checker_Detail::$NOMOR_URUT],
								$qty_awal
							);

							/*$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL,$row_material[Material_RM_PM::$ID]);
							$qry_stock=$this->db->get(Material_RM_PM_Stock::$TABLE_NAME)->row_array()[Material_RM_PM_Stock::$QTY];
							if($qry_stock->num_rows()>0){
								$stock=$qry_stock->row_array()[Material_RM_PM_Stock::$QTY];
								if($stock-$data_detail[Worksheet_Checker_Detail::$QTY]<0) //Stock tidak cukup
									return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
							}else{
								return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
							}*/
						}
						if ($data_detail[Worksheet_Checker_Detail::$QTY] > 0) {

							$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL] = $row_material[Material_RM_PM::$ID];
							$data_detail[Worksheet_Checker_Detail::$ID_HEADER] = $data_header[Worksheet_Checker_Header::$ID];

							if ($arr_vendor[$i] != null)
								$data_detail[Worksheet_Checker_Detail::$ID_VENDOR] = $arr_vendor[$i];

							$data_vendor = null;
							if ($arr_vendor[$i] != null)
								$data_vendor = $arr_vendor[$i];

							//Insert Stock RM PM
							if ($arr_vendor[$i] != null) {
								$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
							}
							$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $row_material[Material_RM_PM::$ID]);
							$temp_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
							if ($temp_stock->num_rows() > 0) {
								$data = [];
								$stock = $temp_stock->row_array();
								$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
								$qty_awal = $stock[Material_RM_PM_Stock::$QTY];
								$harga_satuan = $value_awal / $qty_awal;
								$data[Material_RM_PM_Stock::$QTY] = $qty_awal - $data_detail[Worksheet_Checker_Detail::$QTY];
								$data[Material_RM_PM_Stock::$VALUE] = $data[Material_RM_PM_Stock::$QTY] * $harga_satuan;

								$data_detail[Worksheet_Checker_Detail::$VALUE] = $harga_satuan * $data_detail[Worksheet_Checker_Detail::$QTY];
								$this->db->insert(Worksheet_Checker_Detail::$TABLE_NAME, $data_detail);

								//Insert History Stock RM PM
								$this->insert_history_stock(
									$data_header[Worksheet_Checker_Header::$NOMOR_WS],
									$data_header[Worksheet_Checker_Header::$NOMOR_WS],
									$data_header[Worksheet_Checker_Header::$TANGGAL],
									$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL],
									$data_vendor,$value_awal,
									0,
									$data_detail[Worksheet_Checker_Detail::$QTY],
									$data[Material_RM_PM_Stock::$VALUE],
									Worksheet_Checker_Detail::GEN_INSERT_MESSAGE(
										$data_header[Worksheet_Checker_Header::$NOMOR_WS],
										$data_header[Worksheet_Checker_Header::$ID],
										$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
									)
								);

								$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $row_material[Material_RM_PM::$ID]);
								if ($arr_vendor[$i] != null) {
									$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
								}
								$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
							}
						}
						$idx++;
					}
				}
			} else {
				return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
			}
		}
		return Worksheet_Checker_Detail::$MESSAGE_SUCCESS_INSERT;
	}
	public function checker_detail_get_qty_by_nows_nourut($nomor_ws, $nomor_urut = null){
		$arr_select = array(
			Worksheet_Checker_Detail::$ID_MATERIAL,
			Worksheet_Checker_Detail::$NOMOR_URUT,
			"sum(" . Worksheet_Checker_Detail::$QTY . ") as qty_total",
			Worksheet_Checker_Detail::$ID_VENDOR
		);
		$this->db->select($arr_select);
		$this->db->from(Worksheet_Checker_Detail::$TABLE_NAME);
		$this->db->join(Worksheet_Checker_Header::$TABLE_NAME, Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_HEADER . "=" . Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$ID);
		$this->db->where(Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws);
		$this->db->where(Worksheet_Checker_Header::$IS_DELETED,0);
		$arr_group_by = array(Worksheet_Checker_Detail::$ID_MATERIAL, Worksheet_Checker_Detail::$ID_VENDOR);

		if ($nomor_urut != null) {
			$this->db->where(Worksheet_Checker_Detail::$NOMOR_URUT, $nomor_urut);
			array_push($arr_group_by, Worksheet_Checker_Detail::$NOMOR_URUT);
		} else
			$this->db->where(Worksheet_Checker_Detail::$NOMOR_URUT . Library_Model::$WHERE_IS_NULL, NULL, FALSE);


		$this->db->group_by($arr_group_by);
		return $this->db->get();
	}
	public function checker_adjustment($ctr, $nomor_ws, $tanggal, $arr_id_material, $arr_vendor, $arr_qty){
		$data_header = [];
		$max = "";

		$checker_rm_pm = $this->get(Checker_RM_PM::$TABLE_NAME, Checker_RM_PM::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array();
		//if ($this->equals($checker_rm_pm[Checker_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_KEMASAN)) {
			if ($this->is_fg($nomor_ws)) {
				$data_ws_fg = $this->get(
					Worksheet_Awal::$TABLE_NAME,
					Worksheet_Awal::$NOMOR_WS,
					$nomor_ws,
					null,
					null,
					false
				);
				$material_rm_pm_ws = null;
				$arr_id_material_rm_pm_ws=[];
				$ctr_rm_pm_ws=0;
				if ($data_ws_fg->num_rows() > 0) {
					foreach ($data_ws_fg->result_array() as $row_ws_fg) {
						if ($row_ws_fg[Worksheet_Awal::$KODE_MATERIAL_FG] == NULL) {
							$material_rm_pm_ws = $this->get(
								Material_RM_PM::$TABLE_NAME,
								Material_RM_PM::$KODE_MATERIAL,
								$row_ws_fg[Worksheet_Awal::$KODE_MATERIAL_RM_PM]
							)->row_array();
							$arr_id_material_rm_pm_ws[$ctr_rm_pm_ws++]=$material_rm_pm_ws[Material_RM_PM::$ID];
							//if ($this->equals($material_rm_pm_ws[Material_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_KEMASAN) && $material_rm_pm_ws[Material_RM_PM::$KEMASAN] != null)
								//break;
						}
					}
				}
				//$is_matched_kemasan = false;
				for ($i = 0; $i < $ctr; $i++) {
					$material_rm_pm = $this->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $arr_id_material[$i])->row_array();
					$is_matched_kemasan = false;
					for($j=0;$j<$ctr_rm_pm_ws;$j++){
						//echo $arr_id_material[$i] . "-" . $material_rm_pm_ws[$j]. "<br>";

						if ($material_rm_pm[Material_RM_PM::$ID] == $arr_id_material_rm_pm_ws[$j])
							$is_matched_kemasan = true;
					}
					if(!$is_matched_kemasan)
						break;

					/*if (
						$this->equals($material_rm_pm[Material_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_KEMASAN) &&
						$material_rm_pm[Material_RM_PM::$KEMASAN] != NULL
					) {
						if ($material_rm_pm[Material_RM_PM::$ID] == $material_rm_pm_ws[Material_RM_PM::$ID])
							$is_matched_kemasan = true;
						else
							$is_matched_kemasan = false;
					}*/
				}

				if (!$is_matched_kemasan) { //jika kemasan yang diadjust berbeda
					return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_KEMASAN_IS_NOT_MATCHED;
				}
			}else{ //WS Semi
				for ($i = 0; $i < $ctr; $i++) {
					$mat_rm_pm=$this->rmpm_model->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$ID,$arr_id_material[$i])->row_array();
					//echo $mat_rm_pm[Material_RM_PM::$JENIS];
					if($this->equals($mat_rm_pm[Material_RM_PM::$JENIS],"KEMASAN"))
						return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_KEMASAN_IS_PM;
 
				}
			}
		//}
		$max = intval($this->db->query("select max(substr(id_worksheet_checker_header,3)) as max from " . Worksheet_Checker_Header::$TABLE_NAME."
									where ". Worksheet_Checker_Header::$ID." like '". Worksheet::$S_CODE ."%'")->row()->max);
		if ($max == NULL)
		$max = 0;
		$max += 1;

		//Insert WS Checker Header
		$kode = Worksheet::$S_CODE . str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Worksheet_Checker_Header::$ID] = $kode;
		$data_header[Worksheet_Checker_Header::$TANGGAL] = date('Y-m-d', strtotime($tanggal));
		$data_header[Worksheet_Checker_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Worksheet_Checker_Header::$NOMOR_WS] = $nomor_ws;
		$data_header[Worksheet_Checker_Header::$IS_DELETED] = 0;
		$this->db->insert(Worksheet_Checker_Header::$TABLE_NAME, $data_header);
		echo $this->alert('Kode GI: ' . $kode);
		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];
			//Insert WS Checker
			$kode = $data_header[Worksheet_Checker_Header::$ID] . str_pad(($i + 1), 3, "0", STR_PAD_LEFT);
			$data_detail[Worksheet_Checker_Detail::$ID] = $kode;
			$data_detail[Worksheet_Checker_Detail::$QTY] = $arr_qty[$i];
			$data_detail[Worksheet_Checker_Detail::$ID_HEADER] = $data_header[Worksheet_Checker_Header::$ID];

			if ($arr_vendor[$i] != null)
				$data_detail[Worksheet_Checker_Detail::$ID_VENDOR] = $arr_vendor[$i];
			$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL] = $arr_id_material[$i];

			$data_vendor = null;
			if ($arr_vendor[$i] != null)
				$data_vendor = $arr_vendor[$i];

			//Insert History Stock RM PM

			$debit = 0;
			$kredit = 0;

			if ($data_detail[Worksheet_Checker_Detail::$QTY] < 0) {
				$debit = abs($data_detail[Worksheet_Checker_Detail::$QTY]);
			} else {
				$kredit = abs($data_detail[Worksheet_Checker_Detail::$QTY]);
			}

			//Insert Stock RM PM
			if ($arr_vendor[$i] != null) {
				$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
			}
			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
			$temp_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($temp_stock->num_rows() > 0) {
				$data = [];
				$stock = $temp_stock->row_array();
				$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
				$qty_awal = $stock[Material_RM_PM_Stock::$QTY];
				if ($qty_awal > 0)
					$harga_satuan = $value_awal / $qty_awal;
				else
					$harga_satuan = 0;
				$data[Material_RM_PM_Stock::$QTY] = $qty_awal - $data_detail[Worksheet_Checker_Detail::$QTY];
				$data[Material_RM_PM_Stock::$VALUE] = $data[Material_RM_PM_Stock::$QTY] * $harga_satuan;
				
				$data_detail[Worksheet_Checker_Detail::$VALUE] = $harga_satuan * $data_detail[Worksheet_Checker_Detail::$QTY];
				$this->db->insert(Worksheet_Checker_Detail::$TABLE_NAME, $data_detail);

				$pesan= Worksheet_Checker_Detail::GEN_ADJUSTMENT_MESSAGE(
					$data_header[Worksheet_Checker_Header::$NOMOR_WS],
					$data_header[Worksheet_Checker_Header::$ID],
					$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
				);
				if($this->is_semi_single($data_header[Worksheet_Checker_Header::$NOMOR_WS])){
					$pesan = Worksheet_Checker_Detail::GEN_INSERT_MESSAGE(
						$data_header[Worksheet_Checker_Header::$NOMOR_WS],
						$data_header[Worksheet_Checker_Header::$ID],
						$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
					);
				}

				$this->insert_history_stock(
					$data_header[Worksheet_Checker_Header::$NOMOR_WS],
					$data_header[Worksheet_Checker_Header::$NOMOR_WS],
					$data_header[Worksheet_Checker_Header::$TANGGAL],
					$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL],
					$data_vendor,$value_awal,
					$debit,
					$kredit,
					$data[Material_RM_PM_Stock::$VALUE],
					$pesan
				);

				$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
				if ($arr_vendor[$i] != null) {
					$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
				}
				$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
			} else {
				return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
			}
		}
		return Worksheet_Checker_Detail::$MESSAGE_SUCCESS_ADJUST;
	}
	public function checker_fg_insert_gr($nomor_ws){
		$this->db->where(Worksheet_Connect::$NO_FG, $nomor_ws);
		$qry = $this->db->get(Worksheet_Connect::$TABLE_NAME);
		$ctr = 0;
		$arr_kode_barang = [];
		$arr_kemasan_barang = [];
		$arr_warna_barang = [];
		$arr_dus_barang = [];
		$arr_tin_barang = [];

		if ($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row_connect) {
				$id_ws_connect = $row_connect[Worksheet_Connect::$ID];
				$this->db->where(Gudang_RM_PM_Transit::$ID_CONNECT_WS, $id_ws_connect);
				$this->db->where(Gudang_RM_PM_Transit::$IS_DONE,0);
				$qry_transit = $this->db->get(Gudang_RM_PM_Transit::$TABLE_NAME);
				if ($qry_transit->num_rows() > 0) {

					//Update Is Inputted
					$data_update=[Gudang_RM_PM_Transit::$IS_DONE => 1];
					$id_ws_connect = $row_connect[Worksheet_Connect::$ID];
					$this->db->where(Gudang_RM_PM_Transit::$ID_CONNECT_WS, $id_ws_connect);
					$this->db->update(Gudang_RM_PM_Transit::$TABLE_NAME,$data_update);

					foreach ($qry_transit->result_array() as $row_transit) {
						$id_material = $row_transit[Gudang_RM_PM_Transit::$ID_MATERIAL_FG];
						$material = $this->get(Material::$TABLE_NAME, Material::$ID, $id_material)->row_array();
						$arr_kode_barang[$ctr] = $material[Material::$KODE_BARANG];
						$arr_kemasan_barang[$ctr] = $material[Material::$KEMASAN];
						$arr_warna_barang[$ctr] = $material[Material::$WARNA];
						$dus = 0;
						$tin = $row_transit[Gudang_RM_PM_Transit::$QTY];
						if ($material[Material::$BOX] > 1) {
							$dus = intdiv($tin, $material[Material::$BOX]);
							$tin %= $material[Material::$BOX];
						}
						$arr_dus_barang[$ctr] = $dus;
						$arr_tin_barang[$ctr] = $tin;
						$ctr++;
					}
				}else
					return Gudang_RM_PM_Transit::$MESSAGE_FAILED_SEARCH_WS_ALREADY_INPUT;

			}
			//Jika Berhasil Set Is Deleted 1 Supaya tidak bisa di Adjust
			$ws_connect = $qry->row_array();
			$this->change_status($ws_connect[Worksheet_Connect::$NO_SEMI], 1);
			$this->change_status($ws_connect[Worksheet_Connect::$NO_FG], 1);

		}
		return $this->goodreceipt_model->insert($ctr, "n", Surat_Jalan::$KEYWORD_SO, $nomor_ws, $arr_kode_barang, $arr_kemasan_barang, $arr_dus_barang, $arr_tin_barang, $arr_warna_barang);
	}
	public function admin_proses_insert($ws_semi, $ws_fg, $kemasan, $tin, $berat, $premix, $makeup, $filling, $tgl_persiapan, $tgl_premix, $tgl_makeup, $tgl_cm, $tgl_qc, $tgl_filling){
		//Insert WS Connect
		$data = [
			Worksheet_Connect::$NO_SEMI => $ws_semi,
			Worksheet_Connect::$NO_FG => $ws_fg,
			Worksheet_Connect::$IS_DELETED => 0,
			Worksheet_Connect::$PERSIAPAN_FROM => $tgl_persiapan[0],
			Worksheet_Connect::$PERSIAPAN_TO => $tgl_persiapan[1],
			Worksheet_Connect::$PREMIX_FROM => $tgl_premix[0],
			Worksheet_Connect::$PREMIX_TO => $tgl_premix[1],
			Worksheet_Connect::$MAKEUP_FROM => $tgl_makeup[0],
			Worksheet_Connect::$MAKEUP_TO => $tgl_makeup[1],
			Worksheet_Connect::$CM_FROM => $tgl_cm[0],
			Worksheet_Connect::$CM_TO => $tgl_cm[1],
			Worksheet_Connect::$QC_FROM => $tgl_qc[0],
			Worksheet_Connect::$QC_TO => $tgl_qc[1],
			Worksheet_Connect::$FILLING_FROM => $tgl_filling[0],
			Worksheet_Connect::$FILLING_TO => $tgl_filling[1],
			Worksheet_Connect::$NIK => $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
		];

		//Cek NIK
		//Salah Satu Kosong
		$nik = null;
		if (($premix != -1 && $makeup == -1 && $filling == -1) || ($premix == -1 && $makeup != -1 && $filling == -1) ||
			($premix == -1 && $makeup == -1 && $filling != -1)
		) {
			if ($premix != -1) $nik = $premix;
			else if ($makeup != -1) $nik = $makeup;
			else if ($filling != -1) $nik = $filling;

			$data[Worksheet_Connect::$NIK_PREMIX] = $nik;
			$data[Worksheet_Connect::$NIK_MAKEUP] = $nik;
			$data[Worksheet_Connect::$NIK_FILLING] = $nik;
		} else { //3 3 terisi
			$data[Worksheet_Connect::$NIK_PREMIX] = $premix;
			$data[Worksheet_Connect::$NIK_MAKEUP] = $makeup;
			$data[Worksheet_Connect::$NIK_FILLING] = $filling;
		}

		//Insert History Update
		$this->db->set(Worksheet_Connect_History::$TANGGAL, 'NOW()', FALSE);
		$this->db->insert(Worksheet_Connect_History::$TABLE_NAME, $data);

		$this->db->where(Worksheet_Connect::$NO_SEMI, $ws_semi);
		$this->db->where(Worksheet_Connect::$NO_FG, $ws_fg);
		$this->db->where(Worksheet_Connect::$IS_DELETED, 0);
		$qry = $this->db->get(Worksheet_Connect::$TABLE_NAME);
		if ($qry->num_rows() > 0) { //UPDATE CM & JAM
			
			$this->db->where(Worksheet_Connect::$NO_SEMI, $ws_semi);
			$this->db->where(Worksheet_Connect::$NO_FG, $ws_fg);
			$this->db->update(Worksheet_Connect::$TABLE_NAME, $data);

			return Worksheet_Connect::$MESSAGE_SUCCESS_UPDATE;
		} else {
			//Pemotongan Perekat
			$potong = false;
			$this->db->where(Worksheet_Awal::$NOMOR_WS, $ws_fg);
			$this->db->where(Worksheet_Awal::$KODE_MATERIAL_FG . Library_Model::$WHERE_IS_NULL, NULL, FALSE);
			$qry_ws_fg = $this->db->get(Worksheet_Awal::$TABLE_NAME);
			if ($qry_ws_fg->num_rows() > 0) {
				foreach ($qry_ws_fg->result_array() as $row_ws_fg) {
					if (in_array($row_ws_fg[Worksheet_Awal::$KODE_MATERIAL_RM_PM], Worksheet::$S_ARR_POTONG_PEREKAT)) {
						$potong = true;
					}
				}
			}

			$this->db->insert(Worksheet_Connect::$TABLE_NAME, $data);
			
			if($ws_fg!=null){
				$id_connect_ws = $this->db->insert_id();

				//Insert Gudang RM PM Transit
				$product_code = $this->get_product_code_by_nows($ws_fg);

				$this->db->like(Material::$PRODUCT_CODE, $product_code, 'both');
				$this->db->not_like(Material::$WARNA, "DT", 'after');
				$this->db->where(Material::$KEMASAN, $kemasan);
				$material_fg = $this->db->get(Material::$TABLE_NAME)->row_array();

				if ($potong) {
					$box = $material_fg[Material::$BOX];
					$dus = intdiv($tin, $box);

					$perekat = $this->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, "2394G")->row_array();

					$row_stock_perekat = $this->get(Material_RM_PM_Stock::$TABLE_NAME, Material_RM_PM_Stock::$ID_MATERIAL, $perekat[Material_RM_PM::$ID], null, null, false)->row_array();
					$old_stock = $row_stock_perekat[Material_RM_PM_Stock::$QTY];
					$net_price = $row_stock_perekat[Material_RM_PM_Stock::$VALUE] / $old_stock;
					$kredit = ((Material_RM_PM::$S_MUL_PEREKAT * $dus) / 10000);
					$new_stock = $old_stock - $kredit;
					$new_value = $new_stock * $net_price;

					//History Stock
					$this->insert_history_stock(
						$ws_fg,
						$ws_fg,
						date('Y-m-d'),
						$perekat[Material_RM_PM::$ID],
						null,
						$row_stock_perekat[Material_RM_PM_Stock::$VALUE],
						0,
						$kredit,
						$new_value,
						Worksheet_Checker_Detail::GEN_INSERT_MESSAGE(
							$ws_fg,
							" - ",
							$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
						)
					);


					$data_perekat = array(
						Material_RM_PM_Stock::$QTY => $new_stock,
						Material_RM_PM_Stock::$VALUE => $new_value
					);
					$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $perekat[Material_RM_PM::$ID]);
					$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data_perekat);
				}

				$data_transit = array(
					Gudang_RM_PM_Transit::$ID_CONNECT_WS => $id_connect_ws,
					Gudang_RM_PM_Transit::$ID_MATERIAL_FG => $material_fg[Material::$ID],
					Gudang_RM_PM_Transit::$BERAT => $berat,
					Gudang_RM_PM_Transit::$QTY => $tin,
					Gudang_RM_PM_Transit::$TANGGAL_GENERATE => date('Y-m-d'),
					Gudang_RM_PM_Transit::$IS_DONE => 0
				);

				$this->db->insert(Gudang_RM_PM_Transit::$TABLE_NAME, $data_transit);

				$data_ws = [Worksheet::$IS_DONE => 1];
				$this->db->where(Worksheet::$ID, $ws_fg);
				$this->db->update(Worksheet::$TABLE_NAME, $data_ws);
			}
			return Worksheet_Connect::$MESSAGE_SUCCESS_INSERT;
		}
	}
	public function admin_proses_get_history($dt, $st, $n){

		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet_Connect_History::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet_Connect_History::$TANGGAL . ' <', $until, TRUE);
		}

		if ($n != null) {
			$this->db->where(Worksheet_Connect_History::$NO_SEMI, $n);
			$this->db->or_where(Worksheet_Connect_History::$NO_FG, $n);
		}

		return $this->db->get(Worksheet_Connect_History::$TABLE_NAME);
	}

	public function info_header_get_informasi($nomor_ws_from,$nomor_ws_to, $hirarki, $dt, $st){
		$arr_select = [
			Worksheet::$TABLE_NAME . "." . Worksheet::$ID . " " . Worksheet::$ID, Worksheet::$TANGGAL, "sum(" . Worksheet_Awal::$QTY . ") as " . Worksheet_Awal::$QTY,
			"(case when " . Worksheet_Awal::$KODE_MATERIAL_RM_PM . " is null then '" . Worksheet::$S_FG . "' else '" . Worksheet::$S_SEMI . "' end) as tipe",
			Worksheet::$IS_DONE, Worksheet::$IS_DELETED, Worksheet::$ID_PV
		];
		$this->db->select($arr_select);
		$this->db->from(Worksheet::$TABLE_NAME);
		$join = Worksheet::$TABLE_NAME . "." . Worksheet::$ID . "=" . Worksheet_Awal::$TABLE_NAME . "." . Worksheet_Awal::$NOMOR_WS;
		$this->db->join(Worksheet_Awal::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet::$TANGGAL . ' <', $until, TRUE);
		}
		if ($nomor_ws_from != NULL) {
			if($nomor_ws_to != NULL){
				$this->db->where(Worksheet::$TABLE_NAME . "." . Worksheet::$ID . ' >=', $nomor_ws_from, TRUE);
				$this->db->where(Worksheet::$TABLE_NAME . "." . Worksheet::$ID . ' <=', $nomor_ws_to, TRUE);
			}else
				$this->db->where(Worksheet::$TABLE_NAME . "." . Worksheet::$ID, $nomor_ws_from);
		}
		// if ($hirarki != NULL) {
		// 	$this->db->where(Worksheet::$TABLE_NAME . "." . Worksheet::$ID, $ws);
		// }
		$arr_group_by = [
			Worksheet::$ID, Worksheet::$TANGGAL,
			"tipe",
			Worksheet::$IS_DONE, Worksheet::$IS_DELETED, Worksheet::$ID_PV
		];
		$this->db->group_by($arr_group_by);
		return $this->db->get();
		//echo $this->db->last_query();
	}
	public function info_detail_get_informasi($nomor_ws_from,$nomor_ws_to, $dt, $st){
		$arr_select = [
			Worksheet_Checker_Header::$TABLE_NAME.".". Worksheet_Checker_Header::$NOMOR_WS,
			Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header:: $ID . " " . Worksheet_Checker_Header::$ID,
			Worksheet_Checker_Detail::$NOMOR_URUT, Worksheet_Checker_Detail::$VALUE, Material_RM_PM::$KODE_MATERIAL,
			Worksheet_Checker_Detail::$QTY,Material_RM_PM::$UNIT,Worksheet_Checker_Header::$TANGGAL,
			Worksheet_Checker_Detail::$ID_VENDOR
		];
		$this->db->select($arr_select);
		$this->db->from(Worksheet_Checker_Header::$TABLE_NAME);
		$this->db->where(Worksheet_Checker_Header::$TABLE_NAME.".".Worksheet_Checker_Header::$IS_DELETED,0);

		$join = Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$ID . "=" . Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_HEADER;
		$this->db->join(Worksheet_Checker_Detail::$TABLE_NAME, $join);

		$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . "=" . Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Worksheet_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		if ($nomor_ws_from != NULL) {
			if ($nomor_ws_to != NULL) {
				$this->db->where(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS . ' >=', $nomor_ws_from, TRUE);
				$this->db->where(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS . ' <=', $nomor_ws_to, TRUE);
			} else
				$this->db->where(Worksheet_Checker_Header::$TABLE_NAME . "." . Worksheet_Checker_Header::$NOMOR_WS, $nomor_ws_from);
		}
		$this->db->order_by("1,2,3");
		return $this->db->get();
	}
}
