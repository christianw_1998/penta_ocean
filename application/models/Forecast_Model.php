<?php
class Forecast{
	public static $TABLE_NAME = "forecast";
	public static $ID = "id_forecast";

	public static $PRODUCT_CODE = "product_code";
	public static $PRODUCT_CODE_DISPLAY = "product_code_display";
	public static $OLD_CODE = "fg_code_old";
	public static $NEW_CODE = "fg_code_new";
	public static $TIN = "tin";
	public static $KG = "kg";
	public static $IS_DELETED = "is_deleted";

	//Foreign Key
	public static $ID_BATCH = "id_forecast_batch";

	//Message
	public static $MESSAGE_SUCCESS_UPLOAD = "FORQ001";

	public static $S_NEEDS_KG = "needs_kg";

}
class Forecast_Batch{
	public static $TABLE_NAME = "forecast_batch";
	public static $ID = "id_forecast_batch";

	public static $QTY = "qty";
	public static $DESCRIPTION = "description";
	public static $TANGGAL = "tanggal";

	public static $S_INSERT_CODE = "FB";

}

class Forecast_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika Forecast tidak kosong
					$product_code = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$new_fg_code = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$description = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$tin = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$kg = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;
					$batch = isset($data['values'][$i]["H"]) ? $data['values'][$i]["H"] : NULL;

					try {
						$tin = doubleval($tin);
						$kg = doubleval($kg);
						$old_code=null;
						$data_fg=$this->fg_model->get(Material::$TABLE_NAME,Material::$ID_NEW,$new_fg_code);
						if($data_fg->num_rows()>0){
							$old_code=$data_fg->row_array()[Material::$ID];
						}
						$kode= null;
						if($batch != "" || $batch != null){
							//Insert Tabel Forecast_Batch
							$kode = Forecast_Batch::$S_INSERT_CODE.date("ymd");
							

							//AUTOGEN Kode Forecast_Batch
							$max = $this->db->query("select max(substr(". Forecast_Batch::$ID .",9)) as max from " . Forecast_Batch::$TABLE_NAME . "
									where ". Forecast_Batch::$ID." like '". $kode . "%'")->row()->max;
							if ($max == NULL)
								$max = 0;
							$max += 1;

							$kode .= str_pad($max, 3, "0", STR_PAD_LEFT);
						
							$data_batch =[
								Forecast_Batch::$ID => $kode,
								Forecast_Batch::$DESCRIPTION => $description,
								Forecast_Batch::$QTY => $batch
							];
							$this->db->set(Forecast_Batch::$TANGGAL, 'NOW()', FALSE);
							$this->db->insert(Forecast_Batch::$TABLE_NAME,$data_batch);
						}
						if($kode == null){
							$this->db->where("YEAR(" . Forecast_Batch::$TANGGAL . ")", date("Y"));
							$this->db->where("MONTH(" . Forecast_Batch::$TANGGAL . ")", date("m"));
							$this->db->where("DAY(" . Forecast_Batch::$TANGGAL . ")", date("d"));
							$this->db->where(Forecast_Batch::$DESCRIPTION, $description);

							$data_forecast_batch=$this->db->get(Forecast_Batch::$TABLE_NAME);
							if($data_forecast_batch->num_rows()>0){
								$kode = $data_forecast_batch->row_array()[Forecast_Batch::$ID];
							}
						}
						if($kg!=0 && $tin !=0){
							//Insert Tabel Forecast
							//Split Product Code
							$split=substr($product_code,0,4);
							//explode("-",$product_code);
							$data_forecast = [
								Forecast::$PRODUCT_CODE => $split,
								Forecast::$PRODUCT_CODE_DISPLAY => $product_code,
								Forecast::$NEW_CODE => $new_fg_code,
								Forecast::$TIN => $tin,
								Forecast::$OLD_CODE => $old_code,
								Forecast::$KG => $kg,
								Forecast::$IS_DELETED => 0,
								Forecast::$ID_BATCH => $kode
							];
							$this->db->insert(Forecast::$TABLE_NAME,$data_forecast);
						}
					} catch (Exception $e) {
						
					}
				} else
					break;
			}
		}
		return Forecast::$MESSAGE_SUCCESS_UPLOAD;
	}

	public function get_needs($forecast_batch,$tipe){
		$today = date("Y-m-d");
		if($this->equals($tipe,Material_RM_PM::$S_UNIT_KG)){
			return $this->db->query("SELECT bom_alt_recipe.id_material_rm_pm,material_rm_pm.kode_material,round(sum(bom_alt_recipe.qty*batch_totals.kg/100/pembagi_kg.pembagi),2) needs_kg
										from forecast,forecast_batch,bom,bom_alt,bom_alt_recipe,material_rm_pm,bom_production_version,
											(SELECT forecast.product_code fp,sum(kg) as kg
											from forecast,forecast_batch
											where forecast.id_forecast_batch=forecast_batch.id_forecast_batch and forecast_batch.id_forecast_batch like '$forecast_batch%'
											group by forecast.product_code) as batch_totals,
											(SELECT forecast.product_code,count(*) as pembagi
												from forecast,forecast_batch
												where forecast.id_forecast_batch=forecast_batch.id_forecast_batch and forecast_batch.id_forecast_batch like '$forecast_batch%'
												GROUP by forecast.product_code) as pembagi_kg
										where forecast.product_code=bom.product_code and bom.product_code=bom_alt.product_code and bom_alt.id_bom_alt=bom_alt_recipe.id_bom_alt and batch_totals.fp=forecast.product_code and material_rm_pm.id_material_rm_pm=bom_alt_recipe.id_material_rm_pm and forecast.product_code=pembagi_kg.product_code
										and bom_production_version.id_bom_alt=bom_alt.id_bom_alt
										and bom_production_version.valid_from<='$today' and bom_production_version.valid_to>='$today'
										and forecast.id_forecast_batch=forecast_batch.id_forecast_batch 
										and forecast_batch.id_forecast_batch like '$forecast_batch%'
										group by bom_alt_recipe.id_material_rm_pm 
										ORDER BY `forecast`.`product_code`  DESC");
		}
		return $this->db->query("SELECT bom_alt_recipe.id_material_rm_pm,floor(sum((forecast.tin/material.box)*bom_alt_recipe.qty)) needs_kg
									FROM forecast,forecast_batch,bom,bom_alt,bom_alt_recipe,material,bom_production_version
									where forecast.fg_code_old=bom.product_code and bom.product_code=bom_alt.product_code and bom_alt.id_bom_alt=bom_alt_recipe.id_bom_alt and id_material_rm_pm is not null and material.id_material=forecast.fg_code_old
									and bom_alt.id_bom_alt=bom_alt_recipe.id_bom_alt and bom_production_version.id_bom_alt=bom_alt.id_bom_alt
									and bom_production_version.valid_from<='$today' and bom_production_version.valid_to>='$today'
									and forecast.id_forecast_batch=forecast_batch.id_forecast_batch 
									and forecast_batch.id_forecast_batch like '$forecast_batch%'
									group by bom_alt_recipe.id_material_rm_pm");
	}
	public function get_table_needs(){
		$bulan_akhir = 0;
		$bulan_awal = 0;
		//ambil bulan awal
		$this->db->select(Worksheet_Checker_Header::$TANGGAL);
		$this->db->where("YEAR(" . Worksheet_Checker_Header::$TANGGAL . ")", date("Y") - 1);
		$this->db->order_by(
			"1",
			Library_Model::$ORDER_TYPE_ASC
		);
		$data_awal = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);
		if ($data_awal->num_rows() > 0) {
			$tanggal_awal = $data_awal->row_array()[Worksheet_Checker_Header::$TANGGAL];
			$bulan_awal = intval(date("m", strtotime($tanggal_awal)));
		}

		//ambil bulan akhir
		$this->db->select(Worksheet_Checker_Header::$TANGGAL);
		$this->db->where("YEAR(" . Worksheet_Checker_Header::$TANGGAL . ")", date("Y") - 1);
		$this->db->order_by(
			"1",
			Library_Model::$ORDER_TYPE_DESC
		);
		$data_akhir = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);
		if ($data_akhir->num_rows() > 0) {
			$tanggal_akhir = $data_akhir->row_array()[Worksheet_Checker_Header::$TANGGAL];
			$bulan_akhir = intval(date("m", strtotime($tanggal_akhir)));
		}
		$select = "";
		if ($bulan_awal == 0 && $bulan_akhir == 0) { //Tidak ada Transaksi Tahun Lalu
			$select = "SUM(" . Worksheet_Checker_Detail::$QTY . ")/" . ($bulan_akhir - $bulan_awal);
		} else {
			$select = "0";
		}
		$arr_select = [
			Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . " " . Material_RM_PM::$ID, Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION, $select . " as " . Purchase_Request::$S_BUFFER_STOCK
		];

		$this->db->select($arr_select);
		$this->db->from(Worksheet_Checker_Detail::$TABLE_NAME);
		$join = Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);

		$arr_group_by = [
			Material_RM_PM::$ID, Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION
		];
		$this->db->group_by($arr_group_by);

		return $this->db->get();
	}
	public function get_history($dt, $st){
		$arr_select=[
			"*"
		];

		$this->db->select($arr_select);
		$this->db->from(Forecast::$TABLE_NAME);

		$join = Forecast_Batch::$TABLE_NAME . "." . Forecast_Batch::$ID . "=" . Forecast::$TABLE_NAME . "." . Forecast::$ID_BATCH;
		$this->db->join(Forecast_Batch::$TABLE_NAME, $join);

		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Forecast_Batch::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Forecast_Batch::$TANGGAL . ' <', $until, TRUE);
		}

		return $this->db->get();
	}
	public function batch_get_by_tanggal($dt, $st){
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Forecast_Batch::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Forecast_Batch::$TANGGAL . ' <', $until, TRUE);
		}
		return $this->db->get(Forecast_Batch::$TABLE_NAME);
	}
}
