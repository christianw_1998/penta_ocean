<?php
class Kalender{
	public static $TABLE_NAME="kalender";
	public static $ID="id_kalender";
	
	public static $TANGGAL="tanggal";
	public static $KETERANGAN="keterangan";
	public static $IS_DELETED = "is_deleted";
	
	public static $MESSAGE_SUCCESS_INSERT="CAQ001";
	public static $MESSAGE_SUCCESS_UPDATE="CAQ002";
	public static $MESSAGE_SUCCESS_DELETE="CAQ003";
	public static $MESSAGE_FAILED_DELETE="CAQ004";
	public static $MESSAGE_FAILED_INSERT_UPDATE_DATE_NULL="CAQ005";
	public static $MESSAGE_FAILED_INSERT_UPDATE_KETERANGAN_NULL="CAQ006";
}

class Kalender_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}


}
