<?php

class Kurs{
	public static $TABLE_NAME = "kurs";
	public static $ID = "id_kurs";

	public static $VALUE = "kurs";
	public static $TANGGAL_BERLAKU = "tanggal_berlaku";

	public static $MESSAGE_SUCCESS_INSERT = "KURQ001";

}

class Kurs_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function get_last($tanggal = null){
		$this->db->select(Kurs::$VALUE);
		$this->db->order_by(Kurs::$TANGGAL_BERLAKU,$this::$ORDER_TYPE_DESC);
		
		if($tanggal == null)
			$tanggal = date("Y-m-d");
		$this->db->where(Kurs::$TANGGAL_BERLAKU . ' <=', $tanggal , TRUE);
		
		return $this->db->get(Kurs::$TABLE_NAME);
	}
}
