<?php
class H_Good_Receipt{
	public static $TABLE_NAME = "h_good_receipt";
	public static $ID = "id_h_good_receipt";
	public static $TANGGAL = "tanggal";
	public static $TANGGAL_CONFIRM_ADMIN = "tanggal_confirm_admin";
	public static $NOMOR_SJ = "nomor_sj";
	public static $STO = "sto";
	public static $QTY = "qty";
	public static $KODE_REVISI = "kode_revisi";
	public static $REV_GOOD_ISSUE = "rev_good_issue";
	public static $IS_DELETED="is_deleted";

	//Foreign Key
	public static $NIK = "nik";
	public static $NIK_SOPIR = "nik_sopir";
	public static $NIK_ADMIN = "nik_admin";
	public static $NIK_KERNET = "nik_kernet";

	public static $REV_CODE = "REVGR";
	public static $ADJ_CODE = "ADJGR";
	public static $STO_CODE = "8";
	public static $SO_CODE = "21";
	public static $RETUR_CODE = "RT";
	public static $SIRCLO_CODE = "OUT";

	//Static Variable
	public static $S_TOT_QTY="s_tot_qty";
	public static $S_JENIS="jenis";

	public static $MESSAGE_SUCCESS_INSERT = "HGRQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "HGRQ003";
	public static $MESSAGE_SUCCESS_ADJUSTMENT = "HGRQ005";
	public static $MESSAGE_SUCCESS_UPDATE_KINERJA = "HGRQ008";

	public static $MESSAGE_FAILED_INSERT_SJ_NOT_FOUND = "HGRQ002";
	public static $MESSAGE_FAILED_INSERT_IS_NOT_PERMITTED = "HGRQ006";
	public static $MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND = "HGRQ004";
	public static $MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED = "HGRQ007";

	public static function GEN_INSERT_MESSAGE($params, $params2){
		return "Penambahan dari Surat Jalan no " . $params . " (Kode: " . $params2 . ")";
	}
	public static function GEN_REVISION_MESSAGE($params, $params2, $params3){
		return "Revisi SJ no " . $params . " (Kode Revisi: " . $params2 . ", Kode: " . $params3 . ")";
	}
	public static function GEN_ADJUSTMENT_MESSAGE($params, $params2){
		return "Adjustment oleh " . $params . " (Kode: " . $params2 . ")";
	}
}
class D_Good_Receipt{
	public static $TABLE_NAME = "d_good_receipt";
	public static $ID = "id_d_good_receipt";
	public static $DUS = "dus";
	public static $TIN = "tin";

	//Foreign Key
	public static $ID_MATERIAL = "id_material_fg";
	public static $ID_H_GOOD_RECEIPT = "id_h_good_receipt";
	public static $ID_GUDANG = "id_gudang";

	public static $MESSAGE_SUCCESS_INSERT = "DGRQ001";
	public static $MESSAGE_FAILED_INSERT = "DGRQ002";
}
class GoodReceipt_Model extends FG_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function insert($ctr, $revisi, $sto, $nomor_sj, $arr_kode_barang, $arr_kemasan, $arr_dus, $arr_tin, $arr_warna){
		$arr = [];
		$this->db->where(H_Good_Receipt::$NOMOR_SJ, $nomor_sj);
		$this->db->where(H_Good_Receipt::$KODE_REVISI . $this::$WHERE_IS_NULL, NULL, FALSE);
		$temp = $this->db->get(H_Good_Receipt::$TABLE_NAME);
		if ($temp->num_rows() == 0 && $this->equals($revisi, "y")) {
			$this->db->where(H_Good_Issue::$KODE_REVISI . $this::$WHERE_IS_NULL, NULL, FALSE);
			$this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);
			$temp1 = $this->db->get(H_Good_Issue::$TABLE_NAME);
			if ($temp1->num_rows() == 0)
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_SJ_NOT_FOUND;
			else
				$arr[H_Good_Receipt::$REV_GOOD_ISSUE] = 1;
		}

		//Auto Generate Kode Surat Jalan
		$this->db->select("max(substr(" . H_Good_Receipt::$ID . ",8)) as " . H_Good_Receipt::$ID);
		$this->db->where("MONTH(" . H_Good_Receipt::$TANGGAL . ")", date("m"));
		$this->db->where("YEAR(" . H_Good_Receipt::$TANGGAL . ")", date("Y"));
		$qry_sj= $this->db->get(H_Good_Receipt::$TABLE_NAME)->row_array();
		$max = $qry_sj[H_Good_Receipt::$ID];
		if ($max == null) $max = 0;
		$max += 1;

		//Header
		$kode = date("ym") . str_pad($max, 6, "0", STR_PAD_LEFT);
		$arr[H_Good_Receipt::$ID] = "GR" . $kode;
		$arr[H_Good_Receipt::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$no_sj_insert = "";
		if ($this->equals($revisi, "n")) {
			if ($this->equals($sto, Surat_Jalan::$KEYWORD_SO))
				$no_sj_insert = H_Good_Receipt::$SO_CODE . date("y") . str_pad($nomor_sj, 7, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_STO))
				$no_sj_insert = H_Good_Receipt::$STO_CODE . date("y") . str_pad($nomor_sj, 8, "0", STR_PAD_LEFT);
			else
				$no_sj_insert = H_Good_Receipt::$RETUR_CODE . date("y") ."-". str_pad($nomor_sj, 7, "0", STR_PAD_LEFT);
			$arr[H_Good_Receipt::$NOMOR_SJ] = strtoupper($no_sj_insert);

			//cek apakah surat jalan masuk sudah ada atau belum
			$this->db->where(H_Good_Receipt::$NOMOR_SJ, $no_sj_insert);
			$this->db->where(H_Good_Receipt::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$temp = $this->db->get(H_Good_Receipt::$TABLE_NAME);
			if ($temp->num_rows() > 0 && $this->equals($revisi, "n")) {
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED;
			}
		} else {
			//cek apakah akses revisi diberikan
			$this->db->where(List_Access::$ACCESS, ACCESS_REVISION_SURAT_JALAN);
			$id_access = $this->db->get(List_Access::$TABLE_NAME)->row_array()[List_Access::$ID];

			//$arr_access[User_Access::$IS_TRUE]=0;
			$this->db->where(User_Access::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$this->db->where(User_Access::$ID_ACCESS, $id_access);
			$this->db->where(User_Access::$IS_TRUE,1);
			$is_permitted = $this->db->get(User_Access::$TABLE_NAME);

			//jika diijinkan
			if ($is_permitted->num_rows() > 0) { //ada yang mengijinkan
				//update revisi set 0
				$arr_access[User_Access::$IS_TRUE] = 0;
				$this->db->where(User_Access::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
				$this->db->where(User_Access::$ID_ACCESS, $id_access);
				$this->db->update(User_Access::$TABLE_NAME, $arr_access);

				$kd = strtoupper(H_Good_Receipt::$REV_CODE) . date("y");
				$max_rev_code = $this->db->query("select max(substr(kode_revisi,8)) as max from " . H_Good_Receipt::$TABLE_NAME . "
									where upper(kode_revisi) like '%" . $kd . "%'")->row()->max;
				if ($max_rev_code == null)
					$max_rev_code = 0;
				$max_rev_code += 1;
				$arr[H_Good_Receipt::$KODE_REVISI] = $kd . str_pad($max_rev_code, 6, "0", STR_PAD_LEFT);
				$arr[H_Good_Receipt::$NOMOR_SJ] = strtoupper($nomor_sj);
			} else
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_IS_NOT_PERMITTED;
		}
		$arr[H_Good_Receipt::$STO] = $sto;
		$this->db->set(H_GOOD_RECEIPT::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Receipt::$QTY] = $ctr;
		$arr[H_Good_Receipt::$IS_DELETED] = 0;
		$this->db->insert(H_Good_Receipt::$TABLE_NAME, $arr);
		$ctr_d_good_receipt = 1;
		for ($i = 0; $i < $ctr; $i++) {
			$id_gudang = "";

			//arr id gudang harus dicari dulu, looping untuk gudang yang material nya adalah barang itu, cek dengan fifo qtynya
			$this->db->where(Material::$KEMASAN, $arr_kemasan[$i]);
			$this->db->where(Material::$WARNA, $arr_warna[$i]);
			$this->db->where("Upper(" . Material::$KODE_BARANG . ")", strtoupper($arr_kode_barang[$i]));
			$material = $this->db->get(Material::$TABLE_NAME)->row_array();

			//cek dulu apakah material bergudang
			$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $material[Material::$ID]);
			if (($qry_gudang->num_rows() == 0)){
				$this->db->where(H_Good_Receipt::$ID, $arr[H_Good_Receipt::$ID]);
				$this->db->delete(H_Good_Receipt::$TABLE_NAME);
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
			}

			$fifo=$this->get(Fifo_Stock_Gudang::$TABLE_NAME,Fifo_Stock_Gudang::$ID_MATERIAL,$material[Material::$ID],Fifo_Stock_Gudang::$ID,$this::$ORDER_TYPE_DESC,false);
			$is_first_time=true;

			if($fifo->num_rows()>0){ //Jika Material tsb sudah pernah ada di rak
				$is_first_time=false;
			}
			$ctr_first_time=1; //ctr untuk perulangan
			if($is_first_time){
				$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $material[Material::$ID]);
			}else{
				$ctr_first_time = 2;
				$last_fifo_id_gudang = $fifo->row_array()[Fifo_Stock_Gudang::$ID_GUDANG];
				$qry_gudang = $this->fg_model->gudang_get_by_last_fifo($last_fifo_id_gudang,$material[Material::$ID]);
			}
			$stock_in = ($arr_dus[$i] * $material[Material::$BOX]) + $arr_tin[$i];
			
			for($j=0;$j<$ctr_first_time;$j++){
				if (($qry_gudang->num_rows() > 0 && $ctr_first_time==1) || $ctr_first_time==2) {
					foreach ($qry_gudang->result_array() as $row_gudang) {
						if ($stock_in <= 0) {
							break;
						} else {
							$kapasitas_total = $row_gudang[Gudang::$KAPASITAS] * $material[Material::$BOX];
							$kapasitas_terisi = $this->fifo_get_terisi_by_rak(
								$row_gudang[Gudang::$RAK],$row_gudang[Gudang::$KOLOM],$row_gudang[Gudang::$TINGKAT])->row_array()[Fifo_Stock_Gudang::$S_TERISI];
							$kapasitas_sisa = $kapasitas_total - $kapasitas_terisi;
							if (($kapasitas_terisi == 0 && !$this->equals($row_gudang[Gudang::$RAK], Gudang::$S_TRANSIT)) || $this->equals($row_gudang[Gudang::$RAK], Gudang::$S_TRANSIT)) { //jika kapasitas masih kosong
								$total_tin=0;
								if ($kapasitas_sisa - $stock_in >= 0) //jika kapasitas masih mencukupi
									$total_tin = $stock_in;
								else
									$total_tin = $kapasitas_sisa;
								$stock_in -= $total_tin;

								$arr_detail = [];
								$arr_detail[D_Good_Receipt::$ID] = "DGR" . $kode . str_pad($ctr_d_good_receipt++, "3", "0", STR_PAD_LEFT);

								$stock_dus = 0;
								$stock_tin = 0;
								if ($material[Material::$BOX] > 1) {
									$stock_dus = intdiv($total_tin, $material[Material::$BOX]);
									$stock_tin = $total_tin % $material[Material::$BOX];
								} else {
									$stock_tin = $total_tin;
								}
								$id_gudang_loop = $row_gudang[Gudang::$ID];

								$arr_detail[D_Good_Receipt::$DUS] = $stock_dus;
								$arr_detail[D_Good_Receipt::$TIN] = $stock_tin;
								$arr_detail[D_Good_Receipt::$ID_GUDANG] = $id_gudang_loop;

								$arr_detail[D_Good_Receipt::$ID_MATERIAL] = $material[Material::$ID];
								$arr_detail[D_Good_Receipt::$ID_H_GOOD_RECEIPT] = $arr[H_Good_Receipt::$ID];
								$this->db->insert(D_Good_Receipt::$TABLE_NAME, $arr_detail);

								//Penambahan Stock Gudang
								$pesan = "";
								if ($this->equals($revisi, "n"))
									$pesan = H_Good_Receipt::GEN_INSERT_MESSAGE($no_sj_insert, $arr[H_Good_Receipt::$ID]);
								else
									$pesan = H_Good_Receipt::GEN_REVISION_MESSAGE($arr[H_Good_Receipt::$NOMOR_SJ], $arr[H_Good_Receipt::$KODE_REVISI], $arr[H_Good_Receipt::$ID]);
								$this->insert_history_stock($material[Material::$ID], $id_gudang_loop, $total_tin, 0, $pesan);

								//Fifo Stock Gudang
								//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
								$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $material[Material::$ID]);
								$this->db->order_by(Fifo_Stock_Gudang::$ID, $this::$ORDER_TYPE_DESC);

								$qry_fifo = $this->db->get(Fifo_Stock_Gudang::$TABLE_NAME);
								$id_gudang = "";

								if ($qry_fifo->num_rows() > 0) {
									$qry_fifo = $qry_fifo->row_array();
									$id_gudang = $qry_fifo[Fifo_Stock_Gudang::$ID_GUDANG];
								}
								$arr_fifo = [];
								if ($id_gudang != $id_gudang_loop) {
									$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $material[Material::$ID];
									$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $total_tin;
									$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								} else {
									$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo[Fifo_Stock_Gudang::$ID]);
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo[Fifo_Stock_Gudang::$QTY] + $total_tin;
									$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								}
							}else{
								break;
							}
						}
					}
					if(!$is_first_time){ //jika tidak pertama kali, ulang dari awal, cek kapasitas rak 0 atau tidak
						$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $material[Material::$ID]);
					}
				} else {
					$this->db->where(H_Good_Receipt::$ID, $arr[H_Good_Receipt::$ID]);
					$this->db->delete(H_Good_Receipt::$TABLE_NAME);
					return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
				}
			}
			if ($stock_in > 0) { //Masuk Cadangan
				$this->db->where(Gudang::$RAK, Gudang::$S_CADANGAN);
				$this->db->where(Gudang::$NAMA_GUDANG, $qry_gudang->row_array()[Gudang::$NAMA_GUDANG]);
				$qry_temp_gudang = $this->db->get(Gudang::$TABLE_NAME);
				$id_gudang_loop = $qry_temp_gudang->row_array()[Gudang::$ID];

				//Penambahan Stock Gudang
				$pesan = "";
				if ($this->equals($revisi, "n"))
					$pesan = H_Good_Receipt::GEN_INSERT_MESSAGE($no_sj_insert, $arr[H_Good_Receipt::$ID]);
				else
					$pesan = H_Good_Receipt::GEN_REVISION_MESSAGE($arr[H_Good_Receipt::$NOMOR_SJ], $arr[H_Good_Receipt::$KODE_REVISI], $arr[H_Good_Receipt::$ID]);
				$this->insert_history_stock($material[Material::$ID], $id_gudang_loop, $stock_in, 0, $pesan);

				//Fifo Stock Gudang
				//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
				$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $material[Material::$ID]);
				$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG, $id_gudang_loop);
				$qry_fifo = $this->db->get(fifo_stock_gudang::$TABLE_NAME);

				$arr_fifo = [];
				if ($qry_fifo->num_rows() == 0) {
					$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $material[Material::$ID];
					$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
					$arr_fifo[Fifo_Stock_Gudang::$QTY] = $stock_in;
					$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
				} else {
					$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo->row_array()[Fifo_Stock_Gudang::$ID]);
					$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo->row_array()[Fifo_Stock_Gudang::$QTY] + $stock_in;
					$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
				}

				$stock_dus = 0;
				$stock_tin = 0;
				if ($material[Material::$BOX] > 1) {
					$stock_dus = intdiv($stock_in, $material[Material::$BOX]);
					$stock_tin = $stock_in % $material[Material::$BOX];
				} else {
					$stock_tin = $stock_in;
				}
				$arr_detail = [];
				$arr_detail[D_Good_Receipt::$ID] = "DGR" . $kode . str_pad($ctr_d_good_receipt++, "3", "0", STR_PAD_LEFT);
				$arr_detail[D_Good_Receipt::$DUS] = $stock_dus;
				$arr_detail[D_Good_Receipt::$TIN] = $stock_tin;
				$arr_detail[D_Good_Receipt::$ID_GUDANG] = $id_gudang_loop;

				$arr_detail[D_Good_Receipt::$ID_MATERIAL] = $material[Material::$ID];
				$arr_detail[D_Good_Receipt::$ID_H_GOOD_RECEIPT] = $arr[H_Good_Receipt::$ID];
				$this->db->insert(D_Good_Receipt::$TABLE_NAME, $arr_detail);
			}
		}
		
		echo $this->alert('Kode GR: ' . $arr[H_Good_Receipt::$ID]);
		return H_Good_Receipt::$MESSAGE_SUCCESS_INSERT;
	}
	public function insert_adjustment($ctr, $keterangan, $arr_kode_barang, $arr_kemasan, $arr_dus, $arr_tin, $arr_warna){
		//Auto Generate Kode Surat Jalan
		$this->db->select("max(substr(" . H_Good_Receipt::$ID . ",8)) as " . H_Good_Receipt::$ID);
		$this->db->where("MONTH(" . H_Good_Receipt::$TANGGAL . ")", date("m"));
		$this->db->where("YEAR(" . H_Good_Receipt::$TANGGAL . ")", date("Y"));
		$qry_sj = $this->db->get(H_Good_Receipt::$TABLE_NAME)->row_array();
		$max = $qry_sj[H_Good_Receipt::$ID];
		if ($max == null) $max = 0;
		$max += 1;

		//Header
		$kode = date("ym") . str_pad($max, 6, "0", STR_PAD_LEFT);
		$arr = [];
		$arr[H_Good_Receipt::$ID] = "GR" . $kode;
		$arr[H_Good_Receipt::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];

		$kd = strtoupper(H_Good_Receipt::$ADJ_CODE);
		$max_rev_code = $this->db->query("select max(substr(nomor_sj,7)) as max from " . H_Good_Receipt::$TABLE_NAME . "
							where upper(nomor_sj) like '%" . $kd . "%'")->row()->max;
		if ($max_rev_code == null)
		$max_rev_code = 0;
		$max_rev_code += 1;
		$arr[H_Good_Receipt::$NOMOR_SJ] = $kd . str_pad($max_rev_code, 6, "0", STR_PAD_LEFT);

		$this->db->set(H_GOOD_RECEIPT::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Receipt::$QTY] = $ctr;
		$arr[H_Good_Receipt::$IS_DELETED] = 0;
		$this->db->insert(H_Good_Receipt::$TABLE_NAME, $arr);
		$ctr_d_good_receipt = 1;
		for ($i = 0; $i < $ctr; $i++) {
			$id_gudang = "";
			//arr id gudang harus dicari dulu, looping untuk gudang yang material nya adalah barang itu, cek dengan fifo qtynya

			$this->db->where(Material::$KEMASAN, $arr_kemasan[$i]);
			$this->db->where(Material::$WARNA, $arr_warna[$i]);
			$this->db->where("Upper(" . Material::$KODE_BARANG . ")", strtoupper($arr_kode_barang[$i]));
			$material = $this->db->get(Material::$TABLE_NAME)->row_array();

			//cek dulu apakah material bergudang
			$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $material[Material::$ID]);
			if (($qry_gudang->num_rows() == 0)) {
				$this->db->where(H_Good_Receipt::$ID, $arr[H_Good_Receipt::$ID]);
				$this->db->delete(H_Good_Receipt::$TABLE_NAME);
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
			}

			$fifo = $this->get(Fifo_Stock_Gudang::$TABLE_NAME, Fifo_Stock_Gudang::$ID_MATERIAL, $material[Material::$ID], Fifo_Stock_Gudang::$ID, $this::$ORDER_TYPE_DESC, false);
			$is_first_time = true;

			if ($fifo->num_rows() > 0) { //Jika Material tsb sudah pernah ada di rak
				$is_first_time = false;
			}
			$ctr_first_time = 1; //ctr untuk perulangan
			if ($is_first_time) {
				$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $material[Material::$ID]);
			} else {
				$ctr_first_time = 2;
				$last_fifo_id_gudang = $fifo->row_array()[Fifo_Stock_Gudang::$ID_GUDANG];
				$qry_gudang = $this->fg_model->gudang_get_by_last_fifo($last_fifo_id_gudang, $material[Material::$ID]);
			}
			$stock_in = ($arr_dus[$i] * $material[Material::$BOX]) + $arr_tin[$i];

			for ($j = 0; $j < $ctr_first_time; $j++) {
				if (($qry_gudang->num_rows() > 0 && $ctr_first_time == 1) || $ctr_first_time == 2) {
					foreach ($qry_gudang->result_array() as $row_gudang) {
						if ($stock_in <= 0) {
							break;
						} else {
							$kapasitas_total = $row_gudang[Gudang::$KAPASITAS] * $material[Material::$BOX];
							$kapasitas_terisi = $this->fifo_get_terisi_by_rak(
								$row_gudang[Gudang::$RAK],
								$row_gudang[Gudang::$KOLOM],
								$row_gudang[Gudang::$TINGKAT]
							)->row_array()[Fifo_Stock_Gudang::$S_TERISI];
							$kapasitas_sisa = $kapasitas_total - $kapasitas_terisi;

							if (($kapasitas_terisi == 0 && !$this->equals($row_gudang[Gudang::$RAK], Gudang::$S_TRANSIT)) || $this->equals($row_gudang[Gudang::$RAK], Gudang::$S_TRANSIT)) { //jika kapasitas masih kosong
								$total_tin = 0;
								if ($kapasitas_sisa - $stock_in >= 0) //jika kapasitas masih mencukupi
									$total_tin = $stock_in;
								else
									$total_tin = $kapasitas_sisa;
								$stock_in -= $total_tin;

								$arr_detail = [];
								$arr_detail[D_Good_Receipt::$ID] = "DGR" . $kode . str_pad($ctr_d_good_receipt++, "3", "0", STR_PAD_LEFT);

								$stock_dus = 0;
								$stock_tin = 0;
								if ($material[Material::$BOX] > 1) {
									$stock_dus = intdiv($total_tin, $material[Material::$BOX]);
									$stock_tin = $total_tin % $material[Material::$BOX];
								} else {
									$stock_tin = $total_tin;
								}
								$id_gudang_loop = $row_gudang[Gudang::$ID];

								$arr_detail[D_Good_Receipt::$DUS] = $stock_dus;
								$arr_detail[D_Good_Receipt::$TIN] = $stock_tin;
								$arr_detail[D_Good_Receipt::$ID_GUDANG] = $id_gudang_loop;

								$arr_detail[D_Good_Receipt::$ID_MATERIAL] = $material[Material::$ID];
								$arr_detail[D_Good_Receipt::$ID_H_GOOD_RECEIPT] = $arr[H_Good_Receipt::$ID];
								$this->db->insert(D_Good_Receipt::$TABLE_NAME, $arr_detail);

								//Penambahan Stock Gudang
								$this->insert_history_stock($material[Material::$ID], $id_gudang_loop, $total_tin, 0, H_Good_Receipt::GEN_ADJUSTMENT_MESSAGE($_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID], $arr[H_Good_Receipt::$NOMOR_SJ]) . ", Ket: " . $keterangan);

								//Fifo Stock Gudang
								//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
								$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $material[Material::$ID]);
								$this->db->order_by(Fifo_Stock_Gudang::$ID, $this::$ORDER_TYPE_DESC);

								$qry_fifo = $this->db->get(fifo_stock_gudang::$TABLE_NAME);
								$id_gudang = "";

								if ($qry_fifo->num_rows() > 0) {
									$qry_fifo = $qry_fifo->row_array();
									$id_gudang = $qry_fifo[Fifo_Stock_Gudang::$ID_GUDANG];
								}
								$arr_fifo = [];
								if ($id_gudang != $id_gudang_loop) {
									$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $material[Material::$ID];
									$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $total_tin;
									$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								} else {
									$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo[Fifo_Stock_Gudang::$ID]);
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo[Fifo_Stock_Gudang::$QTY] + $total_tin;
									$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								}
							}else
								break;
						}
					}
					if (!$is_first_time) { //jika tidak pertama kali, ulang dari awal, cek kapasitas rak 0 atau tidak
						$qry_gudang = $this->get(Gudang::$TABLE_NAME, Gudang::$ID_MATERIAL, $material[Material::$ID]);
					}
					
					//return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
				} else {
					$this->db->where(H_Good_Receipt::$ID, $arr[H_Good_Receipt::$ID]);
					$this->db->delete(H_Good_Receipt::$TABLE_NAME);
					return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
				}
			}
			if ($stock_in > 0) { //Masuk Cadangan
				$this->db->where(Gudang::$RAK, Gudang::$S_CADANGAN);
				$this->db->where(Gudang::$NAMA_GUDANG, $qry_gudang->row()->nama_gudang);
				$qry_temp_gudang = $this->db->get(Gudang::$TABLE_NAME);
				$id_gudang_loop = $qry_temp_gudang->row()->id_gudang;

				//Penambahan Stock Gudang
				$this->insert_history_stock($material[Material::$ID], $id_gudang_loop, $stock_in, 0, H_Good_Receipt::GEN_ADJUSTMENT_MESSAGE($_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID], $arr[H_Good_Receipt::$NOMOR_SJ]) . ", Ket: " . $keterangan);

				//Fifo Stock Gudang
				//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
				$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $material[Material::$ID]);
				$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG, $id_gudang_loop);
				$qry_fifo = $this->db->get(fifo_stock_gudang::$TABLE_NAME);

				$arr_fifo = [];
				if ($qry_fifo->num_rows() == 0) {
					$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $material[Material::$ID];
					$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
					$arr_fifo[Fifo_Stock_Gudang::$QTY] = $stock_in;
					$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
				} else {
					$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo->row_array()[Fifo_Stock_Gudang::$ID]);
					$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo->row_array()[Fifo_Stock_Gudang::$QTY] + $stock_in;
					$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
				}

				$stock_dus = 0;
				$stock_tin = 0;
				if ($material[Material::$BOX] > 1) {
					$stock_dus = intdiv($stock_in, $material[Material::$BOX]);
					$stock_tin = $stock_in % $material[Material::$BOX];
				} else {
					$stock_tin = $stock_in;
				}
				$arr_detail = [];
				$arr_detail[D_Good_Receipt::$ID] = "DGR" . $kode . str_pad($ctr_d_good_receipt++, "3", "0", STR_PAD_LEFT);
				$arr_detail[D_Good_Receipt::$DUS] = $stock_dus;
				$arr_detail[D_Good_Receipt::$TIN] = $stock_tin;
				$arr_detail[D_Good_Receipt::$ID_GUDANG] = $id_gudang_loop;

				$arr_detail[D_Good_Receipt::$ID_MATERIAL] = $material[Material::$ID];
				$arr_detail[D_Good_Receipt::$ID_H_GOOD_RECEIPT] = $arr[H_Good_Receipt::$ID];
				$this->db->insert(D_Good_Receipt::$TABLE_NAME, $arr_detail);
			}
		}

		echo $this->alert('Kode GR: ' . $arr[H_Good_Receipt::$ID]);
		return H_Good_Receipt::$MESSAGE_SUCCESS_ADJUSTMENT;
	}

	public function get_rekap_sj($dt, $st, $nomor_sj, $kode_barang, $kemasan, $warna ,$sto){
		$select=array(
			H_Good_Receipt::$TABLE_NAME.".".H_Good_Receipt::$ID." ".H_Good_Receipt::$ID,
			H_Good_Receipt::$NOMOR_SJ,
			H_Good_Receipt::$TANGGAL,
			H_Good_Receipt::$NIK,
			"sum(".D_Good_Receipt::$DUS.")"." ". D_Good_Receipt::$DUS,
			"sum(".D_Good_Receipt::$TIN. ")" . D_Good_Receipt::$TIN,
			D_Good_Receipt::$ID_MATERIAL,
			Material::$BOX,
			Material::$ID_NEW
		);
		$group_by = array(
			H_Good_Receipt::$ID,
			H_Good_Receipt::$NOMOR_SJ,
			H_Good_Receipt::$TANGGAL,
			H_Good_Receipt::$NIK,
			D_Good_Receipt::$ID_MATERIAL,
			Material::$BOX,
			Material::$ID_NEW
		);
		$this->db->select($select);
		$this->db->from(D_Good_Receipt::$TABLE_NAME);
		$join = D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_H_GOOD_RECEIPT . "=" . H_Good_Receipt::$TABLE_NAME . "." . H_Good_Receipt::$ID;
		$this->db->join(H_Good_Receipt::$TABLE_NAME, $join);
		$join = D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_MATERIAL  . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Receipt::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Receipt::$TANGGAL . ' <', $until, TRUE);
		}
		if($nomor_sj != NULL){
			$this->db->where(H_Good_Receipt::$NOMOR_SJ, $nomor_sj);
		}
		if ($kode_barang != NULL) {
			$this->db->where(Material::$KODE_BARANG, $kode_barang);
		}
		if ($kemasan != NULL) {
			$this->db->where(Material::$KEMASAN, $kemasan);
		}
		if ($warna != NULL) {
			$this->db->where(Material::$WARNA, $warna);
		}
		if ($sto != NULL) {
			$this->db->where(H_Good_Receipt::$STO, $sto);
		}
		$this->db->group_by($group_by);
		//$data[H_Good_Receipt::$TABLE_NAME]=$this->get(H_Good_Receipt::ta)
		return $this->db->get();
	}
	public function get_today_info(){
		$today=date("Y-m-d");
		$until = date("Y-m-d", strtotime($today . "+ 1 day"));
		$this->db->distinct();
		$this->db->select(H_Good_Receipt::$NOMOR_SJ);
		$this->db->where(H_Good_Receipt::$KODE_REVISI . Library_Model::$WHERE_IS_NULL, NULL, FALSE);
		$this->db->where(H_Good_Receipt::$TANGGAL . ' >=', $today, TRUE);
		$this->db->where(H_Good_Receipt::$TANGGAL . ' <', $until, TRUE);
		return $this->db->get(H_Good_Receipt::$TABLE_NAME);
	}
	public function get_kinerja_in($dt, $st, $nik = null){
		$select = [
			H_Good_Receipt::$TANGGAL,
			H_Good_Receipt::$NIK,H_Good_Receipt::$NIK_ADMIN,H_Good_Receipt::$NIK_SOPIR,H_Good_Receipt::$NIK_KERNET,
			H_Good_Receipt::$NOMOR_SJ,
			"sum(round((dus*box+tin)*gross,3)) as ". H_Good_Receipt::$S_TOT_QTY,
			"(case when material.box<=1 then 'PAIL' else 'NON_PAIL' end ) as ".H_Good_Receipt::$S_JENIS,
		];
		$this->db->select($select);
		$this->db->from(H_Good_Receipt::$TABLE_NAME);
		$this->db->join(D_Good_Receipt::$TABLE_NAME, H_Good_Receipt::$TABLE_NAME . "." . H_Good_Receipt::$ID . "=" . D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_H_GOOD_RECEIPT);
		$this->db->join(Material::$TABLE_NAME, Material::$TABLE_NAME . "." . Material::$ID . "=" . D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_MATERIAL);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Receipt::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Receipt::$TANGGAL . ' <', $until, TRUE);
		}
		if ($nik != null) {
			$qry = $this->karyawan_model->get_by(Karyawan::$ID, $nik);
			if ($qry->num_rows() > 0) {
				$karyawan = $qry->row_array();
				if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_SOPIR)) //jika karyawan tsb adalah sopir
					$this->db->where(H_Good_Receipt::$NIK_SOPIR, $nik);
				else if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_KERNET)) //jika karyawan tsb adalah kernet
					$this->db->where(H_Good_Receipt::$NIK_KERNET, $nik);
				else if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_GENERAL_SOPIR_FK) || $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_SOPIR_FK)) {
				} //jika karyawan tsb adalah sopir fk
				//$this->db->where(H_Good_Issue::$NIK_SOPIR,$nik);	
				if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_GENERAL_CHECKER) || $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_CHECKER)) //jika karyawan tsb adalah checker
					$this->db->where(H_Good_Receipt::$NIK, $nik);
			}
		}
		$this->db->where(H_Good_Receipt::$REV_GOOD_ISSUE . $this::$WHERE_IS_NULL, NULL, FALSE);
		$group_by = [
			H_Good_Receipt::$NIK, 
			H_Good_Receipt::$NIK_ADMIN, 
			H_Good_Receipt::$NIK_SOPIR, 
			H_Good_Receipt::$NIK_KERNET,
			H_Good_Receipt::$NOMOR_SJ,
			H_Good_Receipt::$S_JENIS
		];
		$this->db->group_by($group_by);
		return $this->db->get();
	}
	public function get_kinerja_out($nik, $nomor_sj){
		$select = [
			"(case when material.box<=1 then 'PAIL' else 'NON_PAIL' end ) as " . H_Good_Receipt::$S_JENIS,
			"sum(round((" . D_Good_Issue::$DUS . "*" . Material::$BOX . "+" . D_Good_Issue::$TIN . ")*" . Material::$GROSS . ",3)) as ". H_Good_Issue::$S_TOT_QTY
		];
		$this->db->select($select);
		$this->db->from(H_Good_Issue::$TABLE_NAME);
		$this->db->join(D_Good_Issue::$TABLE_NAME, H_Good_Issue::$TABLE_NAME . "." . H_Good_Issue::$ID . "=" . D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_H_GOOD_ISSUE);
		$this->db->join(Material::$TABLE_NAME, Material::$TABLE_NAME . "." . Material::$ID . "=" . D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_MATERIAL);
		$this->db->where(H_Good_Issue::$REV_GOOD_RECEIPT, 1);
		$this->db->where(H_Good_Issue::$NIK, $nik);
		$this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);

		return $this->db->get();
	}
}
