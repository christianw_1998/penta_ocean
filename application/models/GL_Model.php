<?php
class General_Ledger
{
	public static $TABLE_NAME = "general_ledger";
	public static $ID = "id_gl";

	public static $DESKRIPSI = "deskripsi_gl";
}
class GL_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}
	public function update_batch($data){
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika PO tidak kosong
					$id = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$deskripsi = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;

					//Insert Tabel GL
					$data_query = array(
						General_Ledger::$ID => $id,
						General_Ledger::$DESKRIPSI => $deskripsi
					);

					$this->db->where(General_Ledger::$ID, $id);
					$qry = $this->db->get(General_Ledger::$TABLE_NAME);
					if ($qry->num_rows() > 0) { //Update PO
						//$this->db->where(Purchase_Order::$ID,$nomor_po);
						//$this->db->update(Purchase_Order::$TABLE_NAME,$data_query);
					} else {
						$this->db->insert(General_Ledger::$TABLE_NAME, $data_query);
					}
				}
			}
		}
	}

	function get_by_arr($arr){
		$this->db->where_in(General_Ledger::$ID, $arr);
		return $this->db->get(General_Ledger::$TABLE_NAME);
	}
}
