<?php
class Cutoff{
	public static $TABLE_NAME = "cutoff";
	public static $ID = "id_cutoff";
	public static $TANGGAL_DARI = "tanggal_dari";
	public static $TANGGAL_SAMPAI = "tanggal_sampai";
	public static $KETERANGAN = "keterangan";

	//MESSAGE
	public static $MESSAGE_SUCCESS_INSERT = "CUQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "CUQ002";
	public static $MESSAGE_SUCCESS_DELETE = "CUQ003";
	public static $MESSAGE_FAILED_DELETE = "CUQ004";
	public static $MESSAGE_FAILED_INSERT_UPDATE_DATE_NULL = "CUQ005";
	public static $MESSAGE_FAILED_INSERT_UPDATE_DATE_FROM_LATER = "CUQ006";
}

class Cutoff_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}


}
