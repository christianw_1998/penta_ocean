<?php



class Other_Asset_Class{
	public static $TABLE_NAME = "other_asset_class";
	public static $ID = "id_other_ac";

	public static $DESKRIPSI = "deskripsi_ac";
	
	//Foreign Key
	public static $ID_GL = "id_gl";
}

class Other_Kategori{
	public static $TABLE_NAME = "other_kategori";
	public static $ID = "id_other_kategori";

	public static $NAMA = "nama_kategori";
	public static $KODE_AWAL = "kode_awal";

	public static $S_ASSET = "ASSET";
	public static $S_MISC = "MISC";
	public static $S_SERVICE = "SERVICE";
	public static $S_SPAREPART = "SPAREPART";
	public static $S_PENAMBAHAN_ASSET = "PENAMBAHAN ASSET";

}

class Other_Material{
	public static $TABLE_NAME = "other_material";
	public static $ID = "id_other_material";

	public static $DESKRIPSI = "deskripsi_material";
	public static $NAMA_VENDOR = "nama_vendor";
	public static $TANGGAL = "tanggal";
	public static $MASA_MANFAAT = "masa_manfaat";

	//Foreign Key
	public static $ID_KATEGORI = "id_other_kategori";
	public static $ID_CC = "id_cc";
	public static $ID_AC = "id_other_ac";
	public static $ID_GL = "id_gl";
	public static $ID_VENDOR = "id_vendor";
	public static $ID_METODE_PENYUSUTAN = "id_metode_penyusutan";

	//Message
	public static $MESSAGE_SUCCESS_INSERT = "OMTQ001";

}

class Other_Material_Stock{
	public static $TABLE_NAME = "other_material_stock";
	public static $ID = "id_other_material_stock";

	public static $QTY = "qty";
	public static $VALUE = "value";

	public static $S_STOCK_AKHIR = "stock_akhir";

	//Foreign Key
	public static $ID_OTHER_MATERIAL = "id_other_material";
}

class Other_Metode_Penyusutan{
	public static $TABLE_NAME = "other_metode_penyusutan";
	public static $ID = "id_metode_penyusutan";

	public static $MASA_MANFAAT = "masa_manfaat";
	public static $METODE = "metode";
	public static $TARIF = "tarif";

}

class Other_Purchase_Order{
	public static $TABLE_NAME = "other_po";
	public static $ID = "id_other_po";

	public static $TANGGAL = "tanggal";
	public static $PPN = "ppn";
	public static $IS_DONE = "is_done";
	public static $IS_DELETED = "is_deleted";

	

	public static $ID_PAYTERM = "id_payterm";
	public static $ID_OTHER_KATEGORI = "id_other_kategori";

}

class Other_Purchase_Order_Awal{
	public static $TABLE_NAME = "other_po_awal";
	public static $ID = "id_other_po_awal";

	public static $NOMOR_URUT = "nomor_urut";
	public static $NAMA_MATERIAL = "nama_material";
	public static $NAMA_SERVICE = "nama_service";
	public static $NAMA_VENDOR = "nama_vendor";
	public static $NET_PRICE = "net_price";
	public static $QTY = "qty";

	public static $ID_OTHER_MATERIAL = "id_other_material";
	public static $ID_OTHER_SERVICE = "id_other_service";
	public static $NOMOR_PO = "id_other_po";
	public static $ID_VENDOR = "id_vendor";

}

class Other_GR_Header{
	public static $TABLE_NAME = "other_gr_header";
	public static $ID = "id_other_gr_header";

	public static $NO_SJ = "no_sj";
	public static $TANGGAL = "tanggal";
	public static $IS_DELETED = "is_deleted";

	public static $S_CODE = "90";

	//Foreign Key
	public static $ID_OTHER_PO = "id_other_po";
	public static $NIK = "nik";
}

class Other_GR_Detail{
	public static $TABLE_NAME = "other_gr_detail";
	public static $ID = "id_other_gr_detail";

	public static $NOMOR_URUT = "nomor_urut";
	public static $QTY = "qty";

	//Foreign Key
	public static $ID_HEADER = "id_other_gr_header";
	public static $ID_OTHER_MATERIAL = "id_other_material";

	public static function GEN_INSERT_MESSAGE($params)
	{
		return "PO " . $params;
	}
}

class Other_GI_Header{
	public static $TABLE_NAME = "other_gi_header";
	public static $ID = "id_other_gi_header";

	public static $TANGGAL = "tanggal";
	public static $IS_DELETED = "is_deleted";

	public static $S_CODE = "91";

	//Foreign Key
	public static $NIK = "nik";

	public static $MESSAGE_SUCCESS_INSERT="OGIQ001";
}

class Other_GI_Detail{
	public static $TABLE_NAME = "other_gi_detail";
	public static $ID = "id_other_gi_detail";

	public static $QTY = "qty";
	public static $ALASAN = "alasan";

	//Foreign Key
	public static $ID_HEADER = "id_other_gi_header";
	public static $ID_MATERIAL = "id_other_material";
	public static $ID_GL = "id_gl";
	public static $ID_CC = "id_cc";

	public static function GEN_INSERT_MESSAGE($params)
	{
		return $params;
	}
}

class Stock_Gudang_Other{
	public static $TABLE_NAME = "stock_gudang_other";
	public static $ID = "id_stock_gudang";

	public static $TANGGAL = "tanggal";
	public static $STOCK_AWAL = "stock_awal";
	public static $VALUE_AWAL = "value_awal";
	public static $NOMOR_PO = "nomor_po";
	public static $NOMOR_SJ = "nomor_sj";
	public static $NOMOR_GI_GR = "nomor_gi_gr";
	public static $KREDIT = "kredit";
	public static $DEBIT = "debit";
	public static $STOCK_AKHIR = "stock_akhir";
	public static $VALUE_AKHIR = "value_akhir";
	public static $PESAN = "pesan";
	public static $NAMA_VENDOR = "nama_vendor";

	public static $IS_DELETED = "is_deleted";

	//Foreign Key
	public static $ID_OTHER_MATERIAL = "id_other_material";
	public static $ID_VENDOR = "id_vendor";
}

class Other_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	//public function material_insert($ik, $im, $d, $ia, $ic, $iv, $ig, $nv, $mp, $mm){
	public function material_insert($ik, $im, $d, $ia, $ic, $iv, $ig, $nv, $mp){
		$masa_manfaat = 0;
		
		
		if($mp != null){
			//hitung masa manfaat
			$this->db->where(Other_Metode_Penyusutan::$ID, $mp);
			$masa_manfaat = ($this->db->get(Other_Metode_Penyusutan::$TABLE_NAME)->row_array()[Other_Metode_Penyusutan::$MASA_MANFAAT] - 1) * 12;
			$bulan = 12 - intval(date("m")) + 1;
			$masa_manfaat += $bulan;
		}
		
		$data=[
			Other_Material::$ID => $im,
			Other_Material::$DESKRIPSI => strtoupper($d),
			Other_Material::$ID_KATEGORI => $ik,
			Other_Material::$ID_CC => $ic,
			Other_Material::$ID_VENDOR => $iv,
			Other_Material::$ID_GL => $ig,
			Other_Material::$NAMA_VENDOR => strtoupper($nv),
			Other_Material::$MASA_MANFAAT => $masa_manfaat
			
		];
		if ($mp != null) {
			$data[Other_Material::$ID_METODE_PENYUSUTAN] = $mp;
		}
		if ($ia != null) {
			$data[Other_Material::$ID_AC] = $ia;
		}
		
		$this->db->set(Other_Material::$TANGGAL, 'NOW()', FALSE);
		$this->db->insert(Other_Material::$TABLE_NAME, $data);

		return Other_Material::$MESSAGE_SUCCESS_INSERT;
	}
	public function material_get_last_code($id){
		$this->db->select("max(substring(". Other_Material::$ID .",4,4)) as ". Other_Material::$ID);
		$this->db->like(Other_Material::$ID, $id, $this::$WHERE_LIKE_AFTER);
		return $this->db->get(Other_Material::$TABLE_NAME)->row_array()[Other_Material::$ID];
		
	}
	public function material_get_rekap($dt, $st, $ik){
		$arr_select = [
			Other_Material::$ID, Other_Material::$DESKRIPSI, Other_Material::$NAMA_VENDOR,
			Other_Material::$TANGGAL, Other_Kategori::$NAMA, 
			Other_Asset_Class::$ID,
			Cost_Center::$TABLE_NAME . "." . Cost_Center::$ID . " " . Cost_Center::$ID, Cost_Center::$NAMA, Vendor::$TABLE_NAME . "." . Vendor::$ID . " " . Vendor::$ID,
			Vendor::$NAMA, General_Ledger::$TABLE_NAME . "." . General_Ledger::$ID . " " . General_Ledger::$ID,
			General_Ledger::$DESKRIPSI
		];
		
		$this->db->select($arr_select);
		$this->db->from(Other_Material::$TABLE_NAME);

		$join = Other_Material::$TABLE_NAME . "." . Other_Material::$ID_KATEGORI . "=" . Other_Kategori::$TABLE_NAME . "." . Other_Kategori::$ID;
		$this->db->join(Other_Kategori::$TABLE_NAME, $join);

		//$join = Other_Material::$TABLE_NAME . "." . Other_Material::$ID_AC . "=" . Other_Asset_Class::$TABLE_NAME . "." . Other_Asset_Class::$ID;
		//$this->db->join(Other_Asset_Class::$TABLE_NAME, $join);

		$join = Other_Material::$TABLE_NAME . "." . Other_Material::$ID_CC . "=" . Cost_Center::$TABLE_NAME . "." . Cost_Center::$ID;
		$this->db->join(Cost_Center::$TABLE_NAME, $join);

		$join = Other_Material::$TABLE_NAME . "." . Other_Material::$ID_GL . "=" . General_Ledger::$TABLE_NAME . "." . General_Ledger::$ID;
		$this->db->join(General_Ledger::$TABLE_NAME, $join);

		$join = Other_Material::$TABLE_NAME . "." . Other_Material::$ID_VENDOR . "=" . Vendor::$TABLE_NAME . "." . Vendor::$ID;
		$this->db->join(Vendor::$TABLE_NAME, $join);

		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Other_Material::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Other_Material::$TANGGAL . ' <', $until, TRUE);
		}
		if ($ik != null){
			$this->db->where(Other_Material::$TABLE_NAME . "." . Other_Material::$ID_KATEGORI, $ik);
		}

		return $this->db->get();
	}
	public function material_get_history($dt, $st, $ik){
		$arr_select = [
			Stock_Gudang_Other::$ID,
			Stock_Gudang_Other::$TABLE_NAME.".".Stock_Gudang_Other::$TANGGAL . " " . Stock_Gudang_Other::$TANGGAL,Stock_Gudang_Other::$NOMOR_PO,Stock_Gudang_Other::$NOMOR_SJ,
			Other_Material::$TABLE_NAME.".". Other_Material::$ID." ". Other_Material::$ID, Other_Material::$DESKRIPSI,
			Stock_Gudang_Other::$TABLE_NAME . "." . Stock_Gudang_Other:: $ID_VENDOR . " " . Stock_Gudang_Other::$ID_VENDOR,
			Stock_Gudang_Other::$TABLE_NAME . "." . Stock_Gudang_Other:: $NAMA_VENDOR . " " . Stock_Gudang_Other::$NAMA_VENDOR, Stock_Gudang_Other::$STOCK_AWAL,
			Stock_Gudang_Other::$DEBIT,Stock_Gudang_Other::$KREDIT,Stock_Gudang_Other::$STOCK_AKHIR,Stock_Gudang_Other::$PESAN,
			Stock_Gudang_Other::$VALUE_AWAL,Stock_Gudang_Other::$VALUE_AKHIR, Stock_Gudang_Other::$NOMOR_GI_GR
		];
		
		$this->db->select($arr_select);
		$this->db->from(Stock_Gudang_Other::$TABLE_NAME);

		$join = Other_Material::$TABLE_NAME . "." . Other_Material::$ID . "=" . Stock_Gudang_Other::$TABLE_NAME . "." . Stock_Gudang_Other::$ID_OTHER_MATERIAL;
		$this->db->join(Other_Material::$TABLE_NAME, $join);

		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Stock_Gudang_Other::$TABLE_NAME . "." . Stock_Gudang_Other::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Stock_Gudang_Other::$TABLE_NAME . "." . Stock_Gudang_Other::$TANGGAL . ' <', $until, TRUE);
		}
		if ($ik != null){
			$this->db->where(Other_Material::$TABLE_NAME . "." . Other_Material::$ID_KATEGORI, $ik);
		}

		return $this->db->get();
	}
	public function material_get_by_id($id,$kategori){
		$this->db->where(Other_Material::$ID_KATEGORI, $kategori);
		$this->db->where(Other_Material::$ID, $id);
		return $this->db->get(Other_Material::$TABLE_NAME);
	}
	public function material_stock_get_all(){
		$arr_select=[
			Other_Material::$TABLE_NAME . "." . Other_Material::$ID . " " . Other_Material::$ID,
			Other_Material::$DESKRIPSI, Other_Material_Stock::$QTY
		];
		$this->db->select($arr_select);
		$this->db->from(Other_Material_Stock::$TABLE_NAME);
		$join = Other_Material::$TABLE_NAME . "." . Other_Material::$ID . "=" . Other_Material_Stock::$TABLE_NAME . "." . Other_Material_Stock::$ID_OTHER_MATERIAL;
		$this->db->join(Other_Material::$TABLE_NAME,$join);
		return $this->db->get();
	}
	public function material_stock_get_by($id_material){
		$this->db->select("sum(qty) as ". Other_Material_Stock::$S_STOCK_AKHIR);
		$this->db->where(Other_Material_Stock::$ID_OTHER_MATERIAL, $id_material);
		$data = array(Other_Material_Stock::$ID_OTHER_MATERIAL);
		$this->db->group_by($data);
		$qry = $this->db->get(Other_Material_Stock::$TABLE_NAME);
		if ($qry->num_rows() > 0) {
			return $qry->row_array()[Other_Material_Stock::$S_STOCK_AKHIR];
		} 
		return 0;
	}

	public function po_insert($idm,$dm,$ds,$ids,$inp,$iqy,$ppn,$ctr,$tgl,$pt,$kat){
		
		//Generate Kode Maks
		
		$this->db->select("max(substr(" . Other_Purchase_Order::$ID . ",4)) as " . Other_Purchase_Order::$ID);
		$this->db->where(Other_Purchase_Order::$ID_OTHER_KATEGORI, $kat);
		$qry_po = $this->db->get(Other_Purchase_Order::$TABLE_NAME)->row_array();

		$max = intval($qry_po[Other_Purchase_Order::$ID]);
		if ($max == null) $max = 0;
		$max += 1;

		//Ambil Kode
		$this->db->where(Other_Kategori::$ID,$kat);
		$qry_kategori=$this->db->get(Other_Kategori::$TABLE_NAME);

		$kode= $qry_kategori->row_array()[Other_Kategori::$KODE_AWAL];


		//Ambil Vendor dan Namanya
		/*$id_vendor = $idv[0];
		for ($i = 0; $i < $ctr; $i++) {
			if(!$this->equals($id_vendor,$idv[$i])){
				return Purchase_Order::$MESSAGE_FAILED_INSERT_VENDOR_NOT_SAME;
			}
		}*/

		//Other Purchase Order
		$kode .= str_pad($max, 7, "0", STR_PAD_LEFT);
		echo "<script>alert('Nomor PO: " . $kode . "');</script>";

		$arr_insert = [
			Other_Purchase_Order::$ID => $kode,
			Other_Purchase_Order::$TANGGAL => $tgl,
			Other_Purchase_Order::$PPN => $ppn,
			Other_Purchase_Order::$IS_DONE => 0,
			Other_Purchase_Order::$IS_DELETED => 0,
			Other_Purchase_Order::$ID_PAYTERM => $pt,
			Other_Purchase_Order::$ID_OTHER_KATEGORI => $kat
		];
		$this->db->insert(Other_Purchase_Order::$TABLE_NAME, $arr_insert);

		
		//Purchase Order Awal
		for ($i = 0; $i < $ctr; $i++) {
			//Ambil Vendor dari Material 
			$this->db->where(Other_Material::$ID, $idm[$i]);
			$other_material = $this->db->get(Other_Material::$TABLE_NAME)->row_array();

			$vendor = $this->get(Vendor::$TABLE_NAME, Vendor::$ID, $other_material[Other_Material::$ID_VENDOR])->row_array();
			
			$arr_insert = [];
			$arr_insert[Other_Purchase_Order_Awal::$NOMOR_URUT] = ($i + 1) . "0";
			$arr_insert[Other_Purchase_Order_Awal::$ID_OTHER_MATERIAL] = strtoupper($idm[$i]);
			$arr_insert[Other_Purchase_Order_Awal::$NAMA_MATERIAL] = strtoupper($dm[$i]);
			if(isset($ids[$i])){
				$arr_insert[Other_Purchase_Order_Awal::$ID_OTHER_SERVICE] = strtoupper($ids[$i]);
				$arr_insert[Other_Purchase_Order_Awal::$NAMA_SERVICE] = strtoupper($ds[$i]);
			}
			$arr_insert[Other_Purchase_Order_Awal::$NET_PRICE] = $inp[$i];
			$arr_insert[Other_Purchase_Order_Awal::$QTY] = $iqy[$i];
			$arr_insert[Other_Purchase_Order_Awal::$NOMOR_PO] = $kode;
			$arr_insert[Other_Purchase_Order_Awal::$ID_VENDOR] = $vendor[Vendor::$ID];
			$arr_insert[Other_Purchase_Order_Awal::$NAMA_VENDOR] = strtoupper($other_material[Other_Material::$NAMA_VENDOR]);
			
			$this->db->insert(Other_Purchase_Order_Awal::$TABLE_NAME, $arr_insert);
		}
		return Purchase_Order::$MESSAGE_SUCCESS_INSERT;
	}
	public function po_get_report($dt, $st){
		$arr_select = [
			Other_Purchase_Order::$TABLE_NAME . "." . Other_Purchase_Order::$ID . " " . Other_Purchase_Order::$ID, Other_Purchase_Order_Awal::$NOMOR_URUT, 
			Other_Purchase_Order_Awal::$TABLE_NAME.".". Other_Purchase_Order_Awal::$ID_OTHER_MATERIAL,
			Other_Purchase_Order_Awal::$NAMA_MATERIAL,Other_Purchase_Order_Awal::$ID_OTHER_SERVICE,
			Other_Purchase_Order_Awal::$NAMA_SERVICE,Other_Purchase_Order_Awal::$QTY,Other_Purchase_Order_Awal::$NET_PRICE,
			Other_Purchase_Order_Awal::$ID_VENDOR,Other_Purchase_Order_Awal::$NAMA_VENDOR,
			Other_Purchase_Order::$TANGGAL

		];

		$this->db->select($arr_select);
		$this->db->from(Other_Purchase_Order::$TABLE_NAME);
		$this->db->join(
			Other_Purchase_Order_Awal::$TABLE_NAME,
			Other_Purchase_Order_Awal::$TABLE_NAME . "." . Other_Purchase_Order_Awal::$NOMOR_PO . "=" . Other_Purchase_Order::$TABLE_NAME . "." . Other_Purchase_Order::$ID
		);
		
		
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Other_Purchase_Order::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Other_Purchase_Order::$TANGGAL . ' <', $until, TRUE);
		}
		return $this->db->get();
	}
	public function po_get_by_no($no){
		$this->db->where(Other_Purchase_Order::$ID, $no);
		$data[Other_Purchase_Order::$TABLE_NAME] = $this->db->get(Other_Purchase_Order::$TABLE_NAME);
		$this->db->where(Other_Purchase_Order_Awal::$NOMOR_PO, $no);
		$data[Other_Purchase_Order_Awal::$TABLE_NAME] = $this->db->get(Other_Purchase_Order_Awal::$TABLE_NAME);

		return $data;
	}
	public function po_get_last_qty($nomor_po, $no_urut, $qty_awal){
		$qty = $qty_awal;
		$this->db->where(Other_GR_Header::$IS_DELETED, 0);
		$this->db->where(Other_GR_Header::$ID_OTHER_PO, $nomor_po);
		$qry = $this->db->get(Other_GR_Header::$TABLE_NAME);
		if ($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row) {
				$this->db->where(Other_GR_Detail::$ID_HEADER, $row[Other_GR_Header::$ID]);
				$this->db->where(Other_GR_Detail::$NOMOR_URUT, $no_urut);
				$qty_potong = 0;
				$qry_po_detail= $this->db->get(Other_GR_Detail::$TABLE_NAME);
				if($qry_po_detail->num_rows()>0)
					$qty_potong = $qry_po_detail->row_array()[Other_GR_Detail::$QTY];
				$qty -= $qty_potong;
			}
		}

		return $qty;
	}

	public function gr_insert($ctr, $nomor_po, $nomor_sj, $tanggal, $arr_id_material, $arr_no_urut, $arr_qty){
		$data_header = [];

		$max = $this->db->query("select max(substr(id_other_gr_header,3)) as max from " . Other_GR_Header::$TABLE_NAME . "
									where " . Other_GR_Header::$ID . " like '" . Other_GR_Header::$S_CODE . "%'")->row()->max;
		if ($max == NULL)
			$max = 0;
		$max += 1;

		//Insert PO Checker Header
		$kode = Other_GR_Header::$S_CODE. str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Other_GR_Header::$ID] = $kode;
		$data_header[Other_GR_Header::$TANGGAL] = date('Y-m-d', strtotime($tanggal));
		$data_header[Other_GR_Header::$NO_SJ] = $nomor_sj;
		$data_header[Other_GR_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Other_GR_Header::$ID_OTHER_PO] = $nomor_po;
		$data_header[Other_GR_Header::$IS_DELETED] = 0;
		$this->db->insert(Other_GR_Header::$TABLE_NAME, $data_header);
		echo $this->alert('Kode GR: ' . $kode);

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];
			if ($arr_qty[$i] > 0) {
				$this->db->where(Other_Material_Stock::$ID_OTHER_MATERIAL, $arr_id_material[$i]);
				$temp_stock = $this->db->get(Other_Material_Stock::$TABLE_NAME);

				//Insert Other PO GR Detail
				$kode = $data_header[Other_GR_Header::$ID] . str_pad(($i + 1), 3, "0", STR_PAD_LEFT);
				$data_detail[Other_GR_Detail::$ID] = $kode;
				$data_detail[Other_GR_Detail::$NOMOR_URUT] = $arr_no_urut[$i];
				$data_detail[Other_GR_Detail::$QTY] = $arr_qty[$i];
				$data_detail[Other_GR_Detail::$ID_OTHER_MATERIAL] = strtoupper($arr_id_material[$i]);
				$data_detail[Other_GR_Detail::$ID_HEADER] = $data_header[Other_GR_Header::$ID];
				$this->db->insert(Other_GR_Detail::$TABLE_NAME, $data_detail);

				//Cek dulu kategori jika sparepart baru masuk stock dan history stock
				$this->db->where(Other_Material::$ID, $data_detail[Other_GR_Detail::$ID_OTHER_MATERIAL]);
				$material=$this->db->get(Other_Material::$TABLE_NAME)->row_array();

				$this->db->where(Other_Kategori::$ID,$material[Other_Material::$ID_KATEGORI]);
				$kategori=$this->db->get(Other_Kategori::$TABLE_NAME)->row_array();

				$value_awal = 0;
				$value_akhir = 0;
				//Insert History Stock Other
				//if ($this->equals($kategori[Other_Kategori::$NAMA], Other_Kategori::$S_SPAREPART)) {
					$this->db->where(Other_Purchase_Order_Awal::$NOMOR_URUT, $data_detail[Other_GR_Detail::$NOMOR_URUT]);
					$this->db->where(Other_Purchase_Order_Awal::$NOMOR_PO, $data_header[Other_GR_Header::$ID_OTHER_PO]);
					$temp_po = $this->db->get(Other_Purchase_Order_Awal::$TABLE_NAME)->row_array();
					$net_price_po = $temp_po[Other_Purchase_Order_Awal::$NET_PRICE];

					$data_stock = [];
					$stock = $temp_stock->row_array();
					$value_awal = $stock[Other_Material_Stock::$VALUE];
					$qty_awal = $stock[Other_Material_Stock::$QTY];

					$data_stock[Other_Material_Stock::$QTY] = $qty_awal + $arr_qty[$i];
					$data_stock[Other_Material_Stock::$VALUE] = $value_awal + ($arr_qty[$i] * $net_price_po);
					$value_akhir = $data_stock[Other_Material_Stock::$VALUE];
				//}

				$this->gr_insert_history_stock(
					$data_header[Other_GR_Header::$ID_OTHER_PO],
					$data_header[Other_GR_Header::$NO_SJ],
					$data_header[Other_GR_Header::$ID],
					$data_header[Other_GR_Header::$TANGGAL],
					$data_detail[Other_GR_Detail::$ID_OTHER_MATERIAL],
					$value_awal,
					$data_detail[Other_GR_Detail::$QTY],
					0,
					$value_akhir,
					Other_GR_Detail::GEN_INSERT_MESSAGE(
						$data_header[Other_GR_Header::$ID_OTHER_PO]
					),
					$material[Other_Material::$ID_VENDOR],
					$material[Other_Material::$NAMA_VENDOR]

				);
				//if ($this->equals($kategori[Other_Kategori::$NAMA], Other_Kategori::$S_SPAREPART)) {
					$this->db->where(Other_Material_Stock::$ID_OTHER_MATERIAL, $arr_id_material[$i]);

					$temp_stock=$this->db->get(Other_Material_Stock::$TABLE_NAME);
					if($temp_stock->num_rows()>0){
						$stock = $this->material_stock_get_by($arr_id_material[$i]);
						$data_stock = [
							Other_Material_Stock::$QTY => $stock + $data_detail[Other_GR_Detail::$QTY],
							Other_Material_Stock::$VALUE => $value_akhir
						];
						$this->db->where(Other_Material_Stock::$ID_OTHER_MATERIAL, $arr_id_material[$i]);
						$this->db->update(Other_Material_Stock::$TABLE_NAME, $data_stock);
					}else{
						$data_stock = [
							Other_Material_Stock::$ID_OTHER_MATERIAL => $data_detail[Other_GR_Detail::$ID_OTHER_MATERIAL],
							Other_Material_Stock::$QTY => $data_detail[Other_GR_Detail::$QTY],
							Other_Material_Stock::$VALUE => $value_akhir
						];
						$this->db->insert(Other_Material_Stock::$TABLE_NAME, $data_stock);
						
					}
				//}
			}
		}
		return Purchase_Order_Checker_Detail::$MESSAGE_SUCCESS_INSERT;
	}
	public function gr_insert_history_stock($no_po, $nomor_sj, $nomor_gi_gr, $tanggal, $id_material, $value_awal = null, $debit, $kredit, $value_akhir = null, $pesan, $id_vendor = null,$nama_vendor = null){
		$arr_stock = [];
		$arr_stock[Stock_Gudang_Other::$TANGGAL] = $tanggal;
		$arr_stock[Stock_Gudang_Other::$STOCK_AWAL] = $this->material_stock_get_by($id_material);
		$arr_stock[Stock_Gudang_Other::$VALUE_AWAL] = $value_awal;
		$arr_stock[Stock_Gudang_Other::$DEBIT] = $debit;
		$arr_stock[Stock_Gudang_Other::$KREDIT] = $kredit;
		$arr_stock[Stock_Gudang_Other::$ID_OTHER_MATERIAL] = $id_material;
		$arr_stock[Stock_Gudang_Other::$STOCK_AKHIR] = $arr_stock[Stock_Gudang_Other::$STOCK_AWAL] + $arr_stock[Stock_Gudang_Other::$DEBIT] - $arr_stock[Stock_Gudang_Other::$KREDIT];
		$arr_stock[Stock_Gudang_Other::$VALUE_AKHIR] = $value_akhir;
		$arr_stock[Stock_Gudang_Other::$PESAN] = $pesan;
		$arr_stock[Stock_Gudang_Other::$NOMOR_PO] = $no_po;
		$arr_stock[Stock_Gudang_Other::$NOMOR_SJ] = $nomor_sj;
		$arr_stock[Stock_Gudang_Other::$NOMOR_GI_GR] = $nomor_gi_gr;
		$arr_stock[Stock_Gudang_Other::$IS_DELETED] = 0;

		if($id_vendor!=null){
			$arr_stock[Stock_Gudang_Other::$ID_VENDOR] = $id_vendor;
			$arr_stock[Stock_Gudang_Other::$NAMA_VENDOR] = strtoupper($nama_vendor);
		}

		$this->db->insert(Stock_Gudang_Other::$TABLE_NAME, $arr_stock);
	}

	public function gi_insert($ctr, $tanggal, $arr_id_material, $arr_id_cc, $arr_id_gl, $arr_alasan, $arr_qty){
		$data_header = [];

		$max = $this->db->query("select max(substr(id_other_gi_header,3)) as max from " . Other_GI_Header::$TABLE_NAME . "
									where " . Other_GI_Header::$ID . " like '" . Other_GI_Header::$S_CODE . "%'")->row()->max;
		if ($max == NULL)
			$max = 0;
		$max += 1;

		//Insert PO Checker Header
		$kode = Other_GI_Header::$S_CODE. str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Other_GI_Header::$ID] = $kode;
		$data_header[Other_GI_Header::$TANGGAL] = date('Y-m-d', strtotime($tanggal));
		$data_header[Other_GI_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Other_GI_Header::$IS_DELETED] = 0;
		$this->db->insert(Other_GI_Header::$TABLE_NAME, $data_header);
		echo $this->alert('Kode GR: ' . $kode);

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];
			if ($arr_qty[$i] > 0) {
				$this->db->where(Other_Material_Stock::$ID_OTHER_MATERIAL, $arr_id_material[$i]);
				$temp_stock = $this->db->get(Other_Material_Stock::$TABLE_NAME);

				//Insert Other GI Detail
				$kode = $data_header[Other_GI_Header::$ID] . str_pad(($i + 1), 3, "0", STR_PAD_LEFT);
				$data_detail[Other_GI_Detail::$ID] = $kode;
				$data_detail[Other_GI_Detail::$QTY] = $arr_qty[$i];
				$data_detail[Other_GI_Detail::$ID_MATERIAL] = strtoupper($arr_id_material[$i]);
				$data_detail[Other_GI_Detail::$ID_CC] = $arr_id_cc[$i];
				$data_detail[Other_GI_Detail::$ID_GL] = $arr_id_gl[$i];
				$data_detail[Other_GI_Detail::$ALASAN] = $arr_alasan[$i];
				$data_detail[Other_GI_Detail::$ID_HEADER] = $data_header[Other_GI_Header::$ID];
				$this->db->insert(Other_GI_Detail::$TABLE_NAME, $data_detail);

				//Cek dulu kategori jika sparepart baru masuk stock dan history stock
				$this->db->where(Other_Material::$ID, $data_detail[Other_GI_Detail::$ID_MATERIAL]);
				$material=$this->db->get(Other_Material::$TABLE_NAME)->row_array();

				$this->db->where(Other_Kategori::$ID,$material[Other_Material::$ID_KATEGORI]);
				$kategori=$this->db->get(Other_Kategori::$TABLE_NAME)->row_array();

				$value_awal = 0;
				$value_akhir = 0;
				//Insert History Stock Other
				//if ($this->equals($kategori[Other_Kategori::$NAMA], Other_Kategori::$S_SPAREPART)) {
					
					$data_stock = [];
					$stock = $temp_stock->row_array();
					$value_awal = $stock[Other_Material_Stock::$VALUE];
					$qty_awal = $stock[Other_Material_Stock::$QTY];

					$net_price = 0;
					if ($qty_awal != 0) {
						$net_price = $value_awal / $qty_awal;
					}

					$data_stock[Other_Material_Stock::$QTY] = $qty_awal - $arr_qty[$i];
					$data_stock[Other_Material_Stock::$VALUE] = $data_stock[Material_RM_PM_Stock::$QTY]*$net_price;
					$value_akhir = $data_stock[Material_RM_PM_Stock::$VALUE];
				//}

				$this->gi_insert_history_stock(
					$data_header[Other_GI_Header::$ID],
					$data_header[Other_GI_Header::$TANGGAL],
					$data_detail[Other_GI_Detail::$ID_MATERIAL],
					$value_awal,
					0,
					$data_detail[Other_GR_Detail::$QTY],
					$value_akhir,
					Other_GI_Detail::GEN_INSERT_MESSAGE(
						$arr_alasan[$i]
					),
					$material[Other_Material::$ID_VENDOR],
					$material[Other_Material::$NAMA_VENDOR]

				);
				//if ($this->equals($kategori[Other_Kategori::$NAMA], Other_Kategori::$S_SPAREPART)) {
					$this->db->where(Other_Material_Stock::$ID_OTHER_MATERIAL, $arr_id_material[$i]);

					$temp_stock=$this->db->get(Other_Material_Stock::$TABLE_NAME);
					if($temp_stock->num_rows()>0){
						$stock = $this->material_stock_get_by($arr_id_material[$i]);
						$data_stock = [
							Other_Material_Stock::$QTY => $stock - $data_detail[Other_GI_Detail::$QTY],
							Other_Material_Stock::$VALUE => $value_akhir
						];
						$this->db->where(Other_Material_Stock::$ID_OTHER_MATERIAL, $arr_id_material[$i]);
						$this->db->update(Other_Material_Stock::$TABLE_NAME, $data_stock);
					}
				//}
			}
		}
		return Other_GI_Header::$MESSAGE_SUCCESS_INSERT;
	}
	public function gi_insert_history_stock($nomor_gi_gr,$tanggal, $id_material, $value_awal = null, $debit, $kredit, $value_akhir = null, $pesan, $id_vendor = null, $nama_vendor = null){
		$arr_stock = [];
		$arr_stock[Stock_Gudang_Other::$TANGGAL] = $tanggal;
		$arr_stock[Stock_Gudang_Other::$STOCK_AWAL] = $this->material_stock_get_by($id_material);
		$arr_stock[Stock_Gudang_Other::$VALUE_AWAL] = $value_awal;
		$arr_stock[Stock_Gudang_Other::$DEBIT] = $debit;
		$arr_stock[Stock_Gudang_Other::$KREDIT] = $kredit;
		$arr_stock[Stock_Gudang_Other::$ID_OTHER_MATERIAL] = $id_material;
		$arr_stock[Stock_Gudang_Other::$STOCK_AKHIR] = $arr_stock[Stock_Gudang_Other::$STOCK_AWAL] + $arr_stock[Stock_Gudang_Other::$DEBIT] - $arr_stock[Stock_Gudang_Other::$KREDIT];
		$arr_stock[Stock_Gudang_Other::$VALUE_AKHIR] = $value_akhir;
		$arr_stock[Stock_Gudang_Other::$PESAN] = $pesan;
		$arr_stock[Stock_Gudang_Other::$NOMOR_GI_GR] = $nomor_gi_gr;
		$arr_stock[Stock_Gudang_Other::$IS_DELETED] = 0;

		if($id_vendor!=null){
			$arr_stock[Stock_Gudang_Other::$ID_VENDOR] = $id_vendor;
			$arr_stock[Stock_Gudang_Other::$NAMA_VENDOR] = strtoupper($nama_vendor);
		}

		$this->db->insert(Stock_Gudang_Other::$TABLE_NAME, $arr_stock);
	}

}
