<?php
class Country{
	public static $TABLE_NAME = "country";
	public static $ID = "id_country";

	public static $NAMA_COUNTRY = "nama_country";

}
class Country_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika Country tidak kosong
					$id = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$nama = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					
					//Insert Tabel Country
					$this->db->where(Country::$ID, $id);
					$qry = $this->db->get(Country::$TABLE_NAME);

					if ($qry->num_rows() > 0) { 
						
					} else{
						$data_country=[
							Country::$ID => $id,
							Country::$NAMA_COUNTRY => strtoupper($nama)
						];
						$this->db->insert(Country::$TABLE_NAME,$data_country);
					}
				}
			}
		}
		
	}
}
