<?php

class BOM{
	public static $TABLE_NAME = "bom";
	public static $ID = "product_code";

	public static $DESCRIPTION = "description";
	public static $JOB_INSTRUCTION = "job_instruction";
	public static $QC = "qc";

	public static $MESSAGE_SUCCESS_UPLOAD = "BOMQ001";

}

class BOM_Alt{
	public static $TABLE_NAME = "bom_alt";
	public static $ID = "id_bom_alt";

	public static $ALT_NO = "alt_no";

	//Foreign Key
	public static $PRODUCT_CODE = "product_code";
}

class BOM_Alt_Recipe{
	public static $TABLE_NAME = "bom_alt_recipe";
	public static $ID = "id_bom_alt_recipe";

	public static $NOMOR_URUT = "nomor_urut";
	public static $QTY = "qty";
	public static $UNIT = "unit";
	public static $KODE_FG = "kode_fg";

	//Foreign Key
	public static $ID_ALT = "id_bom_alt";
	public static $ID_MATERIAL = "id_material_rm_pm";
}

class BOM_Production_Version{
	public static $TABLE_NAME = "bom_production_version";
	public static $ID = "id_bom_pv";

	public static $VALID_FROM = "valid_from";
	public static $VALID_TO = "valid_to";
	public static $CHECK_DATE = "check_date";

	//Foreign Key
	public static $ID_ALT = "id_bom_alt";
	public static $PRODUCT_CODE = "product_code";

	public static $MESSAGE_SUCCESS_INSERT = "BPVQ001";

}

class BOM_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		$arr_bom = array();
		$arr_bom_alt = array();
		$arr_bom_alt_recipe = array();
		$ctr = 0;
		
		$no_urut = 1;
		$old_alt_num = "";
		$old_product_code = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika BOM tidak kosong
					$product_code = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$description = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$bom_alt = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$kode_rm_pm = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$qty = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$unit = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;

					if(!$this->equals($product_code,$old_product_code) || !$this->equals($bom_alt,$old_alt_num)){
						$old_product_code=$product_code;$old_alt_num=$bom_alt;$no_urut = 1;
					}

					//Insert Tabel BOM
					$this->db->where(BOM::$ID, $product_code);
					$qry = $this->db->get(BOM::$TABLE_NAME);

					$skip = false;
					if ($qry->num_rows() > 0) { //Prod Code sudah Ada, sekarang cek lagi apakah ada alt bomnya
						$this->db->from(BOM::$TABLE_NAME);
						$this->db->where(BOM_Alt::$ALT_NO, $bom_alt);
						$this->db->where(BOM::$TABLE_NAME.".".BOM::$ID,$product_code);
						$join = BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$PRODUCT_CODE . "=" . BOM::$TABLE_NAME . "." . BOM::$ID;
						$this->db->join(BOM_Alt::$TABLE_NAME,$join);
						$qry_alt=$this->db->get();
						if($qry_alt->num_rows()>0) //cek apakah Prod code dengan altbom ada, jika ada skip
							$skip = true;
						else{
							$arr_bom[$ctr] = array(
								BOM::$ID => $product_code,
								BOM::$DESCRIPTION => strtoupper($description)
							);
						}
					} else {
						$arr_bom[$ctr] = array(
							BOM::$ID => $product_code,
							BOM::$DESCRIPTION => strtoupper($description)
						);
						//$this->db->insert(Worksheet_RM_PM::$TABLE_NAME, $data_ws_rm_pm[$ctr]);
					}

					if (!$skip) {
						//Insert Tabel BOM Alt
						$arr_bom_alt[$ctr] = array(
							BOM_Alt::$ALT_NO => $bom_alt,
							BOM_Alt::$PRODUCT_CODE => $product_code
						);

						$this->db->where(Material_RM_PM::$KODE_MATERIAL,$kode_rm_pm);
						$rmpm=$this->db->get(Material_RM_PM::$TABLE_NAME);
						//Insert Tabel BOM Alt Recipe
						$arr_bom_alt_recipe[$ctr] = array(
							BOM_Alt_Recipe::$NOMOR_URUT => $no_urut++,
							BOM_Alt_Recipe::$UNIT => $unit
						);
						if($rmpm->num_rows()>0){
							$rmpm = $rmpm->row_array();
							$arr_bom_alt_recipe[$ctr][BOM_Alt_Recipe::$ID_MATERIAL]= $rmpm[Material_RM_PM::$ID];
						}else{
							$arr_bom_alt_recipe[$ctr][BOM_Alt_Recipe::$KODE_FG] = $kode_rm_pm;
						}

						//Jika PCS Bulatkan
						if($this->equals($arr_bom_alt_recipe[$ctr][BOM_Alt_Recipe::$UNIT],Material_RM_PM::$S_UNIT_PCS)){
							$arr_bom_alt_recipe[$ctr][BOM_Alt_Recipe::$QTY] = round($qty);
						}else{
							$arr_bom_alt_recipe[$ctr][BOM_Alt_Recipe::$QTY] = $qty;
						}

						$ctr++;
					}
				} else
					break;
			}
		}
		for ($i = 0; $i < $ctr; $i++) {
			//Insert Tabel BOM
			if (isset($arr_bom[$i])) {
				$this->db->where(BOM::$ID,$arr_bom[$i][BOM::$ID]);
				$qry = $this->db->get(BOM::$TABLE_NAME);
				if ($qry->num_rows() > 0) {
				} else {
					$this->db->insert(BOM::$TABLE_NAME, $arr_bom[$i]);
				}
			}

			//Insert Tabel BOM Alt
			if (isset($arr_bom_alt[$i])) {
				$this->db->where(BOM_Alt::$ALT_NO, $arr_bom_alt[$i][BOM_Alt::$ALT_NO]);
				$this->db->where(BOM_Alt::$PRODUCT_CODE, $arr_bom_alt[$i][BOM_Alt::$PRODUCT_CODE]);
				$qry = $this->db->get(BOM_Alt::$TABLE_NAME);
				if ($qry->num_rows() > 0) {
				} else {
					$this->db->insert(BOM_Alt::$TABLE_NAME, $arr_bom_alt[$i]);
				}
			}

			//Insert Tabel BOM Alt Recipe
			if (isset($arr_bom_alt_recipe[$i])) {
				$this->db->where(BOM_Alt::$ALT_NO, $arr_bom_alt[$i][BOM_Alt::$ALT_NO]);
				$this->db->where(BOM_Alt::$PRODUCT_CODE, $arr_bom_alt[$i][BOM_Alt::$PRODUCT_CODE]);
				$bom_alt = $this->db->get(BOM_Alt::$TABLE_NAME)->row_array();

				$arr_bom_alt_recipe[$i][BOM_Alt_Recipe::$ID_ALT] = $bom_alt[BOM_Alt::$ID];
				
				$this->db->insert(BOM_Alt_Recipe::$TABLE_NAME, $arr_bom_alt_recipe[$i]);
				
			}
		}
		return BOM::$MESSAGE_SUCCESS_UPLOAD;
	}
	public function jd_qc_update_batch($data){
		$arr_listed = array();
		$qc = "";
		$old_product_code = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				$product_code = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
				//JI
				/*if(!in_array($product_code,$arr_listed) && $product_code != NULL){
					$jd = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					array_push($arr_listed, $product_code);
					$arr[BOM::$JOB_INSTRUCTION] = $jd;
					$this->db->where(BOM::$ID, $product_code);
					$this->db->update(BOM::$TABLE_NAME, $arr);
					
				}*/

				if ($old_product_code == "") {
					$old_product_code = $product_code;
					//$qc = "";
					//echo "\n\nawal:".$old_product_code."";
				}
				
				if ($product_code == NULL || $product_code == $old_product_code) {
					$item = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					$tm = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$spec = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					//echo "harusnya kosong".$qc;
					$qc .= $item . "|" . $tm . "|" . $spec . "\n";
					//echo $product_code."-".$qc."\n";
				} else {
					//Concat 2 Item
					$qc .= "N.V %|0| \n";
					$qc .= "STOV. S|0|";
					echo "\n* itemnya:" . $old_product_code . "\n$qc";
					$arr[BOM::$QC] = $qc;
					$this->db->where(BOM::$ID, $old_product_code);
					$this->db->update(BOM::$TABLE_NAME, $arr);
					$item = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					$tm = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$spec = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$qc = $item . "|" . $tm . "|" . $spec . "\n";

					$old_product_code = "";
				}
				if ($product_code != NULL) {
					$old_product_code = $product_code;
				}
				
			}
		}
		
		return BOM::$MESSAGE_SUCCESS_UPLOAD;
	}
	public function get_master(){
		$arr_select=[
			BOM::$TABLE_NAME . "." . BOM::$ID . " " . BOM::$ID,
			BOM::$DESCRIPTION,BOM_Alt::$ALT_NO, BOM_Alt::$ID
		];

		$this->db->select($arr_select);
		$this->db->from(BOM::$TABLE_NAME);
		
		$join= BOM::$TABLE_NAME.".". BOM::$ID."=". BOM_Alt::$TABLE_NAME.".". BOM_Alt::$PRODUCT_CODE;
		$this->db->join(BOM_Alt::$TABLE_NAME,$join);

		$this->db->order_by(BOM::$ID, $this::$ORDER_TYPE_ASC);
		$this->db->order_by(BOM_Alt::$ALT_NO, $this::$ORDER_TYPE_ASC);

		return $this->db->get();

	}

	public function pv_insert($ctr, $arr_product_code, $arr_id_alt, $arr_df, $arr_dt){

		for ($i = 0; $i < $ctr; $i++) {
			$data = [];

			$data[BOM_Production_Version::$CHECK_DATE] = date("Y-m-d");
			$data[BOM_Production_Version::$VALID_FROM] = date('Y-m-d', strtotime($arr_df[$i]));
			$data[BOM_Production_Version::$VALID_TO] = date('Y-m-d', strtotime($arr_dt[$i]));
			$data[BOM_Production_Version::$PRODUCT_CODE] = strtoupper($arr_product_code[$i]);
			$data[BOM_Production_Version::$ID_ALT] = $arr_id_alt[$i];
			
			$this->db->insert(BOM_Production_Version::$TABLE_NAME,$data);
		}

		return BOM_Production_Version::$MESSAGE_SUCCESS_INSERT;
	}
	public function pv_get_rekap($df, $sf, $dt, $st, $product_code){
		if ($df != NULL && $sf != NULL) {
			$until = date("Y-m-d", strtotime($sf . "+ 1 day"));
			$this->db->where(BOM_Production_Version::$VALID_FROM . ' >=', $df, TRUE);
			$this->db->where(BOM_Production_Version::$VALID_FROM . ' <', $until, TRUE);
		}
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(BOM_Production_Version::$VALID_TO . ' >=', $df, TRUE);
			$this->db->where(BOM_Production_Version::$VALID_TO . ' <', $until, TRUE);
		}

		if ($product_code != null) {
			$this->db->where("upper(".BOM_Production_Version::$TABLE_NAME . "." . BOM_Production_Version::$PRODUCT_CODE.")", strtoupper($product_code));
		}

		$arr_select = [
			BOM_Production_Version::$TABLE_NAME . "." . BOM_Production_Version::$PRODUCT_CODE . " " . BOM_Production_Version::$PRODUCT_CODE,
			BOM_Alt::$ALT_NO,BOM_Production_Version::$VALID_FROM,BOM_Production_Version::$VALID_TO,
			BOM::$DESCRIPTION			
		];

		$this->db->select($arr_select);
		$this->db->from(BOM_Production_Version::$TABLE_NAME);
		$join = BOM_Production_Version::$TABLE_NAME . "." . BOM_Production_Version::$ID_ALT . "=" . BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$ID;
		$this->db->join(BOM_Alt::$TABLE_NAME, $join);
		$join = BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$PRODUCT_CODE . "=" . BOM::$TABLE_NAME . "." . BOM::$ID;
		$this->db->join(BOM::$TABLE_NAME, $join);
		return $this->db->get();
	}
	public function pv_set_all_now(){
		$qry = $this->db->query("select * from bom_alt where bom_alt.id_bom_alt not in (select id_bom_alt from bom_production_version)");
		//$qry = $this->db->get(BOM_Alt::$TABLE_NAME);
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				$data_pv=[
					BOM_Production_Version::$ID_ALT => $row[BOM_Alt::$ID],
					BOM_Production_Version::$PRODUCT_CODE => $row[BOM_Alt::$PRODUCT_CODE],
					BOM_Production_Version::$VALID_TO => '9999-12-31',
				];
				$this->db->set(BOM_Production_Version::$CHECK_DATE, 'NOW()', FALSE);
				$this->db->set(BOM_Production_Version::$VALID_FROM, 'NOW()', FALSE);

				$this->db->insert(BOM_Production_Version::$TABLE_NAME,$data_pv);
			}
		}
	}


	public function get_by_id_bom_pv($id_pv){
		$this->db->from(BOM::$TABLE_NAME);
		$join = BOM::$TABLE_NAME . "." . BOM::$ID . "=" . BOM_Production_Version::$TABLE_NAME . "." . BOM_Production_Version::$PRODUCT_CODE;
		$this->db->join(BOM_Production_Version::$TABLE_NAME,$join);
		$this->db->where(BOM_Production_Version::$TABLE_NAME.".". BOM_Production_Version::$ID,$id_pv);
		return $this->db->get();
	}
}
