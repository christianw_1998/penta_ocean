<?php
class User_Access{
	public static $TABLE_NAME = "user_access";
	public static $ID = "id_user_access";

	public static $IS_TRUE = "is_true";

	//Foreign Key
	public static $NIK = "nik";
	public static $NIK_ATASAN = "nik_atasan";
	public static $ID_ACCESS = "id_access";

	public static $MESSAGE_SUCCESS_INSERT = "UACQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "UACQ002";
	public static $MESSAGE_FAILED_GET_NO_SESSION = "UACQ003";
	public static $MESSAGE_FAILED_UPDATE = "UACQ004";

	//Static Variables
	public static $WHILE_INSERT = "WHILE_INSERT";
	public static $WHILE_UPDATE = "WHILE_UPDATE";
}
class UserAccess_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function get_all(){
		$data = [];
		if (isset($_SESSION[SESSION_KARYAWAN_PENTA])) {
			$nik = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
			$data['status'] = 1;
			$data['data'] = $this->db->query("select user_access.nik nik, access, user_access.id_access id_access,description,is_true,nama
											from user_access,list_access,karyawan
											where user_access.id_access=list_access.id_access and karyawan.nik=user_access.nik and
											list_access.is_deleted=0 and user_access.nik_atasan='$nik' and user_access.is_deleted=0");
		} else {
			$data['status'] = 0;
			$data['data'] = User_Access::$MESSAGE_FAILED_GET_NO_SESSION;
		}
		return $data;
	}
	public function get_all_no_restriction(){
		$data = [];
		//if(isset($_SESSION[SESSION_KARYAWAN_PENTA])){
		$data['status'] = 1;
		$data['data'] = $this->db->query("select user_access.nik nik_bawahan,user_access.nik_atasan nik_atasan,user_access.is_deleted is_deleted, access, user_access.id_access id_access,description
											from user_access,list_access
											where user_access.id_access=list_access.id_access");
		//}else{
		//$data['status']=0;
		//$data['data']=User_Access::$MESSAGE_FAILED_GET_NO_SESSION;
		//}		
		return $data;
	}
}
