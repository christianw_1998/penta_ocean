<?php
class Change_Password{
	public static $MESSAGE_SUCCESS_UPDATE = 'CPWQ001';
	public static $MESSAGE_FAILED_UPDATE_NEW_PASSWORD_NOT_SAME = 'CPWQ002';
	public static $MESSAGE_FAILED_UPDATE_WRONG_OLD_PASSWORD = 'CPWQ003';
	public static $MESSAGE_FAILED_UPDATE_NEW_OLD_PASSWORD_SAME = 'CPWQ004';
	public static $MESSAGE_FAILED_UPDATE_FIELD_EMPTY = 'CPWQ005';
}
class Settings_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function change_password($old_password, $new_password, $confirm_new_password){
		if ($this->equals($old_password, "") || $this->equals($new_password, "") || $this->equals($confirm_new_password, "")) {
			return Change_Password::$MESSAGE_FAILED_UPDATE_FIELD_EMPTY;
		}
		if (md5($new_password) != md5($confirm_new_password)) {
			return Change_Password::$MESSAGE_FAILED_UPDATE_NEW_PASSWORD_NOT_SAME;
		}
		$this->db->where(Karyawan::$ID, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
		$qry = $this->db->get(Karyawan::$TABLE_NAME);
		$cur_password = $qry->row_array()[Karyawan::$PASSWORD];
		if ($cur_password != md5($old_password)) {
			return Change_Password::$MESSAGE_FAILED_UPDATE_WRONG_OLD_PASSWORD;
		}
		if ($cur_password == md5($new_password)) {
			return Change_Password::$MESSAGE_FAILED_UPDATE_NEW_OLD_PASSWORD_SAME;
		}
		$data = [];
		$data[Karyawan::$PASSWORD] = md5($new_password);
		$this->db->where(Karyawan::$ID, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
		$this->db->update(Karyawan::$TABLE_NAME, $data);
		return Change_Password::$MESSAGE_SUCCESS_UPDATE;
	}
	public function change_no_sj($old_code, $new_sj, $sto){
		$is_found = false;
		$this->db->where(H_Good_Issue::$ID, $old_code);
		$qry = $this->db->get(H_Good_Issue::$TABLE_NAME);

		if ($qry->num_rows() > 0) { //jika ketemu berarti revisi sj keluar
			$new_sj_insert = "";
			if ($this->equals($sto, Surat_Jalan::$KEYWORD_SO))
				$new_sj_insert = H_Good_Issue::$SO_CODE . date("y") . str_pad($new_sj, 7, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_STO))
				$new_sj_insert = H_Good_Issue::$STO_CODE . date("y") . str_pad($new_sj, 8, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_SIRCLO))
				$new_sj_insert = H_Good_Issue::$SIRCLO_CODE . date("y") . "-" . str_pad($new_sj, 5, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_RETUR))
				$new_sj_insert = H_Good_Issue::$RETUR_CODE . date("y") ."-". str_pad($new_sj, 7, "0", STR_PAD_LEFT);;
			
				//update header good issue
			$arr = [];
			$arr[H_Good_Issue::$NOMOR_SJ] = $new_sj_insert;
			$arr[H_Good_Issue::$STO] = $sto;
			$this->db->where(H_Good_Issue::$ID, $old_code);
			$this->db->update(H_Good_Issue::$TABLE_NAME, $arr);

			//update pesan history stock
			$arr_history = [];
			$this->db->like(Stock_Gudang::$PESAN, $old_code, 'both');
			$qry_history_stock = $this->db->get(Stock_Gudang::$TABLE_NAME);
			foreach ($qry_history_stock->result() as $row) {
				$pesan = $row->pesan;
				$arr_history[Stock_Gudang::$PESAN] = $pesan . " " . Surat_Jalan::GEN_UPDATE_MESSAGE($new_sj_insert, $_SESSION[SESSION_KARYAWAN_PENTA]['nik']);

				$this->db->where(Stock_Gudang::$ID, $row->id_stock_gudang);
				$this->db->update(Stock_Gudang::$TABLE_NAME, $arr_history);
			}
			$is_found = true;
			//return Surat_Jalan::$MESSAGE_SUCCESS_UPDATE;
		}

		$this->db->where(H_Good_Receipt::$ID, $old_code);
		$qry = $this->db->get(H_Good_Receipt::$TABLE_NAME);

		if ($qry->num_rows() > 0) { //jika ketemu berarti revisi sj masuk
			$new_sj_insert = "";
			if ($this->equals($sto, Surat_Jalan::$KEYWORD_SO))
				$new_sj_insert = H_Good_Receipt::$SO_CODE . date("y") . str_pad($new_sj, 7, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_STO))
				$new_sj_insert = H_Good_Receipt::$STO_CODE . date("y") . str_pad($new_sj, 8, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_RETUR))
				$new_sj_insert = H_Good_Receipt::$RETUR_CODE . date("y") ."-". str_pad($new_sj, 7, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_SIRCLO))
				$new_sj_insert = H_Good_Receipt::$SIRCLO_CODE . date("y") . "-" . str_pad($new_sj, 5, "0", STR_PAD_LEFT);
			
			//update header good receipt
			$arr = [];
			$arr[H_Good_Receipt::$NOMOR_SJ] = $new_sj_insert;
			$arr[H_Good_Receipt::$STO] = $sto;
			$this->db->where(H_Good_Receipt::$ID, $old_code);
			$this->db->update(H_Good_Receipt::$TABLE_NAME, $arr);

			//update pesan history stock
			$arr_history = [];
			$this->db->like(Stock_Gudang::$PESAN, $old_code, Library_Model::$WHERE_LIKE_BOTH);
			$qry_history_stock = $this->db->get(Stock_Gudang::$TABLE_NAME);
			foreach ($qry_history_stock->result() as $row) {
				$pesan = $row->pesan;
				$arr_history[Stock_Gudang::$PESAN] = $pesan . " " . Surat_Jalan::GEN_UPDATE_MESSAGE($new_sj_insert, $_SESSION[SESSION_KARYAWAN_PENTA]['nik']);

				$this->db->where(Stock_Gudang::$ID, $row->id_stock_gudang);
				$this->db->update(Stock_Gudang::$TABLE_NAME, $arr_history);
			}
			$is_found = true;
			//return Surat_Jalan::$MESSAGE_SUCCESS_UPDATE;
		}
		if ($is_found)
			return Surat_Jalan::$MESSAGE_SUCCESS_UPDATE;

		return Surat_Jalan::$MESSAGE_FAILED_UPDATE_OLD_SJ_NOT_FOUND;
	}
}
