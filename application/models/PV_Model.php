<?php
class PV_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function get_last_date($product_code){
		$this->db->select(BOM_Production_Version::$CHECK_DATE);
		$this->db->where(BOM_Production_Version::$PRODUCT_CODE,$product_code);
		$this->db->order_by(1,$this::$ORDER_TYPE_DESC);

		$data = $this->db->get(BOM_Production_Version::$TABLE_NAME);
		if($data->num_rows()>0){
			return $data->row_array()[BOM_Production_Version::$CHECK_DATE];
		}
	
		return "";
	}

	public function get_active_by_product_code($product_code){
		$arr_select=[
			BOM_Production_Version::$ID, BOM_Alt::$ALT_NO, BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$ID . " " . BOM_Alt::$ID
		];
		$this->db->select($arr_select);
		$this->db->where(BOM_Production_Version::$TABLE_NAME.".". BOM_Production_Version::$PRODUCT_CODE,$product_code);
		$today = date("Y-m-d");
		$this->db->where(BOM_Production_Version::$VALID_FROM . ' <=', $today, TRUE);
		$this->db->where(BOM_Production_Version::$VALID_TO . ' >=', $today, TRUE);

		$join = BOM_Production_Version::$TABLE_NAME . "." . BOM_Production_Version::$ID_ALT . "=" . BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$ID;
		$this->db->join(BOM_Alt::$TABLE_NAME, $join);

		$this->db->order_by(1,$this::$ORDER_TYPE_DESC);

		return $this->db->get(BOM_Production_Version::$TABLE_NAME);
	}

	public function fg_get_by_product_code($product_code){
		$arr_select=[
			BOM_Production_Version::$ID,BOM_Alt::$ALT_NO,BOM_Alt_Recipe::$QTY, BOM::$DESCRIPTION
		];
		$this->db->select($arr_select);
		$this->db->where(BOM_Production_Version::$TABLE_NAME.".". BOM_Production_Version::$PRODUCT_CODE,$product_code);
		$today = date("Y-m-d");
		$this->db->where(BOM_Production_Version::$VALID_FROM . ' <=', $today, TRUE);
		$this->db->where(BOM_Production_Version::$VALID_TO . ' >=', $today, TRUE);

		$join = BOM_Production_Version::$TABLE_NAME . "." . BOM_Production_Version::$ID_ALT . "=" . BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$ID;
		$this->db->join(BOM_Alt::$TABLE_NAME, $join);

		$join = BOM_Alt_Recipe::$TABLE_NAME . "." . BOM_Alt_Recipe::$ID_ALT . "=" . BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$ID;
		$this->db->join(BOM_Alt_Recipe::$TABLE_NAME, $join);
		$this->db->where(BOM_Alt_Recipe::$NOMOR_URUT , 1); //no urut 1 pasti FG

		$join = BOM_Alt::$TABLE_NAME . "." . BOM_Alt::$PRODUCT_CODE . "=" . BOM::$TABLE_NAME . "." . BOM::$ID;
		$this->db->join(BOM::$TABLE_NAME, $join);

		$this->db->order_by(1,$this::$ORDER_TYPE_DESC);

		return $this->db->get(BOM_Production_Version::$TABLE_NAME);
	}
}
