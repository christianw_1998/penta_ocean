<?php
class Absensi{
	public static $TABLE_NAME="absensi";
	public static $ID="id_absensi";
	public static $TANGGAL="tanggal";
	public static $JAM_MASUK="jam_masuk";
	public static $JAM_KELUAR="jam_keluar";
	public static $KETERANGAN="keterangan";

	//Foreign Key
	public static $NIK="nik";

	public static $MESSAGE_UPLOAD_SUCCESS="ABU001";
	public static $MESSAGE_UPLOAD_FAILED="ABU002";
	public static $MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE="ABU003";
}
class Absensi_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function get_all(){
		$arr_select=[
			Karyawan::$ID,Karyawan::$NAMA,Absensi::$TANGGAL,
			Absensi::$JAM_MASUK,Absensi::$JAM_KELUAR,Absensi::$KETERANGAN
		];
		$this->db->select($arr_select);
		$this->db->from(Absensi::$TABLE_NAME);
		$this->db->join(Karyawan::$TABLE_NAME,
						Karyawan::$TABLE_NAME.".".Karyawan::$ID."=".Absensi::$TABLE_NAME.".".Absensi::$NIK);
		$this->db->where(Karyawan::$IS_DELETED,0);
		return $this->db->get();
	}
}
