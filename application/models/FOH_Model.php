<?php
class FOH{
	public static $TABLE_NAME = "foh";
	public static $ID = "id_foh";

	public static $TANGGAL_INPUT = "tanggal_input";
	public static $TANGGAL_VALIDASI = "tanggal_validasi";
	public static $IS_VALIDATED = "is_validated";

	public static $S_INSERT_ID = "FOH";
	public static $MESSAGE_SUCCESS_INSERT = "FOQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "FOQ002";
	public static $MESSAGE_SUCCESS_VALIDATION = "FOQ004";
	public static $MESSAGE_FAILED_SEARCH_ALREADY_VALIDATED = "FOQ003";
}
class FOH_GL{
	public static $TABLE_NAME = "foh_gl";
	public static $ID = "id_foh_gl";

	public static $HARGA = "harga";
	public static $TANGGAL_UPDATE = "tanggal_update";

	public static $ID_FOH = "id_foh";
	public static $ID_GL = "id_gl";
}
class FOH_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function insert($ctr, $arr_id_gl, $arr_harga){
		$data = [];
		//FOH
		$data[FOH::$IS_VALIDATED] = 0;
		$this->db->set(FOH::$TANGGAL_INPUT, 'NOW()', FALSE);

		//auto generate FOH24090001
		$this->db->select("max(substr(" . FOH::$ID . ",8)) as " . FOH::$ID);
		$this->db->where("MONTH(" . FOH::$TANGGAL_INPUT . ")", date("m"));
		$this->db->where("YEAR(" . FOH::$TANGGAL_INPUT . ")", date("Y"));
		$qry_sj = $this->db->get(FOH::$TABLE_NAME)->row_array();
		$max = $qry_sj[FOH::$ID];
		echo $qry_sj[FOH::$ID];
		if ($max == null) $max = 0;
		$max += 1;

		$kode = date("y") .date("m") .str_pad($max, 5, "0", STR_PAD_LEFT);
		$data[FOH::$ID] = FOH::$S_INSERT_ID . $kode;
		echo "<script>alert('Kode FOH: " . $data[FOH::$ID] . "');</script>";
		
		$this->db->insert(FOH::$TABLE_NAME,$data);
		for ($i = 0; $i < $ctr; $i++) {
			
			//FOH GL
			$data = [];
			$data[FOH_GL::$ID] = FOH::$S_INSERT_ID . $kode . str_pad($i + 1, 3, "0", STR_PAD_LEFT);
			$data[FOH_GL::$ID_FOH] = FOH::$S_INSERT_ID . $kode;
			$data[FOH_GL::$HARGA] = $arr_harga[$i];
			$data[FOH_GL::$ID_GL] = $arr_id_gl[$i];
			$this->db->set(FOH_GL::$TANGGAL_UPDATE, 'NOW()', FALSE);
			$this->db->insert(FOH_GL::$TABLE_NAME, $data);
		}
		
		return FOH::$MESSAGE_SUCCESS_INSERT;
	}

	public function update($id_foh, $ctr, $arr_id_gl, $arr_harga){
		$data = [];
		for ($i = 0; $i < $ctr; $i++) {
			
			//FOH GL
			$data = [];

			//Cek apakah ID GL sudah ada, jika ada update
			$this->db->where(FOH_GL::$ID_GL,$arr_id_gl[$i]);
			$this->db->where(FOH_GL::$ID_FOH, $id_foh);
			$data_foh_gl = $this->db->get(FOH_GL::$TABLE_NAME);

			if($data_foh_gl->num_rows()>0){
				$row_foh_gl = $data_foh_gl->row_array();
				if($row_foh_gl[FOH_GL::$HARGA] != $arr_harga[$i]){ //jika harga beda baru update
					$data[FOH_GL::$HARGA] = $arr_harga[$i];
					
					$this->db->set(FOH_GL::$TANGGAL_UPDATE, 'NOW()', FALSE);
					$this->db->where(FOH_GL::$ID_GL, $arr_id_gl[$i]);
					$this->db->where(FOH_GL::$ID_FOH, $id_foh);
					$this->db->update(FOH_GL::$TABLE_NAME, $data);
				}
			}else{ //FOH GL baru
				$this->db->select("max(substr(" . FOH_GL::$ID . ",11)) as " . FOH_GL::$ID);
				$this->db->where(FOH_GL::$ID_FOH, $id_foh);
				$ai = $this->db->get(FOH_GL::$TABLE_NAME)->row_array()[FOH_GL::$ID] + 1; 
				$data[FOH_GL::$ID] = $id_foh . str_pad($ai + 1, 3, "0", STR_PAD_LEFT);
				$data[FOH_GL::$ID_FOH] = $id_foh;
				$data[FOH_GL::$ID_GL] = $arr_id_gl[$i];
				$data[FOH_GL::$HARGA] = $arr_harga[$i];
				$this->db->set(FOH_GL::$TANGGAL_UPDATE, 'NOW()', FALSE);
				$this->db->insert(FOH_GL::$TABLE_NAME, $data);
			}
			
		}
		
		return FOH::$MESSAGE_SUCCESS_UPDATE;
	}

	public function validasi($id_foh){
		$data = [];
		$data[FOH::$IS_VALIDATED] = 1;
		$this->db->where(FOH::$ID,$id_foh);
		$this->db->set(FOH::$TANGGAL_VALIDASI, 'NOW()', FALSE);
		$this->db->update(FOH::$TABLE_NAME, $data);
		
		return FOH::$MESSAGE_SUCCESS_VALIDATION;
	}

	public function get_history($dt, $st){
		$arr_select = [
			FOH::$TABLE_NAME . "." . FOH::$ID . " " . FOH::$ID, FOH_GL::$TABLE_NAME . "." . FOH_GL::$ID_GL . " " . FOH_GL::$ID_GL,
			General_Ledger::$DESKRIPSI, FOH_GL::$HARGA,FOH::$TANGGAL_INPUT,FOH_GL::$TANGGAL_UPDATE,FOH::$TANGGAL_VALIDASI
		];
		$this->db->select($arr_select);
		$this->db->from(FOH::$TABLE_NAME);
		$join = FOH::$TABLE_NAME . "." . FOH::$ID . "=" . FOH_GL::$TABLE_NAME . "." . FOH_GL::$ID_FOH;
		$this->db->join(FOH_GL::$TABLE_NAME, $join);
		$join = FOH_GL::$TABLE_NAME . "." . FOH_GL::$ID_GL . "=" . General_Ledger::$TABLE_NAME . "." . General_Ledger::$ID;
		$this->db->join(General_Ledger::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(FOH::$TANGGAL_INPUT . ' >=', $dt, TRUE);
			$this->db->where(FOH::$TANGGAL_INPUT . ' <', $until, TRUE);
		}
		return $this->db->get();
	}

}
