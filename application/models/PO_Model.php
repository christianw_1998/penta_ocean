<?php
class Purchase_Order{
	public static $TABLE_NAME = "purchase_order";
	public static $ID = "nomor_po";

	public static $TANGGAL = "tanggal";
	public static $TANGGAL_KIRIM = "tanggal_kirim";
	public static $TANGGAL_ESTIMASI_SAMPAI = "tanggal_estimasi_sampai";
	public static $PPN = "ppn";
	public static $DISKON = "diskon";
	public static $IS_DONE = "is_done";
	public static $IS_DELETED = "is_deleted";

	//Foreign Key
	public static $ID_PAYTERM = "id_payterm";
	public static $ID_PR = "id_purchase_request_header";

	public static $S_CODE = "50";
	public static $S_MUT_CODE = "ADJ"; //mutasi
	public static $S_RM_CODE = "497"; //PO RM
	public static $S_PM_CODE = "498"; //PO PM


	public static $MESSAGE_SUCCESS_UPLOAD = "PROQ001";
	public static $MESSAGE_FAILED_SEARCH_PO_NOT_FOUND = "PROQ002";
	public static $MESSAGE_FAILED_CANCEL_QTY_NOT_ENOUGH = "PROQ003";
	public static $MESSAGE_SUCCESS_CANCEL = "PROQ004";
	public static $MESSAGE_SUCCESS_MUTASI = "PROQ005";
	public static $MESSAGE_SUCCESS_INSERT = "PROQ006";
	public static $MESSAGE_FAILED_INSERT_VENDOR_NOT_SAME = "PROQ007";
	public static $MESSAGE_SUCCESS_CHANGE_STATUS = "PROQ008";
	

	public static function GEN_CANCEL_MESSAGE($params, $params2){
		return "Cancel GR " . $params . " oleh " . $params2;
	}
}
class Purchase_Order_Awal{
	public static $TABLE_NAME = "purchase_order_awal";
	public static $ID = "id_purchase_order_awal";

	public static $NOMOR_URUT = "nomor_urut";
	public static $SHORT_TEXT = "short_text";
	public static $QTY = "qty";
	public static $NET_PRICE = "net_price";
	public static $QTY_TEMPLATE = "qty_template";

	//Foreign Key
	public static $NIK = "nik";
	public static $ID_MATERIAL = "id_material_rm_pm";
	public static $NOMOR_PO = "nomor_po";
	public static $ID_VENDOR = "id_vendor";

	//Static 
	public static $S_QTY_SISA = "qty_sisa";


	public static $MESSAGE_SUCCESS_REVISI = "POAQ001";
}
class Purchase_Order_Checker_Header{
	public static $TABLE_NAME = "purchase_order_checker_header";
	public static $ID = "id_purchase_order_checker_header";

	public static $TANGGAL = "tanggal";
	public static $TANGGAL_CONFIRM_ADMIN = "tanggal_confirm_admin";
	public static $EV_STATUS_PENGIRIMAN = "evaluasi_status_pengiriman";
	public static $EV_KUALITAS = "evaluasi_kualitas";
	public static $EV_QTY_PENERIMAAN = "evaluasi_qty_penerimaan";
	public static $EV_AFTER_SALES = "evaluasi_after_sales";
	public static $NOMOR_SJ = "nomor_sj";
	public static $IS_DELETED = "is_deleted";

	//Foreign Key
	public static $NIK = "nik";
	public static $NOMOR_PO = "nomor_po";

	public static $MESSAGE_SUCCESS_INSERT = "POCQ001";
	public static $MESSAGE_SUCCESS_INSERT_EVALUATION = "POCQ003";
	public static $MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND = "POCQ002";
	public static $MESSAGE_FAILED_UPDATE_OLD_SJ_NOT_FOUND = 'POCQ004';
	public static $MESSAGE_SUCCESS_UPDATE_NO_SJ = 'POCQ005';


	public static function GEN_INSERT_MESSAGE($params, $params2, $params3){
		return "PO " . $params . " oleh " . $params3 . " (Kode: " . $params2 . ")";
	}
	public static function GEN_UPDATE_SJ_MESSAGE($params, $params2, $params3){
		return "(Direvisi no SJ dari $params menjadi $params2 oleh NIK: $params3)";
	}
}
class Purchase_Order_Checker_Detail{
	public static $TABLE_NAME = "purchase_order_checker_detail";
	public static $ID = "id_purchase_order_checker";

	public static $NOMOR_URUT = "nomor_urut";
	public static $QTY = "qty";
	public static $QTY_SJ = "qty_sj";

	public static $S_TOT_QTY = "tot_qty";

	//Foreign Key
	public static $ID_MATERIAL = "id_material_rm_pm";
	public static $ID_HEADER = "id_purchase_order_checker_header";

	public static $MESSAGE_SUCCESS_INSERT = "POCQ001";
	public static $MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND = "POCQ002";

	public static function GEN_INSERT_MESSAGE($params, $params2, $params3)
	{
		return "PO " . $params . " oleh " . $params3 . " (Kode: " . $params2 . ")";
	}

	public static function GEN_MUTASI_MESSAGE($params, $params2, $params3)
	{
		return "PO (Mutasi) oleh " . $params . " (Kode: " . $params2 . ", Pesan: " . $params3 . ")";
	}
}
class Purchase_Order_Price_List{
	public static $TABLE_NAME = "purchase_order_pricelist";
	public static $ID = "id_po_pricelist";

	public static $TANGGAL_MULAI = "tanggal_mulai";
	public static $TANGGAL_AKHIR = "tanggal_akhir";
	public static $CURRENCY = "currency";
	public static $PRICE = "price";

	//Foreign Key
	public static $ID_MATERIAL = "id_material_rm_pm";
	public static $ID_VENDOR = "id_vendor";

	//Static Key
	public static $S_CURRENCY_USD = "USD";
	public static $S_CURRENCY_IDR = "IDR";


	//Message
	public static $MESSAGE_SUCCESS_INSERT = "POPQ001";

}


class PO_Model extends RMPM_Model {
	public static $MIN_ESTIMATION_ARRIVAL_DAY=14;
	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika PO tidak kosong
					$tanggal = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$nomor_po = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$item = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$vendor = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					$kode_material = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$short_text = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$qty_template = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;
					$qty = isset($data['values'][$i]["I"]) ? $data['values'][$i]["I"] : NULL;
					$net_price = isset($data['values'][$i]["J"]) ? $data['values'][$i]["J"] : NULL;

					//Insert Tabel PO
					$data_query = array(
						Purchase_Order::$TANGGAL => date('Y-m-d', strtotime($tanggal)),
						Purchase_Order::$TANGGAL_ESTIMASI_SAMPAI => date('Y-m-d', strtotime( $tanggal . "+ ". $this::$MIN_ESTIMATION_ARRIVAL_DAY ." day")),
						Purchase_Order::$IS_DONE => 0,
						Purchase_Order::$IS_DELETED => 0
					);

					$this->db->where(Purchase_Order::$ID, $nomor_po);
					$qry = $this->db->get(Purchase_Order::$TABLE_NAME);
					if ($qry->num_rows() > 0) { //Update PO
						//$this->db->where(Purchase_Order::$ID,$nomor_po);
						//$this->db->update(Purchase_Order::$TABLE_NAME,$data_query);
					} else {
						$data_query[Purchase_Order::$ID] = $nomor_po;
						$this->db->insert(Purchase_Order::$TABLE_NAME, $data_query);
					}

					$this->db->where(Material_RM_PM::$KODE_MATERIAL, $kode_material);
					if ($net_price == 0) {
						$this->db->where(Material_RM_PM::$IS_KONSINYASI, 1);
					}
					$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
					$id_material = null;
					if ($qry->num_rows() > 0) {
						$id_material = $qry->row_array()[Material_RM_PM::$ID];
					}

					$id_vendor = substr($vendor, 0, 6);
					//Insert Tabel PO Awal
					$data_query = array(
						Purchase_Order_Awal::$NOMOR_URUT => $item,
						Purchase_Order_Awal::$SHORT_TEXT => $short_text,
						Purchase_Order_Awal::$QTY => $qty,
						Purchase_Order_Awal::$QTY_TEMPLATE => $qty_template,
						Purchase_Order_Awal::$NET_PRICE => ($net_price),
						Purchase_Order_Awal::$NOMOR_PO => $nomor_po,
						Purchase_Order_Awal::$ID_MATERIAL => $id_material,
						Purchase_Order_Awal::$ID_VENDOR => $id_vendor,
						Purchase_Order_Awal::$NIK => $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
					);
					$this->db->where(Purchase_Order_Awal::$NOMOR_PO, $nomor_po);
					$this->db->where(Purchase_Order_Awal::$NOMOR_URUT, $item);
					$qry = $this->db->get(Purchase_Order_Awal::$TABLE_NAME);
					if ($qry->num_rows() == 0)
						$this->db->insert(Purchase_Order_Awal::$TABLE_NAME, $data_query);
				} else
					break;
			}
		}
		return Purchase_Order::$MESSAGE_SUCCESS_UPLOAD;
	}
	public function insert($id_pr, $id_payterm, $ctr, $arr_id_material, $arr_deskripsi, $arr_id_vendor, $arr_qty, $arr_netprice, $ppn, $diskon, $tgl, $tgl_kirim){

		$kode = "";
		//Cek dulu apa PO RM atau PM dengan ambil 1 material
		$tipe=$this->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$ID, $arr_id_material[0])->row_array()[Material_RM_PM::$TIPE];
		if ($this->equals($tipe, Purchase_Request::$S_TYPE_RM)) {
			$kode = Purchase_Order::$S_RM_CODE;			
		}else if ($this->equals($tipe, Purchase_Request::$S_TYPE_PM)) {
			$kode = Purchase_Order::$S_PM_CODE;
		}

		//Ambil Kode Maks
		$this->db->select("max(substr(" . Purchase_Order::$ID . ",4)) as " . Purchase_Order::$ID);
		$this->db->like(Purchase_Order::$ID, $kode, 'after');
		$qry_pr = $this->db->get(Purchase_Order::$TABLE_NAME)->row_array();

		$max = intval($qry_pr[Purchase_Order::$ID]);
		if ($max == null) $max = 0;
		$max += 1;

		//Cek Vendor harus sama dalam 1 PO
		/*$id_vendor = $idv[0];
		for ($i = 0; $i < $ctr; $i++) {
			if(!$this->equals($id_vendor,$idv[$i])){
				return Purchase_Order::$MESSAGE_FAILED_INSERT_VENDOR_NOT_SAME;
			}
		}*/

		//Purchase Order
		$kode .= str_pad($max, 7, "0", STR_PAD_LEFT);
		echo "<script>alert('Nomor PO: " . $kode . "');</script>";

		$arr_insert = [
			Purchase_Order::$ID => $kode,
			Purchase_Order::$TANGGAL => date('Y-m-d', strtotime($tgl)),
			Purchase_Order::$TANGGAL_KIRIM => date('Y-m-d', strtotime($tgl_kirim)),
			Purchase_Order::$IS_DONE => 0,
			Purchase_Order::$IS_DELETED => 0,
			Purchase_Order::$ID_PAYTERM => strtoupper($id_payterm),
			Purchase_Order::$TANGGAL_ESTIMASI_SAMPAI => date('Y-m-d', strtotime($tgl_kirim . "+ " . $this::$MIN_ESTIMATION_ARRIVAL_DAY . " day")),
			Purchase_Order::$PPN => $ppn,
			Purchase_Order::$DISKON => $diskon,
			Purchase_Order::$ID_PR => $id_pr
		];
		$this->db->insert(Purchase_Order::$TABLE_NAME,$arr_insert);

		//Purchase Order Awal
		$no_urut = 1;
		for ($i = 0; $i < $ctr; $i++) {
			$arr_insert=[];
			$arr_insert[Purchase_Order_Awal::$NOMOR_URUT] = ($no_urut) . "0";
			$desc = $this->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $arr_id_material[$i])->row_array()[Material_RM_PM::$DESCRIPTION];
			$arr_insert[Purchase_Order_Awal::$SHORT_TEXT] = strtoupper($desc);
			$arr_insert[Purchase_Order_Awal::$QTY_TEMPLATE] = round($arr_qty[$i]);
			$arr_insert[Purchase_Order_Awal::$QTY] = round($arr_qty[$i]);
			$arr_insert[Purchase_Order_Awal::$NET_PRICE] = round($arr_netprice[$i]);
			$arr_insert[Purchase_Order_Awal::$NOMOR_PO] = $kode;
			$arr_insert[Purchase_Order_Awal::$ID_MATERIAL] = $arr_id_material[$i];
			$arr_insert[Purchase_Order_Awal::$ID_VENDOR] = $arr_id_vendor;
			$arr_insert[Purchase_Order_Awal::$SHORT_TEXT] = $arr_deskripsi[$i];
			$arr_insert[Purchase_Order_Awal::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];

			if($arr_insert[Purchase_Order_Awal::$QTY]!=0){
				$this->db->insert(Purchase_Order_Awal::$TABLE_NAME, $arr_insert);
				$no_urut += 1;

			}
		}

		//Update PR supaya tidak bisa dipanggil
		$arr_update=[Purchase_Request_Header::$IS_DONE => 1];
		$this->db->where(Purchase_Request_Header::$ID, $id_pr);
		$this->db->update(Purchase_Request_Header::$TABLE_NAME,$arr_update);
		
		return Purchase_Order::$MESSAGE_SUCCESS_INSERT;

	}
	public function revisi($diskon, $ppn, $nomor_po, $ctr, $arr_nomor_urut, $arr_id_material, $arr_deskripsi, $arr_id_vendor, $arr_qty, $arr_netprice){
		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];
			$data_detail[Purchase_Order_Awal::$QTY] = $arr_qty[$i];
			$data_detail[Purchase_Order_Awal::$SHORT_TEXT] = strtoupper($arr_deskripsi[$i]);
			$data_detail[Purchase_Order_Awal::$ID_VENDOR] = $arr_id_vendor[$i];
			$data_detail[Purchase_Order_Awal::$NET_PRICE] = $arr_netprice[$i];

			$this->db->where(Purchase_Order_Awal::$NOMOR_PO, $nomor_po);
			$this->db->where(Purchase_Order_Awal::$NOMOR_URUT, $arr_nomor_urut[$i]);
			$this->db->where(Purchase_Order_Awal::$ID_MATERIAL, $arr_id_material[$i]);

			$this->db->update(Purchase_Order_Awal::$TABLE_NAME, $data_detail);
	
		}
		$data_po = [
			Purchase_Order::$DISKON => $diskon,
			Purchase_Order::$PPN => $ppn
		];
		$this->db->where(Purchase_Order::$ID, $nomor_po);
		$this->db->update(Purchase_Order::$TABLE_NAME, $data_po);

		return Purchase_Order_Awal::$MESSAGE_SUCCESS_REVISI;

	}
	public function cancel($nomor_gr){
		$this->db->where(Purchase_Order_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Purchase_Order_Checker_Header::$ID, $nomor_gr);
		$qry_header = $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);

		$ctr = 0;
		if ($qry_header->num_rows() > 0) {
			$header = $qry_header->row_array();
			$this->db->where(Purchase_Order_Checker_Detail::$ID_HEADER, $header[Purchase_Order_Checker_Header::$ID]);
			$qry_detail = $this->db->get(Purchase_Order_Checker_Detail::$TABLE_NAME);

			$arr_insert = array();
			if ($qry_detail->num_rows() > 0) {
				foreach ($qry_detail->result_array() as $row_detail) {

					$this->db->where(Purchase_Order_Awal::$NOMOR_URUT, $row_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT]);
					$this->db->where(Purchase_Order_Awal::$NOMOR_PO, $header[Purchase_Order_Checker_Header::$NOMOR_PO]);
					$vendor = $this->db->get(Purchase_Order_Awal::$TABLE_NAME)->row_array()[Purchase_Order_Awal::$ID_VENDOR];

					$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $vendor);
					$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $row_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL]);

					$qry_material_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
					if ($qry_material_stock->num_rows() > 0) {
						$material_stock = $qry_material_stock->row_array();
					} else {
						$vendor = null;
						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $row_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL]);
						$qry_material_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
						$material_stock = $qry_material_stock->row_array();
					}

					if ($material_stock[Material_RM_PM_Stock::$QTY] > 0) {

						$this->db->where(Purchase_Order_Awal::$NOMOR_URUT, $row_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT]);
						$this->db->where(Purchase_Order_Awal::$NOMOR_PO, $header[Purchase_Order_Checker_Header::$NOMOR_PO]);
						$row_po_awal = $this->db->get(Purchase_Order_Awal::$TABLE_NAME)->row_array();

						$new_stock = $material_stock[Material_RM_PM_Stock::$QTY] - $row_detail[Purchase_Order_Checker_Detail::$QTY];
						if ($new_stock < 0) {
							return Purchase_Order::$MESSAGE_FAILED_CANCEL_QTY_NOT_ENOUGH;
						}
						$arr_insert[$ctr] = array(
							Material_RM_PM_Stock::$ID => $material_stock[Material_RM_PM_Stock::$ID],
							Purchase_Order_Awal::$NET_PRICE => $row_po_awal[Purchase_Order_Awal::$NET_PRICE],
							Purchase_Order_Checker_Header::$NOMOR_SJ => $header[Purchase_Order_Checker_Header::$NOMOR_SJ],
							Purchase_Order_Checker_Detail::$ID_MATERIAL => $row_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL],
							Purchase_Order_Awal::$ID_VENDOR => $vendor,
							Purchase_Order_Checker_Detail::$QTY => $row_detail[Purchase_Order_Checker_Detail::$QTY],
							Material_RM_PM_Stock::$ID => $material_stock[Material_RM_PM_Stock::$ID]
						);
						$ctr++;
					} else
						return Purchase_Order::$MESSAGE_FAILED_CANCEL_QTY_NOT_ENOUGH;
				}
				for ($i = 0; $i < $ctr; $i++) {
					$material_rm_pm_stock = $this->get(Material_RM_PM_Stock::$TABLE_NAME, Material_RM_PM_Stock::$ID, $arr_insert[$i][Material_RM_PM_Stock::$ID],null.null,null,false)->row_array();
					$new_value=$material_rm_pm_stock[Material_RM_PM_Stock::$VALUE]-($arr_insert[$i][Purchase_Order_Checker_Detail::$QTY]*$arr_insert[$i][Purchase_Order_Awal::$NET_PRICE]);
					$tanggal=date("Y-m-d");
					//Insert History Stock RM PM
					$this->insert_history_stock(
						$header[Purchase_Order_Checker_Header::$NOMOR_PO],
						$arr_insert[$i][Purchase_Order_Checker_Header::$NOMOR_SJ],
						$tanggal,
						$arr_insert[$i][Purchase_Order_Checker_Detail::$ID_MATERIAL],
						$arr_insert[$i][Purchase_Order_Awal::$ID_VENDOR],
						$material_rm_pm_stock[Material_RM_PM_Stock::$VALUE],
						0,
						$arr_insert[$i][Purchase_Order_Checker_Detail::$QTY],
						$new_value,
						Purchase_Order::GEN_CANCEL_MESSAGE($header[Purchase_Order_Checker_Header::$ID], $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])
					);


					$data = array(
						Material_RM_PM_Stock::$QTY => $material_rm_pm_stock[Material_RM_PM_Stock::$QTY]-$arr_insert[$i][Purchase_Order_Checker_Detail::$QTY],
						Material_RM_PM_Stock::$VALUE => $new_value
					);
					$this->db->where(Material_RM_PM_Stock::$ID, $arr_insert[$i][Material_RM_PM_Stock::$ID]);
					$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
				}
			}
		}
		$data = array(Purchase_Order_Checker_Header::$IS_DELETED => 1);
		$this->db->where(Purchase_Order_Checker_Header::$ID, $nomor_gr);
		$this->db->update(Purchase_Order_Checker_Header::$TABLE_NAME, $data);
		return Purchase_Order::$MESSAGE_SUCCESS_CANCEL;
	}
	public function change_no_sj($old_sj, $new_sj){
		$is_found = false;
		$this->db->where(Purchase_Order_Checker_Header::$ID, $old_sj);
		$qry = $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);

		if ($qry->num_rows() > 0) {
			//update header purchase_order_checker_header
			$arr = [];
			$arr[Purchase_Order_Checker_Header::$NOMOR_SJ] = $new_sj;
			$this->db->where(Purchase_Order_Checker_Header::$ID, $old_sj);
			$this->db->update(Purchase_Order_Checker_Header::$TABLE_NAME, $arr);

			$is_found = true;

			//update history stock rm_pm
			$arr_history = [];
			$this->db->like(Stock_Gudang_RM_PM::$PESAN, $old_sj, 'both');
			$qry_history_stock = $this->db->get(Stock_Gudang_RM_PM::$TABLE_NAME);
			foreach ($qry_history_stock->result_array() as $row) {
				$arr_history[Stock_Gudang_RM_PM::$NOMOR_SJ] = $new_sj;
				$pesan = $row[Stock_Gudang_RM_PM::$PESAN];
				$old_sj = $row[Stock_Gudang_RM_PM::$NOMOR_SJ];
				$arr_history[Stock_Gudang_RM_PM::$PESAN] = $pesan . " " . Purchase_Order_Checker_Header::GEN_UPDATE_SJ_MESSAGE($old_sj,$new_sj, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);

				$this->db->where(Stock_Gudang_RM_PM::$ID, $row[Stock_Gudang_RM_PM::$ID]);
				$this->db->update(Stock_Gudang_RM_PM::$TABLE_NAME, $arr_history);
			}
		}
		if ($is_found)
			return Purchase_Order_Checker_Header::$MESSAGE_SUCCESS_UPDATE_NO_SJ;

		return Purchase_Order_Checker_Header::$MESSAGE_FAILED_UPDATE_OLD_SJ_NOT_FOUND;
	}
	public function change_status($nomor_po, $is_deleted){
		$this->db->where(Purchase_Order::$ID, $nomor_po);
		$data = [];
		$data[Purchase_Order::$IS_DELETED] = $is_deleted;
		$this->db->update(Purchase_Order::$TABLE_NAME,$data);

		
		$this->db->where(Purchase_Order::$ID, $nomor_po);
		$pr = $this->db->get(Purchase_Order::$TABLE_NAME)->row_array()[Purchase_Order::$ID_PR];
		if ($pr != null) {
			$data_pr = [
				Purchase_Request_Header::$IS_DONE => $is_deleted * -1 + 1
			];
			$this->db->where(Purchase_Request_Header::$ID, $pr);
			$this->db->update(Purchase_Request_Header::$TABLE_NAME, $data_pr);
		}
		return Purchase_Order::$MESSAGE_SUCCESS_CHANGE_STATUS;
	}

	public function get_last_date(){
		$this->db->order_by(Purchase_Order::$TANGGAL, $this::$ORDER_TYPE_DESC);
		$qry = $this->db->get(Purchase_Order::$TABLE_NAME);
		if ($qry->num_rows() > 0)
			return date('d M Y', strtotime($qry->row_array()[Purchase_Order::$TANGGAL]));
		return null;
	}
	public function get_utuh($dt, $st){
		$select=[
			Purchase_Order::$ID,
			Purchase_Order::$TANGGAL,
			Purchase_Order::$IS_DELETED
		];
		$this->db->select($select);
		$this->db->from(Purchase_Order::$TABLE_NAME);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order::$TANGGAL . ' <', $until, TRUE);
		}
		$where_not_in = " select " . Purchase_Order_Checker_Header::$NOMOR_PO . 
							" from " . Purchase_Order_Checker_Header::$TABLE_NAME . 
							" where " . Purchase_Order_Checker_Header::$NOMOR_PO . " is not null and " . Purchase_Order_Checker_Header::$IS_DELETED . "=0";
	 	$this->db->where(Purchase_Order::$ID." not in (".$where_not_in.")");

		return $this->db->get();
		echo $this->db->last_query();

	}
	public function get_by_no($no){
		$this->db->where(Purchase_Order::$ID, $no);
		$data[Purchase_Order::$TABLE_NAME] = $this->db->get(Purchase_Order::$TABLE_NAME);
		$this->db->where(Purchase_Order_Awal::$NOMOR_PO, $no);
		$data[Purchase_Order_Awal::$TABLE_NAME] = $this->db->get(Purchase_Order_Awal::$TABLE_NAME);

		return $data;
	}
	public function get_last_qty($nomor_po, $no_urut, $qty_awal){
		$qty = $qty_awal;
		$this->db->where(Purchase_Order_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Purchase_Order_Checker_Header::$NOMOR_PO, $nomor_po);
		$qry = $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);
		if ($qry->num_rows() > 0) {
			foreach ($qry->result_array() as $row) {
				$this->db->where(Purchase_Order_Checker_Detail::$ID_HEADER, $row[Purchase_Order_Checker_Header::$ID]);
				$this->db->where(Purchase_Order_Checker_Detail::$NOMOR_URUT, $no_urut);
				$qty_potong=0;
				$qry_po_detail= $this->db->get(Purchase_Order_Checker_Detail::$TABLE_NAME);
				if($qry_po_detail->num_rows()>0)
					$qty_potong = $qry_po_detail->row_array()[Purchase_Order_Checker_Detail::$QTY];
				$qty -= $qty_potong;
			}
		}

		return $qty;
	}
	public function get_all_distinct_view_cancel(){
		$this->db->where(Purchase_Order_Checker_Header::$IS_DELETED, 0);
		$qry_header = $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);
		$arr_no_po = array();
		$ctr = 0;
		if ($qry_header->num_rows() > 0) {
			foreach ($qry_header->result_array() as $row_header) {
				$arr_no_po[$ctr++] = $row_header[Purchase_Order_Checker_Header::$NOMOR_PO];
			}
		}
		$this->db->select("distinct(" . Purchase_Order_Awal::$NOMOR_PO . ") as ". Purchase_Order_Awal::$NOMOR_PO);
		if ($ctr != 0) {
			$this->db->where_in(Purchase_Order_Awal::$NOMOR_PO, $arr_no_po);
			return $this->db->get(Purchase_Order_Awal::$TABLE_NAME);
		}
		return null;
	}
	public function get_report($dt, $st){
		/*select distinct purchase_order.nomor_po,
		purchase_order.tanggal,purchase_order_awal.nomor_urut,purchase_order_awal.id_vendor,
		purchase_order_awal.id_material_rm_pm,purchase_order_awal.qty_template,
		(qty_template-(COALESCE((select sum(qty)
									from purchase_order_checker_header hd,purchase_order_checker_detail
									where hd.id_purchase_order_checker_header=purchase_order_checker_detail.id_purchase_order_checker_header 
									and hd.nomor_po=purchase_order_awal.nomor_po
									group by hd.nomor_po
									),0))) as qty_akhir
		from purchase_order,purchase_order_awal
		where purchase_order.nomor_po=purchase_order_awal.nomor_po
		*/
		$arr_select = [
			Purchase_Order::$TABLE_NAME . "." . Purchase_Order::$ID . " as " . Purchase_Order::$ID,
			Purchase_Order::$TANGGAL, Purchase_Order::$TABLE_NAME . "." . Purchase_Order::$IS_DELETED . " as " . Purchase_Order::$IS_DELETED,
			Purchase_Order_Awal::$NOMOR_URUT, Vendor::$NAMA, Vendor::$TABLE_NAME . "." . Vendor::$ID . " " . Vendor::$ID,
			Material_RM_PM::$KODE_MATERIAL, "mm.". Material_RM_PM::$ID,
			Purchase_Order_Awal::$SHORT_TEXT,
			Purchase_Order_Awal::$QTY_TEMPLATE, Purchase_Order_Awal::$QTY,
			"(" . Purchase_Order_Awal::$QTY . "-COALESCE((select sum(qty)
												from purchase_order_checker_header hd,purchase_order_checker_detail dd
												where hd.id_purchase_order_checker_header=dd.id_purchase_order_checker_header 
												and hd.nomor_po=purchase_order_awal.nomor_po and dd.id_material_rm_pm=mm.id_material_rm_pm and hd.is_deleted=0
												group by hd.nomor_po
												),0)) as " . Purchase_Order_Awal::$S_QTY_SISA
		];

		$this->db->distinct();
		$this->db->select($arr_select);
		$this->db->from(Purchase_Order::$TABLE_NAME);
		$this->db->join(
			Purchase_Order_Awal::$TABLE_NAME,
			Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$NOMOR_PO . "=" . Purchase_Order::$TABLE_NAME . "." . Purchase_Order::$ID
		);
		$this->db->join(
			Vendor::$TABLE_NAME,
			Vendor::$TABLE_NAME . "." . Vendor::$ID . "=" . Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$ID_VENDOR
		);
		$this->db->join(
			Material_RM_PM::$TABLE_NAME." mm",
			"mm." . Material_RM_PM::$ID . "=" . Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$ID_MATERIAL
		);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order::$TANGGAL . ' <', $until, TRUE);
		}
		//$this->db->having(Purchase_Order_Awal::$S_QTY_SISA." > 0");
		return $this->db->get();
	}
	public function get_rekap($dt, $st, $nomor_po, $id_material){
		$select=array(
			Purchase_Order_Checker_Header::$TABLE_NAME.".".Purchase_Order_Checker_Header::$ID." ".Purchase_Order_Checker_Header::$ID,
			Purchase_Order_Checker_Header::$TANGGAL,
			Purchase_Order_Checker_Header::$TABLE_NAME.".".Purchase_Order_Checker_Header::$NOMOR_PO." ". Purchase_Order_Checker_Header::$NOMOR_PO,
			Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NIK . " " . Purchase_Order_Checker_Header::$NIK,
			Material_RM_PM::$KODE_PANGGIL,
			"sum(".Purchase_Order_Checker_Detail::$TABLE_NAME.".".Purchase_Order_Checker_Detail::$QTY.") as ". Purchase_Order_Checker_Detail::$S_TOT_QTY,
			Purchase_Order_Checker_Detail::$TABLE_NAME.".".Purchase_Order_Checker_Detail::$ID_MATERIAL." ". Purchase_Order_Checker_Detail::$ID_MATERIAL,
			Vendor::$NAMA
		);
		$group_by = array(
			Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$ID,
			Purchase_Order_Checker_Header::$TANGGAL,
			Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NOMOR_PO,
			Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NIK,
			Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_MATERIAL,
			Material_RM_PM::$KODE_PANGGIL, Vendor::$NAMA
		);
		$this->db->select($select);
		$this->db->from(Purchase_Order_Checker_Header::$TABLE_NAME);
		$join = Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_HEADER . "=" . Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$ID;
		$this->db->join(Purchase_Order_Checker_Detail::$TABLE_NAME, $join);
		$join = Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_MATERIAL  . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);
		$join = Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NOMOR_PO  . "=" . Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$NOMOR_PO;
		$this->db->join(Purchase_Order_Awal::$TABLE_NAME, $join);
		$join = Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$ID_VENDOR  . "=" . Vendor::$TABLE_NAME . "." . Vendor::$ID;
		$this->db->join(Vendor::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		if($id_material != NULL){
			$this->db->where(Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_MATERIAL,$id_material);
		}
		if($nomor_po != NULL){
			$this->db->where(Purchase_Order_Checker_Header::$TABLE_NAME.".".Purchase_Order_Checker_Header::$NOMOR_PO, $nomor_po);
		}
		$this->db->where(Purchase_Order_Checker_Header::$TABLE_NAME.".".Purchase_Order_Checker_Header::$IS_DELETED,0);
		$this->db->group_by($group_by);
		//$data[H_Good_Receipt::$TABLE_NAME]=$this->get(H_Good_Receipt::ta)
		return $this->db->get();
	}
	public function get_today_info(){
		$today=date("Y-m-d");
		$until = date("Y-m-d", strtotime($today . "+ 1 day"));
		$this->db->distinct();
		$this->db->select(Purchase_Order_Checker_Header::$NOMOR_PO);
		$this->db->where(Purchase_Order_Checker_Header::$NOMOR_PO . $this::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' >=', $today, TRUE);
		$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		$this->db->where(Purchase_Order_Checker_Header::$IS_DELETED, 0);
		return $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);
	}
	public function checker_get_kinerja($dt, $st, $ws = null){
		$select = array(
			Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NIK . " " . Purchase_Order_Checker_Header::$NIK,
			Karyawan::$ID_SUB_KATEGORI,Material_RM_PM::$JENIS,
			"sum(" . Purchase_Order_Checker_Detail::$QTY . ") as " . Purchase_Order_Checker_Detail::$S_TOT_QTY,
		);
		$group_by = array(
			Purchase_Order_Checker_Header::$NIK, Karyawan::$ID_SUB_KATEGORI,
			Material_RM_PM::$JENIS,
		);
		$this->db->select($select);
		$this->db->from(Purchase_Order_Checker_Header::$TABLE_NAME);
		$join = Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_HEADER . "=" . Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$ID;
		$this->db->join(Purchase_Order_Checker_Detail::$TABLE_NAME, $join);
		$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . "=" . Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_MATERIAL;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);
		$join = Karyawan::$TABLE_NAME . "." . Karyawan::$ID . "=" . Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NIK;
		$this->db->join(Karyawan::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		if ($ws != NULL) {
			$this->db->where(Purchase_Order_Checker_Header::$NOMOR_PO, $ws);
		}
		$this->db->group_by($group_by);
		return $this->db->get();

	}
	public function get_cancel_by_tanggal($dt, $st){
		$this->db->where(Purchase_Order_Checker_Header::$IS_DELETED,0);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		return $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);
	}
	public function get_tabel_create($id_pr){

		$arr_select=[
			Material_RM_PM::$TABLE_NAME.".".Material_RM_PM::$ID." ". Material_RM_PM::$ID, 
			Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION,
			Purchase_Request_Detail::$ID_VENDOR,
			Purchase_Request_Header::$TABLE_NAME . "." . Purchase_Request_Header::$ID . " " . Purchase_Request_Header::$ID, 
			Purchase_Request_Detail::$QTY_PR, Purchase_Request_Header::$IS_DONE,
			Purchase_Request_Header::$TABLE_NAME.".".Purchase_Request_Header::$IS_DELETED." ".Purchase_Request_Header::$IS_DELETED
		];

		$this->db->select($arr_select);
		$this->db->from(Purchase_Request_Header::$TABLE_NAME);
		
		$join = Purchase_Request_Detail::$TABLE_NAME . "." . Purchase_Request_Detail::$ID_HEADER . "=" . Purchase_Request_Header::$TABLE_NAME . "." . Purchase_Request_Header::$ID;
		$this->db->join(Purchase_Request_Detail::$TABLE_NAME, $join);
		
		$join = Purchase_Request_Detail::$TABLE_NAME . "." . Purchase_Request_Detail::$ID_MATERIAL_RM_PM . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME,$join);

		$this->db->where(Purchase_Request_Header::$TABLE_NAME.".".Purchase_Request_Header::$ID, $id_pr);
		
		return $this->db->get();
	}
	public function get_all_sisa(){
		return $this->db->query("select purchase_order_awal.id_material_rm_pm,
		sum((qty_template-(COALESCE((select sum(qty)
									from purchase_order_checker_header hd,purchase_order_checker_detail
									where hd.id_purchase_order_checker_header=purchase_order_checker_detail.id_purchase_order_checker_header 
									and hd.nomor_po=purchase_order_awal.nomor_po
									group by hd.nomor_po
									),0)))) as qty_sisa
		from purchase_order,purchase_order_awal
		where purchase_order.nomor_po=purchase_order_awal.nomor_po
        group by purchase_order_awal.id_material_rm_pm  
        having qty_sisa>0");
	}
	

	public function checker_get_by_gr($nomor_gr){
		$data = [];

		/*$this->db->where(Purchase_Order::$ID, $no);
		$data[Purchase_Order::$TABLE_NAME] = $this->db->get(Purchase_Order::$TABLE_NAME);
		$this->db->where(Purchase_Order_Awal::$NOMOR_PO, $no);
		$data[Purchase_Order_Awal::$TABLE_NAME] = $this->db->get(Purchase_Order_Awal::$TABLE_NAME);
		*/

		$this->db->where(Purchase_Order_Checker_Header::$IS_DELETED, 0);
		$this->db->where(Purchase_Order_Checker_Header::$ID, $nomor_gr);
		$data[Purchase_Order_Checker_Header::$TABLE_NAME] = $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);

		/*$arr_header = array();
		$ctr = 0;
		foreach ($data[Purchase_Order_Checker_Header::$TABLE_NAME]->result_array() as $row_header) {
			$arr_header[$ctr++] = $row_header[Purchase_Order_Checker_Header::$ID];
		}
		if ($ctr != 0)
			$this->db->where_in(Purchase_Order_Checker_Detail::$ID_HEADER, $arr_header);
		*/
		$this->db->where(Purchase_Order_Checker_Detail::$ID_HEADER, $nomor_gr);
		$data[Purchase_Order_Checker_Detail::$TABLE_NAME] = $this->db->get(Purchase_Order_Checker_Detail::$TABLE_NAME);

		return $data;
	}
	public function checker_insert($ctr, $nomor_po, $nomor_sj, $tanggal, $arr_id_material, $arr_nomor_urut, $arr_qty, $arr_qty_sj){
		$data_header = [];

		$max = $this->db->query("select max(substr(id_purchase_order_checker_header,3)) as max from " . Purchase_Order_Checker_Header::$TABLE_NAME."
									where ". Purchase_Order_Checker_Header::$ID." like '". Purchase_Order::$S_CODE ."%'")->row()->max;
		if ($max == NULL)
		$max = 0;
		$max += 1;

		//Insert PO Checker Header
		$kode = Purchase_Order::$S_CODE . str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Purchase_Order_Checker_Header::$ID] = $kode;
		$data_header[Purchase_Order_Checker_Header::$TANGGAL] = date('Y-m-d', strtotime($tanggal));
		$data_header[Purchase_Order_Checker_Header::$NOMOR_SJ] = $nomor_sj;
		$data_header[Purchase_Order_Checker_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Purchase_Order_Checker_Header::$NOMOR_PO] = $nomor_po;
		$data_header[Purchase_Order_Checker_Header::$IS_DELETED] = 0;
		$this->db->insert(Purchase_Order_Checker_Header::$TABLE_NAME, $data_header);
		echo $this->alert('Kode GR: ' . $kode);

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];
			if ($arr_qty[$i] > 0) {
				$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
				$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
				//if ($temp->num_rows() > 0) {
					//Insert PO Checker Detail
					$kode = $data_header[Purchase_Order_Checker_Header::$ID] . str_pad(($i + 1), 3, "0", STR_PAD_LEFT);
					$data_detail[Purchase_Order_Checker_Detail::$ID] = $kode;
					$data_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT] = $arr_nomor_urut[$i];
					$data_detail[Purchase_Order_Checker_Detail::$QTY] = $arr_qty[$i];
					$data_detail[Purchase_Order_Checker_Detail::$QTY_SJ] = $arr_qty_sj[$i];
					$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL] = $arr_id_material[$i];
					$data_detail[Purchase_Order_Checker_Detail::$ID_HEADER] = $data_header[Purchase_Order_Checker_Header::$ID];
					$this->db->insert(Purchase_Order_Checker_Detail::$TABLE_NAME, $data_detail);

					//Insert History Stock RM PM
					$this->db->where(Purchase_Order_Awal::$NOMOR_URUT, $data_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT]);
					$this->db->where(Purchase_Order_Awal::$NOMOR_PO, $data_header[Purchase_Order_Checker_Header::$NOMOR_PO]);
					$temp_po = $this->db->get(Purchase_Order_Awal::$TABLE_NAME)->row_array();
					$id_vendor = $temp_po[Purchase_Order_Awal::$ID_VENDOR];
					$net_price_po = $temp_po[Purchase_Order_Awal::$NET_PRICE];
					//echo $net_price_po;
					

					//Insert Stock RM PM
					if ($temp->num_rows() == 1) { //hanya punya 1 vendor
						$data = [];
						$stock = $temp->row_array();
						$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
						$qty_awal = $stock[Material_RM_PM_Stock::$QTY];

						$data[Material_RM_PM_Stock::$QTY] = $qty_awal + $arr_qty[$i];
						$data[Material_RM_PM_Stock::$VALUE] = $value_awal + ($arr_qty[$i] * $net_price_po);

						$this->insert_history_stock(
							$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
							$data_header[Purchase_Order_Checker_Header::$NOMOR_SJ],
							$data_header[Purchase_Order_Checker_Header::$TANGGAL],
							$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL],
							$id_vendor,$value_awal,
							$data_detail[Purchase_Order_Checker_Detail::$QTY],
							0,$data[Material_RM_PM_Stock::$VALUE],
							Purchase_Order_Checker_Detail::GEN_INSERT_MESSAGE(
								$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
								$data_header[Purchase_Order_Checker_Header::$ID],
								$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
							)
						);

						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
						$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
					} else if ($temp->num_rows() > 0) { //punya lebih dari 1 vendor
						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
						$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $id_vendor);
						$temp_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
						if ($temp_stock->num_rows() > 0) {
							$data = [];
							$stock = $temp_stock->row_array();
							$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
							$qty_awal = $stock[Material_RM_PM_Stock::$QTY];
							$data[Material_RM_PM_Stock::$QTY] = $qty_awal + $arr_qty[$i];
							$data[Material_RM_PM_Stock::$VALUE] = $value_awal + ($arr_qty[$i] * $net_price_po);

							$this->insert_history_stock(
								$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
								$data_header[Purchase_Order_Checker_Header::$NOMOR_SJ],
								$data_header[Purchase_Order_Checker_Header::$TANGGAL],
								$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL],
								$id_vendor,
								$value_awal,
								$data_detail[Purchase_Order_Checker_Detail::$QTY],
								0,
								$data[Material_RM_PM_Stock::$VALUE],
								Purchase_Order_Checker_Detail::GEN_INSERT_MESSAGE(
									$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
									$data_header[Purchase_Order_Checker_Header::$ID],
									$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
								)
							);

							$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
							$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $id_vendor);
							$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
						}
					} else{
						//Insert Stock
						$data=[];
						$data[Material_RM_PM_Stock::$QTY] = $arr_qty[$i];
						$data[Material_RM_PM_Stock::$VALUE] = ($arr_qty[$i] * $net_price_po);
						$data[Material_RM_PM_Stock::$ID_VENDOR] = $id_vendor;
						$data[Material_RM_PM_Stock::$ID_MATERIAL] = $arr_id_material[$i];

						$this->insert_history_stock(
							$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
							$data_header[Purchase_Order_Checker_Header::$NOMOR_SJ],
							$data_header[Purchase_Order_Checker_Header::$TANGGAL],
							$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL],
							$id_vendor,
							0,
							$data_detail[Purchase_Order_Checker_Detail::$QTY],
							0,
							$data[Material_RM_PM_Stock::$VALUE],
							Purchase_Order_Checker_Detail::GEN_INSERT_MESSAGE(
								$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
								$data_header[Purchase_Order_Checker_Header::$ID],
								$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
							)
						);

						$this->db->insert(Material_RM_PM_Stock::$TABLE_NAME,$data);
					}
				//} else {
				//	return Purchase_Order_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
				//}
			}
		}
		return Purchase_Order_Checker_Detail::$MESSAGE_SUCCESS_INSERT;
	}
	
	public function awal_get_by_id_material($nomor_po, $id_material){
        $this->db->where(Purchase_Order_Awal::$NOMOR_PO, $nomor_po);
		$this->db->where(Purchase_Order_Awal::$ID_MATERIAL,$id_material);
		
		return $this->db->get(Purchase_Order_Awal::$TABLE_NAME);
		// if($po_awal->num_rows()>0){
		// 	return $po_awal->row_array()[Purchase_Order_Awal::$NOMOR_URUT];
		// }
		// return -1;
    }

	public function evaluation_get_insert($dt, $st){
		$this->db->select("distinct(" . Purchase_Order_Checker_Header::$NOMOR_SJ . ") as nomor_sj,evaluasi_qty_penerimaan");
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		$this->db->where(Purchase_Order_Checker_Header::$NOMOR_SJ . $this::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->order_by(Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN);
		$this->db->order_by(Purchase_Order_Checker_Header::$ID);
		return $this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);
	}
	public function evaluation_get_report($kategori, $dt, $st){
		if($kategori == 2){
			$arr_select = [
				Purchase_Order_Checker_Header::$TANGGAL,Purchase_Order_Checker_Header::$NOMOR_SJ,
				Vendor::$NAMA,Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN,
				Purchase_Order_Checker_Header::$EV_KUALITAS,Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN,
				Purchase_Order_Checker_Header::$EV_AFTER_SALES
				
			];
		}else{
			$arr_select = [
				Vendor::$NAMA, "SUM(".Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN . "+" . Purchase_Order_Checker_Header::$EV_KUALITAS . "+". Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN."+". Purchase_Order_Checker_Header::$EV_AFTER_SALES.") as total",
				"COUNT(*) as jumlah"
			];
		}
		$this->db->select($arr_select);
		if ($kategori == 2) 
			$this->db->distinct();

		$this->db->from(Purchase_Order_Checker_Header::$TABLE_NAME);
		$join = Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$ID . "=" . Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_HEADER;
		$this->db->join(Purchase_Order_Checker_Detail::$TABLE_NAME, $join);
		$join = Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NOMOR_PO . "=" . Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$NOMOR_PO;
		$this->db->join(Purchase_Order_Awal::$TABLE_NAME, $join);
		$join = Purchase_Order_Awal::$TABLE_NAME . "." . Purchase_Order_Awal::$ID_VENDOR . "=" . Vendor::$TABLE_NAME . "." . Vendor::$ID;
		$this->db->join(Vendor::$TABLE_NAME, $join);
		
		$this->db->where(Purchase_Order_Checker_Header::$TANGGAL_CONFIRM_ADMIN . $this::$WHERE_IS_NOT_NULL, NULL, FALSE);
		$this->db->where(Purchase_Order_Checker_Header::$NOMOR_SJ . $this::$WHERE_IS_NOT_NULL, NULL, FALSE);

		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order_Checker_Header::$TANGGAL . ' <', $until, TRUE);
		}
		if($kategori == 1){
			$this->db->group_by(Vendor::$NAMA);
		}
		return $this->db->get();

	}
	public function evaluation_get_by_sj($nomor_sj){
		$arr = [
			Purchase_Order::$TABLE_NAME . "." . Purchase_Order::$ID . " as " . Purchase_Order::$ID, Purchase_Order_Checker_Header::$NOMOR_SJ,
			Purchase_Order_Checker_Header::$EV_AFTER_SALES, Purchase_Order_Checker_Header::$EV_KUALITAS,
			Purchase_Order_Checker_Header::$TANGGAL_CONFIRM_ADMIN,
			Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$TANGGAL . " as " . Purchase_Order_Checker_Header::$TANGGAL,
			Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN, Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN,
			Purchase_Order::$TANGGAL_ESTIMASI_SAMPAI,
			Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION,
			Purchase_Order_Checker_Detail::$QTY_SJ, Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$QTY . " as " . Purchase_Order_Checker_Detail::$QTY
		];
		$this->db->select($arr);
		$this->db->from(Purchase_Order::$TABLE_NAME);
		$temp = Purchase_Order::$TABLE_NAME . "." . Purchase_Order::$ID . "=" . Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$NOMOR_PO;
		$this->db->join(Purchase_Order_Checker_Header::$TABLE_NAME, $temp);
		$temp = Purchase_Order_Checker_Header::$TABLE_NAME . "." . Purchase_Order_Checker_Header::$ID . "=" . Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_HEADER;
		$this->db->join(Purchase_Order_Checker_Detail::$TABLE_NAME, $temp);
		$temp = Purchase_Order_Checker_Detail::$TABLE_NAME . "." . Purchase_Order_Checker_Detail::$ID_MATERIAL . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $temp);
		$this->db->where(Purchase_Order::$TABLE_NAME.".". Purchase_Order::$IS_DELETED,0);
		$this->db->where(Purchase_Order_Checker_Header::$NOMOR_SJ, $nomor_sj);
		return $this->db->get();
	}
	public function evaluation_insert($nomor_sj,$tanggal, $ev_status_pengiriman, $ev_qty_penerimaan, $ev_kualitas, $ev_after_sales){
		$data=[
			Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN => $ev_status_pengiriman,
			Purchase_Order_Checker_Header::$EV_QTY_PENERIMAAN => $ev_qty_penerimaan,
			Purchase_Order_Checker_Header::$EV_KUALITAS => $ev_kualitas,
			Purchase_Order_Checker_Header::$EV_AFTER_SALES => $ev_after_sales
		];
		$this->db->set(Purchase_Order_Checker_Header::$TANGGAL_CONFIRM_ADMIN, 'NOW()', FALSE);

		$this->db->where(Purchase_Order_Checker_Header::$NOMOR_SJ, $nomor_sj);
		$this->db->update(Purchase_Order_Checker_Header::$TABLE_NAME,$data);

		//Update Tanggal Estimasi Sampai
		$this->db->where(Purchase_Order_Checker_Header::$NOMOR_SJ,$nomor_sj);
		$nomor_po=$this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME)->row_array()[Purchase_Order_Checker_Header::$NOMOR_PO];

		$data_po=[
			Purchase_Order::$TANGGAL_ESTIMASI_SAMPAI=>$tanggal
		];
		$this->db->where(Purchase_Order::$ID,$nomor_po);
		$this->db->update(Purchase_Order::$TABLE_NAME,$data_po);

		/*//Ambil Semua PO yang EV Pengiriman 4
		$this->db->where(Purchase_Order_Checker_Header::$EV_STATUS_PENGIRIMAN,4);
		$data_header=$this->db->get(Purchase_Order_Checker_Header::$TABLE_NAME);

		if($data_header->num_rows()>0){
			foreach($data_header->result_array() as $row_header){
				$tanggal_estimasi = date("Y-m-d", strtotime($row_header[Purchase_Order_Checker_Header::$TANGGAL] . "+ 1 day"));
				$temp=[
					Purchase_Order::$TANGGAL_ESTIMASI_SAMPAI => $tanggal_estimasi
				];
				$this->db->where(Purchase_Order::$ID,$row_header[Purchase_Order_Checker_Header::$NOMOR_PO]);
				$this->db->update(Purchase_Order::$TABLE_NAME,$temp);
			}
		}*/

		return Purchase_Order_Checker_Header::$MESSAGE_SUCCESS_INSERT_EVALUATION;

	}

	public function kurs_update($kurs, $tanggal_berlaku){
		$data=[
			Kurs::$VALUE => $kurs,
			Kurs::$TANGGAL_BERLAKU => $tanggal_berlaku
		];
		$this->db->insert(Kurs::$TABLE_NAME,$data);
		return Kurs::$MESSAGE_SUCCESS_INSERT;
	}
	public function kurs_get_history($dt, $st){
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Kurs::$TANGGAL_BERLAKU . ' >=', $dt, TRUE);
			$this->db->where(Kurs::$TANGGAL_BERLAKU . ' <', $until, TRUE);
		}
		return $this->db->get(Kurs::$TABLE_NAME);
	}

	public function price_insert($ctr, $arr_id_material, $arr_id_vendor, $arr_valid_from, $arr_valid_to, $arr_currency, $arr_price){
		$data=[];
		for($i=0; $i<$ctr;$i++){
			$data[Purchase_Order_Price_List::$ID_MATERIAL] = $arr_id_material[$i];
			$data[Purchase_Order_Price_List::$ID_VENDOR] = $arr_id_vendor[$i];
			$data[Purchase_Order_Price_List::$TANGGAL_MULAI] = $arr_valid_from[$i];
			$data[Purchase_Order_Price_List::$TANGGAL_AKHIR] = $arr_valid_to[$i];
			$data[Purchase_Order_Price_List::$CURRENCY] = strtoupper($arr_currency[$i]);
			$data[Purchase_Order_Price_List::$PRICE] = $arr_price[$i];

			$this->db->insert(Purchase_Order_Price_List::$TABLE_NAME,$data);
		}
		return Purchase_Order_Price_List::$MESSAGE_SUCCESS_INSERT;
	}
	public function price_get_history($df, $sf, $dt, $st, $id_vendor){
		if ($df != NULL && $sf != NULL) {
			$until = date("Y-m-d", strtotime($sf . "+ 1 day"));
			$this->db->where(Purchase_Order_Price_List::$TANGGAL_MULAI . ' >=', $df, TRUE);
			$this->db->where(Purchase_Order_Price_List::$TANGGAL_MULAI . ' <', $until, TRUE);
		}
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Order_Price_List::$TANGGAL_AKHIR . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Order_Price_List::$TANGGAL_AKHIR . ' <', $until, TRUE);
		}

		if($id_vendor != null){
			$this->db->where(Purchase_Order_Price_List::$TABLE_NAME . "." . Purchase_Order_Price_List::$ID_VENDOR, $id_vendor);

		}

		$arr_select=[
			Purchase_Order_Price_List::$TANGGAL_MULAI,Purchase_Order_Price_List::$TANGGAL_AKHIR,
			Purchase_Order_Price_List::$TABLE_NAME.".".Purchase_Order_Price_List::$CURRENCY." ".Purchase_Order_Price_List::$CURRENCY,Purchase_Order_Price_List::$PRICE,
			Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION, Purchase_Order_Price_List::$ID,
			Purchase_Order_Price_List::$TABLE_NAME . "." . Purchase_Order_Price_List::$ID_VENDOR . " " . Purchase_Order_Price_List::$ID_VENDOR,
			Vendor::$NAMA
		];

		$this->db->select($arr_select);
		$this->db->from(Purchase_Order_Price_List::$TABLE_NAME);
		$join = Purchase_Order_Price_List::$TABLE_NAME . "." . Purchase_Order_Price_List::$ID_MATERIAL . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME,$join);
		$join = Purchase_Order_Price_List::$TABLE_NAME . "." . Purchase_Order_Price_List::$ID_VENDOR . "=" . Vendor::$TABLE_NAME . "." . Vendor::$ID;
		$this->db->join(Vendor::$TABLE_NAME, $join);
		return $this->db->get();
	}
	public function price_get_active($id_vendor, $id_material, $tanggal = null){
		$arr_select=[
			Purchase_Order_Price_List::$PRICE,Purchase_Order_Price_List::$CURRENCY
		];
		$this->db->select($arr_select);
		$this->db->where(Purchase_Order_Price_List::$ID_VENDOR, $id_vendor);
		$this->db->where(Purchase_Order_Price_List::$ID_MATERIAL, $id_material);

		$dt = date("Y-m-d");
		if($tanggal == null){
			$tanggal=$dt;
		}
		$this->db->where(Purchase_Order_Price_List::$TANGGAL_MULAI . ' <=', $tanggal, TRUE);
		$this->db->where(Purchase_Order_Price_List::$TANGGAL_AKHIR . ' >=', $tanggal, TRUE);

		$this->db->order_by(Purchase_Order_Price_List::$ID, $this::$ORDER_TYPE_DESC);
		return $this->db->get(Purchase_Order_Price_List::$TABLE_NAME);
	}

	
}
