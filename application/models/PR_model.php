<?php

class Purchase_Request{
	public static $S_BUFFER_STOCK = "buffer_stock";

	public static $S_TYPE_RM = "RM";
	public static $S_TYPE_PM = "PM";

	public static $MESSAGE_FAILED_SEARCH_NOT_FOUND = "PRHQ002";
	public static $MESSAGE_FAILED_SEARCH_PO_ALREADY_MADE = "PRHQ003";
	public static $MESSAGE_FAILED_SEARCH_IS_DELETED = "PRHQ005";
}

class Purchase_Request_Header{
	public static $TABLE_NAME = "purchase_request_header";
	public static $ID = "id_purchase_request_header";

	public static $TANGGAL = "tanggal_generate";
	public static $IS_DONE = "is_done";
	public static $IS_DELETED = "is_deleted";

	//Foreign Key
	public static $NIK = "nik";

	public static $MESSAGE_SUCCESS_INSERT = "PRHQ001";
	public static $MESSAGE_SUCCESS_CHANGE_STATUS = "PRHQ004";
	

}

class Purchase_Request_Detail{
	public static $TABLE_NAME = "purchase_request_detail";
	public static $ID = "id_purchase_request_detail";

	public static $QTY_PR = "qty_pr";

	//Foreign Key
	public static $ID_MATERIAL_RM_PM = "id_material_rm_pm";
	public static $ID_VENDOR = "id_vendor";
	public static $ID_HEADER = "id_purchase_request_header";

}

class PR_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function get_tabel_awal($tipe = null){
		$bulan_akhir=0;
		$bulan_awal=0;
		//ambil bulan awal
		$this->db->select(Worksheet_Checker_Header::$TANGGAL);
		$this->db->where("YEAR(" . Worksheet_Checker_Header::$TANGGAL . ")", date("Y") - 1);
		$this->db->order_by("1",
			Library_Model::$ORDER_TYPE_ASC
		);
		$data_awal= $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);
		if($data_awal->num_rows()>0){
			$tanggal_awal = $data_awal->row_array()[Worksheet_Checker_Header::$TANGGAL];
			$bulan_awal = intval(date("m", strtotime($tanggal_awal)));
		}

		//ambil bulan akhir
		$this->db->select(Worksheet_Checker_Header::$TANGGAL);
		$this->db->where("YEAR(" . Worksheet_Checker_Header::$TANGGAL . ")", date("Y") - 1);
		$this->db->order_by("1",
			Library_Model::$ORDER_TYPE_DESC
		);
		$data_akhir = $this->db->get(Worksheet_Checker_Header::$TABLE_NAME);
		if ($data_akhir->num_rows() > 0) {
			$tanggal_akhir = $data_akhir->row_array()[Worksheet_Checker_Header::$TANGGAL];
			$bulan_akhir = intval(date("m",strtotime($tanggal_akhir)));
		}
		$select="";
		if($bulan_awal==0 && $bulan_akhir==0){ //Tidak ada Transaksi Tahun Lalu
			$select="SUM(" . Worksheet_Checker_Detail::$QTY . ")/" . ($bulan_akhir - $bulan_awal);
		}else{
			$select="0";
		}
		$arr_select=[
			Material_RM_PM::$TABLE_NAME.".".Material_RM_PM::$ID." ". Material_RM_PM::$ID, Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION, $select." as ". Purchase_Request::$S_BUFFER_STOCK,
			Worksheet_Checker_Detail::$ID_VENDOR
		];

		$this->db->select($arr_select);
		$this->db->from(Worksheet_Checker_Detail::$TABLE_NAME);
		$join = Worksheet_Checker_Detail::$TABLE_NAME . "." . Worksheet_Checker_Detail::$ID_MATERIAL . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME,$join);

		/*$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . "=" . Material_RM_PM_Stock::$TABLE_NAME . "." . Material_RM_PM_Stock::$ID_MATERIAL;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);*/

		if($tipe!=null){
			$this->db->where(Material_RM_PM::$TIPE,strtoupper($tipe));
		}
		$arr_group_by = [
			Material_RM_PM::$ID, Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION,Worksheet_Checker_Detail::$ID_VENDOR
		];
		$this->db->group_by($arr_group_by);
		
		return $this->db->get();
	}
	public function get_history($dt, $st){
		$arr_select=[
			Purchase_Request_Header::$TABLE_NAME . "." . Purchase_Request_Header::$ID . " " . Purchase_Request_Header::$ID,
			Material_RM_PM::$DESCRIPTION, Material_RM_PM::$KODE_MATERIAL, Purchase_Request_Header::$TANGGAL, Purchase_Request_Detail::$QTY_PR,
			Purchase_Request_Detail::$ID_VENDOR, Purchase_Request_Header::$IS_DONE
		];

		$this->db->select($arr_select);
		$this->db->from(Purchase_Request_Header::$TABLE_NAME);

		$join = Purchase_Request_Header::$TABLE_NAME . "." . Purchase_Request_Header::$ID . "=" . Purchase_Request_Detail::$TABLE_NAME . "." . Purchase_Request_Detail::$ID_HEADER;
		$this->db->join(Purchase_Request_Detail::$TABLE_NAME, $join);

		$join = Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . "=" . Purchase_Request_Detail::$TABLE_NAME . "." . Purchase_Request_Detail::$ID_MATERIAL_RM_PM;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);

		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Request_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Request_Header::$TANGGAL . ' <', $until, TRUE);
		}

		return $this->db->get();
	}
	public function get_utuh($dt, $st){
		$select=[
			Purchase_Request_Header::$ID,
			Purchase_Request_Header::$TANGGAL,
			Purchase_Request_Header::$IS_DELETED
		];
		$this->db->select($select);
		$this->db->from(Purchase_Request_Header::$TABLE_NAME);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Purchase_Request_Header::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Purchase_Request_Header::$TANGGAL . ' <', $until, TRUE);
		}
		$this->db->where(Purchase_Request_Header::$IS_DONE, 0);

		return $this->db->get();

	}

	public function insert($ctr, $arr_id_material, $arr_id_vendor, $arr_qty_pr){
		//Ambil Kode Maks
		$this->db->select("max(substr(" . Purchase_Request_Header::$ID . ",2)) as " . Purchase_Request_Header::$ID);
		$qry_pr = $this->db->get(Purchase_Request_Header::$TABLE_NAME)->row_array();
		$max = intval($qry_pr[Purchase_Request_Header::$ID]);
		if ($max == null) $max = 0;
		$max += 1;

		
		//Header
		$kode = "1" . str_pad($max, 7, "0", STR_PAD_LEFT);
		$arr = [];
		$arr[Purchase_Request_Header::$ID] = $kode;
		$arr[Purchase_Request_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$arr[Purchase_Request_Header::$IS_DELETED] = 0;
		$arr[Purchase_Request_Header::$IS_DONE] = 0;

		
		$this->db->set(Purchase_Request_Header::$TANGGAL, 'NOW()', FALSE);
		$this->db->insert(Purchase_Request_Header::$TABLE_NAME, $arr);

		echo "<script>alert('Kode PR: " . $kode . "');</script>";
		
		//Detail
		$ctr_pr_detail = 1;
		for ($i = 0; $i < $ctr; $i++) {
			$arr_detail = [];
			$arr_detail[Purchase_Request_Detail::$ID]= $kode . str_pad($ctr_pr_detail++, "3", "0", STR_PAD_LEFT);
			$arr_detail[Purchase_Request_Detail::$ID_MATERIAL_RM_PM] = $arr_id_material[$i];
			if($arr_id_vendor[$i]!=null){
				$arr_detail[Purchase_Request_Detail::$ID_VENDOR] = $arr_id_vendor[$i];
			}
			$arr_detail[Purchase_Request_Detail::$QTY_PR]=$arr_qty_pr[$i];
			$arr_detail[Purchase_Request_Detail::$ID_HEADER]=$arr[Purchase_Request_Header::$ID];
			$this->db->insert(Purchase_Request_Detail::$TABLE_NAME,$arr_detail);
		}

		return Purchase_Request_Header::$MESSAGE_SUCCESS_INSERT;
	}
	public function change_status($nomor_pr, $is_deleted){
		$this->db->where(Purchase_Request_Header::$ID, $nomor_pr);
		$data = [];
		$data[Purchase_Request_Header::$IS_DELETED] = $is_deleted;
		$this->db->update(Purchase_Request_Header::$TABLE_NAME,$data);

		return Purchase_Request_Header::$MESSAGE_SUCCESS_CHANGE_STATUS;
	}

	
}
