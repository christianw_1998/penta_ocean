<?php
class Kebersihan_Checker_Area{
	public static $TABLE_NAME = "kebersihan_checker_area";
	public static $ID = "id_kebersihan_checker_area";

	public static $GUDANG = "gudang";

	//Foreign Key
	public static $NIK = "nik";
}
class Kebersihan_Checker_Gudang{
	public static $TABLE_NAME = "kebersihan_checker_gudang";
	public static $ID = "id_kebersihan_checker_gudang";

	public static $GUDANG = "gudang";

	//Foreign Key
	public static $NIK = "nik";

}
class Kebersihan_Gudang{
	public static $TABLE_NAME = "kebersihan_gudang";
	public static $ID = "id_kebersihan_gudang";

	public static $GUDANG = "gudang";
	public static $RAK = "rak";
}
class Kebersihan_Harian{
	public static $TABLE_NAME = "kebersihan_harian";
	public static $ID = "id_kebersihan_harian";

	public static $TANGGAL = "tanggal";
	public static $KONDISI = "kondisi";

	//Foreign Key
	public static $NIK = "nik";
	public static $GUDANG = "gudang";

	public static $MESSAGE_SUCCESS_INSERT = "KEHQ001";

}
class Kebersihan_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function harian_insert($ctr, $arr_id_gudang, $arr_kondisi){
		$today=date("Y-m-d");
		$data = [];
		for($i=0;$i<$ctr;$i++){
			$this->db->where(Kebersihan_Harian::$TANGGAL,$today);
			$this->db->where(Kebersihan_Harian::$GUDANG,$arr_id_gudang[$i]);
			$query_kebersihan=$this->db->get(Kebersihan_Harian::$TABLE_NAME);
			if($query_kebersihan->num_rows()>0){ //Update
				foreach($query_kebersihan->result_array() as $row_kebersihan){
					$data[Kebersihan_Harian::$KONDISI] = $arr_kondisi[$i];
					$data[Kebersihan_Harian::$GUDANG] = $arr_id_gudang[$i];
					$this->db->where(Kebersihan_Harian::$ID, $row_kebersihan[Kebersihan_Harian::$ID]);
					$this->db->update(Kebersihan_Harian::$TABLE_NAME, $data);
				}
			}else{ //Insert
				$query_checker = $this->get(Kebersihan_Checker_Gudang::$TABLE_NAME, Kebersihan_Checker_Gudang::$GUDANG, $arr_id_gudang[$i], null, null, false);
				if($query_checker->num_rows()>0){
					foreach($query_checker->result_array() as $row_checker){
						$data = [];
						$data[Kebersihan_Harian::$KONDISI] = $arr_kondisi[$i];
						$this->db->set(Kebersihan_Harian::$TANGGAL, 'NOW()', FALSE);
						$data[Kebersihan_Harian::$GUDANG] = $arr_id_gudang[$i];
						$data[Kebersihan_Harian::$NIK] = $row_checker[Kebersihan_Checker_Gudang::$NIK];
						$this->db->insert(Kebersihan_Harian::$TABLE_NAME, $data);
					}
				}

			}
		}
		return Kebersihan_Harian::$MESSAGE_SUCCESS_INSERT;
	}

	public function harian_rmpm_get_by($dt, $st){
		$arr_select=array(
			Kebersihan_Harian::$KONDISI,
			Kebersihan_Harian::$TANGGAL,
			Kebersihan_Gudang::$TABLE_NAME.".".Kebersihan_Gudang::$GUDANG." ".Kebersihan_Gudang::$GUDANG,
			Kebersihan_Gudang::$RAK,
			Kebersihan_Harian::$NIK

		);
		$this->db->select($arr_select);
		$this->db->from(Kebersihan_Harian::$TABLE_NAME);
		$this->db->join(Kebersihan_Gudang::$TABLE_NAME,
		Kebersihan_Harian::$TABLE_NAME.".".Kebersihan_Harian::$GUDANG."=".Kebersihan_Gudang::$TABLE_NAME.".".Kebersihan_Gudang::$ID);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Kebersihan_Harian::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Kebersihan_Harian::$TANGGAL . ' <', $until, TRUE);
		}
		$this->db->where(Kebersihan_Gudang::$TABLE_NAME.".".Kebersihan_Gudang::$GUDANG,"A2")->or_where(Kebersihan_Gudang::$TABLE_NAME . "." . Kebersihan_Harian::$GUDANG,"A3");
		return $this->db->get();
	}

	public function rm_pm_get_kinerja($dt, $st){
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			return $this->db->query('SELECT nik,count(*) as total_bersih FROM kebersihan_harian,kebersihan_gudang
				where kebersihan_harian.gudang=kebersihan_gudang.id_kebersihan_gudang AND
				kebersihan_harian.tanggal >= "' . $dt . '" and kebersihan_harian.tanggal<"' . $until . '" AND
				kebersihan_harian.kondisi=1 and (kebersihan_gudang.gudang="A3" or kebersihan_gudang.gudang="A2")
				group by nik');
		}
		return $this->db->query('SELECT nik,count(*) as total_bersih FROM kebersihan_harian,kebersihan_gudang
				where kebersihan_harian.gudang=kebersihan_gudang.id_kebersihan_gudang AND
				kebersihan_harian.kondisi=1 and (kebersihan_gudang.gudang="A3" or kebersihan_gudang.gudang="A2")
				group by nik');
	}

	public function get_report($dt, $st){
		$arr_select=[
			Kebersihan_Harian::$TANGGAL, Kebersihan_Gudang::$TABLE_NAME.".".Kebersihan_Gudang::$GUDANG." ". Kebersihan_Gudang::$GUDANG, Kebersihan_Gudang::$RAK,
			Kebersihan_Harian::$TABLE_NAME.".". Kebersihan_Harian::$NIK." ". Kebersihan_Harian::$NIK,
			Karyawan::$NAMA, Kebersihan_Harian::$KONDISI
		];
		$this->db->select($arr_select);
		$this->db->from(Kebersihan_Harian::$TABLE_NAME);
		$join=Kebersihan_Harian::$TABLE_NAME.".". Kebersihan_Harian::$GUDANG."=". Kebersihan_Gudang::$TABLE_NAME.".". Kebersihan_Gudang::$ID;
		$this->db->join(Kebersihan_Gudang::$TABLE_NAME,$join);
		$join=Kebersihan_Harian::$TABLE_NAME.".". Kebersihan_Harian::$NIK."=". Karyawan::$TABLE_NAME.".". Karyawan::$ID;
		$this->db->join(Karyawan::$TABLE_NAME,$join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Kebersihan_Harian::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Kebersihan_Harian::$TANGGAL . ' <', $until, TRUE);
		}

		return $this->db->get();
	}
}
