<?php


class Standar_Insentif{
	public static $TABLE_NAME="standar_insentif";
	public static $ID= "id_standar_insentif";
	public static $NAMA="nama";
	public static $NOMINAL="nominal";

	//Foreign Key
	public static $ID_KATEGORI="id_kategori";
	
	public static $MESSAGE_SUCCESS_UPDATE="SIQ001";
	public static $MESSAGE_FAILED_UPDATE="SIQ002";
	public static $MESSAGE_FAILED_INSERT_UPDATE_INSENTIF_NULL="SIQ003";
	public static $MESSAGE_FAILED_INSERT_UPDATE_INSENTIF_NEGATIVE="SIQ004";
}
class Karyawan_Insentif{
	public static $TABLE_NAME="karyawan_insentif";
	public static $ID= "id_karyawan_insentif";
	public static $KETERANGAN="keterangan";
	public static $NOMINAL="nominal";

	//Foreign Key
	public static $ID_KARYAWAN="nik";
	public static $ID_STANDAR_INSENTIF="id_standar_insentif";

	public static $MESSAGE_FAILED_UPDATE_OUT_OF_BOUND="KIQ003";
	public static $MESSAGE_FAILED_UPDATE_ALREADY_UPDATED="KIQ002";
	public static $MESSAGE_SUCCESS_UPDATE="KIQ001";

}
class Standar_Jam_kerja{
	public static $TABLE_NAME="standar_jam_kerja";
	public static $ID= "id_standar_jam_kerja";
	public static $JAM_MASUK="jam_masuk";
	public static $JAM_KELUAR="jam_keluar";

	//Foreign Key
	public static $ID_KATEGORI="id_kategori";
	public static $ID_SUB_KATEGORI="id_sub_kategori";
}

class List_Access{
	public static $TABLE_NAME="list_access";
	public static $ID="id_access";
	public static $ACCESS="access";
	public static $DESCRIPTION="description";
}
class Surat_Jalan{
	public static $MESSAGE_SUCCESS_UPDATE = 'SJAQ001';
	public static $MESSAGE_FAILED_UPDATE_OLD_SJ_NOT_FOUND = 'SJAQ002';
	public static $MESSAGE_FAILED_UPDATE_NEW_SJ_ALREADY_EXIST = 'SJAQ003';

	public static $KEYWORD_SO = 'SO';
	public static $KEYWORD_STO = 'STO';
	public static $KEYWORD_SIRCLO = 'SIRCLO';
	public static $KEYWORD_RETUR = 'RETUR';
	
	public static function GEN_UPDATE_MESSAGE($params,$params2)
	{
		return "(Direvisi menjadi $params oleh NIK: $params2)";
	}
}
class Ekspedisi{
	public static $TABLE_NAME = "ekspedisi";
	public static $ID = "id_ekspedisi";
	public static $NAMA_EKSPEDISI = "nama_ekspedisi";
	public static $INISIAL = "inisial";
	public static $TUJUAN = "tujuan";
	public static $IS_TYPE_LCL = "is_type_lcl";
	public static $IS_TYPE_CONTAINER = "is_type_container";
	public static $IS_TYPE_SIRCLO = "is_type_sirclo";
	public static $IS_SCALE_M3 = "is_scale_m3";
	public static $IS_SCALE_KG = "is_scale_kg";
	public static $IS_SCALE_COLLIE = "is_scale_collie";

	public static $MESSAGE_SUCCESS_UPLOAD="EKSQ001";
}
class model extends CI_Model {

	public static $IS_DELETED="is_deleted";

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}
	public function get($table,$field = null,$value = null,$order_by = null,$ascending = null,$has_is_deleted=null){
		if($has_is_deleted == null)
			$this->db->where($this::$IS_DELETED,0);
		if($field != null)
			$this->db->where("upper(".$field.")", strtoupper($value));
		if($order_by != null){
			if ($ascending != null)
				$this->db->order_by($order_by, $ascending);
			else
				$this->db->order_by($order_by);
		}
		return $this->db->get($table);
	}

	public function cek_login($u,$p){
		$this->db->from(Karyawan::$TABLE_NAME)
					->where("upper(".Karyawan::$USERNAME.")",strtoupper($u))
					->where(Karyawan::$PASSWORD,md5($p))
					->where($this::$IS_DELETED,0)
					->where(Karyawan::$ID_ROLE." is NULL", NULL, FALSE);
		$qry=$this->db->get();
		if($qry->num_rows()>0){
			return Karyawan::$MESSAGE_FOUND_ROLE_NOT_FOUND; 
		}
	
		$this->db->from(Karyawan::$TABLE_NAME)
					->where("upper(".Karyawan::$USERNAME.")",strtoupper($u))
					->where(Karyawan::$PASSWORD,md5($p))
					->where($this::$IS_DELETED,0);
		$qry=$this->db->get();
		if($qry->num_rows()>0){
			return Karyawan::$MESSAGE_FOUND; 
		}
		
		$this->db->from(Karyawan::$TABLE_NAME)
					->where("upper(".Karyawan::$USERNAME.")",strtoupper($u))
					->where(Karyawan::$PASSWORD,md5($p))
					->where($this::$IS_DELETED,1);
		$qry=$this->db->get();
		if($qry->num_rows()>0){
			return Karyawan::$MESSAGE_FOUND_IS_DELETED; 
		}
		
		return Karyawan::$MESSAGE_NOT_FOUND;
	}
	
	//Kalender
	public function insert_kalender($t,$k){
		$data_query=array();
		$data_query[Kalender::$KETERANGAN]=$k;
		if($this->equals($t,""))
			return Kalender::$MESSAGE_FAILED_INSERT_UPDATE_DATE_NULL;
		if ($this->equals($k, ""))
			return Kalender::$MESSAGE_FAILED_INSERT_UPDATE_KETERANGAN_NULL;
		
		$this->db->where(Kalender::$TANGGAL,$t)
					->where($this::$IS_DELETED,0);
		$qry=$this->db->get(Kalender::$TABLE_NAME);			
		if($qry->num_rows()>0){
			$this->db->where(Kalender::$TANGGAL,$t);
			$this->db->update(Kalender::$TABLE_NAME,$data_query);
			return Kalender::$MESSAGE_SUCCESS_UPDATE;
		}
		else{
			$data_query[Kalender::$TANGGAL]=$t;
			$this->db->insert(Kalender::$TABLE_NAME,$data_query);
			return Kalender::$MESSAGE_SUCCESS_INSERT;
		}
		
	}
	public function delete_kalender($i){
		$data_query=array();
		$data_query[$this::$IS_DELETED]=1;
		$this->db->where(Kalender::$ID,$i)
					->update(Kalender::$TABLE_NAME,$data_query);
		if($this->db->affected_rows()>0)
			return Kalender::$MESSAGE_SUCCESS_DELETE;
		return Kalender::$MESSAGE_FAILED_DELETE;
	}

	//Cutoff
	public function insert_cutoff($tf,$tt){
		$data_query=array();
		$data_query[Cutoff::$TANGGAL_DARI]=$tf;
		$data_query[Cutoff::$TANGGAL_SAMPAI]=$tt;

		if($this->equals($tf,"") || $this->equals($tt,""))
			return Cutoff::$MESSAGE_FAILED_INSERT_UPDATE_DATE_NULL;
		if($tf>$tt)
			return Cutoff::$MESSAGE_FAILED_INSERT_UPDATE_DATE_FROM_LATER;
		$month = intval(date("m",strtotime($tt)))-1;
		$year = date("Y",strtotime($tt));
		$data_query[Cutoff::$KETERANGAN]=strtoupper(ARR_BULAN_NO_SEMUA[$month]." ".$year);
		$temp=$data_query[Cutoff::$KETERANGAN];

		$this->db->where("upper(".Cutoff::$KETERANGAN.")",$temp)
					->where($this::$IS_DELETED,0);
		$qry=$this->db->get(Cutoff::$TABLE_NAME);
		if($qry->num_rows()>0){
			$this->db->where(Cutoff::$ID,$qry->row()->id_cut_off);
			$this->db->update(Cutoff::$TABLE_NAME,$data_query);
			return Cutoff::$MESSAGE_SUCCESS_UPDATE;
		}else{
			$this->db->insert(Cutoff::$TABLE_NAME,$data_query);
			return Cutoff::$MESSAGE_SUCCESS_INSERT;
		}
	}
	public function delete_cutoff($i){
		$data_query[$this::$IS_DELETED]=1;
		$this->db->where(Cutoff::$ID,$i);
		$this->db->update(Cutoff::$TABLE_NAME,$data_query);
		if($this->db->affected_rows()>0)
			return Cutoff::$MESSAGE_SUCCESS_DELETE;
		return Cutoff::$MESSAGE_FAILED_DELETE;
		
	}
	public function get_cutoff_by_keterangan($keterangan){
		$keterangan=strtoupper($keterangan);
		$this->db->like(Cutoff::$KETERANGAN, $keterangan, 'both');
		$this->db->where($this::$IS_DELETED,0);
		return $this->db->get(Cutoff::$TABLE_NAME); 
	}
	
	//Standar Jam Kerja
	public function get_str_jam_kerja($kid,$sid){
		if($sid==NULL){
			$this->db->where($this::$IS_DELETED,0)
						->where(Standar_Jam_kerja::$ID_KATEGORI,$kid)
						->where(Standar_Jam_kerja::$ID_SUB_KATEGORI." is NULL", NULL, FALSE);
			$qry=$this->db->get(Standar_Jam_kerja::$TABLE_NAME);
		}else{
			$this->db->where($this::$IS_DELETED,0)
						->where(Standar_Jam_kerja::$ID_KATEGORI,$kid)
						->where(Standar_Jam_kerja::$ID_SUB_KATEGORI,$sid);
			$qry=$this->db->get(Standar_Jam_kerja::$TABLE_NAME);
		if($qry->num_rows()==0)
			$this->db->where($this::$IS_DELETED,0)
						->where(Standar_Jam_kerja::$ID_KATEGORI,$kid)
						->where(Standar_Jam_kerja::$ID_SUB_KATEGORI." is NULL", NULL, FALSE);
			$qry=$this->db->get(Standar_Jam_kerja::$TABLE_NAME);
		}
		return $qry;
	}
	
	//User Access
	public function get_access_by_user_id($id,$type){
		$this->db->from(User_Access::$TABLE_NAME);
		$this->db->join(List_Access::$TABLE_NAME,"user_access.id_access=list_access.id_access");
		$this->db->where("upper(".List_Access::$ACCESS.")",strtoupper($type));
		$this->db->where(User_Access::$NIK,$id);
		$qry=$this->db->get();
		//$qry=$this->db->query("select * from user_access,list_access
		//						where user_access.id_access=list_access.id_access and upper(list_access.access)='".strtoupper($type)."' and nik=".$id);
		if($qry->num_rows()>0){
			foreach($qry->result_array() as $row){
				if($row[User_Access::$IS_TRUE]==1)
					return true;
			}
		}
		return false;
	}
	public function get_all_user_access(){
		$data=[];
		if(isset($_SESSION[SESSION_KARYAWAN_PENTA])){
			$nik=$_SESSION[SESSION_KARYAWAN_PENTA]['nik'];
			$data['status']=1;
			$data['data']=$this->db->query("select user_access.nik nik, access, user_access.id_access id_access,description,is_true,nama
											from user_access,list_access,karyawan
											where user_access.id_access=list_access.id_access and karyawan.nik=user_access.nik and
											list_access.is_deleted=0 and user_access.nik_atasan='$nik' and user_access.is_deleted=0");
		}else{
			$data['status']=0;
			$data['data']=User_Access::$MESSAGE_FAILED_GET_NO_SESSION;
		}		
		return $data;
	}
	public function get_all_user_access_no_restriction(){
		$data=[];
		//if(isset($_SESSION[SESSION_KARYAWAN_PENTA])){
			$data['status']=1;
			$data['data']=$this->db->query("select user_access.nik nik_bawahan,user_access.nik_atasan nik_atasan,user_access.is_deleted is_deleted, access, user_access.id_access id_access,description
											from user_access,list_access
											where user_access.id_access=list_access.id_access");
		//}else{
			//$data['status']=0;
			//$data['data']=User_Access::$MESSAGE_FAILED_GET_NO_SESSION;
		//}		
		return $data;
	}
	public function insert_user_access($nik,$id_access,$is_true){
		$this->db->where(User_Access::$NIK,$nik);
		$this->db->where(User_Access::$ID_ACCESS,$id_access);
		$qry=$this->db->get(User_Access::$TABLE_NAME);
		$data_query=[];
		$data_query[User_Access::$IS_TRUE]=$is_true;
		if($qry->num_rows()>0){ //update
			$this->db->where(User_Access::$NIK,$nik);
			$this->db->where(User_Access::$ID_ACCESS,$id_access);
			$this->db->update(User_Access::$TABLE_NAME,$data_query);
			return User_Access::$MESSAGE_SUCCESS_UPDATE;
		}else{ //insert
			$data_query[User_Access::$NIK]=$nik;
			$data_query[User_Access::$ID_ACCESS]=$id_access;
			$this->db->insert(User_Access::$TABLE_NAME,$data_query);
			return User_Access::$MESSAGE_SUCCESS_INSERT;
		}
		return User_Access::$MESSAGE_FAILED_UPDATE;
	}
	public function insert_user_access_no_restriction($nik_bawahan,$nik_atasan,$id_access,$tipe){
		$this->db->where(User_Access::$NIK,$nik_bawahan);
		$this->db->where(User_Access::$ID_ACCESS,$id_access);
		$this->db->where(User_Access::$NIK_ATASAN,$nik_atasan);
		$qry=$this->db->get(User_Access::$TABLE_NAME);
		$data_query=[];
		
		if($qry->num_rows()>0){ //update
			if($this->equals($tipe,User_Access::$WHILE_UPDATE)){
				if($qry->row()->is_deleted==0)
					$data_query[$this::$IS_DELETED]=1;
				else if($qry->row()->is_deleted==1)
					$data_query[$this::$IS_DELETED]=0;
			}else
				$data_query[$this::$IS_DELETED]=0;
			
			$this->db->where(User_Access::$NIK,$nik_bawahan);
			$this->db->where(User_Access::$ID_ACCESS,$id_access);
			$this->db->where(User_Access::$NIK_ATASAN,$nik_atasan);
			$this->db->update(User_Access::$TABLE_NAME,$data_query);
			return User_Access::$MESSAGE_SUCCESS_UPDATE;
		}else{ //insert
			$data_query[$this::$IS_DELETED]=0;
			$data_query[User_Access::$IS_TRUE]=0;
			$data_query[User_Access::$NIK]=$nik_bawahan;
			$data_query[User_Access::$ID_ACCESS]=$id_access;
			$data_query[User_Access::$NIK_ATASAN]=$nik_atasan;
			$this->db->insert(User_Access::$TABLE_NAME,$data_query);
			return User_Access::$MESSAGE_SUCCESS_INSERT;
		}
		return User_Access::$MESSAGE_FAILED_UPDATE;
	}

	//Karyawan
	public function get_karyawan_by($field,$value){
		$qry=$this->db->query("select tunjangan_obat,role.id_role r_id,kategori_karyawan.id_kategori kat_id,karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,role.nama r_nama,saldo_cuti,is_outsource,karyawan.id_sub_kategori id_sub_kategori
								from karyawan, role, kategori_karyawan,sub_kategori
								where upper($field)='$value' and sub_kategori.id_kategori=kategori_karyawan.id_kategori and karyawan.id_role=role.id_role and karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								union
								select tunjangan_obat,id_role,kategori_karyawan.id_kategori kat_id,karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,'TIDAK ADA' r_nama,saldo_cuti,is_outsource,karyawan.id_sub_kategori
								from karyawan, kategori_karyawan,sub_kategori
								where karyawan.id_kategori=kategori_karyawan.id_kategori and sub_kategori.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0 and id_role is null
								and upper($field)='$value'
								union
								select tunjangan_obat,id_role,NULL,karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,'TIDAK ADA' tipe, 'TIDAK ADA' r_nama, saldo_cuti,is_outsource,NULL
								from karyawan
								where upper($field)='$value' and karyawan.id_kategori is null and karyawan.is_deleted=0");
		return $qry;
	}	
	public function insert_karyawan($ni,$na,$np,$g,$t,$c,$r,$k,$io,$sk){
		$data_query=array();
		$data_query[Karyawan::$IS_OUTSOURCE]=$io;
		$data_query[Karyawan::$NAMA]=strtoupper($na);
		$data_query[Karyawan::$NPWP]=$np;
		$data_query[Karyawan::$GAJI_POKOK]=$g;
		if($ni=="")
			return Karyawan::$MESSAGE_FAILED_INSERT_UPDATE_NIK_EMPTY;
		if($this->contains_number($na)||preg_match('/[\'^£$%&*()}{@#~?><>,.|=_+¬-]/', $na))
			return Karyawan::$MESSAGE_FAILED_INSERT_UPDATE_NAME_CONTAINS_NUMBER_SYMBOL;
		if(preg_match('/[\'^£$%&*()}{@#~?><>,.|=_+¬-]/', $ni))
			return Karyawan::$MESSAGE_FAILED_INSERT_UPDATE_NIK_CONTAINTS_NOT_NUMBER_SYMBOL;

		if($g==-1)
			$data_query[Karyawan::$GAJI_POKOK]=NULL;
		
		$data_query[Karyawan::$TUNJANGAN_JABATAN]=$t;
		if($t==-1)
			$data_query[Karyawan::$TUNJANGAN_JABATAN]=NULL;

		//$data_query['saldo_cuti']=$c;
		
		$data_query[$this::$IS_DELETED]=0;
		
		$data_query[Karyawan::$ID_ROLE]=$r;
		if($r==-1)
			$data_query[Karyawan::$ID_ROLE]=NULL;

		$data_query[Karyawan::$ID_KATEGORI]=$k;
		if($k==-1)
			$data_query[Karyawan::$ID_KATEGORI]=NULL;

		$data_query[Karyawan::$ID_SUB_KATEGORI]=$sk;
		if($sk==-1)
		$data_query[Karyawan::$ID_SUB_KATEGORI]=NULL;

		$qry=$this->get_karyawan_by(Karyawan::$ID,$ni);
		if($qry->num_rows()>0){
			$this->db->where(Karyawan::$ID,$ni);
			$this->db->update(Karyawan::$TABLE_NAME,$data_query);
			return Karyawan::$MESSAGE_SUCCESS_UPDATE;
		}else{
			$data_query[Karyawan::$ID]=$ni;
			$data_query[Karyawan::$SALDO_CUTI]=0;
			$this->db->insert(Karyawan::$TABLE_NAME,$data_query);
			return Karyawan::$MESSAGE_SUCCESS_INSERT;
		}
	}
	public function get_all_karyawan(){
		return $this->db->query("select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,role.nama r_nama,saldo_cuti,is_outsource
								 from karyawan, role, kategori_karyawan
								 where karyawan.id_role=role.id_role and karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								 union
								 select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,tipe,'TIDAK ADA' r_nama,saldo_cuti,is_outsource
								 from karyawan, kategori_karyawan
								 where karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								 and karyawan.nik not in (
									select nik
									from karyawan, role, kategori_karyawan
									where karyawan.id_role=role.id_role and karyawan.id_kategori=kategori_karyawan.id_kategori and karyawan.is_deleted=0
								 )
								 union
								 select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,'TIDAK ADA' tipe, role.nama r_nama, saldo_cuti,is_outsource
								 from karyawan,role
								 where karyawan.id_kategori is null and karyawan.id_role=role.id_role and karyawan.is_deleted=0
								 union
								 select karyawan.nama k_nama,nik,gaji_pokok,npwp,tunjangan_jabatan,'TIDAK ADA' tipe, 'TIDAK ADA' r_nama, saldo_cuti,is_outsource
								 from karyawan
								 where karyawan.id_kategori is NULL and karyawan.id_role is NULL
								 order by nik");
	}
	public function karyawan_update_batch($data){
		if(isset($data)){
			//upload karyawan kepala
			$is_change=false;
			if(count($data['header'][2])>7){
				for($i=3;$i<3+count($data['values']);$i++){
					if(isset($data['values'][$i])){
						if($data['values'][$i]["A"]!=""){ //jika nik tidak kosong
							$nik=isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
							$nama=isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : "";
							$npwp=isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : "";
							$role=isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
							$kategori=isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;

							$temp=$this->get(Role::$TABLE_NAME,Role::$NAMA,$role);
							$id_role=NULL;
							if($temp!=NULL)$id_role=$temp->row()->id_role;

							$temp2=$this->get(Kategori_Karyawan::$TABLE_NAME,Kategori_Karyawan::$TIPE,$kategori);
							$id_kategori=NULL;
							if($temp2!=NULL)$id_kategori=$temp2->row()->id_kategori;
							$gp=isset($data['values'][$i]["F"]) ? str_replace(".","",$data['values'][$i]["F"]) : NULL;
							$tunj=isset($data['values'][$i]["G"]) ? str_replace(".","",$data['values'][$i]["G"]) : NULL;
							$s_cuti=isset($data['values'][$i]["H"]) ? $data['values'][$i]["H"] : NULL;

							$data_query=array(
								Karyawan::$NAMA=>$nama,
								Karyawan::$NPWP=>$npwp
							);
							if(isset($id_role))$data_query[Karyawan::$ID_ROLE]=$id_role;
							else $data_query[Karyawan::$ID_ROLE]=NULL;
							if(isset($id_kategori))$data_query[Karyawan::$ID_KATEGORI]=$id_kategori;
							else $data_query[Karyawan::$ID_KATEGORI]=NULL;
							if(isset($gp))$data_query[Karyawan::$GAJI_POKOK]=$gp;
							else $data_query[Karyawan::$GAJI_POKOK]=NULL;
							if(isset($tunj))$data_query[Karyawan::$TUNJANGAN_JABATAN]=$tunj;
							else $data_query[Karyawan::$TUNJANGAN_JABATAN]=NULL;
							if(isset($s_cuti))$data_query[Karyawan::$SALDO_CUTI]=$s_cuti;
							else $data_query[Karyawan::$SALDO_CUTI]=0;

							if($this->cek_nik($nik)){ //jika sudah ada nik, update
								$is_change=true;
								$this->db->where(Karyawan::$ID,$nik);
								$this->db->update(Karyawan::$TABLE_NAME,$data_query);
									
							}else{
								$data_query[Karyawan::$IS_OUTSOURCE]=0;
								$data_query[Karyawan::$ID]=$nik;
								$this->db->insert(Karyawan::$TABLE_NAME,$data_query);
								if($this->db->affected_rows()>0)
									$is_change=true;
							}
						}else
							break;
					}
				}
				if($is_change)
					return Karyawan::$MESSAGE_UPLOAD_SUCCESS;
				return Karyawan::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
			}
			else if(count($data['header'][2])==6){
				for($i=3;$i<3+count($data['values']);$i++){
					if(isset($data['values'][$i])){
						if($data['values'][$i]["A"]!=""){ //jika nik tidak kosong
							$nik=isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
							$nama=isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
							$npwp=isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
							//$s_cuti=isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
							$data_query=array(
								Karyawan::$NAMA=>$nama,
								Karyawan::$NPWP=>$npwp
							);
							//if(isset($s_cuti))$data_query["saldo_cuti"]=$s_cuti;
							//else $data_query["saldo_cuti"]=0;

							if($this->cek_nik($nik)){ //jika sudah ada nik, update
								$is_change=true;
								$this->db->where(Karyawan::$ID,$nik);
								$this->db->update(Karyawan::$TABLE_NAME,$data_query);
									
							}else{
								$data_query[Karyawan::$ID]=$nik;
								$this->db->insert(Karyawan::$TABLE_NAME,$data_query);
								if($this->db->affected_rows()>0)
									$is_change=true;
							}
						}
					}
				}
				if($is_change)
					return Karyawan::$MESSAGE_UPLOAD_SUCCESS;
				return Karyawan::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
			}
		}
		return Karyawan::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
	}
	public function cek_nik($nik){
		$this->db->where(Karyawan::$ID,$nik);
		$qry=$this->db->get(Karyawan::$TABLE_NAME);
		if($qry->num_rows()>0)
			return true;
		return false;
	}
	public function karyawan_get_colour_matcher(){
		$this->db->where(Karyawan::$ID_SUB_KATEGORI,15); 
		$this->db->where($this::$IS_DELETED,0);
		return $this->db->get(Karyawan::$TABLE_NAME);
	}

	//Absensi
	public function karyawan_absensi_update_batch($data){
		if(isset($data['header'][5])){
			if(count($data['header'][5])==6){
				for($i=6;$i<6+count($data['values']);$i++){
					if(isset($data['values'][$i])){
						if($data['values'][$i]["A"]!=""){ //jika nik tidak kosong
							$nik=isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
							$tgl=isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
							$j_masuk=isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
							$j_keluar=isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
							$ket=isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
							$tanggal=strtotime($tgl);
							$tgl_simpan_db=date('Y-m-d',$tanggal);
							$data_query=array();
							if(isset($ket))$data_query[Absensi::$KETERANGAN]=$ket;
							else $data_query[Absensi::$KETERANGAN]=NULL;
							$data_query[Absensi::$JAM_MASUK]=$j_masuk;
							$data_query[Absensi::$JAM_KELUAR]=$j_keluar;
							if($this->available_absensi($nik,$tgl_simpan_db)){ //jika sudah ada nik, di tanggal itu, update
								
								$keterangan=$this->get_absensi_by_nik_tanggal($nik,$tgl_simpan_db)->row()->keterangan;

								if($this->str_contains($keterangan,LEGEND_TUKAR_HARI))
									$this->db->query("update ".Karyawan::$TABLE_NAME." set saldo_cuti=saldo_cuti-1 where nik='$nik'");

								if($this->str_contains($keterangan,LEGEND_CUTI))
									$this->db->query("update ".Karyawan::$TABLE_NAME." set saldo_cuti=saldo_cuti+1 where nik='$nik'");
								
								$this->db->where(Absensi::$NIK,$nik);
								$this->db->where(Absensi::$TANGGAL,$tgl_simpan_db);
								$this->db->update(Absensi::$TABLE_NAME,$data_query);
							}else{
								$data_query[Absensi::$NIK]=$nik;
								$data_query[Absensi::$TANGGAL]=$tgl_simpan_db;
								if($this->get_karyawan_by("nik",$nik)->num_rows()>0)
									$this->db->insert(Absensi::$TABLE_NAME,$data_query);
							}
							if($this->str_contains($data_query[Absensi::$KETERANGAN],LEGEND_TUKAR_HARI)){
								$this->db->query("update ".Karyawan::$TABLE_NAME." set saldo_cuti=saldo_cuti+1 where nik='$nik'");
							}
							if($this->str_contains($data_query[Absensi::$KETERANGAN],LEGEND_CUTI)){
								$this->db->query("update ".Karyawan::$TABLE_NAME." set saldo_cuti=saldo_cuti-1 where nik='$nik'");
							}
						}else
							break;
					}
				}
			}else
				return Absensi::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
		}else
			return Absensi::$MESSAGE_UPLOAD_FAILED_NOT_TEMPLATE;
		return Absensi::$MESSAGE_UPLOAD_SUCCESS;
		
	}
	public function available_absensi($nik,$t){
		$this->db->where(Absensi::$NIK,$nik)
					->where(Absensi::$TANGGAL,$t);
		$qry=$this->db->get(Absensi::$TABLE_NAME);
		if($qry->num_rows()>0) return true;
		return false;
	}
	public function get_absensi_by_nik_tanggal($nik,$t){
		$this->db->where(Absensi::$NIK,$nik)
					->where(Absensi::$TANGGAL,$t);
		$qry=$this->db->get(Absensi::$TABLE_NAME);
		if($qry->num_rows()>0)return $qry;
		return NULL;
	}
	
	public function get_absensi_where($where){
		return $this->db->query("select karyawan.nik nik,nama,tanggal,jam_masuk,jam_keluar,keterangan 
								from absensi,karyawan
								where karyawan.nik=absensi.nik and $where");
	}

	//Standar Insentif
	public function insert_s_insentif($st,$si){
		if($st=="" || $si=="")
			return Standar_Insentif::$MESSAGE_FAILED_INSERT_UPDATE_INSENTIF_NULL;
		if($st<0 || $si<0)
			return Standar_Insentif::$MESSAGE_FAILED_INSERT_UPDATE_INSENTIF_NEGATIVE;
		$data=array();
		$data[Standar_Insentif::$NOMINAL]=$st;
		$this->db->where('UPPER('.Standar_Insentif::$NAMA.')',strtoupper(S_INSENTIF_T_KEHADIRAN));
		$this->db->update(Standar_Insentif::$TABLE_NAME,$data);

		$data[Standar_Insentif::$NOMINAL]=$si;
		$this->db->where('UPPER('.Standar_Insentif::$NAMA.')',strtoupper(S_INSENTIF_I_LUAR_KOTA));
		$this->db->update(Standar_Insentif::$TABLE_NAME,$data);

		return Standar_Insentif::$MESSAGE_SUCCESS_UPDATE;

	}	

	//Material
	public function material_update_batch($data){
		$kal="";
		for($i=0;$i<count($data['values'])+10;$i++){
			if(isset($data['values'][$i])){
				if($data['values'][$i]["A"]!=""){ //jika material no tidak kosong
					$no=isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$mat_grp=isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$p_code=isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$desc=isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					$mat_type=isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$packing=isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$box=isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;
					$net=isset($data['values'][$i]["H"]) ? $data['values'][$i]["H"] : NULL;
					$gross=isset($data['values'][$i]["I"]) ? $data['values'][$i]["I"] : NULL;
					$kubik=isset($data['values'][$i]["J"]) ? $data['values'][$i]["J"] : NULL;
					$kode_brg=isset($data['values'][$i]["K"]) ? $data['values'][$i]["K"] : NULL;
					$kemasan=isset($data['values'][$i]["L"]) ? $data['values'][$i]["L"] : NULL;
					$warna=isset($data['values'][$i]["M"]) ? $data['values'][$i]["M"] : NULL;
					
					$data_query=array(
						Material::$MAT_GROUP=>$mat_grp,
						Material::$PRODUCT_CODE=>$p_code,
						Material::$DESCRIPTION=>$desc,
						Material::$MAT_TYPE=>$mat_type,
						Material::$PACKING=>$packing,
						Material::$BOX=>$box,
						Material::$NET=>str_replace(',','.',$net),
						Material::$GROSS=>str_replace(',','.',$gross),
						Material::$KUBIK=>str_replace(',','.',$kubik),
						Material::$KODE_BARANG=>$kode_brg,
						Material::$KEMASAN=>$kemasan,
						Material::$WARNA=>$warna,
						$this::$IS_DELETED=>0,
					);
					
					
					//INSERT MATERIAL
					$this->db->where(Material::$ID,$no);
					$qry=$this->db->get(Material::$TABLE_NAME);
					if($qry->num_rows()==0){
						$data_query['id_material']=$no;
						$this->db->insert(Material::$TABLE_NAME,$data_query);	
					}else{
						$kal.=$no." Update!<br>";
						$this->db->where(Material::$ID,$no);
						$this->db->update(Material::$TABLE_NAME,$data_query);
					}

					//Check material yang belum masuk
					//$kal.="$no";
					$this->db->where(Material::$ID,$no);
					$qry=$this->db->get(Material::$TABLE_NAME);
					if($qry->num_rows()==0){
						$kal.=$no." tidak ada<br>";
					}else{
						
					}
					
				}else
					break;
			}
		}
		return $kal;
	}
	public function material_stock_update_batch($data){
		//kosongkan semua stock
		$this->db->empty_table(Fifo_Stock_Gudang::$TABLE_NAME);
		
		for ($i = 0; $i < count($data['values']) + 100; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$id_material = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$qty = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;

					$this->db->where(Material::$ID,$id_material);
					$qry = $this->db->get(Material::$TABLE_NAME);
					
					if($qry->num_rows()>0){
						$material=$qry->row();
						$id_material = $material->id_material;
						$box = $material->box;

						$qry_gudang = $this->get(Gudang::$TABLE_NAME,Gudang::$ID_MATERIAL, $id_material);
						$stock_in = $qty;
						if ($qry_gudang->num_rows() > 0) {
							foreach ($qry_gudang->result() as $row) {
								if ($stock_in <= 0) {
									break;
								} else {
									$kapasitas_total = $row->kapasitas;
									//if ($box>0) { //jika tidak 0 berarti kapasitas masih box, harus dijadikan tin
										$kapasitas_total *= $box;
									//}
									$kapasitas_terisi = $this->get_stock_by($id_material, $row->id_gudang);
									$kapasitas_sisa = $kapasitas_total - $kapasitas_terisi;

									if ($kapasitas_sisa > 0) {
										$total_tin = 0;
										if ($kapasitas_sisa - $stock_in >= 0) //jika kapasitas masih mencukupi
											$total_tin = $stock_in;
										else
											$total_tin = $kapasitas_sisa;

										$stock_in -= $total_tin;
										
										
										$id_gudang_loop = $row->id_gudang;
										//Fifo Stock Gudang
										//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
										$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
										$this->db->order_by(Fifo_Stock_Gudang::$ID, "desc");

										$qry_fifo = $this->db->get(fifo_stock_gudang::$TABLE_NAME);
										$id_gudang = "";

										if ($qry_fifo->num_rows() > 0) {
											$qry_fifo = $qry_fifo->row();
											$id_gudang = $qry_fifo->id_gudang;
										}
										$arr_fifo = [];
										if ($id_gudang != $id_gudang_loop) {
											$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $id_material;
											$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
											$arr_fifo[Fifo_Stock_Gudang::$QTY] = $total_tin;
											$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
										} else {
											$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo->id_fifo);
											$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo->qty + $total_tin;
											$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
										}
									}
								}
							}
							if ($stock_in > 0) {
								//Fifo Stock Gudang
								//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
								$this->db->where(Gudang::$RAK, Gudang::$S_CADANGAN);
								$this->db->where(Gudang::$NAMA_GUDANG, $qry_gudang->row()->nama_gudang);
								$qry_temp_gudang = $this->db->get(Gudang::$TABLE_NAME);
								$id_gudang_loop = $qry_temp_gudang->row()->id_gudang;

								$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
								$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG, $id_gudang_loop);
								$qry_fifo = $this->db->get(fifo_stock_gudang::$TABLE_NAME);

								$arr_fifo = [];
								if ($qry_fifo->num_rows() == 0) {
									$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL] = $id_material;
									$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG] = $id_gudang_loop;
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $stock_in;
									$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								} else {
									$this->db->where(Fifo_Stock_Gudang::$ID, $qry_fifo->row()->id_fifo);
									$arr_fifo[Fifo_Stock_Gudang::$QTY] = $qry_fifo->row()->qty + $stock_in;
									$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);
								}
							}
						} else { //cek material yang tidak ada gudang
							echo $id_material . "- GUDANG TIDAK ADA<br>";
						}
					}else{ //cek material yang tidak ada
						echo $id_material."- MATERIAL TIDAK ADA<br>";
					}
				} else
					break;
			}
		}
		return Fifo_Stock_Gudang::$MESSAGE_SUCCESS_UPLOAD;
	}
	public function material_stock_opname_tahunan_update_batch($data){
		for ($i = 0; $i < count($data['values']) + 100; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$id_material = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$program = $this->get_stock_by($id_material);
					$fisik = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;

					$this->db->where(Material::$ID,$id_material);
					$qry = $this->db->get(Material::$TABLE_NAME);
					if($qry->num_rows()>0){
						$material=$qry->row();
						$id_material = $material->id_material;
						$box = $material->box;
						
						if(($fisik != NULL || $fisik >= 0)){ //program tidak perlu dicek karena sudah di cek pada method get_stock_by
							$fisik=intval($fisik);
							$program=intval($program);
							$varian=$fisik-$program;

							//Insert Material Fisik
							$arr_material_fisik=[];
							//$this->db->where(Material_Fisik::$ID_MATERIAL,$id_material);
							$arr_material_fisik[Material_Fisik::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA]['nik'];
							$arr_material_fisik[Material_Fisik::$QTY_FISIK] = $fisik;
							$arr_material_fisik[Material_Fisik::$QTY_PROGRAM] = $this->get_stock_by($id_material);
							$this->db->set(Material_Fisik::$TANGGAL, 'NOW()', FALSE);
							$arr_material_fisik[Material_Fisik::$ID_MATERIAL] = $id_material;
							$this->db->insert(Material_Fisik::$TABLE_NAME,$arr_material_fisik);

							if($fisik>$program||$varian>0){ //Barang Masuk
								$qry_gudang=$this->get(Gudang::$TABLE_NAME,Gudang::$ID_MATERIAL,$id_material);
								$stock_in=abs(intval($varian));
								if($qry_gudang->num_rows()>0){
									foreach($qry_gudang->result() as $row){
										if($stock_in<=0){
											break;
										}else{
											$kapasitas_total=$row->kapasitas;
											//if($box>0){ //jika tidak 0 berarti kapasitas masih box, harus dijadikan tin
												$kapasitas_total*=$box;
											//}
											$kapasitas_terisi=$this->get_stock_by($id_material,$row->id_gudang);
											$kapasitas_sisa=$kapasitas_total-$kapasitas_terisi;

											if($kapasitas_sisa>0){
												$total_tin=0;
												if($kapasitas_sisa-$stock_in>=0) //jika kapasitas masih mencukupi
													$total_tin=$stock_in;
												else
													$total_tin=$kapasitas_sisa;

												$stock_in-=$total_tin;
												$id_gudang_loop=$row->id_gudang;

												//Penambahan Stock Gudang
												$this->insert_stock_gudang($id_material,$id_gudang_loop,$total_tin, 0, Stock_Gudang::GEN_CALIBRATION_MESSAGE());
											
												//Fifo Stock Gudang
												//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
												$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL,$id_material);
												$this->db->order_by(Fifo_Stock_Gudang::$ID,"desc");

												$qry_fifo=$this->db->get(fifo_stock_gudang::$TABLE_NAME);
												$id_gudang="";

												if($qry_fifo->num_rows()>0){
													$qry_fifo=$qry_fifo->row();
													$id_gudang=$qry_fifo->id_gudang;
												}
												$arr_fifo=[];
												if($id_gudang!=$id_gudang_loop){
													$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL]=$id_material;
													$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG]=$id_gudang_loop;
													$arr_fifo[Fifo_Stock_Gudang::$QTY]=$total_tin;
													$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
												}else{
													$this->db->where(Fifo_Stock_Gudang::$ID,$qry_fifo->id_fifo);
													$arr_fifo[Fifo_Stock_Gudang::$QTY]=$qry_fifo->qty+$total_tin;
													$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
												}
												
												
											}
										}
									}
									if($stock_in>0){
										$this->db->where(Gudang::$RAK,Gudang::$S_CADANGAN);
										$this->db->where(Gudang::$NAMA_GUDANG,$qry_gudang->row()->nama_gudang);
										$qry_temp_gudang=$this->db->get(Gudang::$TABLE_NAME);
										$id_gudang_loop=$qry_temp_gudang->row()->id_gudang;

										//Penambahan Stock Gudang
										$this->insert_stock_gudang($id_material, $id_gudang_loop, $total_tin, 0, Stock_Gudang::GEN_CALIBRATION_MESSAGE());
											
										//Fifo Stock Gudang
										//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
										$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL,$id_material);
										$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG,$id_gudang_loop);
										$qry_fifo=$this->db->get(fifo_stock_gudang::$TABLE_NAME);

										$arr_fifo=[];
										if($qry_fifo->num_rows()==0){
											$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL]=$id_material;
											$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG]=$id_gudang_loop;
											$arr_fifo[Fifo_Stock_Gudang::$QTY]=$stock_in;
											$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
										}else{
											$this->db->where(Fifo_Stock_Gudang::$ID,$qry_fifo->row()->id_fifo);
											$arr_fifo[Fifo_Stock_Gudang::$QTY]=$qry_fifo->row()->qty+$stock_in;
											$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
										}
									}
								}
							}
							else if($fisik<$program||$varian<0){ //Barang Keluar
								$stock_out=abs(intval($varian));
								$mode="";
								do {
									$this->db->from(Fifo_Stock_Gudang::$TABLE_NAME);
									if (!$this->equals($mode, Gudang::$S_CADANGAN)) {
										$this->db->select("fifo_stock_gudang.id_gudang id_gudang,id_fifo,qty");
										$this->db->join(Gudang::$TABLE_NAME, "fifo_stock_gudang.id_gudang=gudang.id_gudang");
										$this->db->where(Gudang::$RAK . " !=", Gudang::$S_CADANGAN);
									}
									$this->db->where(Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
									$this->db->where(Fifo_Stock_Gudang::$QTY . " !=", 0);

									$qry = $this->db->get();
									foreach($qry->result() as $row){
										$this->db->set(Stock_Gudang::$TANGGAL, 'NOW()', FALSE);
										$selisih=0;
										if($row->qty-$stock_out>=0){ //jika stock di gudang masih cukup
											$selisih=$stock_out;
											$stock_out=0;
										}else{
											$selisih=$row->qty;
											$stock_out-=$selisih;
										}

										//Pengurangan Stock Gudang
										$this->insert_stock_gudang($id_material,$row->id_gudang,0,$selisih, Stock_Gudang::GEN_CALIBRATION_MESSAGE());
									
										//Fifo Stock Gudang
										$arr_fifo=[];
										$arr_fifo[Fifo_Stock_Gudang::$QTY]=$row->qty-$selisih;
										$this->db->where(Fifo_Stock_Gudang::$ID,$row->id_fifo);
										$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
										
										if($stock_out<=0){
											break;
										}
									}
									if ($stock_out > 0)
										$mode = Gudang::$S_CADANGAN;
								}while($stock_out>0);
								//hapus semua fifo yang qty 0
								$this->db->where(Fifo_Stock_Gudang::$QTY,0);
								$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

								//Pengecekan stock 0 pada gudang untuk pemindahan material
								$qry_gudang = $this->db->query("select fifo_stock_gudang.id_material id_material,gudang.id_gudang id_gudang,qty,id_fifo from gudang,fifo_stock_gudang where gudang.rak='CADANGAN' and qty>0 and gudang.id_gudang=fifo_stock_gudang.id_gudang");

								if($qry_gudang->num_rows()>0){
									foreach($qry_gudang->result() as $row){ 
										$stock_now=$row->qty;
										$id_material=$row->id_material;
										$qry_fifo=$this->db->query("select id_gudang,nama_gudang,id_material,kapasitas,rak,kolom,tingkat from gudang where gudang.id_gudang not in (select id_gudang from fifo_stock_gudang) and gudang.id_material=".$id_material);
										$this->db->where(Material::$ID,$row->id_material);
										$material=$this->db->get(Material::$TABLE_NAME)->row();
										$box=$material->box;
										if($qry_fifo->num_rows()>0){
											foreach($qry_fifo->result() as $row2){
												$kapasitas=$row2->kapasitas;
												
												//if($box!=0){
													$kapasitas*=$box;
												//}
												$stock_move=0;
												if($kapasitas-$stock_now>=0){ //jika kapasitas masih cukup
													$stock_move=$stock_now;
												}else{
													$stock_move=$kapasitas;
												}

												//Pemindahan Barang dari Cadangan
												$gudang_to = $row2->nama_gudang . ' Rak ' . $row2->rak . $row2->kolom . ' ' . $row2->tingkat;
												$this->insert_stock_gudang($id_material,$row->id_gudang,0,$stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

												//Pemindahan Barang ke Rak
												$this->insert_stock_gudang($id_material, $row2->id_gudang, $stock_move, 0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());

												$stock_now-=$stock_move;
												$arr_fifo_move_from=[];
												$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY]=$stock_now;
												$this->db->where(Fifo_Stock_Gudang::$ID,$row->id_fifo);
												$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo_move_from);

												$arr_fifo_move_to=[];
												$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL]=$row2->id_material;
												$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row2->id_gudang;
												$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
												$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo_move_to);

											}
										}
									}
								}
							}
						}
					}
				} else
					break;
			}
		}
		return Fifo_Stock_Gudang::$MESSAGE_SUCCESS_UPLOAD_STOCK_OPNAME;
	}
	public function delete_material($i){
		$data_query=array();
		$data_query[$this::$IS_DELETED]=1;
		$this->db->where(Material::$ID,$i);
		$this->db->update(Material::$TABLE_NAME,$data_query);
		if($this->db->affected_rows()>0)
			return Material::$MESSAGE_SUCCESS_DELETE;
		return Material::$MESSAGE_FAILED_DELETE;
			
	}
	public function get_material($kode_brg = NULL, $kemasan = NULL, $warna = NULL){
		if($kode_brg!=NULL)
			$this->db->where("UPPER(".Material::$KODE_BARANG.")",strtoupper($kode_brg));
		if($kemasan!=NULL)
			$this->db->where("UPPER(".Material::$KEMASAN.")",strtoupper($kemasan));
		if($warna!=NULL)
			$this->db->where("UPPER(".Material::$WARNA.")",strtoupper($warna));
		$this->db->where($this::$IS_DELETED,0);
		return $this->db->get(Material::$TABLE_NAME);
	}
	/*public function get_material_by($field,$value){
		$this->db->where($field,$value);
		$this->db->where($this::$IS_DELETED,0);
		return $this->db->get(Material::$TABLE_NAME);
	}*/
	/*public function get_all_material_order_desc(){
		$this->db->where($this::$IS_DELETED,0);
		$this->db->order_by(Material::$DESCRIPTION);
		return $this->db->get(Material::$TABLE_NAME);
	}*/
	public function get_material_distinct($tipe,$kb,$k){
		$this->db->select("distinct($tipe) as value");
		$this->db->where($this::$IS_DELETED,0);
		$this->db->where("$tipe is NOT NULL",NULL,FALSE);
		if($kb!="")
			$this->db->where(Material::$KODE_BARANG,$kb);
		if($k!="")
			$this->db->where(Material::$KEMASAN,$k);
		$this->db->order_by("1");
		return $this->db->get(Material::$TABLE_NAME);
	}
	public function get_all_material_stock($with_fisik = NULL){
		$this->db->where($this::$IS_DELETED,0);
		$qry=$this->db->get(Material::$TABLE_NAME);
		$data=[];
		$ctr=0;
		foreach($qry->result_array() as $row){
			$id_material=$row[Material::$ID];
			$stock=$this->get_stock_by($id_material);
			if(($with_fisik!=NULL) || ($with_fisik==NULL && $stock!=0)){
				$data[$ctr][Material::$ID]=$id_material;
				$data[$ctr][Material::$PRODUCT_CODE]=$row[Material::$PRODUCT_CODE];
				$data[$ctr][Material::$DESCRIPTION]=$row[Material::$DESCRIPTION];
				$data[$ctr][Fifo_Stock_Gudang::$QTY]=$stock;

				$stock_fisik=0;
				$this->db->where(Material_Fisik::$ID_MATERIAL,$id_material);
				$this->db->order_by(Material_Fisik::$ID,"DESC");
				$qry_material_fisik=$this->db->get(Material_Fisik::$TABLE_NAME);
				if($qry_material_fisik->num_rows()>0){
					$stock_fisik=$qry_material_fisik->row_array()[Material_Fisik::$QTY_FISIK];
				}

				$qry_gudang=$this->get(Gudang::$TABLE_NAME,Gudang::$ID_MATERIAL,$id_material);
				$data[$ctr]["GUDANG"]="";
				if($qry_gudang->num_rows()>0){
					foreach($qry_gudang->result_array() as $row2){
						$data[$ctr]["GUDANG"].=$row2[Gudang::$NAMA_GUDANG].'-R '.$row2[Gudang::$RAK].$row2[Gudang::$KOLOM].' '.$row2[Gudang::$TINGKAT].'<br>';
					}
				}else
					$data[$ctr]["GUDANG"]='-';
				
				$data[$ctr][Material_Fisik::$QTY_FISIK]=$stock_fisik;

				$ctr++;
			}
		}
		return $data;
	}
	public function material_get_by_product_code_like($p_code){
		$warna=explode(".",$p_code)[1]; //5200.00175 -> ambil yang 00175
		$this->db->like(Material::$PRODUCT_CODE, $p_code, 'after');
		$this->db->where(Material::$WARNA, $warna);
		return $this->db->get(Material::$TABLE_NAME);
	}

	//Material FG SAP
	public function material_fg_sap_update_batch($data){
		$kal = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$no = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					// $desc = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					// $p_memo = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					// $packing = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					// $net = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					// $gross = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					// $box = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;
					// $mat_grp = isset($data['values'][$i]["H"]) ? $data['values'][$i]["H"] : NULL;
					$new_fg = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;

					//Temporary
					//UPDATE MATERIAL FG SAP

					//Cek apakah FG ada pada Master FG
					$this->db->where(Material::$ID, $new_fg);
					if($this->db->get(Material::$TABLE_NAME)->num_rows()>0){
						$this->db->where(Material_FG_SAP::$ID, $no);
						$data_query = array(
							Material_FG_SAP::$ID_SAGE => $new_fg
						);
						$this->db->update(Material_FG_SAP::$TABLE_NAME, $data_query);
					}else{
						$kal .= $no ."->Tidak ada<br>";
					}

					/*
					$data_query = array(
						Material_FG_SAP::$MAT_GROUP => $mat_grp,
						Material_FG_SAP::$PRODUCT_MEMO => $p_memo,
						Material_FG_SAP::$DESCRIPTION => $desc,
						Material_FG_SAP::$PACKING => $packing,
						Material_FG_SAP::$BOX => $box,
						Material_FG_SAP::$NET => str_replace(',', '.', $net),
						Material_FG_SAP::$GROSS => str_replace(',', '.', $gross),
						$this::$IS_DELETED => 0,
					);

					//INSERT MATERIAL FG SAP
					if (!$this->str_contains(Material_FG_SAP::$DESCRIPTION, "X-")) {
						$this->db->where(Material_FG_SAP::$ID, $no);
						$qry = $this->db->get(Material_FG_SAP::$TABLE_NAME);
						if ($qry->num_rows() == 0) {
							$data_query[Material_FG_SAP::$ID] = $no;
							$this->db->insert(Material_FG_SAP::$TABLE_NAME, $data_query);
						} else {
							$kal .= $no . " Update!<br>";
							$this->db->where(Material_FG_SAP::$ID, $no);
							$this->db->update(Material_FG_SAP::$TABLE_NAME, $data_query);
						}

						//Check material yang belum masuk
						//$kal.="$no";
						$this->db->where(Material_FG_SAP::$ID, $no);
						$qry = $this->db->get(Material_FG_SAP::$TABLE_NAME);
						if ($qry->num_rows() == 0) {
							$kal .= $no . " tidak ada<br>";
						} else {
						}
					}
					*/
				} else
					break;
			}
		}
		echo $kal;
		return $kal;
	}

	//Material RM PM
	/*public function material_rm_pm_update_batch($data){
		$kal = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$no = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$jenis = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$tipe = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$is_konsinyasi = isset($data['values'][$i]["D"]) ? 1 : 0;
					$description = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$unit = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$box = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;
					

					$data_query = array(
						Material_RM_PM::$JENIS => $jenis,
						Material_RM_PM::$TIPE => $tipe,
						Material_RM_PM::$IS_KONSINYASI => $is_konsinyasi,
						Material_RM_PM::$DESCRIPTION => $description,
						Material_RM_PM::$UNIT => $unit,
						Material_RM_PM::$BOX => $box,
						$this::$IS_DELETED => 0,
					);


					//INSERT MATERIAL RM PM
					//$this->db->where(Material_RM_PM::$ID, $no);
					//$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
					//if ($qry->num_rows() == 0) {
						$data_query[Material_RM_PM::$KODE_MATERIAL] = $no;
						$this->db->insert(Material_RM_PM::$TABLE_NAME, $data_query);
					/*} else {
						$kal .= $no . " Update!<br>";
						$this->db->where(Material_RM_PM::$ID, $no);
						$this->db->update(Material_RM_PM::$TABLE_NAME, $data_query);
					}

					//Check material RM PM yang belum masuk
					//$kal.="$no";
					$this->db->where(Material_RM_PM::$ID, $no);
					$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
					if ($qry->num_rows() == 0) {
						$kal .= $no . " tidak ada<br>";
					} else {
					}
				} else
					break;
			}
		}
		return $kal;
	}
	public function material_rm_pm_stock_update_batch($data){
		$kal = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$kode_material = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$konsinyasi = isset($data['values'][$i]["B"]) ? 1 : NULL;
					$id_vendor = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$stock = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$value = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;

					$data_query = array(
						Material_RM_PM_Stock::$QTY => $stock,
						Material_RM_PM_Stock::$VALUE => $value
					);

					$id_material=null;
					$this->db->where(Material_RM_PM::$KODE_MATERIAL,$kode_material);
					if($konsinyasi)
						$this->db->where(Material_RM_PM::$IS_KONSINYASI,1);
					$qry=$this->db->get(Material_RM_PM::$TABLE_NAME);
					if($qry->num_rows()>0)
						$id_material=$qry->row_array()[Material_RM_PM::$ID];

					if($id_material!=null){
						//$this->insert_history_stock_rm_pm(null, date('d M Y'), $id_material, $id_vendor, $stock, 0, Stock_Gudang_RM_PM::GEN_UPLOAD_MESSAGE());

						//Cek apakah Stock stock sudah ada atau belum
						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL,$id_material);

						if ($id_vendor != null)
							$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $id_vendor);

						$qry=$this->db->get(Material_RM_PM_Stock::$TABLE_NAME);

						if ($id_vendor != null)
							$data_query[Material_RM_PM_Stock::$ID_VENDOR] = $id_vendor;

						//Insert History Stock
						/*$this->insert_history_stock_rm_pm(
							"",
							"",
							date("Y-m-d"),
							$id_material,
							($id_vendor) != null ? $id_vendor : NULL,
							$stock,
							0,
							Material_RM_PM_Stock::GEN_UPLOAD_MESSAGE()
						);
						
						if ($qry->num_rows() > 0) {
							//UPDATE MATERIAL RM PM Stock
							$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $id_material);
							$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data_query);
						}else{
							//INSERT MATERIAL RM PM Stock
							$data_query[Material_RM_PM_Stock::$ID_MATERIAL] = $id_material;
							$this->db->insert(Material_RM_PM_Stock::$TABLE_NAME, $data_query);
						}
						
					}else{
						$kal.=$kode_material." - Material Tidak ada<br>";
					}
				} else
					break;
			}
		}
		return Material_RM_PM_Stock::$MESSAGE_SUCCESS_UPLOAD;
	}*/
	/*public function get_all_material_rm_pm_stock($with_fisik = NULL){
		/*$this->db->where($this::$IS_DELETED,0);
		$qry=$this->db->get(Material_RM_PM::$TABLE_NAME);
		$data=[];
		$ctr=0;
		foreach($qry->result_array() as $row){
			$id_material=$row[Material_RM_PM::$ID];
			$stock=$this->get_stock_rm_pm_by($id_material);
			if(($with_fisik!=NULL) || ($with_fisik==NULL && $stock!=0)){
				$data[$ctr][Material_RM_PM::$ID]=$row[Material_RM_PM::$KODE_MATERIAL];
				$data[$ctr][Material_RM_PM::$DESCRIPTION]=$row[Material_RM_PM::$DESCRIPTION];
				$data[$ctr][Material_RM_PM_Stock::$QTY]=$stock;
				$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL,$id_material);
				$temp=$this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
				$value=0;
				if($temp->num_rows()>0){
					$value=$temp->row_array()[Material_RM_PM_Stock::$VALUE];
				}
				$data[$ctr][Material_RM_PM_Stock::$VALUE]=$value;
				$data[$ctr][Material_RM_PM_Stock::$S_NET_PRICE]=$value/$stock;

				$stock_fisik=0;
				$this->db->where(Material_Fisik::$ID_MATERIAL,$id_material);
				$this->db->order_by(Material_Fisik::$ID,"DESC");
				$qry_material_fisik=$this->db->get(Material_Fisik::$TABLE_NAME);
				if($qry_material_fisik->num_rows()>0){
					$stock_fisik=$qry_material_fisik->row_array()[Material_Fisik::$QTY_FISIK];
				}
				
				$data[$ctr][Material_Fisik::$QTY_FISIK]=$stock_fisik;

				$ctr++;
			}
		}
		if($with_fisik==null){
			$select=[
				Material_RM_PM::$KODE_MATERIAL,Material_RM_PM::$DESCRIPTION,Material_RM_PM_Stock::$QTY,
				Material_RM_PM_Stock::$VALUE,Material_RM_PM_Stock::$ID_VENDOR
			];
			$this->db->select($select);
			$this->db->from(Material_RM_PM_Stock::$TABLE_NAME);
			$this->db->join(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$TABLE_NAME.".".Material_RM_PM::$ID."=".
							Material_RM_PM_Stock::$TABLE_NAME.".".Material_RM_PM_Stock::$ID_MATERIAL);
			
			$this->db->where(Material_RM_PM::$TABLE_NAME.".".$this::$IS_DELETED,0);
			$data=$this->db->get();
			return $data;
		}
		
	}*/
	public function insert_good_issue_rm_pm_mutasi($ctr, $keterangan, $arr_id_material, $arr_qty, $arr_vendor){
		$data_header = [];
		$max = "";

		$max = $this->db->query("select max(substr(id_worksheet_checker_header,8)) as max from " . Worksheet_Checker_Header::$TABLE_NAME. "
									where id_worksheet_checker_header like 'ADJ%'")->row()->max;
		if ($max == NULL)
			$max = 0;
		$max += 1;

		//Insert WS Checker Header
		$kode = Worksheet::$S_MUT_CODE . "GI" . str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Worksheet_Checker_Header::$ID] = $kode;
		$data_header[Worksheet_Checker_Header::$TANGGAL] = date('Y-m-d');
		$data_header[Worksheet_Checker_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Worksheet_Checker_Header::$NOMOR_WS] = null;
		$data_header[$this::$IS_DELETED] = 0;
		$this->db->insert(Worksheet_Checker_Header::$TABLE_NAME, $data_header);
		echo "<script>alert('Kode GI: " . $kode . "');</script>";

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];

			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
			$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($temp->num_rows() > 0) {
				$this->db->where(Material_RM_PM::$ID, $arr_id_material[$i]);
				$material = $this->db->get(Material_RM_PM::$TABLE_NAME);

				if ($material->num_rows() > 0) {
					$material=$material->row_array();
					$kode_detail = $data_header[Worksheet_Checker_Header::$ID] . str_pad(($i+1), 3, "0", STR_PAD_LEFT);
					$data_detail[Worksheet_Checker_Detail::$ID] = $kode_detail;
					$data_detail[Worksheet_Checker_Detail::$NOMOR_URUT] = null;
					$data_detail[Worksheet_Checker_Detail::$QTY] = $arr_qty[$i];
					$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL] = $material[Material_RM_PM::$ID];
					$data_detail[Worksheet_Checker_Detail::$ID_HEADER] = $data_header[Worksheet_Checker_Header::$ID];

					if ($arr_vendor[$i] != null)
						$data_detail[Worksheet_Checker_Detail::$ID_VENDOR] = $arr_vendor[$i];
					$this->db->insert(Worksheet_Checker_Detail::$TABLE_NAME, $data_detail);

					$data_vendor = null;
					if ($arr_vendor[$i] != null)
						$data_vendor = $arr_vendor[$i];

					//Insert History Stock RM PM
					$this->insert_history_stock_rm_pm(
						$data_header[Worksheet_Checker_Header::$NOMOR_WS],
						$data_header[Worksheet_Checker_Header::$NOMOR_WS],
						$data_header[Worksheet_Checker_Header::$TANGGAL],
						$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL],
						$data_vendor,
						0,
						$data_detail[Worksheet_Checker_Detail::$QTY],
						Worksheet_Checker_Detail::GEN_MUTASI_MESSAGE(
							$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID],
							$kode,
							$keterangan
						)
					);

					//Insert Stock RM PM
					if ($arr_vendor[$i] != null) {
						$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
					}
					$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
					$temp_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
					if ($temp_stock->num_rows() > 0) {
						$data = [];
						$stock = $temp_stock->row_array();
						$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
						$qty_awal = $stock[Material_RM_PM_Stock::$QTY];
						$harga_satuan = $value_awal / $qty_awal;
						$data[Material_RM_PM_Stock::$QTY] = $qty_awal - $data_detail[Worksheet_Checker_Detail::$QTY];
						$data[Material_RM_PM_Stock::$VALUE] = $data[Material_RM_PM_Stock::$QTY] * $harga_satuan;

						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
						if ($arr_vendor[$i] != null) {
							$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
						}
						$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
					}
					
				}
			} else {
				return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
			}
		}
		return Worksheet::$MESSAGE_SUCCESS_MUTASI;
	}
	public function insert_good_receipt_rm_pm_mutasi($ctr, $keterangan, $arr_id_material, $arr_qty, $arr_vendor){
		$data_header = [];
		$max = "";

		$max = $this->db->query("select max(substr(id_purchase_order_checker_header,8)) as max from " . Purchase_Order_Checker_Header::$TABLE_NAME .
								" where id_purchase_order_checker_header like 'ADJ%'")->row()->max;
		if ($max == NULL)
			$max = 0;
		$max += 1;

		//Insert PO Checker Header
		$kode = Purchase_Order::$S_MUT_CODE . "GR" . str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Purchase_Order_Checker_Header::$ID] = $kode;
		$data_header[Purchase_Order_Checker_Header::$TANGGAL] = date('Y-m-d');
		$data_header[Purchase_Order_Checker_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Purchase_Order_Checker_Header::$NOMOR_PO] = null;
		$data_header[$this::$IS_DELETED] = 0;
		$this->db->insert(Purchase_Order_Checker_Header::$TABLE_NAME, $data_header);
		echo "<script>alert('Kode GR: " . $kode . "');</script>";

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];

			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
			$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($temp->num_rows() > 0) {
				$this->db->where(Material_RM_PM::$ID, $arr_id_material[$i]);
				$material = $this->db->get(Material_RM_PM::$TABLE_NAME)->row_array();

				//Insert PO Checker
				$kode_detail = $data_header[Purchase_Order_Checker_Header::$ID] . str_pad(($i+1), 3, "0", STR_PAD_LEFT);
				$data_detail[Purchase_Order_Checker_Detail::$ID] = $kode_detail;
				$data_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT] = null;
				$data_detail[Purchase_Order_Checker_Detail::$QTY] = $arr_qty[$i];

				$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL] = $material[Material_RM_PM::$ID];
				$data_detail[Purchase_Order_Checker_Detail::$ID_HEADER] = $data_header[Purchase_Order_Checker_Header::$ID];

				$this->db->insert(Purchase_Order_Checker_Detail::$TABLE_NAME, $data_detail);

				$data_vendor = null;
				if ($arr_vendor[$i] != null)
					$data_vendor = $arr_vendor[$i];

				//Insert History Stock RM PM
				$this->insert_history_stock_rm_pm(
					$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
					$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
					$data_header[Purchase_Order_Checker_Header::$TANGGAL],
					$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL],
					$data_vendor,
					$data_detail[Purchase_Order_Checker_Detail::$QTY],
					0,
					Purchase_Order_Checker_Detail::GEN_MUTASI_MESSAGE(
						$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID],
						$kode,
						$keterangan
					)
				);

				//Insert Stock RM PM
				if ($arr_vendor[$i] != null) {
					$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
				}
				$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
				$temp_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
				if ($temp_stock->num_rows() > 0) {
					$data = [];
					$stock = $temp_stock->row_array();
					$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
					$qty_awal = $stock[Material_RM_PM_Stock::$QTY];
					$harga_satuan = $value_awal / $qty_awal;
					$data[Material_RM_PM_Stock::$QTY] = $qty_awal + $data_detail[Worksheet_Checker_Detail::$QTY];
					$data[Material_RM_PM_Stock::$VALUE] = $data[Material_RM_PM_Stock::$QTY] * $harga_satuan;

					$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
					if ($arr_vendor[$i] != null) {
						$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
					}
					$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
				}
				
			} else {
				return Purchase_Order_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
			}
		}
		return Purchase_Order::$MESSAGE_SUCCESS_MUTASI;
	}
	
	//Vendor
	public function vendor_update_batch($data){
		$kal = "";
		for ($i = 0;
			$i < count($data['values']) + 10;
			$i++
		) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$id = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$nama = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;


					$data_query = array(
						Vendor::$ID => $id,
						Vendor::$NAMA => $nama,
						$this::$IS_DELETED => 0,
					);

					//INSERT Vendor
					$this->db->insert(Vendor::$TABLE_NAME, $data_query);
				} else
					break;
			}
		}
		return $kal;
	}

	//Gaji
	public function available_karyawan_insentif($nik,$tipe_gaji){
		$tipe_gaji=strtoupper($tipe_gaji);
		$query=$this->db->query("select * from karyawan,karyawan_insentif,standar_insentif
								 where karyawan_insentif.is_deleted=0 and
								 karyawan.nik=karyawan_insentif.nik and
								 karyawan_insentif.id_standar_insentif=standar_insentif.id_standar_insentif
								 and karyawan.nik='$nik' and upper(standar_insentif.nama)='$tipe_gaji'");
		if($query->num_rows()==0)
			return false;
		return true;
	}

	//Karyawan Insentif
	public function get_karyawan_insentif_ket_not_null($nik,$bulan,$tahun,$tipe_tunjangan = NULL){
		$tahun=ARR_TAHUN[$tahun];
		
		$bulan=strtoupper(ARR_BULAN_NO_SEMUA[$bulan]);
		$keterangan=$bulan." ".$tahun;
		$concat="";
		if($tipe_tunjangan!=NULL){
			$id_standar_insentif=$this->get(Standar_Insentif::$TABLE_NAME,Standar_Insentif::$NAMA,strtoupper($tipe_tunjangan))->row_array()[Standar_Insentif::$ID];
			$concat=" and standar_insentif.id_standar_insentif=".$id_standar_insentif;
		}
		$data=$this->db->query("select nama,karyawan_insentif.keterangan keterangan,id_karyawan_insentif,karyawan_insentif.nominal nominal,karyawan_insentif.id_standar_insentif id_standar_insentif 
								 from karyawan_insentif,standar_insentif where nik='$nik' and upper(keterangan)='$keterangan' 
								and karyawan_insentif.id_standar_insentif=standar_insentif.id_standar_insentif and keterangan is not null
								and karyawan_insentif.is_deleted=0 $concat");
		
		
		return $data;
	}
	/*public function insert_karyawan_insentif($nik){
		$data=$this->db->query("select * from standar_insentif where is_deleted=0");
		foreach($data->result() as $row){
			$temp=$this->db->query("select * from karyawan_insentif where is_deleted=0 and nik='$nik' and id_standar_insentif='".$row->id_standar_insentif."'");
			if($temp->num_rows()==0)
				$this->db->query("insert into karyawan_insentif values('$nik','".$row->id_standar_insentif."',0)");
		}

	}*/
	public function update_karyawan_insentif($nik,$tipe_tunjangan,$keterangan,$nominal){
		$id_standar_insentif=$this->get(Standar_Insentif::$TABLE_NAME,Standar_Insentif::$NAMA,strtoupper($tipe_tunjangan))->row_array()[Standar_Insentif::$ID];
		//return $id_standar_insentif; 
		if($this->equals($tipe_tunjangan,S_INSENTIF_T_OBAT)){
			$tunjangan_obat=$this->get_karyawan_by(Karyawan::$ID,$nik)->row()->tunjangan_obat;
			if($tunjangan_obat-$nominal>=0){
				$query=$this->db->query("select karyawan_insentif.nominal nominal
										from karyawan_insentif where nik=$nik and id_standar_insentif=$id_standar_insentif
										and upper(keterangan)='$keterangan'");
				$is_already_edited=false;
				if($query->num_rows()>0){
					if($query->row()->nominal>0){
						$is_already_edited=true;
					}
				}
				if(!$is_already_edited){
					$this->db->query("update karyawan_insentif set nominal=$nominal where nik=$nik and id_standar_insentif=$id_standar_insentif
									and upper(keterangan)='$keterangan'");
					$this->db->query("update karyawan set tunjangan_obat=tunjangan_obat-$nominal where nik='$nik'");
					return Karyawan_Insentif::$MESSAGE_SUCCESS_UPDATE;
				}else{
					return Karyawan_Insentif::$MESSAGE_FAILED_UPDATE_ALREADY_UPDATED;
				}
			}else{
				return Karyawan_Insentif::$MESSAGE_FAILED_UPDATE_OUT_OF_BOUND;
			}
		}else{
			$this->db->query("update karyawan_insentif set nominal=$nominal where nik=$nik and id_standar_insentif=$id_standar_insentif
								and upper(keterangan)='$keterangan'");
			return Karyawan_Insentif::$MESSAGE_SUCCESS_UPDATE;
		}
	}
	public function insert_karyawan_obat($nik){
		for($i=0;$i<12;$i++){
			$tahun=date("Y");
			$bulan=strtoupper(ARR_BULAN_NO_SEMUA[$i]);
			$keterangan=$bulan." ".$tahun;
			$data=$this->db->query("select * from karyawan_insentif,standar_insentif where karyawan_insentif.id_standar_insentif=standar_insentif.id_standar_insentif
									and karyawan_insentif.is_deleted=0 and upper(standar_insentif.nama)='".S_INSENTIF_T_OBAT."' 
									and nik='$nik' and upper(keterangan)='".$keterangan."'");
			if($data->num_rows()==0){
				$this->db->query("insert into karyawan_insentif values('',$nik,3,'$keterangan',0,0)");
			}
		}

	}
	public function insert_karyawan_insentif_1_year($tipe){
		$karyawan=$this->get_all_karyawan();
		foreach($karyawan->result() as $row){
			$nik=$row->nik;
			for($i=0;$i<12;$i++){
				$tahun=date("Y");
				$bulan=strtoupper(ARR_BULAN_NO_SEMUA[$i]);
				$keterangan=$bulan." ".$tahun;
				$data=$this->db->query("select * from karyawan_insentif,standar_insentif where karyawan_insentif.id_standar_insentif=standar_insentif.id_standar_insentif
										and karyawan_insentif.is_deleted=0 and karyawan_insentif.id_standar_insentif=$tipe 
										and nik='$nik' and upper(keterangan)='".$keterangan."'");
				if($data->num_rows()==0){
					$this->db->query("insert into karyawan_insentif values('',$nik,$tipe,'$keterangan',0,0)");
				}
			}
		}
	}

	//Good Receipt
	public function insert_good_receipt($ctr,$r,$sto,$nomor_sj,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang){
		$arr = [];
		//$this->db->like(H_Good_Receipt::$NOMOR_SJ, $nomor_sj, 'before');
		$this->db->where(H_Good_Receipt::$NOMOR_SJ,$nomor_sj);
		$this->db->where(H_Good_Receipt::$KODE_REVISI . " is NULL", NULL, FALSE);
		$temp=$this->db->get(H_Good_Receipt::$TABLE_NAME);
		if($temp->num_rows()==0 && $this->equals($r,"y")){
			$this->db->where(H_Good_Issue::$KODE_REVISI . " is NULL", NULL, FALSE);
			$this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);
			$temp1 = $this->db->get(H_Good_Issue::$TABLE_NAME);
			if($temp1->num_rows() == 0)
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_SJ_NOT_FOUND;
			else
				$arr[H_Good_Receipt::$REV_GOOD_ISSUE]=1;
		}

		//AUTO GEN kode surat jalan
		$max=$this->db->query("select max(substr(id_H_Good_Receipt,8)) as max from ".H_Good_Receipt::$TABLE_NAME."
								where MONTH(tanggal)=".date("m")." and YEAR(tanggal)=".date("Y"))->row()->max;
		if($max==null)
			$max=0;
		$max+=1;
		
		$kode=date("ym").str_pad($max,6,"0",STR_PAD_LEFT);

		//header
		$arr[H_Good_Receipt::$ID]="GR".$kode;
		$arr[H_Good_Receipt::$NIK]=$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$no_sj_insert="";
		if($this->equals($r,"n")){
			if($this->equals($sto,Surat_Jalan::$KEYWORD_SO))
				$no_sj_insert=H_Good_Receipt::$SO_CODE.date("y").str_pad($nomor_sj,7,"0",STR_PAD_LEFT);
			else
				$no_sj_insert=H_Good_Receipt::$STO_CODE.date("y").str_pad($nomor_sj,8,"0",STR_PAD_LEFT);
			$arr[H_Good_Receipt::$NOMOR_SJ]=$no_sj_insert;

			//cek apakah surat jalan masuk sudah ada atau belum
			$this->db->where(H_Good_Receipt::$NOMOR_SJ, $no_sj_insert);
			$this->db->where(H_Good_Receipt::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$temp = $this->db->get(H_Good_Receipt::$TABLE_NAME);
			if ($temp->num_rows() > 0 && $this->equals($r, "n")) {
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED;
			}

			/*
			//cek apakah surat jalan masuk sudah ada atau belum
			$this->db->where(H_Good_Receipt::$NOMOR_SJ, $no_sj_insert);
			$temp2 = $this->db->get(H_Good_Receipt::$TABLE_NAME);
			if($temp2->num_rows()>0){ 
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED;
			}*/
		}else{
			//cek apakah akses revisi diberikan
			$this->db->where(List_Access::$ACCESS, ACCESS_REVISION_SURAT_JALAN);
			$id_access=$this->db->get(List_Access::$TABLE_NAME)->row_array()[List_Access::$ID];

			//$arr_access[User_Access::$IS_TRUE]=0;
			$this->db->where(User_Access::$NIK,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$this->db->where(User_Access::$IS_TRUE,1);
			$this->db->where(User_Access::$ID_ACCESS,$id_access);
			$qry=$this->db->get(User_Access::$TABLE_NAME);

			//jika diijinkan
			if($qry->num_rows()>0){ //ada yang mengijinkan
				//update revisi set 0
				$arr_access[User_Access::$IS_TRUE]=0;
				$this->db->where(User_Access::$NIK,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
				$this->db->where(User_Access::$ID_ACCESS,$id_access);
				$this->db->update(User_Access::$TABLE_NAME,$arr_access);

				$kd=strtoupper(H_Good_Receipt::$REV_CODE).date("y");
				$max_rev_code=$this->db->query("select max(substr(kode_revisi,8)) as max from ".H_Good_Receipt::$TABLE_NAME."
									where upper(kode_revisi) like '%".$kd."%'")->row()->max;
				if($max_rev_code==null)
					$max_rev_code=0;
				$max_rev_code+=1;
				$arr[H_Good_Receipt::$KODE_REVISI]=$kd.str_pad($max_rev_code,6,"0",STR_PAD_LEFT);
				$arr[H_Good_Receipt::$NOMOR_SJ]=$nomor_sj;
			}else	
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_IS_NOT_PERMITTED;	
		}
		$arr[H_Good_Receipt::$STO]=$sto;
		$this->db->set(H_GOOD_RECEIPT::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Receipt::$QTY]=$ctr;
		$arr[$this::$IS_DELETED]=0;
		$this->db->insert(H_Good_Receipt::$TABLE_NAME,$arr);
		echo "<script>alert('Kode GR: ".$arr[H_Good_Receipt::$ID]."');</script>";
		$ctr_d_good_receipt=1;
		for($i=0;$i<$ctr;$i++){
			$id_gudang="";
			//arr id gudang harus dicari dulu, looping untuk gudang yang material nya adalah barang itu, cek dengan fifo qtynya

			$this->db->where(Material::$KEMASAN,$arr_kemasan_barang[$i]);
			$this->db->where(Material::$WARNA,$arr_warna_barang[$i]);
			$this->db->where("Upper(".Material::$KODE_BARANG.")",strtoupper($arr_kode_barang[$i]));
			$material=$this->db->get(Material::$TABLE_NAME)->row_array();


			$qry_gudang=$this->get(Gudang::$TABLE_NAME,Gudang::$ID_MATERIAL,$material[Material::$ID]);
			$stock_in=($arr_dus_barang[$i]*$material[Material::$BOX])+$arr_tin_barang[$i];
			if($qry_gudang->num_rows()>0){
				foreach($qry_gudang->result_array() as $row_gudang){
					if($stock_in<=0){
						break;
					}else{
						$kapasitas_total=$row_gudang[Gudang::$KAPASITAS];
						//if($box>0){ //jika tidak 0 berarti kapasitas masih box, harus dijadikan tin
							$kapasitas_total*= $material[Material::$BOX];
						//}
						$kapasitas_terisi=$this->get_stock_by($material[Material::$ID],$row_gudang[Gudang::$ID]);
						$kapasitas_sisa=$kapasitas_total-$kapasitas_terisi;

						if($kapasitas_sisa>0){
							$total_tin=0;
							if($kapasitas_sisa-$stock_in>=0) //jika kapasitas masih mencukupi
								$total_tin=$stock_in;
							else
								$total_tin=$kapasitas_sisa;

							$stock_in-=$total_tin;

							$arr_detail=[];
							$arr_detail[D_Good_Receipt::$ID]="DGR".$kode.str_pad($ctr_d_good_receipt++,"3","0",STR_PAD_LEFT);

							$stock_dus=0;
							$stock_tin=0;
							if($material[Material::$BOX]>1){
								$stock_dus=intdiv($total_tin, $material[Material::$BOX]);
								$stock_tin=$total_tin % $material[Material::$BOX];
							}else{
								$stock_tin=$total_tin;
							}
							$id_gudang_loop=$row_gudang[Gudang::$ID];

							$arr_detail[D_Good_Receipt::$DUS]=$stock_dus;
							$arr_detail[D_Good_Receipt::$TIN]=$stock_tin;
							$arr_detail[D_Good_Receipt::$ID_GUDANG]=$id_gudang_loop;

							$arr_detail[D_Good_Receipt::$ID_MATERIAL]=$material[Material::$ID];
							$arr_detail[D_Good_Receipt::$ID_H_GOOD_RECEIPT]=$arr[H_Good_Receipt::$ID];
							$this->db->insert(D_Good_Receipt::$TABLE_NAME,$arr_detail);

							//Penambahan Stock Gudang
							$pesan="";
							if ($this->equals($r, "n"))
								$pesan = H_Good_Receipt::GEN_INSERT_MESSAGE($no_sj_insert, $arr[H_Good_Receipt::$ID]);
							else
								$pesan = H_Good_Receipt::GEN_REVISION_MESSAGE($arr[H_Good_Receipt::$NOMOR_SJ], $arr[H_Good_Receipt::$KODE_REVISI], $arr[H_Good_Receipt::$ID]);
							$this->insert_stock_gudang($material[Material::$ID], $id_gudang_loop, $total_tin,0,$pesan);

							//Fifo Stock Gudang
							//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
							$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL,$material[Material::$ID]);
							$this->db->order_by(Fifo_Stock_Gudang::$ID,"desc");

							$qry_fifo=$this->db->get(Fifo_Stock_Gudang::$TABLE_NAME);
							$id_gudang="";

							if($qry_fifo->num_rows()>0){
								$qry_fifo=$qry_fifo->row_array();
								$id_gudang=$qry_fifo[Fifo_Stock_Gudang::$ID_GUDANG];
							}
							$arr_fifo=[];
							if($id_gudang!=$id_gudang_loop){
								$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL]=$material[Material::$ID];
								$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG]=$id_gudang_loop;
								$arr_fifo[Fifo_Stock_Gudang::$QTY]=$total_tin;
								$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
							}else{
								$this->db->where(Fifo_Stock_Gudang::$ID,$qry_fifo[Fifo_Stock_Gudang::$ID]);
								$arr_fifo[Fifo_Stock_Gudang::$QTY]=$qry_fifo[Fifo_Stock_Gudang::$QTY]+$total_tin;
								$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
							}
							
							
						}
					}
				}
				if($stock_in>0){ //Masuk Cadangan
					$this->db->where(Gudang::$RAK, Gudang::$S_CADANGAN);
					$this->db->where(Gudang::$NAMA_GUDANG, $qry_gudang->row_array()[Gudang::$NAMA_GUDANG]);
					$qry_temp_gudang = $this->db->get(Gudang::$TABLE_NAME);
					$id_gudang_loop = $qry_temp_gudang->row_array()[Gudang::$ID];

					//Penambahan Stock Gudang
					$pesan="";
					if ($this->equals($r, "n"))
						$pesan = H_Good_Receipt::GEN_INSERT_MESSAGE($no_sj_insert, $arr[H_Good_Receipt::$ID]);
					else
						$pesan = H_Good_Receipt::GEN_REVISION_MESSAGE($arr[H_Good_Receipt::$NOMOR_SJ], $arr[H_Good_Receipt::$KODE_REVISI], $arr[H_Good_Receipt::$ID]);
					$this->insert_stock_gudang($material[Material::$ID],$id_gudang_loop,$stock_in,0,$pesan);

					//Fifo Stock Gudang
					//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
					$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL,$material[Material::$ID]);
					$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG,$id_gudang_loop);
					$qry_fifo=$this->db->get(fifo_stock_gudang::$TABLE_NAME);

					$arr_fifo=[];
					if($qry_fifo->num_rows()==0){
						$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL]= $material[Material::$ID];
						$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG]=$id_gudang_loop;
						$arr_fifo[Fifo_Stock_Gudang::$QTY]=$stock_in;
						$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
					}else{
						$this->db->where(Fifo_Stock_Gudang::$ID,$qry_fifo->row_array()[Fifo_Stock_Gudang::$ID]);
						$arr_fifo[Fifo_Stock_Gudang::$QTY]=$qry_fifo->row_array()[Fifo_Stock_Gudang::$QTY]+$stock_in;
						$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
					}

					$stock_dus=0;
					$stock_tin=0;
					if($material[Material::$BOX]>1){
						$stock_dus=intdiv($stock_in, $material[Material::$BOX]);
						$stock_tin=$stock_in % $material[Material::$BOX];
					}else{
						$stock_tin=$stock_in;
					}
					$arr_detail=[];
					$arr_detail[D_Good_Receipt::$ID]="DGR".$kode.str_pad($ctr_d_good_receipt++,"3","0",STR_PAD_LEFT);
					$arr_detail[D_Good_Receipt::$DUS]=$stock_dus;
					$arr_detail[D_Good_Receipt::$TIN]=$stock_tin;
					$arr_detail[D_Good_Receipt::$ID_GUDANG]=$id_gudang_loop;

					$arr_detail[D_Good_Receipt::$ID_MATERIAL] = $material[Material::$ID];
					$arr_detail[D_Good_Receipt::$ID_H_GOOD_RECEIPT]=$arr[H_Good_Receipt::$ID];
					$this->db->insert(D_Good_Receipt::$TABLE_NAME,$arr_detail);
					
				}
				//return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
				
			}else{
				$this->db->where(H_Good_Receipt::$ID, $arr[H_Good_Receipt::$ID]);
				$this->db->delete(H_Good_Receipt::$TABLE_NAME);
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
			}

		}
		return H_Good_Receipt::$MESSAGE_SUCCESS_INSERT;
		
		
	}
	public function insert_good_receipt_adjustment($ctr,$keterangan,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang){
		//AUTO GEN kode surat jalan
		$max=$this->db->query("select max(substr(id_H_Good_Receipt,8)) as max from ".H_Good_Receipt::$TABLE_NAME."
								where MONTH(tanggal)=".date("m")." and YEAR(tanggal)=".date("Y"))->row()->max;
		if($max==null)
			$max=0;
		$max+=1;
		
		$kode=date("ym").str_pad($max,6,"0",STR_PAD_LEFT);
		
		//header
		$arr=[];
		$arr[H_Good_Receipt::$ID]="GR".$kode;
		$arr[H_Good_Receipt::$NIK]=$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		
		$kd=strtoupper(H_Good_Receipt::$ADJ_CODE);
		$max_rev_code=$this->db->query("select max(substr(nomor_sj,7)) as max from ".H_Good_Receipt::$TABLE_NAME."
							where upper(nomor_sj) like '%".$kd."%'")->row()->max;
		if($max_rev_code==null)
			$max_rev_code=0;
		$max_rev_code+=1;
		$arr[H_Good_Receipt::$NOMOR_SJ]=$kd.str_pad($max_rev_code,6,"0",STR_PAD_LEFT);
		
		$this->db->set(H_GOOD_RECEIPT::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Receipt::$QTY]=$ctr;
		$arr[$this::$IS_DELETED]=0;
		$this->db->insert(H_Good_Receipt::$TABLE_NAME,$arr);
		echo "<script>alert('Kode GR: ".$arr[H_Good_Receipt::$ID]."');</script>";
		$ctr_d_good_receipt=1;
		for($i=0;$i<$ctr;$i++){
			$id_gudang="";
			//arr id gudang harus dicari dulu, looping untuk gudang yang material nya adalah barang itu, cek dengan fifo qtynya

			$this->db->where(Material::$KEMASAN,$arr_kemasan_barang[$i]);
			$this->db->where(Material::$WARNA,$arr_warna_barang[$i]);
			$this->db->where("Upper(".Material::$KODE_BARANG.")",strtoupper($arr_kode_barang[$i]));
			$material=$this->db->get(Material::$TABLE_NAME)->row_array();

			$qry_gudang=$this->get(Gudang::$TABLE_NAME,Gudang::$ID_MATERIAL,$material[Material::$ID]);
			$stock_in=($arr_dus_barang[$i]*$material[Material::$BOX])+$arr_tin_barang[$i];
			if($qry_gudang->num_rows()>0){
				foreach($qry_gudang->result_array() as $row_gudang){
					if($stock_in<=0){
						break;
					}else{
						$kapasitas_total=$row_gudang[Gudang::$KAPASITAS];
						//if($box>0){ //jika tidak 0 berarti kapasitas masih box, harus dijadikan tin
							$kapasitas_total*= $material[Material::$BOX];
						//}
						$kapasitas_terisi=$this->get_stock_by($material[Material::$ID],$row_gudang[Gudang::$ID]);
						$kapasitas_sisa=$kapasitas_total-$kapasitas_terisi;

						if($kapasitas_sisa>0){
							$total_tin=0;
							if($kapasitas_sisa-$stock_in>=0) //jika kapasitas masih mencukupi
								$total_tin=$stock_in;
							else
								$total_tin=$kapasitas_sisa;

							$stock_in-=$total_tin;

							$arr_detail=[];
							$arr_detail[D_Good_Receipt::$ID]="DGR".$kode.str_pad($ctr_d_good_receipt++,"3","0",STR_PAD_LEFT);

							$stock_dus=0;
							$stock_tin=0;
							if($material[Material::$BOX]>1){
								$stock_dus=intdiv($total_tin,$material[Material::$BOX]);
								$stock_tin=$total_tin % $material[Material::$BOX];
							}else{
								$stock_tin=$total_tin;
							}
							$id_gudang_loop=$row_gudang[Gudang::$ID];

							$arr_detail[D_Good_Receipt::$DUS]=$stock_dus;
							$arr_detail[D_Good_Receipt::$TIN]=$stock_tin;
							$arr_detail[D_Good_Receipt::$ID_GUDANG]=$id_gudang_loop;

							$arr_detail[D_Good_Receipt::$ID_MATERIAL]=$material[Material::$ID];
							$arr_detail[D_Good_Receipt::$ID_H_GOOD_RECEIPT]=$arr[H_Good_Receipt::$ID];
							$this->db->insert(D_Good_Receipt::$TABLE_NAME,$arr_detail);

							//Penambahan Stock Gudang
							$this->insert_stock_gudang($material[Material::$ID],$id_gudang_loop,$total_tin,0, H_Good_Receipt::GEN_ADJUSTMENT_MESSAGE($_SESSION[SESSION_KARYAWAN_PENTA]['nik'],$arr[H_Good_Receipt::$NOMOR_SJ]).", Ket: ".$keterangan);

							//Fifo Stock Gudang
							//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
							$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL,$material[Material::$ID]);
							$this->db->order_by(Fifo_Stock_Gudang::$ID,"desc");

							$qry_fifo=$this->db->get(fifo_stock_gudang::$TABLE_NAME);
							$id_gudang="";

							if($qry_fifo->num_rows()>0){
								$qry_fifo=$qry_fifo->row_array();
								$id_gudang=$qry_fifo[Fifo_Stock_Gudang::$ID_GUDANG];
							}
							$arr_fifo=[];
							if($id_gudang!=$id_gudang_loop){
								$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL]=$material[Material::$ID];
								$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG]=$id_gudang_loop;
								$arr_fifo[Fifo_Stock_Gudang::$QTY]=$total_tin;
								$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
							}else{
								$this->db->where(Fifo_Stock_Gudang::$ID,$qry_fifo[Fifo_Stock_Gudang::$ID]);
								$arr_fifo[Fifo_Stock_Gudang::$QTY]=$qry_fifo[Fifo_Stock_Gudang::$QTY]+$total_tin;
								$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
							}
							
							
						}
					}
				}
				if($stock_in>0){ //Masuk Cadangan
					$this->db->where(Gudang::$RAK,Gudang::$S_CADANGAN);
					$this->db->where(Gudang::$NAMA_GUDANG,$qry_gudang->row()->nama_gudang);
					$qry_temp_gudang=$this->db->get(Gudang::$TABLE_NAME);
					$id_gudang_loop=$qry_temp_gudang->row()->id_gudang;

					//Penambahan Stock Gudang
					$this->insert_stock_gudang($material[Material::$ID],$id_gudang_loop,$stock_in,0, H_Good_Receipt::GEN_ADJUSTMENT_MESSAGE($_SESSION[SESSION_KARYAWAN_PENTA]['nik'],$arr[H_Good_Receipt::$NOMOR_SJ]).", Ket: ".$keterangan);

					//Fifo Stock Gudang
					//cek dulu apakah stock barang dengan id_material di gudang input adalah terakhir? jika ya maka update qty bukan insert
					$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL,$material[Material::$ID]);
					$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG,$id_gudang_loop);
					$qry_fifo=$this->db->get(fifo_stock_gudang::$TABLE_NAME);

					$arr_fifo=[];
					if($qry_fifo->num_rows()==0){
						$arr_fifo[Fifo_Stock_Gudang::$ID_MATERIAL]=$material[Material::$ID];
						$arr_fifo[Fifo_Stock_Gudang::$ID_GUDANG]=$id_gudang_loop;
						$arr_fifo[Fifo_Stock_Gudang::$QTY]=$stock_in;
						$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
					}else{
						$this->db->where(Fifo_Stock_Gudang::$ID,$qry_fifo->row_array()[Fifo_Stock_Gudang::$ID]);
						$arr_fifo[Fifo_Stock_Gudang::$QTY]=$qry_fifo->row_array()[Fifo_Stock_Gudang::$QTY]+$stock_in;
						$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
					}
				}
				//return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;
				
			}else
				return H_Good_Receipt::$MESSAGE_FAILED_INSERT_GUDANG_NOT_FOUND;

		}
		return H_Good_Receipt::$MESSAGE_SUCCESS_ADJUSTMENT;
	}
	public function get_good_receipt_by_sj($id_sj){
		$arr=[H_Good_Receipt::$KODE_REVISI,H_Good_Receipt::$NIK,Material::$KODE_BARANG,Material::$KEMASAN,Material::$WARNA,
				Material::$DESCRIPTION,H_Good_Receipt::$TABLE_NAME.".".H_Good_Receipt::$ID." ".H_Good_Receipt::$ID,
				H_Good_Receipt::$NOMOR_SJ,H_Good_Receipt::$STO,H_Good_Receipt::$TANGGAL,H_Good_Receipt::$QTY,
				Material::$TABLE_NAME.".".Material::$PRODUCT_CODE." ".Material::$PRODUCT_CODE,
				D_Good_Receipt::$DUS,D_Good_Receipt::$TIN,D_Good_Receipt::$ID_GUDANG,H_Good_Receipt::$NIK_SOPIR,H_Good_Receipt::$NIK_KERNET,
				H_Good_Receipt::$TANGGAL_CONFIRM_ADMIN];
		$this->db->select($arr);
		$this->db->from(H_Good_Receipt::$TABLE_NAME);
		$temp = H_Good_Receipt::$TABLE_NAME . "." . H_Good_Receipt::$ID . "=" . D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_H_GOOD_RECEIPT;
		$this->db->join(D_Good_Receipt::$TABLE_NAME, $temp);
		$temp = D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_MATERIAL . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $temp);
		$temp=H_Good_Receipt::$TABLE_NAME.".".$this::$IS_DELETED."=0";
		$this->db->where($temp);
		$this->db->where(H_Good_Receipt::$NOMOR_SJ,$id_sj);
		return $this->db->get();
		return $this->db->query("
			select kode_revisi,nik,kode_barang,kemasan,warna,description,h.id_h_good_receipt id,nomor_sj,sto,tanggal,
			qty,d.product_code p_code,dus,tin,id_gudang
			from h_good_receipt h,d_good_receipt d,material m
			where h.id_h_good_receipt=d.id_h_good_receipt and
				h.is_deleted=0 and (nomor_sj='$id_sj') and
				m.product_code=d.product_code");
	}
	public function get_good_receipt_by_tanggal($dt,$st){
		//$this->db->from(H_Good_Receipt::$TABLE_NAME);
		$this->db->where(H_Good_Receipt::$TANGGAL . ' is NOT NULL', NULL, FALSE);
		if($dt!=NULL && $st!=NULL){
			$until=date("Y-m-d",strtotime($st. "+ 1 day"));
			$this->db->where(H_Good_Receipt::$TANGGAL.' >=', $dt, TRUE);
			$this->db->where(H_Good_Receipt::$TANGGAL.' <', $until, TRUE);
		}
		return $this->db->get(H_Good_Receipt::$TABLE_NAME);
	}
	public function good_receipt_get_all_rekap_sj($dt,$st,$n,$kb,$k,$w){
		/*
		SELECT h_good_receipt.id_h_good_receipt,nomor_sj,tanggal,nik,material.id_material,material.box,sum(dus),sum(tin)
		FROM `d_good_receipt`,h_good_receipt,material
		where d_good_receipt.id_h_good_receipt=h_good_receipt.id_h_good_receipt AND
		d_good_receipt.id_material_fg=material.id_material
		group by h_good_receipt.id_h_good_receipt,nomor_sj,tanggal,nik,material.id_material
		*/
		$select=array(
			H_Good_Receipt::$TABLE_NAME.".".H_Good_Receipt::$ID." ".H_Good_Receipt::$ID,
			H_Good_Receipt::$NOMOR_SJ,
			H_Good_Receipt::$TANGGAL,
			H_Good_Receipt::$NIK,
			"sum(".D_Good_Receipt::$DUS.")"." ". D_Good_Receipt::$DUS,
			"sum(".D_Good_Receipt::$TIN. ")" . D_Good_Receipt::$TIN,
			D_Good_Receipt::$ID_MATERIAL,
			Material::$BOX,
		);
		$group_by = array(
			H_Good_Receipt::$ID,
			H_Good_Receipt::$NOMOR_SJ,
			H_Good_Receipt::$TANGGAL,
			H_Good_Receipt::$NIK,
			D_Good_Receipt::$ID_MATERIAL,
			Material::$BOX,
		);
		$this->db->select($select);
		$this->db->from(D_Good_Receipt::$TABLE_NAME);
		$join = D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_H_GOOD_RECEIPT . "=" . H_Good_Receipt::$TABLE_NAME . "." . H_Good_Receipt::$ID;
		$this->db->join(H_Good_Receipt::$TABLE_NAME, $join);
		$join = D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_MATERIAL  . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Receipt::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Receipt::$TANGGAL . ' <', $until, TRUE);
		}
		if($n != NULL){
			$this->db->where(H_Good_Receipt::$NOMOR_SJ,$n);
		}
		if ($kb != NULL) {
			$this->db->where(Material::$KODE_BARANG, $kb);
		}
		if ($k != NULL) {
			$this->db->where(Material::$KEMASAN, $k);
		}
		if ($w != NULL) {
			$this->db->where(Material::$WARNA, $w);
		}
		$this->db->group_by($group_by);
		//$data[H_Good_Receipt::$TABLE_NAME]=$this->get(H_Good_Receipt::ta)
		return $this->db->get();
	}
	public function good_receipt_get_rev_good_issue_by_sj($id_sj){
		$arr_select=[
			H_Good_Receipt::$NOMOR_SJ,H_Good_Receipt::$TANGGAL,D_Good_Receipt::$DUS,D_Good_Receipt::$TIN,
			D_Good_Receipt::$ID_MATERIAL,D_Good_Receipt::$ID_GUDANG
		];
		$this->db->select($arr_select);
		$this->db->from(H_Good_Receipt::$TABLE_NAME);
		$this->db->join(D_Good_Receipt::$TABLE_NAME,H_Good_Receipt::$TABLE_NAME.".".H_Good_Receipt::$ID."=".D_Good_Receipt::$TABLE_NAME.".".D_Good_Receipt::$ID_H_GOOD_RECEIPT);
		$this->db->where(H_Good_Receipt::$NOMOR_SJ,$id_sj);
		$this->db->where(H_Good_Receipt::$REV_GOOD_ISSUE,1);
		return $this->db->get();
	}

	//Good Issue
	//$r -> revisi
	public function insert_good_issue($ctr,$r,$sto,$ekspedisi,$nomor_sj,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang){
		$arr = [];
		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY,0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//$this->db->like(H_Good_Issue::$NOMOR_SJ, $nomor_sj, 'before');
		$this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);
		$this->db->where(H_Good_Issue::$KODE_REVISI." is NULL", NULL, FALSE);
		$temp=$this->db->get(H_Good_Issue::$TABLE_NAME);
		if($temp->num_rows()==0 && $this->equals($r,"y")){
			$this->db->where(H_Good_Receipt::$KODE_REVISI . " is NULL", NULL, FALSE);
			$this->db->where(H_Good_Receipt::$NOMOR_SJ, $nomor_sj);
			$temp1 = $this->db->get(H_Good_Receipt::$TABLE_NAME);
			if($temp1->num_rows() == 0)
				return H_Good_Issue::$MESSAGE_FAILED_INSERT_SJ_NOT_FOUND;
			else 
				$arr[H_Good_Issue::$REV_GOOD_RECEIPT]=1;
		}

		//AUTO GEN kode surat jalan
		$max=$this->db->query("select max(substr(id_H_Good_Issue,8)) as max from ".H_Good_Issue::$TABLE_NAME."
								where MONTH(tanggal)=".date("m")." and YEAR(tanggal)=".date("Y"))->row()->max;
		if($max==NULL)
			$max=0;
		$max+=1;
		
		$kode=date("ym").str_pad($max,6,"0",STR_PAD_LEFT);

		//header
		$arr[H_Good_Issue::$ID]="GI".$kode;
		$arr[H_Good_Issue::$NIK]=$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$no_sj_insert="";
		if($this->equals($r,"n")){
			if($this->equals($sto,Surat_Jalan::$KEYWORD_SO))
				$no_sj_insert=H_Good_Issue::$SO_CODE.date("y").str_pad($nomor_sj,7,"0",STR_PAD_LEFT);
			else
				$no_sj_insert=H_Good_Issue::$STO_CODE.date("y").str_pad($nomor_sj,8,"0",STR_PAD_LEFT);
			$arr[H_Good_Issue::$NOMOR_SJ]=$no_sj_insert;

			//cek apakah surat jalan keluar sudah ada atau belum
			$this->db->where(H_Good_Issue::$NOMOR_SJ, $no_sj_insert);
			$this->db->where(H_Good_Issue::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$temp = $this->db->get(H_Good_Issue::$TABLE_NAME);
			if ($temp->num_rows() > 0 && $this->equals($r, "n")) {
				return H_Good_Issue::$MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED;
			}

			/*//cek apakah surat jalan keluar sudah ada atau belum
			$this->db->where(H_Good_Issue::$NOMOR_SJ, $no_sj_insert);
			$temp2 = $this->db->get(H_Good_Issue::$TABLE_NAME);
			if($temp2->num_rows()>0){
				return H_Good_Issue::$MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED;
			}

			//cek apakah surat jalan masuk sudah ada atau belum
			$this->db->where(H_Good_Receipt::$NOMOR_SJ, $no_sj_insert);
			$temp2 = $this->db->get(H_Good_Receipt::$TABLE_NAME);
			if($temp2->num_rows()>0){ 
				return H_Good_Issue::$MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED;
			}*/
		}else{
			//cek apakah akses revisi diberikan
			$this->db->where(List_Access::$ACCESS, ACCESS_REVISION_SURAT_JALAN);
			$id_access=$this->db->get(List_Access::$TABLE_NAME)->row_array()[List_Access::$ID];

			//$arr_access[User_Access::$IS_TRUE]=0;
			$this->db->where(User_Access::$NIK,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$this->db->where(User_Access::$ID_ACCESS,$id_access);
			$is_permitted=$this->db->get(User_Access::$TABLE_NAME)->row_array()[User_Access::$IS_TRUE];

			//jika diijinkan
			if($is_permitted){
				
				$kd=strtoupper(H_Good_Issue::$REV_CODE).date("y");
				$max_rev_code=$this->db->query("select max(substr(kode_revisi,8)) as max from ".H_Good_Issue::$TABLE_NAME. "
									where upper(kode_revisi) like '%".$kd."%'")->row()->max;
				if($max_rev_code==null)
					$max_rev_code=0;
				$max_rev_code+=1;
				$arr[H_Good_Issue::$KODE_REVISI]=$kd.str_pad($max_rev_code,6,"0",STR_PAD_LEFT);
				$arr[H_Good_Issue::$NOMOR_SJ]=$nomor_sj;
				
				//update revisi set 0
				$arr_access[User_Access::$IS_TRUE] = 0;
				$this->db->where(User_Access::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
				$this->db->where(User_Access::$ID_ACCESS, $id_access);
				$this->db->update(User_Access::$TABLE_NAME, $arr_access);
			}else
				return H_Good_Issue::$MESSAGE_FAILED_INSERT_IS_NOT_PERMITTED;
		}
		$arr[H_Good_Issue::$STO]=$sto;
		$this->db->set(H_Good_Issue::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Issue::$QTY]=$ctr;
		$arr[H_Good_Issue::$EKSPEDISI]=$ekspedisi;
		$arr[$this::$IS_DELETED]=0;

		//Cek apakah SJ Sudah pernah dibuat atau belum (kalau sudah, tanggal confirm admin akan diisi mengikuti inputan lalu)
		$this->db->where(H_Good_Issue::$NOMOR_SJ,$arr[H_Good_Issue::$NOMOR_SJ]);
		$h_good_issue=$this->db->get(H_Good_Issue::$TABLE_NAME);
		if($h_good_issue->num_rows() > 0){
			foreach($h_good_issue->result_array() as $row){
				if($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]!=null){
					$arr[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]=$row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN];
					break;
				}
			}
		}

		$this->db->insert(H_Good_Issue::$TABLE_NAME,$arr);
		echo "<script>alert('Kode GI: ".$arr[H_Good_Issue::$ID]."');</script>";
		$counter=1;
		for($i=0;$i<$ctr;$i++){
			$this->db->where(Material::$KEMASAN,$arr_kemasan_barang[$i]);
			$this->db->where(Material::$WARNA,$arr_warna_barang[$i]);
			$this->db->where("Upper(".Material::$KODE_BARANG.")",strtoupper($arr_kode_barang[$i]));
			$material=$this->db->get(Material::$TABLE_NAME)->row_array();
			$id_material=$material[Material::$ID];
			$stock_out = ($arr_dus_barang[$i] * $material[Material::$BOX]) + $arr_tin_barang[$i];
			$mode="";
			do{
				$this->db->from(Fifo_Stock_Gudang::$TABLE_NAME);
				if(!$this->equals($mode,Gudang::$S_CADANGAN)){
					$this->db->select("fifo_stock_gudang.id_gudang id_gudang,id_fifo,qty");
					$this->db->join(Gudang::$TABLE_NAME,"fifo_stock_gudang.id_gudang=gudang.id_gudang");
					$this->db->where(Gudang::$RAK . " !=", Gudang::$S_CADANGAN);
				}
				$this->db->where(Fifo_Stock_Gudang::$TABLE_NAME.".".Fifo_Stock_Gudang::$ID_MATERIAL,$id_material);
				$this->db->where(Fifo_Stock_Gudang::$QTY." !=",0);
				
				$qry=$this->db->get();
				foreach($qry->result() as $row){
					$selisih=0;
					if($stock_out-$row->qty<=0){
						$selisih=$stock_out;
						$stock_out=0;
					}else{
						$selisih=$row->qty;
						$stock_out-=$selisih;
					}

					//Pengurangan Stock Gudang
					$pesan="";
					if($this->equals($r,"n"))
						$pesan=H_Good_Issue::GEN_INSERT_MESSAGE($no_sj_insert, $arr[H_Good_Issue::$ID]);
					else
						$pesan=H_Good_Issue::GEN_REVISION_MESSAGE($arr[H_Good_Issue::$NOMOR_SJ], $arr[H_Good_Issue::$KODE_REVISI], $arr[H_Good_Issue::$ID]);
					$this->insert_stock_gudang($id_material, $row->id_gudang, 0, $selisih, $pesan);

					$arr_detail=[];
					$arr_detail[D_Good_Issue::$ID]="DGI".$kode.str_pad($counter,"3","0",STR_PAD_LEFT);
					$counter+=1;
					$stock_dus=0;
					$stock_tin=0;
					if($material[Material::$BOX]>1){
						$stock_dus=intdiv($selisih,$material[Material::$BOX]);
						$stock_tin=$selisih%$material[Material::$BOX];
					}else{
						$stock_tin=$selisih;
					}
					$arr_detail[D_Good_Issue::$DUS]=$stock_dus;
					$arr_detail[D_Good_Issue::$TIN]=$stock_tin;
					$arr_detail[D_Good_Issue::$ID_GUDANG]=$row->id_gudang;

					$arr_detail[D_Good_Issue::$ID_MATERIAL]=$id_material;
					$arr_detail[D_Good_Issue::$ID_H_GOOD_ISSUE]=$arr[H_Good_Issue::$ID];
					$this->db->insert(D_Good_Issue::$TABLE_NAME,$arr_detail);
					
					//Fifo Stock Gudang
					$arr_fifo=[];
					$arr_fifo[Fifo_Stock_Gudang::$QTY]=$row->qty-$selisih;
					$this->db->where(Fifo_Stock_Gudang::$ID,$row->id_fifo);
					$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
					
					if($stock_out<=0){
						break;
					}
				}
				if($stock_out>0)
					$mode=Gudang::$S_CADANGAN;
			}while($stock_out>0);
		}

		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY,0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//Pengecekan stock 0 pada gudang untuk pemindahan material
		$qry_gudang = $this->db->query("select fifo_stock_gudang.id_material id_material,gudang.id_gudang id_gudang,qty,id_fifo from gudang,fifo_stock_gudang where gudang.rak='CADANGAN' and qty>0 and gudang.id_gudang=fifo_stock_gudang.id_gudang");

		$kal="";
		if($qry_gudang->num_rows()>0){
			foreach($qry_gudang->result() as $row){
				$stock_now=$row->qty;
				$id_material=$row->id_material;
				$qry_fifo=$this->db->query("select id_gudang,nama_gudang,id_material,kapasitas,rak,kolom,tingkat from gudang where gudang.id_gudang not in (select id_gudang from fifo_stock_gudang) and gudang.id_material=".$id_material);
				$this->db->where(Material::$ID,$row->id_material);
				$material=$this->db->get(Material::$TABLE_NAME)->row();
				$box=$material->box;
				$product_code=$material->product_code;
				if($qry_fifo->num_rows()>0){
					foreach($qry_fifo->result() as $row2){
						$kapasitas=$row2->kapasitas;
						
						//if($box!=0){
							$kapasitas*=$box;
						//}
						$stock_move=0;
						if ($kapasitas - $stock_now >= 0) { //jika kapasitas masih cukup
							$stock_move = $stock_now;
						} else {
							$stock_move = $kapasitas;
						}

						if($stock_move!=0){
							//Pemindahan Barang dari Cadangan
							$gudang_to = $row2->nama_gudang . ' Rak ' . $row2->rak . $row2->kolom . ' ' . $row2->tingkat;
							$this->insert_stock_gudang($id_material, $row->id_gudang, 0, $stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

							//Pemindahan Barang ke Rak
							$this->insert_stock_gudang($id_material,$row2->id_gudang,$stock_move,0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());

							
							$stock_now -= $stock_move;
							$arr_fifo_move_from = [];
							$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY] = $stock_now;
							$this->db->where(Fifo_Stock_Gudang::$ID, $row->id_fifo);
							$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_from);

							$arr_fifo_move_to = [];
							$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL] = $row2->id_material;
							$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row2->id_gudang;
							$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
							$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_to);

							$qty=$stock_move." tin";

							$kal.=Stock_Gudang::GEN_MOVE_MATERIAL_FROM_CADANGAN_MESSAGE($product_code,$qty,$gudang_to);
						}
					}
				}
			}
		}
		if($kal!=""){
			echo "<script>alert('".$kal."');</script>";
		}
		return H_Good_Issue::$MESSAGE_SUCCESS_INSERT;
	
	}
	public function insert_good_issue_adjustment($ctr,$keterangan,$arr_kode_barang,$arr_kemasan_barang,$arr_dus_barang,$arr_tin_barang,$arr_warna_barang){
		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY,0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//AUTO GEN kode surat jalan
		$max=$this->db->query("select max(substr(id_H_Good_Issue,8)) as max from ".H_Good_Issue::$TABLE_NAME."
								where MONTH(tanggal)=".date("m")." and YEAR(tanggal)=".date("Y"))->row()->max;
		if($max==NULL)
			$max=0;
		$max+=1;
		
		$kode=date("ym").str_pad($max,6,"0",STR_PAD_LEFT);

		//header
		$arr=[];
		$arr[H_Good_Issue::$ID]="GI".$kode;
		$arr[H_Good_Issue::$NIK]=$_SESSION[SESSION_KARYAWAN_PENTA]['nik'];
		
		$kd=strtoupper(H_Good_Issue::$ADJ_CODE);
		$max_adj_code=$this->db->query("select max(substr(nomor_sj,7)) as max from ".H_Good_Issue::$TABLE_NAME."
							where upper(nomor_sj) like '%".$kd."%'")->row()->max;
		if($max_adj_code==null)
			$max_adj_code=0;
		$max_adj_code+=1;
		$arr[H_Good_Issue::$NOMOR_SJ]=$kd.str_pad($max_adj_code,6,"0",STR_PAD_LEFT);
		
		$this->db->set(H_Good_Issue::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Issue::$QTY]=$ctr;
		$arr[$this::$IS_DELETED]=0;
		$this->db->insert(H_Good_Issue::$TABLE_NAME,$arr);
		echo "<script>alert('Kode GI: ".$arr[H_Good_Issue::$ID]."');</script>";
		$counter=1;
		for($i=0;$i<$ctr;$i++){
			$this->db->where(Material::$KEMASAN,$arr_kemasan_barang[$i]);
			$this->db->where(Material::$WARNA,$arr_warna_barang[$i]);
			$this->db->where("Upper(".Material::$KODE_BARANG.")",strtoupper($arr_kode_barang[$i]));
			$material=$this->db->get(Material::$TABLE_NAME)->row_array();
			$id_material=$material[Material::$ID];
			$stock_out=($arr_dus_barang[$i]*$material[Material::$BOX])+$arr_tin_barang[$i];
			$mode="";
			do {
				$this->db->from(Fifo_Stock_Gudang::$TABLE_NAME);
				if (!$this->equals($mode, Gudang::$S_CADANGAN)) {
					$this->db->select("fifo_stock_gudang.id_gudang id_gudang,id_fifo,qty");
					$this->db->join(Gudang::$TABLE_NAME, "fifo_stock_gudang.id_gudang=gudang.id_gudang");
					$this->db->where(Gudang::$RAK . " !=", Gudang::$S_CADANGAN);
				}
				$this->db->where(Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
				$this->db->where(Fifo_Stock_Gudang::$QTY . " !=", 0);

				$qry = $this->db->get();
				foreach($qry->result_array() as $row){
					$selisih=0;
					if($stock_out-$row[Fifo_Stock_Gudang::$QTY]<=0){
						$selisih=$stock_out;
						$stock_out=0;
					}else{
						$selisih=$row[Fifo_Stock_Gudang::$QTY];
						$stock_out-=$selisih;
					}

					//Pengurangan Stock Gudang
					$this->insert_stock_gudang($id_material,$row[Fifo_Stock_Gudang::$ID_GUDANG],0,$selisih, H_Good_Issue::GEN_ADJUSTMENT_MESSAGE($_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID],$arr[H_Good_Issue::$NOMOR_SJ]).", Ket: ".$keterangan);

					$arr_detail=[];
					$arr_detail[D_Good_Issue::$ID]="DGI".$kode.str_pad($counter,"3","0",STR_PAD_LEFT);
					$counter+=1;
					$stock_dus=0;
					$stock_tin=0;
					if($material[Material::$BOX]>1){
						$stock_dus=intdiv($selisih,$material[Material::$BOX]);
						$stock_tin=$selisih%$material[Material::$BOX];
					}else{
						$stock_tin=$selisih;
					}
					$arr_detail[D_Good_Issue::$DUS]=$stock_dus;
					$arr_detail[D_Good_Issue::$TIN]=$stock_tin;
					$arr_detail[D_Good_Issue::$ID_GUDANG]=$row[Fifo_Stock_Gudang::$ID_GUDANG];
					$arr_detail[D_Good_Issue::$ID_MATERIAL]=$id_material;
					$arr_detail[D_Good_Issue::$ID_H_GOOD_ISSUE]=$arr[H_Good_Issue::$ID];
					$this->db->insert(D_Good_Issue::$TABLE_NAME,$arr_detail);

					//Fifo Stock Gudang
					$arr_fifo=[];
					$arr_fifo[Fifo_Stock_Gudang::$QTY]=$row[Fifo_Stock_Gudang::$QTY]-$selisih;
					$this->db->where(Fifo_Stock_Gudang::$ID,$row[Fifo_Stock_Gudang::$ID]);
					$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME,$arr_fifo);
					
					if($stock_out<=0){
						break;
					}
				}
				if ($stock_out > 0)
					$mode = Gudang::$S_CADANGAN;
			}while($stock_out>0);
		}

		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY,0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//Pengecekan stock 0 pada gudang untuk pemindahan material
		$qry_gudang = $this->db->query("select fifo_stock_gudang.id_material id_material,gudang.id_gudang id_gudang,qty,id_fifo from gudang,fifo_stock_gudang where gudang.rak='CADANGAN' and qty>0 and gudang.id_gudang=fifo_stock_gudang.id_gudang");

		$kal="";
		if($qry_gudang->num_rows()>0){
			foreach($qry_gudang->result() as $row){
				$stock_now=$row->qty;
				$id_material=$row->id_material;
				$qry_fifo=$this->db->query("select id_gudang,nama_gudang,id_material,kapasitas,rak,kolom,tingkat from gudang where gudang.id_gudang not in (select id_gudang from fifo_stock_gudang) and gudang.id_material=".$id_material);
				$this->db->where(Material::$ID,$row->id_material);
				$material=$this->db->get(Material::$TABLE_NAME)->row();
				$box=$material->box;
				$product_code=$material->product_code;
				if($qry_fifo->num_rows()>0){
					foreach($qry_fifo->result() as $row2){
						$kapasitas=$row2->kapasitas;
						
						//if($box!=0){
							$kapasitas*=$box;
						//}
						$stock_move=0;
						if ($kapasitas - $stock_now >= 0) { //jika kapasitas masih cukup
							$stock_move = $stock_now;
						} else {
							$stock_move = $kapasitas;
						}

						//Pemindahan Barang dari Cadangan
						$gudang_to = $row2->nama_gudang . ' Rak ' . $row2->rak . $row2->kolom . ' ' . $row2->tingkat;
						$this->insert_stock_gudang($id_material, $row->id_gudang,0, $stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

						//Pemindahan Barang ke Rak
						$this->insert_stock_gudang($id_material, $row2->id_gudang, $stock_move, 0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());

						$stock_now -= $stock_move;
						$arr_fifo_move_from = [];
						$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY] = $stock_now;
						$this->db->where(Fifo_Stock_Gudang::$ID, $row->id_fifo);
						$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_from);

						$arr_fifo_move_to = [];
						$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL] = $row2->id_material;
						$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row2->id_gudang;
						$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
						$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_to);

						$qty=$stock_move." tin";

						$kal.=Stock_Gudang::GEN_MOVE_MATERIAL_FROM_CADANGAN_MESSAGE($product_code,$qty,$gudang_to);
					}

				}
			}
		}
		if($kal!=""){
			echo "<script>alert('".$kal."');</script>";
		}
		return H_Good_Issue::$MESSAGE_SUCCESS_ADJUSTMENT;
	
	}
	public function get_good_issue_by_sj($id_sj){
		$arr=[H_Good_Issue::$KODE_REVISI,H_Good_Issue::$NIK_SOPIR,H_Good_Issue::$NIK_KERNET,
				H_Good_Issue::$TANGGAL_CONFIRM_ADMIN,H_Good_Issue::$ID_EKSPEDISI,H_Good_Issue::$NIK,
				Material::$KODE_BARANG,Material::$KEMASAN,Material::$WARNA,Material::$DESCRIPTION,
				H_Good_Issue::$TABLE_NAME.".".H_Good_Issue::$ID." ".H_Good_Issue::$ID,H_Good_Issue::$NOMOR_SJ,
				H_Good_Issue::$STO,H_Good_Issue::$EKSPEDISI,H_Good_Issue::$QTY,H_Good_Issue::$TANGGAL,
				Material::$TABLE_NAME.".".Material::$PRODUCT_CODE." ".Material::$PRODUCT_CODE,
				D_Good_Issue::$DUS,D_Good_Issue::$TIN,D_Good_Issue::$ID_GUDANG];
		$this->db->select($arr);
		$this->db->from(H_Good_Issue::$TABLE_NAME);
		$temp=H_Good_Issue::$TABLE_NAME.".".H_Good_Issue::$ID."=".D_Good_Issue::$TABLE_NAME.".".D_Good_Issue::$ID_H_GOOD_ISSUE;
		$this->db->join(D_Good_Issue::$TABLE_NAME, $temp);
		$temp=D_Good_Issue::$TABLE_NAME.".".D_Good_Issue::$ID_MATERIAL."=".Material::$TABLE_NAME.".".Material::$ID;
		$this->db->join(Material::$TABLE_NAME,$temp);
		$temp=H_Good_Issue::$TABLE_NAME.".".$this::$IS_DELETED."=0";
		$this->db->where($temp);
		$this->db->where(H_Good_Issue::$NOMOR_SJ,$id_sj);
		return $this->db->get();
		/*return $this->db->query("
			select kode_revisi,nik_sopir,nik_kernet,tanggal_confirm_admin,id_ekspedisi,nik,kode_barang,kemasan,warna,
			description,h.id_h_good_issue id,nomor_sj,sto,ekspedisi,tanggal,qty,d.product_code p_code,dus,
			tin,id_gudang
			from h_good_issue h,d_good_issue d,material m
			where h.id_h_good_issue=d.id_h_good_issue and
				h.is_deleted=0 and (nomor_sj='$id_sj') and
				m.product_code=d.product_code");*/
	}
	public function get_good_issue_by_tanggal($dt,$st){
		$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN.' is not NULL',NULL,FALSE);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . ' <', $until, TRUE);
		}
		return $this->db->get(H_Good_Issue::$TABLE_NAME);
	}
	public function good_issue_get_all_rekap_sj($dt,$st,$n,$kb,$k,$w){
		/*
		SELECT h_good_receipt.id_h_good_receipt,nomor_sj,tanggal,nik,material.id_material,material.box,sum(dus),sum(tin)
		FROM `d_good_receipt`,h_good_receipt,material
		where d_good_receipt.id_h_good_receipt=h_good_receipt.id_h_good_receipt AND
		d_good_receipt.id_material_fg=material.id_material
		group by h_good_receipt.id_h_good_receipt,nomor_sj,tanggal,nik,material.id_material
		*/
		$select=array(
			H_Good_Issue::$TABLE_NAME.".".H_Good_Issue::$ID." ". H_Good_Issue::$ID,
			H_Good_Issue::$NOMOR_SJ,
			H_Good_Issue::$TANGGAL,
			H_Good_Issue::$NIK,
			"sum(".D_Good_Issue::$DUS.")"." ". D_Good_Issue::$DUS,
			"sum(".D_Good_Issue::$TIN. ")" . D_Good_Issue::$TIN,
			D_Good_Issue::$ID_MATERIAL,
			H_Good_Issue::$TANGGAL_CONFIRM_ADMIN,
			Material::$BOX,
		);
		$group_by = array(
			H_Good_Issue::$ID,
			H_Good_Issue::$NOMOR_SJ,
			H_Good_Issue::$TANGGAL,
			H_Good_Issue::$NIK,
			D_Good_Issue::$ID_MATERIAL,
			H_Good_Issue::$TANGGAL_CONFIRM_ADMIN,
			Material::$BOX,
		);
		$this->db->select($select);
		$this->db->from(D_Good_Issue::$TABLE_NAME);
		$join = D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_H_GOOD_ISSUE . "=" . H_Good_Issue::$TABLE_NAME . "." . H_Good_Issue::$ID;
		$this->db->join(H_Good_Issue::$TABLE_NAME, $join);
		$join = D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_MATERIAL  . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Issue::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Issue::$TANGGAL . ' <', $until, TRUE);
		}
		if ($n != NULL) {
			$this->db->where(H_Good_Issue::$NOMOR_SJ, $n);
		}
		if ($kb != NULL) {
			$this->db->where(Material::$KODE_BARANG,$kb);
		}
		if ($k != NULL) {
			$this->db->where(Material::$KEMASAN, $k);
		}
		if ($w != NULL) {
			$this->db->where(Material::$WARNA, $w);
		}
		$this->db->group_by($group_by);
		//$data[H_Good_Receipt::$TABLE_NAME]=$this->get(H_Good_Receipt::ta)
		return $this->db->get();
	}

	//Gudang
	public function gudang_update_batch($data){
		$kal="";
		$ctr=0;
		for($i=0;$i<count($data['values'])+10;$i++){
			if(isset($data['values'][$i])){
				if($data['values'][$i]["A"]!=""){ //jika gudang no tidak kosong
					$rak=isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$kolom=isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$tingkat=isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$kapasitas=isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					$product_code=isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					
					if($kolom!=""){
						$ctr=$kolom;
					}
					$kolom=$ctr;
					$nama="A7";
					$qry=$this->get(Material::$TABLE_NAME,Material::$PRODUCT_CODE,$product_code);
					$id_material="";
					if($qry->num_rows()>0){
						$id_material=$qry->row()->id_material;
					}else{
						$kal.=$product_code."-";
					}
					$data_query=array(
						Gudang::$KAPASITAS=>$kapasitas,
						$this::$IS_DELETED=>0
					);
					if($id_material!=""){
						$data_query[Gudang::$ID_MATERIAL]=$id_material;
					}
					
					//INSERT GUDANG
					/*$this->db->where(Gudang::$NAMA_GUDANG,$nama);
					$this->db->where(Gudang::$RAK,$rak);
					$this->db->where(Gudang::$KOLOM,$kolom);
					$this->db->where(Gudang::$TINGKAT,$tingkat);
					$qry=$this->db->get(Gudang::$TABLE_NAME);*/
					//if($qry->num_rows()==0){
						$data_query[Gudang::$NAMA_GUDANG]=$nama;
						$data_query[Gudang::$RAK]=$rak;
						$data_query[Gudang::$KOLOM]=$kolom;
						$data_query[Gudang::$TINGKAT]=$tingkat;
						$this->db->insert(Gudang::$TABLE_NAME,$data_query);	
					//}else{
						/*$this->db->where(Gudang::$NAMA_GUDANG,$nama);
						$this->db->where(Gudang::$RAK,$rak);
						$this->db->where(Gudang::$KOLOM,$kolom);
						$this->db->where(Gudang::$TINGKAT,$tingkat);
						$this->db->update(Gudang::$TABLE_NAME,$data_query);*/
					//}
					
				}else
					break;
			}
			
		}
		return $kal;
	}
	public function get_gudang_distinct($field){
		$this->db->distinct();
		$this->db->select($field." as $field");
		return $this->db->get(Gudang::$TABLE_NAME);
	}

	//Gudang RM PM
	public function gudang_rm_pm_update_batch($data){
		$kal = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$kode_material = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$nama_gudang = isset($data['values'][$i]["H"]) ? $data['values'][$i]["H"] : NULL;


					$data_query = array(
							Gudang_RM_PM::$NAMA_GUDANG => $nama_gudang,
							$this::$IS_DELETED => 0,
						);

					$qry = $this->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$KODE_MATERIAL, $kode_material);


					if ($qry->num_rows() > 0) {
						$data_query[Gudang_RM_PM::$ID_MATERIAL_RM_PM] = $qry->row_array()[Material_RM_PM::$ID];
					}
					//INSERT Gudang RM PM
					$this->db->insert(Gudang_RM_PM::$TABLE_NAME, $data_query);
				} else
					break;
			}
		}
		return $kal;
	}

	//Stock FG
	public function get_history_stock($n,$dt,$st,$kb,$k,$w,$g){
		$arr=[Stock_Gudang::$ID,
				Stock_Gudang::$TABLE_NAME.".".Stock_Gudang::$ID_MATERIAL." ".Stock_Gudang::$ID_MATERIAL,
				Stock_Gudang::$TANGGAL,Gudang::$TABLE_NAME.".".Gudang::$ID." ".Gudang::$ID,
				Stock_Gudang::$STOCK_AWAL,Stock_Gudang::$DEBIT,Stock_Gudang::$KREDIT,Stock_Gudang::$STOCK_AKHIR,
				Stock_Gudang::$PESAN];
		$this->db->select($arr);
		$this->db->from(Stock_Gudang::$TABLE_NAME);
		$this->db->join(Material::$TABLE_NAME, "stock_gudang.id_material=material.id_material");
		$this->db->join(Gudang::$TABLE_NAME, "gudang.id_gudang=stock_gudang.id_gudang");
		if($dt!=null){
			$until=date("Y-m-d H:i:s",strtotime($st. "+ 1 day"));
			$this->db->where(Stock_Gudang::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Stock_Gudang::$TANGGAL . ' <', $until, TRUE);
		}
		if($n!=null){
			$this->db->like(Stock_Gudang::$PESAN, $n, 'both');
		}
		if($kb!=null)
			$this->db->where(Material::$KODE_BARANG,strtoupper($kb));
		if($k!=null)
			$this->db->where(Material::$KEMASAN, strtoupper($k));
		if($w!=null)
			$this->db->where(Material::$WARNA, strtoupper($w));
		if($g!=null)
			$this->db->where(Gudang::$NAMA_GUDANG, strtoupper($g));
		
		return $this->db->get();
	}
	public function get_history_stock_opname($dt,$st){
		$arr=[Material_Fisik::$ID,Material_Fisik::$TANGGAL,
				Material_Fisik::$TABLE_NAME.".".Material_Fisik::$ID_MATERIAL." ".Material::$ID,
				Material_Fisik::$TABLE_NAME.".".Material_Fisik::$NIK." ".Material_Fisik::$NIK,
				Karyawan::$NAMA,Material::$PRODUCT_CODE,Material_Fisik::$QTY_PROGRAM,Material_Fisik::$QTY_FISIK];
		$this->db->select($arr);
		$this->db->from(Material_Fisik::$TABLE_NAME);
		$temp = Material_Fisik::$TABLE_NAME . "." . Material_Fisik::$ID_MATERIAL . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $temp);
		$temp = Material_Fisik::$TABLE_NAME . "." . Material_Fisik::$NIK . "=" . Karyawan::$TABLE_NAME . "." . Karyawan::$ID;
		$this->db->join(Karyawan::$TABLE_NAME, "material_fisik.nik=karyawan.nik");
		if($dt!=null){
			$until=date("Y-m-d H:i:s",strtotime($st. "+ 1 day"));
			$this->db->where(Material_Fisik::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Material_Fisik::$TANGGAL . ' <', $until, TRUE);
		}
		
		return $this->db->get();
	}
	public function get_stock_by($id_material,$id_gudang = null){
		/*$qry=$this->db->query("select sum(stock_akhir) as stock_akhir
								from stock_gudang,(select id_material,id_gudang,max(id_stock_gudang) id_stock_gudang from stock_gudang group by id_material,id_gudang) last_stock
								where stock_gudang.id_stock_gudang=last_stock.id_stock_gudang and  stock_gudang.id_material=$id
								group by stock_gudang.id_material");
		*/
		$this->db->select("sum(qty) as stock_akhir");
		$this->db->where(Fifo_Stock_Gudang::$QTY." !=",0);
		$this->db->where(Fifo_Stock_Gudang::$ID_MATERIAL,$id_material);
		if($id_gudang!=null){
			$this->db->where(Fifo_Stock_Gudang::$ID_GUDANG,$id_gudang);
		}
		$this->db->group_by(Fifo_Stock_Gudang::$ID_MATERIAL,$id_material);
		$qry=$this->db->get(Fifo_Stock_Gudang::$TABLE_NAME);
		if($qry->num_rows()>0){
			return $qry->row_array()[Stock_Gudang::$STOCK_AKHIR];
		}
		return 0;
	}
	public function insert_stock_gudang($id_material,$id_gudang,$debit,$kredit,$pesan){
		$arr_stock = [];
		$arr_stock[$this::$IS_DELETED] = 0;
		$arr_stock[Stock_Gudang::$ID_GUDANG] = $id_gudang;
		$arr_stock[Stock_Gudang::$STOCK_AWAL] = $this->get_stock_by($id_material, $id_gudang);
		$arr_stock[Stock_Gudang::$DEBIT] = $debit;
		$arr_stock[Stock_Gudang::$KREDIT] = $kredit;
		$arr_stock[Stock_Gudang::$ID_MATERIAL] = $id_material;
		$arr_stock[Stock_Gudang::$STOCK_AKHIR] = $arr_stock[Stock_Gudang::$STOCK_AWAL] + $arr_stock[Stock_Gudang::$DEBIT] - $arr_stock[Stock_Gudang::$KREDIT];
		$arr_stock[Stock_Gudang::$PESAN] = $pesan;
		$this->db->set(Stock_Gudang::$TANGGAL, 'NOW()', FALSE);
		$this->db->insert(Stock_Gudang::$TABLE_NAME, $arr_stock);
	}
	public function get_fisik_stock_by($id_material){
		/*$this->db->where(Material_Fisik::$ID_MATERIAL,$id_material);
		$this->db->order_by(Material_Fisik::$ID,"DESC");
		$qry = $this->db->get(Material_Fisik::$TABLE_NAME);*/
		$qry = $this->get(Material_Fisik::$TABLE_NAME, Material_Fisik::$ID_MATERIAL, $id_material, Material_Fisik::$ID, "DESC");
		if ($qry->num_rows() > 0) {
			return $qry->row_array()[Material_Fisik::$QTY_FISIK];
		}
		return 0;
	}
	public function insert_material_fisik($ctr,$arr_kode,$arr_kemasan,$arr_stock_fisik,$arr_warna){
		for($i=0;$i<$ctr;$i++){
			$this->db->where(Material::$KODE_BARANG,$arr_kode[$i]);
			$this->db->where(Material::$KEMASAN,$arr_kemasan[$i]);
			$this->db->where(Material::$WARNA,$arr_warna[$i]);
			$qry_material=$this->db->get(Material::$TABLE_NAME);
			$material=$qry_material->row_array();
			$id_material=$material[Material::$ID];
			
			$arr_material_fisik=[];
			$this->db->where(Material_Fisik::$ID_MATERIAL,$id_material);
			$arr_material_fisik[Material_Fisik::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
			$arr_material_fisik[Material_Fisik::$QTY_FISIK] = $arr_stock_fisik[$i];
			$arr_material_fisik[Material_Fisik::$QTY_PROGRAM] = $this->get_stock_by($id_material);
			$this->db->set(Material_Fisik::$TANGGAL, 'NOW()', FALSE);
			$arr_material_fisik[Material_Fisik::$ID_MATERIAL] = $id_material;
			$this->db->insert(Material_Fisik::$TABLE_NAME,$arr_material_fisik);
		}
		return Material_Fisik::$MESSAGE_SUCCESS_INSERT;

	}

	//Stock RM PM
	//done
	public function insert_history_stock_rm_pm($no_po_ws,$nomor_sj,$tanggal,$id_material,$id_vendor,$debit,$kredit,$pesan){
		$arr_stock = [];
		$arr_stock[$this::$IS_DELETED] = 0;
		$arr_stock[Stock_Gudang_RM_PM::$ID_VENDOR] = $id_vendor;
		$arr_stock[Stock_Gudang_RM_PM::$TANGGAL] = $tanggal;
		$arr_stock[Stock_Gudang_RM_PM::$STOCK_AWAL] = $this->get_stock_rm_pm_by($id_material,$id_vendor);
		$arr_stock[Stock_Gudang_RM_PM::$DEBIT] = $debit;
		$arr_stock[Stock_Gudang_RM_PM::$KREDIT] = $kredit;
		$arr_stock[Stock_Gudang_RM_PM::$ID_MATERIAL] = $id_material;
		$arr_stock[Stock_Gudang_RM_PM::$STOCK_AKHIR] = $arr_stock[Stock_Gudang_RM_PM::$STOCK_AWAL] + $arr_stock[Stock_Gudang_RM_PM::$DEBIT] - $arr_stock[Stock_Gudang_RM_PM::$KREDIT];
		$arr_stock[Stock_Gudang_RM_PM::$PESAN] = $pesan;
		$arr_stock[Stock_Gudang_RM_PM::$NOMOR_PO_WS] = $no_po_ws;
		$arr_stock[Stock_Gudang_RM_PM::$NOMOR_SJ] = $nomor_sj;

		$this->db->insert(Stock_Gudang_RM_PM::$TABLE_NAME, $arr_stock);
	}
	//done
	public function get_stock_rm_pm_by($id_material,$id_vendor = null){
		$this->db->select("sum(qty) as stock_akhir");
		$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL,$id_material);
		$data = array(Material_RM_PM_Stock::$ID_MATERIAL);
		if($id_vendor!=null){
			$this->db->where(Material_RM_PM_Stock::$ID_VENDOR,$id_vendor);
			$data[1]=Material_RM_PM_Stock::$ID_VENDOR;
		}
		$this->db->group_by($data);
		$qry=$this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
		if($qry->num_rows()>0){
			return $qry->row_array()[Material_RM_PM_Stock::$S_STOCK_AKHIR];
		}else{//jika vendor di stock null tapi di po awal ada, tetap digunakan stock dari material tsb
			$this->db->select("sum(qty) as stock_akhir");
			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $id_material);
			$this->db->group_by(Material_RM_PM_Stock::$ID_MATERIAL);
			$qry = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($qry->num_rows() > 0) {
				return $qry->row_array()[Material_RM_PM_Stock::$S_STOCK_AKHIR];
			}
		}
		return 0;
	}
	
	public function get_history_stock_rm_pm($dt,$st,$n=null,$m=null){
		$arr=[Stock_Gudang_RM_PM::$ID,
				Stock_Gudang_RM_PM::$TABLE_NAME.".".Stock_Gudang_RM_PM::$ID_MATERIAL." ".Stock_Gudang_RM_PM::$ID_MATERIAL,
				Stock_Gudang_RM_PM::$TANGGAL, Material_RM_PM::$KODE_PANGGIL, Stock_Gudang_RM_PM::$ID_VENDOR, 
				Stock_Gudang_RM_PM::$NOMOR_SJ,
				Stock_Gudang_RM_PM::$STOCK_AWAL, Stock_Gudang_RM_PM::$DEBIT, Stock_Gudang_RM_PM::$KREDIT,
				Stock_Gudang_RM_PM::$STOCK_AKHIR,Stock_Gudang_RM_PM::$PESAN];
		$this->db->select($arr);
		$this->db->from(Stock_Gudang_RM_PM::$TABLE_NAME);
		$this->db->join(Material_RM_PM::$TABLE_NAME,"material_rm_pm.id_material_rm_pm=stock_gudang_rm_pm.id_material_rm_pm");
		
		if($dt!=null){
			$until=date("Y-m-d H:i:s",strtotime($st. "+ 1 day"));
			$this->db->where(Stock_Gudang_RM_PM::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Stock_Gudang_RM_PM::$TANGGAL . ' <', $until, TRUE);
		}
		if($n!=null){
			$this->db->where(Stock_Gudang_RM_PM::$NOMOR_SJ,$n);
		}
		if ($m != null) {
			$this->db->where(Stock_Gudang_RM_PM::$TABLE_NAME.".".Stock_Gudang_RM_PM::$ID_MATERIAL, $m);
		}
		$qry=$this->db->get();
		//echo print_r($this->db->last_query());  
		return $qry;
	}
	
	//Kinerja
	public function get_kinerja_by($dt,$st,$nik,$code){
		$data=[];
		$data[KINERJA_KEY_TIPE]="OUTSOURCE";
		$data[KINERJA_KEY_LCL]=0;
		$data[KINERJA_KEY_CONTAINER]=0;
		$data[KINERJA_KEY_PAIL]=0;
		$data[KINERJA_KEY_NON_PAIL]=0;

		//LCL CONTAINER
		$arr_lcl_container=array(KINERJA_KEY_LCL,KINERJA_KEY_CONTAINER);
		for($i=0;$i<count($arr_lcl_container);$i++){
			$this->db->from(H_Good_Issue::$TABLE_NAME);
			$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN.' is not NULL',NULL,FALSE);
			if($code!=NULL){
				$this->db->where(H_Good_Issue::$ID, $code);
			}
			if($nik!=NULL){ //jika tidak kosong maka minta nik
				$qry=$this->get_karyawan_by(Karyawan::$ID,$nik);
				if($qry->num_rows()>0){
					$karyawan=$qry->row_array();
					$data[KINERJA_KEY_TIPE]=$karyawan[Karyawan::$S_K_NAMA];
					if($this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_SOPIR)) //jika karyawan tsb adalah sopir
						$this->db->where(H_Good_Issue::$NIK_SOPIR,$nik);	
					else if($this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_KERNET)) //jika karyawan tsb adalah kernet
					$this->db->where(H_Good_Issue::$NIK_KERNET,$nik);	
					else if($this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_GENERAL_SOPIR_FK)|| $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_SOPIR_FK)){} //jika karyawan tsb adalah sopir fk
						//$this->db->where(H_Good_Issue::$NIK_SOPIR,$nik);	
					if($this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_GENERAL_CHECKER)|| $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_CHECKER)) //jika karyawan tsb adalah checker
						$this->db->where(H_Good_Issue::$NIK,$nik);	
				}
			}
			if ($dt != NULL && $st != NULL) {
				$until = date("Y-m-d", strtotime($st . "+ 1 day"));
				$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . ' >=', $dt, TRUE);
				$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . ' <', $until, TRUE);
			}
			$this->db->where(H_Good_Issue::$REV_GOOD_RECEIPT.' is NULL',NULL,FALSE);
			$this->db->where("upper(".H_Good_Issue::$EKSPEDISI.")",strtoupper($arr_lcl_container[$i]));
			$qry=$this->db->get();
			$data[$arr_lcl_container[$i]]=$this->kinerja_calculate_container_lcl($qry);
		}

		//PAIL & NON PAIL
		$this->db->from(H_Good_Receipt::$TABLE_NAME);
		$this->db->where(H_Good_Receipt::$REV_GOOD_ISSUE . ' is NULL', NULL, FALSE);
		$this->db->where(H_Good_Receipt::$TANGGAL . ' is NOT NULL', NULL, FALSE);
		if($code!=NULL){
			$this->db->where(H_Good_Receipt::$ID, $code);
		}
		if($dt!=NULL && $st!=NULL){
			$until=date("Y-m-d",strtotime($st. "+ 1 day"));
			$this->db->where(H_Good_Receipt::$TANGGAL.' >=', $dt, TRUE);
			$this->db->where(H_Good_Receipt::$TANGGAL.' <', $until, TRUE);
		}
		if($nik!=NULL){
			$qry = $this->get_karyawan_by(Karyawan::$ID, $nik);
			if ($qry->num_rows() > 0) {
				if($this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_GENERAL_CHECKER)|| $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_CHECKER)) //jika karyawan tsb adalah checker
					$this->db->where(H_Good_Issue::$NIK,$nik);	
			}
		}
		$qry_h_good_receipt=$this->db->get();
		$temp=$this->kinerja_calculate_pail_nonpail($qry_h_good_receipt);
		$data[KINERJA_KEY_PAIL]=$temp[KINERJA_KEY_PAIL];
		$data[KINERJA_KEY_NON_PAIL]=$temp[KINERJA_KEY_NON_PAIL];

		//Pembalikkan nilai 0 untuk NIK sesuai bagiannya
		//container=a,lcl=b,pail=c,nonpail=d 
		//outsource=a b c d
		//sopir=b
		//kernet=b
		//checker=a b c d
		//sopir fk=a b d
		if($nik!=NULL){ 
			$qry=$this->get_karyawan_by(Karyawan::$ID,$nik);
			if($qry->num_rows()>0){
				$data[KINERJA_KEY_TIPE]=$qry->row()->k_nama;
				if($this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_SOPIR)||$this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_KERNET)){
					$data[KINERJA_KEY_CONTAINER]=0;
					$data[KINERJA_KEY_PAIL]=0;
					$data[KINERJA_KEY_NON_PAIL]=0;
				}
				else if($this->equals($karyawan[Sub_Kategori::$ID],KARYAWAN_SUB_KATEGORI_GENERAL_SOPIR_FK)|| $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_SOPIR_FK)){
					$data[KINERJA_KEY_PAIL]=0;
				}
			}
		}

		return $data;
	}	
	/*
	public function kinerja_calculate_container_lcl($qry_h_good_issue){
		$total_in_kg=0;
		if($qry_h_good_issue->num_rows()>0){
			foreach($qry_h_good_issue->result_array() as $row){
				$this->db->where(D_Good_Issue::$ID_H_GOOD_ISSUE,$row[H_Good_Issue::$ID]);
				$qry_d_good_issue=$this->db->get(D_Good_Issue::$TABLE_NAME);
				if($qry_d_good_issue->num_rows()>0){
					foreach($qry_d_good_issue->result_array() as $row2){
						$this->db->where(Material::$ID, $row2[D_Good_Issue::$ID_MATERIAL]);
						$material = $this->db->get(Material::$TABLE_NAME)->row_array();
						$gross = $material[Material::$GROSS];
						$box = $material[Material::$BOX];

						//cek dulu apakah ada revisi sj masuk atas barang tsb
						$this->db->where(H_Good_Receipt::$REV_GOOD_ISSUE,1);
						$this->db->where(H_Good_Receipt::$NOMOR_SJ,$row[H_Good_Issue::$NOMOR_SJ]);
						$qry_rev_h_good_receipt=$this->db->get(H_Good_Receipt::$TABLE_NAME);
						if($qry_rev_h_good_receipt->num_rows()>0){ //jika ada
							foreach ($qry_rev_h_good_receipt->result_array() as $row3) {
								$this->db->where(D_Good_Receipt::$ID_H_GOOD_RECEIPT, $row3[H_Good_Receipt::$ID]);
								$qry_d_good_receipt = $this->db->get(D_Good_Receipt::$TABLE_NAME);
								if ($qry_d_good_receipt->num_rows() > 0) {
									foreach ($qry_d_good_receipt->result_array() as $row4) {
										if($this->equals($row2[D_Good_Issue::$ID_MATERIAL],$row4[D_Good_Receipt::$ID_MATERIAL])){
											$total_in_kg -= ((($row4[D_Good_Receipt::$DUS] * $box) + $row4[D_Good_Receipt::$TIN]) * $gross);
										}
									}
								}
							}
						}
						$total_in_kg+=((($row2[D_Good_Issue::$DUS]*$box)+$row2[D_Good_Issue::$TIN])*$gross);
					}
				}
			}
		}
		return $total_in_kg;
	}*/
	public function kinerja_calculate_container_lcl($qry_h_good_issue){
		$total_in_kg=0;
		$arr_id_material_done_minus=[];
		if($qry_h_good_issue->num_rows()>0){
			foreach($qry_h_good_issue->result_array() as $row){
				$this->db->where(D_Good_Issue::$ID_H_GOOD_ISSUE,$row[H_Good_Issue::$ID]);
				$qry_d_good_issue=$this->db->get(D_Good_Issue::$TABLE_NAME);
				if($qry_d_good_issue->num_rows()>0){
					foreach($qry_d_good_issue->result_array() as $row2){
						$this->db->where(Material::$ID, $row2[D_Good_Issue::$ID_MATERIAL]);
						$material = $this->db->get(Material::$TABLE_NAME)->row_array();
						$gross = $material[Material::$GROSS];
						$box = $material[Material::$BOX];
						$temp_id_material="";

						//cek dulu apakah ada revisi sj masuk atas barang tsb
						$this->db->where(H_Good_Receipt::$REV_GOOD_ISSUE,1);
						$this->db->where(H_Good_Receipt::$NOMOR_SJ,$row[H_Good_Issue::$NOMOR_SJ]);
						$qry_rev_h_good_receipt=$this->db->get(H_Good_Receipt::$TABLE_NAME);
						if($qry_rev_h_good_receipt->num_rows()>0){ //jika ada
							foreach ($qry_rev_h_good_receipt->result_array() as $row3) {
								$this->db->where(D_Good_Receipt::$ID_MATERIAL, $row2[D_Good_Issue::$ID_MATERIAL]);
								$this->db->where(D_Good_Receipt::$ID_H_GOOD_RECEIPT, $row3[H_Good_Receipt::$ID]);
								$qry_d_good_receipt = $this->db->get(D_Good_Receipt::$TABLE_NAME);
								if ($qry_d_good_receipt->num_rows() > 0) {
									foreach ($qry_d_good_receipt->result_array() as $row4) {
										$temp_id_material=$row2[D_Good_Issue::$ID_MATERIAL];
										if(!in_array($temp_id_material,$arr_id_material_done_minus)){
											
											if($this->equals($row2[D_Good_Issue::$ID_MATERIAL],$row4[D_Good_Receipt::$ID_MATERIAL])){
												$total_in_kg -= ((($row4[D_Good_Receipt::$DUS] * $box) + $row4[D_Good_Receipt::$TIN]) * $gross);
											}
												
										}
									}
								}
								if(!in_array($temp_id_material,$arr_id_material_done_minus)){
									array_push($arr_id_material_done_minus,$temp_id_material);
								}
							}
						}
						$total_in_kg+=((($row2[D_Good_Issue::$DUS]*$box)+$row2[D_Good_Issue::$TIN])*$gross);
					}
				}
			}
		}
		
		return $total_in_kg;
	}
	public function kinerja_calculate_pail_nonpail($qry_h_good_receipt){
		$data=[];
		$data[KINERJA_KEY_PAIL]=0;
		$data[KINERJA_KEY_NON_PAIL]=0;
		if($qry_h_good_receipt->num_rows()>0){
			foreach($qry_h_good_receipt->result_array() as $row){
				$this->db->where(D_Good_Receipt::$ID_H_GOOD_RECEIPT,$row[H_Good_Receipt::$ID]);
				$qry_d_good_receipt=$this->db->get(D_Good_Receipt::$TABLE_NAME);
				if($qry_d_good_receipt->num_rows()>0){
					foreach($qry_d_good_receipt->result_array() as $row2){
						$this->db->where(Material::$ID,$row2[D_Good_Receipt::$ID_MATERIAL]);
						$material=$this->db->get(Material::$TABLE_NAME)->row_array();
						$gross=$material[Material::$GROSS];
						$box=$material[Material::$BOX];

						//cek dulu apakah ada revisi sj keluar atas barang tsb
						$this->db->where(H_Good_Issue::$REV_GOOD_RECEIPT, 1);
						$this->db->where(H_Good_Issue::$NOMOR_SJ, $row[H_Good_Receipt::$NOMOR_SJ]);
						$qry_rev_h_good_issue = $this->db->get(H_Good_Issue::$TABLE_NAME);
						if ($qry_rev_h_good_issue->num_rows() > 0) { //jika ada
							foreach ($qry_rev_h_good_issue->result_array() as $row3) {
								$this->db->where(D_Good_Issue::$ID_H_GOOD_ISSUE, $row3[H_Good_Issue::$ID]);
								$qry_d_good_issue = $this->db->get(D_Good_Issue::$TABLE_NAME);
								if ($qry_d_good_issue->num_rows() > 0) {
									foreach ($qry_d_good_issue->result_array() as $row4) {
										if ($this->equals($row2[D_Good_Receipt::$ID_MATERIAL], $row4[D_Good_Issue::$ID_MATERIAL])) {
											if ($box <= 1) //pail
												$data[KINERJA_KEY_PAIL] -= (($row4[D_Good_Issue::$TIN]) * $gross);
											else
												$data[KINERJA_KEY_NON_PAIL] -= ((($row4[D_Good_Issue::$DUS] * $box) + $row4[D_Good_Issue::$TIN]) * $gross);
										}
									}
								}
							}
						}

						if($box<=1) //pail
							$data[KINERJA_KEY_PAIL]+=(($row2[D_Good_Receipt::$TIN])*$gross);
						else
							$data[KINERJA_KEY_NON_PAIL]+=((($row2[D_Good_Receipt::$DUS]*$box)+$row2[D_Good_Receipt::$TIN])*$gross);
					}
				}
			}
		}
		return $data;
	}
	public function kinerja_get_all_distinct_nomor_sj_good_issue(){
		$this->db->select("distinct(".H_Good_Issue::$NOMOR_SJ.") as nomor_sj,id_ekspedisi");
		//$this->db->where(H_Good_Issue::$REV_GOOD_RECEIPT." is NULL", NULL, FALSE);
		//$this->db->or_where(H_Good_Issue::$REV_GOOD_RECEIPT,0); //artinya benar" dibuat di surat jalan keluar
		return $this->db->get(H_Good_Issue::$TABLE_NAME);
	}
	public function kinerja_insert_good_issue($sj,$tanggal,$id_ekspedisi,$tipe_ekspedisi,$sopir,$kernet){
		$this->db->where(H_Good_Issue::$NOMOR_SJ,$sj);
		$data=[];
		$data[H_Good_Issue::$NIK_SOPIR]=NULL;
		$data[H_Good_Issue::$NIK_KERNET]=NULL;
		$data[H_Good_Issue::$NIK_ADMIN] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];

		if($tipe_ekspedisi!=null)
			$data[H_Good_Issue::$EKSPEDISI]=strtoupper($tipe_ekspedisi);

		if($id_ekspedisi!=null)
			$data[H_Good_Issue::$ID_EKSPEDISI]=$id_ekspedisi;
		
		if($sopir!=-1)
			$data[H_Good_Issue::$NIK_SOPIR]=$sopir;

		if($kernet!=-1)	
			$data[H_Good_Issue::$NIK_KERNET]=$kernet; 

		if($tanggal!=null)
			$data[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN]=$tanggal;

		if($this->equals($data[H_Good_Issue::$EKSPEDISI],KINERJA_KEY_CONTAINER)){
			$data[H_Good_Issue::$NIK_KERNET]=NULL; 
			$data[H_Good_Issue::$NIK_SOPIR]=NULL; 
		}
		
		$this->db->update(H_Good_Issue::$TABLE_NAME,$data);
		//if($this->db->affected_rows() > 0)
			return H_Good_Issue::$MESSAGE_SUCCESS_UPDATE_KINERJA;
		//return H_Good_Issue::$MESSAGE_FAILED_UPDATE_KINERJA;
	}

	//Ekspedisi
	public function ekspedisi_update_batch($data){
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika gudang no tidak kosong
					$nama = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$inisial = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$tujuan = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$is_lcl = isset($data['values'][$i]["D"]) ? 1 : 0;
					$is_container = isset($data['values'][$i]["E"]) ? 1 : 0;
					$is_m3 = isset($data['values'][$i]["F"]) ? 1 : 0;
					$is_kilo = isset($data['values'][$i]["G"]) ? 1 : 0;
					$is_collie = isset($data['values'][$i]["H"]) ? 1 : 0;

					
					$data_query = array(
						$this::$IS_DELETED => 0,
						Ekspedisi::$NAMA_EKSPEDISI => $nama,
						Ekspedisi::$INISIAL => $inisial,
						Ekspedisi::$TUJUAN => $tujuan,
						Ekspedisi::$IS_TYPE_CONTAINER => $is_container,
						Ekspedisi::$IS_TYPE_LCL => $is_lcl,
						Ekspedisi::$IS_SCALE_COLLIE => $is_collie,
						Ekspedisi::$IS_SCALE_M3 => $is_m3,
						Ekspedisi::$IS_SCALE_KG => $is_kilo,
					);
					
					//INSERT EKSPEDISI
					$this->db->insert(Ekspedisi::$TABLE_NAME, $data_query);
				} else
					break;
			}
		}
		return Ekspedisi::$MESSAGE_SUCCESS_UPLOAD;
	}

	//Purchase Order
	

	//Work Sheet
	
	
	//Helper
	public function str_contains($haystack,$needle){
		if(strpos(strtoupper($haystack),strtoupper($needle))!==FALSE)
			return true;
		return false;
	}
	public function equals($string1,$string2){
		if(strcmp(strtoupper($string1),strtoupper($string2))!==0)
			return false;
		return true;
	}
	public function contains_number($string){
		for($i=0;$i<strlen($string);$i++){
			if(is_numeric($string[$i]))
				return true;
		}
		return false;
	}
	public function contains_not_number($string){
		for($i=0;$i<strlen($string);$i++){
			if(!is_numeric($string[$i]))
				return true;
		}
		return false;
	}

	//Settings
	public function change_password($old_password,$new_password,$confirm_new_password){
		if($this->equals($old_password,"")||$this->equals($new_password,"")||$this->equals($confirm_new_password,"")){
			return Change_Password::$MESSAGE_FAILED_UPDATE_FIELD_EMPTY;
		}
		if(md5($new_password)!=md5($confirm_new_password)){
			return Change_Password::$MESSAGE_FAILED_UPDATE_NEW_PASSWORD_NOT_SAME;
		}
		$this->db->where(Karyawan::$ID,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
		$qry=$this->db->get(Karyawan::$TABLE_NAME);
		$cur_password=$qry->row_array()[Karyawan::$PASSWORD];
		if($cur_password!=md5($old_password)){
			return Change_Password::$MESSAGE_FAILED_UPDATE_WRONG_OLD_PASSWORD;
		}
		if($cur_password==md5($new_password)){
			return Change_Password::$MESSAGE_FAILED_UPDATE_NEW_OLD_PASSWORD_SAME;
		}
		$data=[];
		$data[Karyawan::$PASSWORD]=md5($new_password);
		$this->db->where(Karyawan::$ID,$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
		$this->db->update(Karyawan::$TABLE_NAME,$data);
		return Change_Password::$MESSAGE_SUCCESS_UPDATE;
	}
	public function change_no_sj($old_code,$new_sj,$sto){
		$is_found=false;
		$this->db->where(H_Good_Issue::$ID,$old_code);
		$qry=$this->db->get(H_Good_Issue::$TABLE_NAME);
		
		if($qry->num_rows()>0){ //jika ketemu berarti revisi sj keluar
			$new_sj_insert="";
			if($this->equals($sto,Surat_Jalan::$KEYWORD_SO))
				$new_sj_insert = H_Good_Issue::$SO_CODE . date("y") . str_pad($new_sj, 7, "0", STR_PAD_LEFT);
			else
				$new_sj_insert = H_Good_Issue::$STO_CODE . date("y") . str_pad($new_sj, 8, "0", STR_PAD_LEFT);
			
			//update header good issue
			$arr=[];
			$arr[H_Good_Issue::$NOMOR_SJ]=$new_sj_insert;
			$arr[H_Good_Issue::$STO]=$sto;
			$this->db->where(H_Good_Issue::$ID,$old_code);
			$this->db->update(H_Good_Issue::$TABLE_NAME,$arr);

			//update pesan history stock
			$arr_history=[];
			$this->db->like(Stock_Gudang::$PESAN, $old_code, 'both');
			$qry_history_stock=$this->db->get(Stock_Gudang::$TABLE_NAME);
			foreach($qry_history_stock->result() as $row){
				$pesan=$row->pesan;
				$arr_history[Stock_Gudang::$PESAN]=$pesan." ".Surat_Jalan::GEN_UPDATE_MESSAGE($new_sj_insert, $_SESSION[SESSION_KARYAWAN_PENTA]['nik']);

				$this->db->where(Stock_Gudang::$ID,$row->id_stock_gudang);
				$this->db->update(Stock_Gudang::$TABLE_NAME,$arr_history);
			}
			$is_found=true;
			//return Surat_Jalan::$MESSAGE_SUCCESS_UPDATE;
		}

		$this->db->where(H_Good_Receipt::$ID, $old_code);
		$qry = $this->db->get(H_Good_Receipt::$TABLE_NAME); 

		if ($qry->num_rows() > 0) { //jika ketemu berarti revisi sj masuk
			$new_sj_insert="";
			if ($this->equals($sto, Surat_Jalan::$KEYWORD_SO))
				$new_sj_insert = H_Good_Receipt::$SO_CODE . date("y") . str_pad($new_sj, 7, "0", STR_PAD_LEFT);
			else
				$new_sj_insert = H_Good_Receipt::$STO_CODE . date("y") . str_pad($new_sj, 8, "0", STR_PAD_LEFT);

			//update header good receipt
			$arr = [];
			$arr[H_Good_Receipt::$NOMOR_SJ] = $new_sj_insert;
			$arr[H_Good_Receipt::$STO] = $sto;
			$this->db->where(H_Good_Receipt::$ID, $old_code);
			$this->db->update(H_Good_Receipt::$TABLE_NAME, $arr);

			//update pesan history stock
			$arr_history = [];
			$this->db->like(Stock_Gudang::$PESAN, $old_code, 'both');
			$qry_history_stock = $this->db->get(Stock_Gudang::$TABLE_NAME);
			foreach ($qry_history_stock->result() as $row) {
				$pesan = $row->pesan;
				$arr_history[Stock_Gudang::$PESAN] = $pesan . " " . Surat_Jalan::GEN_UPDATE_MESSAGE($new_sj_insert, $_SESSION[SESSION_KARYAWAN_PENTA]['nik']);
				
				$this->db->where(Stock_Gudang::$ID, $row->id_stock_gudang);
				$this->db->update(Stock_Gudang::$TABLE_NAME, $arr_history);
			}
			$is_found=true;
			//return Surat_Jalan::$MESSAGE_SUCCESS_UPDATE;
		}
		if($is_found)
			return Surat_Jalan::$MESSAGE_SUCCESS_UPDATE;

		return Surat_Jalan::$MESSAGE_FAILED_UPDATE_OLD_SJ_NOT_FOUND;
	}

	//Other
	public function get_holiday_date(){
		$this->db->where($this::$IS_DELETED,0);
		return $this->db->get(Kalender::$TABLE_NAME);
	}
	public function get_null_start_end_work_hour_no_LK($nik,$month,$year){
		$query=$this->db->query("select * from absensi where nik=$nik and 
									month(tanggal)=$month and year(tanggal)=$year");
		if($query->num_rows()>0){
			return $this->db->query("select * from absensi where jam_masuk is null and jam_keluar is null 
									and (upper(keterangan) like '%SAKIT%' or upper(keterangan) like '%IZIN%'
									or upper(keterangan) like '%CUTI%') and
									nik=$nik and month(tanggal)=$month and year(tanggal)=$year")->num_rows();
		}
		return -1;
	}
	public function get_null_start_end_work_hour_LK($nik,$month,$year){
		$query=$this->db->query("select * from absensi where nik=$nik and 
									month(tanggal)=$month and year(tanggal)=$year");
		if($query->num_rows()>0){
			return $this->db->query("select * from absensi where jam_masuk is null and jam_keluar is null 
									and upper(keterangan) like '%LUAR KOTA%' and
									nik=$nik and month(tanggal)=$month and year(tanggal)=$year")->num_rows();
		}
		return -1;
	}

}
