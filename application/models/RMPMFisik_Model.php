<?php
class Material_RM_PM_Fisik{
	public static $TABLE_NAME = "material_rm_pm_fisik";
	public static $ID = "id_material_rm_pm_fisik";

	public static $TANGGAL = "tanggal";
	public static $QTY_PROGRAM = "qty_program";
	public static $QTY_FISIK = "qty_fisik";
	public static $IS_DELETED = "is_deleted";

	//Foreign Keys
	public static $NIK = "nik";
	public static $ID_MATERIAL = "id_material";

	//Message
	public static $MESSAGE_SUCCESS_INSERT = "RPFQ001"; 
	public static $MESSAGE_FAILED_INSERT_TYPE_NOT_SAME = "RPFQ002"; 

}
class RMPMFisik_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function insert($ctr, $arr_id_material, $arr_fisik_0, $arr_fisik_1){
		$checker = $this->get(Checker_RM_PM::$TABLE_NAME, Checker_RM_PM::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID])->row_array();
		for($i=0;$i<$ctr;$i++){
			$material=$this->get(Material_RM_PM::$TABLE_NAME,Material_RM_PM::$ID,$arr_id_material[$i])->row_array();
			if(!$this->equals($material[Material_RM_PM::$JENIS],$checker[Checker_RM_PM::$JENIS]))
				return Material_RM_PM_Fisik::$MESSAGE_FAILED_INSERT_TYPE_NOT_SAME;
		}

		$ctr_fisik=0;
		$arr_idm = [];
		$data = [];
		for ($i = 0; $i < $ctr; $i++) {
			$material = $this->get(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$ID, $arr_id_material[$i])->row_array();
			
			if(in_array($material[Material_RM_PM::$ID],$arr_idm)){
				$idx=-1;
				for($j=0;$j<$ctr_fisik;$j++){
					if($arr_idm[$j]==$material[Material_RM_PM::$ID])
						$idx=$j;
				}
				if(!$this->equals($checker[Checker_RM_PM::$JENIS],Checker_RM_PM::$S_TYPE_KEMASAN))
					$data[$idx][Material_RM_PM_Fisik::$QTY_FISIK] += ($arr_fisik_0[$i] * $material[Material_RM_PM::$BOX]) + $arr_fisik_1[$i];
				else
					$data[$idx][Material_RM_PM_Fisik::$QTY_FISIK] += $arr_fisik_0[$i];

			}else{
				$arr_idm[$ctr_fisik]=$material[Material_RM_PM::$ID];
				$data[$ctr_fisik][Material_RM_PM_Fisik::$ID_MATERIAL] = $material[Material_RM_PM::$ID];
				$data[$ctr_fisik][Material_RM_PM_Fisik::$QTY_PROGRAM] = $this->rmpm_model->stock_get_by($material[Material_RM_PM::$ID]);

				if (!$this->equals($checker[Checker_RM_PM::$JENIS], Checker_RM_PM::$S_TYPE_KEMASAN))
					$data[$ctr_fisik][Material_RM_PM_Fisik::$QTY_FISIK] = $arr_fisik_0[$i] * $material[Material_RM_PM::$BOX] + $arr_fisik_1[$i];
				else
					$data[$ctr_fisik][Material_RM_PM_Fisik::$QTY_FISIK] = $arr_fisik_0[$i];

				$ctr_fisik++;
			}

		}

		for ($i = 0; $i < $ctr_fisik; $i++) {
			/*//Cek dulu apakah dihari ini ada stock opname untuk barang itu
			$this->db->where(Material_RM_PM_Fisik::$TANGGAL,date('Y-m-d'));
			$this->db->where(Material_RM_PM_Fisik::$ID_MATERIAL,$data[$i][Material_RM_PM_Fisik::$ID_MATERIAL]);

			$qry=$this->db->get(Material_RM_PM_Fisik::$TABLE_NAME);
			if($qry->num_rows()>0){

			}*/
			$data_query=[
				Material_RM_PM_Fisik::$ID_MATERIAL => $data[$i][Material_RM_PM_Fisik::$ID_MATERIAL],
				Material_RM_PM_Fisik::$QTY_PROGRAM => $data[$i][Material_RM_PM_Fisik::$QTY_PROGRAM],
				Material_RM_PM_Fisik::$QTY_FISIK => $data[$i][Material_RM_PM_Fisik::$QTY_FISIK],
				Material_RM_PM_Fisik::$NIK => $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
			];
			$this->db->set(Material_RM_PM_Fisik::$TANGGAL, 'NOW()', FALSE);
			$this->db->insert(Material_RM_PM_Fisik::$TABLE_NAME,$data_query);
		}
		return Material_RM_PM_Fisik::$MESSAGE_SUCCESS_INSERT;
	}

	public function get_by_tanggal($dt, $st){
		/*$arr=[Material_RM_PM_Fisik::$ID, Material_RM_PM_Fisik::$TANGGAL,
				Material_RM_PM_Fisik::$TABLE_NAME.".".Material_RM_PM_Fisik::$ID_MATERIAL." ".Material_RM_PM_Fisik::$ID_MATERIAL,
				Material_RM_PM_Fisik::$TABLE_NAME.".". Material_RM_PM_Fisik::$NIK." ".Material_RM_PM_Fisik::$NIK,
				Karyawan::$NAMA,Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION,
				Material_RM_PM_Fisik::$QTY_PROGRAM, Material_RM_PM_Fisik::$QTY_FISIK];
		$this->db->select($arr);
		$this->db->from(Material_RM_PM_Fisik::$TABLE_NAME);
		$temp = Material_RM_PM_Fisik::$TABLE_NAME . "." . Material_RM_PM_Fisik::$ID_MATERIAL . "=" . Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $temp);
		$temp = Material_RM_PM_Fisik::$TABLE_NAME . "." . Material_RM_PM_Fisik::$NIK . "=" . Karyawan::$TABLE_NAME . "." . Karyawan::$ID;
		$this->db->join(Karyawan::$TABLE_NAME, $temp);
		if($dt!=null){
			$until=date("Y-m-d H:i:s",strtotime($st. "+ 1 day"));
			$this->db->where(Material_RM_PM_Fisik::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Material_RM_PM_Fisik::$TANGGAL . ' <', $until, TRUE);
		}
		
		return $this->db->get();*/
		$tanggal="";
		if ($dt != null) {
			$until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
			$tanggal.=" AND tanggal>='$dt' AND tanggal<'$until' ";
		}
		return $this->db->query("select id_material_rm_pm_fisik,tanggal,material_rm_pm_fisik.id_material id_material_rm_pm,box,kode_material,material_rm_pm_fisik.nik nik,nama,description,qty_program,qty_fisik,material_rm_pm.jenis
									from material_rm_pm_fisik,material_rm_pm,karyawan
									where material_rm_pm_fisik.id_material=material_rm_pm.id_material_rm_pm AND karyawan.nik=material_rm_pm_fisik.nik
									$tanggal
									UNION
									select null,null,id_material_rm_pm,box,kode_material,null,null,description,null,null,jenis
									from material_rm_pm
									where material_rm_pm.id_material_rm_pm not in (select material_rm_pm_fisik.id_material
									from material_rm_pm_fisik,material_rm_pm,karyawan
									where material_rm_pm_fisik.id_material=material_rm_pm.id_material_rm_pm AND karyawan.nik=material_rm_pm_fisik.nik
									$tanggal)
									order by tanggal desc");
	}
}
