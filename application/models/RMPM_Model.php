<?php
class Material_RM_PM{
	public static $TABLE_NAME = "material_rm_pm";
	public static $ID = "id_material_rm_pm";

	public static $KODE_MATERIAL = "kode_material";
	public static $KODE_PANGGIL = "kode_panggil";
	public static $DESCRIPTION = "description";
	public static $JENIS = "jenis";
	public static $KEMASAN = "kemasan";
	public static $KG = "kg";
	public static $TIPE = "tipe";
	public static $IS_KONSINYASI = "is_konsinyasi";
	public static $UNIT = "unit";
	public static $BOX = "box";
	public static $IS_DELETED = "is_deleted";

	//Static Variables
	public static $S_UNIT_KG = "KG";
	public static $S_UNIT_PCS = "PCS";
	public static $S_MUL_PEREKAT = 110;

	public static $S_ARR_INPUT_SG = ['2307B', '2308A'];

}
class Stock_Gudang_RM_PM{
	public static $TABLE_NAME = "stock_gudang_rm_pm";
	public static $ID = "id_stock_gudang";

	public static $TANGGAL = "tanggal";
	public static $STOCK_AWAL = "stock_awal";
	public static $VALUE_AWAL = "value_awal";
	public static $KREDIT = "kredit";
	public static $DEBIT = "debit";
	public static $STOCK_AKHIR = "stock_akhir";
	public static $VALUE_AKHIR = "value_akhir";
	public static $IS_DELETED = "is_deleted";
	public static $PESAN = "pesan";
	public static $NOMOR_PO_WS = "nomor_po_ws";
	public static $NOMOR_SJ = "nomor_sj";

	//Foreign Key
	public static $ID_VENDOR = "id_vendor";
	public static $ID_MATERIAL = "id_material_rm_pm";

	public static function GEN_UPLOAD_MESSAGE()
	{
		return "Upload Stock Awal";
	}
	public static function GEN_UPLOAD_OPNAME_MESSAGE()
	{
		return "Upload Stock Opname Tahunan";
	}
}
class Material_RM_PM_Stock{
	public static $TABLE_NAME = "material_rm_pm_stock";
	public static $ID = "id_material_rm_pm_stock";

	public static $QTY = "qty";
	public static $VALUE = "value";

	//Foreign Key
	public static $ID_MATERIAL = "id_material_rm_pm";
	public static $ID_VENDOR = "id_vendor";

	//Static Variable
	public static $S_STOCK_AKHIR = "stock_akhir";
	public static $S_NET_PRICE = "net_price";

	public static $MESSAGE_SUCCESS_UPLOAD = "MRPQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "MRPQ002";
	public static $MESSAGE_SUCCESS_UPLOAD_OPNAME = "MRPQ003";

	public static function GEN_UPLOAD_MESSAGE()
	{
		return "Upload Stock Awal";
	}
}

//Hanya untuk Program, Bukan Tabel
class Document{
	public static $MESSAGE_SUCCESS_SEARCH = "DWPQ001"; //Document WorkSheet PO
	public static $MESSAGE_FAILED_SEARCH_NOT_FOUND = "DWPQ002"; 

}

class RMPM_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function update_batch($data){
		$kal = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$no = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$jenis = isset($data['values'][$i]["B"]) ? $data['values'][$i]["B"] : NULL;
					$tipe = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$is_konsinyasi = isset($data['values'][$i]["D"]) ? 1 : 0;
					$description = isset($data['values'][$i]["E"]) ? $data['values'][$i]["E"] : NULL;
					$unit = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$box = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;


					$data_query = array(
						Material_RM_PM::$JENIS => $jenis,
						Material_RM_PM::$TIPE => $tipe,
						Material_RM_PM::$IS_KONSINYASI => $is_konsinyasi,
						Material_RM_PM::$DESCRIPTION => $description,
						Material_RM_PM::$UNIT => $unit,
						Material_RM_PM::$BOX => $box,
						Material_RM_PM::$IS_DELETED => 0,
					);


					//INSERT MATERIAL RM PM
					//$this->db->where(Material_RM_PM::$ID, $no);
					//$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
					//if ($qry->num_rows() == 0) {
					$data_query[Material_RM_PM::$KODE_MATERIAL] = $no;
					$this->db->insert(Material_RM_PM::$TABLE_NAME, $data_query);
					/*} else {
						$kal .= $no . " Update!<br>";
						$this->db->where(Material_RM_PM::$ID, $no);
						$this->db->update(Material_RM_PM::$TABLE_NAME, $data_query);
					}*/

					//Check material RM PM yang belum masuk
					//$kal.="$no";
					$this->db->where(Material_RM_PM::$ID, $no);
					$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
					if ($qry->num_rows() == 0) {
						$kal .= $no . " tidak ada<br>";
					} else {
					}
				} else
					break;
			}
		}
		return $kal;
	}
	public function stock_update_batch($data){
		//jika tidak kosong sebelumnya, dihapus semua
		$this->db->empty_table(Material_RM_PM_Stock::$TABLE_NAME);

		$kal = "";
		for ($i = 0; $i < count($data['values']) + 10; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["A"] != "") { //jika material no tidak kosong
					$kode_material = isset($data['values'][$i]["A"]) ? $data['values'][$i]["A"] : NULL;
					$konsinyasi = isset($data['values'][$i]["B"]) ? 1 : NULL;
					$id_vendor = isset($data['values'][$i]["C"]) ? $data['values'][$i]["C"] : NULL;
					$stock = isset($data['values'][$i]["F"]) ? $data['values'][$i]["F"] : NULL;
					$value = isset($data['values'][$i]["G"]) ? $data['values'][$i]["G"] : NULL;

					$data_query = array(
						Material_RM_PM_Stock::$QTY => $stock,
						Material_RM_PM_Stock::$VALUE => $value
					);

					$id_material = null;
					$this->db->where(Material_RM_PM::$KODE_MATERIAL, $kode_material);
					if ($konsinyasi)
						$this->db->where(Material_RM_PM::$IS_KONSINYASI, 1);
					$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
					if ($qry->num_rows() > 0)
						$id_material = $qry->row_array()[Material_RM_PM::$ID];

					if ($id_material != null) {
						//$this->insert_history_stock_rm_pm(null, date('d M Y'), $id_material, $id_vendor, $stock, 0, Stock_Gudang_RM_PM::GEN_UPLOAD_MESSAGE());

						//Cek apakah Stock stock sudah ada atau belum
						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $id_material);

						if ($id_vendor != null)
							$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $id_vendor);

						$qry = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);

						if ($id_vendor != null)
							$data_query[Material_RM_PM_Stock::$ID_VENDOR] = $id_vendor;

						//Insert History Stock
						/*$this->insert_history_stock_rm_pm(
							"",
							"",
							date("Y-m-d"),
							$id_material,
							($id_vendor) != null ? $id_vendor : NULL,
							$stock,
							0,
							Material_RM_PM_Stock::GEN_UPLOAD_MESSAGE()
						);
						*/
						if ($qry->num_rows() > 0) {
							//UPDATE MATERIAL RM PM Stock
							$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $id_material);
							$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data_query);
						} else {
							//INSERT MATERIAL RM PM Stock
							$data_query[Material_RM_PM_Stock::$ID_MATERIAL] = $id_material;
							$this->db->insert(Material_RM_PM_Stock::$TABLE_NAME, $data_query);
						}
					} else {
						$kal .= $kode_material . " - Material Tidak ada<br>";
					}
				} else
					break;
			}
		}
		return Material_RM_PM_Stock::$MESSAGE_SUCCESS_UPLOAD;
	}
	public function stock_opname_update_batch($data){
		for ($i = 0; $i < count($data['values']) + 200; $i++) {
			if (isset($data['values'][$i])) {
				if ($data['values'][$i]["D"] != "") { //jika material no tidak kosong
					$kode_material = isset($data['values'][$i]["D"]) ? $data['values'][$i]["D"] : NULL;
					
					//Ambil id Material yang tidak Konsinyasi
					$this->db->where(Material_RM_PM::$KODE_MATERIAL,$kode_material);
					$this->db->where(Material_RM_PM::$IS_KONSINYASI,0);
					$material=$this->db->get(Material_RM_PM::$TABLE_NAME)->row_array();
					
					$program = $this->stock_get_by($material[Material_RM_PM::$ID]);
					$fisik = isset($data['values'][$i]["I"]) ? $data['values'][$i]["I"] : NULL;
					$qty_selisih = isset($data['values'][$i]["J"]) ? $data['values'][$i]["J"] : NULL;
					$value_selisih = isset($data['values'][$i]["L"]) ? $data['values'][$i]["L"] : NULL;
					//echo $material[Material_RM_PM::$ID] . " - ". $this->decimal($program, 3)." - ". $this->decimal($fisik, 3)."<br>";
					//Insert Material Fisik
					$arr_fisik=[
						Material_RM_PM_Fisik::$ID_MATERIAL => $material[Material_RM_PM::$ID],
						Material_RM_PM_Fisik::$IS_DELETED => 0,
						Material_RM_PM_Fisik::$NIK => $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]
					];
					$arr_fisik[Material_RM_PM_Fisik::$QTY_FISIK]= str_replace(",","",$this->decimal($fisik,3));
					$arr_fisik[Material_RM_PM_Fisik::$QTY_PROGRAM] = str_replace(",","",$this->decimal($program,3));
					//echo $material[Material_RM_PM::$ID] . " - ". $arr_fisik[Material_RM_PM_Fisik::$QTY_FISIK]." - ". $arr_fisik[Material_RM_PM_Fisik::$QTY_PROGRAM]."<br>";

					$this->db->set(Material_RM_PM_Fisik::$TANGGAL, 'NOW()', FALSE);
					$this->db->insert(Material_RM_PM_Fisik::$TABLE_NAME,$arr_fisik);
					
					//Insert History Stock
					$varian=$fisik-$program;
					
					
					if($varian!=0){
						//Update Stock
						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $material[Material_RM_PM::$ID]);
						$stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME)->row_array();
						if($qty_selisih!=0)
							$net_price=abs($value_selisih/$qty_selisih);
						else{
							$net_price=$stock[Material_RM_PM_Stock::$VALUE]/$stock[Material_RM_PM_Stock::$QTY];
						}
						$arr_stock=[
							Material_RM_PM_Stock::$QTY => str_replace(",", "", $this->decimal($fisik, 3)),
							Material_RM_PM_Stock::$VALUE => $fisik*$net_price
						];
						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL,$material[Material_RM_PM::$ID]);
						$value_awal=$this->db->get(Material_RM_PM_Stock::$TABLE_NAME)->row_array()[Material_RM_PM_Stock::$VALUE];
						//echo $material[Material_RM_PM::$ID] . " - " . $net_price . " - " . $value_selisih . " - ". $qty_selisih ."<br>";
						if ($varian > 0) { //Barang Masuk
							$this->insert_history_stock(null, null, date("Y-m-d H:i:s"), $material[Material_RM_PM::$ID], null, $value_awal, abs($varian), 0, $arr_stock[Material_RM_PM_Stock::$VALUE], Stock_Gudang_RM_PM::GEN_UPLOAD_OPNAME_MESSAGE());
						} else if ($varian < 0) { //Barang Keluar 
							$this->insert_history_stock(null, null, date("Y-m-d H:i:s"), $material[Material_RM_PM::$ID], null, $value_awal, 0, abs($varian), $arr_stock[Material_RM_PM_Stock::$VALUE], Stock_Gudang_RM_PM::GEN_UPLOAD_OPNAME_MESSAGE());
						}
						$this->db->where(Material_RM_PM_Stock::$ID,$stock[Material_RM_PM_Stock::$ID]);
						$this->db->update(Material_RM_PM_Stock::$TABLE_NAME,$arr_stock);
					}
				}
			}
		}
		return Material_RM_PM_Stock::$MESSAGE_SUCCESS_UPLOAD_OPNAME;
	}

	public function insert_history_stock($nomor_po_ws, $nomor_sj, $tanggal, $id_material, $id_vendor, $value_awal, $debit, $kredit, $value_akhir, $pesan){
		$arr_stock = [];
		$arr_stock[Stock_Gudang_RM_PM::$IS_DELETED] = 0;
		$arr_stock[Stock_Gudang_RM_PM::$ID_VENDOR] = $id_vendor;
		$arr_stock[Stock_Gudang_RM_PM::$TANGGAL] = $tanggal;
		$arr_stock[Stock_Gudang_RM_PM::$STOCK_AWAL] = $this->stock_get_by($id_material, $id_vendor);
		$arr_stock[Stock_Gudang_RM_PM::$VALUE_AWAL] = $value_awal;
		$arr_stock[Stock_Gudang_RM_PM::$DEBIT] = $debit;
		$arr_stock[Stock_Gudang_RM_PM::$KREDIT] = $kredit;
		$arr_stock[Stock_Gudang_RM_PM::$ID_MATERIAL] = $id_material;
		$arr_stock[Stock_Gudang_RM_PM::$STOCK_AKHIR] = $arr_stock[Stock_Gudang_RM_PM::$STOCK_AWAL] + $arr_stock[Stock_Gudang_RM_PM::$DEBIT] - $arr_stock[Stock_Gudang_RM_PM::$KREDIT];
		$arr_stock[Stock_Gudang_RM_PM::$VALUE_AKHIR] = $value_akhir;
		$arr_stock[Stock_Gudang_RM_PM::$PESAN] = $pesan;
		$arr_stock[Stock_Gudang_RM_PM::$NOMOR_PO_WS] = $nomor_po_ws;
		$arr_stock[Stock_Gudang_RM_PM::$NOMOR_SJ] = $nomor_sj;


		$this->db->insert(Stock_Gudang_RM_PM::$TABLE_NAME, $arr_stock);
	}
	public function insert_gi_mutasi($ctr, $keterangan, $arr_id_material, $arr_qty, $arr_vendor){
		$data_header = [];
		$max = "";

		$max = $this->db->query("select max(substr(id_worksheet_checker_header,8)) as max from " . Worksheet_Checker_Header::$TABLE_NAME . "
									where id_worksheet_checker_header like 'ADJ%'")->row()->max;
		if ($max == NULL)
		$max = 0;
		$max += 1;

		//Insert WS Checker Header
		$kode = Worksheet::$S_MUT_CODE . "GI" . str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Worksheet_Checker_Header::$ID] = $kode;
		$data_header[Worksheet_Checker_Header::$TANGGAL] = date('Y-m-d');
		$data_header[Worksheet_Checker_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Worksheet_Checker_Header::$NOMOR_WS] = null;
		$data_header[Worksheet_Checker_Header::$IS_DELETED] = 0;
		$this->db->insert(Worksheet_Checker_Header::$TABLE_NAME, $data_header);
		echo $this->alert('Kode GI: ' . $kode);

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];

			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
			$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($temp->num_rows() > 0) {
				$this->db->where(Material_RM_PM::$ID, $arr_id_material[$i]);
				$material = $this->db->get(Material_RM_PM::$TABLE_NAME);

				if ($material->num_rows() > 0) {
					$material = $material->row_array();
					$kode_detail = $data_header[Worksheet_Checker_Header::$ID] . str_pad(($i + 1), 3, "0", STR_PAD_LEFT);
					$data_detail[Worksheet_Checker_Detail::$ID] = $kode_detail;
					$data_detail[Worksheet_Checker_Detail::$NOMOR_URUT] = null;
					$data_detail[Worksheet_Checker_Detail::$QTY] = $arr_qty[$i];
					$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL] = $material[Material_RM_PM::$ID];
					$data_detail[Worksheet_Checker_Detail::$ID_HEADER] = $data_header[Worksheet_Checker_Header::$ID];

					if ($arr_vendor[$i] != null)
						$data_detail[Worksheet_Checker_Detail::$ID_VENDOR] = $arr_vendor[$i];
					$this->db->insert(Worksheet_Checker_Detail::$TABLE_NAME, $data_detail);

					$data_vendor = null;
					if ($arr_vendor[$i] != null) {
						$data_vendor = $arr_vendor[$i];
						$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $arr_vendor[$i]);
					}
					$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
					$temp_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
					if ($temp_stock->num_rows() > 0) {
						$data = [];
						$stock = $temp_stock->row_array();
						$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
						$qty_awal = $stock[Material_RM_PM_Stock::$QTY];
						$harga_satuan=0;
						if($qty_awal!=0)
							$harga_satuan = $value_awal / $qty_awal;

						$data[Material_RM_PM_Stock::$QTY] = $qty_awal - $data_detail[Worksheet_Checker_Detail::$QTY];
						$data[Material_RM_PM_Stock::$VALUE] = $data[Material_RM_PM_Stock::$QTY] * $harga_satuan;
						//Insert History Stock RM PM
						$this->insert_history_stock(
							$data_header[Worksheet_Checker_Header::$NOMOR_WS],
							$data_header[Worksheet_Checker_Header::$NOMOR_WS],
							$data_header[Worksheet_Checker_Header::$TANGGAL],
							$data_detail[Worksheet_Checker_Detail::$ID_MATERIAL],
							$data_vendor,
							$value_awal,
							0,
							$data_detail[Worksheet_Checker_Detail::$QTY],
							$data[Material_RM_PM_Stock::$VALUE],
							Worksheet_Checker_Detail::GEN_MUTASI_MESSAGE(
								$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID],
								$kode,
								$keterangan
							)
						);
						$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
						if ($arr_vendor[$i] != null) {
							$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $data_detail[Worksheet_Checker_Detail::$ID_VENDOR]);
						}
						
						$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
					}
				}
			} else {
				return Worksheet_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
			}
		}
		return Worksheet::$MESSAGE_SUCCESS_MUTASI;
	}
	public function insert_gr_mutasi($ctr, $keterangan, $arr_id_material, $arr_qty, $arr_vendor){
		$data_header = [];
		$max = "";

		$max = $this->db->query("select max(substr(id_purchase_order_checker_header,8)) as max from " . Purchase_Order_Checker_Header::$TABLE_NAME .
			" where id_purchase_order_checker_header like 'ADJ%'")->row()->max;
		if ($max == NULL)
		$max = 0;
		$max += 1;

		//Insert PO Checker Header
		$kode = Purchase_Order::$S_MUT_CODE . "GR" . str_pad($max, 8, "0", STR_PAD_LEFT);
		$data_header[Purchase_Order_Checker_Header::$ID] = $kode;
		$data_header[Purchase_Order_Checker_Header::$TANGGAL] = date('Y-m-d');
		$data_header[Purchase_Order_Checker_Header::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data_header[Purchase_Order_Checker_Header::$NOMOR_PO] = null;
		$data_header[Purchase_Order_Checker_Header::$IS_DELETED] = 0;
		$this->db->insert(Purchase_Order_Checker_Header::$TABLE_NAME, $data_header);
		echo $this->alert('Kode GR: ' . $kode);

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];

			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
			$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($temp->num_rows() > 0) {
				$this->db->where(Material_RM_PM::$ID, $arr_id_material[$i]);
				$material = $this->db->get(Material_RM_PM::$TABLE_NAME)->row_array();

				//Insert PO Checker
				$kode_detail = $data_header[Purchase_Order_Checker_Header::$ID] . str_pad(($i + 1), 3, "0", STR_PAD_LEFT);
				$data_detail[Purchase_Order_Checker_Detail::$ID] = $kode_detail;
				$data_detail[Purchase_Order_Checker_Detail::$NOMOR_URUT] = null;
				$data_detail[Purchase_Order_Checker_Detail::$QTY] = $arr_qty[$i];

				$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL] = $material[Material_RM_PM::$ID];
				$data_detail[Purchase_Order_Checker_Detail::$ID_HEADER] = $data_header[Purchase_Order_Checker_Header::$ID];

				$this->db->insert(Purchase_Order_Checker_Detail::$TABLE_NAME, $data_detail);

				$data_vendor = null;
				if ($arr_vendor[$i] != null) {
					$data_vendor = $arr_vendor[$i];
					$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $arr_vendor[$i]);
				}
				$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
				$temp_stock = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
				if ($temp_stock->num_rows() > 0) {
					$data = [];
					$stock = $temp_stock->row_array();
					$value_awal = $stock[Material_RM_PM_Stock::$VALUE];
					$qty_awal = $stock[Material_RM_PM_Stock::$QTY];
					$harga_satuan = 0;
					if($qty_awal!=0)
						$harga_satuan = $value_awal / $qty_awal;
					
					$data[Material_RM_PM_Stock::$QTY] = $qty_awal + $data_detail[Purchase_Order_Checker_Detail::$QTY];
					$data[Material_RM_PM_Stock::$VALUE] = $data[Material_RM_PM_Stock::$QTY] * $harga_satuan;
					//Insert History Stock RM PM
					$this->insert_history_stock(
						$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
						$data_header[Purchase_Order_Checker_Header::$NOMOR_PO],
						$data_header[Purchase_Order_Checker_Header::$TANGGAL],
						$data_detail[Purchase_Order_Checker_Detail::$ID_MATERIAL],
						$data_vendor,
						$value_awal,
						$data_detail[Purchase_Order_Checker_Detail::$QTY],
						0,
						$data[Material_RM_PM_Stock::$VALUE],
						Purchase_Order_Checker_Detail::GEN_MUTASI_MESSAGE(
							$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID],
							$kode,
							$keterangan
						)
					);
					$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $arr_id_material[$i]);
					if ($arr_vendor[$i] != null) {
						$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $arr_vendor[$i]);
					}
					
					$this->db->update(Material_RM_PM_Stock::$TABLE_NAME, $data);
				}
			} else {
				return Purchase_Order_Checker_Detail::$MESSAGE_FAILED_INSERT_STOCK_NOT_FOUND;
			}
		}
		return Purchase_Order::$MESSAGE_SUCCESS_MUTASI;
	}

	public function stock_get_all($with_fisik = false){
		if ($with_fisik == false) {
			$select = [
				Material_RM_PM::$KODE_MATERIAL, Material_RM_PM::$DESCRIPTION, Material_RM_PM_Stock::$QTY,
				Material_RM_PM_Stock::$VALUE, Material_RM_PM_Stock::$ID_VENDOR
			];
			$this->db->select($select);
			$this->db->from(Material_RM_PM_Stock::$TABLE_NAME);
			$this->db->join(Material_RM_PM::$TABLE_NAME, Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$ID . "=" .
				Material_RM_PM_Stock::$TABLE_NAME . "." . Material_RM_PM_Stock::$ID_MATERIAL);

			$this->db->where(Material_RM_PM::$TABLE_NAME . "." . Material_RM_PM::$IS_DELETED, 0);
			$data = $this->db->get();
			return $data;
		}else{
			$this->db->where(Material_RM_PM::$IS_DELETED, 0);
			$this->db->where(Material_RM_PM::$IS_KONSINYASI,0);
			$qry = $this->db->get(Material_RM_PM::$TABLE_NAME);
			$data = [];
			$ctr = 0;
			foreach ($qry->result_array() as $row) {
				$id_material = $row[Material_RM_PM::$ID];
				$stock = $this->stock_get_by($id_material);
				$data[$ctr][Material_RM_PM::$KODE_MATERIAL] = $row[Material_RM_PM::$KODE_MATERIAL];
				$data[$ctr][Material_RM_PM::$DESCRIPTION] = $row[Material_RM_PM::$DESCRIPTION];
				$data[$ctr][Material_RM_PM_Fisik::$QTY_PROGRAM] = $stock;
				$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $id_material);
				$temp = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
				$value = 0;
				if ($temp->num_rows() > 0) {
					$value = $temp->row_array()[Material_RM_PM_Stock::$VALUE];
				}
				$data[$ctr][Material_RM_PM_Stock::$VALUE] = $value;
				if($stock!=0)
					$data[$ctr][Material_RM_PM_Stock::$S_NET_PRICE] = $value / $stock;
				else
					$data[$ctr][Material_RM_PM_Stock::$S_NET_PRICE] = 0;

				$stock_fisik = 0;
				$this->db->where(Material_RM_PM_Fisik::$ID_MATERIAL, $id_material);
				$this->db->order_by(Material_RM_PM_Fisik::$ID, $this::$ORDER_TYPE_DESC);
				$qry_material_fisik = $this->db->get(Material_RM_PM_Fisik::$TABLE_NAME);
				if ($qry_material_fisik->num_rows() > 0) {
					$stock_fisik = $qry_material_fisik->row_array()[Material_RM_PM_Fisik::$QTY_FISIK];
				}

				$data[$ctr][Material_Fisik::$QTY_FISIK] = $stock_fisik;

				$ctr++;
			}
			return $data;
		}
	}
	public function stock_get_history_by($dt, $st, $nomor_sj = null, $id_material = null){
		$arr = [
			Stock_Gudang_RM_PM::$ID,
			Stock_Gudang_RM_PM::$TABLE_NAME . "." . Stock_Gudang_RM_PM::$ID_MATERIAL . " " . Stock_Gudang_RM_PM::$ID_MATERIAL,
			Stock_Gudang_RM_PM::$TANGGAL, Material_RM_PM::$KODE_PANGGIL, Stock_Gudang_RM_PM::$ID_VENDOR,
			Stock_Gudang_RM_PM::$NOMOR_SJ,
			Stock_Gudang_RM_PM::$STOCK_AWAL, Stock_Gudang_RM_PM::$DEBIT, Stock_Gudang_RM_PM::$KREDIT,
			Stock_Gudang_RM_PM::$STOCK_AKHIR, Stock_Gudang_RM_PM::$PESAN,
			Stock_Gudang_RM_PM::$VALUE_AWAL,Stock_Gudang_RM_PM::$VALUE_AKHIR
		];
		$this->db->select($arr);
		$this->db->from(Stock_Gudang_RM_PM::$TABLE_NAME);
		$join=Material_RM_PM::$TABLE_NAME.".". Material_RM_PM::$ID."=". Stock_Gudang_RM_PM::$TABLE_NAME.".". Stock_Gudang_RM_PM::$ID_MATERIAL;
		$this->db->join(Material_RM_PM::$TABLE_NAME, $join);

		if ($dt != null) {
			$until = date("Y-m-d H:i:s", strtotime($st . "+ 1 day"));
			$this->db->where(Stock_Gudang_RM_PM::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Stock_Gudang_RM_PM::$TANGGAL . ' <', $until, TRUE);
		}
		if ($nomor_sj != null) {
			$this->db->where(Stock_Gudang_RM_PM::$NOMOR_SJ, $nomor_sj);
		}
		if ($id_material != null) {
			$this->db->where(Stock_Gudang_RM_PM::$TABLE_NAME . "." . Stock_Gudang_RM_PM::$ID_MATERIAL, $id_material);
		}
		$qry = $this->db->get();
		return $qry;
	}
	public function stock_get_by($id_material, $id_vendor = null){
		$this->db->select("sum(qty) as ". Material_RM_PM_Stock::$S_STOCK_AKHIR);
		$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $id_material);
		$data = array(Material_RM_PM_Stock::$ID_MATERIAL);
		if ($id_vendor != null) {
			$this->db->where(Material_RM_PM_Stock::$ID_VENDOR, $id_vendor);
			$data[1] = Material_RM_PM_Stock::$ID_VENDOR;
		}
		$this->db->group_by($data);
		$qry = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
		if ($qry->num_rows() > 0) {
			return $qry->row_array()[Material_RM_PM_Stock::$S_STOCK_AKHIR];
		} else { //jika vendor di stock null tapi di po awal ada, tetap digunakan stock dari material tsb
			$this->db->select("sum(qty) as " . Material_RM_PM_Stock::$S_STOCK_AKHIR);
			$this->db->where(Material_RM_PM_Stock::$ID_MATERIAL, $id_material);
			$this->db->group_by(Material_RM_PM_Stock::$ID_MATERIAL);
			$qry = $this->db->get(Material_RM_PM_Stock::$TABLE_NAME);
			if ($qry->num_rows() > 0) {
				return $qry->row_array()[Material_RM_PM_Stock::$S_STOCK_AKHIR];
			}
		}
		return 0;
	}
	public function stock_get_by_pesan($pesan){
		$this->db->like(Stock_Gudang_RM_PM::$PESAN, $pesan, $this::$WHERE_LIKE_BOTH);
		return $this->db->get(Stock_Gudang_RM_PM::$TABLE_NAME);

	}
}
