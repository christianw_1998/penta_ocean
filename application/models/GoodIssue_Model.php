<?php
class H_Good_Issue{
	public static $TABLE_NAME = "h_good_issue";
	public static $ID = "id_h_good_issue";
	public static $TANGGAL = "tanggal";
	public static $NOMOR_SJ = "nomor_sj";
	public static $STO = "sto";
	public static $QTY = "qty";
	public static $EKSPEDISI = "ekspedisi";
	public static $TANGGAL_CONFIRM_ADMIN = "tanggal_confirm_admin";
	public static $KODE_REVISI = "kode_revisi";
	public static $REV_GOOD_RECEIPT = "rev_good_receipt";

	//Foreign Key
	public static $NIK = "nik";
	public static $NIK_ADMIN = "nik_admin";
	public static $NIK_SOPIR = "nik_sopir";
	public static $NIK_KERNET = "nik_kernet";
	public static $ID_EKSPEDISI = "id_ekspedisi";

	public static $REV_CODE = "REVGI";
	public static $ADJ_CODE = "ADJGI";
	public static $STO_CODE = "8";
	public static $SIRCLO_CODE = "OUT";
	public static $SO_CODE = "21";
	public static $RETUR_CODE = "RT";

	//Static Variable
	public static $S_TOT_QTY="tot_qty";

	public static $MESSAGE_SUCCESS_INSERT = "HGIQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "HGIQ003";
	public static $MESSAGE_SUCCESS_ADJUSTMENT = "HGIQ004";
	public static $MESSAGE_SUCCESS_UPDATE_KINERJA = "HGIQ006";

	public static $MESSAGE_FAILED_INSERT_SJ_NOT_FOUND = "HGIQ002";
	public static $MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED = "HGIQ008";
	public static $MESSAGE_FAILED_INSERT_IS_NOT_PERMITTED = "HGIQ005";
	public static $MESSAGE_FAILED_UPDATE_KINERJA = "HGIQ007";

	public static function GEN_INSERT_MESSAGE($params, $params2)
	{
		return "Pengurangan dari Surat Jalan no " . $params . " (Kode: " . $params2 . ")";
	}
	public static function GEN_REVISION_MESSAGE($params, $params2, $params3)
	{
		return "Revisi SJ no " . $params . " (Kode Revisi: " . $params2 . ", Kode: " . $params3 . ")";
	}
	public static function GEN_ADJUSTMENT_MESSAGE($params, $params2)
	{
		return "Adjustment oleh " . $params . " (Kode: " . $params2 . ")";
	}
}
class D_Good_Issue{
	public static $TABLE_NAME = "d_good_issue";
	public static $ID = "id_d_good_issue";
	public static $DUS = "dus";
	public static $TIN = "tin";

	//Foreign Key
	public static $ID_MATERIAL = "id_material_fg";
	public static $ID_H_GOOD_ISSUE = "id_h_good_issue";
	public static $ID_GUDANG = "id_gudang";

	public static $MESSAGE_SUCCESS_INSERT = "DGIQ001";
	public static $MESSAGE_FAILED_INSERT = "DGIQ002";
}
class GoodIssue_Model extends FG_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}

	public function insert($ctr, $revisi, $sto, $ekspedisi, $nomor_sj, $arr_kode_barang, $arr_kemasan, $arr_dus, $arr_tin, $arr_warna){
		$arr = [];
		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY, 0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//$this->db->like(H_Good_Issue::$NOMOR_SJ, $nomor_sj, 'before');
		$this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);
		$this->db->where(H_Good_Issue::$KODE_REVISI . $this::$WHERE_IS_NULL, NULL, FALSE);
		$temp = $this->db->get(H_Good_Issue::$TABLE_NAME);
		if ($temp->num_rows() == 0 && $this->equals($revisi, "y")) {
			$this->db->where(H_Good_Receipt::$KODE_REVISI . $this::$WHERE_IS_NULL, NULL, FALSE);
			$this->db->where(H_Good_Receipt::$NOMOR_SJ, $nomor_sj);
			$temp1 = $this->db->get(H_Good_Receipt::$TABLE_NAME);
			if ($temp1->num_rows() == 0)
			return H_Good_Issue::$MESSAGE_FAILED_INSERT_SJ_NOT_FOUND;
			else
			$arr[H_Good_Issue::$REV_GOOD_RECEIPT] = 1;
		}

		//AUTO GEN kode surat jalan
		$max = $this->db->query("select max(substr(id_H_Good_Issue,8)) as max from " . H_Good_Issue::$TABLE_NAME . "
								where MONTH(tanggal)=" . date("m") . " and YEAR(tanggal)=" . date("Y"))->row()->max;
		if ($max == NULL)
		$max = 0;
		$max += 1;

		$kode = date("ym") . str_pad($max, 6, "0", STR_PAD_LEFT);

		//header
		$arr[H_Good_Issue::$ID] = "GI" . $kode;
		$arr[H_Good_Issue::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$no_sj_insert = "";
		if ($this->equals($revisi, "n")) {
			if ($this->equals($sto, Surat_Jalan::$KEYWORD_SO))
				$no_sj_insert = H_Good_Issue::$SO_CODE . date("y") . str_pad($nomor_sj, 7, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_STO))
				$no_sj_insert = H_Good_Issue::$STO_CODE . date("y") . str_pad($nomor_sj, 8, "0", STR_PAD_LEFT);
			else if ($this->equals($sto, Surat_Jalan::$KEYWORD_SIRCLO))
				$no_sj_insert = H_Good_Issue::$SIRCLO_CODE . date("y") ."-". str_pad($nomor_sj, 5, "0", STR_PAD_LEFT);
			else
				$no_sj_insert = H_Good_Issue::$RETUR_CODE . date("y") . "-" . str_pad($nomor_sj, 7, "0", STR_PAD_LEFT);

			$arr[H_Good_Issue::$NOMOR_SJ] = strtoupper($no_sj_insert);

			//cek apakah surat jalan keluar sudah ada atau belum
			$this->db->where(H_Good_Issue::$NOMOR_SJ, $no_sj_insert);
			$this->db->where(H_Good_Issue::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$temp = $this->db->get(H_Good_Issue::$TABLE_NAME);
			if ($temp->num_rows() > 0 && $this->equals($revisi, "n")) {
				return H_Good_Issue::$MESSAGE_FAILED_INSERT_SJ_ALREADY_EXISTED;
			}
		} else {
			//cek apakah akses revisi diberikan
			$this->db->where(List_Access::$ACCESS, ACCESS_REVISION_SURAT_JALAN);
			$id_access = $this->db->get(List_Access::$TABLE_NAME)->row_array()[List_Access::$ID];

			//$arr_access[User_Access::$IS_TRUE]=0;
			$this->db->where(User_Access::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
			$this->db->where(User_Access::$ID_ACCESS, $id_access);
			$this->db->where(User_Access::$IS_TRUE,1);
			$is_permitted = $this->db->get(User_Access::$TABLE_NAME);

			//jika diijinkan
			if ($is_permitted->num_rows()>0) {

				$kd = strtoupper(H_Good_Issue::$REV_CODE) . date("y");
				$max_rev_code = $this->db->query("select max(substr(kode_revisi,8)) as max from " . H_Good_Issue::$TABLE_NAME . "
									where upper(kode_revisi) like '%" . $kd . "%'")->row()->max;
				if ($max_rev_code == null)
					$max_rev_code = 0;
				$max_rev_code += 1;
				$arr[H_Good_Issue::$KODE_REVISI] = $kd . str_pad($max_rev_code, 6, "0", STR_PAD_LEFT);
				$arr[H_Good_Issue::$NOMOR_SJ] = strtoupper($nomor_sj);

				//update revisi set 0
				$arr_access[User_Access::$IS_TRUE] = 0;
				$this->db->where(User_Access::$NIK, $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID]);
				$this->db->where(User_Access::$ID_ACCESS, $id_access);
				$this->db->update(User_Access::$TABLE_NAME, $arr_access);
			} else
				return H_Good_Issue::$MESSAGE_FAILED_INSERT_IS_NOT_PERMITTED;
		}
		$arr[H_Good_Issue::$STO] = $sto;
		$this->db->set(H_Good_Issue::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Issue::$QTY] = $ctr;
		$arr[H_Good_Issue::$EKSPEDISI] = $ekspedisi;
		$arr[$this::$IS_DELETED] = 0;

		//Cek apakah SJ Sudah pernah dibuat atau belum (kalau sudah, tanggal confirm admin akan diisi mengikuti inputan lalu)
		$this->db->where(H_Good_Issue::$NOMOR_SJ, $arr[H_Good_Issue::$NOMOR_SJ]);
		$h_good_issue = $this->db->get(H_Good_Issue::$TABLE_NAME);
		if ($h_good_issue->num_rows() > 0) {
			foreach ($h_good_issue->result_array() as $row) {
				if ($row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN] != null) {
					$arr[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN] = $row[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN];
					break;
				}
			}
		}

		$this->db->insert(H_Good_Issue::$TABLE_NAME, $arr);
		$counter = 1;
		for ($i = 0; $i < $ctr; $i++) {
			$this->db->where(Material::$KEMASAN, $arr_kemasan[$i]);
			$this->db->where(Material::$WARNA, $arr_warna[$i]);
			$this->db->where("Upper(" . Material::$KODE_BARANG . ")", strtoupper($arr_kode_barang[$i]));
			$material = $this->db->get(Material::$TABLE_NAME)->row_array();
			$id_material = $material[Material::$ID];
			$stock_out = ($arr_dus[$i] * $material[Material::$BOX]) + $arr_tin[$i];
			$mode = "";
			do {
				$this->db->from(Fifo_Stock_Gudang::$TABLE_NAME);
				if (!$this->equals($mode, Gudang::$S_CADANGAN)) {
					$select = [
						Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_GUDANG . " " . Fifo_Stock_Gudang::$ID_GUDANG,
						Fifo_Stock_Gudang::$ID,
						Fifo_Stock_Gudang::$QTY
					];
					$this->db->select($select);
					$join = Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_GUDANG . "=" . Gudang::$TABLE_NAME . "." . Gudang::$ID;
					$this->db->join(Gudang::$TABLE_NAME, $join);
					$this->db->where(Gudang::$RAK . " !=", Gudang::$S_CADANGAN);
					$this->db->where($this::$IS_DELETED,0);
				}
				$this->db->where(Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
				$this->db->where(Fifo_Stock_Gudang::$QTY . " !=", 0);

				$qry = $this->db->get();
				foreach ($qry->result_array() as $row) {
					$selisih = 0;
					if ($stock_out - $row[Fifo_Stock_Gudang::$QTY] <= 0) {
						$selisih = $stock_out;
						$stock_out = 0;
					} else {
						$selisih = $row[Fifo_Stock_Gudang::$QTY];
						$stock_out -= $selisih;
					}

					//Pengurangan Stock Gudang
					$pesan = "";
					if ($this->equals($revisi, "n"))
						$pesan = H_Good_Issue::GEN_INSERT_MESSAGE($no_sj_insert, $arr[H_Good_Issue::$ID]);
					else
						$pesan = H_Good_Issue::GEN_REVISION_MESSAGE($arr[H_Good_Issue::$NOMOR_SJ], $arr[H_Good_Issue::$KODE_REVISI], $arr[H_Good_Issue::$ID]);
					$this->insert_history_stock($id_material, $row[Gudang::$ID], 0, $selisih, $pesan);

					$arr_detail = [];
					$arr_detail[D_Good_Issue::$ID] = "DGI" . $kode . str_pad($counter, "3", "0", STR_PAD_LEFT);
					$counter += 1;
					$stock_dus = 0;
					$stock_tin = 0;
					if ($material[Material::$BOX] > 1) {
						$stock_dus = intdiv($selisih, $material[Material::$BOX]);
						$stock_tin = $selisih % $material[Material::$BOX];
					} else {
						$stock_tin = $selisih;
					}
					$arr_detail[D_Good_Issue::$DUS] = $stock_dus;
					$arr_detail[D_Good_Issue::$TIN] = $stock_tin;
					$arr_detail[D_Good_Issue::$ID_GUDANG] = $row[Gudang::$ID];

					$arr_detail[D_Good_Issue::$ID_MATERIAL] = $id_material;
					$arr_detail[D_Good_Issue::$ID_H_GOOD_ISSUE] = $arr[H_Good_Issue::$ID];
					$this->db->insert(D_Good_Issue::$TABLE_NAME, $arr_detail);

					//Fifo Stock Gudang
					$arr_fifo = [];
					$arr_fifo[Fifo_Stock_Gudang::$QTY] = $row[Fifo_Stock_Gudang::$QTY] - $selisih;
					$this->db->where(Fifo_Stock_Gudang::$ID, $row[Fifo_Stock_Gudang::$ID]);
					$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);

					if ($stock_out <= 0) {
						break;
					}
				}
				if ($stock_out > 0)
					$mode = Gudang::$S_CADANGAN;
			} while ($stock_out > 0);
		}

		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY, 0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//Pengecekan stock 0 pada gudang untuk pemindahan material
		$qry_gudang = $this->fg_model->gudang_get_cadangan();

		$kal = "";
		if ($qry_gudang->num_rows() > 0) {
			foreach ($qry_gudang->result_array() as $row_cadangan) {
				$stock_now = $row_cadangan[Fifo_Stock_Gudang::$QTY];
				$id_material = $row_cadangan[Fifo_Stock_Gudang::$ID_MATERIAL];
				$this->db->where(Material::$ID, $row_cadangan[Fifo_Stock_Gudang::$ID_MATERIAL]);
				$material = $this->db->get(Material::$TABLE_NAME)->row_array();
				$box = $material[Material::$BOX];
				$product_code = $material[Material::$PRODUCT_CODE];

				$fifo = $this->get(Fifo_Stock_Gudang::$TABLE_NAME, Fifo_Stock_Gudang::$ID_MATERIAL, $id_material, Fifo_Stock_Gudang::$ID, $this::$ORDER_TYPE_DESC, false);
				$last_fifo_id_gudang = $fifo->row_array()[Fifo_Stock_Gudang::$ID_GUDANG];
				$qry_fifo = $this->fg_model->gudang_get_by_last_fifo($last_fifo_id_gudang, $id_material);
			
				if ($qry_fifo->num_rows() > 0) {
					foreach ($qry_fifo->result_array() as $row_gudang) {
						$kapasitas = $row_gudang[Gudang::$KAPASITAS] * $box;
						$stock_move = 0;
						$kapasitas_terisi = $this->fifo_get_terisi_by_rak(
							$row_gudang[Gudang::$RAK],
							$row_gudang[Gudang::$KOLOM],
							$row_gudang[Gudang::$TINGKAT]
						)->row_array()[Fifo_Stock_Gudang::$S_TERISI];
						if ($kapasitas_terisi == 0) {
							if ($kapasitas - $stock_now >= 0) { //jika kapasitas masih cukup
								$stock_move = $stock_now;
							} else {
								$stock_move = $kapasitas;
							}

							if ($stock_move != 0) {
								//Pemindahan Barang dari Cadangan
								$gudang_to = $row_gudang[Gudang::$NAMA_GUDANG] . ' Rak ' . $row_gudang[Gudang::$RAK] . $row_gudang[Gudang::$KOLOM] . ' ' . $row_gudang[Gudang::$TINGKAT];
								$this->insert_history_stock($id_material, $row_cadangan[Gudang::$ID], 0, $stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

								//Pemindahan Barang ke Rak
								$this->insert_history_stock($id_material, $row_gudang[Gudang::$ID], $stock_move, 0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());


								$stock_now -= $stock_move;
								$arr_fifo_move_from = [];
								$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY] = $stock_now;
								$this->db->where(Fifo_Stock_Gudang::$ID, $row_cadangan[Fifo_Stock_Gudang::$ID]);
								$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_from);

								$arr_fifo_move_to = [];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL] = $row_gudang[Gudang::$ID_MATERIAL];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row_gudang[Gudang::$ID];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
								$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_to);

								$qty = $stock_move . " tin";

								$kal .= Stock_Gudang::GEN_MOVE_MATERIAL_FROM_CADANGAN_MESSAGE($product_code, $qty, $gudang_to);
							}
						}
					}
				}
				
				$qry_fifo = $this->db->query("select id_gudang,nama_gudang,id_material,kapasitas,rak,kolom,tingkat from gudang where gudang.id_gudang not in (select id_gudang from fifo_stock_gudang) and gudang.is_deleted=0 and gudang.id_material=" . $id_material);
				if ($qry_fifo->num_rows() > 0) {
					foreach ($qry_fifo->result_array() as $row_gudang) {
						$kapasitas = $row_gudang[Gudang::$KAPASITAS] * $box;
						$kapasitas_terisi = $this->fifo_get_terisi_by_rak(
							$row_gudang[Gudang::$RAK],
							$row_gudang[Gudang::$KOLOM],
							$row_gudang[Gudang::$TINGKAT]
						)->row_array()[Fifo_Stock_Gudang::$S_TERISI];
						if($kapasitas_terisi == 0){
							$stock_move = 0;
							if ($kapasitas - $stock_now >= 0) { //jika kapasitas masih cukup
								$stock_move = $stock_now;
							} else {
								$stock_move = $kapasitas;
							}

							if ($stock_move != 0) {
								//Pemindahan Barang dari Cadangan
								$gudang_to = $row_gudang[Gudang::$NAMA_GUDANG] . ' Rak ' . $row_gudang[Gudang::$RAK] . $row_gudang[Gudang::$KOLOM] . ' ' . $row_gudang[Gudang::$TINGKAT];
								$this->insert_history_stock($id_material, $row_cadangan[Gudang::$ID], 0, $stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

								//Pemindahan Barang ke Rak
								$this->insert_history_stock($id_material, $row_gudang[Gudang::$ID], $stock_move, 0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());


								$stock_now -= $stock_move;
								$arr_fifo_move_from = [];
								$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY] = $stock_now;
								$this->db->where(Fifo_Stock_Gudang::$ID, $row_cadangan[Fifo_Stock_Gudang::$ID]);
								$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_from);

								$arr_fifo_move_to = [];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL] = $row_gudang[Fifo_Stock_Gudang::$ID_MATERIAL];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row_gudang[Gudang::$ID];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
								$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_to);

								$qty = $stock_move . " tin";

								$kal .= Stock_Gudang::GEN_MOVE_MATERIAL_FROM_CADANGAN_MESSAGE($product_code, $qty, $gudang_to);
							}
						}
					}
				}
			}
		}
		if ($kal != "") {
			echo $this->alert($kal);
		}

		//hapus semua fifo yang qty 0 -> pasti cadangan
		$this->db->where(Fifo_Stock_Gudang::$QTY, 0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		echo $this->alert('Kode GI: ' . $arr[H_Good_Issue::$ID]);
		return H_Good_Issue::$MESSAGE_SUCCESS_INSERT;
	}
	public function insert_adjustment($ctr, $keterangan, $arr_kode_barang, $arr_kemasan, $arr_dus, $arr_tin, $arr_warna){
		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY, 0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//AUTO GEN kode surat jalan
		$max = $this->db->query("select max(substr(id_H_Good_Issue,8)) as max from " . H_Good_Issue::$TABLE_NAME . "
								where MONTH(tanggal)=" . date("m") . " and YEAR(tanggal)=" . date("Y"))->row()->max;
		if ($max == NULL)
			$max = 0;
		$max += 1;

		$kode = date("ym") . str_pad($max, 6, "0", STR_PAD_LEFT);

		//header
		$arr = [];
		$arr[H_Good_Issue::$ID] = "GI" . $kode;
		$arr[H_Good_Issue::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];

		$kd = strtoupper(H_Good_Issue::$ADJ_CODE);
		$max_adj_code = $this->db->query("select max(substr(nomor_sj,7)) as max from " . H_Good_Issue::$TABLE_NAME . "
							where upper(nomor_sj) like '%" . $kd . "%'")->row()->max;
		if ($max_adj_code == null)
			$max_adj_code = 0;
		$max_adj_code += 1;
		$arr[H_Good_Issue::$NOMOR_SJ] = $kd . str_pad($max_adj_code, 6, "0", STR_PAD_LEFT);

		$this->db->set(H_Good_Issue::$TANGGAL, 'NOW()', FALSE);
		$arr[H_Good_Issue::$QTY] = $ctr;
		$arr[$this::$IS_DELETED] = 0;
		$this->db->insert(H_Good_Issue::$TABLE_NAME, $arr);
		$counter = 1;
		for ($i = 0; $i < $ctr; $i++) {
			$this->db->where(Material::$KEMASAN, $arr_kemasan[$i]);
			$this->db->where(Material::$WARNA, $arr_warna[$i]);
			$this->db->where("Upper(" . Material::$KODE_BARANG . ")", strtoupper($arr_kode_barang[$i]));
			$material = $this->db->get(Material::$TABLE_NAME)->row_array();
			$id_material = $material[Material::$ID];
			$stock_out = ($arr_dus[$i] * $material[Material::$BOX]) + $arr_tin[$i];
			$mode = "";
			do {
				$this->db->from(Fifo_Stock_Gudang::$TABLE_NAME);
				if (!$this->equals($mode, Gudang::$S_CADANGAN)) {
					$select = [
						Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_GUDANG . " " . Fifo_Stock_Gudang::$ID_GUDANG,
						Fifo_Stock_Gudang::$ID,
						Fifo_Stock_Gudang::$QTY
					];
					$this->db->select($select);
					$join = Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_GUDANG . "=" . Gudang::$TABLE_NAME . "." . Gudang::$ID;
					$this->db->join(Gudang::$TABLE_NAME, $join);
					$this->db->where(Gudang::$RAK . " !=", Gudang::$S_CADANGAN);
					$this->db->where($this::$IS_DELETED, 0);
				}
				$this->db->where(Fifo_Stock_Gudang::$TABLE_NAME . "." . Fifo_Stock_Gudang::$ID_MATERIAL, $id_material);
				$this->db->where(Fifo_Stock_Gudang::$QTY . " !=", 0);

				$qry = $this->db->get();
				foreach ($qry->result_array() as $row) {
					$selisih = 0;
					if ($stock_out - $row[Fifo_Stock_Gudang::$QTY] <= 0) {
						$selisih = $stock_out;
						$stock_out = 0;
					} else {
						$selisih = $row[Fifo_Stock_Gudang::$QTY];
						$stock_out -= $selisih;
					}

					//Pengurangan Stock Gudang
					$this->insert_history_stock($id_material, $row[Fifo_Stock_Gudang::$ID_GUDANG], 0, $selisih, H_Good_Issue::GEN_ADJUSTMENT_MESSAGE($_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID], $arr[H_Good_Issue::$NOMOR_SJ]) . ", Ket: " . $keterangan);

					$arr_detail = [];
					$arr_detail[D_Good_Issue::$ID] = "DGI" . $kode . str_pad($counter, "3", "0", STR_PAD_LEFT);
					$counter += 1;
					$stock_dus = 0;
					$stock_tin = 0;
					if ($material[Material::$BOX] > 1) {
						$stock_dus = intdiv($selisih, $material[Material::$BOX]);
						$stock_tin = $selisih % $material[Material::$BOX];
					} else {
						$stock_tin = $selisih;
					}
					$arr_detail[D_Good_Issue::$DUS] = $stock_dus;
					$arr_detail[D_Good_Issue::$TIN] = $stock_tin;
					$arr_detail[D_Good_Issue::$ID_GUDANG] = $row[Fifo_Stock_Gudang::$ID_GUDANG];
					$arr_detail[D_Good_Issue::$ID_MATERIAL] = $id_material;
					$arr_detail[D_Good_Issue::$ID_H_GOOD_ISSUE] = $arr[H_Good_Issue::$ID];
					$this->db->insert(D_Good_Issue::$TABLE_NAME, $arr_detail);

					//Fifo Stock Gudang
					$arr_fifo = [];
					$arr_fifo[Fifo_Stock_Gudang::$QTY] = $row[Fifo_Stock_Gudang::$QTY] - $selisih;
					$this->db->where(Fifo_Stock_Gudang::$ID, $row[Fifo_Stock_Gudang::$ID]);
					$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo);

					if ($stock_out <= 0) {
						break;
					}
				}
				if ($stock_out > 0)
					$mode = Gudang::$S_CADANGAN;
			} while ($stock_out > 0);
		}

		//hapus semua fifo yang qty 0
		$this->db->where(Fifo_Stock_Gudang::$QTY, 0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		//Pengecekan stock 0 pada gudang untuk pemindahan material
		$qry_gudang = $this->fg_model->gudang_get_cadangan();

		$kal = "";
		if ($qry_gudang->num_rows() > 0) {
			foreach ($qry_gudang->result_array() as $row_cadangan) {
				$stock_now = $row_cadangan[Fifo_Stock_Gudang::$QTY];
				$id_material = $row_cadangan[Fifo_Stock_Gudang::$ID_MATERIAL];
				$this->db->where(Material::$ID, $row_cadangan[Fifo_Stock_Gudang::$ID_MATERIAL]);
				$material = $this->db->get(Material::$TABLE_NAME)->row_array();
				$box = $material[Material::$BOX];
				$product_code = $material[Material::$PRODUCT_CODE];

				$fifo = $this->get(Fifo_Stock_Gudang::$TABLE_NAME, Fifo_Stock_Gudang::$ID_MATERIAL, $id_material, Fifo_Stock_Gudang::$ID, "DESC", false);
				$last_fifo_id_gudang = $fifo->row_array()[Fifo_Stock_Gudang::$ID_GUDANG];
				$qry_fifo = $this->fg_model->gudang_get_by_last_fifo($last_fifo_id_gudang, $id_material);
			
				if ($qry_fifo->num_rows() > 0) {
					foreach ($qry_fifo->result_array() as $row_gudang) {
						$kapasitas = $row_gudang[Gudang::$KAPASITAS] * $box;
						$stock_move = 0;
						$kapasitas_terisi = $this->fifo_get_terisi_by_rak(
							$row_gudang[Gudang::$RAK],
							$row_gudang[Gudang::$KOLOM],
							$row_gudang[Gudang::$TINGKAT]
						)->row_array()[Fifo_Stock_Gudang::$S_TERISI];
						if ($kapasitas_terisi == 0) {
							if ($kapasitas - $stock_now >= 0) { //jika kapasitas masih cukup
								$stock_move = $stock_now;
							} else {
								$stock_move = $kapasitas;
							}

							if ($stock_move != 0) {
								//Pemindahan Barang dari Cadangan
								$gudang_to = $row_gudang[Gudang::$NAMA_GUDANG] . ' Rak ' . $row_gudang[Gudang::$RAK] . $row_gudang[Gudang::$KOLOM] . ' ' . $row_gudang[Gudang::$TINGKAT];
								$this->insert_history_stock($id_material, $row_cadangan[Gudang::$ID], 0, $stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

								//Pemindahan Barang ke Rak
								$this->insert_history_stock($id_material, $row_gudang[Gudang::$ID], $stock_move, 0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());

								$stock_now -= $stock_move;
								$arr_fifo_move_from = [];
								$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY] = $stock_now;
								$this->db->where(Fifo_Stock_Gudang::$ID, $row_cadangan[Fifo_Stock_Gudang::$ID]);
								$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_from);

								$arr_fifo_move_to = [];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL] = $row_gudang[Gudang::$ID_MATERIAL];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row_gudang[Gudang::$ID];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
								$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_to);

								$qty = $stock_move . " tin";

								$kal .= Stock_Gudang::GEN_MOVE_MATERIAL_FROM_CADANGAN_MESSAGE($product_code, $qty, $gudang_to);
							}
						}
					}
				}

				$qry_fifo = $this->db->query("select id_gudang,nama_gudang,id_material,kapasitas,rak,kolom,tingkat from gudang where gudang.id_gudang not in (select id_gudang from fifo_stock_gudang) and gudang.is_deleted=0 and gudang.id_material=" . $id_material);
				if ($qry_fifo->num_rows() > 0) {
					foreach ($qry_fifo->result_array() as $row_gudang) {
						$kapasitas = $row_gudang[Gudang::$KAPASITAS] * $box;
						$kapasitas_terisi = $this->fifo_get_terisi_by_rak(
							$row_gudang[Gudang::$RAK],
							$row_gudang[Gudang::$KOLOM],
							$row_gudang[Gudang::$TINGKAT]
						)->row_array()[Fifo_Stock_Gudang::$S_TERISI];
						if ($kapasitas_terisi == 0) {
							$stock_move = 0;
							if ($kapasitas - $stock_now >= 0) { //jika kapasitas masih cukup
								$stock_move = $stock_now;
							} else {
								$stock_move = $kapasitas;
							}

							if ($stock_move != 0) {
								//Pemindahan Barang dari Cadangan
								$gudang_to = $row_gudang[Gudang::$NAMA_GUDANG] . ' Rak ' . $row_gudang[Gudang::$RAK] . $row_gudang[Gudang::$KOLOM] . ' ' . $row_gudang[Gudang::$TINGKAT];
								$this->insert_history_stock($id_material, $row_cadangan[Gudang::$ID], 0, $stock_move, Stock_Gudang::GEN_MOVE_TO_RAK_MESSAGE($gudang_to));

								//Pemindahan Barang ke Rak
								$this->insert_history_stock($id_material, $row_gudang[Gudang::$ID], $stock_move, 0, Stock_Gudang::GEN_MOVE_FROM_CADANGAN_MESSAGE());


								$stock_now -= $stock_move;
								$arr_fifo_move_from = [];
								$arr_fifo_move_from[Fifo_Stock_Gudang::$QTY] = $stock_now;
								$this->db->where(Fifo_Stock_Gudang::$ID, $row_cadangan[Fifo_Stock_Gudang::$ID]);
								$this->db->update(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_from);

								$arr_fifo_move_to = [];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_MATERIAL] = $row_gudang[Fifo_Stock_Gudang::$ID_MATERIAL];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$ID_GUDANG] = $row_gudang[Gudang::$ID];
								$arr_fifo_move_to[Fifo_Stock_Gudang::$QTY] = $stock_move;
								$this->db->insert(Fifo_Stock_Gudang::$TABLE_NAME, $arr_fifo_move_to);

								$qty = $stock_move . " tin";

								$kal .= Stock_Gudang::GEN_MOVE_MATERIAL_FROM_CADANGAN_MESSAGE($product_code, $qty, $gudang_to);
							}
						}
					}
				}
			}
		}
		if ($kal != "") {
			echo $this->alert($kal);
		}

		//hapus semua fifo yang qty 0 -> pasti cadangan
		$this->db->where(Fifo_Stock_Gudang::$QTY, 0);
		$this->db->delete(Fifo_Stock_Gudang::$TABLE_NAME);

		echo $this->alert('Kode GI: ' . $arr[H_Good_Issue::$ID]);
		return H_Good_Issue::$MESSAGE_SUCCESS_ADJUSTMENT;
	}

	public function get_rekap_sj($dt, $st, $nomor_sj, $kode_barang, $kemasan, $warna, $sto){
		/*
		SELECT h_good_receipt.id_h_good_receipt,nomor_sj,tanggal,nik,material.id_material,material.box,sum(dus),sum(tin)
		FROM `d_good_receipt`,h_good_receipt,material
		where d_good_receipt.id_h_good_receipt=h_good_receipt.id_h_good_receipt AND
		d_good_receipt.id_material_fg=material.id_material
		group by h_good_receipt.id_h_good_receipt,nomor_sj,tanggal,nik,material.id_material
		*/
		$select=array(
			H_Good_Issue::$TABLE_NAME.".".H_Good_Issue::$ID." ". H_Good_Issue::$ID,
			H_Good_Issue::$NOMOR_SJ,
			H_Good_Issue::$TANGGAL,
			H_Good_Issue::$NIK,
			"sum(".D_Good_Issue::$DUS.")"." ". D_Good_Issue::$DUS,
			"sum(".D_Good_Issue::$TIN. ")" . D_Good_Issue::$TIN,
			D_Good_Issue::$ID_MATERIAL,
			H_Good_Issue::$TANGGAL_CONFIRM_ADMIN,
			Material::$BOX,
			Material::$ID_NEW
		);
		$group_by = array(
			H_Good_Issue::$ID,
			H_Good_Issue::$NOMOR_SJ,
			H_Good_Issue::$TANGGAL,
			H_Good_Issue::$NIK,
			D_Good_Issue::$ID_MATERIAL,
			H_Good_Issue::$TANGGAL_CONFIRM_ADMIN,
			Material::$BOX,
			Material::$ID_NEW
		);
		$this->db->select($select);
		$this->db->from(D_Good_Issue::$TABLE_NAME);
		$join = D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_H_GOOD_ISSUE . "=" . H_Good_Issue::$TABLE_NAME . "." . H_Good_Issue::$ID;
		$this->db->join(H_Good_Issue::$TABLE_NAME, $join);
		$join = D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_MATERIAL  . "=" . Material::$TABLE_NAME . "." . Material::$ID;
		$this->db->join(Material::$TABLE_NAME, $join);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Issue::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Issue::$TANGGAL . ' <', $until, TRUE);
		}
		if ($nomor_sj != NULL) {
			$this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);
		}
		if ($kode_barang != NULL) {
			$this->db->where(Material::$KODE_BARANG, $kode_barang);
		}
		if ($kemasan != NULL) {
			$this->db->where(Material::$KEMASAN, $kemasan);
		}
		if ($warna != NULL) {
			$this->db->where(Material::$WARNA, $warna);
		}
		if ($sto != NULL) {
			$this->db->where(H_Good_Issue::$STO, $sto);
		}
		$this->db->group_by($group_by);
		//$data[H_Good_Receipt::$TABLE_NAME]=$this->get(H_Good_Receipt::ta)
		return $this->db->get();
	}
	public function get_today_info(){
		$today=date("Y-m-d");
		$until = date("Y-m-d", strtotime($today . "+ 1 day"));
		$this->db->distinct();
		$this->db->select(H_Good_Issue::$NOMOR_SJ);
		$this->db->where(H_Good_Issue::$KODE_REVISI . $this::$WHERE_IS_NULL, NULL, FALSE);
		$this->db->where(H_Good_Issue::$TANGGAL . ' >=', $today, TRUE);
		$this->db->where(H_Good_Issue::$TANGGAL . ' <', $until, TRUE);
		return $this->db->get(H_Good_Issue::$TABLE_NAME);
	}
	public function get_kinerja_out($dt, $st, $nik = null){
		$select = [
			H_Good_Issue::$TANGGAL_CONFIRM_ADMIN,
			H_Good_Issue::$NIK_ADMIN,
			H_Good_Issue::$NIK,
			H_Good_Issue::$ID_EKSPEDISI,
			H_Good_Issue::$NIK_SOPIR,
			H_Good_Issue::$NIK_KERNET,
			H_Good_Issue::$NOMOR_SJ,
			H_Good_Issue::$EKSPEDISI,
			"sum(round((" . D_Good_Issue::$DUS . "*" . Material::$BOX . "+" . D_Good_Issue::$TIN . ")*" . Material::$GROSS . ",3)) as ". H_Good_Issue::$S_TOT_QTY
		];
		$this->db->select($select);
		$this->db->from(H_Good_Issue::$TABLE_NAME);
		$this->db->join(D_Good_Issue::$TABLE_NAME, H_Good_Issue::$TABLE_NAME . "." . H_Good_Issue::$ID . "=" . D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_H_GOOD_ISSUE);
		$this->db->join(Material::$TABLE_NAME, Material::$TABLE_NAME . "." . Material::$ID . "=" . D_Good_Issue::$TABLE_NAME . "." . D_Good_Issue::$ID_MATERIAL);
		$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . $this::$WHERE_IS_NOT_NULL,NULL, FALSE);
		
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . ' <', $until, TRUE);
		}
		if($nik!=null){
			$qry = $this->karyawan_model->get_by(Karyawan::$ID, $nik);
			if ($qry->num_rows() > 0) {
				$karyawan = $qry->row_array();
				if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_SOPIR)) //jika karyawan tsb adalah sopir
					$this->db->where(H_Good_Issue::$NIK_SOPIR, $nik);
				else if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_KERNET)) //jika karyawan tsb adalah kernet
					$this->db->where(H_Good_Issue::$NIK_KERNET, $nik);
				else if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_GENERAL_SOPIR_FK) || $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_SOPIR_FK)) {
				} //jika karyawan tsb adalah sopir fk
				//$this->db->where(H_Good_Issue::$NIK_SOPIR,$nik);	
				if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_GENERAL_CHECKER) || $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_CHECKER)) //jika karyawan tsb adalah checker
					$this->db->where(H_Good_Issue::$NIK, $nik);
			}
		}
		//$this->db->where(H_Good_Issue::$REV_GOOD_RECEIPT . $this::$WHERE_IS_NULL, NULL, FALSE);
		$group_by=[
			H_Good_Issue::$NIK,
			H_Good_Issue::$NOMOR_SJ
		];
		$this->db->group_by($group_by);
		return $this->db->get();
	}
	public function get_kinerja_in($nik, $nomor_sj){
		$select = [
			"sum(round((" . D_Good_Receipt::$DUS . "*" . Material::$BOX . "+" . D_Good_Receipt::$TIN . ")*" . Material::$GROSS . ",3)) as ". H_Good_Receipt::$S_TOT_QTY
		];
		$this->db->select($select);
		$this->db->from(H_Good_Receipt::$TABLE_NAME);
		$this->db->join(D_Good_Receipt::$TABLE_NAME, H_Good_Receipt::$TABLE_NAME . "." . H_Good_Receipt::$ID . "=" . D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_H_GOOD_RECEIPT);
		$this->db->join(Material::$TABLE_NAME, Material::$TABLE_NAME . "." . Material::$ID . "=" . D_Good_Receipt::$TABLE_NAME . "." . D_Good_Receipt::$ID_MATERIAL);
		$this->db->where(H_Good_Receipt::$REV_GOOD_ISSUE, 1);
		$this->db->where(H_Good_Receipt::$NIK, $nik);
		$this->db->where(H_Good_Receipt::$NOMOR_SJ, $nomor_sj);

		return $this->db->get();
	}
	public function get_kinerja_sisa($dt, $st){
		$arr_select = [
			H_Good_Issue::$NOMOR_SJ,
			H_Good_Issue::$TANGGAL,
			H_Good_Issue::$NIK
		];
		$this->db->select($arr_select);
		$this->db->where(H_Good_Issue::$REV_GOOD_RECEIPT . Library_Model::$WHERE_IS_NULL, NULL, FALSE); //artinya benar" dibuat di surat jalan keluar
		$this->db->where(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN . Library_Model::$WHERE_IS_NULL, NULL, FALSE);
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Issue::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Issue::$TANGGAL . ' <', $until, TRUE);
		}
		return $this->db->get(H_Good_Issue::$TABLE_NAME);
	}
	
}
