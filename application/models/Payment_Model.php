<?php
class Payment_Voucher_Detail_H{
	public static $TABLE_NAME = "payment_voucher_detail_h";
	public static $ID = "id_payment_header";

	public static $TANGGAL = "tanggal";
	public static $REFERENCE = "reference";
	public static $TEXT = "text";
	public static $TOTAL = "total";

	public static $S_INSERT_ID = "40";

	public static $ID_VENDOR = "id_vendor";
	public static $ID_TAX = "id_taxes";
	public static $ID_CANCEL = "id_cancel";

	public static $MESSAGE_SUCCESS_INSERT = "PDQ001";
	public static $MESSAGE_SUCCESS_UPDATE = "PDQ002";
	public static $MESSAGE_FAILED_SEARCH_NOT_FOUND = "PDQ003";
}
class Payment_Voucher_Detail_D{
	public static $TABLE_NAME = "payment_voucher_detail_d";
	public static $ID = "id_payment_detail";

	public static $AMOUNT = "amount";
	public static $DESCRIPTION = "description";
	public static $TIPE = "tipe";
	public static $ASSIGNMENT = "assignment";

	public static $ID_CC = "id_cc";
	public static $ID_GL = "id_gl";
	public static $ID_MATERIAL = "id_other_material";
	public static $ID_HEADER = "id_payment_header";
}
class Payment_Voucher_H{
	public static $TABLE_NAME = "payment_voucher_h";
	public static $ID = "id_payment_header";

	public static $TANGGAL = "tanggal";
	public static $REFERENCE = "reference";
	public static $TEXT = "text";
	public static $TOTAL = "total";
	public static $ASSIGNMENT = "assignment";
	public static $TIPE = "tipe";

	public static $S_INSERT_ID = "50";

	public static $ID_GL = "id_gl";
	public static $ID_CANCEL = "id_cancel";

	public static $PEMBULATAN_ID_GL = "pembulatan_id_gl";
	public static $PEMBULATAN_TIPE = "pembulatan_tipe";
	public static $PEMBULATAN_AMOUNT = "pembulatan_amount";
	public static $PEMBULATAN_ID_CC = "pembulatan_id_cc";
	public static $PEMBULATAN_DESCRIPTION = "pembulatan_description";

	public static $MESSAGE_SUCCESS_INSERT = "PHQ001";
}
class Payment_Voucher_D{
	public static $TABLE_NAME = "payment_voucher_d";
	public static $ID = "id_payment_detail";

	public static $AMOUNT = "amount";

	public static $ID_VENDOR = "id_vendor";
	public static $ID_HEADER = "id_payment_header";
}
class Payment_Voucher_Transaction{
	public static $TABLE_NAME = "payment_voucher_transaction";
	public static $ID = "id_payment_voucher_t";

	public static $AMOUNT = "amount";

	public static $ID_PV_DETAIL = "id_payment_voucher_d";
	public static $ID_PVD_HEADER = "id_payment_voucher_detail_h";
	public static $ID_CANCEL = "id_cancel";
}
class Taxes{
	public static $TABLE_NAME = "taxes";
	public static $ID = "id_taxes";

	public static $DESCRIPTION = "description";
	public static $VALUE = "value";
	
	public static $ID_GL = "id_gl";
	
}
class Payment_Voucher_Cancel{
	public static $TABLE_NAME = "payment_voucher_cancel";
	public static $ID = "id_payment_voucher_cancel";

	public static $TANGGAL = "tanggal";
	public static $REASON = "reason";

	public static $S_INSERT_ID = "60";

	public static $NIK = "nik";

	public static $MESSAGE_SUCCESS_INSERT = "PCQ001";
	public static $MESSAGE_FAILED_INSERT_TRANS_STILL_ACTIVE = "PCQ002";

}
class Payment_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}
	public function voucher_insert(
            $ctr,$tanggal,$reference,$text,$assignment,$id_gl,
            $pembulatan_id_gl,$pembulatan_tipe,$pembulatan_amount,$pembulatan_id_cc,$pembulatan_desc,
            $arr_id_vendor,$arr_total,$arr_detail_h,$arr_idx_detail_h,$arr_amount_detail_h
        ){
		
		$total_header=0;
		//Payment_Voucher_Detail_H
		$data = [
			Payment_Voucher_H::$TANGGAL => $tanggal,
			Payment_Voucher_H::$REFERENCE => $reference,
			Payment_Voucher_H::$TEXT => $text,
			Payment_Voucher_H::$ASSIGNMENT => $assignment,
			Payment_Voucher_H::$ID_GL => $id_gl,	
			Payment_Voucher_H::$PEMBULATAN_TIPE => $pembulatan_tipe,
			Payment_Voucher_H::$PEMBULATAN_AMOUNT => $pembulatan_amount,
			Payment_Voucher_H::$PEMBULATAN_DESCRIPTION => $pembulatan_desc,
		];
		if (!$this->equals($pembulatan_id_gl, "")) {
			$data[Payment_Voucher_H::$PEMBULATAN_ID_GL] = $pembulatan_id_gl;
		}
		if (!$this->equals($pembulatan_id_cc, "")) {
			$data[Payment_Voucher_H::$PEMBULATAN_ID_CC] = $pembulatan_id_cc;
		}

		//auto generate 5024 00001
		$this->db->select("max(substr(" . Payment_Voucher_H::$ID . ",5)) as " . Payment_Voucher_H::$ID);
		$this->db->where("YEAR(" . Payment_Voucher_H::$TANGGAL . ")", date("Y", strtotime($tanggal)));
		$this->db->like(Payment_Voucher_H::$ID, Payment_Voucher_H::$S_INSERT_ID, $this::$WHERE_LIKE_AFTER);
		$qry_payment = $this->db->get(Payment_Voucher_H::$TABLE_NAME)->row_array();
		$max = $qry_payment[Payment_Voucher_H::$ID];
		if ($max == null)
			$max = 0;
		$max += 1;

		$kode = date("y") . str_pad($max, 5, "0", STR_PAD_LEFT);
		$data[Payment_Voucher_H::$ID] = Payment_Voucher_H::$S_INSERT_ID . $kode;
		echo "<script>alert('Kode Payment: " . $data[Payment_Voucher_H::$ID] . "');</script>";
		
		$this->db->insert(Payment_Voucher_H::$TABLE_NAME, $data);
		$temp_idx = 0;

		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [];
			$data_detail[Payment_Voucher_D::$ID] = Payment_Voucher_H::$S_INSERT_ID . $kode . str_pad(++$temp_idx, 3, "0", STR_PAD_LEFT);
			$data_detail[Payment_Voucher_D::$ID_VENDOR] = $arr_id_vendor[$i];
			$data_detail[Payment_Voucher_D::$AMOUNT] = $arr_total[$i];
			$total_header += $arr_total[$i];
			$data_detail[Payment_Voucher_D::$ID_HEADER] = $data[Payment_Voucher_H::$ID];
			$this->db->insert(Payment_Voucher_D::$TABLE_NAME, $data_detail);
			for ($j = 0; $j < $arr_idx_detail_h[$i]; $j++) {
				//Payment Detail
				if (isset($arr_detail_h[$i][$j]) && !$this->equals($arr_detail_h[$i][$j], "")){
					$data_insert = [
						Payment_Voucher_Transaction::$ID_PV_DETAIL => $data_detail[Payment_Voucher_D::$ID],
						Payment_Voucher_Transaction::$ID_PVD_HEADER => $arr_detail_h[$i][$j],
						Payment_Voucher_Transaction::$AMOUNT => $arr_amount_detail_h[$i][$j]
					];
					$this->db->insert(Payment_Voucher_Transaction::$TABLE_NAME, $data_insert);
				}
			}
			
		}
		$total_header += ($pembulatan_amount * $pembulatan_tipe);
		//Update Total Header
		$data_update=[
			Payment_Voucher_H::$TOTAL => $total_header + ($pembulatan_amount * $pembulatan_tipe)
		];
		$this->db->where(Payment_Voucher_H::$ID, $data[Payment_Voucher_H::$ID]);
		$this->db->update(Payment_Voucher_H::$TABLE_NAME,$data_update);

		return Payment_Voucher_Detail_H::$MESSAGE_SUCCESS_INSERT;
	}
	public function voucher_cancel($id_header,$reason){
		$data = [];
		
		//Ambil Transaksi Payment Voucher
		$this->db->where(Payment_Voucher_H::$ID,$id_header);
		$row_payment_voucher=$this->db->get(Payment_Voucher_H::$TABLE_NAME)->row_array();

		$tanggal = $row_payment_voucher[Payment_Voucher_H::$TANGGAL];
		//auto generate 6024 00001
		$this->db->select("max(substr(" . Payment_Voucher_Cancel::$ID . ",5)) as " . Payment_Voucher_Cancel::$ID);
		$this->db->where("YEAR(" . Payment_Voucher_Cancel::$TANGGAL . ")", date("Y",strtotime($tanggal)));
		$qry_payment = $this->db->get(Payment_Voucher_Cancel::$TABLE_NAME)->row_array();
		$max = $qry_payment[Payment_Voucher_Cancel::$ID];
		
		if ($max == null)
			$max = 0;
		$max += 1;
		
		$kode = date("y") . str_pad($max, 5, "0", STR_PAD_LEFT);
		$data[Payment_Voucher_Cancel::$ID] = Payment_Voucher_Cancel::$S_INSERT_ID . $kode;
		echo "<script>alert('Kode Payment: " . $data[Payment_Voucher_Cancel::$ID] . "');</script>";

		//Input Cancel
		$data[Payment_Voucher_Cancel::$NIK] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
		$data[Payment_Voucher_Cancel::$REASON] = $reason;
		$data[Payment_Voucher_Cancel::$TANGGAL] = $tanggal;

		$this->db->insert(Payment_Voucher_Cancel::$TABLE_NAME, $data);

		//Update Semua Transaksi pada Payment_Voucher_Transaction menjadi id_cancel
		$data_update=[
			Payment_Voucher_Transaction::$ID_CANCEL => $data[Payment_Voucher_Cancel::$ID]
		];
		$this->db->like(Payment_Voucher_Transaction::$ID_PV_DETAIL, $id_header, $this::$WHERE_LIKE_AFTER);
		$this->db->update(Payment_Voucher_Transaction::$TABLE_NAME,$data_update);

		//Set id_cancel pada Payment_Voucher_H Lama menjadi kode cancel
		$data_update = [
			Payment_Voucher_H::$ID_CANCEL => $data[Payment_Voucher_Cancel::$ID]
		];
		$this->db->where(Payment_Voucher_H::$ID, $id_header);
		$this->db->update(Payment_Voucher_H::$TABLE_NAME, $data_update);

		//Input Transaksi Baru Cuma Semua dibalik

		//Header
		$row_payment_voucher[Payment_Voucher_H::$ID] = $data[Payment_Voucher_Cancel::$ID];
		$row_payment_voucher[Payment_Voucher_H::$TOTAL] = $row_payment_voucher[Payment_Voucher_H::$TOTAL] * -1;
		$row_payment_voucher[Payment_Voucher_H::$PEMBULATAN_AMOUNT] = $row_payment_voucher[Payment_Voucher_H::$PEMBULATAN_AMOUNT] * -1;
		$row_payment_voucher[Payment_Voucher_H::$ID_CANCEL] = $data[Payment_Voucher_Cancel::$ID];
		$this->db->insert(Payment_Voucher_H::$TABLE_NAME,$row_payment_voucher);

		//Detail
		$this->db->where(Payment_Voucher_D::$ID_HEADER,$id_header);
		$data_payment_voucher_d=$this->db->get(Payment_Voucher_D::$TABLE_NAME);
		if($data_payment_voucher_d->num_rows()>0){
			foreach($data_payment_voucher_d->result_array() as $row_detail){
				$row_detail[Payment_Voucher_D::$ID] = $row_payment_voucher[Payment_Voucher_H::$ID] . substr($row_detail[Payment_Voucher_D::$ID], 9);
				$row_detail[Payment_Voucher_D::$AMOUNT] *= -1;
				$row_detail[Payment_Voucher_D::$ID_HEADER] = $row_payment_voucher[Payment_Voucher_H::$ID];
				$this->db->insert(Payment_Voucher_D::$TABLE_NAME,$row_detail);
			}
		}

		return Payment_Voucher_Cancel::$MESSAGE_SUCCESS_INSERT;
	}

	public function voucher_detail_cancel($id_header,$reason){

		//Cek dulu apakah ada Transaksi yang aktif yang memakai voucher detail ini, jika ada tidak boleh

		$this->db->where(Payment_Voucher_Transaction::$ID_PVD_HEADER,$id_header);
		$this->db->where(Payment_Voucher_Transaction::$ID_CANCEL . Library_Model::$WHERE_IS_NULL, NULL, FALSE);

		$data_transaction = $this->db->get(Payment_Voucher_Transaction::$TABLE_NAME);
		if($data_transaction->num_rows()>0){ //Tidak boleh dicancel karena Ada Transaksi Aktif
			$arr_id_active = [];
			foreach($data_transaction->result_array() as $row_transaction){
				$this->db->where(Payment_Voucher_D::$ID, $row_transaction[Payment_Voucher_Transaction::$ID_PV_DETAIL]);
				$pv_detail=$this->db->get(Payment_Voucher_D::$TABLE_NAME);
				array_push($arr_id_active,$pv_detail->row_array()[Payment_Voucher_D::$ID_HEADER]);
			}
			echo "<script>alert('ID yang masih aktif: ". implode(",",$arr_id_active) ."');</script>";
			return Payment_Voucher_Cancel::$MESSAGE_FAILED_INSERT_TRANS_STILL_ACTIVE;
		}else{

			$data = [];
			
			//Ambil Transaksi Payment Voucher
			$this->db->where(Payment_Voucher_Detail_H::$ID,$id_header);
			$row_payment_voucher_detail=$this->db->get(Payment_Voucher_Detail_H::$TABLE_NAME)->row_array();

			$tanggal = $row_payment_voucher_detail[Payment_Voucher_Detail_H::$TANGGAL];
			//auto generate 6024 00001
			$this->db->select("max(substr(" . Payment_Voucher_Cancel::$ID . ",5)) as " . Payment_Voucher_Cancel::$ID);
			$this->db->where("YEAR(" . Payment_Voucher_Cancel::$TANGGAL . ")", date("Y",strtotime($tanggal)));
			$qry_payment = $this->db->get(Payment_Voucher_Cancel::$TABLE_NAME)->row_array();
			$max = $qry_payment[Payment_Voucher_Cancel::$ID];
			
			if ($max == null)
				$max = 0;
			$max += 1;
			
			$kode = date("y") . str_pad($max, 5, "0", STR_PAD_LEFT);
			$data[Payment_Voucher_Cancel::$ID] = Payment_Voucher_Cancel::$S_INSERT_ID . $kode;
			echo "<script>alert('Kode Payment: " . $data[Payment_Voucher_Cancel::$ID] . "');</script>";

			//Input Cancel
			$data[Payment_Voucher_Cancel::$NIK] = 111;//$_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];
			$data[Payment_Voucher_Cancel::$REASON] = $reason;
			$data[Payment_Voucher_Cancel::$TANGGAL] = $tanggal;

			$this->db->insert(Payment_Voucher_Cancel::$TABLE_NAME, $data);

			//Set id_cancel pada Payment_Voucher_H Lama menjadi kode cancel
			$data_update = [
				Payment_Voucher_Detail_H::$ID_CANCEL => $data[Payment_Voucher_Cancel::$ID]
			];
			$this->db->where(Payment_Voucher_Detail_H::$ID, $id_header);
			$this->db->update(Payment_Voucher_Detail_H::$TABLE_NAME, $data_update);

			//Input Transaksi Baru Cuma Semua dibalik

			//Header
			$row_payment_voucher_detail[Payment_Voucher_Detail_H::$ID] = $data[Payment_Voucher_Cancel::$ID];
			$row_payment_voucher_detail[Payment_Voucher_Detail_H::$TOTAL] = $row_payment_voucher_detail[Payment_Voucher_H::$TOTAL] * -1;
			$row_payment_voucher_detail[Payment_Voucher_Detail_H::$ID_CANCEL] = $data[Payment_Voucher_Cancel::$ID];
			$this->db->insert(Payment_Voucher_Detail_H::$TABLE_NAME, $row_payment_voucher_detail);

			//Detail
			$this->db->where(Payment_Voucher_Detail_D::$ID_HEADER,$id_header);
			$data_payment_voucher_detail_d=$this->db->get(Payment_Voucher_Detail_D::$TABLE_NAME);
			if($data_payment_voucher_detail_d->num_rows()>0){
				foreach($data_payment_voucher_detail_d->result_array() as $row_detail){
					$row_detail[Payment_Voucher_Detail_D::$ID] = $row_payment_voucher_detail[Payment_Voucher_Detail_H::$ID] . substr($row_detail[Payment_Voucher_Detail_D::$ID], 9);
					$row_detail[Payment_Voucher_Detail_D::$AMOUNT] *= -1;
					$row_detail[Payment_Voucher_Detail_D::$ID_HEADER] = $row_payment_voucher_detail[Payment_Voucher_Detail_H::$ID];
					$this->db->insert(Payment_Voucher_Detail_D::$TABLE_NAME,$row_detail);
				}
			}

			return Payment_Voucher_Cancel::$MESSAGE_SUCCESS_INSERT;
		}
	}
	public function voucher_detail_get_by_id_vendor($id_vendor){
		$arr_select=[
			Payment_Voucher_Detail_H::$ID,
			Payment_Voucher_Detail_H::$TANGGAL,
			Payment_Voucher_Detail_H::$ID_VENDOR,
			"(". Payment_Voucher_Detail_H::$TOTAL."-(COALESCE((select sum(". Payment_Voucher_Transaction::$AMOUNT .")
									from " . Payment_Voucher_Transaction::$TABLE_NAME . "
									where ". Payment_Voucher_Transaction::$TABLE_NAME .".". Payment_Voucher_Transaction::$ID_PVD_HEADER."=".
									Payment_Voucher_Detail_H::$ID." and " . Payment_Voucher_Transaction::$TABLE_NAME .".". Payment_Voucher_Transaction::$ID_CANCEL . " is null 
									group by ". Payment_Voucher_Detail_H::$ID ."
									),0))) as ". Payment_Voucher_Detail_H::$TOTAL
		];
		$this->db->select($arr_select);
		$this->db->like(Payment_Voucher_Detail_H::$ID, Payment_Voucher_Detail_H::$S_INSERT_ID, $this::$WHERE_LIKE_AFTER);
		$this->db->where(Payment_Voucher_Detail_H::$ID_VENDOR, $id_vendor);
		$this->db->where(Payment_Voucher_Detail_H::$ID_CANCEL . Library_Model::$WHERE_IS_NULL, NULL, FALSE);

		return $this->db->get(Payment_Voucher_Detail_H::$TABLE_NAME); 
	}
	public function voucher_detail_insert(
            $ctr,$id_vendor,$tanggal,$reference,$text,$tax,$total,
            $arr_id_gl,$arr_nominal,$arr_id_cc,$arr_tipe,$arr_description,$arr_assignment,$arr_order
        ){
		//Payment_Voucher_Detail_H
		$data = [
			Payment_Voucher_Detail_H::$ID_VENDOR => $id_vendor,
			Payment_Voucher_Detail_H::$TANGGAL => $tanggal,
			Payment_Voucher_Detail_H::$REFERENCE => $reference,
			Payment_Voucher_Detail_H::$TEXT => $text,
			Payment_Voucher_Detail_H::$TOTAL => $total
		];

		if($tax != -1){
			$data[Payment_Voucher_Detail_H::$ID_TAX] = $tax;
		}
		
		//auto generate 4024 00001
		$this->db->select("max(substr(" . Payment_Voucher_Detail_H::$ID . ",5)) as " . Payment_Voucher_Detail_H::$ID);
		$this->db->where("YEAR(" . Payment_Voucher_Detail_H::$TANGGAL . ")", date("Y", strtotime($tanggal)));
		$this->db->like(Payment_Voucher_Detail_H::$ID, Payment_Voucher_Detail_H::$S_INSERT_ID, $this::$WHERE_LIKE_AFTER);
		$qry_payment = $this->db->get(Payment_Voucher_Detail_H::$TABLE_NAME)->row_array();
		$max = $qry_payment[Payment_Voucher_Detail_H::$ID];
		if ($max == null) 
			$max = 0;
		$max += 1;

		$kode = date("y") . str_pad($max, 5, "0", STR_PAD_LEFT);
		$data[Payment_Voucher_Detail_H::$ID] = Payment_Voucher_Detail_H::$S_INSERT_ID . $kode;
		echo "<script>alert('Kode Payment: " . $data[Payment_Voucher_Detail_H::$ID] . "');</script>";
		

		$this->db->insert(Payment_Voucher_Detail_H::$TABLE_NAME,$data);
		for ($i = 0; $i < $ctr; $i++) {
			//Payment Detail
			$data_detail = [];
			$data_detail[Payment_Voucher_Detail_D::$ID] = Payment_Voucher_Detail_H::$S_INSERT_ID . $kode . str_pad($i + 1, 3, "0", STR_PAD_LEFT);
			$data_detail[Payment_Voucher_Detail_D::$ID_GL] = $arr_id_gl[$i];
			$data_detail[Payment_Voucher_Detail_D::$ID_CC] = $arr_id_cc[$i];
			$data_detail[Payment_Voucher_Detail_D::$TIPE] = $arr_tipe[$i];
			$data_detail[Payment_Voucher_Detail_D::$DESCRIPTION] = $arr_description[$i];
			$data_detail[Payment_Voucher_Detail_D::$ASSIGNMENT] = $arr_assignment[$i];
			if ($arr_order[$i] != NULL || $arr_order[$i] != "")
				$data_detail[Payment_Voucher_Detail_D::$ID_MATERIAL] = $arr_order[$i];
			$data_detail[Payment_Voucher_Detail_D::$AMOUNT] = $arr_nominal[$i];
			$data_detail[Payment_Voucher_Detail_D::$ID_HEADER] = $data[Payment_Voucher_Detail_H::$ID];
			$this->db->insert(Payment_Voucher_Detail_D::$TABLE_NAME, $data_detail);
		}
		
		return Payment_Voucher_Detail_H::$MESSAGE_SUCCESS_INSERT;
	}
	public function voucher_detail_update($ctr,$header,$text,$arr_detail,$arr_assignment,$arr_description){
		//Payment_Voucher_Detail_H
		$data = [
			Payment_Voucher_Detail_H::$TEXT => $text
		];
		
		$this->db->where(Payment_Voucher_Detail_H::$ID,$header);
		$this->db->update(Payment_Voucher_Detail_H::$TABLE_NAME,$data);

		//Payment Detail
		for ($i = 0; $i < $ctr; $i++) {
			$data_detail = [
				Payment_Voucher_Detail_D::$ASSIGNMENT => $arr_assignment[$i],
				Payment_Voucher_Detail_D::$DESCRIPTION => $arr_description[$i]
			];
			
			$this->db->where(Payment_Voucher_Detail_D::$ID,$arr_detail[$i]);
			$this->db->update(Payment_Voucher_Detail_D::$TABLE_NAME, $data_detail);
			
		}
		
		return Payment_Voucher_Detail_H::$MESSAGE_SUCCESS_UPDATE;
	}

	// public function jurnal_lain_insert(
	// 	$tanggal,
	// 	$reference,
	// 	$text,
	// 	$data_rekap
    //     ){
	// 	//Payment_Voucher_Detail_H

	// 	$data = [
	// 		Payment_Voucher_Detail_H::$ID_VENDOR => $id_vendor,
	// 		Payment_Voucher_Detail_H::$TANGGAL => $tanggal,
	// 		Payment_Voucher_Detail_H::$REFERENCE => $reference,
	// 		Payment_Voucher_Detail_H::$TEXT => $text,
	// 		Payment_Voucher_Detail_H::$TOTAL => $total
	// 	];

	// 	//auto generate 4024 00001
	// 	$this->db->select("max(substr(" . Payment_Voucher_Detail_H::$ID . ",5)) as " . Payment_Voucher_Detail_H::$ID);
	// 	$this->db->where("YEAR(" . Payment_Voucher_Detail_H::$TANGGAL . ")", date("Y", strtotime($tanggal)));
	// 	$this->db->like(Payment_Voucher_Detail_H::$ID, Payment_Voucher_Detail_H::$S_INSERT_ID, $this::$WHERE_LIKE_AFTER);
	// 	$qry_payment = $this->db->get(Payment_Voucher_Detail_H::$TABLE_NAME)->row_array();
	// 	$max = $qry_payment[Payment_Voucher_Detail_H::$ID];
	// 	if ($max == null) 
	// 		$max = 0;
	// 	$max += 1;

	// 	$kode = date("y") . str_pad($max, 5, "0", STR_PAD_LEFT);
	// 	$data[Payment_Voucher_Detail_H::$ID] = Payment_Voucher_Detail_H::$S_INSERT_ID . $kode;
	// 	echo "<script>alert('Kode Payment: " . $data[Payment_Voucher_Detail_H::$ID] . "');</script>";
		

	// 	$this->db->insert(Payment_Voucher_Detail_H::$TABLE_NAME,$data);
	// 	for ($i = 0; $i < $ctr; $i++) {
	// 		//Payment Detail
	// 		$data_detail = [];
	// 		$data_detail[Payment_Voucher_Detail_D::$ID] = Payment_Voucher_Detail_H::$S_INSERT_ID . $kode . str_pad($i + 1, 3, "0", STR_PAD_LEFT);
	// 		$data_detail[Payment_Voucher_Detail_D::$ID_GL] = $arr_id_gl[$i];
	// 		$data_detail[Payment_Voucher_Detail_D::$ID_CC] = $arr_id_cc[$i];
	// 		$data_detail[Payment_Voucher_Detail_D::$TIPE] = $arr_tipe[$i];
	// 		$data_detail[Payment_Voucher_Detail_D::$DESCRIPTION] = $arr_description[$i];
	// 		$data_detail[Payment_Voucher_Detail_D::$ASSIGNMENT] = $arr_assignment[$i];
	// 		if ($arr_order[$i] != NULL || $arr_order[$i] != "")
	// 			$data_detail[Payment_Voucher_Detail_D::$ID_MATERIAL] = $arr_order[$i];
	// 		$data_detail[Payment_Voucher_Detail_D::$AMOUNT] = $arr_nominal[$i];
	// 		$data_detail[Payment_Voucher_Detail_D::$ID_HEADER] = $data[Payment_Voucher_Detail_H::$ID];
	// 		$this->db->insert(Payment_Voucher_Detail_D::$TABLE_NAME, $data_detail);
	// 	}
		
	// 	return Payment_Voucher_Detail_H::$MESSAGE_SUCCESS_INSERT;
	// }

	public function get_history($dt, $st, $dg, $sg, $lg){
		$arr_select = [
			Payment_Voucher_Detail_D::$ASSIGNMENT,Payment_Voucher_Detail_D::$TABLE_NAME.".". Payment_Voucher_Detail_D::$ID." ". Payment_Voucher_Detail_D::$ID,
			Payment_Voucher_Detail_D::$TIPE,Payment_Voucher_Detail_H::$TANGGAL,Payment_Voucher_Detail_D::$AMOUNT,
			Payment_Voucher_Detail_H::$TEXT,Payment_Voucher_Detail_H::$REFERENCE, Payment_Voucher_Detail_D::$ID_MATERIAL,
			Payment_Voucher_Detail_H::$TABLE_NAME.".". Payment_Voucher_Detail_H::$ID." ". Payment_Voucher_Detail_H::$ID,
			Payment_Voucher_Detail_H::$ID_CANCEL, Payment_Voucher_Detail_D::$TABLE_NAME.".".Payment_Voucher_Detail_D:: $ID_GL . " " . Payment_Voucher_Detail_D::$ID_GL,
			Payment_Voucher_Detail_D::$ID_CC//,General_Ledger::$DESKRIPSI

		];
		
		$this->db->select($arr_select);
		$this->db->from(Payment_Voucher_Detail_H::$TABLE_NAME);
		$join = Payment_Voucher_Detail_H::$TABLE_NAME . "." . Payment_Voucher_Detail_H::$ID . "=" . Payment_Voucher_Detail_D::$TABLE_NAME . "." . Payment_Voucher_Detail_D::$ID_HEADER;
		$this->db->join(Payment_Voucher_Detail_D::$TABLE_NAME, $join);
		// $join = General_Ledger::$TABLE_NAME . "." . General_Ledger::$ID . "=" . Payment_Voucher_Detail_D::$TABLE_NAME . "." . Payment_Voucher_Detail_D::$ID_GL;
		// $this->db->join(General_Ledger::$TABLE_NAME, $join);
		
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(Payment_Voucher_Detail_H::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(Payment_Voucher_Detail_H::$TANGGAL . ' <', $until, TRUE);
		}
		if ($dg != NULL && $sg != NULL) {
			$this->db->where(Payment_Voucher_Detail_D::$ID_GL . ' >=', $dg, TRUE);
			$this->db->where(Payment_Voucher_Detail_D::$ID_GL . ' <=', $sg, TRUE);
		}
		if ($lg != NULL) {
			$this->db->where_in(Payment_Voucher_Detail_D::$ID_GL, $lg);
		}
		return $this->db->get();
	}
	public function get_rekap($id_header){
		$arr_select = [
			Payment_Voucher_Detail_D::$ASSIGNMENT,Payment_Voucher_Detail_D::$TABLE_NAME.".". Payment_Voucher_Detail_D::$ID." ". Payment_Voucher_Detail_D::$ID,
			Payment_Voucher_Detail_D::$TIPE,Payment_Voucher_Detail_H::$TANGGAL,Payment_Voucher_Detail_D::$AMOUNT,
			Payment_Voucher_Detail_H::$TEXT,Payment_Voucher_Detail_H::$REFERENCE,Payment_Voucher_Detail_D::$ID_MATERIAL
		];
		
		$this->db->select($arr_select);
		$this->db->from(Payment_Voucher_Detail_H::$TABLE_NAME);
		$join = Payment_Voucher_Detail_H::$TABLE_NAME . "." . Payment_Voucher_Detail_H::$ID . "=" . Payment_Voucher_Detail_D::$TABLE_NAME . "." . Payment_Voucher_Detail_D::$ID_HEADER;
		$this->db->join(Payment_Voucher_Detail_D::$TABLE_NAME, $join);

		if($id_header!=null){
			$this->db->where(Payment_Voucher_Detail_H::$ID,$id_header);
		}
		
		return $this->db->get();
	}
	public function get_newest($table,$column,$order,$limit){
		$this->db->select($column);
		$this->db->distinct();
		if($this->equals($table,Payment_Voucher_Detail_H::$TABLE_NAME)) //Jika PVD tidak boleh ambil cancelan
			$this->db->not_like(Payment_Voucher_Detail_H::$ID, Payment_Voucher_Cancel::$S_INSERT_ID, 'after');
		$this->db->order_by($order,$this::$ORDER_TYPE_DESC);
		$this->db->limit($limit);
		return $this->db->get($table);
	}
}
