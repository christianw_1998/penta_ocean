<?php
class Checker_RM_PM{
	public static $TABLE_NAME = "checker_rm_pm";
	public static $ID = "id_checker_rm_pm";
	
	public static $JENIS = "jenis";

	//Foreign Key
	public static $NIK = "NIK";

	//Static Variables
	public static $S_TYPE_POWDER = "POWDER";
	public static $S_TYPE_KEMASAN = "KEMASAN";
	public static $S_TYPE_CAIRAN = "CAIRAN";
}
class RMPMChecker_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}


}
