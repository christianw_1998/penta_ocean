<?php
class Kinerja_Model extends Library_Model {

	public function __construct() {
		parent::__construct(); 	
		$this->load->database(); 
	}
	public function fg_checker_calculate($dt, $st, $nik = null){

		$data = [];
		$data[KINERJA_KEY_TIPE] = "OUTSOURCE";
		if($nik!=null){
			$qry_karyawan = $this->karyawan_model->get_by(Karyawan::$ID, $nik);
			if ($qry_karyawan->num_rows() > 0) {
				$karyawan = $qry_karyawan->row_array();
				$data[KINERJA_KEY_TIPE] = $karyawan[Karyawan::$S_K_NAMA];
			}
		}
		//LCL CONTAINER
		$data[KINERJA_KEY_LCL] = 0;
		$data[KINERJA_KEY_CONTAINER] = 0;
		$query = $this->goodissue_model->get_kinerja_out($dt, $st, $nik);
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row_out) {
				if ($this->equals($row_out[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_LCL) || $this->equals($row_out[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_SIRCLO)) {
					$data[KINERJA_KEY_LCL] += $row_out[H_Good_Issue::$S_TOT_QTY];
				} else if ($this->equals($row_out[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_CONTAINER)) {
					$data[KINERJA_KEY_CONTAINER] += $row_out[H_Good_Issue::$S_TOT_QTY];
				}

				$total_minus = 0;
				$query_minus = $this->goodissue_model->get_kinerja_in($row_out[H_Good_Issue::$NIK], $row_out[H_Good_Issue::$NOMOR_SJ]);
				if ($query_minus->num_rows() > 0) {
					$total_minus = $query_minus->row_array()[H_Good_Receipt::$S_TOT_QTY];
				}

				if ($this->equals($row_out[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_LCL)|| $this->equals($row_out[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_SIRCLO)) {
					$data[KINERJA_KEY_LCL] -= $total_minus;
				} else if ($this->equals($row_out[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_CONTAINER)) {
					$data[KINERJA_KEY_CONTAINER] -= $total_minus;
				}
			}
		}

		//PAIL NONPAIL
		$data[KINERJA_KEY_PAIL] = 0;
		$data[KINERJA_KEY_NON_PAIL] = 0;
		$query = $this->goodreceipt_model->get_kinerja_in($dt, $st, $nik);
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row_in) {
				$total_minus = 0;
				$query_minus = $this->goodreceipt_model->get_kinerja_out($row_in[H_Good_Receipt::$NIK], $row_in[H_Good_Receipt::$NOMOR_SJ]);
				if ($this->equals($row_in[H_Good_Receipt::$S_JENIS], KINERJA_KEY_PAIL)) {
					$data[KINERJA_KEY_PAIL]+= $row_in[H_Good_Receipt::$S_TOT_QTY];
					if ($query_minus->num_rows() > 0) {
						foreach ($query_minus->result_array() as $row_minus)
							if ($this->equals($row_minus[H_Good_Receipt::$S_JENIS], KINERJA_KEY_PAIL))
								$total_minus = $query_minus->row_array()[H_Good_Issue::$S_TOT_QTY];
					}
					$data[KINERJA_KEY_PAIL] -= $total_minus;
				}else if ($this->equals($row_in[H_Good_Receipt::$S_JENIS], KINERJA_KEY_NON_PAIL)) {
					$data[KINERJA_KEY_NON_PAIL]+= $row_in[H_Good_Receipt::$S_TOT_QTY];
					if ($query_minus->num_rows() > 0) {
						foreach ($query_minus->result_array() as $row_minus)
							if ($this->equals($row_minus[H_Good_Receipt::$S_JENIS], KINERJA_KEY_NON_PAIL))
								$total_minus = $query_minus->row_array()[H_Good_Issue::$S_TOT_QTY];
					}
					$data[KINERJA_KEY_NON_PAIL] -= $total_minus;
					
				}
			}
		}
		//Pembalikkan nilai 0 untuk NIK sesuai bagiannya
		//container=a,lcl=b,pail=c,nonpail=d 
		//outsource=a b c d
		//sopir=b c d
		//kernet=b c d
		//checker=a b c d
		//sopir fk=a b d
		if ($nik != NULL) {
			if ($qry_karyawan->num_rows() > 0) {
				if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_SOPIR) || $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_KERNET)) {
					$data[KINERJA_KEY_CONTAINER] = 0;
				} else if ($this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_GENERAL_SOPIR_FK) || $this->equals($karyawan[Sub_Kategori::$ID], KARYAWAN_SUB_KATEGORI_PRODUKSI_SOPIR_FK)) {
					$data[KINERJA_KEY_PAIL] = 0;
				}
			}
		}
		return $data;
	}
	
	public function fg_get_good_issue($dt, $st){
		$this->db->select("distinct(" . H_Good_Issue::$NOMOR_SJ . ") as nomor_sj,id_ekspedisi");
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Issue::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Issue::$TANGGAL . ' <', $until, TRUE);
		}
		$this->db->not_like(H_Good_Issue::$NOMOR_SJ, "ADJ", 'after');
		$this->db->not_like(H_Good_Issue::$NOMOR_SJ, "REV", 'after');
		$this->db->order_by(H_Good_Issue::$TANGGAL_CONFIRM_ADMIN);
		return $this->db->get(H_Good_Issue::$TABLE_NAME);
	}
	public function fg_insert_good_issue($nomor_sj, $tanggal, $id_ekspedisi, $tipe_ekspedisi, $sopir, $kernet){
		$this->db->where(H_Good_Issue::$NOMOR_SJ, $nomor_sj);
		$data = [];
		$data[H_Good_Issue::$NIK_SOPIR] = NULL;
		$data[H_Good_Issue::$NIK_KERNET] = NULL;
		$data[H_Good_Issue::$NIK_ADMIN] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];

		if ($tipe_ekspedisi != null)
			$data[H_Good_Issue::$EKSPEDISI] = strtoupper($tipe_ekspedisi);

		if ($id_ekspedisi != null)
			$data[H_Good_Issue::$ID_EKSPEDISI] = $id_ekspedisi;

		if ($sopir != -1)
			$data[H_Good_Issue::$NIK_SOPIR] = $sopir;

		if ($kernet != -1)
			$data[H_Good_Issue::$NIK_KERNET] = $kernet;

		if ($tanggal != null)
			$data[H_Good_Issue::$TANGGAL_CONFIRM_ADMIN] = $tanggal;

		if ($this->equals($data[H_Good_Issue::$EKSPEDISI], KINERJA_KEY_CONTAINER)) {
			$data[H_Good_Issue::$NIK_KERNET] = NULL;
			$data[H_Good_Issue::$NIK_SOPIR] = NULL;
		}

		$this->db->update(H_Good_Issue::$TABLE_NAME, $data);
		//if($this->db->affected_rows() > 0)
		return H_Good_Issue::$MESSAGE_SUCCESS_UPDATE_KINERJA;
		//return H_Good_Issue::$MESSAGE_FAILED_UPDATE_KINERJA;
	}

	public function fg_insert_good_receipt($nomor_sj, $tanggal, $sopir, $kernet){
		$this->db->where(H_Good_Receipt::$NOMOR_SJ, $nomor_sj);
		$data = [];
		$data[H_Good_Receipt::$NIK_SOPIR] = NULL;
		$data[H_Good_Receipt::$NIK_KERNET] = NULL;
		$data[H_Good_Receipt::$NIK_ADMIN] = $_SESSION[SESSION_KARYAWAN_PENTA][Karyawan::$ID];

		if ($sopir != -1)
			$data[H_Good_Receipt::$NIK_SOPIR] = $sopir;

		if ($kernet != -1)
			$data[H_Good_Receipt::$NIK_KERNET] = $kernet;

		if ($tanggal != null)
			$data[H_Good_Receipt::$TANGGAL_CONFIRM_ADMIN] = $tanggal;

		
		$this->db->update(H_Good_Receipt::$TABLE_NAME, $data);
		//if($this->db->affected_rows() > 0)
		return H_Good_Receipt::$MESSAGE_SUCCESS_UPDATE_KINERJA;
		//return H_Good_Issue::$MESSAGE_FAILED_UPDATE_KINERJA;
	}
	public function fg_get_good_receipt($dt, $st){
		$this->db->select("distinct(" . H_Good_Receipt::$NOMOR_SJ . ") as nomor_sj,nik_sopir");
		if ($dt != NULL && $st != NULL) {
			$until = date("Y-m-d", strtotime($st . "+ 1 day"));
			$this->db->where(H_Good_Receipt::$TANGGAL . ' >=', $dt, TRUE);
			$this->db->where(H_Good_Receipt::$TANGGAL . ' <', $until, TRUE);
		}
		$this->db->where(H_Good_Receipt::$STO,KINERJA_KEY_RETUR);
		$this->db->order_by(H_Good_Receipt::$TANGGAL_CONFIRM_ADMIN);

		return $this->db->get(H_Good_Receipt::$TABLE_NAME);
	}
}
