<?php
class Library_Model extends CI_Model{

	public static $IS_DELETED = "is_deleted";

	public static $ORDER_TYPE_ASC = "asc";
	public static $ORDER_TYPE_DESC = "desc";

	public static $WHERE_LIKE_BOTH = "both";
	public static $WHERE_LIKE_BEFORE = "before";
	public static $WHERE_LIKE_AFTER = "after";
	
	public static $WHERE_IS_NULL=" is NULL ";
	public static $WHERE_IS_NOT_NULL=" is NOT NULL ";

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get($table, $field = null, $value = null, $order_by = null, $ascending = null, $has_is_deleted = true)
	{
		if ($has_is_deleted == true)
			$this->db->where($this::$IS_DELETED, 0);
		if ($field != null)
			$this->db->where("upper(" . $field . ")", strtoupper($value));
		if ($order_by != null) {
			if ($ascending != null)
				$this->db->order_by($order_by, $ascending);
			else
				$this->db->order_by($order_by);
		}
		return $this->db->get($table);
	}

	//Helper
	public function str_contains($haystack, $needle)
	{
		if (strpos(strtoupper($haystack), strtoupper($needle)) !== FALSE)
			return true;
		return false;
	}
	public function equals($string1, $string2)
	{
		if (strcmp(strtoupper($string1), strtoupper($string2)) !== 0)
			return false;
		return true;
	}
	public function contains_number($string)
	{
		for ($i = 0; $i < strlen($string); $i++) {
			if (is_numeric($string[$i]))
				return true;
		}
		return false;
	}
	public function contains_not_number($string)
	{
		for ($i = 0; $i < strlen($string); $i++) {
			if (!is_numeric($string[$i]))
				return true;
		}
		return false;
	}
	public function decimal($number, $params = null)
	{
		if ($params != null)
			return number_format($number, $params, '.', ',');

		return number_format($number, 2, '.', ',');
	}

	public function alert($message)
	{
		return "<script>alert('" . $message . "');</script>";
	}
}
