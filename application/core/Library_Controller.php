<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Library_Controller extends CI_Controller
{

	public static $UANG_HADIR;
	public static $UANG_LUAR_KOTA;

	public static $TODAY_INFO = "today_info";

	//All Navigation
	public static $NAV_ABSENSI_GAJI = "GAJI_ABSENSI";
	public static $NAV_ABSENSI_TABEL = "ABSENSI_TABEL";
	public static $NAV_ABSENSI_PERFORMANCE = "ABSENSI_PERFORMANCE";
	public static $NAV_SLIP_GAJI = "SLIP_GAJI";
	public static $NAV_REKAP_GAJI = "REKAP_GAJI";
	public static $NAV_REKAP_PERFORMANCE = "REKAP_PERFORMANCE";
	public static $NAV_GOOD_RECEIPT = "GOOD_RECEIPT";
	public static $NAV_GOOD_RECEIPT_ADJUSTMENT = "GOOD_RECEIPT_ADJUSTMENT";
	public static $NAV_MATERIAL_FISIK = "MATERIAL_FISIK";
	public static $NAV_GOOD_ISSUE = "GOOD_ISSUE";
	public static $NAV_GOOD_ISSUE_ADJUSTMENT = "GOOD_ISSUE_ADJUSTMENT";
	public static $NAV_USER_ACCESS = "USER_ACCESS";
	public static $NAV_USER_ACCESS_NO_RESTRICTION = "USER_ACCESS_NO_RESTRICTION";
	public static $NAV_KINERJA_GOOD_ISSUE = "KINERJA_GOOD_ISSUE";
	public static $NAV_WORK_SHEET_ADMIN_PROSES = "WS_ADMIN_PROSES_INSERT";
	public static $NAV_WORK_SHEET_CHECKER_FG_INSERT_GR = "WS_CHECKER_FG_INSERT_GR";
	public static $NAV_PURCHASE_ORDER_REVISI_QTY = "PO_REVISI_QTY";
	public static $NAV_RM_PM_GOOD_RECEIPT_MUTASI = "GOOD_RECEIPT_RM_PM_MUTASI";
	public static $NAV_RM_PM_GOOD_ISSUE_MUTASI = "GOOD_ISSUE_RM_PM_MUTASI";

	public static $MESSAGE_UPLOAD_SUCCESS = "FILE BERHASIL DI UPLOAD!!";
	public static $MESSAGE_UPLOAD_ERROR_FILE_NOT_SELECTED = "YOU DID NOT SELECT A FILE TO UPLOAD";
	public static $MESSAGE_UPLOAD_ERROR_NOT_ALLOWED_FILETYPE = "THE FILETYPE YOU ARE ATTEMPTING TO UPLOAD IS NOT ALLOWED";

	public function static_update_s_insentif()
	{
		$this::$UANG_HADIR = $this->library_model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_T_KEHADIRAN)->row_array()[Standar_Insentif::$NOMINAL];
		$this::$UANG_LUAR_KOTA = $this->library_model->get(Standar_Insentif::$TABLE_NAME, Standar_Insentif::$NAMA, S_INSENTIF_I_LUAR_KOTA)->row_array()[Standar_Insentif::$NOMINAL];
	}

	//db function
	public function insert($table)
	{
		$table = strtoupper($table);
		if ($this->equals($table, Karyawan::$TABLE_NAME)) {
			$ni = $this->i_p("ni");
			$na = $this->i_p("na");
			$np = $this->i_p("np");
			$g = $this->i_p("g");
			$t = $this->i_p("t");
			$c = $this->i_p("c");
			$r = $this->i_p("r");
			$k = $this->i_p("k");
			$io = $this->i_p("io");
			$sk = $this->i_p("sk");

			echo MESSAGE[$this->model->insert_karyawan($ni, $na, $np, $g, $t, $c, $r, $k, $io, $sk)];
		} else if ($this->equals($table, Kalender::$TABLE_NAME)) {
			$t = $this->i_p("t");
			$k = $this->i_p("k");

			echo MESSAGE[$this->model->insert_kalender($t, $k)];
		} else if ($this->equals($table, Cutoff::$TABLE_NAME)) {
			$tf = $this->i_p("tf");
			$tt = $this->i_p("tt");

			echo MESSAGE[$this->model->insert_cutoff($tf, $tt)];
		} else if ($this->equals($table, Standar_Insentif::$TABLE_NAME)) {
			$st = $this->i_p("stk");
			$si = $this->i_p("silk");
			
			echo MESSAGE[$this->model->insert_s_insentif($st, $si)];
		} else if ($this->equals($table, $this::$NAV_GOOD_RECEIPT)) {
			$sto = $this->i_p("s");
			$ctr = $this->i_p("c");
			$r = $this->i_p("r");
			$arr_kode_barang = $this->i_p("kb");
			$arr_kemasan_barang = $this->i_p("keb");
			$arr_dus_barang = $this->i_p("db");
			$arr_tin_barang = $this->i_p("tb");
			$arr_warna_barang = $this->i_p("wb");
			$no_sj = $this->i_p("n");
			
			echo MESSAGE[$this->goodreceipt_model->insert($ctr, $r, $sto, $no_sj, $arr_kode_barang, $arr_kemasan_barang, $arr_dus_barang, $arr_tin_barang, $arr_warna_barang)];
		} else if ($this->equals($table, $this::$NAV_GOOD_RECEIPT_ADJUSTMENT)) {
			$ctr = $this->i_p("c");
			$arr_kode_barang = $this->i_p("kb");
			$arr_kemasan_barang = $this->i_p("keb");
			$arr_dus_barang = $this->i_p("db");
			$arr_tin_barang = $this->i_p("tb");
			$arr_warna_barang = $this->i_p("wb");
			$keterangan = $this->i_p("k");

			echo MESSAGE[$this->goodreceipt_model->insert_adjustment($ctr, $keterangan, $arr_kode_barang, $arr_kemasan_barang, $arr_dus_barang, $arr_tin_barang, $arr_warna_barang)];
		} else if ($this->equals($table, $this::$NAV_GOOD_ISSUE)) {
			$sto = $this->i_p("s");
			$ctr = $this->i_p("c");
			$r = $this->i_p("r");
			$e = $this->i_p("e");
			$arr_kode_barang = $this->i_p("kb");
			$arr_kemasan_barang = $this->i_p("keb");
			$arr_dus_barang = $this->i_p("db");
			$arr_tin_barang = $this->i_p("tb");
			$arr_warna_barang = $this->i_p("wb");
			$no_sj = $this->i_p("n");

			echo MESSAGE[$this->goodissue_model->insert($ctr, $r, $sto, $e, $no_sj, $arr_kode_barang, $arr_kemasan_barang, $arr_dus_barang, $arr_tin_barang, $arr_warna_barang)];
		} else if ($this->equals($table, $this::$NAV_GOOD_ISSUE_ADJUSTMENT)) {
			$ctr = $this->i_p("c");
			$arr_kode_barang = $this->i_p("kb");
			$arr_kemasan_barang = $this->i_p("keb");
			$arr_dus_barang = $this->i_p("db");
			$arr_tin_barang = $this->i_p("tb");
			$arr_warna_barang = $this->i_p("wb");
			$keterangan = $this->i_p("k");

			echo MESSAGE[$this->goodissue_model->insert_adjustment($ctr, $keterangan, $arr_kode_barang, $arr_kemasan_barang, $arr_dus_barang, $arr_tin_barang, $arr_warna_barang)];
		} else if ($this->equals($table, Material_Fisik::$TABLE_NAME)) {
			$ctr = $this->i_p("c");
			$arr_kode_barang = $this->i_p("kb");
			$arr_kemasan_barang = $this->i_p("keb");
			$arr_stock_fisik_barang = $this->i_p("sfb");
			$arr_warna_barang = $this->i_p("wb");

			echo MESSAGE[$this->model->insert_material_fisik($ctr, $arr_kode_barang, $arr_kemasan_barang, $arr_stock_fisik_barang, $arr_warna_barang)];
		} else if ($this->equals($table, $this::$NAV_USER_ACCESS)) {
			$nik = $this->i_p("ni");
			$id_access = $this->i_p("ia");
			$is_true = $this->i_p("it");

			echo MESSAGE[$this->model->insert_user_access($nik, $id_access, $is_true)];
		} else if ($this->equals($table, $this::$NAV_USER_ACCESS_NO_RESTRICTION)) {
			$nik_bawahan = $this->i_p("nb");
			$nik_atasan = $this->i_p("na");
			$id_access = $this->i_p("i");
			$tipe = $this->i_p("t");

			echo MESSAGE[$this->model->insert_user_access_no_restriction($nik_bawahan, $nik_atasan, $id_access, $tipe)];
		} else if ($this->equals($table, $this::$NAV_KINERJA_GOOD_ISSUE)) {
			$sopir = $this->i_p("ns");
			$kernet = $this->i_p("nk");
			$sj = $this->i_p("n");
			$tipe_eks = $this->i_p("te");
			$eks = $this->i_p("ie");
			$tanggal = $this->i_p("t");
			echo MESSAGE[$this->model->kinerja_insert_good_issue($sj, $tanggal, $eks, $tipe_eks, $sopir, $kernet)];
		
		} else if ($this->equals($table, Purchase_Order_Checker_Header::$TABLE_NAME)) {
			$arr_id_material = $this->i_p("ai");
			$arr_no_urut = $this->i_p("an");
			$arr_qty = $this->i_p("aq");
			$arr_qty_sj = $this->i_p("qj");
			$nomor_po = $this->i_p("np");
			$nomor_sj = $this->i_p("ns");
			$tanggal = $this->i_p("t");
			$ctr = $this->i_p("c");
			
			echo MESSAGE[$this->po_model->checker_insert($ctr, $nomor_po, $nomor_sj, $tanggal, $arr_id_material, $arr_no_urut, $arr_qty, $arr_qty_sj)];
		} else if ($this->equals($table, $this::$NAV_PURCHASE_ORDER_REVISI_QTY)) {
			$arr_id_material = $this->i_p("ai");
			$arr_no_urut = $this->i_p("an");
			$arr_qty = $this->i_p("aq");
			$nomor_po = $this->i_p("np");
			$ctr = $this->i_p("c");
			
			echo MESSAGE[$this->po_model->revisi_qty($ctr, $nomor_po, $arr_id_material, $arr_no_urut, $arr_qty)];
		} else if ($this->equals($table, Worksheet_Checker_Header::$TABLE_NAME)) {
			$arr_id_material = $this->i_p("ai");
			$arr_no_urut = $this->i_p("an");
			$arr_qty = $this->i_p("aq");
			$arr_vendor = $this->i_p("av");
			$nomor_ws = $this->i_p("nw");
			$tanggal = $this->i_p("t");
			$ctr = $this->i_p("c");
			
			echo MESSAGE[$this->ws_model->checker_insert($ctr, $nomor_ws, $tanggal, $arr_id_material, $arr_vendor, $arr_no_urut, $arr_qty)];
		} else if ($this->equals($table, Worksheet_Checker_Header::$S_ADJUSTMENT_CHECKER)) {
			$arr_id_material = $this->i_p("ai");
			$arr_qty = $this->i_p("aq");
			$arr_vendor = $this->i_p("av");
			$nomor_ws = $this->i_p("nw");
			$tanggal = $this->i_p("t");
			$ctr = $this->i_p("c");
			
			echo MESSAGE[$this->ws_model->checker_adjustment($ctr, $nomor_ws, $tanggal, $arr_id_material, $arr_vendor, $arr_qty)];
		} else if ($this->equals($table, $this::$NAV_WORK_SHEET_ADMIN_PROSES)) {
			$kemasan = $this->i_p("k");
			$tin = $this->i_p("t");
			$berat = $this->i_p("b");
			$ws_semi = $this->i_p("wss");
			$ws_fg = $this->i_p("wsf");
			$premix = $this->i_p("p");
			$makeup = $this->i_p("m");
			$filling = $this->i_p("f");
			$tgl_persiapan = $this->i_p("tp");
			$tgl_premix = $this->i_p("tpr");
			$tgl_makeup = $this->i_p("tm");
			$tgl_cm = $this->i_p("tc");
			$tgl_qc = $this->i_p("tq");
			$tgl_filling = $this->i_p("tf");

			echo MESSAGE[$this->ws_model->admin_proses_insert($ws_semi, $ws_fg, $kemasan, $tin, $berat, $premix, $makeup, $filling, $tgl_persiapan, $tgl_premix, $tgl_makeup, $tgl_cm, $tgl_qc, $tgl_filling)];
		} else if ($this->equals($table, $this::$NAV_WORK_SHEET_CHECKER_FG_INSERT_GR)) {
			$no_ws = $this->i_p("n");
			
			echo MESSAGE[$this->ws_model->checker_fg_insert_gr($no_ws)];
		} else if ($this->equals($table, $this::$NAV_RM_PM_GOOD_RECEIPT_MUTASI)) {
			$ctr = $this->i_p("c");
			$arr_id_material = $this->i_p("ai");
			$arr_qty = $this->i_p("aq");
			$keterangan = $this->i_p("k");
			$arr_vendor = $this->i_p("av");
			
			echo MESSAGE[$this->rmpm_model->insert_gr_mutasi($ctr, $keterangan, $arr_id_material, $arr_qty, $arr_vendor)];
		} else if ($this->equals($table, $this::$NAV_RM_PM_GOOD_ISSUE_MUTASI)) {
			$ctr = $this->i_p("c");
			$arr_id_material = $this->i_p("ai");
			$arr_qty = $this->i_p("aq");
			$keterangan = $this->i_p("k");
			$arr_vendor = $this->i_p("av");
			
			echo MESSAGE[$this->rmpm_model->insert_gi_mutasi($ctr, $keterangan, $arr_id_material, $arr_qty, $arr_vendor)];
		} else if ($this->equals($table, Material_RM_PM_Fisik::$TABLE_NAME)) {
			$ctr = $this->i_p("c");
			$arr_id_material = $this->i_p("idm");
			$arr_fisik_0 = $this->i_p("af0");
			$arr_fisik_1 = $this->i_p("af1");
			
			echo MESSAGE[$this->rmpmfisik_model->insert($ctr, $arr_id_material, $arr_fisik_0, $arr_fisik_1)];
		} else if ($this->equals($table, Kebersihan_Harian::$TABLE_NAME)) {
			$ctr = $this->i_p("c");
			$arr_gudang = $this->i_p("g");
			$arr_kondisi = $this->i_p("k");
			
			echo MESSAGE[$this->kebersihan_model->harian_insert($ctr, $arr_gudang, $arr_kondisi)];
		} else if ($this->equals($table, Purchase_Order::$TABLE_NAME)){
			$idm = $this->i_p("am");
			$ids = $this->i_p("ad");
			$idv = $this->i_p("av");
			$iqy = $this->i_p("aq");
			$inp = $this->i_p("an");
			$ppn = $this->i_p("p");
			$ctr = $this->i_p("c");
			$tgl = $this->i_p("t");
			$tgl_krm = $this->i_p("tk");
			$pr = $this->i_p("pr");
			$pt = $this->i_p("pt");
			$d = $this->i_p("d");
			echo MESSAGE[$this->po_model->insert($pr,$pt,$ctr,$idm,$ids,$idv,$iqy,$inp,$ppn,$d,$tgl,$tgl_krm)];

		} else if ($this->equals($table, Other_Material::$TABLE_NAME)) {
			$ik = $this->i_p("ik");
			$im = $this->i_p("im");
			$d = $this->i_p("d");
			$ia = $this->i_p("ia");
			$ic = $this->i_p("ic");
			$iv = $this->i_p("iv");
			$ig = $this->i_p("ig");
			$nv = $this->i_p("nv");
			$mp = $this->i_p("mp");
			//$mm = $this->i_p("mm");
			//echo MESSAGE[$this->other_model->material_insert($ik, $im, $d, $ia, $ic, $iv, $ig, $nv, $mp, $mm)];
			echo MESSAGE[$this->other_model->material_insert($ik, $im, $d, $ia, $ic, $iv, $ig, $nv, $mp)];
		} else if ($this->equals($table, Other_Purchase_Order::$TABLE_NAME)) {
			$ctr = $this->i_p("c");
			$idm = $this->i_p("am");
			$dm = $this->i_p("dm");
			$ds = $this->i_p("ds");
			$ids = $this->i_p("as");
			$inp = $this->i_p("an");
			$iqy = $this->i_p("aq");
			$ppn = $this->i_p("pp");
			$pt = $this->i_p("pt");
			$tgl = $this->i_p("t");
			$kat = $this->i_p("k");

			echo MESSAGE[$this->other_model->po_insert($idm,$dm,$ds,$ids,$inp,$iqy,$ppn,$ctr,$tgl,$pt,$kat)];
		} else if ($this->equals($table, Other_GR_Header::$TABLE_NAME)) {
			$arr_id_material = $this->i_p("ai");
			$arr_no_urut = $this->i_p("an");
			$arr_qty = $this->i_p("aq");
			$nomor_po = $this->i_p("np");
			$nomor_sj = $this->i_p("ns");
			$tanggal = $this->i_p("t");
			$ctr = $this->i_p("c");

			echo MESSAGE[$this->other_model->gr_insert($ctr, $nomor_po, $nomor_sj, $tanggal, $arr_id_material, $arr_no_urut, $arr_qty)];
		} else if ($this->equals($table, Other_GI_Header::$TABLE_NAME)) {
			$ctr = $this->i_p("c");
			$tgl = $this->i_p("t");
			$idm = $this->i_p("am");
			$idc = $this->i_p("ac");
			$idg = $this->i_p("ag");
			$ial = $this->i_p("al");
			$k = $this->i_p("k");
			$q= $this->i_p("aq");
			echo MESSAGE[$this->other_model->gi_insert($ctr, $tgl, $idm, $idc, $idg,$ial,$q)];
		} else if ($this->equals($table, BOM_Production_Version::$TABLE_NAME)) {
			$idk = $this->i_p("ak");
			$idb = $this->i_p("ab");
			$idf = $this->i_p("af");
			$idt = $this->i_p("at");
			$ctr = $this->i_p("c");
			echo MESSAGE[$this->bom_model->pv_insert($ctr, $idk, $idb, $idf, $idt)];
		}
	}
	public function delete($table)
	{
		$id = $this->i_p('id');
		$table = strtoupper($table);
		if ($this->equals($table, Kalender::$TABLE_NAME)) {
			echo MESSAGE[$this->model->delete_kalender($id)];
		} else if ($this->equals($table, Cutoff::$TABLE_NAME)) {
			echo MESSAGE[$this->model->delete_cutoff($id)];
		} else if ($this->equals($table, Material::$TABLE_NAME)) {
			echo MESSAGE[$this->model->delete_material($id)];
		}
	}
	public function insert_f($table)
	{ //insert form
		$kal = "";
		$table = strtoupper($table);
		if ($this->equals($table, Standar_Insentif::$TABLE_NAME)) {
			$this->static_update_s_insentif();
			$kal .= "<h1 class='f-aleo-bold text-center'>DATA STANDAR INSENTIF</h1>";
			$kal .= "<br>";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= '<div class="col-sm-6">';
			$kal .= '<table class="table table-borderless">';
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Insentif Luar Kota (Rp)</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" value=' . $this::$UANG_LUAR_KOTA . ' class="f-aleo" type="number" id="txt_i_luar_kota"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Tunjangan Kehadiran (Rp)</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" value=' . $this::$UANG_HADIR . ' class="f-aleo" type="number" id="txt_t_kehadiran"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= '<td colspan=3 class="align-middle text-center">
						<button type="button" class="btn btn-outline-primary btn_modify_s_insentif" onclick="update_s_insentif()" data-mdb-ripple-color="dark">
						Update
						</button></td>';
			$kal .= "</tr>";
			$kal .= "</table>";
			$kal .= "</div>";
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= "</div>";
		} else if ($this->equals($table, Karyawan::$TABLE_NAME)) {
			$kal .= "<h1 class='f-aleo-bold text-center'>DATA KARYAWAN</h1>";
			$kal .= "<h6 class='f-aleo-bold text-center red-text'>* : Wajib Diisi</h6>";
			$kal .= "<br>";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= '<div class="col-sm-6">';
			$kal .= '<table class="table table-borderless">';
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>NIK*</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-left font-sm'>";
			$kal .= '<input onkeyup="check_nik()" style="width:50%" class="f-aleo" type="text" id="txt_nik"/>';
			$kal .= '<input disabled type="text" id="txt_status_nik" style="border:0;width:50%" class="f-aleo font-sm"></input>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Nama</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" class="f-aleo" type="text" id="txt_nama"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>NPWP</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" class="f-aleo" type="text" id="txt_npwp"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr class='role-kepala'>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Gaji Pokok (Rp)</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" value=0 class="f-aleo" type="number" id="txt_gp"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr class='role-kepala'>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Tunjangan (Rp)</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" value=0 class="f-aleo" type="number" id="txt_tunjangan"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			/*$kal.="<tr class='role-kepala'>";
							$kal.="<td class='align-middle text-right font-sm f-aleo'>Saldo Cuti (Hari)</td>";
							$kal.="<td class='align-middle text-right font-sm'>:</td>";
							$kal.="<td class='align-middle text-right font-sm'>";
								$kal.='<input style="width:100%" value=0 class="f-aleo" type="number" id="txt_s_cuti"/>';
							$kal.="</td>";
						$kal.="</tr>";*/
			$kal .= "<tr class='role-kepala'>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Role</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<select class="form-control f-aleo" id="dd_role">';
			$data = $this->model->get("role");
			foreach ($data->result() as $row) {
				$kal .= '<option value=' . $row->id_role . '>' . $row->nama . '</option>';
			}
			$kal .= '<option value=-1>TIDAK ADA</option>';
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Kategori</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<select onchange="check_sub_kategori()" class="form-control f-aleo" id="dd_kategori">';
			$data = $this->model->get("kategori_karyawan");
			foreach ($data->result() as $row) {
				$kal .= '<option value=' . $row->id_kategori . '>' . $row->tipe . '</option>';
			}
			$kal .= '<option value=-1>TIDAK ADA</option>';
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr class='tr_sub_kategori'>";
			$kal .= "<td class='td_sub_kategori align-middle text-right font-sm f-aleo'>Sub Kategori</td>";
			$kal .= "<td class='td_sub_kategori align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='td_sub_kategori td_sub_kategori_dd align-middle text-right font-sm'>";
			$kal .= '<select class="form-control f-aleo" id="dd_sub_kategori">';
			$kal .= '<option value=-1>TIDAK ADA</option>';
			$data = $this->model->get(Sub_Kategori::$TABLE_NAME);
			foreach ($data->result() as $row) {
				$kal .= '<option value=' . $row->id_sub_kategori . ' class="opt_sub_kategori opt_sub_kat_' . $row->id_kategori . '">' . strtoupper($row->nama) . '</option>';
			}
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= '<td></td>
							   <td></td>
							   <td>
								<div class="form-check f-aleo font-sm">
									<input class="form-check-input" type="checkbox" value="" id="cb_outsource">
									<label class="form-check-label" for="cb_outsource">
									OUTSOURCE
									</label>
								</div>
							   </td>';
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= '<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary btn_modify_karyawan" onclick="insert_karyawan()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
			$kal .= "</tr>";
			$kal .= "</table>";
			$kal .= "</div>";
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= "</div>";
		} else if ($this->equals($table, Kalender::$TABLE_NAME)) {
			$kal .= "<h1 class='f-aleo-bold text-center'>INSERT KALENDER</h1>";
			$kal .= "<br>";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= '<div class="col-sm-6">';
			$kal .= '<table class="table table-borderless">';
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Tanggal</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" class="f-aleo" type="date" id="txt_tanggal"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Keterangan</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<textarea type="area" class="f-aleo" style="width:100%" id="txt_keterangan"></textarea>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= '<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_calendar()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
			$kal .= "</tr>";
			$kal .= "</table>";
			$kal .= "</div>";
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= "</div>";
		} else if ($this->equals($table, Cutoff::$TABLE_NAME)) {
			$kal .= "<h1 class='f-aleo-bold text-center'>INSERT CUT OFF</h1>";
			$kal .= "<br>";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= '<div class="col-sm-6">';
			$kal .= '<table class="table table-borderless">';
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Dari Tanggal</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" class="f-aleo" type="date" id="txt_tanggal_from"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Sampai Tanggal</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<input style="width:100%" class="f-aleo" type="date" id="txt_tanggal_to"/>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= '<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_cutoff()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
			$kal .= "</tr>";
			$kal .= "</table>";
			$kal .= "</div>";
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= "</div>";
		} else if ($this->equals($table, Standar_Jam_Kerja::$TABLE_NAME)) {
			$kal .= "<h1 class='f-aleo-bold text-center'>INSERT STANDAR JAM KERJA</h1>";
			$kal .= "<br>";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= '<div class="col-sm-6">';
			$kal .= '<table class="table table-borderless">';
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Bulan</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<select class="form-control" id="dd_bulan">';
			for ($i = 1; $i < count(ARR_BULAN); $i++) {
				$kal .= '<option value=' . ARR_BULAN[$i] . '>' . ARR_BULAN[$i] . '</option>';
			}
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Tahun</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<select class="form-control" id="dd_tahun">';
			for ($i = 0; $i < count(ARR_TAHUN); $i++) {
				$kal .= '<option value=' . ARR_TAHUN[$i] . '>' . ARR_TAHUN[$i] . '</option>';
			}
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Jam Kerja (Menit)</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= "<input style='margin-top:7%' class='form-control' id='txt_jam_kerja' type='number'/>'";
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= '<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_s_jam_kerja()" data-mdb-ripple-color="dark">
							Tambah
						  </button></td>';
			$kal .= "</tr>";
			$kal .= "</table>";
			$kal .= "</div>";
			$kal .= '<div class="col-sm-3"></div>';
			$kal .= "</div>";
		} else if ($this->equals($table, $this::$NAV_USER_ACCESS_NO_RESTRICTION)) {
			$kal .= "<br>";
			$kal .= "<h3 class='f-aleo-bold text-center'>TAMBAH HAK AKSES</h3>";
			$kal .= "<br>";
			$kal .= '<div class="row" style="margin-right:1%">';
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= '<div class="col-sm-10">';
			$kal .= '<table class="table table-borderless">';
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Karyawan Bawahan</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<select class="form-control" id="dd_nik_bawahan">';
			$query = $this->model->get_all_karyawan();
			foreach ($query->result() as $row) {
				$kal .= '<option value=' . $row->nik . '>' . $row->nik . " - " . $row->k_nama . '</option>';
			}
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";

			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Karyawan Atasan</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<select class="form-control" id="dd_nik_atasan">';
			$query = $this->model->get_all_karyawan();
			foreach ($query->result() as $row) {
				$kal .= '<option value=' . $row->nik . '>' . $row->nik . " - " . $row->k_nama . '</option>';
			}
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Hak Akses</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<select class="form-control" id="dd_list_access" onchange="change_desc_text()">';
			$query = $this->model->get(List_Access::$TABLE_NAME);
			foreach ($query->result() as $row) {
				$kal .= '<option id="' . $row->description . '" value=' . $row->id_access . '>' . $row->access . '</option>';
			}
			$kal .= '</select>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= "<td class='align-middle text-right font-sm f-aleo'>Deskripsi</td>";
			$kal .= "<td class='align-middle text-right font-sm'>:</td>";
			$kal .= "<td class='align-middle text-right font-sm'>";
			$kal .= '<textarea rows=5 style="width:100%;resize: none" class="f-aleo" disabled type="text" id="txt_deskripsi"></textarea>';
			$kal .= "</td>";
			$kal .= "</tr>";
			$kal .= "<tr>";
			$kal .= '<td colspan=3 class="align-middle text-center"><button type="button" class="btn btn-outline-primary" onclick="insert_user_access()" data-mdb-ripple-color="dark">
								Tambah
							</button></td>';
			$kal .= "</tr>";
			$kal .= "</table>";
			$kal .= "</div>";
			$kal .= '<div class="col-sm-1"></div>';
			$kal .= "</div>";
		}
		echo $kal;
	}

	//helper function
	public function i_p($value)
	{
		return $this->input->post($value);
	}
	public function rupiah($number, $with_decimal = 0)
	{
		return number_format($number, $with_decimal, '.', ',');
	}
	public function decimal($number, $params = 2)
	{
		return number_format($number, $params, '.', ',');
	}
	public function capitalize($words)
	{
		return ucwords(strtolower($words));
	}
	public function equals($string1, $string2)
	{
		if (strcmp(strtoupper($string1), strtoupper($string2)) !== 0)
			return false;
		return true;
	}
	public function str_contains($haystack, $needle)
	{
		if (strpos(strtoupper($haystack), strtoupper($needle)) !== FALSE)
			return true;
		return false;
	}
	public function get_index_from_array($array, $key)
	{
		for ($i = 0; $i < count($array); $i++) {
			if ($this->equals($array[$i], $key))
				return $i;
		}
		return -1;
	}
	public function get_message($data)
	{
		if ($this->str_contains($data, $this::$MESSAGE_UPLOAD_ERROR_FILE_NOT_SELECTED)) {
			return "File belum dipilih, silahkan cek lagi!";
		} else if ($this->str_contains($data, $this::$MESSAGE_UPLOAD_ERROR_NOT_ALLOWED_FILETYPE)) {
			return "File yang diupload bukan dalam format xlsx!";
		}
		return $data;
	}
	public function contains_number($string)
	{
		for ($i = 0; $i < count($string); $i++) {
			if (is_numeric($string[$i]))
				return true;
		}
		return false;
	}
	public function gen_table_header($arr_header)
	{
		$kal = "";
		$kal .= '<tr class="table-primary">';
		for ($i = 0; $i < count($arr_header); $i++) {
			$kal .= '<th scope="col" class="fw-bold align-middle text-center">' . $arr_header[$i] . '</th>';
		}
		$kal .= '</tr>';
		return $kal;
	}
	public function insert_empty_td($count)
	{
		$kal = "";
		for ($i = 0; $i < $count; $i++) {
			$kal .= "<td></td>";
		}
		return $kal;
	}
}
